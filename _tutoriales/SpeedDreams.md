---
title: Speed Dreams
description: Compilar Speed Dreams en Ubuntu y Debian (y sus derivados)
layout: page
---

La razón para seguir este tutorial puede ser variada. Puede que no quieras o no puedas usar las opciones actuales para instalar el juego con paquetes preconstruidos ([Ubuntu PPA](https://sourceforge.net/projects/speed-dreams/files/2.3.0/), [Flatpak](https://flathub.org/apps/org.speed_dreams.SpeedDreams) or [AppImage](https://xtradeb.net/play/speed-dreams/)); o tal vez te gustaría probar los últimos cambios en el código. Sea cual sea tu razón, aquí tienes los pasos para compilar el juego para estas distribuciones, Ubuntu y Debian, y sus derivadas:

# -Paso 1: Instalar los requisitos necesarios para construir y ejecutar el juego. 

Speed Dreams necesita bastantes dependencias/bibliotecas para construirse y ejecutarse. Para instalarlas vamos a abrir un terminal y ejecutar el siguiente comando de instalación:

`sudo apt-get install build-essential cmake libplib-dev libogg-dev libenet-dev libexpat1-dev libpng-dev libjpeg8-dev libopenscenegraph-dev libsdl2- dev libplib1 libvorbis-dev libcurl4-gnutls-dev libopenal-dev gcc make perl libxmu-dev libsdl2-mixer-dev openjdk-18-jre openjdk-18-jdk libsqlite3-dev`

Las tres últimas dependencias son necesarias para compilar **Trackeditor** (openjdk-18-jre openjdk-18-jdk) **y habilitar las repeticiones** (libsqlite3-dev). En el caso de las primeras, Java, **en Debian puede ser necesario utilizar openjdk-17-jre y openjdk-17-jdk**.

# -Paso 2: Descargar el código fuente. Aquí tenemos dos opciones:

***Opción 1*:** Si quieres compilar el último código fuente oficial, debes descargarlo primero. Para ello debes ir al [repositorio de Speed Dreams en Sourceforge](https://sourceforge.net/projects/speed-dreams/files/2.3.0/), en "Files", y **descargar los paquetes de código fuente de la última versión, 2.3.0**:

~~~
speed-dreams-src-base-2.3.0-r8786.tar.xz
speed-dreams-src-hq-coches-y-pistas-2.3.0-r8786.tar.xz
speed-dreams-src-más-coches-y-pistas-hq-2.3.0-r8786.tar.xz
speed-dreams-src-wip-cars-and-tracks-2.3.0-r8786.tar.xz
~~~

A continuación, debes crear una carpeta llamada ***"speed-dreams-code "***:

`mkdir speed-dreams-code`

Ahora debemos extraer el contenido de los archivos descargados dentro de esta carpeta siguiendo el orden mostrado anteriormente, y sobrescribiendo el contenido siempre que sea necesario.
	
***Opción 2:*** Si quieres compilar el último código en desarrollo (trunk) con los últimos commits, debes **descargarlo con subversion**. Para instalarlo previamente, escribe el siguiente comando en un terminal:

`sudo apt install subversion`.
	
A continuación descargarás el último código fuente:

`svn checkout https://svn.code.sf.net/p/speed-dreams/code/trunk speed-dreams-code`
	
Este proceso llevará mucho tiempo, así que te recomiendo que seas paciente.

# -Paso 3: Preparar y configurar el código fuente: 
En este punto ya tenemos el código fuente que vamos a compilar en la carpeta "***speed-dreams-code***". Ahora tenemos que crear una carpeta llamada "***build***" dentro de ella. Para ello:

~~~
cd speed-dreams-code
mkdir build
cd build
~~~
	
**Ahora vamos a configurar la compilación con cmake**. Podemos hacerlo de varias maneras. Voy a proponer algunas:

-Configurar la compilación para activar el motor gráfico OpenSceneGraph y sólo el contenido oficial.

`cmake -D OPTION_OSGGRAPH:BOOL=ON -D OPTION_OFFICIAL_ONLY:BOOL=ON ..`
    
-Configurar la compilación para activar el motor OpenSceneGraph Graph y todo el contenido, incluyendo WIP y Desarrollo:

`cmake -D OPTION_OSGGRAPH:BOOL=ON -D OPTION_OFFICIAL_ONLY:BOOL=OFF ..`
    
-Configurar la compilación para activar el motor OpenSceneGraph Graph, todo el contenido, incluidos WIP y Desarrollo, y activar las repeticiones :

`cmake -D OPTION_OSGGRAPH:BOOL=ON -D OPTION_OFFICIAL_ONLY:BOOL=OFF -D OPTION_3RDPARTY\_SQLITE3:BOOL=ON ..`

# -Paso 4: Construir el código fuente:

El último paso para disfrutar del juego es sencillo. Sólo tienes que ejecutar este comando en el terminal:  

`make`

Lo más probable es que tu PC permita más de un hilo de procesamiento, por lo que aprovecharlos puede ahorrarte mucho tiempo. Para ello sólo tienes que utilizar "***make -jX***", **donde X es el número de hilos que queremos utilizar**. Por ejemplo, si nuestro procesador tiene 4 núcleos/hilos se puede usar:
`make -j4`
	
Una vez finalizado este proceso, espero que sin errores, ya puedes ejecutar el juego. Para ello, navega hasta "***speed-dreams-code/build/games***", y haz doble click en "***speed-dreams-2***", o si lo deseas, utiliza el terminal "***./speed-dreams-2***".
# -Paso 5 (Opcional): Instalar el juego en tu sistema operativo. 
Como puse arriba, este paso es opcional, pero si quieres hacerlo, es tan simple como hacer:  

`sudo make install`

Al terminar probablemente verás el lanzador de Speed Dreams en el menú de aplicaciones. Si no es así, probablemente bastará con cerrar la sesión de usuario y volver a abrirla.
**En caso de que quieras ejecutarlo en un terminal simplemente escribe:**  

`speed-dreams-2`

# -Paso 6 (Opcional): Actualizar el juego a la última versión de Desarrollo:
Para los que hayáis usado subversion para descargar la última versión de desarrollo del código, en caso de que haya nuevos commits y queráis construirlos, sólo tenéis que ir a la carpeta "***speed-dreams-code/build***", y dentro de ella ejecutar este comando en terminal:  

`svn udpate`

**Después usarás el comando cmake con las opciones que quieras y finalmente make de nuevo** (y `sudo make install` de nuevo si quieres instalarlo)

Este tutorial probablemente sea válido para otras distribuciones. La diferencia estará en la forma de instalar las dependencias y librerías, así que puedes probar. Agradecería vuestras respuestas. Espero que este tutorial os sea de utilidad. 

¡SALUDOS!
