export function shareMastodon() {
	$('#mastodon-share').on('click', function() {
		const instace = prompt('Indica la instancia de Mastodon a usar', 'mastodon.social');
		if (!instace) return;
		const title= $(this).attr('data-title');
		const url = window.location.href;
		const shareLink = `https://${instace}/share?text=${title}%20${url}`;
		window.open(shareLink, '_blank')
	})
}
	