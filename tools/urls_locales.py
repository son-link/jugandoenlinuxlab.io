#!/usr/bin/env python3

# Este script es para reemplazar todas las URL internas de la web
import re
from sys import argv
from os.path import isfile

regex_articulos = r"\(index\.php\/homepage\/([0-9a-zA-Z]+)\/(\d+)-([\w\-]+)\)"
regex_tags = r"\(index\.php\/component\/tags\/tag\/([\w\-]+)\)"
subst_articulos = "({{ \"/posts/\\3\" | absolute_url }})"
subst_tags = "({{ \"/tags/\\1\" | absolute_url }})"

if len(argv) != 2:
    print('Se debe de pasar la ruta al fichero')
    exit(1)

if not isfile(argv[1]):
    print(f'El fichero {argv[0]} no existe')
    exit(1)

with open(argv[1], 'r') as fp:
    content = fp.read()
    fp.close()
    content = re.sub(regex_articulos, subst_articulos, content, 0)
    content = re.sub(regex_tags, subst_tags, content, 0)

    with open(argv[1], 'w') as fp:
        fp.write(content)
        fp.close()
