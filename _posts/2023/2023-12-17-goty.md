---
author: Serjor
category: Editorial
date: 2023-12-27 12:00:00
excerpt: Muchos lo han intentado, pero pocos lo han conseguido, entra para saber qué juegos nos han llamado la atención este año
image: 
layout: post
tags:
  - GOTY
  - JuegoDelAño
  - JDADCASJEL
title: El JDADCASJEL (El Juego Del Año, De Cualquier Año, Según Jugando En Linux)
---

Sobre estas fechas hay dos tradiciones canónicas en el mundo de la prensa de los videojuegos:
1. Resumen de cómo ha ido el año
2. Decir cuál es el GOTY

En JEL, que vamos un poco a lo nuestro, lo de hacer un resumen no suele ser lo nuestro, y lo del GOTY, qué os vamos a decir, no hay juego más divertido que instalar Gentoo con una mano atada a la espalda y los ojos vendados (Y que venga ahora nadie a decirnos que pasarse Dark Souls III de no-hit es un reto...), y por lo tanto nuestro GOTY ya está decidido, ahora y siempre.

Igual también influye un poco a la hora de elegir un juego el hecho de que no somos prensa especializada, que estamos aquí más por la comunidad que por jugar, y que un juego que salió hace 7 años, pero lo estamos jugando hoy nos parece tan buen candidato a juego del año como cualquiera estrenado este 2023, y por eso os traemos nuestros JDADCASJEL, y decimos **nuestros**, en plural, porque esto no es una competición, no hay vencedores ni vencidos, ni mejores ni peores, el juego del año es algo muy particular y personal de cada uno, y si un juego nos ha dado lo que buscábamos en ese momento, como que todo lo demás no importa.

Ahora ya, sin más preámbulos, los juegos que hemos jugado este año, y nos apetece recomendar:

# Serjor

Este año ha sido galardonado como uno de los mejores de la historia en el mundo de los videojuegos, y puede ser. Yo pensaba que el año pasado fue un buen año, pero en este han salido unos cuántos juegos que tengo apuntados en mi lista de deseados para cuando pueda hacerme con ellos. Quizás en uno o dos años, juegos como Baldur's Gate 3 o Spider-Man 2 aparezcan en mi lista de juegos del año, pero no será en este.

Este año traigo dos recomendaciones, que nada tiene que ver el uno con el otro, pero que he disfrutado un montón:

## Deathloop
¿Tenéis la Steam Deck? ¿Se la habéis dejado a algún colega, y ha dejado su biblioteca como compartida con vuestra cuenta? ¿Habéis jugado un rato a DeatLoop, y cuándo por lo que sea no habéis podido usar su biblioteca, habéis ido a comprarlo porque estabais engorilados con el juego? Si es que sí, me entendéis perfectamente. Si es que no, voy a intentar resumir porqué me ha gustado tanto. 

<div class="resp-iframe">
<iframe width="560" height="315" src="https://www.youtube.com/embed/XeKu8xxr3lY?si=X7cLL73SFwDgjSlo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</div>

Su diseño de niveles, su puzzle jugable, su jugabilidad, y su narrativa basada en repetir el mismo día una y otra vez lo convierten en un juego que no puedo parar de recomendar. El juego el fluido como él solo, todos los escenarios están pensados para ser recorridos de maneras diferentes, con diferentes entradas y salidas, el gunplay (que no gameplay, sino lo de disparar y hacer pium, pium) es muy satisfactorio, y el puzzle que hay que desenmarañar para poder completar el día y que no se repita es tremendamente interesante.
Es muy muy divertido de jugar. 

Puedes encontrarlo en [Humble Bundle](https://www.humblebundle.com/store/deathloop?partner=jugandoenlinux) de oferta (al momento de escribir el artículo)


## Cocoon
Normalmente no compro un juego el mismo año en el que sale, tengo tantos pendientes para jugar que suelo esperarme a que bajen de precio. Curiosamente, y aún siendo un juego salido este 2023, ha habido alguna oferta con él, y como lo único que he leído o visto de este juego es todo positivo, me hice con él.
<div class="resp-iframe">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/ybLUzOvdgDo?si=1a2W6oCLnONXXqXj" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</div>

Los controles: stick izquierdo para mover al personaje por el escenario, y un botón (el que en los mandos de PC suele ser el A, o en los de Sony, la X). No hay más, nos movemos, y cuando queramos interactuar con algo, pulsamos un botón. Como mucho, en algún caso, el botón hay que mantenerlo pulsado un par de segundos, pero es siempre un botón, el mismo botón.

Y con una configuración de controles más sencilla que el primer Super Mario, tenemos un juego de puzzles que es una maravilla en todos los sentidos. Estéticamente a mí me gusta mucho, sin ser un portento gráfico, es muy bonito, el sonido está muy cuidado, y de hecho, es a veces una pieza fundamental del juego, pero sobre todo sus puzzles son una obra de arte. 
Cocoon va enseñándote a solucionar los puzzles poco a poco, no introduce uno nuevo hasta que no te has familiarizado con el anterior, y lo hace de una manera suave, sutil, casi sin que te des cuenta. Es, como he escuchado por ahí, ASMR hecho videjuego. Constantemente te hace tener esos momentos "¡Ajá!", al entender cómo hay que resolver un puzzle.
Una joya, que con una duración moderada, os aportará más de lo que pueda parecer a simple vista.

Si te interesa, puedes hacerte con él en [Humble Bundle](https://www.humblebundle.com/store/cocoon?partner=jugandoenlinux)

# Son Link

Este año ha sido algo movidito, con nuevas versiones de juegos de código abierto, varias de ellas mayores, con grandes novedades y correcciones, como [Warzone 2100 4.4.0]({% link _posts/2023-10-30-warzone-2100-4-4-0.md %}) y [Speed Dreams 2.3.0]({% link _posts/2023-03-16-la-version-2-3-0-de-speed-dreams-lanzada-oficialmente.md %}), así como el lanzamiento de [Godot 4]({% link _posts/2023-03-01-la-espera-ha-terminado-godot-engine-4.md %}), se anuncio la nueva **Steam Deck OLED**, la celebración por el 25 aniversario de **Half-Life**, con una gran actualización de este gran clásico, el nuevo [Counter-Strike 2]({% link _posts/2023-03-22-valve-anuncia-counter-strike-2.md %}), y tantas y tantas cosas que es difícil resumir todo, sin olvidarme tampoco del 30 aniversario de Doom y del polémico cambio en las políticas de precios del motor **Unity**, aunque bueno, eso beneficio a **Godot** ;)

## Marvel Snap

El juego Free to Play de cartas de Marvel es, junto a **Valheim**, uno de los juegos que más he jugado en Steam. En dicho juego podremos hacernos con cartas de los muchos personajes de La Casa de las Ideas, desde los más conocidos como Hulk, Spider-Man, Capitán América o Lobezno, a menos conocidos por el gran público, como La Muerte, Sebastian Shaw o Jeff. Según vayamos ganando partidas y completando misiones diarias y las misiones de cada temporada podremos ir obteniendo nuevas cartas y potenciadores para ellas, llegando a poder tener copias de las cartas que ya tengamos, además de tener varias variantes de ellas, como cabezones, pixelados, etc.

Cada carta tiene un coste, una cantidad de poder y un efecto (aunque hay también cartas sin efectos), si bien estos pueden ser alterados por efectos de otras cartas y de las ubicaciones (estas últimas también puede ser alteradas por algunos efectos), por lo que tendremos que tener todo esto en cuanta para que nuestros mazos sean buenos.

La mecánica es sencilla: Cada partida consta de 6 rondas (aunque puede variar por algún efecto), y en cada una tendremos, como mínimo, el mismo valor de poder, y podremos bajar tantas cartas como podamos usando dicho poder. Una vez acabadas las rondas, el ganador es el que, o bien haya ganado al menos 2 ubicaciones, o en caso de empate (cada jugador ha ganado una y en otra, ambos tienen la misma cantidad de poder), tenga más poder total.

Además de las partidas estándar, podemos jugar partidas amistosas contra amigos, o jugar al modo Conquista.

En resumen, es un buen juego, con partidas rápidas (3/4 minutos) y que, aunque se pueden obtener algunas cartas y otros objetos del juego pagando, se puede jugar perfectamente sin pagar nada. Juego recomendado tanto si te gusta el universo de Marvel y/o juegos similares.

<div class="steam-iframe">
  <iframe src="https://store.steampowered.com/widget/1997040/" frameborder="0" width="646" height="190"></iframe>
</div>

## Warzone 2100

Cada año también me gusta nombrar algún juego de código abierto, y este año le toca a **Warzone 2100**, uno de los grandes veteranos y que sigue muy activo, de hecho este año se publicó la [versión 4.4.0]({% link _posts/2023-10-30-warzone-2100-4-4-0.md %}).

Este juego de estrategia se ambienta en un futuro post-apocalíptico nuclear y en él somos el Comandante de las tropas de **El Proyecto**, que busca artefactos de antes de la guerra que arraso la mayor parte del planeta, y que buscan la manera de reconstruir la civilización. Gracias a dichos artefactos podremos investigar nuevas armas, defensas, entre otros, y así poder avanzar a lo largo de las tres campañas de la que se compone el juego.

Una de las características de este juego es que, gracias a la investigación, podemos acceder a nuevas armas, chasis y tipos de tracción (tanto para moverse por tierra, agua y aire), y cada componente tiene un coste de energía, que es el recurso principal del juego, y una cantidad de vida, por lo que será indispensable valorar cuáles son las mejores opciones, ya que, o podemos tener vehículos fuertes pero a un precio muy alto, o muchos vehículos, pero muy débiles.

Juego altamente recomendado si te gustan los RTS. Lo podéis bajar desde [su web](https://wz2100.net/)