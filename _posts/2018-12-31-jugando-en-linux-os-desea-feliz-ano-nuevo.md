---
author: Jugando en Linux
category: Editorial
date: 2018-12-31 17:05:48
excerpt: "<p>Que el 2019 sea el a\xF1o del videojuego en Linux para todos</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/15812787389d5e520282cfe444c2e037.webp
joomla_id: 944
joomla_url: jugando-en-linux-os-desea-feliz-ano-nuevo
layout: post
title: "\xA1Jugando en Linux os desea Feliz A\xF1o Nuevo!"
---
Que el 2019 sea el año del videojuego en Linux para todos

Desde Jugando en Linux **os deseamos a todos un feliz y próspero año 2019**, y cómo no, muchos juegos en Linux.


Leo, Serjor y Pato

