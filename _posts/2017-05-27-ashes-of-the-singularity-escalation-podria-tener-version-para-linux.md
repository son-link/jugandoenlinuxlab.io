---
author: Serjor
category: Estrategia
date: 2017-05-27 11:25:10
excerpt: <p>Finalmente parece que podremos ver este RTS en nuestra plataforma.</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/08f61c52357dcc6d2503bfea790efe4d.webp
joomla_id: 346
joomla_url: ashes-of-the-singularity-escalation-podria-tener-version-para-linux
layout: post
tags:
- rts
- vulkan
- ashes-of-the-singularity
title: "Ashes Of The Singularity: Escalation podr\xEDa tener versi\xF3n para Linux\
  \ (ACTUALIZADO)"
---
Finalmente parece que podremos ver este RTS en nuestra plataforma.

**ACTUALIZACIÓN 23-11-17:** Navegando por los foros de Steam del juego [Star Control: Origins](http://store.steampowered.com/app/271260/Star_Control_Origins/), que por cierto, **también llegará a Linux**, nos hemos encontrado con [la respuesta de uno de los desarrolladores](http://steamcommunity.com/app/271260/discussions/1/2381701715713320871/#c1480982971183245010) de Stardock Entertaiment donde anuncia que Ashes of Singularity será el primer juego de Linux en usar el [motor gráfico Nitrous](http://oxidegames.com/nitrous/). Concretamente la respuesta en inglés es la siguiente:


 




> 
>  *[Draginol](http://steamcommunity.com/id/draginol)  [desarrollador]  7 NOV a las 23:46*
>  
> *Ashes of the Singularity is going to be our first Linux game that uses Nitrous.*
> 



 


Como veis, parece que finalmente se confirma el secreto a voces, y más teniendo en cuenta el exitazo de peticiones que han recibido (976 en este momento) en [el hilo](http://steamcommunity.com/app/507490/discussions/0/1290691308574348748/) del que hablábamos hace unos meses en esta misma noticia. Esperemos que pronto podamos ver este fantástico juego de estrategia en tiempo real en nuestros PC's.


 




---


**NOTICIA ORIGINAL:** La gente de Stardock Games, editores entre otros del famoso Sins of a Solar Empire, están dispuestos a traer al sistema del pingüino el juego Ashes of the Singularity si hay suficiente interés por parte de la comunidad linuxera.


Y es que según dicen, la versión para Windows bajo Vulkan está casi terminada, con lo que se están planteando el port para Linux, así que han abierto [este hilo](https://steamcommunity.com/app/507490/discussions/0/1290691308574348748/) en los foros de Steam para que todo aquel interesado pase y firme, por lo que si te gustan los juegos de estrategia en tiempo real, pasa y muestra tu interés.


Siendo un port de la propia empresa desarrolladora y haciendo uso de Vulkan podemos pensar que el rendimiento del juego estará dentro de unos límites más que aceptables.


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/m2WT-MBBMq4" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


Y tú, ¿vas a sumarte a la petición del port para Linux? Déjanos un comentario sobre qué te parecen estas propuestas o pásate por nuestros grupos de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD) para compartir tus opiniones.

