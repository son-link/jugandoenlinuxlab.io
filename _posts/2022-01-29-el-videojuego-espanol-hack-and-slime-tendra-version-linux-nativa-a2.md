---
author: leillo1975
category: Plataformas
date: 2022-01-29 13:00:55
excerpt: "<p><span class=\"css-901oao css-16my406 r-poiln3 r-bcqeeo r-qvutc0\">@BULLWAREsocial</span>\
  \ y SpoonBox Studio, los creadores de <span class=\"css-901oao css-16my406 r-poiln3\
  \ r-bcqeeo r-qvutc0\">@HackAndSlime</span> acaban de lanzar una demo p\xFAblica</p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/HackAndSlime/HackSlime01.webp
joomla_id: 1410
joomla_url: el-videojuego-espanol-hack-and-slime-tendra-version-linux-nativa-a2
layout: post
tags:
- indie
- metroidvania
- espanol
- hack-and-slime
- spoonbox
- bullware-soft
title: "El videojuego espa\xF1ol Hack and Slime tendr\xE1 versi\xF3n Linux nativa\
  \ (ACTUALIZADO)"
---
@BULLWAREsocial y SpoonBox Studio, los creadores de @HackAndSlime acaban de lanzar una demo pública


**ACTUALIZACIÓN 29-1-22:** Los desarrolladores del juego acaban de hacer pública en itch.io la [demo del juego](https://bullwaresoft.itch.io/hackandslime) (pre-alpha) para que todos podais disfrutarla. Por supuesto, si veis fallos o quereis proponer alguna cosa, estais invitados a ello, y para esto se han abierto unas [salas de Discord](https://discord.gg/bullwaresoft). Aquí os dejo el tweet que lo anunciaba:



> 
> Happy [#screenshotsaturday](https://twitter.com/hashtag/screenshotsaturday?src=hash&ref_src=twsrc%5Etfw)!  
>   
> Haven't played Hack and Slime yet? Come and play our demo with 6 levels, treasures, statistics, skills, and a Boss!  
>   
> Wishlist <https://t.co/jbUzOjtHTl>  
> Demo <https://t.co/VigpGkyP4C>[#indiedev](https://twitter.com/hashtag/indiedev?src=hash&ref_src=twsrc%5Etfw) [#indiegame](https://twitter.com/hashtag/indiegame?src=hash&ref_src=twsrc%5Etfw) [#pixelart](https://twitter.com/hashtag/pixelart?src=hash&ref_src=twsrc%5Etfw) [#ドット絵](https://twitter.com/hashtag/%E3%83%89%E3%83%83%E3%83%88%E7%B5%B5?src=hash&ref_src=twsrc%5Etfw) [#gamedev](https://twitter.com/hashtag/gamedev?src=hash&ref_src=twsrc%5Etfw) [#Retro](https://twitter.com/hashtag/Retro?src=hash&ref_src=twsrc%5Etfw) [#metroidvania](https://twitter.com/hashtag/metroidvania?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/OijRtvCsjR](https://t.co/OijRtvCsjR)
> 
> 
> — Hack and Slime (@HackAndSlime) [January 29, 2022](https://twitter.com/HackAndSlime/status/1487383925001498624?ref_src=twsrc%5Etfw)



 Si aun no conoceis el juego, leed la noticia original, justo aquí debajo, y echadle un ojo al video que la acompaña, donde estuvimos jugando un buen rato.


 




---


**NOTICIA ORIGINAL (13-1-22):** Pegándole uno de tantos "repasos" a [nuestra cuenta de twitter](https://mobile.twitter.com/jugandoenlinux), de repente encontramos algo que nos llamó mucho la atención por el video que lo acompañaba, y es que como sabeis nos encantan los juegos de estética retro. El tweet en cuestión era este, y la verdad es que lo poco que se ve en él ya apunta maneras:



> 
> Happy [#WishlistWednesday](https://twitter.com/hashtag/WishlistWednesday?src=hash&ref_src=twsrc%5Etfw)!  
> Are you a content creator that enjoys streaming indie games? Come to our discord and ask for a Hack and Slime Key  
>   
> Wishlist-<https://t.co/cT72CJQpO5>  
> Discord-<https://t.co/q9odBjIEtD>[#IndieGameDev](https://twitter.com/hashtag/IndieGameDev?src=hash&ref_src=twsrc%5Etfw) [#indiedev](https://twitter.com/hashtag/indiedev?src=hash&ref_src=twsrc%5Etfw) [#indiegame](https://twitter.com/hashtag/indiegame?src=hash&ref_src=twsrc%5Etfw) [#pixelart](https://twitter.com/hashtag/pixelart?src=hash&ref_src=twsrc%5Etfw) [#ドット絵](https://twitter.com/hashtag/%E3%83%89%E3%83%83%E3%83%88%E7%B5%B5?src=hash&ref_src=twsrc%5Etfw) [#rpg](https://twitter.com/hashtag/rpg?src=hash&ref_src=twsrc%5Etfw) [#metroidvania](https://twitter.com/hashtag/metroidvania?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/skayQREIrv](https://t.co/skayQREIrv)
> 
> 
> — Hack and Slime (@HackAndSlime) [January 12, 2022](https://twitter.com/HackAndSlime/status/1481247792383381504?ref_src=twsrc%5Etfw)



 Enseguida nos pusimos en contacto con sus creadores y rapidamente nos confirmaron que **el juego ya tenía incluso una versión pre-alpha para testeo** que funciona en nuestro sistema. El juego está siendo desarrollado por **dos compañías con base en Castellón de la Plana**, [Bullware Soft](https://bullwaresoft.com/) y [SpoonBox](https://www.spoonboxstudio.com), y para su creación se está usando  [Construct](https://www.construct.net/en) , el **conocido motor de videjuegos indie**. La idea de ambos estudios es lanzar una versión **demo pública en Steam entre Febrero y Marzo**, y a finales del verano una versión en **Acceso Anticipado**. Para ello llevan trabajando desde el mes de Junio del pasado año, y ciertamente el resultado promete.


![Hack and Slime](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/HackAndSlime/HackSlime02.webp)


 En Hack & Slime encontraremos un juego de **acción 2D con aspecto retro** donde encontraremos pinceladas (o más bien brochazos) de otros géneros como el **Rol**, las **plataformas** o el **Metroidvania**. Los personajes que encontraremos están realizados con la técnica del **pixelart**, y las mecánicas de juego son de sobra conocidas y ya las hemos visto en otros juegos del género. Esta es la descripción oficial que podemos encontrar en Steam:


*¿Qué podría dar más miedo que una inmensa torre-mazmorra infestada de calaveras, ogros y dragones? Que los monstruos monten la multinacional más grande del mundo con ella. Ya sabes, los negocios son los negocios.*


*Ábrete paso a fuego y espada por más de 25 niveles de pura acción en un Hack and Slash tan épico como tronchante: Sáltate todas las secuencias y diálogos ¿a quién le importan? farmea decenas de horas hasta que los enemigos no sean más que sacos de boxeo, supera las 200 horas de juego en un indie sin presupuesto y luego llora en los foros sobre lo corto que es. ¿Quieres que te convenza un poco más? Sin problemas:*


*- ¿2D de scroll lateral? Check it.  
- ¿Pixelart? Check it.  
- ¿Screen Shake? Check it.  
- ¿Bloom y aberración cromática? Check it.  
- ¿Atracción de monedas? Check it.  
- ¿Llamarlo Roguelike en internet? Check it.  
- ¿Habilidades absurdamente chetas? Check it.  
- ¿Ponernos a caer en un burro en Metacritics? Check it.*


*No esperes más y embárcate en la aventura de derrotar a “Warpmazon” (no nos denuncien, por favor) y de paso, si ganas suficiente dinero... salvar el mundo.*


Desde aquí os prometemos que os traeremos las noticias que vaya generando este proyecto lo más puntualmente posible. También nos gustaría comentaros que **estamos involucrados en el testeo de la versión de Linux**. Os dejamos este **stream que hemos emitido hace unos días probando una versión pre-alpha** para que podais ir echándole un ojo:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/AQ7QySR7oKU" title="YouTube video player" width="780"></iframe></div>


 


  ¿Qué os parece este Hack and Slime? ¿Os gustan este tipo de juegos de aspecto retro? Cuentanoslo en nuestro grupo de JugandoEnLinux en [Telegram](https://t.me/jugandoenlinux), o nuestras salas de [Matrix](https://matrix.to/#/#jugandoenlinux.com:matrix.org). Os dejamos el widget de Steam por si quereis segurilo e ir añadiéndolo a vuestra lista de deseados:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/1773700/" width="646"></iframe></div>


 

