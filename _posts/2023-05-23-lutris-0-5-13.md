---
author: Son Link
category: Software
date: 2023-05-23 20:13:48
excerpt: "<p>Nueva versi\xF3n de este popular herramienta para gestionar nuestra biblioteca\
  \ de juegos, con nuevas integraciones</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Lutris/lutris-0.5.13.webp
joomla_id: 1537
joomla_url: lutris-0-5-13
layout: post
tags:
- lutris
- proton
- itch-io
- battle-net
title: Lutris 0.5.13
---
Nueva versión de este popular herramienta para gestionar nuestra biblioteca de juegos, con nuevas integraciones


Hace pocos días ha sido lanzada una nueva versión de **Lutris**, la **0.5.13** que trae muchas novedades, entre ellas la integración de 2 tiendas y una popular web de mods de juegos:


* **Itch.io**: La popular tienda de juegos indie, de aficionados y pequeños estudios. Por el momento no se puede buscar ni descargar juegos gratuitos.
* **Battle.net**: Una vieja conocida, la tienda y plataforma de juego online de **Blizzard**, desarrolladora de Diablo, World of Warcraft y Overwatch entre otros. Requiere tener instalado el paquete **protobuf** y la propia tienda)
* **MODDB**: Una página con una gran cantidad de mods para muchos juegos. Ahora Lutris soporta enlaces en los instaladores para facilitar la tarea de su instalación. Es necesario el modulo de Python **moddb**


Y las novedades son:


* Añadido soporte para **Proton**
* Añadida la posibilidad de arrastrar y soltar archivos en la ventana principal. Los archivos soltados se compararán con sumas de verificación No-Intro, Redump y TOSEC.
* Añadida la opción "Missing" en la barra lateral para los juegos cuyo directorio no fue encontrado.
* Rediseño de las ventanas de configuración, preferencias, instalador y añadir juegos.
* Agrupación de las opciones de configuración en secciones
* Se ha añadido una casilla para dejar de pedir la configuración de lanzamiento de un juego.
* Se ha añadido una casilla para ordenar primero los juegos instalados.
* Soporte para configuraciones de lanzamiento en accesos directos y la línea de comandos
* Ahora muestra insignias de la plataforma en banners y carátulas.
* La instalación de juegos desde archivos de instalación ahora puede utilizar diferentes preajustes (Win98, 3DFX, ...)
* Añadido campo de filtro a la lista de ejecutores
* Mostrar el recuento de juegos en la barra de búsqueda
* Solucionar los problemas de autenticación de **Humble Bundle** permitiendo importar cookies desde **Firefox**
* Mejorada la detección de juegos **DOSBox** (emulador de **MS-DOS**) en **GOG**
* Añadida la opción "Unspecified" en Vulkan ICD
* Eliminado **ResidualVM** (ahora fusionado en **ScummVM**)
* Detectar los controladores **Vulkan** obsoletos y por defecto a **DXVK 1.x** para ello
* Mejorado el soporte High-DPI para medios personalizados
* Mejoras de rendimiento


La verdad es que las novedades son la mar de interesantes y ya son 9 tiendas online soportadas. Por pedir, quizás que en próximas versiones añadan soporte a Gamejolt, la cual es similar a Itch.io


Agradecimientos a Leillo por la imagen usada

