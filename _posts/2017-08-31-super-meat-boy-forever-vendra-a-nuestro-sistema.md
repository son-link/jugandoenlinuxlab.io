---
author: leillo1975
category: Arcade
date: 2017-08-31 07:50:48
excerpt: "<p>Team meat ha confirmado la llegada de este nuevo t\xEDtulo.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/2d6c15d654bd7f03d72e8900a9ebd07f.webp
joomla_id: 447
joomla_url: super-meat-boy-forever-vendra-a-nuestro-sistema
layout: post
tags:
- super-meat-boy
- forever
- team-meat
title: "Super Meat Boy Forever vendr\xE1 a nuestro sistema."
---
Team meat ha confirmado la llegada de este nuevo título.

A veces una idea muy sencilla basta, y aplicando esta norma nos encontramos con [Super Meat Boy](http://store.steampowered.com/app/40800/Super_Meat_Boy/) allá a finales de 2010. Ya han pasado unos cuantos años, y este juego indie ha conseguido un buen puñado de reviews muy positivas y las alabanzas de la prensa especializada. Ha sido llevado a multitud de plataformas y en todas a conseguido un considerable éxito, incluida por supuesto la nuestra.


 


El caso es que ayer salto la noticia en [GamingOnLinux](https://www.gamingonlinux.com/articles/need-more-meat-super-meat-boy-forever-announced-will-have-a-linux-version.10252) de que la segunda parte saldría también en Linux, con el consiguiente revuelo. El anuncio por parte del Team Meat venía en respuesta al [tweet de un usuario](https://twitter.com/Tuxbot123/status/902957954533601282) y podeis verla aquí:


 



> 
> Linux yes, but after PC is out. No plans for Vita at this time. <https://t.co/J3r2weqkxR>
> 
> 
> — Team Meat (@SuperMeatBoy) [30 de agosto de 2017](https://twitter.com/SuperMeatBoy/status/902958390955057152)



 


Como veis el juego no llegará en el lanzamiento en "PC" (Windows) sinó más tarde, pero almenos podremos disfrutar de él... esperemos que no sea mucho tiempo después. Para aquellos que no conozcais **Super Meat Boy**, se trata  de un plataformas en el que deberemos ir sorteando obstaculos en diferentes pantallas con nuestro sangriento personaje hasta llegar a una meta determinada. Como dije antes, una idea muy sencilla, pero que proporciona una jugabilidad y adicción tremendas. Desde JugandoEnLinux.com estamos deseando echarle el guante y ofreceros más contenido de este pequeño-gran juego. Os dejo con el video del anuncio:


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/8DLPiGJDSQs" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


¿Has jugado a Super Meat Boy? ¿Qué te parece que haya una segunda parte? Deja tus impresiones en los comentarios o en nuestro canal de [Telegram](https://t.me/jugandoenlinux).


 

