---
author: leillo1975
category: "Acci\xF3n"
date: 2017-05-18 07:13:57
excerpt: <p>Realizamos algunas pruebas para ver las diferencias entre Vulkan y OpenGL</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/7af0193b8351bd1eb04c09dfe5731fa3.webp
joomla_id: 329
joomla_url: incluido-modo-becnhmark-en-serious-sam-fusion-2017
layout: post
tags:
- vulkan
- opengl
- croteam
- serious-sam-fusion-2017
- benchmarks
title: Incluido modo Becnhmark en Serious Sam Fusion 2017
---
Realizamos algunas pruebas para ver las diferencias entre Vulkan y OpenGL

Todos sabemos que ultimamamente [el estudio croata Croteam](index.php/component/search/?searchword=Croteam&searchphrase=all&Itemid=445) esta que se sale. La verdad es que este estudio lo está dando todo en darnos soporte para GNU-Linux/SteamOS en sus juegos, y buena prueba de ello son la inclusión paulatina de sus juegos para nuestro sistema en su plataforma Fusion 2017. En este momento podemos disfrutar, a parte de Serious Sam 3 que tiene soporte desde hace tiempo, del [primer episodio](index.php/homepage/generos/accion/item/380-serious-fusion-2017-se-aproxima), así como del [segundo](index.php/homepage/generos/accion/item/401-croteam-lanza-serious-sam-vr-the-second-encounter), ambos en su versión HD.


 


![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SSVR2E.webp)


 


En el día de ayer, el lanzador **[se actualizaba incluyendo entre otras muchas cosas un modo Benchmark](http://steamcommunity.com/games/564310/announcements/detail/1273802170604814296)** que nos permitirá realizar tests de rendimiento de forma sencilla. Podemos encontrar este nuevo modo entrando en la **sección Extras del menú principal**, donde podremos elegir la duración de este test, así como el mapa donde queremos hacerlo. Hay que recalcar una cosa, y es que estos test no son todos iguales entre si, por lo que **las comparativas realizadas pueden no ser exactas**, aunque si bastante clarificadoras. Se tratan de gameplays realizados por lo que parece ser un bot.


 


Nosotros en JugandoEnLinux.com hemos realizado unas cuantas pruebas para que veais los diferentes rendimientos que hemos encontrado usando Vulkan (Beta) y OpenGL. Para ello  hemos usado un sistema **Ubuntu 17.04 64Bits** en un **Intel i5 3550** (4x 3.3Ghz) con **16GB DDR 1600 Mhz**, y una Nvidia Geforce **GTX 1050Ti-4GB** (Driver 381.22). También hemos tomado como referencia los siguientes ajustes: **CPU Speed:** High, **GPU Speed:** High, **GPU Memory:** High, **Level Caching:** Medium, **Resolución** 1920x1080


 




|  |  |  |  |
| --- | --- | --- | --- |
| **Mapa** | **FPS Vulkan** | **FPS OpenGL** | **% Diferencia** |
| **Hatshepsut (SS1HD)** | 118.7 | 112.1 | 5.8% |
| **Karnak Temple (SS1HD)** | 111.2 | 103.6 | 7.3% |
| **Thebes-Luxor (SS1HD)** | 139.6 | 125.3 | 11.4% |
| **Sierra de Chiapas (SS2HD)** | 103.0 | 101.0 | 1.9% |
| **Ziggurat (SS2HD)** | 131.4 | 123.9 | 6% |
| **Tower of Babel (SS2HD)** | 137.5 | 137.6 | 0% |


 


Como veis, las diferencias no son muy notables, viendose eso si, una **cierta ventaja en usar la API de Vulkan**. Haciendo una media de los porcentajes de diferencia entre Vulkan y OpenGL podemos decir que **usar la primera opción nos va a reportar una ventaja del 5.4%** con respecto a la segunda. Por supuesto, y como comentaba antes, estamos ante una versión Beta de la implementación de Vulkan y puede haber diferencias en los tests debido a que no son exactamente iguales; pero esto nos puede servir para hacernos una idea del rendimiento actual entre una y otra API.


 


Si quereis haceros con Serious Sam Fusion 2017 (lanzador + The First Encounter HD) podeis hacerlo en Steam:


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/564310/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


¿Habeis hecho vuestros propios tests? ¿Cuales son vuestras impresiones sobre ellos? Cuentanos esto o cualquier cosa relacionada sobre Croteam y Serious Sam en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

