---
author: leillo1975
category: Ofertas
date: 2018-12-20 17:04:05
excerpt: <p>....id preparando las carteras</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/19ace523c3fcf08ab0aa2a0aa1b550ec.webp
joomla_id: 933
joomla_url: llegan-al-fin-las-rebajas-de-invierno-de-steam
layout: post
tags:
- steam
- rebajas
- valve
- ofertas
title: Llegan al fin las rebajas de Invierno de Steam.
---
....id preparando las carteras

Trás pasar hace bien poco por los descuentos de [Halloween](index.php/homepage/noticias/39-noticia/1007-las-rebajas-de-halloween-de-steam-ya-estan-aqui) y [Otoño](index.php/homepage/ofertas/37-ofertas/1029-llegan-los-descuentos-de-otono-de-steam-hasta-el-27-de-noviembre), los chicos de Valve no quieren darnos tregua y ya están de nuevo con sus irresistibles y esperadas ofertas. Una vez más, como suele ser habitual en estas fechas navideñas la conocida tienda digital nos trae montones de descuentos en multitud de videojuegos, por lo que es un buen momento para regalar o regalarnos esos títulos que tenemos en mira desde hace algún tiempo. El inicio de las rebajas se anunciaba con el siguiente tweet:


 



> 
> The Steam Winter Sale also marks the start of voting for The Steam Awards!  
>   
> This year, you can vote on all eight categories throughout the sale. Winners will be revealed in February 2019.  
>   
> ? Steam Awards 2018: <https://t.co/Pc8B0e3Uhw> [pic.twitter.com/fLiQDXhIGi](https://t.co/fLiQDXhIGi)
> 
> 
> — Steam (@steam_games) [December 20, 2018](https://twitter.com/steam_games/status/1075814692038164481?ref_src=twsrc%5Etfw)


  





Desde ahora mismo y hasta el próximo 3 de enero podéis realizar las compras de vuestros videojuegos favoritos. Nosotros en JugandoEnLinux.com hemos hecho una pequeña selección de títulos interesantes como los siguientes:


 [XCOM 2 al 75% de descuento](https://store.steampowered.com/app/268500/XCOM_2/)


[Door Kickers al 85% de descuento](https://steamcommunity.com/app/248610/)


[Total War: WARHAMMER II al 50% de descuento](https://store.steampowered.com/app/594570/)


[Rise of the Tomb Raider al 80% de descuento](https://store.steampowered.com/app/391220/Rise_of_the_Tomb_Raider/)


[Neverwinter Nights Enhaced edition al 70% de descuento](https://store.steampowered.com/app/704450/)


[Everspace al 75% de descuento](https://store.steampowered.com/app/396750/)


[American Truck Simulator al 75% de descuento](https://store.steampowered.com/app/270880)


[Tooth and Tail al 75% de descuento](https://store.steampowered.com/app/286000)


[Candle al 67% de descuento](https://store.steampowered.com/app/420060)


[Steam Controller al 50% de descuento](https://store.steampowered.com/app/353370/Steam_Controller/?snr=1_41_4__42)


 


Además, los descuentos vendrán acompañados por la **votación final** de los **Steam Awards**, cuyo resultado final conoceremos en el próximo mes de Febrero.


**Premio «Juego del año»**


* PLAYERUNKNOWN’S BATTLEGROUNDS
* MONSTER HUNTER: WORLD
* Kingdom Come: Deliverance
* HITMAN 2
* Assassin’s Creed Odyssey


**Premio «Juego de RV del año»**


* The Elder Scrolls V: Skyrim VR
* VRChat
* Beat Saber
* Fallout 4 VR
* SUPERHOT VR


**Premio «Con amor y dedicación»**


* Dota 2
* Grand Theft Auto V
* No Man’s Sky
* Path of Exile
* Stardew Valley


**Premio «Mejor entorno»**


* The Witcher 3: Wild Hunt
* Subnautica
* Shadow of the Tomb Raider
* Far Cry 5
* DARK SOULS III


**Premio «Mejor con amigos»**


* Counter-Strike: Global Offensive
* Tom Clancy’s Rainbow Six Siege
* PAYDAY 2
* Dead by Daylight
* Overcooked! 2


**Premio «Mejor historia alternativa»**


* Wolfeinstein II: The New Colossus
* Assassin’s Creed Odyssey
* Hearts of Iron IV
* Sid Meier’s Civilization VI
* Fallout 4


**Premio «Más divertido con una máquina»**


* Euro Truck Simulator 2
* Rocket League
* NieR: Automata
* Factorio
* Space Engineers


**Premio «Mejor desarrollador»**


* CD PROJEKT RED
* Ubisoft
* Bethesda
* Rockstar Games
* Digital Extremes Ltd.
* Square Enix
* Capcom
* Paradox Interactive
* BANDAI NAMCO Entertainment
* Klei


Por otra parte, y a diferencia de otras rebajas, este año Steam nos trae la "[casa acogedora](https://store.steampowered.com/promotion/cottage_2018/)", un minijuego donde cada cierto tiempo podemos abrir una "ventana" que nos irá obsequiando con regalos como objetos de juegos, artículos de la comunidad o "chismes" que luego se irán utilizando para hacer la insignia correspondiente.


 Para ver todas las ofertas y opciones, y descubrir los chismes de la casa acogedora de Steam puedes visitar: <https://store.steampowered.com/>


Y tu, ¿ya tienes en mente alguna compra? No te cortes y dínosla en los comentarios o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

