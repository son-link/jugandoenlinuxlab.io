---
author: Jugando en Linux
category: Editorial
date: 2022-01-21 17:55:27
excerpt: "Breve repaso sobre de d\xF3nde venimos, y un replanteo de nuestro futuro virtual"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/headers/Proyectologo.webp
joomla_id: 1412
joomla_url: presente-y-futuro-del-juego-en-linux-y-como-nos-vamos-a-adaptar-en-jugandoenlinux-com
layout: post
tags:
- editorial
title: Presente y futuro del juego en Linux, y como nos vamos a adaptar en jugandoenlinux.com
---
Breve repaso sobre de dónde venimos, y un replanteo de nuestro futuro virtual


Hace ya algo más de 5 años que publicamos [nuestro primer artículo]({{ "/posts/creacion-jugandoenlinux-fundamentos" | absolute_url }}) en jugandoenlinux.com. Un editorial que llevaba por subtítulo una frase emblemática de cierta película que rezaba así:

> 
> "El mundo ha cambiado. Lo siento en el agua. Lo siento en la tierra. Lo huelo en el aire"
> 

Y lo cierto es que el mundo del videojuego en Linux no ha parado de cambiar desde que en 2013 Steam apareciese en nuestro sistema favorito. Y al igual que entonces, nuestra declaración de intenciones sigue intacta con el propósito de seguir informando y promoviendo el mundo del videojuego en Linux, pero con todos los cambios que han sucedido y los que seguro están próximos a suceder quizás ha llegado la hora de actualizarnos, de replantear nuestras posiciones y dar un nuevo aire a todo lo que concierne a jugandoenlinux.com.


Como sabéis, desde el principio planteamos jugandoenlinux.com como una web y proyecto colaborativo y abierto, donde todos los usuarios tuviesen cabida y donde pudiese convivir tanto la información de los videojuegos libres, como información sobre videojuegos privativos, hardware de toda índole que corriese bajo GNU-Linux, y mucho más sin hacer distinción entre unos y otros. Además, [en más de una ocasión]({{ "/posts/jugandoenlinux-com-es-para-todos-los-jugadores-bajo-gnu-linux" | absolute_url }}) hemos defendido la inclusión de todos aquellos que incluso prefieren jugar con capas de compatibilidad como Wine y Proton, o con plataformas de videojuegos en streaming como por ejemplo los que utilizan Stadia mediante un navegador de base Cromium. Consideramos que todos esos jugadores entran dentro de la familia de jugandoenlinux, y como tales siempre se les ha dado y se les dará la bienvenida, y aportaremos información sobre todos esos modos de distribución de videojuegos bajo nuestro sistema favorito.


La única salvedad que siempre hemos hecho, han sido con la promoción de videojuegos sin soporte nativo en nuestra web y los canales oficiales, pues siempre hemos considerado que un videojuego que no ofrece un soporte oficial bajo Linux, aunque pueda funcionar mediante capas de compatibilidad y ser contado por Steam como un juego jugado en Linux siempre tendrá un riesgo asociado a que cualquier cambio que se produzca en cualquier actualización de este pueda derivar en que termine no siendo jugable en nuestro sistema, además del perjuicio asociado a que los programadores y editores se lavasen las manos a la hora de corregir problemas para que sus videojuegos pudieran correr bajo dichas capas de compatibilidad. 


Esto ha sido así hasta ahora, y en parte seguirá siendo así, salvo con una excepción. 


Creemos que a nadie que esté al tanto de lo que sucede en nuestro mundillo se le puede escapar que el videojuego en Linux está a punto de sufrir una nueva vuelta de tuerca. La aparición de Proton supuso ya de por si, un incremento exponencial en cuanto a la exposición de Linux como sistema de juego, lo que con el paso de los años ha derivado en que miles y miles de juegos ahora son jugables mediante dicha capa de compatibilidad. Esto, y la promoción que Valve ha hecho de Proton con el objetivo de apoyar a su inminente Steam deck ha tenido como consecuencia que multitud de estudios de todo tipo y editoras grandes y pequeñas se hayan fijado en Proton, y en el hecho de que sus juegos se ejecuten sin problemas para no quedarse fuera del futuro pastel que se supone va a tener la máquina de Valve. De este modo, **ya hay editoras y estudios comenzando a soportar de forma oficial a Proton**, y por tanto a Linux. No solo eso, si no que como ya sabemos, **Valve se encuentra [verificando ella misma]({{ "/posts/valve-lanza-un-programa-de-verificacion-de-juegos-para-steam-deck" | absolute_url }}) la práctica totalidad de su biblioteca de juegos para asignarle un soporte propio** de manera "oficial" en base a diferentes pruebas y parámetros, e incluso planteando su apoyo a los videojuegos que corran sin problemas bajo Proton **asignándoles una etiqueta de verificación oficial para Steam Deck**. Y **no hay que olvidar que SteamOS 3.0 es una distribución GNU-Linux.**


Todo esto, con la promesa por parte de Valve de que cualquier juego que no se ejecute corréctamente bajo SteamOS 3.0/Proton será considerado como un bug y tratarán de solucionarlo, nos hace replantearnos nuestra posición. A partir de ahora, **consideraremos la promoción en Jugandoenlinux de cualquier juego que pueda ejecutarse con soporte oficial por parte de editoras/desarrolladoras bajo Wine/Proton**, ya sea en Steam, o en cualquier otra plataforma, incluidos los que la propia Valve considere aptos mediante su programa de verificación "Steam Deck Verified", y ello será aceptado tanto en la web como en los canales oficiales de jugandoenlinux.com.


Sabemos que esto puede no agradar a alguno de vosotros, pero como ya hemos dicho, consideramos que debemos integrar a todo el juego en Linux, y **si a partir de ahora existe un soporte oficial para videojuegos mediante capas de compatibilidad, no seremos nosotros los que dejemos de apoyar el juego bajo esa modalidad**. No podemos dejar fuera a los juegos que se ejecutan oficialmente en Linux. Nunca lo hemos hecho, y menos ahora.


Para terminar, esta misma noche vamos a llevar a cabo un directo que podréis seguir en nuestros canales oficiales donde hablaremos largo y tendido de todos estos temas, y posíblemente mucho más de lo que está por venir.


Bienvenidos al nuevo jugandoenlinux.com
