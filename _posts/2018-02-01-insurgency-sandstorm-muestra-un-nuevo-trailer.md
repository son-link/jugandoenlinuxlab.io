---
author: leillo1975
category: "Acci\xF3n"
date: 2018-02-01 10:09:05
excerpt: "<p>New World Interactive nos deja ver un poco m\xE1s del juego</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/1450fe47fc6d3f93ff072c134eaeda35.webp
joomla_id: 629
joomla_url: insurgency-sandstorm-muestra-un-nuevo-trailer
layout: post
tags:
- cooperativo
- insurgency
- unreal-engine-4
- motor-source
- sandstorm
- focus-interactive
- new-world-interactive
title: 'Insurgency: Sandstorm muestra un nuevo trailer.'
---
New World Interactive nos deja ver un poco más del juego

El pasado año, a punto de llegar el verano [os hablábamos](index.php/homepage/generos/accion/item/502-el-modo-campana-de-insurgency-insurgency-sandstorm-tiene-planeada-una-version-para-linux) de **Insurgency: Sandstorm**, y desde entonces poco más hemos sabido de su desarrollo. Hace un par de días se publicaba un **nuevo video** donde se muestra un poco más de este título que nos permitirá jugar una campaña e incluso compartir con misiones contra la computadora junto con compañeros. El [trailer](http://newworldinteractive.com/insurgency-sandstorm-teaser-trailer-released/) en cuestión es este:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/avJ50YRup0o" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


 Como veis la acción transcurre en el mundo que anteriormente se nos presentaba en el juego original, pero con un toque **muy cinemátográfico** y en la linea de otros juegos como Call of Duty o Battlefield. El juego hará uso de **Unreal Engine 4**, cambiando el ya tan trillado pero eficiente [Motor Source](https://es.wikipedia.org/wiki/Source) que luce [la versión actual](http://store.steampowered.com/app/222880/Insurgency/) del juego, que actualmente está unicamente enfocada al multijugador.  Pero no todo va a ser campaña sinó que también podremos, por ejemplo participar en batallas de gran escala con **escaramuzas de 16vs16 jugadores**, pudiendo hacer uso de artillería, drones, helicopteros, coches y **todo tipo de vehículos** durante la contienda. El juego, como su predecesor hará incapié en la comunicación, la consecución de objetivos y el trabajo en equipo, en detrimento del lucimiento personal. Habrá **Matchmaking** y la posibilidad de cooperar contra equipos de bots.


El juego, desarrollado por [New World Interactive,](http://newworldinteractive.com) estará editado por [Focus Home Interactive](http://www.focus-home.com/), y llegará probablemente en la segunda mitad de este año 2018. Como se ha comentado por sus desarrolladores desembarcará primero en Windows, PS4 y XBOX-One, y **posteriormente a GNU-Linux** y Mac. Desde JugandoEnLinux.com prometemos "marcarles de cerca" para ofreceros toda cuanta información encontremos sobre este juego.


 


¿Qué te parece la propuesta de New World Interactive? ¿Te gustan las campañas de un jugador en los shooters? Déjanos tus impresiones en los comentarios o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

