---
author: leillo1975
category: "Simulaci\xF3n"
date: 2021-03-12 17:02:25
excerpt: "<p>@scssoftware acaba de anunciar su pr\xF3xima expansi\xF3n para su juego.</p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ATS/Texas/TExasAnuncio900.webp
joomla_id: 1286
joomla_url: american-truck-simulator-llegara-a-texas-en-su-proxima-dlc
layout: post
tags:
- dlc
- american-truck-simulator
- ats
- scs-software
- simulacion
- texas
title: "American Truck Simulator llegar\xE1 a Texas en su pr\xF3xima DLC "
---
@scssoftware acaba de anunciar su próxima expansión para su juego.


 Cuando aun estamos recuperándonos tras el anuncio de "[Heart of Russia]({{ "/posts/heart-of-russia-sera-la-nueva-dlc-de-euro-truck-simulator-2" | absolute_url }})" para Euro Truck Simulator 2, y la **llegada la esperada DLC "[Iberia]({{ "/posts/la-proxima-expansion-de-euro-truck-simulator-2-sera-iberia" | absolute_url }})" el próximo mes de Abril,** nos acabamos de enterar gracias a las redes sociales que **American Truck Simulator volverá al sur,** concretamente al estado de [**Texas**](https://blog.scssoft.com/2021/03/introducing-texas.html). Podeis ver el **tweet** del anuncio justo aquí debajo:

> 
> Howdy, partners! We've got some very exciting news to share with ya'll 🤠  
>   
> It is our pleasure, to officially announce the development of Texas for American Truck Simulator! 🇺🇸  
>   
> Take a stop by our blog for exclusive images: <https://t.co/z1u17OGSfb> [pic.twitter.com/ps9CuzLmIW](https://t.co/ps9CuzLmIW)
> 
> 
> — SCS Software (@SCSsoftware) [March 12, 2021](https://twitter.com/SCSsoftware/status/1370410054864805893?ref_src=twsrc%5Etfw)


El estado de la estrella solitaria, como se le suele llamar a Texas, **es el segundo de más extensión y población de EEUU**, y se caracteríza por ser diverso en muchos aspectos, conjugando paisajes de costa, con montañas y también zonas áridas. Se trata también de un **estado economicamente fuerte, lleno de industrias muy diversas** como la ganadería, la energetica o la aeronautica, y donde podremos encontrar montones de trabajos que realizar en nuestros camiones.


![Texas](https://1.bp.blogspot.com/-DjfjPZXqM_g/YEuATSHM01I/AAAAAAAAYSg/MeuZ3O5KgMApm2ChOu-etsNA89cv4aqBgCLcBGAsYHQ/s1920/02.webp)


Como es lógico, **el estado del desarrollo de esta expansión aun es muy temprano**, esperándose antes la llegada de [Wyoming](https://store.steampowered.com/app/1415692/American_Truck_Simulator__Wyoming/), del que aun no tenemos fecha tampoco. En ambos casos, en JugandoEnLinux.com, permaneceremos atentos a todas las noticias importantes que generen los trabajos de [SCSsoftware]({{ "/tags/scs-software" | absolute_url }}), y por supuesto sus fechas de lanzamiento. Por el momento podeis añadir el estado de Texas a vuestra lista de deseados:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/1465750/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


Puedes dejarnos tus preguntas o impresiones en los comentarios sobre la DLC de Texas en nuestros grupos de JugandoEnLinux en [Telegram](https://twitter.com/JugandoenLinux) o  [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

