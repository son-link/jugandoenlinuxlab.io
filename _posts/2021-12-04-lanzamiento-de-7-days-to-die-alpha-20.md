---
author: Laegnur
category: Terror
date: 2021-12-04 15:52:22
excerpt: "<p>Hoy os traemos la nueva Alpha de <strong>7 Days to Die</strong>, el juego\
  \ de supervivencia zombi por excelencia y que podemos jugar de forma nativa en GNU/Linux.</p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/7daystodie/header_image_steam_1920x622.webp
joomla_id: 1392
joomla_url: lanzamiento-de-7-days-to-die-alpha-20
layout: post
tags:
- zombies
- rpg
- survival-horror
- 7-days-to-die
title: 'Lanzamiento de 7 Days to Die: Alpha 20'
---
Hoy os traemos la nueva Alpha de **7 Days to Die**, el juego de supervivencia zombi por excelencia y que podemos jugar de forma nativa en GNU/Linux.


¡Salve jugadores!


Para celebrar el lanzamiento de la nueva **Alpha 20**, la gente de **Fun Pimps**, abrió la alpha durante este fin de semana, de manera exclusiva para una selección de streamers que habitualmente juegan a este juego, mientras que el resto de los jugadores podemos empezar a disfrutarlo a partir del 6 de Diciembre de 2021.


Vamos a ver que novedades nos trae el **A20**, aunque ya os adelante que el rendimiento del juego sigue siendo "no óptimo".


Las principales novedades de este alpha son:


* Se mejora el sistema de generación aleatoria del mundo, destacando que ahora las ciudades  estarán compuestas de diferentes distritos diferenciados por su arquitectura.
* Se añaden más de 175 nuevos puntos de interés explorables y se actualizan más de 25 puntos de interés antiguos, haciendo un total de 550 ubicaciones explorables.
* Se actualizan 24 modelados de los zombis y personajes con los que nos podremos encontrar.
* Se añaden 6 nuevas armas y se modifican 13 de las ya existentes, destacando las nuevas armas de tubería al mas puro estilo Fallout 4.
* En la construcción se añaden cientos de nuevas formas nuevas a los bloques de construcción y se realizan mejoras en la colocación de los mismos. Añadiendo ayudas visuales para la estabilidad de los bloques.
* Se añade una nueva torreta robótica, esta vez en forma de compañero drone que mediante modificaciones servirá de mula de carga, o para aplicar curaciones al jugador, entre otras funciones.
* Se mejoran los vehículos añadiendo soporte para pasajeros en modo cooperativo y modificaciones para mejorar y personalizar sus vehículos
* Se ha integrado un nuevo sistema de desmembramiento con algunos de los zombis.


Los nuevos modelos HD de los zombis incluyen:


* **Matón zombi**
* **Tom Clarke Zombi**
* **Zombi trabajador**
* **Zombi mecánico**
* **Hazmat Zombie**
* **Perro zombi**
* **Zombi motero**
* **Araña zombi**
* **Soldado zombi**
* **Científico zombi**
* **Policía zombi**
* **Zombi turista**
* **Enfermera zombi**
* **Zombi leñador**
* **Hombre de negocios zombi**
* **Zombi mutante**
* **Zombi chillón**
* **Stripper Zombie**
* **Zombi quemado**


Entre los comerciantes se actualizan:


* **Comerciante Joel**
* **Comerciante Rekt**
* **Comerciante Hugh**
* **Comerciante Bob**


Ademas de todas estas novedades, han introducido una serie de caracteristicas de cara a los streamer en forma de integraciones con Twitch, que podemos ver en este video oficial:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/PfcXKBNSSb8" title="YouTube video player" width="780"></iframe></div>


 


Lamentablemente, a la hora del rendimiento, el juego sigue sin ser estable, notándose una caída grave de frames al entrar en zonas con exceso de mallas, como pueden ser las ciudades.


Para ver el listado completo de modificaciones podéis visitar las [notas de lanzamiento de 7DayaToDie Alpa 20](https://7daystodie.com/a20-official-release-notes/) en la pagina oficial del juego.


¿Habéis probado este juego? ¿Vais a probar esta nueva Alpha? ¿Que os parecen todas estas novedades y cambios? Contadnos lo que opináis en los comentarios o en nuestras redes sociales en [Telegram](https://t.me/jugandoenlinux) y [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).


¡Me despido hasta la próxima!


 

