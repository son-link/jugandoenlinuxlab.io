---
author: leillo1975
category: Realidad Virtual
date: 2020-01-22 10:43:42
excerpt: "<p>Los conocidisimos juegos de @ValveSoftware ya est\xE1n disponibles en\
  \ Steam.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/HalfLife/half-life-free.webp
joomla_id: 1151
joomla_url: juega-gratis-a-la-saga-half-life-hasta-la-llegada-de-hl-alyx
layout: post
tags:
- steamos
- realidad-virtual
- valve
- half-life-2
- valve-index
- alyx
title: Juega gratis a la saga Half-Life hasta la llegada de HL:Alyx
---
Los conocidisimos juegos de @ValveSoftware ya están disponibles en Steam.


Han corrido rios de tinta desde que anunciamos [la NO llegada de Half-Life: Alyx](index.php/homepage/realidad-virtual/1134-half-life-alyx-tendra-version-para-linux) a GNU/Linux. Como mucho de vosotros sabreis el juego a generado una expectación sin precedentes que ha hecho que Valve haya agotado sus existencias de sus gafas de Realidad Virtual, las [Valve Index](index.php/component/k2/15-hardware/1157-las-gafas-de-realidad-virtual-valve-index-aparecen-en-la-seccion-hardware-de-steam), a pesar de su elevado precio. Para celebrarlo, la compañía [permitirá jugar gratuitamente](https://steamcommunity.com/games/546560/announcements/detail/3094470492881722385) a los títulos más emblemáticos de esta saga hasta el día del lanzamiento oficial de Half-Life Alyx. Concretamente los juegos que se podrán disfrutar serán los siguientes:  
  
[Half-Life](steam://run/70)  
[Half-Life 2](steam://run/220)  
[Half-Life 2: Episode One](steam://run/380)  
[Half-Life 2: Episode Two](steam://run/420)


También se incluye en este paquete y están disponibles para jugar de forma gratuita:  
[Half-Life: Opposing Force](https://steamcommunity.com/linkfilter/?url=https://store.steampowered.com/app/50/HalfLife_Opposing_Force/)  
[Half-Life: Blue Shift](https://steamcommunity.com/linkfilter/?url=https://store.steampowered.com/app/130/HalfLife_Blue_Shift/)  
[Team Fortress Classic](https://steamcommunity.com/linkfilter/?url=https://store.steampowered.com/app/20/Team_Fortress_Classic/)


Sobre HL: Alyx hay que decir que la historia se localizará temporalmente entre la primera y la segunda parte, compartiendo personajes y elementos de sus tramas, por lo que jugar a los títulos ofrecidos gratuitamente previamente será de gran utilidad para comprender lo que ocurra. [Half-Life: Alyx llegará a Steam](https://store.steampowered.com/app/546560/HalfLife_Alyx/) en el proximo mes de marzo, sin fecha concreta. También os recordamos que por el momento no se ha confirmado la versión para nuestro sistema operativo. Os dejamos nuevamente con el espectacular video que vimos ya hace algunos meses:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/O2W0N3uKXmo" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


Aunque en JugandoEnLinux dudamos mucho que no hayais disfrutado aun a estos geniales juegos, os recomendamos encarecidamente que lo hagais... y si ya lo habeis hecho, no está mal repetirlo. Como siempre seguiremos atentos al desarrollo de Half Life: Alyx y su posible soporte en nuestro sistema.  Recuerda que puedes contarnos tu parecer sobre esta noticia en los comentarios, o en mensajes en nuestra comunidad de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

