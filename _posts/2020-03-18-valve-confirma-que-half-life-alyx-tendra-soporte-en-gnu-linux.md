---
author: Serjor
category: Realidad Virtual
date: 2020-03-18 19:32:07
excerpt: "<p>Y aunque solamente unos pocos tengan la equipaci\xF3n necesaria, es una\
  \ buena noticia</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/HalfLife/Alyx/HalfLifeAlyx.webp
joomla_id: 1192
joomla_url: valve-confirma-que-half-life-alyx-tendra-soporte-en-gnu-linux
layout: post
tags:
- realidad-virtual
- valve
- alyx
- half-life
title: "Valve confirma que Half-Life: Alyx tendr\xE1 soporte en GNU/Linux"
---
Y aunque solamente unos pocos tengan la equipación necesaria, es una buena noticia


Leemos en [gaming on linux](https://www.gamingonlinux.com/articles/half-life-alyx-is-confirmed-for-linux-to-arrive-with-vulkan-support-post-release.16244) una gran noticia para todos aquellos linuxeros a los que les guste la franquicia Half Life (y tenga gafas de realidad virtual...).


Y es que según cuentan en su noticia, a pesar de que [inicialmente](index.php/homepage/realidad-virtual/1134-half-life-alyx-tendra-version-para-linux) no sería así, Valve les ha confirmado que Alyx estará disponible en GNU/Linux. Lo que no es tan positivo de la noticia es que inicialmente el soporte será a través de protón, en vez de un port nativo, y que una vez el juego haya sido lanzado ya prepararán un port nativo basado en Vulkan.


Si bien es positivo saber que se podrá jugar a Half-Life: Alyx en GNU/Linux, es un poco decepcionante que una de las empresas que más empuja (sino la más) por el videojuego en GNU/Linux, a la hora de lanzar un juego nuevo lo haga en exclusiva para Windows, y además usando DirectX, no Vulkan.


Podemos entender que Valve apueste por una versión para Windows por lo que comentábamos al principio, linuxeros somos pocos, es la realidad, y en este nicho usuarios con gafas VR no habrá muchos, pero que usen DX en vez de Vulkan por defecto es un tanto decepcionante.


Pero quedémonos con lo positivo, la franquicia Half-Life sigue y Alyx llegará al sistema del pingüino, que al final, y en el fondo, lo importante es que se pueda disfrutar.


Y tú, ¿has conseguido hacerte con unas gafas de realidad virtual para disfrutar de este Half Life: Alyx? Cuéntanoslo en los comantarios o en nuestros canales de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org) y [Telegram](https://t.me/jugandoenlinux).

