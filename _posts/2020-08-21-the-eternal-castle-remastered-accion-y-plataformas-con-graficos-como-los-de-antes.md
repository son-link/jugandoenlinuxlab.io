---
author: leillo1975
category: "An\xE1lisis"
date: 2020-08-21 12:31:38
excerpt: "<p>Nos vamos de vuelta a los a\xF1os 80 con <span class=\"css-901oao css-16my406\
  \ r-1qd0xha r-ad9z0x r-bcqeeo r-qvutc0\">@</span><span class=\"css-901oao css-16my406\
  \ r-1qd0xha r-ad9z0x r-bcqeeo r-qvutc0\">EternalCastle87 . </span></p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/TheEternalCastle/TEC.webp
joomla_id: 1250
joomla_url: the-eternal-castle-remastered-accion-y-plataformas-con-graficos-como-los-de-antes
layout: post
tags:
- retro
- ryan-c-gordon
- the-eternal-castle
- tfl-studios
- cga
title: "The Eternal Castle [Remastered], acci\xF3n y plataformas con gr\xE1ficos como\
  \ los de antes"
---
Nos vamos de vuelta a los años 80 con @EternalCastle87 . 


 En primer lugar y para ser justos nos gustaría aclarar que **este juego tiene ya más de un año y medio**, y desde hace 10 meses tiene soporte para nuestro sistema; pero no ha sido hasta hoy que nos ha llegado un correo de [Jesus Fabre](https://twitter.com/jesusfabre?lang=es), hablándonos sobre su salida en Switch que me he enterado de su existencia. Ciertamente es un poco tarde para hablar de este título, pero creo que su calidad y propuesta merecen que fijemos nuestra atención en él. Como hace un momento os comentaba, **el juego tiene soporte gracias a Ryan Gordon** ([@icculus](https://twitter.com/icculus)) tal y como podeis ver en este tweet:



> 
> LINUX build is now live on Steam!<https://t.co/Eyq0BoBtus>  
>   
> It's been a while but we finally managed thanks to [@icculus](https://twitter.com/icculus?ref_src=twsrc%5Etfw) and his really good work
> 
> 
> — The Eternal Castle [REMASTERED] (@EternalCastle87) [October 3, 2019](https://twitter.com/EternalCastle87/status/1179568298591182853?ref_src=twsrc%5Etfw)







Antes de continuar, me gustaría hacer un receso y comentaros que **este artículo es mitad "noticia", mitad análisis** (o ni una cosa ni la otra, según lo veamos). Comento esto porque para ser un análisis es un tanto recortadito, y además no he jugado lo suficiente como para emitir una opinión mejor formada y un veredicto final, que vereis que no existe como suele ser habitual en este tipo de artículos. Espero que no os importe esta licencia que me he tomado. Hecha la aclaración continuamos:


The Eternal Castle REMASTERED ha sido desarrollado por [TFL Studios](https://www.menchiari.net/), una **pequeña compañía formada por tan solo 3 personas** de Europa y Los Angeles, dirigidas por [Leonard Menchiari](https://twitter.com/LMenchiari) , extrabajador de Valve entre 2011y 2012. El equipo trabajó en remoto reuniéndose cada cierto tiempo en diferentes lugares del mundo para mantener un contacto más estrecho. Como podeis ver en el material gráfico que acompaña este artículo se trata de un juego que emula lo que muchos jugamos a finales de los años 80. **El título utiliza tan solo una paleta de tan solo 4 colores**, tal y como era posible en los antiqusimos PC's de aquella época. **Este tipo de gráficos se llamaba CGA** y aunque existían modos que daban más calidad (EGA, con 16 colores, y VGA con 256), **era el más usado por ser el que menos recursos exigía** de la tarjeta gráfica, y el que menos espacio ocupaba en aquellos disquetes de la época.


Es en este último aspecto en el que se basa la **falsa historia que rodea este juego**, y es que sus creadores han querido darle un aire misterioso al juego contándonos que se trata de un remake de un supuesto juego creado en 1987 que llegó a sus manos en un viejo diskete , donde los archivos que habilitaban el modo VGA habían sido suprimidos para ahorrar espacio. **El juego al parecer nunca ha existido y todo esto ha sido una especie de campaña de marketing para dotar de más interés al título**.


La **historia del juego** gira en torno a que la tierra sufrió un cataclismo que condenó a la raza humana a huir a instalaciones de supervivencia autosuficientes en el espacio, pero debido a guerras y ataques terroristas, los servidores centrales se apagaron y esto provocó que el agua se contaminase de radioactividad y la mayor parte de la población muriese. Unos pocos lograron sobrevivir agotando las reservas, pero debido a esto fué necesario enviar a la tierra a patrullas de recolectores que encontrasen los recursos necesarios para restaurar los sistemas que permitirían la expansión y colonización en el espacio. En una de esas misiones, la que parece ser tu novia, se ha perdido y tu has decidido viajar a la tierra en una misión suicida para intentar salvarla.


![tec_gif](https://cdn.cloudflare.steamstatic.com/steam/apps/963450/extras/fighting_steam.gif)


En cuanto a **manejabilidad y movimientos**, recuerda enormemente por su calidad y cantidad  a títulos como el archiconocido [Prince of Persia](https://es.wikipedia.org/wiki/Prince_of_Persia_(videojuego_de_1989)), juego que a principios de los 90 me robó innumerables horas. Sobre la **estética y escenarios** tiene claras influencias de otro clásico como [Another Word](https://en.wikipedia.org/wiki/Another_World_(video_game)). En mi opinión **exagera un poco el bajo nivel de detalle gráfico** y eso se hace especialmente plausible en la resolución de las tipografías, que en algunas ocasiones la hace practicamente ilegible. A nivel de escenarios como digo, se aprecian **gran cantidad de detalles, pero con una resolución mínima**. Obviamente **esto es una licencia que se toman sus desarrolladores** para darle más un aspecto más "viejuno" a este juego, lo cual lejos de ser un problema para el avance en el juego, en ciertos momentos puede costar un poco, especialmente a aquellos jugadores que no conocieron esta época de los videojuegos en PC.


Si nos centramos en **las sensaciones**, el efecto conseguido con el uso reducido de colores en pantalla no solo nos hace retroceder en el tiempo, sinó que **hace que se incremente nuestra atención a los detalles de los escenarios, y a nivel general nos produzca una absorbente sensación de desasosiego y misterio.** El juego por desgracia no está traducido al español, lo cual puede dificultar un poco el poder seguir la historia, pero no supone un problema muy grande para poder disfrutarlo.


**A nivel sonoro**, los efectos de los personajes y escenarios son correctos, y en este caso alejados de la calidad posible en la época, lo cual es un poco extraño teniendo en cuenta el aspecto tan retro que tiene el juego, pero suponemos que es también otra licencia de sus creadores y que eran perfectamente conscientes de esto cuando los introdujeron. Un aspecto que no debemos dejar de lado es su **excelente** [**banda sonora** creada por Kiiro](https://kiiro.bandcamp.com/album/the-eternal-castle-original-soundtrack), y que casa perfectamente con el título.


Según la **descripción oficial**, en The Eternal Castle encontrareis:


- Disfruta de la atmósfera o corre a toda velocidad a través del cataclismo provocado por la IA en fases repletas de desafíos.  
- Medita, predice y despierta, en el sueño eterno con Adán o Eva  
- Ábrete camino a través de las ruinas antiguas  
- Ábrete camino a tiros a través de la ciudad olvidada  
- Escápate a los monstruos en el laboratorio profano  
- Entra en el Castillo Eterno  
- Recoge los 4 GLIDERS para encender tu nave:  
- Usa hasta 10 armas diferentes:  
- Desbloquea hasta 10 elementos diferentes:  
- Encuentra todos los FRAGMENTOS que faltan para volver a casa:  
- Repite el sueño tantas veces como puedas antes de llegar al final.


Os dejamos con un **video que acabamos de grabar** para que podais entender mejor de todo lo que os hemos estado hablando y esperamos que el juego os guste tanto como a nosotros:  
  
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="420" src="https://www.youtube.com/embed/gkLrEPIe39c" style="display: block; margin-left: auto; margin-right: auto;" width="750"></iframe></div>


Podeis comprar "The Eternal Castle REMASTERED" en Steam, donde actualmente **lo encontrareis rebajado con un 55%** de descuento al fantástico precio de **2.69€**:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/963450/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


¿Qué os ha parecido este "The Eternal Castle REMASTERED"? ¿Os gusta su historia y su original aspecto retro? Cuéntamelo en los comentarios o en los canales de Jugando en Linux en [Telegram](https://twitter.com/JugandoenLinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

