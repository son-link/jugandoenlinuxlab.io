---
author: leillo1975
category: "Simulaci\xF3n"
date: 2018-03-02 16:17:53
excerpt: <p>@SCSsoftware nos anuncia la fecha de lanzamiento</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/7c7b96f53ac4e184df929c559e57af26.webp
joomla_id: 668
joomla_url: beyond-the-baltic-sea-sera-la-nueva-expansion-de-euro-truck-simulator-2
layout: post
tags:
- ets2
- scs-software
- euro-truck-simulator-2
title: "\"Beyond the Baltic Sea\" ser\xE1 la nueva expansi\xF3n de Euro Truck Simulator\
  \ 2 (ACTUALIZADO)"
---
@SCSsoftware nos anuncia la fecha de lanzamiento

**ACTUALIZACIÓN 23-11-18:** Cuando casi no ha dado tiempo a saborear la DLC [Oregon](index.php/homepage/analisis/20-analisis/998-analisis-american-truck-simulator-dlc-oregon) en American Truck Simulator, nos llega la [noticia](http://blog.scssoft.com/2018/11/beyond-baltic-sea-releasing-on-november.html) por parte de la desarrolladora Checha de que **"Beyond the Baltic Sea"**, la nueva expansión para ETS2 estará disponible la semana que viene, concretamente el **día 29 de este mismo mes**. Podeis ver el tweet anunciándolo:



> 
> Beyond the Baltic Sea map expansion for Euro Truck Simulator 2 will be released November 29. We can't wait for you to cross the border and to explore the countries of the Baltic Sea! <https://t.co/aAHnw3veLK>
> 
> 
> — SCS Software (@SCSsoftware) [November 23, 2018](https://twitter.com/SCSsoftware/status/1066012674062983171?ref_src=twsrc%5Etfw)



En esta expansión podremos disfrutar de más de **13000 KM de nuevas carreteras**, **23 nuevas ciudades**, arquitectura, vehiculos, clima y vegetación propios, **30 remolques y empresas locales**, y por supuesto montones de sitios de interés y lugares reconocibles. Como veis todo lo que se puede esperar de las fantásticas DLC's de SCS.


Nosotros por nuestra parte os informaremos puntualmente de la salida de este expansión tan pronto como se produzca. Os dejamos con el video oficial de esta DLC:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/vTGAW3vuY0g" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 




---


**NOTICIA ORIGINAL:** Realmente hace bien poco que recibimos la mejor expansión hasta la fecha de ETS2,  "[Italia](index.php/homepage/analisis/item/688-analisis-euro-truck-simulator-2-italia-dlc)", y justo hace un mes os confirmábamos que [Oregón](index.php/homepage/generos/simulacion/item/753-oregon-sera-la-proxima-adicion-al-mapa-de-american-truck-simulator) sería la próxima area a cubrir en American Truck Simulator.... pues hoy los hiperactivos chicos de SCS Software nos acaban de confirmar mediante un **tweet** que su próxima expansión será la región Báltica. Podeis verlo aquí:



> 
> We're happy to confirm that the next world expansion which we're building for Euro Truck Simulator 2 is the region Beyond the Baltic Sea! ?  
>   
> Check out the link down below for more info ?<https://t.co/oZTvAF8GxK> [pic.twitter.com/x7aCKIziD6](https://t.co/x7aCKIziD6)
> 
> 
> — SCS Software (@SCSsoftware) [March 2, 2018](https://twitter.com/SCSsoftware/status/969561508001787906?ref_src=twsrc%5Etfw)



 Hace bien poco jugaban un poco con los fans dejando caer unos cuantos [screenshots](http://blog.scssoft.com/2018/02/something-else-is-cooking.html) de esta expansión, y muchos jugadores de ETS2 enseguida reconocieron los lugares y elementos que conformaban esta expansión, por lo que no ha sido una sorpresa el anuncio que acaban de realizar. El juego incluirá **Litiuania, Letonia y Estonia**, así como el sur de **Finlandia** y cachitos de **Rusia**, incluida San Petesburgo. La expansión, que se llamará "**Beyond the Baltic Sea**" (Más allá del mar Báltico) no tiene aun fecha de salida definida, pero la desarrolladora nos invita a estar atentos a sus redes sociales para más detalles. Así lo haremos...


![baltic04](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ETS2/BeyondTheBalticSea/baltic04.webp)


¿Qué opinais de esta nueva zona? ¿Estais deseando conducir por los paises bálticos? Déjanos tus respuestas en los comentarios o en nuestro canal de [Telegram](https://t.me/jugandoenlinux).

