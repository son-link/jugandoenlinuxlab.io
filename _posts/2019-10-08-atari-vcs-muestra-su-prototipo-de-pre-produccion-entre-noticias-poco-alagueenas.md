---
author: Pato
category: Hardware
date: 2019-10-08 16:29:52
excerpt: "<p>Seg\xFAn algunos medios, Atari no estar\xEDa gestionando bien el proyecto</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/da9a8cddd3279727df22914d9bc8b003.webp
joomla_id: 1117
joomla_url: atari-vcs-muestra-su-prototipo-de-pre-produccion-entre-noticias-poco-alagueenas
layout: post
tags:
- hardware
- atari
- atarivcs
title: "Atari VCS muestra su prototipo de pre-producci\xF3n entre noticias poco alag\xFC\
  e\xF1as"
---
Según algunos medios, Atari no estaría gestionando bien el proyecto

Primero las buenas noticias. Atari ha enseñado el que sería el PCB (placa) de pre-producción de la próxima Atari VCS, la consola que la compañía quiere lanzar con Linux bajo el capó. Según podemos leer en la última [entrada oficial en Medium](https://medium.com/@atarivcs/atari-vcs-plastics-thermals-and-internals-f72e61897b81), Atari ya tiene un prototipo que sería funcional, y en donde se pueden apreciar distintos elementos como el procesador AMD Ryzen Embedded ensamblado, dos puertos USB 3.0, un puerto HDMI, un conector de alimentación y un puerto para discos M.2 para discos S-ATA (no soporta SSDs NVMe por lo visto).


![AtariprepVCS](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Ataribox/AtariprepVCS.webp)


La placa se completa con dos módulos SODIMM de 4GB DDR4 cada uno para un total de 8GB de RAM, que se podrá expandir. Como ya sabemos, además la consola llevará dos conectores USB más en el frontal,y que no se muestran al no estar directamente conectados a la placa.


Respecto al diseño y fabricación de la carcasa, la compañía afirma que está prácticamente está terminando el proceso por lo que una vez que esté todo listo serán capaces de fabricar el hardware en grandes cantidades.


Por otra parte, según el comunicado la compañía admite que aún les queda un largo camino que recorrer, pues deben testear el hardware y además desarrollar el software que deberá gestionar el sistema personalizado por Atari con Linux como núcleo. En este sentido Atari afirma que actualmente el prototipo aunque tiene una bios personalizada, se comporta como un PC normal y corriente siendo capaz de ejecutar distintos sistemas operativos de escritorio y ya sería capaz de ejecutar juegos en lo que se supone que sería el "modo sandbox". Esto se corrobora mediante una de las imágenes en las que se puede ver como el prototipo ejecuta Ubuntu (en una sesión live USB a tenor del stick que lleva conectado la consola a uno de los puertos USB).


![atariubuntu](/https://miro.medium.com/max/1000/1*r_IROxhsMywWYTB_Lu9H5Q.webp)


Tal y como están las cosas, el plazo estimado de entrega de las primeras unidades a los backers que apoyaron el proyecto en las campañas de financiación ha sufrido un nuevo retraso y se estima para primavera de 2020. Aún así, Atari avisa en el comunicado que aunque el hardware de la consola se entregará 100% terminado, el sistema operativo y las características de software llegarán en fase de "acceso temprano", y se irán actualizando conforme se vayan desarrollando y testeando junto a la comunidad.


En cuanto a las malas noticias, según un (duro) artículo de [The Register](https://www.theregister.co.uk/2019/10/08/atari_architect_quits/) del que también se hace eco [Gamingonlinux.com](https://www.gamingonlinux.com/articles/things-are-going-downhill-for-the-atari-vcs-as-rob-wyatt-quits.15175) **Rob Wyatt**, el flamante diseñador que iba a estar a cargo del diseño de la Atari VCS y que estuvo involucrado en el diseño de otras consolas como la Xbox ha anunciado que dejó de trabajar con Atari al acumular seis meses de facturas impagadas. Al parecer, Atari habría contratado ya a otra empresa, **SurfaceInk** para terminar el proyecto.


Se sabe que Rob Wyatt ha trabajado para Atari a través de su consultora de diseño de hardware **Tin Giant** y que ahora se encuentra trabajando en otro proyecto, curiosamente relacionado con una consola retro llamado "[The last gameboard](https://lastgameboard.com/)", proyecto en el que habría estado trabajando durante los últimos meses y que también pretende lanzar una campaña de financiación.


Sea como sea, y **según el artículo de The Register** Rob ya no trabaja en la Atari VCS, y al parecer la compañía estaría pasando dificultades financieras poniendo en entredicho el desarrollo y lanzamiento de la consola. También afirman haber hablado (anónimamente) con distintos desarrolladores supuestamente relacionados con el desarrollo de la consola que afirman que el proyecto internamente sería un desastre, afirmando que los directivos no saben nada sobre desarrollar un sistema personalizado con base Linux, y que actualmente ni siquiera tendrían un prototipo de sistema ni un frontend desarrollado sobre ningún sistema operativo, mucho menos un proyecto de tienda propia donde poder vender los videojuegos.


Además, se afirma que no hay desarrolladores trabajando en portar juegos y aplicaciones para la consola, con lo que según el artículo los servicios anunciados como disponibles en un futuro para la consola (Hulu, HBO, Spotify...) **se ejecutarían sobre un navegador Chromium modificado**, dejando de lado las aplicaciones nativas.


Tal y como están las cosas, el proyecto despierta ciertas incógnitas que de resultar ciertas pueden dar al traste con el proyecto de PC-Consola de Atari, y un posible buen producto con base Linux si el precio estimado (250$ aprox) acompaña.


¿Tu que piensas? ¿Puede Atari lanzar un producto atractivo al mercado y dar un impulso al juego en Linux? ¿Piensas que el proyecto fracasará y los que apoyaron el proyecto mediante financiación van a perder su dinero? **¿Te gustaría ver una Steam Machine con las especificaciones y precio de esta Atari VCS?**


Cuentamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) , [Discord](https://discord.gg/ftcmBjD) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

