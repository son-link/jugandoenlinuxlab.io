---
author: Laegnur
category: "Exploraci\xF3n"
date: 2022-06-07 17:31:56
excerpt: "<p>Hoy os traemos todas las novedades de la \xFAltima version de <span class=\"\
  css-901oao css-16my406 r-poiln3 r-bcqeeo r-qvutc0\">@Minecraft</span> .</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Minecraft/Minecraft-Update-119.webp
joomla_id: 1473
joomla_url: minecraft-1-19-the-wild-update
layout: post
tags:
- minecraft
- mojang
- the-wild-update
title: 'Minecraft 1.19: The Wild Update'
---
Hoy os traemos todas las novedades de la última version de @Minecraft .


¡Salve jugadores!


Vuelvo a salir de mi cueva para traeros las nuevas novedades de este popular juego. Y es que hoy, martes 7 de Junio de 2022 se lanza la última versión, **Minecraft 1.19**, también conocida como **The Wild Update**.


Esta nueva actualización se centra en dos partes, correspondientes a los nuevos biomas de **Manglar** en la superficie, y **Oscuridad Profunda** en las profundidades del mundo.


Aunque en el anterior artículo sobre el evento [Minecraft Live 2021](index.php?option=com_content&view=article&id=1376:minecraft-live-2021&catid=15:exploracion&Itemid=1418) ya se habían comentando algunas de las actualizaciones de esta versión, veamos cuales son finalmente las nuevas características.


Tened en cuenta que de momento desconozco cual es la traducción definitiva para algunos bloques, por lo que puede no coincidir con las traducciones que estoy empleando, para no poneros los nombres en Inglés.


![Minecraft Wild Update](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Minecraft/minecraft-wild-update.webp)


### Criaturas


* **Ranas**: La primera criatura que encontraremos serán las ranas, añadidas al nuevo bioma, el Manglar, que veremos a continuación. Esta criatura vendrá en tres variantes de color, según la temperatura del bioma donde aparezcan.
* **Renacuajos**: La segunda criatura, esta relacionada con la primera. Alimentando dos ranas con bolas de slime, producirán un bloque no solido sobre la superficie del agua con textura de huevos de rana, de los cuales crecerán los renacuajos antes de crecer a ranas. Podremos recogerlos con un cubo, de igual manera que se puede hacer con los peces o con los axolotes.
* **Allay**: Mediante la votación realizada en el Minecraft Liv 2021, de la que ya hablamos en un articulo anterior, se decidió que de entre tres criaturas, el Allay era el elegido para ser introducido en esta actualización. Sera un valioso aliado que al entregarle un bloque de cualquier tipo, recogerá automáticamente todos los bloques de ese mismo tipo que encuentre sueltos y los llevara al punto de encuentro definido por un bloque Musical.
* **Warden**: El oscuro guardián que residirá en los nuevos biomas de las Ciudades Antiguas, en la Oscuridad Profunda de las que hablaremos a continuación. Es una criatura ciega, pero con buen sentido del olfato, que rastreara y atacara el jugador, una vez alertado por los nuevos sistemas de detección Sculk.


Si en el articulo anterior se nos indicaba que se iban a añadir Luciérnagas al juego, parece que al final esta criatura no va a estar. La idea original era que las ranas comiesen luciérnagas y a cambio produjesen un nuevo bloque de iluminación, pero debido a que en el mundo real algunas especies de luciérnagas son venenosas para las ranas, han desestimado esta idea.


### Bloques


En cuanto a bloques, además de todos los nuevos bloques de Sculk (detectores, catalizadores, chilladores,...) que ya habíamos revisado en el artículo anterior, se nos añaden un buen puñado de nuevos bloques relacionados con los Manglares y su nuevo tipo de árbol, el Mangle.


Asi tendremos bloques de **Hojas de Mangle**, **Madera de Mangle**, **Tablas de Mangle**, **Raíz de Mangle**, la semilla del Mangle en forma de Propágulo; pero también bloques de **Lodo**, **Ladrillos de Lodo**, **Lodo Compactado** y **Huevas de rana**.


La idea de que las ranas coman  a otras criaturas y produzcan un nuevo tipo de bloque de luz sigue en pie, a pesar de que ya no se introduzcan las Luciérnagas. Por lo que ahora, si las ranas se comen **Cubos de Magma** pequeños, producirán un cubo nuevo, **Ranaluz**, similar a los ya existentes de Piedra Luminosa, Linterna de Mar o Brillongo; emitiendo un nivel de luz de 15. El color del bloque de Ranaluz dependerá de la variedad de rana que se coma el Cubo de Magma.


### Elementos


Al haber introducido un nuevo tipo de árbol, tendremos disponible una nueva variante del barco hecho con la nueva madera.


Además se nos añade un nuevo disco musical, el **Disco Musical 5**, que podremos obtener, reconstruyéndolo a partir de fragmentos que podremos encontrar en cofres en las Ciudades Antiguas.


En las Ciudades Antiguas ademas podremos encontrar un nuevo tipo de fragmentos de cristales, los **Fragmentos de Eco**, similares a los que obtenemos de las Ametistas. Con este fragmento podremos crear un nuevo tipo de Brújula, **Brujula de Recuperacion**, que nos señalara el ultimo punto en que hayamos muerto, útil para poder recuperar nuestro equipo.


Ademas de esto, se añade una nueva mecánica por la que si las Cabras se golpean contra bloques de Minerales intentando embestir al jugador, soltaran **Cuernos de Cabra**, que se pueden utilizar para emitir sonidos. El sonido del cuerno variara de uno a otro, definiendo de forma aleatorio cual produce en el momento en que se sueltan, pero divididos en dos grupos dependiendo si la Cabra es de las que chillan o no.


Otro de los cambios más esperados es la opción de poder combinar un Barco con un Cofre dando lugar a un **Barco con Cofre**, que nos permite viajar llevando un inventario ampliado.


**Biomas y Estructuras**


Como ya hemos mencionado, en esta nueva actualización se introducen dos nuevos Biomas, el **Manglar**, similar a los actuales Pantanos, pero compuesto principalmente de bloques de **Lodo** en lugar de bloques de Hierba, y unicamente con arboles de tipo **Mangle**;  y la **Oscuridad Profunda**, compuesta por bloques de Skulk, y donde encontraremos las nuevas estructuras de las **Ciudades Antiguas**, donde deberemos enfrentarnos al temido **Warden**.


A parte de todo esto, habrá como siempre multitud de cambios a bloques ya existentes, correcciones de errores,... Para ver una lista completa de cambios, podemos recurrir a la [Minecraft Wiki](https://minecraft.fandom.com/wiki/Java_Edition_1.19).


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/chuRE9nxLT4" title="YouTube video player" width="780"></iframe></div>


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/htOLSv8-oR8" title="YouTube video player" width="780"></iframe></div>


Ademas de todos estos cambios, aunque no relacionado con el lanzamiento, Microsoft, actual dueño de Mojang, en algún momento de este verano comenzara a regalar la otra versión de Minecraft a aquellos que compren una de ellas en la tienda. Esto es, si compramos Minecraft Java, que es el que funciona en Linux. nos regalaran el Minecraft Windows (o Bedrock) tambien.


Y para aquellos que ya lo hayamos comprado en el pasado, también. En algún momento de este verano podremos descargar la otra versión que no hayamos comprado.


¿Que os parecen todas estas novedades y cambios? Contadnos lo que opináis en los comentarios o en nuestras redes sociales en [Telegram](https://t.me/jugandoenlinux) y [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).


¡Me despido hasta la próxima!

