---
author: yodefuensa
category: "An\xE1lisis"
date: 2017-04-14 18:30:38
excerpt: "<p>Si durante a\xF1os hemos tenido en nuestras manos el poder gestionar\
  \ un club de f\xFAtbol, \xBFpor qu\xE9 ha tardado en llegar lo mismo en el mundo\
  \ del motor?</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/28075211d6824084961c071e8b76c06b.webp
joomla_id: 266
joomla_url: analisis-motorsport-manager
layout: post
tags:
- analisis
- motorsport-manager
title: "An\xE1lisis: Motorsport Manager"
---
Si durante años hemos tenido en nuestras manos el poder gestionar un club de fútbol, ¿por qué ha tardado en llegar lo mismo en el mundo del motor?

Si nunca has oído hablar de Motorsport Manager debes saber que no se trata de un nuevo título. Ya existía anteriormente un juego para dispositivos [moviles](https://play.google.com/store/apps/details?id=com.game.motorsportmanager&hl=es). Sin embargo, estamos ante un juego que ha sido creado desde cero para ofrecer una experiencia más completa a los jugadores de PC. Motorsport Manager nos pone en la piel de un director de equipo, desde ahí tendremos que tomar las decisiones correctas para llevar a nuestro equipo a la cima.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/lHnYWoUKB5I" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>

Tenemos ante nosotros un juego que en principio puede abrumar entre las muchas opciones que nos ofrece, pero una interfaz bien construida y el empezar en una categoría inferior limitará los aspectos del juego para que no nos sintamos perdidos, aun así la curva de aprendizaje es bastante alta.


La base del juego es la toma de decisiones continua. Pongámonos en situación, llevas varias vueltas con neumáticos duros y de repente sale el safety car, con lluvia prevista en las 8 siguientes vueltas. ¿Entras a cambiar neumáticos e intentar recuperar el tiempo perdido? ¿o intentas mantener el tipo procurando que te aguante el neumático? Y no solo se reduce a eso, administrar el consumo de combustible, decidir si queremos que un piloto este por encima del otro, o el hecho de querer exprimir más al coche, esto último acabará afectando a su condición, y su rendimiento. Si, exigir mas a un coche hará que corra más, pero un coche más deteriorado a la larga irá peor y puede obligarnos a pasar por boxes.

![MM1](https://steamuserimages-a.akamaihd.net/ugc/85973093975531346/3F20E8716B018D304B4706324D10F8B955B0F314/?interpolation=lanczos-none&output-format=jpeg&output-quality=95&fit=inside|1024:576&composite-to%3D%2A%2C%2A%7C1024%3A576&background-color=black.webp)  

Además de la propia gestion de las carreras, tendremos en nuestras manos el deber (o no) de renovar los contratos de los corredores, ingenieros y mecánicos. A medida que estos se vuelvan mejores querrán un aumento de presupuesto.

Las carreras serán la representación de todo nuestro trabajo. El hecho de tener un buen equipo, ingenieros, el desarrollo de la sede, todo al final acaba repercutiendo en el coche que tengamos. Cada pieza del coche, personas y circuito tienen sus características, por eso cada carrera sera diferente.


Algo de lo que he disfrutado ha sido comprobar el efecto de mis decisiones dentro del juego. Me explico: si bien empezamos en la categoría de “formula asia” con coches con depósitos de gasolina pequeños, podremos cambiar esto en una de las reuniones de jefes de equipo. Creedme si os digo que las carreras pierden cierta chispa prohibiendo los repostajes, toda aquel consumo extra deberá ser compensado ahorrando después


Podríamos entrar más de lleno a comentar algunos de los aspectos del juego, pero explicarlos todos sería demasiado largo, e incluso desvelaríamos sorpresas que es mejor descubrir por las malas.


En el apartado gráfico tenemos un juego muy cuidado, del que se podrá disfrutar las diferentes localizaciones, ya se sabe no es lo mismo un circuito clásico que uno urbano. Podremos distinguir el gran premio de “suzuka” o el “circuit de catalunya” remarcar las comillas porque el juego de [Playsport Games](http://store.steampowered.com/search/?developer=Playsport%20Games&snr=1_5_9__408) no cuenta con las licencias de la f1 recurren a la vieja picaresca ya vista en los juegos de fútbol en los años 90 (que se parezca es mera coincidencia)

![MM2](https://steamuserimages-a.akamaihd.net/ugc/85973093975599123/9DEF344E62F1FC997ACA64406055A19E883A31C7/?interpolation=lanczos-none&output-format=jpeg&output-quality=95&fit=inside|1024:576&composite-to%3D%2A%2C%2A%7C1024%3A576&background-color=black.webp)


El apartado visual puede ser dejado de lado para máquinas con pocos recursos mediante un modo 2D, eso si la experiencia jugable será la misma. De hecho los requisitos mínimos son bastante bajos:


***MÍNIMO:***


* ***SO:****Ubuntu 14.04+, SteamOS*
* ***Procesador:****Intel Core 2 Duo P8700 @ 2.5 GHz*
* ***Memoria:****4 GB de RAM*
* ***Gráficos:****nVIDIA GT 335M, 512MB or AMD Radeon HD 4670, 512MB or Intel HD 4000 series*
* ***Almacenamiento:****16 GB de espacio disponible*

![MM3](https://steamuserimages-a.akamaihd.net/ugc/94973258569509754/FFA2A0A737C53F21550E1B0435FE2F7EBFDB1167/?interpolation=lanczos-none&output-format=jpeg&output-quality=95&fit=inside|1024:575&composite-to%3D%2A%2C%2A%7C1024%3A575&background-color=black.webp)


Respecto al apartado sonoro del juego, se echa en falta un banda sonora, tendremos una música de ambiente en los menús y durante las carreras escucharemos el sonido de los motores y la grada. Si es cierto que no desentona, se antoja un poco pobre. Sin duda la falta de banda sonora no es un gran escollo en este tipo de juegos, pero no dejo de pensar en lo que podría haber sido.


Motorsport manager pese a ser un buen juego, no es perfecto, echamos en falta un modo multijugador como el ya visto en football manager. Éste posiblemente es el mayor defecto del juego, no poder picarnos online en tablas de clasificación o grupos privados.


Su otro gran defecto es la falta de variedad en los coches. En la misma disciplina todos los coches serán iguales salvo en la pintura, y al cambiar de de categoría los cambios siguen siendo muy parecidos a los de la categoría anterior  
Si bien es cierto que con el [último DLC](http://store.steampowered.com/app/559430/) y gracias a la comunidad podremos disponer de mayor variedad. El juego sigue en desarrollo y se espera que sigan llegando mejoras y es de suponer que más dlcs.


![MM4](https://steamuserimages-a.akamaihd.net/ugc/171541499401675852/168863158FA621AC02923D8C1E87AEFE10FCFA0C/?interpolation=lanczos-none&output-format=jpeg&output-quality=95&fit=inside|1024:576&composite-to%3D%2A%2C%2A%7C1024%3A576&background-color=black.webp)


*¿Sería mucho pedir llegar a ver esto en el juego?*

En conclusión estamos ante un gran título imperdible para cualquier amante del motor, uno de los mejores managers del mercado, puede parecer menos complejo que el football manager pero eso solo será de un primer vistazo. A la espera de que vayan llegando mejoras y contenido que lo completen aun más.


Si te gustan los managers y el mundo del motor, con este 'Motorsport Manager' desde luego disfrutarás de la emoción y la tensión de las carreras como si estuvieras en el Pit Lane, o en el despachio del director deportivo.


Puedes comprar 'Motorsport Manager' traducido al español en su página de Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/415200/" width="646"></iframe></div>


Este análisis ha sido escrito por un invitado (**Yodefuensa**). Si quieres enviarnos tus artículos o análisis de tus juegos en Linux puedes hacerlo [desde este enlace](index.php/contacto/envianos-tu-articulo).


También puedes contarnos qué te parece este Motorsport Manager en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

