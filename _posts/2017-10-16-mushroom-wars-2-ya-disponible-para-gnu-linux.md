---
author: Serjor
category: Estrategia
date: 2017-10-16 20:38:27
excerpt: "<p>Alucinar\xE1s con estas setas tan peleonas</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/6423aed892dbf43714968caa3f627583.webp
joomla_id: 488
joomla_url: mushroom-wars-2-ya-disponible-para-gnu-linux
layout: post
tags:
- rts
- estrategia
- mushroom-wars-2
title: Mushroom Wars 2 ya disponible para GNU/Linux
---
Alucinarás con estas setas tan peleonas

Ya lo avisaban los propios desarrolladores, y es que unas semanas después de su lanzamiento en Windows, Mushroom Wars 2 haría su aparición en la plataforma del pingüino, y finalmente, hoy ha llegado tal ansiado día.


Así nos lo hacía saber uno de los desarrolladores en este [mensaje](http://steamcommunity.com/app/457730/discussions/0/343787920139474612/#c1483232961046497868), la espera había terminado incluso algo antes de lo esperado.


Este Mushroom Wars 2, que ya vio la luz en iOS hace casi un año, nos llega para traernos un RTS claramente enfocado a dispositivos móviles ya que las partidas serán rápidas y cortas, pero no por ello menos divertidas ya que deberemos comandar un ejército con miles de unidades mientras nos preocupamos de la gestión de los recursos. Trae también un modo historia y varios modos multiplayer, con un modo liga que parece orientado a eventos competitivos.


Así que ya sabes, si te gusta la experiencia de juego que propone este Mushroom Wars 2 no dudes en hacerte con él.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" frameborder="0" height="315" src="https://www.youtube.com/embed/XaPiMr4l41M" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


<div class="steam-iframe"><iframe frameborder="0" height="190" src="https://store.steampowered.com/widget/457730/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


Y tú, ¿eres más de setas o de champiñones? Cuéntanos qué te parece este Mushroom Wars 2 en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD)

