---
author: leillo1975
category: Software
date: 2017-05-23 07:31:39
excerpt: "<p>Esta versi\xF3n Beta incluye adem\xE1s numerosos cambios.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/69b5f85da08d181d48515c6d41e3e0a5.webp
joomla_id: 333
joomla_url: valve-sustituye-amdgpu-pro-por-mesa-en-steamos
layout: post
tags:
- steamos
- mesa
- valve
- amdgpupro
- kernel
- debian
title: Valve sustituye AMDGPU-PRO por Mesa en SteamOS (ACTUALIZADO)
---
Esta versión Beta incluye además numerosos cambios.

Ayer por la noche [recibíamos la noticia](http://steamcommunity.com/groups/steamuniverse/discussions/1/1291816880503248813/) de que Valve actualizaba su distribución enfocada hacia juegos por una versión **BETA** con numerosos e importantes cambios. Estas novedades son de calado y muy importantes para todos los usuarios de este sistema operativo y muy especialmente para los que dispongan de Steam Machines con gráficas AMD, tanto integradas como dedicadas.Con estos cambios **se deja de utilizar el driver privativo (AMDGPU-PRO) en favor del libre (Mesa)**, que se ha demostrado mucho más capaz en sus últimas versiones en la mayoría de los juegos, además de aumentar en gran medida su compatibilidad.


 


También es muy importante destacar que se ha cambiado la versión del Kernel por la reciente 4.11 que viene repleto de novedades y mejoras a nivel gráfico, así como todas las actualizaciones de paquetes correspondientes a **Debian 8.8**, distribución madre de SteamOS: Para disponer del **driver de Vulkan** será necesario activar la Beta del Cliente de Steam. En cuanto al driver privativo de Nvidia, se pasa a utilizar la última versión, la 381.22.


 


Como todos sabreis, **Valve ha estado involucrada muy activamente en la mejora de los drivers tanto para AMD como para Intel**. Esto supone el comienzo de una nueva etapa en la compañía hace uso de sus esfuerzos y colaboraciones con la comunidad. Sin duda es un movimiento muy importante que nos demuestra que a pesar del largo tiempo sin noticias no han estado de brazos cruzados y siguen mejorando su distribución. Si el driver libre para AMD funciona, esto abarataría el precio de las Steam machines y aumentaría las opciones para los usuarios, además de tener la compañia más control sobre el driver, al no depender de otra empresa para mejorar o corregir cosas.


 


**ACTUALIZACIÓN:** En este momento **[la actualización ya está en el canal official de Brewmaster](http://steamcommunity.com/groups/steamuniverse/discussions/1/1291817837636983784/)**, por lo que los que no estén en la Beta recibirán estas suculentas y potentes actualizaciones.


 


¿Que os parece esta noticia? ¿Sois usuarios de SteamOS? ¿Contemplais la posibilidad de una Steam Machine? Déjanos tus impresiones en los comentarios, o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD).

