---
author: Pato
category: Estrategia
date: 2017-11-29 18:33:58
excerpt: <p>Construye y gestiona un imperio de ferrocarriles</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/18ff4d910cedae8e6e6069cb21af4163.webp
joomla_id: 558
joomla_url: railway-empire-partira-de-la-estacion-linux-steamos-el-proximo-26-de-enero
layout: post
tags:
- estrategia
- simulacion
- gestion
title: "'Railway Empire' partir\xE1 de la estaci\xF3n Linux/SteamOS el pr\xF3ximo\
  \ 26 de Enero"
---
Construye y gestiona un imperio de ferrocarriles

¿Eres de los que les gustan los juegos de gestión tipo ticoon? Kalipso Media nos trae noticias frescas sobre Railway Empire, un juego en el que tendrás que construir y gestionar tu propio imperio de ferrocarriles y transportes.



> 
> En **Railway Empire** crearás una compleja y **extensa red de ferrocarril**, comprarás más de **40 trenes diferentes**, modelados con extraordinario detalle, y adquirirás o **construirás estaciones de ferrocarril, edificios de mantenimiento, fábricas y atracciones turísticas** para mantener tu red de transporte por delante de la competencia. Si quieres que el servicio de trenes sea eficaz, también deberás contratar y supervisar a trabajadores, así como desarrollar más de **300 tecnologías**, como mejorar los mecanismos o los propios trenes, crear infraestructuras o servicios avanzados en el lugar de trabajo, etc., todo ello mientras progresas a través de cinco eras de innovaciones tecnológicas.
> 
> 
> 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/RO6dGjtgFc0" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 En 'Railway Empire' no te bastará con construir e investigar para llegar a lo más alto: la competencia nunca duerme, y para que tu negocio vaya por buen camino, tendrás que sobrevivir a tres magnates rivales. Puede que, para triunfar, debas recurrir cada vez más a tácticas poco ortodoxas, como atacar y sabotear a tus enemigos mediante asaltos y espionaje industrial.


En cuanto a los requisitos de sistema, aún no han sido publicados en Steam, pero si nos guiamos por los de Windows, no deberían diferir mucho:


Recomendados:


* **Procesador:** Intel Core i5 2400s @ 2.5 GHz or AMD FX 4100 @ 3.6
* **Memoria:** 8 GB de RAM
* **Gráficos:** nVidia GeForce GTX 680 o AMD Radeon HD7970 o superior (2048MB VRAM o superior)
* **Almacenamiento:** 7 GB de espacio disponible


Si estás interesado en convertirte en un magnate del ferrocarril y construir tu imperio, tendrás disponible 'Railway Empire' totalmente en español el próximo 26 de Enero, aunque ya se puede precomprar en acceso anticipado (solo para Windows) con un descuento del 15%:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/503940/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 ¿Qué te parece 'Railway Empire? ¿Construirás tu propia línea de ferrocarriles?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

