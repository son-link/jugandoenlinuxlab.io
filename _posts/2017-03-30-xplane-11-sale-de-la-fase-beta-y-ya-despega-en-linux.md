---
author: Pato
category: "Simulaci\xF3n"
date: 2017-03-30 20:48:18
excerpt: "<p>La \xFAltima versi\xF3n del simulador de Laminar Research viene cargada\
  \ de novedades</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/5ef17ed4d733d4dc3519b291889643fe.webp
joomla_id: 270
joomla_url: xplane-11-sale-de-la-fase-beta-y-ya-despega-en-linux
layout: post
tags:
- simulador
- vuelo
- realista
- mundo-abierto
title: '''Xplane 11'' sale de la fase beta y ya despega en Linux'
---
La última versión del simulador de Laminar Research viene cargada de novedades

Seguimos con los lanzamientos. Hace tan solo una hora nos llegaba notificación de que Xplane 11 ya había terminado su fase beta y estaba disponible de forma oficial.



> 
> X-Plane 11 is now officially released on our website and Steam! <https://t.co/eijNduf0Xa> <https://t.co/V7phDxbAJY>
> 
> 
> — X-Plane (@XPlaneOfficial) [30 de marzo de 2017](https://twitter.com/XPlaneOfficial/status/847525964674473988)



'XPlane 11' [[web oficial](http://www.x-plane.com/)] es un simulador de aviación realista que viene a cubrir el hueco que dejó Microsoft Flight Simulator X hace ya unos años, y que aunque aún sigue recibiendo soporte por parte de otras empresas que se han encargado de seguir actualizándolo e introduciéndole novedades, lo cierto es que ya se le nota la edad. Es aquí donde este XPlane 11, con un renovado diseño y novedades tanto gráficas como técnicas nos trae una nueva propuesta realmente novedosa y modernizada de un simulador realista y realmente profundo.


Por suerte, y a diferencia de Flight Simulator Laminar Research viene desarrollando su simulador en multiplataforma, de modo que nos llega a Linux de forma nativa. De hecho, XPlane es utilizado de forma profesional en algunas de las escuelas de vuelo real en EEUU. No hay que engañarse; 'XPlane 11' es un simulador "hardcore" con una curva de aprendizaje bastante pronunciada pero que a los puristas y sibaritas de la simulación aérea nos supondrá horas y horas de entretenimiento.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/Ov0Sm9GDha4" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


 Sinopsis de Steam:


*X-PLANE 11 es el simulador detallado, real y moderno que has estado esperando.*   
*El nuevo X-PLANE 11, es un simulador completamente rediseñado, con una interfaz de usuario intuitiva que hará la configuración y edición de vuelo más fácil que nunca.*   
*Cabinas de mando en 3-D y modelos exteriores con un sensacional diseño en alta definición para todas las aeronaves disponibles que harán su experiencia más real.*   
*Un nuevo motor de efectos para iluminación, sonidos y explosiones.*  
*Aviónicas y sistemas de navegación realistas: todas las aeronaves están preparadas para volar IFR desde el inicio.*  
*Aeropuertos “con vida”, ocupados, donde se encontrará vehículos de remolque, camiones de combustible, que están listos para prestar servicio a todas las aeronaves del simulador, incluyendo la suya.*  
*Nuevas edificaciones, calles, avenidas que simulan mejor las ciudades europeas.*


*Todo esto y mucho más…*


Además, Xplane 11 viene con varias aeronaves disponibles de forma gratuita ya en el simulador de base, al igual que podremos descargar unos cuantos mapas ya disponibles como el teatro europeo o el asiático también de forma gratuita.


Desde la aviónica hasta las características de vuelo personalizadas para cada aeronave, XPlane 11 es lo que todo aficionado a la simulación de vuelo civil estaba esperando. Sin embargo, un simulador de este calibre es de los "programas" que más carga de procesamiento necesitan, tanto de CPU como de GPU. Aquí teneis los requisitos mínimos y recomendados:


Mínimos:


SO: Testeado en Ubuntu 16.04LTS  
Procesador: Intel Core i3, i5, o i7 CPU con 2 núcleos o mas, o AMD equivalente  
Memoria: 8 GB de RAM  
Gráficos: DirectX 11 compatible NVIDIA o AMD  con /512 MB VRAM  
Red: Conexión de banda ancha a Internet  
Almacenamiento: 20 GB de espacio disponible  
Tarjeta de sonido: Defecto  
Notas adicionales: El requerimiento de espacio en disco se basa en cuantas áreas de escenarios se descarguen. El mínimo con un escenario es de 20 GB pero puede variar añadiendo escenarios de forma gratuita disponibles sin coste después de comprar el simulador


Recomendados:


SO: Testeado en Ubuntu 16.04LTS  
Procesador: Intel Core i5 6600K a 3.5 ghz o superior  
Memoria: 16 GB de RAM  
Gráficos: Tarjeta NVIDIA o AMD con /4 GB VRAM  
Red: Conexión de banda ancha a Internet  
Almacenamiento: 65 GB de espacio disponible  
Tarjeta de sonido: Defecto


Como podeis ver, pide bastante máquina, pero desde luego no es algo que a los aficionados a este tipo de simuladores les pille de susto.


Si estás interesado en esto de los simuladores de vuelo, puedes comprar 'XPlane 11' diréctamente [en su página web](http://www.x-plane.com/desktop/buy-it/), o en su página de Steam:


<div class="steam-iframe"><iframe frameborder="0" height="190" src="https://store.steampowered.com/widget/269950/" width="646"></iframe></div>


Además, en su web tienes disponible una versión demo para que puedas probarlo y ver que tal te va, e incluso yo mismo hice un pequeño tutorial en el foro de cómo ejecutarlo. Puedes verlo [en este enlace](index.php/foro/juegos-nativos-linux/6-x-plane-11-demo-como-ejecutarlo).


¿Te gustan los simuladores? ¿Piensas volar este XPlane 11?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

