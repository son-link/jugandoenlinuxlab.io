---
author: Serjor
category: "Exploraci\xF3n"
date: 2018-02-14 21:41:31
excerpt: <p>Este roguelike espacial demuestra su dificultad incluso a los propios
  desarrolladores</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/96e5c789d9b8bf3a9e86699e2b445aec.webp
joomla_id: 645
joomla_url: nueva-beta-disponible-para-everespace
layout: post
tags:
- espacio
- aventura
- beta
- roguelike
- everspace
- rockfish-games
title: Nueva beta disponible para EVERESPACE
---
Este roguelike espacial demuestra su dificultad incluso a los propios desarrolladores

Leemos en [GOL](https://www.gamingonlinux.com/articles/the-linux-beta-of-everspace-has-been-updated-with-unreal-engine-417-and-bug-fixes.11225) una esperanzadora noticia, y es que la gente detrás de [EVERSPACE](index.php/buscar?searchword=everespace&ordering=newest&searchphrase=all) ha sacado una versión actualizada de la beta para GNU/Linux.


Y es especialmente esperanzadora porque según [comentan](http://steamcommunity.com/app/396750/discussions/0/1473096694440345684/?ctp=28) han tenido muchos problemas con la integración de SDL2 dentro del motor del juego, así que es de agradecer que esos problemas y retrasos no les hayan echado para atrás y continúen con el desarrollo de la versión oficial para GNU/Linux.


A parte de corregir errores, como ya os [anunciamos](index.php/homepage/generos/accion/item/722-everspace-actualizara-su-motor-a-unreal-engine-4-17-con-su-proximo-parche), han migrado a la versión 4.17 del Unreal Engine 4, aunque no parece que hayan podido solucionar aún sus problemas con los drivers de AMD.


Si os pica la curiosidad, hicimos un [análisis](index.php/homepage/analisis/item/605-analisis-everspace) del estado del juego cuando salió, y además nuestro compañero Leo nos dejó un gameplay contrastando opiniones:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/G6zj57jcJSM?rel=0" style="border: 0px;" width="560"></iframe></div>


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/396750/" style="border: 0px;" width="646"></iframe></div>


Y a tú, ¿ya navegas por el espacio o estás esperando a la versión final para GNU/Linux? Cuéntanoslo en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD)

