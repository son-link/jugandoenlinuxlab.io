---
author: leillo1975
category: "Software"
date: 09-10-2023 16:28:55
excerpt: "@bernat_arlandis ha añadido soporte a multitud de nuevos dispositivos"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SuperWodenGP2/SWGP2.webp
joomla_id: 1532
joomla_url: super-woden-gp-2-tendra-correra-nativamente-en-linux
layout: post
tags:
- analisis
- nativos
- vijuda
- super-woden-gp
- super-woden-gp-2
title: "Super Woden GP 2, que correrá nativamente en Linux, ya tiene fecha de lanzamiento (ACTUALIZACIÓN 2)"
---
Su creador, @ViJuDaGD , nos acaba de comunicar que la fecha de salida se ha adelantado.

* * *

**ACTUALIZADO 19-10-23:** 

Victor, el creador de Super Woden GP se ha puesto en contacto con nosotros para comunicarnos que finalmente **podremos disfrutar de su juego una semana antes de lo esperado**, por lo que la fecha definitiva de salida será el día **10 de Noviembre a las 16:00**. Seguro que muchos de vosotros, que estáis deseosos de echarle el guante, agradecéis este cambio de fecha, ya que lo habitual en este mundillo son los retrasos y no los adelantos. Os dejamos aquí el tweet donde se hace este anuncio:

> 📣Attention! [#SuperWodenGP2](https://twitter.com/hashtag/SuperWodenGP2?src=hash&ref_src=twsrc%5Etfw) Super Woden GP 2 will arrive on Steam sooner than expected, on November 10! Please, can you RT 🔁 so it reaches everyone? Thank you very much for your support!🩷[https://t.co/DEgh7QEDti](https://t.co/DEgh7QEDti)[#GamersUnite](https://twitter.com/hashtag/GamersUnite?src=hash&ref_src=twsrc%5Etfw) [#indiedev](https://twitter.com/hashtag/indiedev?src=hash&ref_src=twsrc%5Etfw) [#gamedev](https://twitter.com/hashtag/gamedev?src=hash&ref_src=twsrc%5Etfw) [#NewRelease](https://twitter.com/hashtag/NewRelease?src=hash&ref_src=twsrc%5Etfw) [#racing](https://twitter.com/hashtag/racing?src=hash&ref_src=twsrc%5Etfw) [#IndieGameDev](https://twitter.com/hashtag/IndieGameDev?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/bJFEvoZZ38](https://t.co/bJFEvoZZ38)
> 
> — ViJuDa (@ViJuDaGD) [October 18, 2023](https://twitter.com/ViJuDaGD/status/1714670804321853725?ref_src=twsrc%5Etfw)

Nosotros, por nuestra parte, os mantendremos al corriente en cuanto salga el juego, por lo que os encomendamos a que sigáis nuestras redes sociales de [Mastodon](https://mastodon.social/@jugandoenlinux) y [Twitter](https://twitter.com/JugandoenLinux) (o X, come quieren llamarle ahora).

* * *

**ACTUALIZADO 9-10-23:**   
Tras medio año desde que os hablamos por última vez de este juego, y tras un arduo proceso de desarrollo volvemos a vosotros para comunicaros que finalmente el juego ya tiene un día marcado en el calendario para su lanzamiento. Han sido meses donde **se ha pulido multitud de aspectos del juego, corregido bugs e incluso añadido algunas cosas nuevas**, como la esperada **posibilidad de mejorar los coches en el taller** con piezas de mayor rendimiento. También se ha hecho hincapié en la **personalización del juego**, permitiendo a los jugadores **cambiar los nombres y logos de los vehículos**, e incluso **agregar tu propia música**, algo que no será necesario ya que el juego cuenta con una magnífica banda sonora compuesta por el mismo desarrollador que se adapta perfectamente al título.

Tal y como nos promete su desarrollador, **Victor Justo** ([@ViJuDaGD](https://twitter.com/ViJuDaGD)), "Super Woden GP 2 promete llevar la emoción de las carreras a un nuevo nivel y ofrecer una experiencia única en el género de carreras". Finalmente **podreis disfrutarlo a partir del próximo 18 de Noviembre**. Nosotros desde JugandoEnLinux estaremos al tanto para ofreceros información tanto de su salida en [Steam](https://store.steampowered.com/app/2083210/Super_Woden_GP_2/), así como con gameplays y/o directos del juego. ¡Nos vemos en un mes!

* * *

  
**NOTICIA ORIGINAL 7-4-23:**  
Hace año y medio publicamos un [análisis de Super Woden GP]({{ "/analisis-super-woden-gp" | base_url}}), y en él ensalzábamos las numerosas cualidades de este título que nos encandiló en cuanto pusimos nuestras manos en el gamepad para jugarlo. Por suerte para nosotros, eso ocurrió muchos meses antes de su lanzamiento, ya que en JugandoEnLinux.com tuvimos el honor de participar en su desarrollo como **betatesters de la versión Linuxera**. Al igual que en esa ocasión, Víctor, su creador ha confiado una vez más en nosotros para esa tarea y os podemos asegurar que su trabajo no os va a dejar indiferentes.

En esta nueva versión encontrareis un **apartado visual mucho más trabajado, donde el 3D toma protagonismo**, no solo por una mayor calidad gráfica, sino por una iluminación más compleja. diferentes **elevaciones del trazado**, o por las reacciones de los coches a estas (lo cual demuestra que las **físicas renovadas** toman un papel relevante). Por ejemplo, podemos ver **espectaculares saltos**, accidentes entre coches mucho más realistas, perdidas de adherencia en los cambios de rasante... Otra cosa que se percibe es una **IA más "cabrona"** que te bloquea el paso en los adelantamientos cambiando la trazada.

![](https://cdn.cloudflare.steamstatic.com/steam/apps/2083210/ss_90e908704c2470de12e4fcbe73500b4b60b14bf0.1920x1080.jpg)

Se nota también **mucho esfuerzo en los menúes del juego**, donde al igual que en la anterior ocasión impera un **mapa al estilo Gran Turismo** como menú principal, pero con muchos más apartados, encontrando más marcas que en el anterior (con una cantidad de nuevos coches asombrosa), o con un **divertidísimo modo Arcade** al más puro estilo World Rally Championship, incluso un **nuevo taller** donde además de lavar y pintar nuestro coche, podemos realizarle **mantenimientos para que no pierda rendimiento** (y quizás en el futuro alguna cosilla más).

Para ir abriendo boca, hoy mismo se ha lanzado una **demo del modo arcade** del juego donde podréis comprobar por vosotros mismos algunas cosas de las que os hablo en una ruta de 8 etapas, y además con la **posibilidad de ganaros una copia del juego si conseguís estar entre los 5 mejores tiempos**. Teneis más info en este tweet de la cuenta oficial de ViJuDa:  
  

> Demo Contest!  
> This Friday the 7th, the -Arcade Demo- will be available, representing a small part of this game mode. As in SWGP1, the top 5 best times will get a free copy of the game when it is released. Conditions in the next tweet!  
> Please, RT 😁[https://t.co/sKgSfuC6g7](https://t.co/sKgSfuC6g7)\> [pic.twitter.com/XyNF7dX59u](https://t.co/XyNF7dX59u)
> 
> — ViJuDa (@ViJuDaGD) [April 5, 2023](https://twitter.com/ViJuDaGD/status/1643713702695149569?ref_src=twsrc%5Etfw)

El juego se espera para **algún momento entre el final del verano y el principio del otoño**. Nosotros desde JugandoEnLinux.com seguiremos, como es lógico el desarrollo de este título e os iremos informando a medida que tengamos más cosas que contaros. Es muy probable también que publiquemos algún gameplay de una versión previa muy pronto, por lo que os recomendamos que permanezcais atentos a nuestras redes sociales en [Mastodon](https://mastodon.social/@jugandoenlinux) y [Twitter](https://mobile.twitter.com/jugandoenlinux). Si quereis podeis ir poniendo en deseados el juego pinchando en el botón del Widget de Steam:

<div class="iframe-steam">
	<iframe src="https://store.steampowered.com/widget/2083210/" width="646" height="190"></iframe> 
</div>