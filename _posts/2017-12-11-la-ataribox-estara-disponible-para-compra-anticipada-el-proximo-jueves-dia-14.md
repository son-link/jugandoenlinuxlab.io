---
author: Pato
category: Hardware
date: 2017-12-11 18:35:13
excerpt: "<p>Atari ha suspendido moment\xE1neamente la campa\xF1a en Indiegogo</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/55bd777287987cb3888bd81f33e8a7ad.webp
joomla_id: 569
joomla_url: la-ataribox-estara-disponible-para-compra-anticipada-el-proximo-jueves-dia-14
layout: post
tags:
- proximamente
- hardware
- ataribox
title: "La Ataribox estar\xE1 disponible para compra anticipada el pr\xF3ximo jueves\
  \ d\xEDa 14 (actualizaci\xF3n)"
---
Atari ha suspendido momentáneamente la campaña en Indiegogo

**(Actualización 14/12/2017)** Atari ha emitido un comunicado con la siguiente información que reproducimos íntegra:



> 
> La cuenta atrás para el lanzamiento de la Ataribox en Indiegogo se ha pausado oficialmente. Debido a la falta de un elemento clave en nuestro checklist, está tomando mas tiempo el crear la plataforma y el ecosistema que la comunidad de Atari merece. Construir la Ataribox es increíblemente importante para nosotros y haremos lo que sea necesario para asegurarnos que la espera merece la pena.
> 
> 
> Un plan de lanzamiento está en camino y más información detallada estará disponible pronto.
> 
> 
> Os mantendremos a vosotros, nuestra comunidad informada de cada paso en el camino. Gracias otra vez por vuestro apoyo a Ataribox.
> 
> 
> -El equipo de Ataribox
> 
> 
> 


Así pues, toca esperar hasta ver qué es lo que ha fallado y a qué se debe este retraso. ¿Os inspira confianza? ¿Que pensáis de esto?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).




---


**(Artículo original)**


En un comunicado a primera hora de esta tarde Atari nos mostraba una imagen con una fecha significativa, 12.14.17 para informarnos de que su próxima consola "Ataribox" estará disponible para su compra anticipada precisamente el próximo Jueves día 14 de este mes de Diciembre.


Sin más detalles sobre sus características finales o su precio anticipado, nos emplazan a dicha fecha para saber más sobre su "Ataribox", a la vez que nos recuerdan que la oferta de pre-compra será muy limitada, por lo que si queréis haceros con una de estas unidades a un precio especial habrá que estar atentos al anuncio de la campaña. Por supuesto, en cuanto tengamos noticias de que la compra anticipada esté disponible os lo haremos saber puntualmente aquí, en Jugandoenlinux.com.


Recordaros que la "Ataribox" será [una consola con un sistema Linux](index.php/homepage/hardware/item/588-ataribox-usara-gnu-linux) en su interior, contará con una APU Bristol de AMD junto con una gráfica Radeon que correrá los míticos juegos de la compañía aparte de otros títulos que no requieran de mucha potencia y que será capaz de ejecutar juegos de otras plataformas además de [hacer Streaming](index.php/homepage/hardware/item/613-ataribox-podra-reproducir-contenido-de-servicios-de-streaming-como-hulu-hbo-go-o-netflix) de distintos servicios de vídeo bajo demanda, tal y como ya os hemos contado en anteriores artículos.


Se sabe que el precio estará entre los 250 y 300 dólares y [traerá un joystick](index.php/homepage/hardware/item/675-atari-muestra-las-primeras-imagenes-del-joystick-de-la-ataribox) al estilo de los de las míticas consolas de Atari de los 80.


¿Qué te parece este anuncio? ¿Esperas a la campaña de indiegogo para aportar al proyecto? ¿Piensas hacerte con una "Ataribox" en oferta de compra anticipada?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

