---
author: Pato
category: Ofertas
date: 2017-12-13 15:48:59
excerpt: "<p>M\xE1s de 900 juegos rebajados hasta el 90%</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/8d7346542ffded93378375d0c10efb4f.webp
joomla_id: 575
joomla_url: ya-estan-disponibles-las-rebajas-de-invierno-de-gog
layout: post
tags:
- rebajas
- ofertas
title: "Ya est\xE1n disponibles las rebajas de invierno de GOG"
---
Más de 900 juegos rebajados hasta el 90%

Estamos ya prácticamente en épocas navideñas y además, es época de rebajas en las que las tiendas digitales aprovechan para hacer su particular "agosto"con las "rebajas de invierno". Es el caso de [gog.com](https://www.gog.com/), que como es habitual se adelanta a otras tiendas y ya ha publicado sus rebajas de invierno con más de 900 títulos rebajados con hasta un 90% de descuento.


Además de eso, cada día habrán nuevos títulos rebajados, y tenemos a nuestra disposición las "estrellas misteriosas" al módico precio de **2,59€**, con las que nos ofrecerán un título sorpresa con cada una que compremos.


Las rebajas de gog.com estarán disponibles hasta el próximo día 26 de Diciembre.


¿A qué esperas? Pásate ya por [gog.com](https://www.gog.com/), y ¡no olvides comentarnos qué es lo que has pillado de oferta!

