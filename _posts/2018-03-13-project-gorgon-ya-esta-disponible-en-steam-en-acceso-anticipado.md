---
author: Pato
category: Rol
date: 2018-03-13 09:21:55
excerpt: <p>El juego ofrece soporte en Linux de salida</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/0a47a31ecf1649c40be3821bbc214aed.webp
joomla_id: 674
joomla_url: project-gorgon-ya-esta-disponible-en-steam-en-acceso-anticipado
layout: post
tags:
- rol
- acceso-anticipado
- mmorpg
title: "'Project: Gorgon' ya est\xE1 disponible en Steam en acceso anticipado"
---
El juego ofrece soporte en Linux de salida

Hace un tiempo [hablamos](index.php/homepage/generos/rol/item/768-project-gorgon-dispone-de-soporte-experimental-para-gnu-linux) sobre un nuevo MMORPG que ofrecía soporte experimental en GNU-Linux y que hoy llega a Steam en acceso anticipado. Se trata de 'Project: Gorgon', un juego de rol masivo online en 3D de corte clásico en donde poder vivir nuestras aventuras "roleras" junto a otros en un vasto mundo virtual. ¿Alguien dijo WoW?


*"Project: Gorgon [[web oficial](https://www.projectgorgon.com/)] es un MMORPG 3D de temática fantástica caracterizado por ofrecer una experiencia inmersiva que permite a los jugadores crear su propio camino hacia la exploración y el descubrimiento. No te guiaremos en un "mundo sobre railes", y como resultado hay muchos secretos ocultos esperando a ser descubiertos. Porject: Gorgon también tiene un sistema de progreso basado en habilidades muy ambicioso que se aparta de la actual tendencia de las clases predeterminadas, permitiendo a los jugadores combinar habilidades para crear una experiencia de juego única."*


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/lCbqcWESWPo" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Entre las características destacadas de 'Project: Gorgon' encontramos:


* Cada uno de los NPC tiene sus propios intereses y actuarán en base a ellos.
* Puedes dejar objetos en el suelo y otros jugadores los podrán recoger.
* Los vendedores mantienen los objetos en stock, por lo que podrás comprar lo que otros jugadores les han vendido.
* Puedes escribir mensajes en los objetos, y otros jugadores podrán leerlos.
* Puedes crear las clases a tu antojo, mezclando habilidades de combate según tu criterio para dar forma a tu personaje.
* Puedes aprender todas las habilidades que quieras.


En cuanto a los requisitos, según han publicado son:


**Mínimo:**  

+ Requiere un procesador y un sistema operativo de 64 bits
+ **SO:** Ubuntu 14.04
+ **Procesador:** Intel Core 2 DUO 2.4 GHz / AMD Athlon X2 2.7 GHz
+ **Memoria:** 4 GB de RAM
+ **Gráficos:** ATI Radeon HD 3870 / NVIDIA 8800 GT / Intel HD 4000
+ **Red:** Conexión de banda ancha a Internet
+ **Almacenamiento:** 10 GB de espacio disponible


**Recomendado:**  

+ Requiere un procesador y un sistema operativo de 64 bits
+ **SO:** Ubuntu 14.04
+ **Procesador:** Quad Core Processor 2.4GHz o superior
+ **Memoria:** 8 GB de RAM
+ **Gráficos:** AMD Radeon R9 / NVIDIA GeForce GTX 1070
+ **Red:** Conexión de banda ancha a Internet
+ **Almacenamiento:** 10 GB de espacio disponible


Si estabas esperando un MMORPG que echarle a tu GNU-Linux, puedes verlo y comprarlo, eso sí, en acceso anticipado y sin traducción en español en su página de Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/342940/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>
 



