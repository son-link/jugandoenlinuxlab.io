---
author: Serjor
category: Puzzles
date: 2017-07-17 04:00:00
excerpt: "<p>Funky y charcos de sangre con m\xE1s flow que Tony Manero</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/0e055015385980da4e111a90c5284edc.webp
joomla_id: 410
joomla_url: serial-cleaner-disponible-para-linux
layout: post
tags:
- sigilo
- serial-cleaner
- infiltracion
title: Serial Cleaner disponible para Linux
---
Funky y charcos de sangre con más flow que Tony Manero

El pasado 14 de julio los chicos de [ifun4all](http://ifun4all.com/index.php/serial-cleaner/) nos trajeron la versión final de Serial Cleaner, un juego en el que encarnaremos con estilo y soul a esa parte de la mafia que no sale en las películas y de la que nadie se acuerda, el servicio de limpieza.


Nuestra misión será limpiar de cuerpos, sangre y cualquier resto que pueda incriminar a la familia antes de que nos descubran, así que tendremos que usar todas nuestras habilidades para ocultarnos y realizar nuestro trabajo huyendo de la policía.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" frameborder="0" height="315" src="https://www.youtube.com/embed/l8W3g_l46es" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


El juego está de oferta hasta el 21 de julio, así que si os interesa ir a ritmo de disco a steam para haceros con él.


<div class="steam-iframe"><iframe frameborder="0" height="190" src="https://store.steampowered.com/widget/522210/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


Y tú, ¿te dejarás llevar por la sangre y el soul en este Serial Cleaner? Déjanos tu opinión en los comentarios, o a través de nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD).


 

