---
author: Serjor
category: "Acci\xF3n"
date: 2018-09-27 18:52:12
excerpt: <p>Un fin de semana para poner las espadas en alto</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/937d96879d6edd9b329e7774b441a1f4.webp
joomla_id: 860
joomla_url: blade-symphony-prepara-su-gran-actualizacion-con-una-beta-que-podremos-disfrutar-gratis-este-fin-de-semana
layout: post
tags:
- fin-de-semana
- blade-symphony
title: "Blade Symphony prepara su gran actualizaci\xF3n con una beta que podremos\
  \ disfrutar gratis este fin de semana"
---
Un fin de semana para poner las espadas en alto

Leemos en [phoronix](https://www.phoronix.com/scan.php?page=news_item&px=Blade-Symphony-HP-Linux) que Blade Symphony, ese juego de lucha de espadas del que os [hablamos](index.php/homepage/generos/accion/item/822-blade-symphony-llegara-a-mediados-de-abril-a-gnu-linux) hace unos meses, con motivo de una gran actualización llamada Harmony Prelude, en la que traen mejoras y correcciones interesantes en la rama beta del juego de cara a poder llevarla a la versión estable, será gratuito durante el fin de semana, y además estará a precio reducido (muy reducido de hecho en Steam), aunque en teoría el juego será F2P.


La verdad es que la lista de cambios entre versión candidata y versión candidata es enorme, la cuál podéis encontrar [aquí](https://steamcommunity.com/games/bladesymphony/announcements/detail/1690429528470518608), y como veréis, hay mejoras y correcciones de todos los tipos. Una que me ha llamada especialmente la atención ha sido que ahora mientras esperamos a jugar contra alguien podremos entretenernos jugando contra bots, un poco como ya pasa en otros juegos, en los que mientras estamos en la cola de espera podemos ir haciendo otras cosas en el juego, cosa que ameniza drásticamente la espera.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/4YMSe71sn5Y" style="border: 0px;" width="560"></iframe></div>


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/225600/27252/" style="border: 0px;" width="646"></iframe></div>


Ya sabes, aprovecha este fin de semana para probarlo y si te convence, no dudes en sacar filo a tu tarjeta de crédito o cuenta de PayPal, además de contarnos qué te parece el juego en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD)

