---
author: leillo1975
category: "An\xE1lisis"
date: 2020-09-23 17:45:31
excerpt: "<p>@funselektor ya ha lanzado la DLC gratuita \"Kenya\"<strong>.&nbsp;</strong><span\
  \ style=\"text-decoration: underline;\"></span></p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ArtOfRally/ArtOFRallyCartel.webp
joomla_id: 1258
joomla_url: disponible-art-of-rally-para-gnu-linux-a3
layout: post
tags:
- indie
- dlc
- analisis
- unity3d
- nativos
- art-of-rally
- funselektor-labs
title: Disponible "Art Of Rally" para GNU/Linux - ANALISIS (ACTUALIZADO 3)
---
@funselektor ya ha lanzado la DLC gratuita "Kenya"**.**


****ACTUALIZACIÓN 12-08-21**:**Tras varios meses después del anuncio, finalmente ya está disponible esta última actualización del juego que incluye la expansión Kenya de forma **totalmente gratuita**. Las [novedades](https://store.steampowered.com/news/app/550320/view/2993191480166772217) que incluye son las siguientes:


**Nuevas Características:**  
*6 nuevas etapas de rally en Kenia: monte kenya, karura, bahía homa, isla ndere, lago baringo, lago nakuru*  
 *1 nueva localización de freeroam*  
 *4 nuevos coches: gacela, el rey de África, das 559, la hiena*  
 *añadido dx12 en las opciones de lanzamiento del juego*


**Mejoras**


*cambiado el comportamiento del marcador de la carretera para que no reaccione con el coche después de ser golpeado*  
 *escalado dinámico de la resolución cuando se ejecuta el juego en DX12, que ha mostrado mejoras en el rendimiento*  
 *se ha cambiado el códec de los trailers del grupo de coches para mejorar la compatibilidad entre los distintos sistemas operativos*  
 *se ha añadido un mensaje de error al acceder a los eventos online si no se ha aceptado la política de privacidad*  
 *se ha evitado que los eventos online se oculten en caso de error de conexión*  
 *se ha añadido una barra de progreso al reproductor de vídeo*


**Se ha corregido**


*se ha corregido la actualización incorrecta de las letras de RALLY en la pantalla de carga de freeroam*  
 *Arreglada la casa flotante en Cerdeña 4*  
 *Arreglado el conductor de la IA que utiliza el coche del Grupo S en el Grupo B*  
 *Se ha corregido un error por el que al pulsar el botón de salto de canción durante la pantalla de inicio se reproducía una canción a todo volumen incluso cuando el volumen estaba ajustado al 0%.*  
 *Arreglado el texto de la clasificación de los eventos online que no respondía al modo oscuro*  
 *Se ha corregido un error por el que conducir alrededor de la línea de meta podía hacer que el jugador se reiniciara en una posición más allá de la línea de meta*  
 *Arreglado un tramo de carretera en el monte Asama que trataba incorrectamente el coche como si estuviera fuera de la carretera*  
 *Arreglado el tiempo de la etapa en la pantalla de finalización de la etapa que se leía como cero en el daño terminal*  
 *Arreglado el dibujo del cozzie sr71 que dibujaba partículas de transmisión incorrectas (estaba ajustado a AWD, debería haber sido RWD)*  
 *arreglado el colisionador de la barandilla en el monte asama*  
 *Arreglado el congelamiento al final de la etapa*  
 *Arreglado que los fantasmas aparezcan correctamente*  
 *Arreglado un error por el que la limpieza del coche se deshacía (visualmente) en un reinicio rápido*  
 *Se ha corregido que los tiempos fuera de línea no se tienen en cuenta para el mejor tiempo después de la etapa*  
 *Se ha corregido un error por el que el botón de reinicio de la etapa podía ser pulsado infinitamente*  
 *Se ha corregido que los marcadores de carretera no responden a las colisiones lentas con el coche.*


El desarrollador también nos adelanta que **la proxima localización será en un entorno tropical,** pero aun no ha especificado ni cual, ni si será también gratuita como esta de Kenya. Además de todos estos cambios, el juego ha sido lanzado también para consolas, reuniendo todo esto en este nuevo video:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/78Au-AW6frc" style="display: block; margin-left: auto; margin-right: auto;" title="YouTube video player" width="780"></iframe></div>


 




---


 **ACTUALIZACIÓN 13-04-21**:


Buff.... "semapasao". El caso es que hace unas semanas vi la noticia en algunos medios (no recuerdo cuales) y dije "Esto lo tengo que poner en la web", pero al final "por haches por bes", han pasado 3 semanas. Bueno, más vale tarde que nunca. El caso es que [Funselektor](http://funselektor.com/) publicó que este verano llegará de forma gratuita una [nueva actualización](https://store.steampowered.com/news/app/550320/view/3016822694916228619) de Kenia para art of rally, incluyendo 4 nuevos coches, 6 nuevas pistas y una nueva área de freeroam. Os dejamos con el trailer de esta dlc:


  
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/kE5o3kgW2N0" style="display: block; margin-left: auto; margin-right: auto;" title="YouTube video player" width="780"></iframe></div>


... y para no quedarnos cortos, un tweet con un video "gameplay" del juego en acción:



> 
> yes [pic.twitter.com/N4tr74ppKq](https://t.co/N4tr74ppKq)
> 
> 
> — dune (@funselektor) [April 9, 2021](https://twitter.com/funselektor/status/1380658442147221504?ref_src=twsrc%5Etfw)


  





 




---


**ACTUALIZACIÓN 14-12-20**:


Han pasado casi 3 mesecillos desde que se lanzó este estupendo juego de Rallys con perspectiva isométrica, y hoy se estrena una [actualización](https://store.steampowered.com/news/app/550320/view/2935748388713173589) importante que mejora bastante el juego añadiendo muchas nuevas características entre las que destacan:  
  
-**8 coches nuevos**:


grupo 2: le gorde, la regina  
grupo 3: la hepta, the pebble v1, the pebble v2, the zetto  
grupo 4: la super montaine, la longana


-**Coches fantasma**: Puedes ver tu propio fantasma para obtener los tiempos más rápidos!


-**Mods de librea**: Las libreas creadas por los jugadores serán descargadas de [Race Department](https://www.racedepartment.com/downloads/categories/art-of-rally-liveries.215/). Las libreas pueden ser creadas desde nuestras plantillas con un editor .psd (photoshop, gimp, etc.), y luego exportadas como un png para añadirlas a los archivos del juego.


-**Menú principal con polaroids:** Las fotos que hagas en el modo fotográfico se cargarán en las polaroids del menú principal, para que puedas revivir tus momentos favoritos cada vez que inicies el juego.


-**Más mejoras**: Los aterrizajes de salto rebotan mucho menos. Rotar los coches en el menú principal con el pulgar derecho en los mandos. Ver las tablas de clasificación diarias/semanales anteriores


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/psZnVDiWre4" style="display: block; margin-left: auto; margin-right: auto;" width="780"></iframe></div>




---


**NOTICIA/ANALISIS ORIGINAL**:


Ha pasado más de un año desde la primera vez que supimos de este juego, y desde entonces como todos sabeis [hemos ido siguiendo su desarrollo]({{ "/posts/habra-version-de-art-of-rally-para-nuestros-sistemas-a3" | absolute_url }}) con interés, y es que el juego a nuestro juicio lo merece. Desde el principio, el juego nos cautivó dejándonos "Ojiplaticos", como bien dijimos el día que conocimos la existencia de este trabajo del Canadiense [Funselektor](http://funselektor.com/), también conocido por su anterior juego [Absolute Drift](http://absolutedrift.com/). A medida que conocíamos más y más de él a lo largo de los meses, más se incrementaba nuestro interés por este título.


Hay algo que hace que "Art of Rally" te atrape desde la primera vez que lo ves, y es que tal y como reza su nombre, **el juego es una auténtica obra de arte a nivel visual**. Con ese estilo inconfundible "lowpoli", pero sin dejar atrás ningún detalle, **el juego es una delicia que entra por los ojos desde el principio**. En el no solo veremos unos escenarios cuidadísimos, sinó que rapidamente nos maravillaremos con la extensa colección de automóviles icónicos facilmente reconocibles por todos los fans de los coches.


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="420" src="https://www.youtube.com/embed/rQDq59aNRyU" style="display: block; margin-left: auto; margin-right: auto;" width="750"></iframe></div>


Pero el juego no solo destaca en lo visual, sinó que **cuenta con unas físicas muy trabajadas y realistas** que nos provocarán más de un quebradero de cabeza hasta que no dominemos la conducción. Notaremos no solo las diferencias existentes entre los muy diversos coches con los que contaremos, sinó también el agarre en los diferentes tipos de terreno, o si la meteorología no acompaña.


En cuanto a **vehículos**, encontraremos **más de 50** estando divididos en **6 Clases** (Grupo 2, Grupo 3, Grupo 4, Grupo B, Grupo S y Grupo A). En cada clase encontraremos varios coches, tales como el Mini, el Lancia Stratos, el Subaru Impreza, el Peugeot 205 o el clásico Ford Escort, **todos obviamente con pseudónimos** al obviamente no contar el juego con licencias oficiales.


Si hablamos de los **escenarios** nos sonarán de otros juegos de rally hiperconocidos, y estos serán muy diversos, con **diferentes tipos de terreno y clima**. Podremos visitar **Finlandia** (grava y asfalto), **Cerdeña** (grava Y asfalto), **Japón** (asfalto), **Noruega** (nieve, grava y asfalto) y **Alemania** (asfalto). Cada una de las etapas podrá ser jugada a **diferentes horas del día** (o noche), y con **condiciones climáticas diferentes** (despejado, niebla, lluvia o nieve).


![aor01](https://steamcdn-a.akamaihd.net/steam/apps/550320/ss_9e6de963c60552e484d87ebf720ce1f4cf40f441.600x338.jpg)


El juego cuenta además con varios **modos de juego**, entre los que encontraremos el **modo historia**, donde a lo largo del tiempo iremos sorteando diversas pruebas con coches y tramos diferentes hasta completar un total de **60 etapas** de la época dorada de los Rallys. También podremos competir contra el tiempo en el **modo Contrareloj**, pudiendo personalizar todos los elementos, como escenario, tramo, clase, coche y meteorología. En el **evento personalizado** podremos competir contra la IA poniendo a nuestro gusto las difentes opciones disponibles. No menos importante es la oportunidad de disputar contra el resto de jugadores de "Art of Rally" **eventos diarios y semanales** que nos permitirán medir nuestras habilidades contra ellos.


No podíamos dejar de hablar del **modo de conducción libre**, que a mi juicio es todo un acierto, pues no solo nos permite **recorrer las carreteras de las difentes localizaciones del juego a nuestro gusto,** sinó que además dispone de diversos **objetos y elementos que podremos coleccionar** hasta completarlos al 100% del escenario. Desconocemos si al conseguirlos todos pasa algo, pues aun no hemos tenido la oportunidad de jugar lo suficiente.


Hay que decir también que el juego cuenta con una **acertada banda sonora que se ajusta perfectamente al tipo de juego** que estamos disfrutando. En cuanto a los efectos de sonido, estos son correctos, aunque por ejemplo, los motores de los coches suenan un poco artificiales y enlatados, pero no sabemos si esto está hecho así a proposito.


A nivel técnico, hemos de decir que el juego, **desarrollado con el motor Unity**, nos permite controlar los coches con perspectiva isométrica, pudiendo acercar o alejar la cámara. También es posible girar la cámara alrededor del coche durante la condución. Podemos además usar el **modo foto** para inmortalizar nuestros coches en los paisajes más llamativos. También existe un **modo repetición** en el que podremos  por supuesto seguir la acción desde diferentes ángulos.


![aor02](https://steamcdn-a.akamaihd.net/steam/apps/550320/ss_c9c73e7439632d7ad885ccb5c8d8b412eecf3efe.600x338.jpg)


El juego se mueve con soltura y no se aprecian ni ralentizaciones ni tirones. Nosotros hemos podido jugarlo sin problema alguno con el **Steam Controller**, el mando de la **Xbox 36**0 y ¡el volante **Logitech G29**!, aunque hemos de decir que en el caso de este último, probablemente sea algo anecdótico, pues resulta bastante difícil conducir los coches con este tipo de controladores.


Los únicos **puntos negativos** que hemos podido encontrar en el juego son la **imposibilidad de competir contra otros jugadores a la vez a traves de la red o internet**, y no tener más variedad en las cámaras. Sobre el primer punto, se echa de menos el poder disputar una carrera contra un amigo, o realizar tramos simultaneos donde pudiesemos ver el coche (fantasma) de nuestro rival. En cuanto al tema de las cámaras, **estaría bien el poder poner una cámara trasera más cercana**, o por que no, una subjetiva. Aunque no es un defecto, también se podría decir que la dificultad inicial a la hora de controlar el coche puede desesperar a más de uno, pero quizás eso sea un aspecto que caracteriza el juego, y es que no se trata de un arcade para pasar el rato, pues como antes dijimos, las físicas son realistas.


Por lo demás, el trabajo de Funselektor no merece más que alabanzas, y tanto los que disfrutan de los juegos de coches, como los aficionados a los juegos indie, encontrarán un título que colmará de sobra todas sus espectativas, ofreciendo al jugador un producto cuidado en el que **rezuma la pasión y el cariño al mundo de los Rallyes por parte de su creador**. Art of Rally, a nuestro juicio da la jugador mucho más de lo que cuesta, y es por supuesto un nuevo clásico.


Podeis comprar Art of Rally con un **descuento de lanzamiento del 10%**, a **18.89€**,  en [Steam](https://store.steampowered.com/app/550320/art_of_rally/), [GOG](https://www.gog.com/game/art_of_rally) y en otra tienda de cuyo nombre preferimos no acordarnos. Os dejamos con el video que grabamos hace unos días donde podreis ver algunas de las cosas que aquí comentamos:
  
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="420" src="https://www.youtube.com/embed/yRc__5EhMIQ" style="display: block; margin-left: auto; margin-right: auto;" width="750"></iframe></div>

¿Alucinando con "Art of Rally"? ¿Qué os parece este nuevo trabajo de Funselektor? ¿Os gusta la estética de este juego? Cuéntanoslo en los comentarios o en nuestros grupos de JugandoEnLinux en [Telegram](https://twitter.com/JugandoenLinux) o  [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

