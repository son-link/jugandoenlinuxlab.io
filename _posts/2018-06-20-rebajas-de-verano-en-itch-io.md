---
author: Serjor
category: Ofertas
date: 2018-06-20 20:32:20
excerpt: <p>Las contraofertas llegan a los indies</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/f964ddc910acbc5c426350c1b152e08d.webp
joomla_id: 779
joomla_url: rebajas-de-verano-en-itch-io
layout: post
tags:
- rebajas
- itchio
title: Rebajas de verano en itch.io
---
Las contraofertas llegan a los indies

Vía twitter nos enteramos de que la tienda de juegos indies por excelencia, [itch.io](http://itch.io), ha lanzado sus ofertas de verano.



> 
> Worried you won't have enough to do this summer? We're happy to announce the <https://t.co/VrknuMinwO> Summer Sale! There are sitewide discounts on hundreds of games, books, and tools. Check out the discounts now! [#itchsummersale](https://twitter.com/hashtag/itchsummersale?src=hash&ref_src=twsrc%5Etfw) <https://t.co/1cisly86ir> [pic.twitter.com/rJAPLekXsS](https://t.co/rJAPLekXsS)
> 
> 
> — itch.io (@itchio) [19 de junio de 2018](https://twitter.com/itchio/status/1009118622151634944?ref_src=twsrc%5Etfw)


Entendemos que este movimiento viene para adelantarse a las ofertas de Steam, las cuales, en el momento de publicar este artículo, los medios no oficiales de Valve [pronosticaban](https://www.whenisthenextsteamsale.com/) para este 21 de junio.


La verdad es que itch.io es una tienda, que teniendo un cliente nativo para GNU/Linux, no está tan extendida como otras a la hora de buscar títulos indies, así que quizas estas rebajas sean una buena excusa para echar un vistazo.


Y tú, ¿has visto algún título interesante en [itch.io](http://itch.io)? Cuéntanoslo en los comentarios, en el foro de ofertas, o sino también en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD).

