---
author: leillo1975
category: Terror
date: 2018-11-14 12:26:48
excerpt: <p class="ProfileHeaderCard-screenname u-inlineBlock u-dir" dir="ltr"><span
  class="username u-dir" dir="ltr">Su creador @wadaholic nos presenta un Survival
  Horror excelente</span></p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/60468aa82473fd32700d48478784d483.webp
joomla_id: 902
joomla_url: conozcamos-total-chaos-un-mod-completo-de-doom-ii
layout: post
tags:
- mod
- survival-horror
- total-chaos
- wadaholic
- gzdoom
- doom-ii
title: Conozcamos "Total Chaos", un mod completo de DOOM II
---
Su creador @wadaholic nos presenta un Survival Horror excelente

Personalmente desconocía totalmente la existencia de este proyecto, pero gracias a un magnífico artículo de [LinuxAdictos](https://www.linuxadictos.com/total-chaos-un-mod-de-doom-ii-de-terror-y-supervivencia.html), hemos podido descubir esta **conversión total de DOOM II**. En el nos encontramos con un **Survival Horror** de muy buena factura, con unos gráficos excelentes, "a pesar" de tomar como base un motor gráfico tan antiguo, el juego muestra unas **texturas de alta resolución**, **modelos 3D** muy trabajados, **Motion Blur** o **godrays**. Todo esto es posible gracias a la utilización de [GZDOOM](https://zdoom.org/wiki/GZDoom), una **modificación completa y actualizada** de este vetusto motor. Teniendo en cuenta esto, el resultado es simplemente espectacular.


El juego recuerda enormente a otros títulos como **Resident Evil**, **Silent Hill**, **STALKER** o **Amnesia**, por lo que las referencias, como veis, son muy buenas. Nos encontramos ante un **mundo abierto** que podremos recorrer libremente y explorar a nuestro antojo, mientras nos encontramos con monstruos y demonios horripilantes. Para luchar con ello dispondremos de armas donde **la munición escasea**, por lo que tendremos que utilizar en más de una ocasión el **cuerpo a cuerpo** usando picas, hachas, cuchillas... que además podremos fabricar y mejorar para hacerlas más efectivas.


En [Total Chaos](https://wadaholic.wordpress.com/) tendremos que explorar la **isla de Fort Oasis**, a la que llegamos tras una tormenta en nuestro barco averiado. Nada más atracar en la isla recibimos unas inquietantes transmisiones de radio y esto nos lleva a investigar. El lugar es una **antígua colonia minera**, en la que sus habitantes desarparecieron repentinamente, dejando atrás una jungla de hormigón abandonada de apartamentos, túneles, centros comerciales... y por supuesto la mina de carbón. Como veis la historia promete grandes dosis de terror y por supuesto diversión.


El juego se lanzó la noche de Halloween (perdón por la tardanza) y  desde entonces se ha actualizado varias veces para incluir arreglos, mejoras y características nuevas, siendo la **última versión la 0.97.6**, de hoy mismo, y que podeis descargar gratuitamente desde [ModDB](https://www.moddb.com/mods/total-chaos/downloads/total-chaos-standalone-0976-latest). También, y si lo preferís teneis una versión en **Flatpak** un poco anterior (0.97.4), disponible en [Flathub](https://flathub.org/apps/details/com.moddb.TotalChaos). Os dejamos con el trailer del juego:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/L7IITZDBvqE" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


¿Qué os parece Total Chaos? ¿No os parece increible que tenga como motor el de DOOM II? Contéstanos en los comentarios o charla sobre ello en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).


 

