---
author: leillo1975
category: "Acci\xF3n"
date: 2017-03-30 22:25:00
excerpt: "<p>El conocido port es el primer t\xEDtulo en recibir este soporte por parte\
  \ de Feral Interactive</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/e9f3064a37460e22935d3df9e26e53bb.webp
joomla_id: 268
joomla_url: feral-da-soporte-vulkan-a-mad-max
layout: post
tags:
- feral-interactive
- beta
- vulkan
- mad-max
title: Feral da soporte Vulkan a Mad Max (Actualizado)
---
El conocido port es el primer título en recibir este soporte por parte de Feral Interactive

Perdonadme por que aún estoy en estado de shock. Hace unos meses escribía en esta web [mi primer análisis](index.php/homepage/analisis/item/102-analisis-mad-max) sobre el que es uno de mis ports favoritos de Feral. Hace un momento me quedaba de piedra al ver la noticia en [GamingOnLinux](https://www.gamingonlinux.com/articles/mad-max-meets-vulkan-in-a-new-fully-public-beta-for-linux-benchmarks-and-opengl-vs-vulkan-comparisons.9345) y twitter:


 



> 
> Linux gamers: we’re revving our Magnum Opus. The [@VulkanAPI](https://twitter.com/VulkanAPI) Beta of Mad Max has been unleashed.  
>   
> To join the Beta:<https://t.co/7yePr1yfdS> [pic.twitter.com/BnbHQzMPxE](https://t.co/BnbHQzMPxE)
> 
> 
> — Feral Interactive (@feralgames) [March 30, 2017](https://twitter.com/feralgames/status/847448634736099329)



 


En primer lugar es necesario aclarar que se trata de una **Beta pública** y que por lo tanto es susceptible de tener fallos. Para activarla debemos ir a Steam, y con el botón derecho del ratón pulsar en el nombre del juego y seleccionar propiedades. Después debemos pulsar la **pestaña Beta** y en el campo designado para ello introducir el código ***"livelongandprosper"*** . Después pulsaremos "Verificar Código" y en el desplegable seleccionaremos "vulkan_beta". Podremos desactivar el modo Vulkan desmarcando el campo en el lanzador de Feral cuando iniciamos el juego. Si por un casual, tenemos el lanzador de Feral deshabilitado, podemos volver a invocarlo pulsando Control mientras pulsamos el botón de jugar en Steam.


 


También es importante decir que Feral ha habilitado en **exclusiva para Linux** **un modo Bechmark** al que podremos acceder si en las propiedades del juego añadimos en "Definir Parámetros de lanzamiento" el siguiente modificador: **“--feral-benchmark”**. Para ver los resultados iremos a nuestra carpeta personal y abriremos los archivos .xml que estan en *”.local/share/feral-interactive/Mad Max/VFS/User/AppData/Roaming/WB Games/Mad Max/FeralBenchmark”*. Decir que el Bechmark contiene spoilers del juego, por lo que quedais avisados. En cuanto al desempeño mostrado por el juego con la nueva API hay que decir que, según lo que podemos ver en los tests realizados por GOL, el rendimiento es absolutamente brutal.


 


Si no teneis Mad Max, deciros que se trata de un Sandbox totalmente recomendable y uno de los mejores juegos AAA de los que dispone nuestro sistema. Si quereis comprarlo podeis pasaros por la **[tienda de Feral Interactive](https://store.feralinteractive.com/es/mac-linux-games/madmax/)** o por Steam:


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/234140/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


Iré actualizando la información de esta noticia con mis propias pruebas. También podeis dejar vuestras pruebas e impresiones en los comentarios, así como usar nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD) donde os estamos esperando.


 


**ACTUALIZACIÓN:** He estado realizando algunas pruebas en modo Benchmark en mi equipo y puedo decir, que aunque no tanto como en otras tarjetas superiores, si hay diferencia de rendimiento. Para ello he utilizado la resolución de 1920x1080 en calidad Muy Alta. Mi equipo es un Intel i5-3550 (3.3ghz x4) con 16GB DDR3, Nvidia GTX950-2GB en un Ubuntu 16.10 de 64 bits. Estos son los resultados obtenidos:


**OPENGL**




|  |  |  |  |  |
| --- | --- | --- | --- | --- |
| **FPS\TESTS** | **Camp - Hollow Point** | **Stronghold - Tyrant's Lash** | **Cutscene - Hope, Glory, and Dog is Dead** | **Cutscene - Landmover** |
| **Medios** |  32.80 |  22.90 |  40.80 | 47.62 |
| **Mínimos** |  18.78 |  12.70 |  4.81 |  5.12 |
| **Máximos** |  45.20 |  34.18 |  193.50 |  210.48 |


 


**VULKAN**




|  |  |  |  |  |
| --- | --- | --- | --- | --- |
| **FPS\TESTS** | **Camp - Hollow Point** | **Stronghold - Tyrant's Lash** | **Cutscene - Hope, Glory, and Dog is Dead** | **Cutscene - Landmover** |
| **Medios** |  55.59 |  44.39 |  53.41 |  63.68 |
| **Mínimos** |  48.72 |  31.23 |  6.70 | 9.83 |
| **Máximos** |  64.22 |  45.87 |  80.41 | 181.39 |


 


 


 


 Tengo que decir que en juego también va mucho más fluido que antes, **oscilando entre los 50 y los 70 FPS en calidad Muy Alta**, por lo que es completamente jugable al máximo con una tarjeta normalita como la mia.


 

