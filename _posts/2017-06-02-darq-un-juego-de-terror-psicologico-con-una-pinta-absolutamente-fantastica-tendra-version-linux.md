---
author: Pato
category: Terror
date: 2017-06-02 09:07:33
excerpt: "<p>El juego obra de Unfold Games est\xE1 anunciado para este mismo a\xF1\
  o</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/0e17febd522cd9389b04ce5c00f25aec.webp
joomla_id: 354
joomla_url: darq-un-juego-de-terror-psicologico-con-una-pinta-absolutamente-fantastica-tendra-version-linux
layout: post
tags:
- indie
- proximamente
- terror
- psicologico
title: "'DARQ' un juego de terror psicol\xF3gico con una pinta absolutamente fant\xE1\
  stica Tendr\xE1 versi\xF3n Linux"
---
El juego obra de Unfold Games está anunciado para este mismo año

Hoy mismo nos han llegado noticias del estudio "Unfold Games" que está trabajando en este más que interesante 'DARQ' [[web oficial](http://darqgame.com/)].


Se trata de un juego de terror psicológico donde viviremos dentro de los sueños de Lloyd, un chico que toma conciencia de que está durmiendo. Sin embargo, cada intento de despertar termina en fracaso, por lo que rápidamente todo se torna en una terrible pesadilla. Tendrá que explorar lo más profundo de su subconsciente, donde aprenderá a sobrevivir desafiando las leyes físicas dentro de su pesadilla y manipulando la fábrica de fluidos del mundo de sus sueños.


Lo primero que llama la atención dentro de 'DARQ' es el diseño artístico. La mezcla de entornos con una belleza absorvente en 3D de aspecto realista mezclados con los suaves y elegantes movimientos de los personajes 2D dibujados a mano hacen que la combinación sea absolutamente fantástica, consiguiendo una experiencia opresiva y aterradora a cada paso que da Lloyd. Juzga por ti mismo:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/tbXJxTAENoM" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


 


La luz dentro del juego tendrá un papel muy importante, al punto de que según los desarrolladores casi tanto como Lloyd, el protagonista dentro de DARQ será la iluminación y el tratamiento de luces y sombras. El juego mantendrá un nivel de tensión considerable, pues nunca sabremos qué puede suceder si tenemos la luz encendida, o si la apagamos.


En cuanto a la jugabilidad, Lloyd estará solo ante enemigos muy poderosos, por lo que enfrentarse a ellos no es una opción. Su mejor baza será el sigilo tratando de pasar inadvertido para todos los seres que se encuentre en su camino. Sin embargo los enemigos también tendrán sus sentidos puestos en detectar a Lloyd, por lo que la quietud, el sigilo y el tener la luz encendida o apagada será esencial para evitar ser detectado. Escuchar la respiración más o menos agitada indicará el estado de alerta y las mayores posibilidades de ser detectado.


El estudio hace énfasis en que el sonido dentro del juego será esencial, ya que habrán escenas donde el juego se desarrollará en la más absoluta obscuridad, por lo que escuchar los sonidos del entorno será imprescindible para que Lloyd pueda avanzar en su terrible aventura. Esto me recuerda a otros juegos en los que el sonido era esencial para tener una experiencia que cambiaba totalmente con un buen sistema de sonido haciéndola aún mas inmersiva (y aterradora... ¿alguien dijo Amnesia?).


'DARQ' llegará a Linux vía Steam en una fecha aún por determinar, aunque está anunciado para este mismo año. En Jugando en Linux estaremos atentos a las novedades que puedan surgir respecto a este juego.


¿Qué te parece la propuesta de 'DARQ'? No me negarás que parece absolutamente interesante ¿Te gustan los juegos de terror psicológico?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 

