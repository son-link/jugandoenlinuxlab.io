---
author: leillo1975
category: Software
date: 2019-05-14 20:33:24
excerpt: <p>La herramienta de Steam Play se actualiza con algunas mejoras.</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/3e04fb9a1a5c2d0e8729e590da1f436a.webp
joomla_id: 1045
joomla_url: nueva-version-de-proton-la-4-2-4
layout: post
tags:
- steam-play
- dxvk
- proton
- rage-2
title: "Nueva versi\xF3n de Proton, la 4.2-4."
---
La herramienta de Steam Play se actualiza con algunas mejoras.

No se por que me olía que una nueva versión de Proton estaba cerca.... bueno, no me las voy a dar de adivino porque realmente [la última salió hace más o menos un mes](index.php/homepage/generos/software/12-software/1146-nueva-version-de-proton-4-2-3), y es bastante normal que saquen nueva release. Cada vez que Valve mueve un dedo cruzamos los dedos y no es de extrañar, pues cada nueva lanzamiento aumenta la calidad y compatibilidad de Steam Play. En este caso los cambios no son de tanto calado como en anteriores ocasiones, pero como siempre merece la pena dedicarle un artículo, pues sabemos del interés que despierta. Como siempre **Pierre-Loup Griffais** lo ha anunciado en twitter:



> 
> Proton 4.2-4 is now out with a fix for RAGE 2, a new Vulkan-powered game that just got released on Steam today!
> 
> 
> — Pierre-Loup Griffais (@Plagman2) [May 14, 2019](https://twitter.com/Plagman2/status/1128391958047612928?ref_src=twsrc%5Etfw)







Tal y como podeis ver la principal novedad es un arreglo (requiere builds de desarrollo de MESA) para poder jugar al recien salido del horno **RAGE 2**, que se colgaba al arrancar. El [resto de las novedades](https://github.com/ValveSoftware/Proton/releases/tag/proton-4.2-4) podeis verlas aquí:


*-Actualizada **DXVK** a la versión 1.1.1.*  
*-Mejora el soporte de Vulkan para la nueva build de "**No Man's Sky**" Vulkan.*  
*-Mejores iconos en algunos gestores de ventanas.*  
*-Corrección ocasional del proceso atasco de Wine cuando se actualiza la versión de Proton.*  
*-Corrección de la detección del controlador para los juegos **Yakuza Kiwami y Telltale**.*  
*-Arreglo en la generación de terreno in **Space Engineers**.*  
*-Arreglado el fallo en el lanzamiento de **Flower***


Es una pena que no actualizasen **DXVK a la versión 1.2** que ayer mismo salió aunque eso implicase un pequeño retraso, pues tiene mejoras muy importantes en el rendimiento y la carga de GPU y CPU mejorando juegos como Quake Champions y Mirror's Edge Catalyst,  y múltiples correcciones como podeis ver en las [notas de lanzamiento](https://github.com/doitsujin/dxvk/releases/tag/v1.2). Esperemos pronto poder verlo includo en Proton.


Podeis comentar vuestras impresiones y reportar los tests que hayais hecho con esta versión en los comentarios, o charlar sobre ello en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

