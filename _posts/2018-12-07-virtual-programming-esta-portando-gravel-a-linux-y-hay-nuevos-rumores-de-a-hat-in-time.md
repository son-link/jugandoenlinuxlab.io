---
author: Pato
category: Carreras
date: 2018-12-07 11:00:18
excerpt: <p>Parece que la Milestone sigue confiando en Virtual Programming @virtualprog
  para traernos sus proyectos</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/ebf6e59d749d99dbc015d81db8a7d241.webp
joomla_id: 923
joomla_url: virtual-programming-esta-portando-gravel-a-linux-y-hay-nuevos-rumores-de-a-hat-in-time
layout: post
tags:
- carreras
- proximamente
- virtual-progamming
title: "Virtual Programming est\xE1 portando Gravel a Linux y hay nuevos rumores de\
  \ 'A Hat in Time'"
---
Parece que la Milestone sigue confiando en Virtual Programming @virtualprog para traernos sus proyectos

Después de traernos el MXGP3, nos llegan noticias desde [gamingonlinux.com](https://www.gamingonlinux.com/articles/looks-like-both-a-hat-in-time-and-gravel-are-coming-to-linux-ports-from-virtual-programming.13114) de que Virtual Programming sigue ampliando su catálogo de juegos portados a nuestro sistema favorito junto al estudio Milestone.


En este caso se trata de **Gravel**, un juego de carreras de coches todoterreno que nos plantea recorrer el mundo para correr en las pistas mas extremas realizando locas acrobacias, y del que [ya os informamos](index.php/homepage/generos/carreras/2-carreras/1002-gravel-podria-llegar-a-linux-en-un-futuro) en su momento de que habían posibilidades de que acabase siendo portado. Ahora, tras la aparición del proyecto en la web de Virtual Programming ya podemos hacerlo oficial.


*¡Gravel es el juego de carreras más extremo, que te permitirá disfrutar con increíbles acrobacias en los lugares más salvajes del planeta!  
Pura diversión, paisajes espectaculares y una competición sin cuartel donde cada carrera resulta una batalla memorable.*


*<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/7tMvVcnRpNg" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>  
Participa en el espectáculo más delirante de la televisión por Internet en Gravel Channel, viaja por todo el mundo descubriendo una amplia variedad de entornos y déjate asombrar por la impresionante calidad visual del juego.   
Disfruta con cuatro disciplinas diferentes (Cross Country, Wild Rush, Stadium y Speed Cross) para vivir una experiencia de conducción con los vehículos más extremos en todo tipo de terreno.*


Puedes ver toda la información de Gravel en su [página web oficial](http://gravelvideogame.com/).


Por otra parte, gamingonlinux se ha hecho eco de una nueva aparición en la web de Virtual Programming de **una imagen de "A Hat in Time"**, el notable videojuego de plataformas del que ya hubieron rumores de desarrollo para Linux el verano pasado, sin embargo hoy hemos intentado encontrar alguna referencia o la imagen que ayer encontraron los chicos de gamingonlinux y nos ha sido imposible encontrar nada.


En cualquier caso, si al final Virtual Programming nos acaba trayendo A Hat in Time a Linux/SteamOS serán muy buenas noticias. ¿no crees?


Cuéntame lo que piensas en los comentarios o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

