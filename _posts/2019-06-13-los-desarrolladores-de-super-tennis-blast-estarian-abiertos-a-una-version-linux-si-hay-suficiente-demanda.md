---
author: Pato
category: Deportes
date: 2019-06-13 18:33:11
excerpt: "<p>El juego de <span class=\"css-901oao css-16my406 r-1qd0xha r-ad9z0x r-bcqeeo\
  \ r-qvutc0\" dir=\"auto\">@UnfinishedPixel</span> es una mezcla de tenis desenfadado\
  \ con sabor a los juegos cl\xE1sicos del g\xE9nero</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/e412d8c340fee3729deb11bde45f02dd.webp
joomla_id: 1066
joomla_url: los-desarrolladores-de-super-tennis-blast-estarian-abiertos-a-una-version-linux-si-hay-suficiente-demanda
layout: post
tags:
- deportes
- indie
- arcade
- tenis
title: "Los desarrolladores de Super Tennis Blast estar\xEDan abiertos a una versi\xF3\
  n Linux si hay suficiente demanda"
---
El juego de @UnfinishedPixel es una mezcla de tenis desenfadado con sabor a los juegos clásicos del género

Dentro del ecosistema de juegos que manejmos en Linux, si hay un género que no tiene exponentes es el tenis y del que yo particularmente siempre he sido seguidor. Sin embargo hace ya mucho tiempo que no tenemos ni una sola novedad tenística que llevarnos a Linux. Hoy dándo la habitual vuelta por Steam me he topado con un juego de tenis que enseguida me ha llamado la atención, se trata de Super Tennis Blast, un juego de tenis con un aspecto desenfadado y fresco que pretende traer la jugabilidad de los clásicos del género. Viendo que no tiene anunciada versión para Linux he buscado en los foros por el típico post de anuncios o peticiones, y aunque en él los desarrolladores dicen no tener planeado realizar un port a nuestro sistema favorito, no cierran la puerta a realizarlo si hubiera la suficiente demanda.


Así que, ¿por que no intentarlo?... **Super Tennis Blast** es un juego de tenis desenfadado con una estética "cartoon" muy llamativa:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/DSVAam2ZHLw" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


*Golpea la pelota y pásalo en grande con el mejor tennis!*  
  
*Comienza tu carrera profesional en el modo World Tour, entrena tus habilidades y conviértete en una leyenda del tenis jugando en pistas alrededor del mundo.*  
  
*Reúne a tus amigos en el sofá jugando torneos individuales o a dobles con las reglas clásicas o diviértete con los increíbles modos Super Blast.*  
  
*5 minijuegos y un completo editor de avatares para conseguir la mejor experiencia de tenis hasta ahora.*


Si te gusta el género y te atrae Super Tennis Blast, puedes pasarte por [el hilo de la petición](https://steamcommunity.com/app/1050510/discussions/0/1649918058737209604/) y mostrar tu interés al desarrollador, y sobre todo no olvides de [añadirlo a tu lista de deseados de Steam](https://store.steampowered.com/app/1050510/Super_Tennis_Blast/), ya que es en lo que más se suelen fijar los desarrolladores a la hora de ver las tendencias de sus potenciales clientes.


¿Qué te parece Super Tennis Blast? ¿Te gustaría que llegase a Linux? ¿Lo añadirás a tu lista de deseados?


Cuéntamelo en los comentarios o en el cana de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux).

