---
author: yodefuensa
category: "Acci\xF3n"
date: 2018-07-19 12:22:54
excerpt: "<p>Consecuencia de que 'The Culling 2' no ha tenido el \xE9xito esperado</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/ed7b17da6cf518a8c679fcad36d4a22c.webp
joomla_id: 805
joomla_url: the-culling-pasa-a-ser-free-to-play
layout: post
tags:
- accion
- battle-royale
title: The Culling pasa a ser "free to play"
---
Consecuencia de que 'The Culling 2' no ha tenido el éxito esperado

Parece ser que finalmente tendremos un Battle Royale f2p en Linux. Aunque para que eso se diera, merece la pena comentar como hemos llegado hasta aquí.


The Culling 2 fue lanzado por sorpresa la semana pasada, (para Windows, ps4 y xbox). El cual ha sido un rotundo fracaso para Xaviant, el equipo del juego. Desde que se puso a la venta, el máximo de jugadores de The Culling 2 ha estado en 249 jugadores simultáneos. En los dos siguientes días ese máximo ha estado en 5 personas conectadas. Imposible jugar así.


Por lo tanto, la decisión de la desarrolladora ha sido retirar el juego. Todos aquellos que decidieron darle una oportunidad al título verán su dinero reembolsado.


Josh Van Veld, director de operaciones de Xaviant, se disculpó con la comunidad, Van Veld admite que *The Culling 2* no era el juego que la comunidad pedía y que no logró convertirse en un digno sucesor del original. Además, no escucharon los comentarios de los jugadores con el primero y en su lugar decidieron crear una secuela completamente nueva. 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/24rR69Ygn20" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Para quien no lo sepa, el primer juego, "The Culling" fué un Battle Royale, que gozó de cierto éxito por ser pionero, y por estar centrado en el combate cuerpo a cuerpo. 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/KnNHBYZGo4s" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Por esta razón han anunciado que regresan al desarrollo del primer The Culling, centrándose en su versión Day One como punto de partida para trabajar en mejoras y nuevos contenidos. Además el título pasará a ser gratuito para todo el mundo y a lo largo de esta semana se abrirán las puertas de los servidores de prueba.


Lo último que nos queda será dar una oportunidad al juego. Puedes encontrarlo en:


<https://store.steampowered.com/app/437220/The_Culling/>

