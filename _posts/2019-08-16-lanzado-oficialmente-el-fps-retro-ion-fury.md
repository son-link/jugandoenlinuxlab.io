---
author: leillo1975
category: "Acci\xF3n"
date: 2019-08-16 09:47:04
excerpt: <p>El juego de <span class="username u-dir" dir="ltr">@voidpnt ha salido
  finalmente del Acceso Anticipado.</span></p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/1e05225eed30502e1f4540fba2cfd9c6.webp
joomla_id: 1099
joomla_url: lanzado-oficialmente-el-fps-retro-ion-fury
layout: post
tags:
- retro
- fps
- 3d-realms
- 1c-entertaiment
- ion-fury
- ion-maiden
- void-point
title: Lanzado oficialmente el FPS Retro "Ion Fury".
---
El juego de @voidpnt ha salido finalmente del Acceso Anticipado.

Tras [más de un año desde que el juego nos llegase anticipadamente](index.php/homepage/generos/accion/5-accion/786-3d-realms-vuelve-ion-maiden-llega-a-gnu-linux-steamos-en-acceso-anticipado) a través de un Early Access, [Ion Fury](http://www.ionfury.com/) ya está disponible en su versión definitiva, no sin algunos **retrasos y algunos inconvenientes** como la [amenaza de demanda de del grupo Iron Maiden](index.php/homepage/generos/accion/5-accion/1197-ion-maiden-cambia-su-nombre-por-ion-fury-y-anuncia-la-fecha-de-lanzamiento-oficial) hace algunos meses, lo cual acabó desembocando en su cambio de nombre (antes "Ion Maiden"). Ayer, finalmente, el juego se anunciaba oficialmente en Steam, tal y como sus desarrolladores habían prometido hace un mes, y como podíamos ver anunciado en la cuenta de twitter de 3D Realms:



> 
> ION FURY IS AVAILABLE NOW!<https://t.co/Zsh40rjuvX>  
>   
> "One of the best first-person shooters we've ever played!" - Digital Foundry  
>   
> “A fantastic, timeless, FPS experience”  
> 9/10 - Game Informer  
>   
> “Probably the best BUILD Engine game ever”  
> 9/10 - PCGamesN[@voidpnt](https://twitter.com/voidpnt?ref_src=twsrc%5Etfw) [@1C_Company](https://twitter.com/1C_Company?ref_src=twsrc%5Etfw) [pic.twitter.com/l1RncECKpv](https://t.co/l1RncECKpv)
> 
> 
> — ION FURY IS AVAILABLE NOW! (@3DRealms) [15 de agosto de 2019](https://twitter.com/3DRealms/status/1161991709301100544?ref_src=twsrc%5Etfw)


  





Si a estas alturas aun no conoceis el juego , debeis saber que se trata de un **Shooter en primera persona** desarrollado con el mítico **motor "[Build](https://en.wikipedia.org/wiki/Build_(game_engine))"**, responsable de clasicazos del género en los 90` como los incombustibles **Duke Nukem**, **Shadow Warrior** o **Blood**... y siguiendo esas premisas, eso es casualmente lo que nos vamos a encontrar, pero con resoluciones a actuales, controles actualizados y multitud de mejoras encaminadas a darle una mejor jugabilidad. El juego ha sido publicado por la mítica [3D Realms](https://3drealms.com/) y [1C Entertaiment](http://www.1cpublishing.eu/), y llegará en unos meses a consolas, pero como veis en esta ocasión los amantes del pingüino no tendremos que esperar. Según la descripción oficial, en Ion Fury encontraremos:


*""A pesar de la ley marcial permanente, el criminal Neo D.C. está bajo ataque: El Dr. Jadus Heskel y su ejército de cultistas cibernéticos dejan destrucción a su paso. El cabo [Shelly "Bombshell" Harrison](https://store.steampowered.com/app/353190/Bombshell/), líder de la Fuerza de Defensa Global, se detiene para emborracharse en un club nocturno cuando una explosión interrumpe su trago. Igual de furiosa que malhablada, Shelly se dispone a derribar a Heskel.*


*Corre a través de una ciudad cyberpunk llena de las horribles creaciones de Heskel y erradica a todos y a todo a la manera de Shelly. Lleva su siempre fiel revólver de 18 balas y tres cañones, Loverboy, y agáchate y zigzaguea por las calles. Pinta la ciudad de rojo con subfusiles Penetrator de doble disparo, realiza trucos con bombas de bolos, causa estragos con el todopoderoso Ion Bow o nivela el bloque con Clusterpucks.*


*Cada nivel abierto esconde muchos secretos, así que entre la reducción de enemigos a gibs n' gore, la búsqueda de munición, nuevos armamentos mortales y armamento extra para hacer la próxima pelea un poco más fácil. Los niveles laberínticos fomentan la exploración, así que mantén los ojos abiertos mientras buscas la siguiente tarjeta, y disfruta de la gloriosa bondad de los píxeles.*


*Construido para ser un tirador retro en lugar de simplemente emular lo que los hacía grandes, Ion Fury es alimentado por el mismo motor Build que impulsaba clásicos como Duke Nukem 3D, Blood and Shadow Warrior. Ion Fury ofrece una acción de ritmo rápido fusionada con toques modernos tales como fotos, auto-guarda, física moderna y soporte de pantalla ancha. Los ciber-cultistas instigados nunca se han visto tan satisfactoriamente.... salpicados.*


*Además de la campaña, Ion Fury viene con Mapster32, una versión actualizada y modernizada del editor de niveles integrado que se ofrece con Duke Nukem 3D en su día. Forja cualquier cosa, desde salas de matanza con munición ilimitada hasta pasillos retorcidos llenos de secretos. ¿Tienes un mapa favorito de los días del Duque y del Guerrero de las Sombras? Cárgalo en Ion Fury y disfruta de las modernas adiciones introducidas por Voidpoint. Coloca los niveles directamente en el Taller Steam para que todos puedan disfrutarlos.*


*"Por primera vez en más de 20 años, 3D Realms ha vuelto a sus raíces, haciendo lo que mejor sabemos hacer. Nuestra comunidad ha sido tan paciente y emocionada por Ion Fury", dijo Frederik Schreiber, vicepresidente de 3D Realms. "Estamos muy contentos de traer a Ion Fury a nuestros fans para que puedan experimentar una nueva visión de la destrucción de los clásicos'90s por ellos mismos."""*


Como os podeis imaginar, no necesitareis una máquina muy potente para mover el juego y podreis jugarlo en casi cualquier equipo, tal y como podeis ver aquí:


**Mínimo:**  

+ Requiere un procesador y un sistema operativo de 64 bits
+ **SO:** Ubuntu 14.04 or Steam OS 2.0
+ **Procesador:** Any 64-bit Intel or AMD CPU
+ **Memoria:** 1024 MB de RAM
+ **Gráficos:** 512 MB video memory. Intel integrated graphics supported.
+ **Almacenamiento:** 100 MB de espacio disponible


También nos gustaría anunciaros que **en unos días realizararemos un Stream o Video** jugando a esta versión definitiva para que tengais una idea de las novedades que incluye esta versión final , tal y como hicimos anteriormente en otras ocasiones ([video1](https://youtu.be/4OpSjWC4anw), [video2](https://youtu.be/Rc3bJ1WKgFI) y [video3](https://youtu.be/HxrTCTFJANY)). Mientras tanto os dejamos con su fantástico trailer de lanzamiento :


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/ncC9jrCqgG8" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


Podeis comprar "Ion Fury" por tan solo 20.99€ en [Steam,](https://store.steampowered.com/app/562860/Ion_Fury/) [GOG.com](https://www.gog.com/game/ion_fury) y la tienda de [3D Realms](https://3drealms.com/catalog/ion-maiden_56/).... y vosotros, ¿habeis jugado ya a Ion Fury? ¿Qué os parecen los shooters de los 90? ¿Disfrutais de los juegos retro? Cuéntanoslo en los comentarios o deja tus mensajes en nuestra comunidad de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).


