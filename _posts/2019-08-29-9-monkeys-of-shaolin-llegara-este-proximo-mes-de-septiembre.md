---
author: Pato
category: "Acci\xF3n"
date: 2019-08-29 15:38:24
excerpt: <p><span class="css-901oao css-16my406 r-1qd0xha r-ad9z0x r-bcqeeo r-qvutc0">@KochMediaUK</span>
  lo ha anunciado pero sin fecha concreta</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/2d3adfa8352a656a606912bf6acf7119.webp
joomla_id: 1103
joomla_url: 9-monkeys-of-shaolin-llegara-este-proximo-mes-de-septiembre
layout: post
tags:
- accion
- indie
- aventura
- beat-em-up
title: "9 Monkeys of Shaolin llegar\xE1 este pr\xF3ximo mes de Septiembre"
---
@KochMediaUK lo ha anunciado pero sin fecha concreta

Salimos del periodo vacacional y comenzamos a recibir noticias sobre próximos lanzamientos. En este caso ha sido la cuenta de Koch Media UK la que ha desvelado que el juego de acción y aventuras 9 Monkeys of Shaolin llegará este próximo mes de Septiembre, eso sí sin confirmar una fecha concreta.



> 
> A rebirth of the beat 'em up genre in vein of old-school video games! As a Chinese fisherman you have to avenge the death of your friends and family killed in a pirate raid.[#9MonkeysOfShaolin](https://twitter.com/hashtag/9MonkeysOfShaolin?src=hash&ref_src=twsrc%5Etfw) launches this September on PS4, XB1, PC and Switch.  
>   
> Trailer: <https://t.co/Jnls7Obtf0> [pic.twitter.com/3wr4wod0aU](https://t.co/3wr4wod0aU)
> 
> 
> — Koch Media UK (@KochMediaUK) [August 29, 2019](https://twitter.com/KochMediaUK/status/1167025103869231104?ref_src=twsrc%5Etfw)



*9 Monkeys of Shaolin marca el verdadero renacer del género icónico de lucha del mismo estilo que los videojuegos de la vieja escuela. Si cuando eras niño te divertiste durante horas jugando a los videojuegos de peleas SNES o SEGA derrotando a enemigos por doquier, entonces este nuevo título de los creadores del violento REDEMEER, ¡es perfecto para ti!*


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/rQ9ulnVAjXw" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Si quieres saber más detalles sobre el juego, puedes visitar su [web oficial](http://9monkeysofshaolin.com/) o su página de [Steam](http://store.steampowered.com/app/739080/).


¿Qué te parece '9 Monkeys of Shaolin'? ¿Te gustan los Shoot'em Ups de la vieja escuela?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

