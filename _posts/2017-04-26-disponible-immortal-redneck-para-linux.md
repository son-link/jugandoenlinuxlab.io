---
author: leillo1975
category: "Acci\xF3n"
date: 2017-04-26 19:34:00
excerpt: "<p>El estudio madrile\xF1o Crema Games ha lanzado el juego en Steam</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/832731af5d81b95ba82de72993209600.webp
joomla_id: 299
joomla_url: disponible-immortal-redneck-para-linux
layout: post
tags:
- steam
- gog
- crema-games
- immortal-redneck
- dia-1
- espana
title: Disponible Immortal Redneck para Linux (ACTUALIZADO)
---
El estudio madrileño Crema Games ha lanzado el juego en Steam

**ACTUALIZACIÓN 23-8-17:** Acabo de leer en [GOL](https://www.gamingonlinux.com/articles/immortal-redneck-the-fps-set-in-egypt-has-arrived-on-gog.10212) que el juego ha sido lanzado finalmente en [GOG](https://www.gog.com/game/immortal_redneck), por lo que teneis otra opción más para haceros con el juego, especialmente a los que no les guste el DRM de Steam. Podeis ver el Tweet anunciandolo aquí:


 



> 
> Immortal Redneck is finally available on [@GOGcom](https://twitter.com/GOGcom) hell yeah! Grab it now with a 20% discount<https://t.co/h4lsPZ6YX2>
> 
> 
> — Immortal Redneck (@ImmortalRedneck) [August 22, 2017](https://twitter.com/ImmortalRedneck/status/899988272881053698)



 


A veces el quehacer diario hace que te olvides de las ocasiones importantes. En este caso, el ajetreo del día de ayer hizo que no recordase que este proyecto vería por fin la luz en esa fecha, por lo que, en primer lugar os pedimos disculpas por no haberos informado antes. A principios de año le dedicábamos [un artículo en nuestra web](index.php/homepage/generos/accion/item/292-immortal-redneck-de-los-madrilenos-crema-games-esta-en-campana-en-greenlight) cuando estaban buscando la aprobación en Greenlight para incluir su trabajo en Steam. Finalmente [**Crema Games**](http://cremagames.com/) pudieron cumplir su objetivo y hacer realidad la publicación de su título.


 


¿Y por que tanto revuelo?, porque el juego realmente lo merece y me parece una propuesta muy digna y original **mezclando varios géneros** que luego detallaremos. Además algo muy importante, y es que se trata de un **trabajo realizado en nuestro pais**, por lo que es justo que tengan todo nuestro apoyo desde los medios nacionales, a los que pertenecemos. Por supuesto, digno de mención y de agradecer es el **soporte para nuestro sistema desde el primer día**; lo cual, aunque cada vez es más habitual, aun sigue siendo poco frecuente.


 


![Immortal Redneck](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/InmortalRedneck.webp)


 


[Immortal Redneck](http://www.immortalredneck.com/) es un juego de acción ambientado en el el antiguo Egipto que enseguida nos recordará a **Serious Sam**, pero con toques importantes de aventura, roguelike, e incluso RPG, ya que dispone de un arbol de habilidades donde poder mejorar nuestro personaje. Con estas premisas puede parecer una propuesta arriesgada, pero según se puede leer por la red el juego resulta tremendamente divertido y estos elementos se complementan perfectamente en lugar de chocar unos con otros con un resultado notable.


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/VGyvt1Vk5bs" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


Nosotros en JugandoEnLinux.com estamos deseando tener la oportunidad de ofreceros más información sobre este juego y sus creadores, y si es posible un completo análisis para que os hagais una idea más aproximada de lo que nos ofrece este título. Podeis comprar Immortal Redneck con un 10% como oferta de lanzamiento en su página de la tienda de Steam:


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/595140/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


¿Qué te parece este título? Dejanos tus impresiones en los comentarios de este artículo o en los canales de Jugando en Linux en [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

