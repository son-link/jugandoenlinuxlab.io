---
author: leillo1975
category: Software
date: 2018-08-22 10:28:39
excerpt: <p>Continuan las buenas noticias</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/48e356cffadcfae02e8c0dc7e82338a0.webp
joomla_id: 840
joomla_url: nvidia-mejora-de-rendimiento-en-nuevo-driver-y-ray-tracing-en-vulkan
layout: post
tags:
- nvidia
- driver
- rtx
- ray-tracing
title: 'Nvidia: Mejora de rendimiento en nuevo driver y Ray Tracing en Vulkan.'
---
Continuan las buenas noticias

Bufff... menudo día. Primero Valve nos da la noticia del año y justo después llega NVIDIA con novedades a pares. Como suele ser habitual, la magnifica [Phoronix](https://www.phoronix.com) ha sido la encargada de que nos enterásemos de estos dos temas relaccionados con la multinacional afincada en Santa Clara, California. Sin más dilación empecemos con la primera:


**La nueva versión del driver 396.54 trae importantes mejoras en el rendimiento de los juegos**, según han podido comprobar con diversos tests en [Phoronix](https://www.phoronix.com/scan.php?page=article&item=nvidia-39654-linux&num=1). En principio el driver fué lanzado para corregir una fuga de recursos existente desde la versión 390 que se producía después de que varias aplicaciones OpenGL o Vulkan se hubiesen iniciado. Pero tras diversos benchmarks se ha comprobado que el rendimiento en muchos de los juegos más populares se había incrementado bastante. Por lo que se puede ver en los tests, Deus Ex Mankind Divided, Dirt Rally, F1 2017, Rise of The Tomb Raider o Thrones of Britannia, entre otros han ganado en frames por segundo de forma notable, por lo que conviene actualizar lo antes posible. Para los usuarios de Ubuntu se supone que estará disponible en las próximas horas o días a través del [repositorio oficial de drivers de Nvidia](index.php/foro/drivers-graficas-nvidia/19-instalar-el-ultimo-driver-propietario-de-nvidia-en-ubuntu-y-derivadas).


En segundo lugar, y no menos importante, es que **NVIDIA dará soporte al Ray Tracing en sus tarjetas RTX en Linux,** gracias, como no a la API de Vulkan. Según comentan en la [noticia original](https://www.phoronix.com/scan.php?page=news_item&px=NVIDIA-RTX-Vulkan-WIP), se está trabajando desde Mayo en incorporar el soporte de esta nueva tecnología al driver de Linux, al igual que se ha hecho en Windows con DirectX 12 . En teoría debería estar disponible a partir del 20 de Septiembre a través de una nueva versión de los controladores, pero no se sabe cuando realmente podremos ver los primeros "rayos" en Linux.


Otro tema será si está tecnología será facil de implementar en los juegos de Linux, pues en caso de que sean completamente nativos si sería posible a través de Unity o UE4, ya que estos dos motores lo incluyen. Por otro lado, ¿tendrían los ports realizados por terceras empresas como Feral, Aspyr o Virtual Programing la capacidad de "traducir" DirectX12 a Vulkan de una forma adecuada?. También habrá que ver que tal se llevará Wine (o Proton) con el Ray Tracing para incorporarlo a los juegos a través del [recién estrenado Steam Play](index.php/homepage/generos/software/item/960-steam-play-de-valve-da-un-salto-de-gigante-y-amplia-enormemente-el-catalogo-para-linux-steamos).


Para los que no sepais que es esto del Ray Tracing, se trata de una tecnología que incorporarán las nuevas gráficas de Nvidia que permite realizar los cálculos en tiempo real para trazar rayos, reflejos, etc. Pero lo mejor es que lo veais en este video de Star Wars que lleva circulando por la red desde hace algún tiempo:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/YWcawaa_9HA" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


Supongo que estareis deseando experimentar las mejoras de rendimiento con el nuevo driver y en un futuro disfrutar de gráficos más realistas y espectaculares gracias al Ray Tracing, ¿no? Déjanos tu opinión sobre estos temas en los comentarios o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

