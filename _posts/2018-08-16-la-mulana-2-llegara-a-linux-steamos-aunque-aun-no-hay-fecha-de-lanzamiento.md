---
author: Pato
category: Aventuras
date: 2018-08-16 09:51:04
excerpt: <p>El juego de @NIGORO fue lanzado para otros sistemas el pasado 30 de Julio</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/59bbfcb2e1844b9599ff015000348c7d.webp
joomla_id: 833
joomla_url: la-mulana-2-llegara-a-linux-steamos-aunque-aun-no-hay-fecha-de-lanzamiento
layout: post
tags:
- accion
- indie
- aventura
- proximamente
- metroidvania
title: "'La-Mulana 2' llegar\xE1 a Linux/SteamOS aunque a\xFAn no hay fecha de lanzamiento"
---
El juego de @NIGORO fue lanzado para otros sistemas el pasado 30 de Julio

Uno de los indies que ha estado dando que hablar ha sido la secuela 'La-Mulana 2', segunda entrega de la saga desarrollada por el estudio japonés NIGORO. Tras el éxito del primer título, el pasado 30 de Julio llegó esta secuela para sistemas Windows y Mac, pero al contrario que el primer La-Mulana no habían referencias al lanzamiento en nuestro sistema favorito.


Algunos usuarios abrieron un hilo en los foros de Steam donde preguntaron por la posible versión para Linux/SteamOS del juego y ahora el estudio ha confirmado que nuestra versión está planificada, solo que no llegaron a tiempo para el lanzamiento del juego:



> 
> *Hi, the linux version is planned, but it wasn't ready in time for the release. We are hoping to get it out in the future.*
> 
> 
> *Hola, la versión de Linux está planeada, pero no estaba lista para el lanzamiento. Esperamos lanzarla en el futuro.*
> 
> 
> 


Puedes ver el [comentario](https://steamcommunity.com/app/835430/discussions/0/2828702372997150077/?tscn=1534267409#c1736588252367401500) y la conversación en el hilo del foro de Steam en [este enlace](https://steamcommunity.com/app/835430/discussions/0/2828702372997150077/?tscn=1534267409).


***Sinopsis:***


*La-Mulana 2 presenta ruinas antiguas en expansión, misterios alucinantes, innumerables objetos y terribles enemigos que se combinan para formar lo que posiblemente sea el perfecto "juego de acción de exploración de ruina arqueológica" de Metroidvania.  
  
En La-Mulana 2, asumes el papel de Lumisa Kosugi, hija del héroe del título anterior, mientras explora las antiguas ruinas de La-Mulana, considerada la cuna de la civilización humana. Desesperado por encontrar la causa de las numerosas apariciones recientes de monstruos de las ruinas, Lumisa se dirige al "otro" La-Mulana: las ruinas conocidas como Eg-Lana.*


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/5E3HKRRfcWU" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


La-Mulana 2 se define como un metroidvania no lineal donde la exploración es fundamental y el objetivo es descifrar los enigmas dispersos a lo largo del juego y resolver los acertijos que existen como parte de las ruinas.


* Conviértete en un aventurero y explora antiguas ruinas
* Consigue pistas para resolver los diversos misterios de las ruinas
* Sumérgete en la acción mientras luchas contra enemigos duros que intentan obstaculizar tu progreso
* Un mundo detallado y bellamente renderizado
* Siete armas principales y más de diez armas secundarias
* Más de 60 elementos de varios tipos para ayudarte a explorar las ruinas
* Más de 20 aplicaciones para instalar en la tableta de su fiel aventurero, el "Mobile Super X3"
* Incluye libro de referencia con más de 200 monstruos y otros personajes en el juego


 Si quieres saber más sobre 'La-Mulana 2' puedes visitar su [página web oficial](https://la-mulana.com/en/l2/index.php) o su [página de Steam](https://store.steampowered.com/app/835430/LaMulana_2/) donde estará disponible para Linux/SteamOS en un futuro.

