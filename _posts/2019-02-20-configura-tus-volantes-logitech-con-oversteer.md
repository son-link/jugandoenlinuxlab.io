---
author: leillo1975
category: Software
date: 2019-02-20 19:47:38
excerpt: "<p>Ya tenemos otra herramienta m\xE1s para sacar m\xE1s partido a estos\
  \ perif\xE9ricos.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/5bcbc2e6adc6ac9dfb690a3edcb64ab2.webp
joomla_id: 976
joomla_url: configura-tus-volantes-logitech-con-oversteer
layout: post
tags:
- logitech
- herramientas-libres
- oversteer
- pylinuxwheel
title: Configura tus volantes Logitech con Oversteer (ACTUALIZADO)
---
Ya tenemos otra herramienta más para sacar más partido a estos periféricos.

**ACTUALIZACIÓN 21-2-19**:  No ha pasado ni un día y este proyecto se ha actualizado, de forma que ahora no es necesario introducir sudo previamente al ejecutable, ya que en caso de que la aplicación necesite de permisos administrativos para cambiar parámetros del volante, nos pedirá la contraseña a través de la interfaz gráfica de Gnome, por lo que ahora es mucho más cómodo y "Oversteer" ya no depende de la abandonada orden"gksudo". Seguiremos informando de las actualizaciones que vaya teniendo este proyecto.




---


**NOTICIA ORIGINAL**: Si recordais, hace unos meses os hablábamos de [PyLinuxWheel](index.php/homepage/generos/software/12-software/971-pylinuxwheel-una-pequena-gran-utilidad-para-configurar-los-grados-de-nuestros-volantes-logitech), una utilidad que nos traía **un miembro de nuestra comunidad, @OdinTdh**, y gracias a ella ya no teníamos que teclear interminables comandos ni ejecutar incómodos scripts para cambiar los grados de nuestro volante. Este pequeño programa fué un alivio para los muchos que nos encantan los juegos de coches y cumplió a la perfección con lo que se esperaba de ella.


Hoy, ese mismo usuario (@OdinTdh) nos facilitaba el enlace a otro nuevo proyecto que trata de exprimir aun más las opciones de nuestro volante. Este proyecto es **Oversteer**, y está desarrollado en **Python** (al igual que PyLinuxWheel) por Bernat Arlandis, un programador Valenciano. Gracias a Oversteer podremos, además de **cambiar los grados de nuestro volante**, usar los **modos de emulación** que permita nuestro volante, guardar diferentes **perfiles para nuestros juegos favoritos**, **combinar pedales**, o incluso **testear los ejes y botones** de nuestro volante gracias a que incluye un botón que abre "**jstest-gtk**".


**Para instalar Oversteer,** solo tenemos que descargarlo de la [página de su proyecto](https://github.com/berarma/oversteer). Es necesario instalar algunas librerias para que funcione tal y como podeis leer en su "Readme.md". Para instalarlas en Ubuntu tan solo tenemos que teclear:  
`sudo apt install python3 python3-gi python3-pyudev python3-xdg jstest-gtk`


**Para usarlo tendremos que anteceder a su ejecutable con sudo**, ya que el programa no funcionará sin él. Una de sus dependencias es "gksudo" que ya no está disponible en los repositorios de Ubuntu. También debeis saber que su creador solo lo ha testeado con un Logitech G29, por lo que no garantiza que funcione con otros modelos, aunque lo más probable es que si lo haga. Ya sabeis, si encotrais algún problema no dudeis en [notificarlo en la paǵina de su proyecto](https://github.com/berarma/oversteer/issues).


Bueno, pues si quereis sacarle más jugo a  vuestros volantes Logitech, sabed que a partir de ahora teneis otra alternativa más.

