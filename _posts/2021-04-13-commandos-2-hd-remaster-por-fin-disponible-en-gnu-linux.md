---
author: leillo1975
category: Estrategia
date: 2021-04-13 13:55:08
excerpt: "<p>El juego de <span class=\"css-901oao css-16my406 r-1qd0xha r-ad9z0x r-bcqeeo\
  \ r-qvutc0\" dir=\"auto\">@kalypsomedia</span> ya se puede disfrutar en nuestros\
  \ sistemas</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/commandos2/Commandos-2-blog-header.webp
joomla_id: 1299
joomla_url: commandos-2-hd-remaster-por-fin-disponible-en-gnu-linux
layout: post
tags:
- kalypso-media
- remasterizacion
- pyro-studios
- commandos
- commandos-2-hd
title: Commandos 2 HD Remaster por fin disponible en GNU-Linux
---
El juego de @kalypsomedia ya se puede disfrutar en nuestros sistemas


Demasiado tiempo ha pasado ya desde [la última vez que hemos hablado de Commandos 2 HD Remaster]({{ "/posts/commandos-2-hd-remaster-uno-de-los-clasicos-de-pyro-studios-llegara-a-linux" | absolute_url }}). El caso es que su editora, [Kalypso Media](https://www.kalypsomedia.com/eu), había anunciado hace más de un año (Diciembre de 2019), que el juego tendría soporte un mes después. Como podeis calcular, el desarrollo del port de Linux ha sufrido un "leve" retraso. Pero dejemos de quejarnos y vamos al lio, que no se puede decir que todos los días tenemos una noticia tan buena como esta. En primer lugar vamos a darle las gracias a **@Yodefuensa** por el aviso sobre el port, algo que hemos confirmado viendo la [tienda de Steam](https://store.steampowered.com/app/1100410/Commandos_2__HD_Remaster/) y el siguiente tweet:



> 
> We are excited to announce that [#Commandos2](https://twitter.com/hashtag/Commandos2?src=hash&ref_src=twsrc%5Etfw) – HD Remaster is now available for MacOS and Linux:<https://t.co/QcIYuwBxjc> [pic.twitter.com/wqL8qqSM5W](https://t.co/wqL8qqSM5W)
> 
> 
> — Kalypso Media (@kalypsomedia) [April 13, 2021](https://twitter.com/kalypsomedia/status/1381907602037149698?ref_src=twsrc%5Etfw)


Según la **descripción oficial** en esta remasterización de Commandos 2 encontraremos lo siguiente:


* *Reimaginado en HD con controles rediseñados, IU modernizada y un tutorial reformado.*
* *Entornos interactivos: roba uniformes y armas del enemigo, escala postes, desplázate con cables, nada, usa vehículos y entra en edificios, barcos y aviones.*
* *Controla nueve comandos únicos con habilidades y especializaciones diferentes, entre los que se incluyen el boina verde, el francotirador, el zapador, el marine, la seductora y el ladrón.*
* *La primera aparición de la serie Commandos en un motor 3D moderno: rota el entorno 360 grados, entra y sal de edificios, submarinos, aviones y agua de forma fluida y amplía y aleja la vista del entorno.*
* *Escenarios auténticos de la Segunda Guerra Mundial: 10 misiones en 9 entornos distintos tanto de día como de noche con efectos climáticos realistas.*
* *Es tu decisión: tú decides cómo afrontar cada misión. Experimenta con habilidades y armas en un desafiante estilo de juego "contra todas las expectativas".*
* *Armas y vehículos de la Segunda Guerra Mundial, entre los que se incluyen jeeps, tanques, camiones, barcos, lanzacohetes y lanzallamas.*


*Revive la obra maestra de la estrategia en tiempo real que definió el género como ningún otro juego: Desarrollado originalmente por el legendario Pyro Studios, **Commandos 2 - HD Remaster** es un verdadero homenaje a una de las obras maestras más aclamadas del mundo de los videojuegos. Experimenta Commandos 2: Men of Courage como nunca **antes en alta definición y con controles, IU y tutoriales rediseñados**.*  
  
![](https://cdn.akamai.steamstatic.com/steam/apps/1100410/extras/c2hd_comp1.gif)
  
*Toma el control de un grupo de comandos de élite que debe aventurarse en el territorio enemigo y usar sus talentos combinados para completar una serie de misiones notoriamente exigentes ambientadas en la Segunda Guerra Mundial. Infíltrate en varios entornos basados en ubicaciones reales de la Segunda Guerra Mundial, dirige a tu grupo de comandos contra probabilidades abrumadoras, opera de forma encubierta y cambia el curso de la guerra.*
  
![](https://cdn.akamai.steamstatic.com/steam/apps/1100410/extras/c2hd_comp2.gif)
 

Según datos oficiales necesitareis un PC con los siguientes **requerimientos del sistema** como mínimo para poder jugarlo con garantías, aunque a nosotros nos parece un tanto exagerado. Quizás hayan querido lavarse las manos:


* **SO:** Ubuntu 20.04.1 LTS + SteamOS (latest)
* **Procesador:** Intel Core i5-9300H 2.4 GHZ
* **Memoria:** 8 GB de RAM
* **Gráficos:** Nvidia GTX 980, 2GB Vram
* **Almacenamiento:** 8 GB de espacio disponible


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/tb90nBvuwl4" title="YouTube video player" width="780"></iframe></div>


Podeis comprar Commandos 2 HD remaster en la [Humble Store](https://www.humblebundle.com/store/commandos-2-hd-remaster?partner=jugandoenlinux) (**Patrocinado**) y en [Steam](https://store.steampowered.com/app/1100410/Commandos_2__HD_Remaster/). Ahora solo queda que nos dejeis en los comentarios o en nuestros grupos de JugandoEnLinux en [Telegram](https://twitter.com/JugandoenLinux) o  [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org), vuestra opinión sobre esta saga de juegos, así como vuestras impresiones si habeis jugado esta remasterización.

