---
author: leillo1975
category: Aventuras
date: 2019-12-19 14:33:01
excerpt: "<p>@feralgames acaba de lanzarla con soporte para nuestro sistema.</p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/LifeIsStrange/LiS2_full_season_keyart_landscape_copy.webp
joomla_id: 1140
joomla_url: la-coleccion-completa-de-life-is-strange-2-ya-esta-disponible-en-gnu-linux
layout: post
tags:
- feral-interactive
- vulkan
- square-enix
- life-is-strange-2
- dontnod
title: "La colecci\xF3n completa de Life is Strange 2 ya est\xE1 disponible en GNU/Linux."
---
@feralgames acaba de lanzarla con soporte para nuestro sistema.


 Hace tan solo dos días que os anunciábamos la inminente llegada de Life Is Strange 2, y con puntualidad Británica, el juego está al fin entre nosotros, tal y como Feral Interactive nos había comunicado tanto por email, como a través de las redes sociales, tal y como vemos en este tweet:



> 
> The complete season of Life is Strange 2 is out now for macOS and Linux!  
>   
> To get all five episodes in one purchase, and take your first step into the world of Sean and Daniel, buy Life is Strange 2 on:  
>   
> Feral Store - <https://t.co/C7CCONtUCU>  
>   
> Steam - <https://t.co/VsevMp3Z3L> [pic.twitter.com/0tdAQCdj06](https://t.co/0tdAQCdj06)
> 
> 
> — Feral Interactive (@feralgames) [December 19, 2019](https://twitter.com/feralgames/status/1207618386378842112?ref_src=twsrc%5Etfw)


  





Desarrollada originalmente por [DONTNOD](http://dont-nod.com/) Entertainment y publicada por [Square Enix](https://square-enix-games.com/) originalmente,  en este port podremos disfrutar del total de los 5 episodios de los que cuenta este título, continuación (aunque con una historia independiente) de aquel lanzado hace unos años también con soporte, y que cosecho fantásticas críticas de la prensa especializada y un premio BAFTA en el año 2015. Para quien no conozca la saga, los títulos de Life is Strange son una serie de aventuras gráficas narrativas que han revitalizado el género. Esta es la descripción facilitada por Feral Interactive:




*Tras un trágico incidente, los hermanos Sean y Daniel Diaz se dan a la fuga por temor a la policía. Por si esto fuera poco, resulta que Daniel ahora puede mover objetos con la mente, así que los dos hermanos deciden poner rumbo a México. En la ciudad natal de su padre, Puerto Lobos, deberían estar a salvo.*


*De un día para otro, Sean, de dieciséis años, pasa a estar a cargo de Daniel y debe velar por su seguridad y bienestar, además de enseñarle lo que está bien y lo que está mal. La telequinesis de Daniel se vuelve cada vez más fuerte, y Sean tendrá que sentar una serie de reglas. ¿Mantener ese poder en secreto o sacarle utilidad? ¿Pedir, tomar prestado o robar? ¿Contactar con la familia o permanecer ocultos?*


*En el papel de Sean, tus elecciones determinarán el destino de los hermanos Diaz y las vidas de la gente que van conociendo.*


*Desde Seattle, Portland y California… pasando por gasolineras, cabañas abandonadas, callejones y bosques… el camino a México es largo y está lleno de peligros, pero en él también hay amistades, cosas maravillosas y oportunidades.*


*Este viaje podría unir a Sean y Daniel para siempre... o destruir por completo su relación.*


*-Una historia cautivadora impulsada por un gran elenco de personajes.*


*-Daniel siempre sigue el ejemplo de Sean, y lo que le enseñes tendrá consecuencias trascendentales.*


*-Gráficos impresionantes con texturas pintadas a mano.*


*-Una emotiva banda sonora original de Jonathan Morali, el compositor del Life is Strange original, además de temas de Phoenix, The Streets, Sufjan Stevens, Bloc Party y First Aid Kit, entre otros.*




 Para jugar a Life is Strange 2 en Linux tendrás que tener al menos un PC con estas características:


**OS:**Ubuntu 18.04 64-bit    
 **Procesador:**3.4GHz Intel Core i3-4130    
 **Memoria:**4 GB RAM    
 **Gráfica:** NVIDIA GeForce GTX 680 2GB  o AMD Radeon R9 380 4GB  
 **Almacenamiento:**42 GB de espacio disponible    
   
 Notas adicionales:


* Requiere Vulkan
* NVIDIA requiere 430.14 o drivers más nuevos.
* AMD requiere Mesa 19.1.2 or más nuevos.
* Intel GPUs no están soportadas.


Podeis comprar "Life is Strange 2 - Complete Season" en la [Humble Store](https://www.humblebundle.com/store/life-is-strange-2-complete-season?partner=jugandoenlinux) (**Patrocinado**), [la Tienda de Feral](https://store.feralinteractive.com/es/mac-linux-games/lifeisstrange2/) (recomendado) o en [Steam](https://store.steampowered.com/app/532210/Life_is_Strange_2/?l=spanish). Ahora te dejamos disfrutando del trailer que celebra el lanzamiento en nuestro sistema:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/Fshknn1o49M" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


En JugandoEnLinux también os queremos comentar que **pronto publicaremos un análisis completo de este título** para que tengais más información sobre él. ¿Jugasteis al anterior "Life Is Strange"? ¿Qué os parecen los nuevos aires que ha traido este juego al género de las aventuras gráficas? Cuéntanoslo en los comentarios, o en mensajes en nuestra comunidad de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

