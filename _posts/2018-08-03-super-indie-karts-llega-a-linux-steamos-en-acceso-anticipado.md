---
author: Pato
category: Arcade
date: 2018-08-03 11:36:04
excerpt: <p>Carreras de estilo retro 16bits con multitud de circuitos basados en juegos
  indie</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/319ff947d1e8d3f27976610814ba3f98.webp
joomla_id: 818
joomla_url: super-indie-karts-llega-a-linux-steamos-en-acceso-anticipado
layout: post
tags:
- carreras
- indie
- acceso-anticipado
- arcade
title: '''Super Indie Karts'' llega a Linux/SteamOS en acceso anticipado'
---
Carreras de estilo retro 16bits con multitud de circuitos basados en juegos indie

De nuevo volvemos a hacernos eco de la noticia de [gamingonlinux.com](https://www.gamingonlinux.com/articles/super-indie-karts-a-retro-inspired-racer-is-now-available-on-linux.12265), en la que nos anuncian la llegada de '**Super Indie Karts**' en acceso anticipado a Linux/SteamOS. En este caso hablamos de un juego de carreras arcade de estilo retro e inspirado en aquellos títulos de 16bits, con carreras frenéticas y multitud de personajes de estilo "cartoon". La novedad aquí es que correremos en circuitos basados en juegos indie de actualidad, como por ejemplo "Freedom Planet", "Teslagrad", "Cross Code", Guacamelee y muchos otros. El juego como es normal tiene múltiples modos de juego como Time Trials, Grand Prix, SplitscreenGP, modos batalla y soporta multijugador local donde los piques y la diversión con amigos parece estar asegurada.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/H8RP3iF8y-s" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


El juego ha recibido soporte en Linux aunque en la campaña de Kickstarter que llevó a cabo en 2014 no llegó al objetivo aunque se quedó muy cerca, lo que ha llevado a su desarrollador a plantearse su soporte para nuestro sistema favorito.


Si te gustan las carreras desenfadadas, tienes 'Super Indie Karts en acceso anticipado en su [página de Steam](https://store.steampowered.com/app/323670/Super_Indie_Karts/).


