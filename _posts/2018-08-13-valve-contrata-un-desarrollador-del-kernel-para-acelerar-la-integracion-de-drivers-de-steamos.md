---
author: Pato
category: Noticia
date: 2018-08-13 07:49:19
excerpt: "<p>Adem\xE1s se encargar\xE1 de \"otros proyectos\" relacionados con el\
  \ Kernel</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/f5dbf2c2ce8ec21ce67ae22c254e0091.webp
joomla_id: 826
joomla_url: valve-contrata-un-desarrollador-del-kernel-para-acelerar-la-integracion-de-drivers-de-steamos
layout: post
tags:
- steamos
- steam
title: "Valve contrata un desarrollador del Kernel para acelerar la integraci\xF3\
  n de drivers de SteamOS"
---
Además se encargará de "otros proyectos" relacionados con el Kernel

Valve, suma y sigue. Aunque parezca que no, y aunque el [porcentaje de usuarios](https://www.gamingonlinux.com/index.php?module=steam_linux_share) de Steam en Linux sigue sin despegar Valve sigue en su empeño de hacer de Linux y su SteamOS un sistema para jugar en PC. Ciertamente el desarrollo y empuje de Valve con su sistema operativo propio sigue siendo "insuficiente" para algunos de nosotros, con un desarrollo lento, pobre estrategia y nula promoción.


Sin embargo eso no quita para que Valve siga siendo el principal puntal en el que se apoya buena parte del desarrollo del videojuego para sistemas Linux. Hoy nos hacemos eco del anuncio que hizo el pasado viernes **Pierre Loup**, cabeza visible del desarrollo para Linux de SteamOS anunciando la contratación de **Andrey Smirnov**, un desarrollador que se encargará de llevar a cabo la integración de drivers en el Kernel y otros proyectos relativos a este:



> 
> Andrey is a kernel developer we're hired to help us upstream various SteamOS driver changes, as well as undertake other kernel projects. First stop: Xbox One S rumble support!<https://t.co/rYmZFVeucN>
> 
> 
> — Pierre-Loup Griffais (@Plagman2) [August 11, 2018](https://twitter.com/Plagman2/status/1028071360658956288?ref_src=twsrc%5Etfw)



 Según sus propias palabras, el primer driver en el que está trabajando es el de Xbox One S y el soporte para la vibración de este mando.


Andrey se suma a una cada vez mas extensa lista de desarrolladores que trabajan para Valve en diversas áreas relacionadas con linux/StreamOS, como drivers gráficos, desarrollo de Vulkan, SteamOS, drivers para periféricos, VR y un ya largo etc...


Esperemos que Valve vaya desvelando novedades sobre su estrategia respecto a Linux/SteamOS, ya que puede resultar crucial para el juego en Linux de cara a medio/largo plazo. De momento, parece que SteamOS 3.0 está cada vez mas cerca a tenor de los [cambios recientes](index.php/homepage/apt-get-update/item/932-apt-get-update-steamos-steam-companyofheroes2-arma3-warlocks-2-star-renegades) en el chat y los anuncios sobre el desarrollo de las nuevas características y actualizaciones de ese sistema operativo.


¿Que te parece que Valve siga contratando desarrolladores para Linux/SteamOS? ¿Piensas que SteamOS 3.0 será el revulsivo que necesita Linux para comenzar a despegar en cuanto a número de usuarios?


Cuéntamelo en los comentarios o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

