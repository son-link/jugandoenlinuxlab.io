---
author: Pato
category: Ofertas
date: 2017-10-26 17:10:04
excerpt: "<p>\xBFTen\xE9is preparadas vuestras carteras de ping\xFCino?</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/9fadb6d3e28e2be47da4c87e012c5962.webp
joomla_id: 507
joomla_url: llega-la-campana-de-halloween-a-steam-cientos-de-juegos-de-rebajas
layout: post
tags:
- steamos
- steam
- rebajas
- ofertas
title: "\xA1Llega la campa\xF1a de Halloween a Steam! \xA1Cientos de juegos de rebajas!"
---
¿Tenéis preparadas vuestras carteras de pingüino?

Como ya anunciamos en su momento, ¡Steam ya ha comenzado su campaña de rebajas de Halloween!


Como viene siendo habitual por estas fechas, los chicos de Steam nos traen cientos de juegos de temática terrorífica rebajados para la ocasión. Hasta el día 1 de Noviembre tendremos tiempo de repasar nuestras listas de deseados...


¿Que vais a comprar? ¿Que juegos estáis esperando para pasarlo "de miedo" jugando estos días?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

