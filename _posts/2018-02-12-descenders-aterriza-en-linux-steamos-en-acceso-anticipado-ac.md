---
author: leillo1975
category: Carreras
date: 2018-02-12 08:17:43
excerpt: "<p class=\"ProfileHeaderCard-screenname u-inlineBlock u-dir\" dir=\"ltr\"\
  ><span class=\"username u-dir\" dir=\"ltr\">El juego de @RageSquid deja atr\xE1\
  s el Early Access</span></p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/45fc1fd2e4639db7a71d040581c2cb26.webp
joomla_id: 640
joomla_url: descenders-aterriza-en-linux-steamos-en-acceso-anticipado-ac
layout: post
tags:
- acceso-anticipado
- mountain-bike
- desdenders
- ragesquid
title: "Descenders aterriza en Linux/SteamOS en Acceso Anticipado (ACTUALIZACI\xD3\
  N)"
---
El juego de @RageSquid deja atrás el Early Access

**ACTUALIZADO 8-5-19:** Finalmente, y tras más de un año de trabajos, el desarrollo de Descenders llega a su fin y sale de la fase de Acceso Anticipado. Como principal novedad llega la inclusión del Multijugador que te permitirá jugar con amigos o entrar en el Hub general y buscar partidas. También hay cuatro nuevos mundos y multitud de nuevos artículos de personalización del aspecto.  Os dejamos con el trailer sobre el multijugador que recientemente han puesto en Youtube:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/yv1Goix3ZSg" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


Para poder jugarlo tendreis que disponer al menos de las siguientes caracteristicas en vuestro equipo:


**Mínimo:**  

+ **SO:** Ubuntu 12.04 32-bit
+ **Procesador:** Intel Core i5
+ **Memoria:** 4 GB de RAM
+ **Gráficos:** Nvidia Geforce GTX 650/equivalent or higher
+ **Almacenamiento:** 3 GB de espacio disponible



El juego sale con un precio realmente atractivo y podeis comprarlo en la [Humble Store](https://www.humblebundle.com/store/descenders?partner=jugandoenlinux) (patrocinado) o en [Steam](https://store.steampowered.com/app/681280/Descenders/), donde en este momento se encuentra con un descuento del 25% por oferta de lanzamiento.





---


**NOTICIA ORIGINAL:**


Ultimamente ando un tanto despistado. Ya me pasó el otro día con [MXGP3](index.php/homepage/generos/carreras/item/759-mxgp3-parece-que-llegara-a-linux-finalmente) y ahora con este "[Descenders](http://www.descendersgame.com/)", y es que parece que las dos ruedas se me pasan de largo. El pasado viernes se puso a la venta en **Acceso Anticipado** este juego de descensos extremos en Mountain Bike. La información de su lanzamiento me ha llegado gracias a [GamingOnLinux](https://www.gamingonlinux.com/articles/extreme-downhill-biking-game-descenders-flips-into-early-access-some-initial-thoughts.11202), lo cual es curioso pues tengo seguimiento de este juego gracias a mi cuenta de Steam.


El juego está siendo desarrollado por [RageSquid](http://ragesquid.com/), una compañía de los paises bajos que hace más o menos un par de años creó [Action Henk](http://store.steampowered.com/app/285820/Action_Henk/), también disponible para nuestro sistema. El juego por ahora solo dispone de la posibilidad de funcionar en modo de un jugador, pero con la **posbilidad de jugar desafios comunitarios en equipo y clasificaciones en Linea**; aunque no descartan el añadir un modo multijugador en linea en los próximos meses. Tampoco niegan que se pueda dotar al juego del uso de sistemas de Realidad virtual, aunque es una posibilidad mucho más remota. No hay una fecha estimada para una salida de su versión definitiva.


En "Descenders" vamos a tener la posibilidad de intentar dominar una bicicleta en en **circuitos generados de forma procedural**, encontrando saltos, curvas y pendientes diferentes en cada ocasión. También podremos realizar **acrobacias y combos** que aumentarán considerablemente el puntaje de la prueba. Dispondrá de un **sistema de reputación Online** que te dará acceso a nuevas bicicletas y pruebas.


Como veis se trata de un juego de lo más atractivo y que por lo que podemos apreciar tiene todas las trazas de ser un revienta-gamepads. No se si existe algún juego anterior por el estilo, pero lo más parecido que he podido jugar es Dave Mirra BMX hace casi 20 años; y creo que a excepción de las acrobacias este no tiene nada que ver. En JugandoEnLinux os ofreceremos toda la información sobre este juego, tanto sus avances en el Acceso Anticipado, como de su versión oficial. Podeis ver el trailer del juego justo aquí debajo:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/UVNSH-HFG-c" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 Si quereis comprar **Descenders (Acceso Anticipado)** con un 10% de descuento podeis hacerlo en Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/681280/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


¿Qué opinas de descenders?¿Comprarías este juego en Acceso Anticipado? Deja tus respuestas en los comentarios o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

