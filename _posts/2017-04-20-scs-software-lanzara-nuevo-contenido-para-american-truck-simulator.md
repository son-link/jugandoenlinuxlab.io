---
author: leillo1975
category: "Simulaci\xF3n"
date: 2017-04-20 10:54:20
excerpt: "<p>El estudio nos muestra un misterioso video para presentarnos su nueva\
  \ expansi\xF3n</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/78a1f76203c562c3d7c318765865da44.webp
joomla_id: 294
joomla_url: scs-software-lanzara-nuevo-contenido-para-american-truck-simulator
layout: post
tags:
- american-truck-simulator
- ats
- scs-software
- nuevo-mexico
title: "SCS Software lanzar\xE1 nuevo contenido para American Truck Simulator (ACTUALIZADO)"
---
El estudio nos muestra un misterioso video para presentarnos su nueva expansión

**ACTUALIZACIÓN a 18-10-17:** SCS Software acaba de publicar un video oficial donde podemos ver parte del contenido que tendrá está nueva DLC para ATS. Podeis verlo aquí:


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/g3BY2j8u010" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 




---


 Hace ya bastante tiempo que se sabe que los atareados chicos de [SCS Software](index.php/component/search/?searchword=scs&searchphrase=all&Itemid=101) están diseñando lo que será la nueva expansión para **American Truck Simulator**. En esta ocasión le tocará al estado de **Nuevo México**, tal y como informamos hace un par de meses [aquí mismo](index.php/item/328-american-truck-simulator-se-expandira-hacia-nuevo-mexico). En esta ocasión nos dejan caer un escueto video [sin más](http://blog.scssoft.com/2017/04/reference-research-for-american-truck.html) para ir abriendonos el apetito de conducir por nuevas carreteras. En él, aunque trata de ser misterioso, se entiende muy bien que se trata del estado cuya capital es Santa Fe, ya que se nos muestran elementos tan claros como el logo de su bandera y un gracioso alien, tan "típico" de esas latitudes por el [caso Roswell](https://es.wikipedia.org/wiki/Caso_Roswell). Si más os dejo el video para que podais verlo:


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/O4NBx-O8I7A" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


Como ya os había comentado en su día se tratará de una DLC de pago, al contrario que sus expansiones de Nevada y Arizona, que son gratuitas y se añadieron con posterioridad al juego base. En cuanto tengamos más información sobre esta expansión o sobre cualquier trabajo de SCS Soft os irformaremos lo antes posible.


 


¿Qué os ha parecido el video? ¿Teneis ganas de echarle ya el diente a esta DLC? Dejanos tu comentario o usa nuestros grupos de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD) donde os estamos esperando.


 


 

