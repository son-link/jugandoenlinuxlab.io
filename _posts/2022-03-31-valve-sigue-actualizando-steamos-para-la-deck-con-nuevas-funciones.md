---
author: Pato
category: Hardware
date: 2022-03-31 21:18:50
excerpt: "<p>El ritmo de actualizaciones no baja tanto en el canal estable como en\
  \ el beta</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Steamdeck/SteamDeckVerified.webp
joomla_id: 1456
joomla_url: valve-sigue-actualizando-steamos-para-la-deck-con-nuevas-funciones
layout: post
tags:
- steamos
- beta
- steam-deck
title: Valve sigue actualizando SteamOS para la Deck con nuevas funciones
---
El ritmo de actualizaciones no baja tanto en el canal estable como en el beta


Valve sigue poniendo toda la carne en el asador con la Steam Deck, y buena prueba de ello es el ritmo de actualizaciones de sistema de la ultraportatil que nos trae nuevas mejoras, ajustes y características. Es rara la semana que no hay al menos un par de actualizaciones.


Comenzando por la última actualización del sistema, las novedades son las siguentes:


* Añadido soporte de escritura de panel táctil dual al teclado en pantalla
* Añadido el modo de juego en el teclado en pantalla al modo Escritorio
* Añadido el estado de Compartir familia a la página de detalles del juego. Los prestatarios verán de que biblioteca están pidiendo prestada, y los prestamistas verán un mensaje si su biblioteca está actualmente en uso por un prestatario.
* Añadida una pantalla de Calibración y Configuración avanzada con opciones para:
* Ajuste de puntos muertos para los Joysticks izquierdo y derecho
* Ajuste de la fuerza háptica para los trackpads izquierdo y derecho
* Joysticks y otros sensores en gamepads externos
* Flujo de conexión de red actualizado para conectarse sin volver a solicitar una contraseña conocida
* Rendimiento mejorado que descarga imágenes de la biblioteca después de iniciar sesión, lo que lleva a menos parpadeo
* Se eliminó la visualización del botón 'B' en el menú Superposición de acceso rápido
* Se corrigieron problemas de entrada del teclado en pantalla cuando se conectaba a portales públicos cautivos de WiFi
* Se corrigió el problema en el que Chrome no se instalaría desde la sección que no es Steam de la Biblioteca


Puedes ver el anuncio en el post oficial [en este enlace](https://steamcommunity.com/games/1675200/announcements/detail/3215014689176368944).


En cuanto a los que opten por instalar el canal beta del sistema, las novedades son:


* Añadidos mensajes cuando se conecta un cargador que no cumple con la barra mínima
* Añadida una configuración de framerate sin tapa en el menú Acceso rápido> Rendimiento
* Añadido soporte fTPM, lo que permite la instalación de Windows 11
* Botón combinado agregado: mantener "..."+" Volumen hacia abajo "para restablecer el contrato de PD en los casos en que Steam Deck se atasca debido a un dispositivo Tipo C incompatible
* LED de alimentación actualizado para atenuarse unos segundos después de los eventos de conexión de la fuente de alimentación para una mejor experiencia en entornos oscuros
* Compatibilidad mejorada para varios dispositivos tipo C y PSU
* Mejora de la duración de la batería en escenarios de uso inactivo o muy bajo
* Estabilidad mejorada
* Se corrigieron problemas en los que la pantalla táctil no funciona después de algunos aranques
* Corregida la compatibilidad con algunas tarjetas SD específicamente cuando se usaban como dispositivos de arranque.
* Error ACPI fijo en el núcleo


Hay que recordar que el canal beta es para aquellos usuarios que quieren probar las novedades que aún no se han testeado debidamente y reportar fallos o comentarios al respecto.


Puedes ver el anuncio del canal beta en el post oficial [en este enlace](https://steamcommunity.com/app/1675200/discussions/0/3267931984799445343/).

