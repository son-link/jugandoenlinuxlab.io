---
author: leillo1975
category: Software
date: 2018-03-07 16:11:36
excerpt: "<p>La primera actualizaci\xF3n importante de esta API ya est\xE1 disponible.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/0f9f2c4ac314569afbfb8ab4ee9198bf.webp
joomla_id: 670
joomla_url: lanzada-vulkan-1-1
layout: post
tags:
- vulkan
- amd
- intel
- nvidia
- khronos-group
- api
title: Lanzada Vulkan 1.1 (ACTUALIZADO)
---
La primera actualización importante de esta API ya está disponible.

**ACTUALIZACIÓN 8-3-18**: Nos acaban de soplar en nuestro grupo de [Telegram](https://t.me/jugandoenlinux) (gracias **@OdinTdh**) que el driver Radeon (RADV) también se ha [actualizado](https://www.phoronix.com/scan.php?page=news_item&px=RADV-Vulkan-1.1-Conformant) para cumplir con las especificaciones de Vulkan 1.1. Los que por una razón o por otra prefieran o necesiten usar este driver, también tendrán soporte  en cuanto se comprueben los cambios. Estos estarán en Mesa 18.1 dentro de poco.




---


**NOTICIA ORIGINAL**: Acabamos de leer en [Phoronix](https://www.phoronix.com/scan.php?page=article&item=khr-vulkan-11&num=1) que Vulkan acaba de publicar su versión 1.1, lo que significa que nos encontramos ante la primera revisión de importancia a este software de gráficos de alto rendimiento. Ya han pasado dos años desde que se publicase la versión 1.0, y tras sucesivas revisiones finalmente se ha llegado a esta versión que significa que esta API ya está dando sus primeros signos de madurez e implantación. Hace justo también una semana que se lanzó la versión de Vulkan (MoltenVK) para usar en MacOS e IOS, por lo que el avance parece garantizado. Este nuevo hito en la corta pero intensa historia de esta API viene justo a tiempo para ser presentada en la GDC 18 (Conferencia de desarrolladores de juegos).


Si quereis información (eso si, muy técnica) sobre este lanzamiento podeis consultar la [web del grupo Khronos](https://www.khronos.org/news/press/khronos-group-releases-vulkan-1-1). También están disponibles las [especificaciones](https://www.khronos.org/registry/vulkan/specs/1.1/html/vkspec.html) de la API. Podeis ver una [presentación explicativa](https://www.khronos.org/assets/uploads/developers/library/overview/Vulkan-1-1-Presentation_Mar18.pdf) de las novedades. Entre estas destacan las nuevas extensiones que podemos ver en esta diapositiva:


![ExtensionesVulkan1.1 copiar](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Vulkan/ExtensionesVulkan1.1_copiar.webp)


Es importante decir que los drivers gráficos principales ya se han actualizado pudiendo encontrar soporte para esta nueva especificación, estando disponibles para Nvidia ([387.42.05 Beta](https://developer.nvidia.com/vulkan-driver)) , AMD ([AMDVLK](https://github.com/GPUOpen-Drivers/AMDVLK) y [AMDGPU-PRO](https://support.amd.com/en-us/kb-articles/Pages/Radeon-Software-for-Linux-with-Vulkan-1.1-support.aspx)) e Intel (disponibles a partir de Mesa 18.1)


Es agradable ver como avanza poco a poco esta API, y como gracias a ella es mucho más fácil para los desarrolladores traer su trabajo a Linux. Como sabeis una de las grandes bazas de Vulkan, a parte de todas las nuevas posibilidades y optimizaciones, o el aprovachamiento multinúcleo, es su capacidad Multiplataforma, que permite aprovechar los desarrollos para otras sistemas y convertirlos facilmente a cualquier otro (Android, Windows, Mac, Switch, Linux...). En Linux ya tenemos unos cuantos juegos que aprovechan sus virtudes, como Mad Max, F1 2017 o Serious Sam.  Esperemos ver más pronto que tarde nuevos juegos aprovechando sus beneficios.


¿Qué opinais de Vulkan? ¿Creeis que puede marcar la diferencia para nosotros los linuxeros el uso de esta API? Deja tu opnión e los comentarios o anímate a discutirlo en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

