---
author: Serjor
category: "Acci\xF3n"
date: 2019-05-04 07:36:36
excerpt: <p>CS:GO Danger Zone se actualiza con un nuevo mapa y cambios para actualizar
  su jugabilidad</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/d2ba94f3222b824cf3568813b993f00a.webp
joomla_id: 1040
joomla_url: cs-go-danger-zone-presenta-su-nuevo-mapa-sirocco-y-sus-nuevas-opciones-jugables
layout: post
tags:
- valve
- battle-royale
- danger-zone
- cs-go
title: CS:GO Danger zone presenta su nuevo mapa, Sirocco, y sus nuevas opciones jugables
---
CS:GO Danger Zone se actualiza con un nuevo mapa y cambios para actualizar su jugabilidad

Cuando Valve [lanzó](index.php/homepage/generos/accion/5-accion/1044-valve-lanza-el-modo-de-cs-go-danger-zone-en-modalidad-free-to-play) CS:GO Danger Zone no lo hizo con pocas críticas, no tan solo por el tema de pasar a ser free to play y el posible aumento de niños rata y demás troles que abundan en el juego online, y que muchas veces son responsables de que la gente que quiere jugar y divertirse, abandone el juego, sino en el modo Danger Zone, aunque tuvo buena acogida, una de las mayores críticas que recibió este modo es que se quedaba realmente pequeño comparado con otros battle royale donde tanto el mapa como el número de jugadores en la partida era mucho mayor.


Y aunque la diferencia de número de jugadores sigue ahí, Valve ha [anunciado](http://www.counter-strike.net/sirocco) una actualización de este modo donde ha introducido un nuevo mapa en el modo Danger Zone, Sirocco, y junto a este nuevo mapa, algunos cambios que hacen que el juego se vuelva un poco más interesante. Para empezar tenemos que al comenzar la partida podemos elegir qué habilidad queremos tener equipada, como la pistola taser, el paracaídas, una jeringuilla para recuperar vida y una de las novedades más interesantes de esta actualización: las botas de ExoSalto (ExoJump Boots, la traducción queda fatal, la verdad), que nos permitirán saltar más alto, llegando así más rápido y lejos. Como podremos hacer que tanto nosotros como nuestros enemigos lleguen más alto y lejos usando minas de salto, las cuales lanzaran realmente lejos a cualquiera que pase por encima, por lo que si somos nosotros quienes decimos plantarla para usarla, deberemos llevar el paracaídas equipado si no queremos morir en el aterrizaje. Lo que aún no está en el juego pero sí está anunciado es que en *breve* (Valve time) tendremos disponible un escudo protector, pero no podremos disparar si lo tenemos equipado, un dron pilotable para llevar recursos de un lado a otro o para espiar a los enemigos y bonus extra por explorar el mapa o por bombas. Eso sí, "coming soon", con lo que tocará estar pendiente de las actualizaciones del juego.


Además, ahora si morimos, mientras uno de nuestros compañeros de escuadra esté vivo, podemos reaparecer en cualquier lugar del mapa. También han añadido pequeños ajustes como por ejemplo que las armas vienen ahora con más munición, para que de acuerdo a las palabras de Valve, el jugador esté más tiempo cazando que buscando munición, se ha implementado un sistema de avisos para poder indicar a nuestros compañeros donde hay enemigos, armas o algo interesante, y por último, ahora la tablet nos da información precisa sobre donde tenemos el arma más cercana.


Buscando información sobre la actualización he topado con [esportsportal](https://www.esportsportal.com/csgo-danger-zone-update/), y tengo que reconocer que al leer tantas veces "Apex Legends" en su artículo me han servido para darme cuenta del nuevo rumbo que está tomando este modo de juego. Sin querer dejar de ser táctico, las botas para tener mejores saltos, las minas para saltar, el uso del drone (a futuro) para espiar o robar y algún cambio más están llevando este modo de juego a una jugabilidad más vertical y, esperemos, más dinámica.


Y tú, ¿ya has probado esta nueva actualización de CS:GO Danger Zone? Cuéntanos qué te parecen estos cambios en los comentarios o en nuestros canales de [Telegram](https://t.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD)

