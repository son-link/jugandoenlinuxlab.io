---
author: P_Vader
category: Arcade
date: 2021-05-14 20:59:31
excerpt: "<p>@_EasyTrigger_ nos trae el fren\xE9tico e impresionante @huntdowngame\
  \ .<em><strong><br /></strong></em></p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Huntdown/huntdonw900.webp
joomla_id: 1304
joomla_url: hutndown
layout: post
tags:
- plataformas
- nuevos-lanzamientos
- retro
- local
- cooperativo
title: El arcade retro Huntdown llega con soporte nativo.
---
@_EasyTrigger_ nos trae el frenético e impresionante @huntdowngame .


**"El crimen no compensa... ¡excepto que seas un cazador de recompensas!"**


Y con esta frase lapidaria comienza el frenético e impresionante Huntdown. Y aquí podrías poner a Kurl Rusell con su parche o un terminator T800, por que Hutndown es eso, accion de los años 80 en estado puro. **La ambientación es una mezcla de Blade runner con Mad Max a lo cyberpunk. ¡Que locura!**


La historia es simple. En un futuro caótico y en decadencia solo quedan megacorporaciones y bandas criminales que nadie puede detener. Para eso estas tu, el cazarecompensas, para acabar con ellas.


Después de un año de espera en exclusiva con Epic store, por fin podemos disfrutar de este titulo **lanzado ahora si en [Humble bundle](https://es.humblebundle.com/store/huntdown?partner=jugandoenlinux) (patrocinado) [Steam](https://store.steampowered.com/app/598550/HUNTDOWN/) y [GOG](https://www.gog.com/game/huntdown) totalmente nativo para nuestro sistema linux**.


El juego es un **clásico run and gun, con momentos brutales de pura adrenalina, lleno de rafagas y explosiones por doquier**. Si en su día disfrutaste de los Metal Slug, Duke nukem o Megaman, te vas a enamorar de este juego. Se puede jugar en cooperativo local, máximo dos jugadores.


![Huntdown](https://huntdown.com/wp-content/uploads/2020/04/Huntdown_ScreenShot4.png)


Un estilo **16 bit pixelado muy bien conseguido,** una ambientación sonora tecnho-cañera de los 80s, unas animaciones que son una delicia, con multitud de detalles y gag que te sacan una risa de vez en cuando, **se puede decir que han logrado un gran producto**. Por cierto, tienen tan claro que es un homenaje a aquellas recreativas que [próximamente sacaran la suya propia con el juego](https://huntdown.com/arcade/), ¡quien da mas!


Por ponerle alguna pega, **se echa de menos un cooperativo en linea** aunque en el caso de steam se suple con Remote Play **y falta algún subtitulo en español.**


Sin mas os enseñamos una partida que hemos podido disfrutar gracias a [Easy Trigger](https://easytrigger.com/) que amablemente nos ha cedido este juego para probarlo. Y las gracias a CansecoGPC por acompañarnos en esta presentación.  


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/Fes-uoA-ntk" style="display: block; margin-left: auto; margin-right: auto;" width="780"></iframe></div>


¿Que te pareció este alocado retro? Cuentanos tus sensaciones en nuestros grupos de [Telegram](https://twitter.com/JugandoenLinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

