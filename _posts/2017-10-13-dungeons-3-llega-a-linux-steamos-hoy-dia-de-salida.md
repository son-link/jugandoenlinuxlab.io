---
author: Pato
category: Estrategia
date: 2017-10-13 17:04:47
excerpt: "<p>Expande tus dominios como el Se\xF1or de la Mazmorra</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/48ed12281ae1f6453d2d5ec95b17e082.webp
joomla_id: 484
joomla_url: dungeons-3-llega-a-linux-steamos-hoy-dia-de-salida
layout: post
tags:
- fantasia
- estrategia
title: "'Dungeons 3' llega a Linux/SteamOS hoy d\xEDa de salida"
---
Expande tus dominios como el Señor de la Mazmorra

 Hoy nos llega a Linux/SteamOS la última entrega de la serie Dungeons. Se trata de 'Dungeons 3' y al igual que los anteriores títulos de la saga tendremos que construir nuestra mazmorra, crear nuestras hordas del mal y liderarlas hacia la victoria destruyendo todo cuanto encontremos a nuestro paso. 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/N27f2proi5E" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 **Características:**


*"Da rienda suelta a tu lado oscuro y **crea una mazmorra subterránea única** a partir de una gran variedad de salas, trampas y estructuras. Crea el ejército más aterrador que el mundo haya visto jamás, y elige entre criaturas despreciables como **orcos, súcubos, zombis** y muchos, muchos más. Luego, una vez hayas constituido tus fuerzas, surge de la oscuridad y guía a tu ejército hasta la luz del mundo exterior, donde corromperás la tierra y acabarás con cualquier cosa vagamente heroica, bonita o con forma de unicornio. Además, y por primera vez en la serie Dungeons, podrás disfrutar de **niveles generados aleatoriamente**, para que ninguna partida sea igual a la anterior. ¡Los malvados conquistadores jamás dejarán de divertirse!"*


**Requisitos mínimos:**


* **SO:** Ubuntu 16.04.3 LTS + SteamOS (última versión)
* **Procesador:** Intel Quad Core 2.8 GHz (i7 900 series) o 3.5 GHz AMD (FX 6000 series)
* **Memoria:** 4 GB de RAM
* **Gráficos:** AMD/NVIDIA tarjeta dedicada, con al menos 1024MB de VRAM y soporte Shader Model 5.0 (AMD Radeon HD 7000 series y NVIDIA GeForce GTX 600 series)


**Recomendados:**


* **SO:** Ubuntu 16.04.3 LTS + SteamOS (última versión)
* **Procesador:** Quad core 3.5 GHz o superior (Intel i5 4000 Series / AMD Ryzen 3 Series)
* **Memoria:** 8 GB de RAM
* **Gráficos:** AMD/NVIDIA tarjeta dedicada, con al menos 3072MB de VRAM y soporte Shader Model 5.0 (AMD R9 300 Series y NVIDIA GeForce GTX 900 Series o superior)
* **Almacenamiento:** 5 GB de espacio disponible


**Nota adicional:**  Otras distribuciones Linux (Mint, etc) deberían funcionar, pero no podemos dar soporte oficial para ellas.


'Dungeons 3' está disponible en español en su página de Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/493900/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 ¿Piensas esparcir el mal por las mazmorras?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 

