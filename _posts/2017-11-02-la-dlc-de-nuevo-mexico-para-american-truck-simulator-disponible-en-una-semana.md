---
author: leillo1975
category: "Simulaci\xF3n"
date: 2017-11-02 16:32:32
excerpt: "<p>El jueves d\xEDa 9 de Noviembre podremos conducirlo.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/f585186e84a63498921b17587e9e17f9.webp
joomla_id: 517
joomla_url: la-dlc-de-nuevo-mexico-para-american-truck-simulator-disponible-en-una-semana
layout: post
tags:
- dlc
- american-truck-simulator
- ats
- scs-software
- nuevo-mexico
title: "La DLC de Nuevo M\xE9xico para American Truck Simulator disponible (ACTUALIZACI\xD3\
  N)"
---
El jueves día 9 de Noviembre podremos conducirlo.

**ACTUALIZACIÓN 9-11-17:** Finalmente, y según lo esperado, la expansión de pago para American Truck Simulator, Nuevo Mexico acaba de ser lanzada y ya podemos disfrutarla, tal y como comunican a través de Twitter:


 



> 
> New Mexico DLC for American Truck Simulator is out! <https://t.co/DHngPCC4hD>
> 
> 
> — SCS Software (@SCSsoftware) [9 de noviembre de 2017](https://twitter.com/SCSsoftware/status/928653377864650752?ref_src=twsrc%5Etfw)



 En ella econtrareis más de 4000 millas de nuevas carreteras, 14 nuevas ciudades (Albuquerque, Santa Fe, Roswell, etc), nuevas compañías y mucho más. Podeis encotrar más información sobre esta DLC en el [Blog de SCS Software](http://blog.scssoft.com/2017/11/new-mexico-dlc-arrives-alongside.html).


Si quereis comprar American Truck Simulator: New Mexico podeis hacerlo en Steam:


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/684630/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 




---


**Artículo Original (2-11-17):**


Menudo día. Hace unos minutos el [lanzamiento de F1 2017](index.php/homepage/generos/carreras/item/638-f1-2017-disponible-para-gnu-linux-steamos), y ahora el anuncio de la salida del nuevo DLC de American Truck Simulator, Nuevo México. [Unos meses atrás](index.php/homepage/generos/simulacion/item/416-scs-software-lanzara-nuevo-contenido-para-american-truck-simulator) dabamos cuenta de la intención de SCS Software de ampliar hacía este estado la red de carreteras de ATS, y la semana que viene, al fin podremos disfrutarla. Concretamente podremos hacernos con ella el día 9 de Noviembre, Jueves. El anuncio se ha realizado mediante este tweet:



> 
> The release date has been set! ?
> 
> 
> Your wait for New Mexico DLC will be over on Thursday, November 9th, 2017!<https://t.co/9aryqm8Wpy> [pic.twitter.com/pZS01cLr33](https://t.co/pZS01cLr33)
> 
> 
> — SCS Software (@SCSsoftware) [November 2, 2017](https://twitter.com/SCSsoftware/status/926121693193613312?ref_src=twsrc%5Etfw)


  





 


En [su blog](http://blog.scssoft.com/2017/11/new-mexico-map-expansion-for-ats-will.html) indican que están dando los últimos retoques y optimizaciones a esta expansión, pero que el feedback que les están mandando los betatesters es muy bueno, por lo que creen que no habrá contratiempos ni retrasos. El precio será de 11.99€ y podreis comprarlo en [Steam](http://store.steampowered.com/app/684630/American_Truck_Simulator__New_Mexico/).


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/g3BY2j8u010" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


Desde JugandoEnLinux os informaremos de la salida de esta expansión en cuanto se produzca, y si nos es posible, contenido videográfico y/o un pequeño análisis.


 


¿Os gustan los simuladores de SCS Soft? ¿Habeis jugado ya a American Truck Simulator? Déjanos tus impresiones sobre este lanzamiento en los comentarios o en nuestro canal de [Telegram](https://t.me/jugandoenlinux).

