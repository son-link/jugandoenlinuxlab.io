---
author: Pato
category: "Simulaci\xF3n"
date: 2017-12-28 10:55:54
excerpt: <p>Incluyendo Asseto Corsa, Test Drive y Cars3</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/0345422ddcb43c580d78147360d8e84a.webp
joomla_id: 590
joomla_url: fetal-cede-a-la-presion-de-la-comunidad-y-portara-todos-los-juegos-de-coches-a-linux
layout: post
tags:
- carreras
- simulacion
title: "Fetal cede a la presi\xF3n de la comunidad y portar\xE1 todos los juegos de\
  \ coches a Linux"
---
Incluyendo Asseto Corsa, Test Drive y Cars3

Gracias a la presión de la comunidad hoy por fin podemos dar la esperada noticia que estábamos deseando publicar. Liderados por un tal Leo, un grupo de aficionados a los juegos de coches han estado presionando a Feral Interactive Inc con mensajes vía SMS para que la compañía llegase a distintos acuerdos con las distribuidoras de juegos de coches y poder portarlos a nuestro sistema favorito.


Debido a esta presión, parece que la compañía al fin ha cedido y ha prometido que portará a GNU/Linux todos los juegos de coches de conocidas franquicias como Asseto Corsa, Test Driver, Forza Motorsport o Cars.


Según palabras del director de marketing de Feral, Williams Runner Vroom *"no hemos podido soportar más el sonido de entrada de mensajes en nuestros móviles y hemos tenido que ceder. Esperamos que los aficionados a los juegos de coches en Linux nos agradezcan este titánico esfuerzo y poder seguir con nuestras vidas sin estar pendientes de los mensajes"*


Es de suponer que en breves minutos comiencen a aparecer multitud de "OFNIS" en su ya famoso [radar](https://www.feralinteractive.com/es/upcoming/) donde suelen anunciar a bombo y platillo sus próximos lanzamientos, aunque desde Jugando en Linux recomendamos que mientras esperáis a que aparezcan toméis asiento y alguna bebida refrescante para amortiguar la espera.


Como siempre, desde Jugando en Linux os mantendremos informados de cualquier información al respecto.


![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Externo/Srekchristmas.webp)

