---
author: Jugando en Linux
category: Editorial
date: 2017-02-17 19:41:23
excerpt: "<p>Reconozcamoslo, en el fondo somos unos rom\xE1nticos. Unos cuantos chicos\
  \ \"raros\" con un sistema operativo diferente que se empe\xF1an en no seguir la\
  \ corriente a capa y espada. Seguro que en el fondo os sent\xEDs as\xED. Seguro\
  \ tambi\xE9n que hace a\xF1os, cuando todo esto de los juegos en Linux comenz\xF3\
  \ so\xF1\xE1steis con poder disfrutar de grandes t\xEDtulos en vuestros ordenadores\
  \ y poneros a la par de otros sistemas, sobre todo Windows. Y es cierto que la cosa\
  \ ha cambiado&nbsp;considerablemente a mejor, pero aun as\xED estamos lejos de poder\
  \ mirarnos de t\xFA a t\xFA con los que escogieron el SO de Redmond.</p>\r\n<p>&nbsp;</p>\r\
  \n<p>Hace unos d\xEDas pudimos leer un<span style=\"color: #000080;\"> <a href=\"\
  http://www.phoronix.com/scan.php?page=article&amp;item=pascal-win10-linux&amp;num=1\"\
  \ target=\"_blank\" style=\"color: #000080;\">art\xEDculo de Phoronix</a></span>\
  \ en el que se comparaba el rendimiento de dos graficas Nvidia (la 1060 y la 1080)\
  \ tanto en Windows 10 como en Ubuntu 16.10, y los datos no pueden ser m\xE1s claros.\
  \ Los n\xFAmeros no mienten, el rendimento de nuestro sistema operativo arroja resultados\
  \ en la mayor\xEDa de los juegos que dan mucho que pensar, y es que en la mayor\
  \ parte de los juegos la diferencia de rendimiento entre un sistema y otro es notable,\
  \ saliendo perdedor nuestro amado Linux. Si vemos la conclusi\xF3n de este art\xED\
  culo vemos que salvo honrosas excepciones hablamos de un 60-70% de rendimiento con\
  \ respecto a Windows, lo cual es para tener muy en cuenta.</p>\r\n<p>&nbsp;</p>\r\
  \n<p>Por ahora est\xE1 claro que en cuanto a videojuegos no podemos competir ni\
  \ presumir, y que es casi imposible que un gamer (entendiendo como tal a alguien\
  \ que busca lo mejor para jugar) escoja GNU/Linux como plataforma para su afici\xF3\
  n (o profesi\xF3n) si no tiene en cuenta otros factores. Tambi\xE9n est\xE1 claro\
  \ que los que jugamos en Linux, lo hacemos principalmente por eso, por que somos\
  \ linuxeros, y como tales sacrificamos nuestra alma gaming en favor de un sistema\
  \ operativo superior, m\xE1s libre y m\xE1s abierto; y eso tiene un coste que estamos\
  \ dispuestos a asumir. Por lo tanto implica que al final jugar en nuestros PC's\
  \ con GNU/Linux nos supone:</p>\r\n<p>-<strong>Mayor desembolso en Hardware</strong>,\
  \ ya que implica tener mejores componentes para poder disfrutar en igualdad de condiciones\
  \ que un usuario de Windows.</p>\r\n<p>-Si competimos con el mismo hardware vamos\
  \ a tener que <strong>bajar el nivel de detalle para no perder jugabilidad</strong>\
  \ con respecto a Windows.</p>\r\n<p>-<strong>M\xE1s limitaci\xF3n en el Hardware\
  \ gr\xE1fico</strong>. Se puede decir que si queremos jugar con garant\xEDas en\
  \ Linux tenemos que tener una gr\xE1fica Nvidia. Los drivers para AMD o Intel por\
  \ ahora son muy inferiores a los de Windows, aunque gracias a dios la cosa parece\
  \ que est\xE1 avanzando a buen paso, m\xE1s por \xEDmpetu de la comunidad y de Valve,\
  \ que por los fabricantes de Hardware.</p>\r\n<p>-<strong>Menor cantidad y <em>\"\
  calidad\"</em> de juegos</strong>. Si bien cada vez nos podemos quejar menos por\
  \ la cantidad, ya que recientemente se sobrepas\xF3 el n\xFAmero de 3000 t\xEDtulos\
  \ en Steam, es cierto que la cifra de Windows es muy superior. Tambi\xE9n entrecomillaba\
  \ la calidad, ya que la gran mayor\xEDa de estos juegos son de tipo indie, habiendo\
  \ muchisimos menos juegos considerados AAA, y de franquicias importantes.</p>\r\n\
  <p>-<strong>El mercado se ci\xF1e casi exclusivamente a Steam</strong>, y en menor\
  \ medida GOG e itch.io, por lo que si por la raz\xF3n que fuere no nos interesa\
  \ el hacer de alguna de estas tiendas tenemos posibilidades nulas de encontrar tiendas-sistemas\
  \ alternativos de distribuci\xF3n. Por supuesto, la posibilidad de comprar juegos\
  \ en formato f\xEDsico pr\xE1cticamente es inexistente.</p>\r\n<p>&nbsp;</p>\r\n\
  <p>&nbsp;</p>\r\n<figure class=\"pull-center\" style=\"text-align: center;\"><img\
  \ src=\"images/Articulos/linux-vs-windows.jpg\" alt=\"Imagen: Taringa\" style=\"\
  display: block; margin-left: auto; margin-right: auto;\" title=\"Imagen: Taringa\"\
  \ /><figcaption><span style=\"color: #808080;\"><em>Imagen: Taringa</em></span></figcaption></figure>\r\
  \n<p>&nbsp;</p>\r\n<p>De todos modos, dentro de todas estas sombras, hay siempre\
  \ alguna luz, ya que por ejemplo desde Phoronix tenemos confirmaci\xF3n de que Hitman\
  \ tiene el <a href=\"http://www.phoronix.com/scan.php?page=news_item&amp;px=Vulkan-Signs-HITMAN\"\
  >soporte para Vulkan parcialmente implementado</a>, del mismo modo que Aspyr est\xE1\
  \ <a href=\"http://steamcommunity.com/app/289070/discussions/0/144512942755435118/?ctp=27#c135509124602029681\"\
  \ target=\"_blank\">barajando usar Vulkan</a> entre otras opciones para mejorar\
  \ Civilization VI, as\xED como la propia Valve est\xE1 volcada en mejorar la experiencia\
  \ de juego en linux, y est\xE1 <a href=\"http://www.phoronix.com/scan.php?page=news_item&amp;px=Arceri-Joined-Valve\"\
  >contratando gente</a> para trabajar en los drivers. Por otra parte, tambi\xE9n\
  \ tenemos a Feral Interactive colaborando en la mejora de los drivers Mesa y ya\
  \ ha trabajado en algunos parches y mejoras en este terreno. Con OpenGL llegando\
  \ al l\xEDmite de lo que puede dar de si, y buena muestra tenemos con los \xFAltimos\
  \ desarrollos y ports que nos han llegado, Vulkan se postula como una pieza clave\
  \ de cara al desarrollo y expansi\xF3n del juego en sistemas Linux. De su buen desarrollo\
  \ y su adopci\xF3n como API de referencia para los desarrolladores, depende en gran\
  \ medida la llegada en el futuro de los grandes juegos (AAA) a nuestro sistema favorito.\
  \ Si Vulkan es capaz de llegar a ofrecer el rendimiento que se le supone, y puede\
  \ poner a Linux al menos a la par del rendimiento que Windows tiene con DirectX12,\
  \ tendremos argumentos para decir que hemos dado un paso definitivo para asentar\
  \ a los sistemas Linux como alternativa completa a Windows.</p>\r\n<p>&nbsp;</p>\r\
  \n<p>Tambi\xE9n parece andar cerca la llegada de la realidad virtual a Linux, ya\
  \ que desarrolladoras como Croteam est\xE1n hablando de implementarlo junto a Vulkan\
  \ en sus juegos. Hay que tener en cuenta tambi\xE9n que si los esfuerzos tanto de\
  \ Valve, algunas desarrolladoras y las compa\xF1\xEDas que realizan ports; inviertiendo\
  \ tiempo y dinero en dar soporte a los usuarios de Linux y SteamOS, ser\xE1 porque,\
  \ o bien les sale rentable, o bien ven que realmente es un mercado con potencial.</p>\r\
  \n<p>&nbsp;</p>\r\n<p>Lo que si est\xE1 claro es que una empresa o un estudio de\
  \ videojuegos puede hacer una prueba en un mercado para realizar un sondaje, pero\
  \ si no funciona lo m\xE1s normal es que lo abandone al poco tiempo y no siga insistiendo\
  \ en ello. Teniendo en cuenta que Linux ya lleva unos a\xF1os introducido en el\
  \ mercado del videojuego, es de suponer que ya no se trata de esto \xFAltimo, sino\
  \ de una apuesta seria por esta peque\xF1a porci\xF3n de la tarta y que adem\xE1\
  s esta porci\xF3n tiene potencial para crecer.</p>\r\n<p>&nbsp;</p>\r\n<p>Este art\xED\
  culo es un art\xEDculo de opini\xF3n, y est\xE1 escrito, y es suscrito por los redactores\
  \ de JugandoEnLinux.com (Pato, Serjor y leillo1975). Si quieres participar en el\
  \ debate puedes dejar un mensaje en los comentarios, o usar nuestros canales de\
  \ <a href=\"https://telegram.me/jugandoenlinux\" target=\"_blank\">Telegram</a>\
  \ o <a href=\"https://discord.gg/ftcmBjD\" target=\"_blank\">Discord</a>.</p>\r\n\
  <p>&nbsp;</p>\r\n<p><strong>FUENTES:</strong> <a href=\"http://www.phoronix.com/scan.php?page=article&amp;item=pascal-win10-linux&amp;num=1\"\
  \ target=\"_blank\">Phoronix</a></p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/4b9f9da50cf2f358abdcd4a4321104f9.webp
joomla_id: 217
joomla_url: opengl-vulkan-y-el-futuro-de-los-juegos-en-linux-steamos
layout: post
tags:
- steamos
- vulkan
- opengl
- linux
- directx
- phoronix
title: OpenGL, Vulkan y el futuro de los juegos en Linux/SteamOS
---
Reconozcamoslo, en el fondo somos unos románticos. Unos cuantos chicos "raros" con un sistema operativo diferente que se empeñan en no seguir la corriente a capa y espada. Seguro que en el fondo os sentís así. Seguro también que hace años, cuando todo esto de los juegos en Linux comenzó soñásteis con poder disfrutar de grandes títulos en vuestros ordenadores y poneros a la par de otros sistemas, sobre todo Windows. Y es cierto que la cosa ha cambiado considerablemente a mejor, pero aun así estamos lejos de poder mirarnos de tú a tú con los que escogieron el SO de Redmond.


 


Hace unos días pudimos leer un [artículo de Phoronix](http://www.phoronix.com/scan.php?page=article&item=pascal-win10-linux&num=1) en el que se comparaba el rendimiento de dos graficas Nvidia (la 1060 y la 1080) tanto en Windows 10 como en Ubuntu 16.10, y los datos no pueden ser más claros. Los números no mienten, el rendimento de nuestro sistema operativo arroja resultados en la mayoría de los juegos que dan mucho que pensar, y es que en la mayor parte de los juegos la diferencia de rendimiento entre un sistema y otro es notable, saliendo perdedor nuestro amado Linux. Si vemos la conclusión de este artículo vemos que salvo honrosas excepciones hablamos de un 60-70% de rendimiento con respecto a Windows, lo cual es para tener muy en cuenta.


 


Por ahora está claro que en cuanto a videojuegos no podemos competir ni presumir, y que es casi imposible que un gamer (entendiendo como tal a alguien que busca lo mejor para jugar) escoja GNU/Linux como plataforma para su afición (o profesión) si no tiene en cuenta otros factores. También está claro que los que jugamos en Linux, lo hacemos principalmente por eso, por que somos linuxeros, y como tales sacrificamos nuestra alma gaming en favor de un sistema operativo superior, más libre y más abierto; y eso tiene un coste que estamos dispuestos a asumir. Por lo tanto implica que al final jugar en nuestros PC's con GNU/Linux nos supone:


-**Mayor desembolso en Hardware**, ya que implica tener mejores componentes para poder disfrutar en igualdad de condiciones que un usuario de Windows.


-Si competimos con el mismo hardware vamos a tener que **bajar el nivel de detalle para no perder jugabilidad** con respecto a Windows.


-**Más limitación en el Hardware gráfico**. Se puede decir que si queremos jugar con garantías en Linux tenemos que tener una gráfica Nvidia. Los drivers para AMD o Intel por ahora son muy inferiores a los de Windows, aunque gracias a dios la cosa parece que está avanzando a buen paso, más por ímpetu de la comunidad y de Valve, que por los fabricantes de Hardware.


-**Menor cantidad y *"calidad"* de juegos**. Si bien cada vez nos podemos quejar menos por la cantidad, ya que recientemente se sobrepasó el número de 3000 títulos en Steam, es cierto que la cifra de Windows es muy superior. También entrecomillaba la calidad, ya que la gran mayoría de estos juegos son de tipo indie, habiendo muchisimos menos juegos considerados AAA, y de franquicias importantes.


-**El mercado se ciñe casi exclusivamente a Steam**, y en menor medida GOG e itch.io, por lo que si por la razón que fuere no nos interesa el hacer de alguna de estas tiendas tenemos posibilidades nulas de encontrar tiendas-sistemas alternativos de distribución. Por supuesto, la posibilidad de comprar juegos en formato físico prácticamente es inexistente.


 


 


![Imagen: Taringa](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/linux-vs-windows.webp "Imagen: Taringa")*Imagen: Taringa*
 


De todos modos, dentro de todas estas sombras, hay siempre alguna luz, ya que por ejemplo desde Phoronix tenemos confirmación de que Hitman tiene el [soporte para Vulkan parcialmente implementado](http://www.phoronix.com/scan.php?page=news_item&px=Vulkan-Signs-HITMAN), del mismo modo que Aspyr está [barajando usar Vulkan](http://steamcommunity.com/app/289070/discussions/0/144512942755435118/?ctp=27#c135509124602029681) entre otras opciones para mejorar Civilization VI, así como la propia Valve está volcada en mejorar la experiencia de juego en linux, y está [contratando gente](http://www.phoronix.com/scan.php?page=news_item&px=Arceri-Joined-Valve) para trabajar en los drivers. Por otra parte, también tenemos a Feral Interactive colaborando en la mejora de los drivers Mesa y ya ha trabajado en algunos parches y mejoras en este terreno. Con OpenGL llegando al límite de lo que puede dar de si, y buena muestra tenemos con los últimos desarrollos y ports que nos han llegado, Vulkan se postula como una pieza clave de cara al desarrollo y expansión del juego en sistemas Linux. De su buen desarrollo y su adopción como API de referencia para los desarrolladores, depende en gran medida la llegada en el futuro de los grandes juegos (AAA) a nuestro sistema favorito. Si Vulkan es capaz de llegar a ofrecer el rendimiento que se le supone, y puede poner a Linux al menos a la par del rendimiento que Windows tiene con DirectX12, tendremos argumentos para decir que hemos dado un paso definitivo para asentar a los sistemas Linux como alternativa completa a Windows.


 


También parece andar cerca la llegada de la realidad virtual a Linux, ya que desarrolladoras como Croteam están hablando de implementarlo junto a Vulkan en sus juegos. Hay que tener en cuenta también que si los esfuerzos tanto de Valve, algunas desarrolladoras y las compañías que realizan ports; inviertiendo tiempo y dinero en dar soporte a los usuarios de Linux y SteamOS, será porque, o bien les sale rentable, o bien ven que realmente es un mercado con potencial.


 


Lo que si está claro es que una empresa o un estudio de videojuegos puede hacer una prueba en un mercado para realizar un sondaje, pero si no funciona lo más normal es que lo abandone al poco tiempo y no siga insistiendo en ello. Teniendo en cuenta que Linux ya lleva unos años introducido en el mercado del videojuego, es de suponer que ya no se trata de esto último, sino de una apuesta seria por esta pequeña porción de la tarta y que además esta porción tiene potencial para crecer.


 


Este artículo es un artículo de opinión, y está escrito, y es suscrito por los redactores de JugandoEnLinux.com (Pato, Serjor y leillo1975). Si quieres participar en el debate puedes dejar un mensaje en los comentarios, o usar nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 


**FUENTES:** [Phoronix](http://www.phoronix.com/scan.php?page=article&item=pascal-win10-linux&num=1)

