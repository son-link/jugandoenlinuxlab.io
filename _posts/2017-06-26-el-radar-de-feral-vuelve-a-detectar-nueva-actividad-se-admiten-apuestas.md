---
author: Pato
category: Editorial
date: 2017-06-26 16:05:32
excerpt: "<p>\xA1Un nuevo port se nos viene encima! \xBFeres bueno con las adivinanzas?</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/ca9456ad89fef6c66a71b99b32dfe05e.webp
joomla_id: 389
joomla_url: el-radar-de-feral-vuelve-a-detectar-nueva-actividad-se-admiten-apuestas
layout: post
tags:
- proximamente
- feral-interactive
title: El radar de Feral vuelve a detectar nueva actividad, se admiten apuestas
---
¡Un nuevo port se nos viene encima! ¿eres bueno con las adivinanzas?

Esta tarde y casi de rebote nos hemos enterado de que Feral tiene un nuevo port entre manos para nuestro sistema favorito. Al principio jugaron un poco al despiste poniendo una imagen distinta de un juego para iOS:



> 
> Mysterious new signals on the Feral Radar!  
> Ah, just ignore it. That cranky old thing is probably just overheating.<https://t.co/v2d998T2GE> [pic.twitter.com/gHGa3cWQpr](https://t.co/gHGa3cWQpr)
> 
> 
> — Feral Interactive (@feralgames) [June 26, 2017](https://twitter.com/feralgames/status/879299655900835840)



pero luego repasando los otros OFNIS (Objetos Feral No Identificados) enseguida nos hemos dado cuenta de que otro port del que no teníamos noticias ha aparecido. La imagen que aparece es la siguiente:


![FeralOFNI260617](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Feral/FeralOFNI260617.webp)


Como puedes ver, aparece el frontal de un autobús de dos plantas y la leyenda: WEST NORTWOOD. Puedes ver el radar con el OFNI en su [página web oficial](https://www.feralinteractive.com/es/upcoming/).


¿Alguien se atreve a apostar qué juego puede ser el que nos traerán los chicos de Feral próximamente? ¿Propuestas? ¿Apuestas?


Cuéntanoslo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 

