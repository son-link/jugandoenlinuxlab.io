---
author: Pato
category: "Acci\xF3n"
date: 2017-01-12 19:36:33
excerpt: "<p>El juego promete soporte en Linux y pronto comenzar\xE1 su campa\xF1\
  a de Kickstarter</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/c2be3493ce42ee658842d93c6d54529d.webp
joomla_id: 173
joomla_url: dead-cells-una-mezcla-de-generos-muy-interesante-llegara-a-linux
layout: post
tags:
- accion
- indie
- plataformas
- roguelike
title: "'Dead Cells' una mezcla de g\xE9neros muy interesante llegar\xE1 a Linux"
---
El juego promete soporte en Linux y pronto comenzará su campaña de Kickstarter

![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Deadcellsbanner.webp)


Debo reconocer que soy seguidor de los juegos de tipo "Castlevania" y que cada vez que veo un juego del estilo que ahora nos ocupa se me escapa una sonrisa. El estudio francés Motion Twin [[web oficial](http://motion-twin.com/en/)] nos anuncia este 'Dead Cells' [[web oficial](http://dead-cells.com/)] que ellos describen como un juego de acción y plataformas inspirado en Castlevania y con esencias roguelike, lo que ellos llaman "roguevania". 


Entre sus características principales se encuentran 


Muerte permanente.


Juego estilo "Metroidvania".


Acción, aventura y plataformas.


Mecánicas de combate avanzadas con multitud de armas.


Elementos generados proceduralmente para que cada partida sea distinta. Nuevo castillo, nuevos enemigos, nuevos elementos, nuevos NPCs y un fuerte elemento de exploración hacia lo desconocido.


El juego se encuentra actualmente en desarrollo pero por lo que muestran en su teaser desde luego promete:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/sIFNlXk-n3Q" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


 Dead Cells llegará a Steam en una fecha aún por determinar. Y lo que a nosotros nos interesa: ¿llegará a Linux?


Según el propio estudio respondiendo a una de las preguntas en su [facebook](https://www.facebook.com/dead.cells.game) Dead Cells llegará a Linux tras su fase de acceso anticipado:


![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/social/Deadcelllinux.webp)


Por otra parte el estudio llevará a cabo una campaña en Greenlight de la que estaremos atentos:



> 
> We're going to Greenlight next week. Please follow and [#rt](https://twitter.com/hashtag/rt?src=hash) the hell out of us! Thank you! <https://t.co/BRI0xHGor7> [#gamedev](https://twitter.com/hashtag/gamedev?src=hash) [#indiedev](https://twitter.com/hashtag/indiedev?src=hash) [#steam](https://twitter.com/hashtag/steam?src=hash) [pic.twitter.com/qkzaDvGBuW](https://t.co/qkzaDvGBuW)
> 
> 
> — Motion Twin (@motiontwin) [10 de enero de 2017](https://twitter.com/motiontwin/status/818866999627386880)







¿Te gustan los juegos tipo "metroidvania"? ¿Que te parece este 'Dead Cells'?


Cuéntamelo en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD). 

