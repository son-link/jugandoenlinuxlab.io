---
author: Pato
category: "Acci\xF3n"
date: 2018-05-19 09:50:10
excerpt: <p class="ProfileHeaderCard-screenname u-inlineBlock u-dir" dir="ltr"><span
  class="username u-dir" dir="ltr">@Snapshot_Games lanza su segunda "Baker Build"</span></p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/597106f07a53c1d5cb7a9980c4c166ec.webp
joomla_id: 740
joomla_url: phoenix-point-se-actualiza-y-lanza-la-version-para-backers-de-linux-pero-retrasa-su-lanzamiento-actualizado
layout: post
tags:
- accion
- estrategia
- phoenix-point
- julian-gollop
title: "'Phoenix Point' se actualiza y lanza la versi\xF3n para backers de Linux pero\
  \ retrasa su lanzamiento (ACTUALIZADO)"
---
@Snapshot_Games lanza su segunda "Baker Build"

**ACTUALIZADO 4-7-18**: Ayer mismo nos llegaba la noticia de la salida de la segunda "build" para los que apoyaron el juego en su campaña de Financiación. En esta ocasión presenta [numerosas novedades](https://phoenixpoint.info/blog/2018/6/26/backer-build-two?utm_source=Phoenix+Point&utm_campaign=182b22713a-BACKER_BUILD_TWO_CTD&utm_medium=email&utm_term=0_0f91b4b9df-182b22713a-178016385&mc_cid=182b22713a&mc_eid=1f7eb10941) que harán las delicias de todos aquellos que apoyaron su proyecto, y que calmarán el ansia hasta que el juego vea la luz oficialmente. Entre estas nuevas características destacan:


-La posibilidad de poder usar un APC (Armadillo) para transportar de forma segura nuestros soldados, usar su torreta para atacar enemigos, y embestir obstaculos y enemigos.


-La inclusión de una nueva unidad, el Ingeniero, que puede instalar torretas, curar a compañeros y reparar su equipamento, y usar sus brazos robóticos para golpear cuerpo a cuerpo al enemigo.


-Más información sobre el daño producido por nuestros disparos.


-Posibilidad de rotar la cámara libremente, y no solo en ángulos de 90º


En JugandoEnLinux estamos en disposición de ofreceros pronto un gameplay de este juego para que veais como ha evolucionado desde la última vez que lo probamos. Os mantendremos informados.




---


**NOTICIA ORIGINAL:** Tras unos meses en los que el equipo de Snapshot Games ha estado trabajando en pulir el juego para eliminar los bugs y problemas mas acuciantes en Windows, por fin tenemos noticias sobre la versión para Linux de 'Phoenix Point'.


En el último post sobre el estado de desarrollo han anunciado que ya está disponible una versión del juego para Linux para todo el que apoyó el proyecto en su campaña de crowdfunding. Si eres de los que apostaron por este proyecto deberías repasar tus correos pues debería haberte llegado uno desde la cuenta [noreply@xsolla.com](mailto:noreply@xsolla.com) con el enlace personal de descarga de la versión 1.2 del juego para *backers*.


Por otra parte, también se ha anunciado el retraso del lanzamiento oficial del juego debido al propio proceso de desarrollo. Según reconocen en el propio anuncio "*el juego está teniendo aún una fuerte campaña de pre-compras, las espectativas de la gente son mayortes, nuestro equipo está creciendo y Phoenix Point se ha convertido en un juego más grande. Para poder alcanzar este potencial necesitamos retrasar la fecha objetivo de lanzamiento a Junio de 2019."*


Puedes ver el anuncio [en este enlace](https://phoenixpoint.info/blog/2018/5/14/release-announcement).


También puedes ver al mismo Julian Gollop hablando sobre todas las novedades del juego en este vídeo:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/8AWT4WO5VyE" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Por otra parte, ayer mismo tuvimos un stream en directo con Leo testeando esta *build.* Puedes verlo en este vídeo:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/eXw8FVT6ZR0" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


Si quieres más información sobre el juego, puedes visitar su [página web oficial](https://phoenixpoint.info/home/).


¿Que te parece Phoenix Point? ¿Te gustan los juegos que desarrolla Julian Gollop?


Cuéntamelo en los comentarios o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

