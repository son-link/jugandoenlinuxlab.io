---
author: leillo1975
category: Carreras
date: 2021-10-04 09:42:24
excerpt: "<p>La creaci\xF3n de pistas personalizadas y el multijugador ser\xE1n las\
  \ grandes bazas de <span class=\"css-901oao css-16my406 r-poiln3 r-bcqeeo r-qvutc0\"\
  >@raceleaguegame </span>.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/RaceLeague/RaceLeaguePoster.webp
joomla_id: 1372
joomla_url: raceleague-llegara-de-forma-nativa-a-linux
layout: post
tags:
- unity3d
- early-access
- raceleague
- next-fest
title: "RaceLeague llegar\xE1 de forma nativa a Linux"
---
La creación de pistas personalizadas y el multijugador serán las grandes bazas de @raceleaguegame .


 El último [Next Fest de Steam](https://store.steampowered.com/sale/nextfest) está dando mucho de si, poniendo en manos de los jugadores montones de demos de futuros juegos que veremos pronto en esta conocida tienda; y el juego que nos ocupa hoy es un buen ejemplo de ello. Aunque en este momento la [demo](https://store.steampowered.com/app/1565890/RaceLeague/) que podemos descargar solamente está disponible para Windows, debido a la falta de tiempo del desarrollador para lanzarla a tiempo para este evento; **la versión nativa está confirmada**, en primer lugar por el famoso iconito en la tienda de Steam, y después afianzado en un una [respuesta de su creador en los foros de Steam](https://steamcommunity.com/app/1565890/discussions/0/3056240878550324318/#c2957167122127030848) donde nos indica que estará a tiempo para el **Acceso Anticipado**, que se espera en algún momento entre este año y el siguiente.


![](https://race-league.com/images/presskit/presskit_1.jpg)


Antes de continuar conviene aclarar que [RaceLeague](https://race-league.com/) está siendo creado por una sola persona, **Jali Hautala**, un **desarrollador indie Finlandés**, con años de experiencia en el creación de videojuegos y como ingeniero de software; siendo todo el trabajo de modelado, texturas y programación suyo. El juego está desarrollado usando el conocido motor de videojuegos **Unity 3D**, por lo que debemos esperar pocos problemas para versión de Linux.


El juego, según su autor, está influenciado por [GeneRally](https://en.wikipedia.org/wiki/GeneRally) (2002) y [Stunts](https://es.wikipedia.org/wiki/Stunts) (1992), títulos que en su día permitían la creación de circuitos, así como el poder correr en ellos. La jugabilidad **trata de imitar de una forma realista las carreras de la vida real, pero intentando ponerlo al alcance de todo tipo de jugadores**, siendo sencillo de jugar desde el principio, pero premiando la práctica. Como acabamos de comentar, el juego hace hincapié en la **creación de pistas gracias a la inclusión de un potente editor**, así como la posiblidad de competir contra otros jugadores tanto en carreras multijugador, como intentando batir sus tiempos, gracias una **base de datos de pistas** donde compartir las creaciones. En este apartado se usará un sistema de clasificación ELO.  


![](https://race-league.com/images/presskit/presskit_10.jpg)


Podremos **visualizar las repeticiones** de nuestras partidas, e incluso ver una **análisis post-carrera** para ver en que puntos podemos mejorar . El juego cuenta además con unas **físicas  realistas y un sofisticado sistema de daños**, tanto en los coches como en los objetos de las pistas. En el caso de los vehículos, este no será solo visual, sinó que también tendrá impacto en las físicas y en el rendimiento. En este aspecto por ahora solo influye el daño en la suspensión, pero pronto lo hará también en el motor. Será posible utilizar **skinks personalizados** en los coches de carreras, y los podremos conducir de diferentes tipos, desde el Rallycross, hasta los monoplazas.  


Nosotros, desde JugandoEnLinux.com, vamos a seguir muy de cerca este desarrollo, y por supuesto os iremos acercando todas las novedades que vayan saliendo de este proyecto. Ahora os dejamos con este video en el que podeis ver algunas de las virtudes de este videojuego:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/XZ9cHPG25RA" title="YouTube video player" width="780"></iframe></div>


¿Qué os ha parecido el poder crear vuestros propios circuitos en RaceLeague? ¿Os atrae la posibilidad de competir en pistas creadas por otros jugadores? Si quereis contestarnos o dicirnos vuestra opinión sobre RaceLeague, hacedlo en los comentarios, en nuestro grupo de [Telegram](https://t.me/jugandoenlinux), o nuestras salas de [Matrix](https://matrix.to/#/#jugandoenlinux.com:matrix.org).

