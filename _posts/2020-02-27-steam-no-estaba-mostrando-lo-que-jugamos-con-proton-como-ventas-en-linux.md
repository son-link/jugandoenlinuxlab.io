---
author: Jugando en Linux
category: Noticia
date: 2020-02-27 22:02:20
excerpt: "<p><span class=\"username txt-mute\">@gamingonlinux </span> junto con algunos\
  \ desarrolladores descubren un problema en la forma de mostrar las ventas</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/headers/steam-wallpaper.webp
joomla_id: 1176
joomla_url: steam-no-estaba-mostrando-lo-que-jugamos-con-proton-como-ventas-en-linux
layout: post
title: Steam no estaba mostrando lo que jugamos con Proton como ventas en Linux
---
@gamingonlinux  junto con algunos desarrolladores descubren un problema en la forma de mostrar las ventas


Hoy nos hacemos eco de una noticia que nos afecta como plataforma. Según leemos en [gamingonlinux.com](https://www.gamingonlinux.com/articles/a-note-on-using-steam-play-proton-and-counting-the-sales-for-linux-updated.15965) Steam no estaría mostrando correctamente las ventas de los juegos que compramos desde Linux y ejecutamos con Proton.


Cuando Valve lanzó Protón para poder ejecutar los juegos mediante Steam Play la pregunta obvia era cómo se iban a tratar las ventas de los juegos de Windows que pudiésemos comprar desde nuestros sistemas Linux y ejecutarlos vía Proton. En aquel momento el incombustible Liam preguntó a Valve por la posibilidad de que dichos juegos contasen como ventas en Windows de cara a los desarrolladores aunque los estuviésemos jugando desde Linux.


[Valve respondió](https://www.gamingonlinux.com/articles/valve-officially-confirm-a-new-version-of-steam-play-which-includes-a-modified-version-of-wine.12400) que *según los algoritmos de Steam, **si el tiempo de juego mayoritario al final de las dos primeras semanas es bajo Linux, la venta contaría como Linux***. Proton entonces debería contar como Linux. Pero al parecer había un problema ya que las ventas no aparecían de forma correcta en los informes.


Liam junto a, al menos un desarrollador ha verificado por si mismo que **al comprar un juego solo disponible para Windows y ejecutarlo al 100% bajo Linux con Proton la venta de cara al desarrollador contó como venta en Windows.**


Al anunciar esto y correrse la voz en varios canales, el propio Pierre Loup uno de los principales desarrolladores del entorno Linux en Steam comentó en Twitter que **esto no debería ser así**, y que tendrán que echarle un vistazo, si bien "el equipo ahora mismo está centrado en la estabilidad y el rendimiento, por lo que puede tomar algo de tiempo".




> 
> Getting lots of pings about this: <https://t.co/uhvLp7Nb6a>  
>   
> That doesn't seem like intended behavior, we'll look into it. At this early stage, the team's focus is still on compatibility and performance, so it might take a little bit.
> 
> 
> — Pierre-Loup Griffais (@Plagman2) [February 10, 2020](https://twitter.com/Plagman2/status/1226998786846687233?ref_src=twsrc%5Etfw)



En otro artículo de [GoL](https://www.gamingonlinux.com/articles/steam-play-proton-is-correctly-tracking-linux-sales-a-statement-from-valve.16036) sobre el tema, Valve comunica que **el sistema de contabilización funciona bien**, pero hay un **problema en el filtrado de los resultados**. Según comenta [Pierre-Loup Griffais](index.php/component/search/?searchword=Pierre&ordering=newest&searchphrase=all). al parecer los desarrolladores y editores pueden acceder a **dos tipos de informes diferentes** para monitorizar un juego. Hay una **página de resumen por juego**; esa se actualiza bastante rápido. También hay un **detallado informe financiero mensual** que desglosa todos los pagos realizados a un editor, con cierto retraso. Al parecer **había un filtro que no separaba "Unidades e ingresos de Linux" de "Unidades e ingresos totales"** en la página de resumen por juego. Esto sólo ocurría cuando una aplicación no se podía lanzar a través de Linux, y **era puramente un problema de cómo la información era mostrada, no de cómo quedaba contabilizada.** 


En cambio, **si se comparan los informes mensuales se puede observar cómo las unidades vendidas se desglosan correctamente según el sistema operativo utilizado**, independientemente de si el juego es nativo o necesita de herramientas como Proton para ser ejecutado. Desde Valve han tomado cartas en el asunto y **han corregido el informe-resumen** para que muestre correctamente las unidades correspondientes a nuestro sistema de un juego ejecutado con Steam Play.


En otro orden de cosas, GoL aclaraba la forma de contabilizar que tiene Valve para sus ventas. Para que un juego sea considerado como comprado en nuestro sistema **es necesario comprarlo con el Cliente de Linux**, y **posteriormente jugarlo durante dos semanas en él**; y lo que es más importante, **si compramos a través del navegador o de la aplicación de Android**, el juego **quedará contabilizado como de Windows, a menos que lo juguemos exclusivamente en Linux** a lo largo de las dos semanas siguientes, por lo que si compras juegos de esta forma, **debes asegurarte de jugarlos antes de que se cumpla este tiempo** para que queden asignados como ventas para nuestro sistema.


Desde nuestro punto de vista, **este último punto debería ser corregido**, para que al menos si compramos desde el navegador detecte que usamos GNU-Linux, y que si usamos la apliación móvil tome como referencia nuestras [preferencias de plataforma](https://steamcommunity.com/games/221410/announcements/detail/1475356649450732547). Esperemos que los chicos de Valve tomen cartas en el asunto lo antes posible para que nuestras compras no vayan en detrimento de nuestro sistema favorito al menos de cara al número de ventas de videojuegos, cosa que los desarrolladores tienen siempre muy en cuenta de cara a ofrecernos soporte en sus desarrollos.


Fuente: [@Liam](https://twitter.com/thenaughtysquid/status/1230131486856224769) y [gamingonlinux.com](https://www.gamingonlinux.com/articles/a-note-on-using-steam-play-proton-and-counting-the-sales-for-linux-updated.15965)

