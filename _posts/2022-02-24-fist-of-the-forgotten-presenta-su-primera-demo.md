---
author: P_Vader
category: Plataformas
date: 2022-02-24 18:16:44
excerpt: "<p>Durante el Next Fest de Steam tendr\xE1s la oportunidad de probarla.</p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/FistOfTheForgotten/fist_of_the_forgotten.webp
joomla_id: 1435
joomla_url: fist-of-the-forgotten-presenta-su-primera-demo
layout: post
tags:
- indie
- plataformas
- demo
- godot
- blender
- next-fest
title: Fist of the Forgotten presenta su primera demo
---
Durante el Next Fest de Steam tendrás la oportunidad de probarla.


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube.com/embed/gJLReJ1HaPY" title="YouTube video player" width="720"></iframe></div>


Hace bastante tiempo que **sigo a [@jitspoe](https://twitter.com/jitspoe) en su canal de [Twich](https://www.twitch.tv/jitspoe/)** desde que me fijara en su juego a través de la web de Godot. Es uno de esos tipos geniales que decidió hacer su carrera como verdadero indie (de independiente) después de trabajar para diferentes estudios AAA. **Este "hombre orquesta" retransmite [cada día](https://www.twitch.tv/jitspoe/schedule) el desarrollo de su video juego, capaz de la programación en Godot, el diseño con Blender, o el marketing.** Si quieres ver el mundo del desarrollo indie, es una gran oportunidad de verlo en acción, aprender y apoyarlo en su aventura.


Después de **probar la [demo de Fist of the Forgotten](https://store.steampowered.com/app/1105470/Fist_of_the_Forgotten/) que ha presentado para el [Next Fest de Steam](https://store.steampowered.com/sale/nextfest) y llevarme un espléndido sabor de boca,** os invito a que la probéis y que no le perdáis la pista, porque **aunque** **está aún en desarrollo, creo que apunta para ser un indie de éxito.**


Fist of the Forgotten es un juego de **plataformas de precisión y combate muy basado en el impulso e inercia**, donde el movimiento fluido es clave. Los juegos de plataformas parten de diversos parámetros físicos donde su ajuste puede determinar que el juego enganche o no, en ese aspecto este juego me ha conquistado, el ir deslizándote en ocasiones es brillante. Cierto es que comienzas a ciegas, pero no tardarás en descubrir esas habilidades y trucos para ganar unos metros de salto.


![fist of the forgotten gameplay](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/FistOfTheForgotten/fist_of_the_forgotten_gameplay.webp)


Tiene **un estilo muy bien acabado, gráficos con siluetas y un efecto 2.5D que luce fantástico**. Aunque no lo parezca, no es fácil diseñar un estilo así y que sea atractivo. A la vista queda que se ve magnífico.


La historia vienen a ser la de una niña en un mundo post apocaliptico, con los restos de lo que parece una vieja civilización, llena de viejos robots que nos harán sudar en ciertos momentos. Conforme avanzamos vamos recolectando algunas de estas reliquias tecnológicas (con sus habilidades) e irás desentrañando la historia de ella con su familia.


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube.com/embed/VJrcr6itnkQ" title="YouTube video player" width="720"></iframe></div>


Las características principales son:


* Movimientos y animaciones fluidas, diseñadas para verse bien en pantallas de alta tasa de refresco a 100 fps (a diferencia de los clásicos juegos de plataformas bloqueados a veces a 30/60fps).
* Geometría y sombreadores altamente optimizados para un estilo visual con siluetas que debería de funcionar bien en hardware de gama alta y baja.
* Mecánica, como deslizarse agachado, para generar impulso (debería ser excelente para los speedrunners).
* Entorno intrigante e historia sin diálogos que permite a los jugadores elegir si quieren investigar el mundo o simplemente concentrarse en jugar con la mecánica.
* ¡Batallas de jefes!
* Coleccionables y secretos, por supuesto.


No tardéis en probar esta demo, solo tendréis hasta el próximo 28 de febrero que finaliza el Next Fest. Y sobre todo si os ha gustado el juego **no olvidéis añadirla a vuestros deseados para estar atentos a sus novedades**. Si todo va bien, es posible que el juego finalmente llegue a lo largo de este 2022.


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/1105470/" style="border: 0px;" width="646"></iframe></div>


 


¿Lo has probado? ¿Qué te ha parecido? Cuéntanoslo en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).


 

