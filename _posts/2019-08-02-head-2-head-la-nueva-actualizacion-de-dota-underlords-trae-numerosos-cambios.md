---
author: Serjor
category: Estrategia
date: 2019-08-02 16:43:31
excerpt: "<p>Nuevos modos de juego, cambios y correcciones en la \xFAltima actualizaci\xF3\
  n</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/42ef3937b979091cf46e2f58109b31fa.webp
joomla_id: 1093
joomla_url: head-2-head-la-nueva-actualizacion-de-dota-underlords-trae-numerosos-cambios
layout: post
tags:
- dota-2
- dota-underlords
title: "Head 2 Head, la nueva actualizaci\xF3n de Dota Underlords trae numerosos cambios"
---
Nuevos modos de juego, cambios y correcciones en la última actualización

Dota Underlords, el juego basado en un mod de un mod, presenta una de sus mayores actualizaciones desde su lanzamiento.


La novedad más destacable de esta actualización es cuando los combates tienen lugar. Hasta esta actualización los combates no estaban sincronizados, de tal manera que nosotros podíamos estar viendo un combate contra un rival, y nuestro rival podía estar viendo un combate completamente diferente, con resultados diferentes. A partir de ahora esto ya no será así, ahora el combate que vea cada combatiente será el mismo que verá su rival. En el único caso en el que esto será una excepción es cuando haya un número de jugadores impar, y en este caso uno de los jugadores jugará contra una escuadra clon de uno de sus rivales. Además ahora, cuando se están jugando los combates, la lista de posiciones de los jugadores se recompondrá en parejas para mostrar quién está jugando con quién.


Otra nueva incorporación es el nuevo sistema de rango que ya [habían anunciado](https://steamcommunity.com/games/underlords/announcements/detail/2707145128901426684) con anterioridad. Este nuevo sistema de rango ahora cuando se termina una partida ya sabremos cuántos puntos de rango obtendremos de antemano, ya que se basa en la posición en la que terminemos la partida, no depende del rango de los rivales.


Además, para facilitar entrenamientos y probar estrategias nuevas, hay dos modos de juego, un modo "Casual", donde no hay impacto en el rango, puedes emparejarte con cualquiera, y se pueden crear partidas con amigos, "Ranked" (con rango), donde tenemos justo lo contrario, el emparejamiento se hace en base del rango, en solitario, y por su puesto, nuestro rango se verá afectado, hacia arriba, eso sí, una vez alcancemos un nivel, no descenderemos.


También hay cambios estéticos en algunos héroes, ya que ahora en función del número de estrellas que alcancemos con los personajes, su skin cambiará para reflejar su nivel.


Y otra serie de cambios, nerfeos y ajustes en items que podéis encontrar en las [notas de la versión](https://steamcommunity.com/games/underlords/announcements/detail/1600386073106475922).


Y tú, ¿qué opinas de estos cambios? Cuéntanoslo en los comentarios o en nuestros canales de [Telegram](https://t.me/jugandoenlinux) y [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org)

