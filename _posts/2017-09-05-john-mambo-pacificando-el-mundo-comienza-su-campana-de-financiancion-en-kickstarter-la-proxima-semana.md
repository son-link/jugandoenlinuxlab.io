---
author: Serjor
category: "Acci\xF3n"
date: 2017-09-05 21:44:36
excerpt: "<p>John Mambo va pacificar el mundo, porque nadie m\xE1s estaba disponible</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/cca34d17b5186097267678a76444a0b0.webp
joomla_id: 455
joomla_url: john-mambo-pacificando-el-mundo-comienza-su-campana-de-financiancion-en-kickstarter-la-proxima-semana
layout: post
tags:
- beta
- kickstarter
- john-mambo
title: "(Actualizado) John Mambo, pacificando el mundo, comienza su campa\xF1a de\
  \ financianci\xF3n en kickstarter la pr\xF3xima semana"
---
John Mambo va pacificar el mundo, porque nadie más estaba disponible

Actualización: Ya está en marcha su campaña en [kickstarter](https://www.kickstarter.com/projects/2034244219/john-mambo).


No suele ser muy habitual que traigamos artículos de juegos en desarrollo, pero con el producto patrio haremos una excepción, y es que la gente de [Iction Games](http://www.ictiongames.com/) están desarrollando el juego [John Mambo](http://www.ictiongames.com/mambo/), un arcade 2D isométrico con un corte descaradamente retro en todos los sentidos, ya que tanto la ambientación como el arte gráfico son muy ochenteros/noventeros.


![poster john mambo](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/johnmambo/poster_john_mambo.webp)


Cómo decíamos, el juego está en plena fase de desarrollo, y el próximo día 12 de septiembre comienza su campaña en Kickstarter. Por lo que nos comentan la versión para GNU/Linux puede que llegue unos días más tardes respecto al inicio de la campaña ya que están teniendo algunos problemas en la exportación, y es que a pesar de que están usando Unity3D y de sus capacidades multiplataforma, siempre hay que hacer algún retoque final, por lo que si os interesa el juego y queréis colaborar en el desarrollo, os recomendamos que esperéis a la demo para Linux y podáis jugarla. Hay una versión disponible en este hilo de [reddit](https://www.reddit.com/r/linux_gaming/comments/6whiqt/john_mambo_pacifying_the_world/?st=j784e553&sh=0ffb5cd4) pero creo que es de alguna versión anterior, no estoy seguro de que esté actualizado.


![JohnMamboCompose](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/johnmambo/JohnMamboCompose.webp)


Respecto al juego, hemos podido probarlo y la verdad, es muy entretenido, así cómo divertido. Al menos la parte que hemos probado tiene un montón de referencias y bromas absurdas dignas de los mejores *aterriza como puedas* que cómo mínimo nos sacaran unas cuantas sonrisas mientras avanzamos pudiendo escoger diferentes caminos, alternamos entre diferentes armas o escapamos de las trampas del entorno.


Os dejamos con un gameplay bajo GNU/Linux:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/HaS23yZ5qH4?rel=0" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Y tú, ¿colaborarás en Kickstarter para que John Mambo pacifique el mundo? Cuéntanos qué te parece en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD)

