---
author: Serjor
category: "Simulaci\xF3n"
date: 2020-03-16 19:34:00
excerpt: "<p>Ahora no solamente podemos dise\xF1ar nuestra universidad ideal, sino\
  \ que podemos dise\xF1ar el juego para dise\xF1ar nuestra universidad ideal</p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/univercity/univercity.webp
joomla_id: 1191
joomla_url: univercity-celebra-su-version-1-0-liberando-el-codigo-fuente
layout: post
tags:
- simulador
- open-source
- codigo-fuente
- codigo-abierto
- univercity
title: "UniverCity celebra su versi\xF3n 1.0 liberando el c\xF3digo fuente"
---
Ahora no solamente podemos diseñar nuestra universidad ideal, sino que podemos diseñar el juego para diseñar nuestra universidad ideal


En [reddit](https://www.reddit.com/r/linux_gaming/comments/fin3j6/open_source_release_of_the_game_univercity_steam/?utm_source=share&utm_medium=web2x) podemos leer que el código fuente del juego UniverCity ha sido liberado, y que está accesible en [GitHub](https://github.com/Thinkofname/UniverCity/blob/master/README.md).


UniverCity es un building simulator con vista isométrica, en el que podremos diseñar y gestionar universidades. Es un Parkitech o Cities: Skylines, pero para el mundo universitario. No hemos podido jugar, así que no sabemos con qué nivel de detalle se pueden reproducir fielmente las fiestas universitarias, pero entendemos que si aún no se puede, ahora ya tenemos el código fuente disponible para añadir esta funcionalidad.


La verdad es que este tipo de acciones son siempre bienvenidas, y más cuando (y esto es un tema personal del que escribe estas líneas) el código fuente está escrito en Rust, un lenguaje de programación desarrollado por Mozilla, que no hay que confundir con el infame juego de Sucker Punch.


Eso sí, y volviendo al juego que nos ocupa, lo que se ha liberado, como decíamos al comienzo, es el código fuente, pero no así sus *assets*, que siguen siendo propietarios, por lo que si queremos jugar a una versión compilada por nosotros mismos, antes deberemos hacernos con el juego.


Ojalá más estudios se apuntaran a este tipo de iniciativas, donde el software es simplemente una herramienta que por sí sola no sirve de nada, pero que se puede utilizar si se desarrollan los recursos necesarios.


El juego se puede comprar en su página de [Steam](https://store.steampowered.com/app/808160/UniverCity/)


Y tú, ¿estás interesado en diseñar la universidad a la que fuiste o a la que te hubiera gustado ir? Cuéntanoslo en los comentarios o en nuestros canales de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org) y [Telegram](https://t.me/jugandoenlinux)

