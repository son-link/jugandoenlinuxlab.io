---
author: Pato
category: Estrategia
date: 2017-05-24 09:40:12
excerpt: "<p>El juego cruzado con los usuarios de Windows no ser\xE1 posible</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/d197c421d422f5cbf569ea13f09ef700.webp
joomla_id: 338
joomla_url: warhammer-40-000-dawn-of-war-iii-tendra-multijugador-entre-linux-y-macos
layout: post
tags:
- proximamente
- feral-interactive
- estrategia
- warhammer
- dawn-of-war
title: "'Warhammer 40.000 Dawn of War III' tendr\xE1 multijugador entre Linux y macOS"
---
El juego cruzado con los usuarios de Windows no será posible

 Como ya os anunciamos hace poco en Jugando en Linux, Warhammer 40.000: Dawn of War III' [llegará a nuestro sistema favorito el próximo día 8 de Junio](index.php/homepage/generos/estrategia/item/453-warhammer-4000-dawn-of-war-iii-tendra-version-para-linux), y siguiendo en la línea de los últimos "ports" de Feral, los usuarios de Linux y macOS podrán jugar en modos multijugador en línea entre ambos sistemas, pero no con los jugadores de Windows. Así nos lo han confirmado en su respuesta en Twitter:


 



> 
> [@JugandoenLinux](https://twitter.com/JugandoenLinux) That's right, the macOS and Linux versions won't have multiplayer against the Windows version as they use different multiplayer tech.
> 
> 
> — Feral Interactive (@feralgames) [May 23, 2017](https://twitter.com/feralgames/status/866942639983255552)



Esto se debe a la forma en la que trabajan los distintos sistemas en cuanto al código de red. En concreto algunos motores de juegos trabajan de forma distinta según el sistema en el que se ejecuten por lo que por ejemplo, los modos multijugador entre Linux y Windows pueden dar lugar a retardos pronunciados, pérdida de sincronía u otros efectos adversos con lo que el juego cruzado se hace imposible. Haría falta rediseñar gran parte del código de red del juego para que funcionase correctamente, cosa que no es sencillo o factible dado el esfuerzo necesario para ello. De todos modos Feral como siempre hará un buen trabajo y podremos disfrutar del juego con nuestros amigos siempre que sea en estos sistemas.







 


Teniendo en cuenta que el juego en cuestión lleva apenas un mes en el mercado no debemos tener problemas en encontrar jugadores con los que compartir "estratégia". ¿Verdad?


¿Tú que piensas? ¿Te gustaría poder jugar contra usuarios en Windows?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 

