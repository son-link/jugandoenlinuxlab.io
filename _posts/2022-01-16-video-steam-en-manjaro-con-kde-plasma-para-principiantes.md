---
author: P_Vader
category: "V\xEDdeos JEL"
date: 2022-01-16 19:42:44
excerpt: "<p><span class=\"style-scope yt-formatted-string\" dir=\"auto\">Peque\xF1\
  o tutorial b\xE1sico de como usar Steam en Manjaro KDE Plasma para reci\xE9n llegados\
  \ a Linux.</span></p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Videotutoriales/steam-manjaro-kde.webp
joomla_id: 1411
joomla_url: video-steam-en-manjaro-con-kde-plasma-para-principiantes
layout: post
tags:
- steam
- videos
- tutorial
- videotutorial
- manjaro
- kde
- plasma
title: "V\xCDDEO: Steam en Manjaro con KDE Plasma para principiantes"
---
Pequeño tutorial básico de como usar Steam en Manjaro KDE Plasma para recién llegados a Linux.



 


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/KUgeTEKD0eE" title="YouTube video player" width="780"></iframe></div>


 


 

