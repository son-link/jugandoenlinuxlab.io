---
author: leillo1975
category: Software
date: 2018-03-09 12:02:25
excerpt: "<p>Gabe Newell confirma que habr\xE1 juegos nuevos</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/4b0a3f295543bcdb7e84aacab1169fb5.webp
joomla_id: 673
joomla_url: parece-que-valve-va-a-volver-de-nuevo-al-software
layout: post
tags:
- software
- valve
- hardware
- gabe-newell
title: "Parece que Valve volver\xE1 al desarrollo de Software."
---
Gabe Newell confirma que habrá juegos nuevos

Acabamos de leer en [PC-Gamer](https://www.pcgamer.com/gabe-newell-hooray-valves-going-to-start-shipping-games-again/?utm_content=buffer72ea0&utm_medium=social&utm_source=twitter&utm_campaign=buffer-pcgamertw) que en la presentación del **nuevo juego de cartas basado en Dota2, Artifact**, [Gabe Newell](https://es.wikipedia.org/wiki/Gabe_Newell) aclaró que su empresa, Valve, está volviendo a desarrollar de nuevo juegos. Tras una etapa en la que la empresa ha estado más centrada en el Hardware, con proyectos como HTC Vive, Steam Controller, Steam Link o incluso el tema de las Steam Machines, la empresa parece que está retomando de nuevo la senda del desarrollo de software.


Valve en los últimos tiempos tiene bastante abandonada la creación de juegos, limitándose casi exclusivamente a mantener y dar soporte a sus títulos, y ciertamente se les echa de menos, ya no solo por el siempre mentado **Half Life 3**, sinó por su saga Portal o alguna nueva IP.  Según Newell, Artifact será el primero de una tandada de nuevos juegos, lo cual tranquiliza, aunque cierto es que un juego de cartas no es lo que está esperando la mayor parte de sus fans. A lo largo del año pasado se supo que se trabaja en **un juego de un solo jugador y algunos de realidad virtual**. Según sus palabras no se sabe si se refiere a estos últimos o realmente tiene más cartuchos en la recámara (esperemos esto último...)


Algo muy interesante que también dijo en dicha presentación es que en este momento tienen la capacidad de trabajar tanto en Hardware como Software al mismo tiempo, lo que puede dar una idea del gran potencial de la compañía en este momento. También, en este sentido,  ha afirmado que en el pasado se sentían celosos de "La Gran N" (Nintendo) por poder desarrollar juegos teniendo control total sobre el Hardware. Ahora Valve también puede hacer lo mismo...


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/DA5mY8XqrHU" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


¿Qué opinais de las palabras de Gabe Newell? ¿Qué esperais de Valve? ¿Os parece adecuada su comparación con Nintendo? Déjanos tu opinión en los comentarios o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

