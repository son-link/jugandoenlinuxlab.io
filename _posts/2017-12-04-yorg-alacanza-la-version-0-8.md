---
author: leillo1975
category: Carreras
date: 2017-12-04 08:29:33
excerpt: "<p>La nueva versi\xF3n de este juego Open Source incluye notables mejoras\
  \ y a\xF1adidos.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/4cd4eaee1c7cc4757d33459591114de7.webp
joomla_id: 564
joomla_url: yorg-alacanza-la-version-0-8
layout: post
tags:
- open-source
- yorg
- ya2
title: "YORG alcanza la versi\xF3n 0.8."
---
La nueva versión de este juego Open Source incluye notables mejoras y añadidos.

El estudio italiano Ya2, desarrolladores de YORG, acaba de lanzar al público su nueva revisión de este juego de carreras de **código libre**. Si recordais, hace un par de meses [escribimos un artículo](index.php/homepage/generos/carreras/item/580-yorg-un-juego-de-carreras-open-source) sobre este título y poco después publicamos una entrevista con [Flavio Calva](index.php/homepage/entrevistas/item/587-entrevista-a-flavio-calva-de-ya2-yorg), desarrollador principal del juego, donde nos desgranaba los detalles su creación y sus referentes. El anuncio de la publicación de esta nueva versión se ha hecho este fin de semana como podeis ver en este tweet:



> 
> We've released the version 0.8 of our free [#opensource](https://twitter.com/hashtag/opensource?src=hash&ref_src=twsrc%5Etfw) racing [#indiegame](https://twitter.com/hashtag/indiegame?src=hash&ref_src=twsrc%5Etfw)! <https://t.co/a9z9uLyVgo> [pic.twitter.com/0B0cfcPCJq](https://t.co/0B0cfcPCJq)
> 
> 
> — Ya2 technologies (@ya2tech) [December 2, 2017](https://twitter.com/ya2tech/status/937010554417352709?ref_src=twsrc%5Etfw)


   



Esta nueva release incluye importantes cambios que mejoran la experiencia jugable en gran medida y también varias novedades. Entre las más destacables podemos encontrar:



> 
> -Inclusión de un **nuevo circuito**, llamado Roma y con su correspondiente música.
> 
> 
> -Un **nuevo coche**, llamado Teia, añadido a los 7 ya existentes.
> 
> 
> -Se ha añadido la posibilidad de jugar **Sesiones**, donde podremos realizar un campeonato a lo largo de los 5 circuitos existentes, donde tendremos que ir ganando puntos para proclamarnos campeones y donde también podremos ir mejorando nuestro coche.
> 
> 
> -Los coches ahora **derrapan mejor** y son más sencillos de controlar.
> 
> 
> -Dos **nuevos idiomas**, como son el Alemán y el Gaélico Escocés.
> 
> 
> -El **sonido** de los coches se ha mejorado. Ahora , en los coches, podemos escuchar como cambian de marcha y no simplemente el incremento de revoluciones de estos.
> 
> 
> -Se ha mejorado la **cámara** que sigue a nuestro vehículo.
> 
> 
> 


 


Podeis ver la lista completa de cambios en el [anuncio](http://www.ya2.it/articles/yorg-08-has-been-released.html#yorg-08-has-been-released) de su página. También es importante decir que en la **próxima versión** van a intentar centrarse en el **desarrollo de un modo multijugador**, algo que sin duda daría un impulso importante a este proyecto. Desde JugandoEnLinux.com nos gustaría también agradecer al equipo de YORG la atención que nos han brindado en todo momento y por supuesto la **inclusión de un banner de nuestra web en el nuevo circuito de Roma**, GRACIAS!!!.  Os dejamos con video que hemos grabado repasando las nuevas características:


 <div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/PUc0SQSgkpk" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


Podeis descargar el juego y encontrar toda la información sobre este proyecto en [su página](http://www.ya2.it/pages/yorg.html#yorg). Nosotros os recomendamos que lo jugueis y envieis vuestro Feedback a los desarrolladores para que sigan mejorándolo.¿A qué esperais para disfrutarlo? 


Dejadnos vuestras opiniones sobre este proyecto Open Source en nuestros comentarios o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

