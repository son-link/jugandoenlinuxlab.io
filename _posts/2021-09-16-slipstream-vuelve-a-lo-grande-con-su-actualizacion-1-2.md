---
author: leillo1975
category: Carreras
date: 2021-09-16 18:24:59
excerpt: "<p>Su creador @ansdor acaba de hacerla p\xFAblica tras un periodo en beta.\
  \ </p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SleepStream/SlipStream12.webp
joomla_id: 1360
joomla_url: slipstream-vuelve-a-lo-grande-con-su-actualizacion-1-2
layout: post
tags:
- retro
- slipstream
- ansdor
- fna
- steam-deck
title: "Slipstream vuelve a lo grande con su actualizaci\xF3n 1.2"
---
Su creador @ansdor acaba de hacerla pública tras un periodo en beta. 


 Ya ha llovido desde que [en su día analizamos]({{ "/posts/analisis-slipstream" | absolute_url }}) este fantástico título de **carreras al más puro estilo retro-ochentero**, y su creador, [Ansdor](https://www.ansdor.com/), no ha parado de mejorarlo y mejorarlo hasta llegar a esta recien salida del horno [v.1.2](https://store.steampowered.com/news/app/732810/view/2967298318919195238).... y viene bien cargada de novedades muy, pero que muy interesantes. Según nos comenta, **realmente el juego podría numerarse como 2.0**, y a decir verdad estaría mucho más que justificado, ya que como vereis a continuación los cambios son tremendos:


**-Rescritura completa del juego:** El juego que en su día **fué desarrollado en libGDX/java, se ha "traducido" a [FNA](https://fna-xna.github.io/)/C#**. El proceso ha sido largo, y lo que en principio tomaría sobre 6 meses, al final fué más de un año, pero por el camino se ha aprovechado para desarrollar muchas más cosas que no estaban en la [versión anterior]({{ "/posts/slipstream-actualizado-con-soporte-multijugador" | absolute_url }}), y que ahora mismo comprobareis.


**-Nueva IA:** ha sido cambiada completamente y **no depende de "gomas elásticas"**, es decir ahora no es más fácil o difícil según la posición que tengas en la carrera, sinó que **está en igualdad de condiciones que el jugador**, usando sus mismos coches, mecánica, especificaciones, pueden usar el rebufo... Aunque aun no está afinada del todo y necesita un mejor balance, es mucho más divertido competir con ella, y también es más difícil, pero para ello se ha creado la siguiente nueva mecánica.


**-Posibilidad de rebobinar el juego:** algo así como los famosos flashbacks de los juegos de [Codemasters]({{ "/tags/codemasters" | absolute_url }}). Ahora, si cometemos un error, la nueva IA no nos va a perdonar, y esto implicaría que nos tomaría mucha ventaja y nos costaría horrores el volver a "pillarla". Para ello **podremos volver atrás en el tiempo 5 segundos**, y de esa manera podremos intentar tomar esa maldita curva de otra manera para no perder ventaja.


**-Se eleminan los niveles de dificultad y se sustituyen por "pesos":** Haciendo una comparación, ahora no hay niveles de dificultad, sinó "centímetros cúbicos", al estilo Mario Kart. Existen 3 pesos, **ligero, medio y pesado**, pero no es más fácil o difícil ganar en un peso determinado. Para que me entendais, si escojo el nivel ligero, lo coches serán más lentos pero acelerarán más rápido, y en el pesado será al revés. Es decir, será más fácil jugar en ligero, pero la dificultad de la IA será la misma, pues van en nuestras mismas condiciones.


**-Cambios en Stock Cars y Custom Cars:** antes, en el modo "single race" solo se podía competir con coches de fábrica, y en "Grand Prix" con coches modificados. A partir de ahora **en Grand Prix podremos competir también con Stock Cars**. Además las actualizaciones iban del 1 al 10, y ahora lo hacen del 1 al 100.


**-Cambios en las tablas de Clasificación Online:** debido a todos estos cambios ahora es necesario utilizar muchas más tablas para registrar los tiempos de los jugadores. **Cada pista ahora tiene 9 tablas de clasificación en línea**, una para cada categoría de peso, en cada modo: stock, personalizado y contrarreloj. También hay una nueva pantalla de Registros en el menú principal, que mantiene sus tiempos para cada pista / categoría / modo.


**-Nuevo arte y diálogo de personajes:** Todos los rivales tienen **nuevos diálogos y nuevos retratos**, creados por su amigo Víctor, quien también creó el arte de [Dandara: Trials of Fear.](https://store.steampowered.com/app/612390/Dandara_Trials_of_Fear_Edition/?snr=1_2108_9__2107)


**-Posibilidad de usar Mods:** Aunque no está probado del todo, y aún no abarca todos los aspectos del juego, pero en este momento **se pueden añadir automóviles, pistas, fondos y accesorios de paisaje proporcionados por el usuario**. Los personajes aun no están soportados, pero lo harán en un futuro, posiblemente a finales de este año. Se pueden utilizar Mods en los modos Carrera única, Bala de cañón y Contrarreloj.


**-Otros cambios menores como:**


* Alt + Enter ahora cambia entre pantalla completa / ventana.
* Los cambios gráficos en el menú de configuración ahora ocurren instantáneamente; no es necesario 'aplicar cambios'.
* El radar del juego ahora muestra tu distancia de los corredores / rivales adyacentes.
* La pantalla ahora se inclina cuando gira el automóvil.
* Screen Shake e Screen Tilt se pueden configurar desde el menú de pausa.
* Los iconos del teclado ahora muestran las teclas reales a las que están vinculados, no más sólo X / C.
* El modo Battle Royale ahora se puede jugar con 4, 8 o 12 corredores en lugar de siempre con 16.
* El efecto NTSC ha sido reemplazado por una versión más rápida / ligera.
* La vibración se ha reducido a una cantidad más sensible.
* Ranuras de guardado separadas para un jugador y multijugador en el modo Grand Prix.


 ![Derrape](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SleepStream/Derrape.webp)


Como comentábamos al principio estamos ante un **juego completamente renovado**, y debido a esto será también relanzado en otras plataformas. Su creador también comenta que muy posiblemente **funcione de maravilla en la Steam Deck** (eso es seguro, pensamos nosotros, y además le quedará que ni pintado). También nos cuenta cuando termine de corregir los errores de esta versión será el **fin del proyecto**, y dejará en manos de los moders la vida útil del juego, no descartando el lanzar en un futuro un paquete de contenido o expansión, pero **ahora quiere concentrarse en lo que será su próximo juego**, al que quiere dedicarse desde ya hace mucho tiempo.


Slipstream, como veis ha crecido en todos los sentidos con esta nueva versión, algo que beneficiará sin duda a todos sus jugadores, que son muchos, ya que el título ha vendido hasta ahora **15.000 unidades en Steam**, y más de **200.000 en el paquete del Humble Monthly**. Nosotros, como lo hicimos en su día no podemos más que recomendaros que os hagais con él si aún no lo teneis, porque estamos seguros que no os va decepcionar, y mucho más tras esta actualización. Si no estais seguros de lo que os vais a encontrar, [podeis leer nuestro análisis]({{ "/posts/analisis-slipstream" | absolute_url }}) y ver el stream que le dedicamos hace más de 3 años, ambos como es lógico, completamente desactualizados:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/LqkZebwbYyY" title="YouTube video player" width="780"></iframe></div>


 Si quereis adquirir Slipstream, podeis hacerlo en [Steam](https://store.steampowered.com/app/732810/Slipstream/), [Itch.io](https://ansdor.itch.io/slipstream) y [GOG.com](https://www.gog.com/game/slipstream). Si quieres decir que te parece este juego o tienes cualquier pregunta sobre él, hazlo en los comentarios o en nuestros grupos de JugandoEnLinux en [Telegram](https://t.me/jugandoenlinux) o nuestras salas de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

