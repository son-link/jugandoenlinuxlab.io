---
author: Pato
category: Rol
date: 2018-03-02 10:22:06
excerpt: "<p><span class=\"username u-dir\" dir=\"ltr\">@<b class=\"u-linkComplex-target\"\
  >IronOakGames </b></span>acaba de publicar el juego, que ya est\xE1 disponible para\
  \ nuestro sistema.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/116c4a6f4a9ff848cca05952e5c3c420.webp
joomla_id: 666
joomla_url: for-the-king-llegara-linux-steamos-tras-el-acceso-anticipado
layout: post
tags:
- indie
- rol
- estrategia
title: "El prometedor 'For the King' llegar\xE1 Linux/SteamOS tras el acceso anticipado\
  \ (ACTUALIZADO)"
---
@**IronOakGames** acaba de publicar el juego, que ya está disponible para nuestro sistema.

**ACTUALIZACION 19-4-18:** Finalmente, y tal y como os contábamos hace un mes y medio, los desarrolladores de "For The King", [IronOak Games](http://www.ironoakgames.com/), han liberado la versión para GNU-Linux/SteamOS. Aunque no se aprecia ninguna información sobre el soporte, en la página de Steam podemos ver el **Icono correspondiente**. El juego además cuenta con una **oferta de lanzamiento del 20%** por lo que podeis haceros con el por 15,99€:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/527230/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 




---


**NOTICIA ORIGINAL:** Otro juego de rol estratégico nos llegará próximamente. Hablamos de 'For the King', un título desarrollado por IronOak Games en el que tendremos que luchar para salvar el reino de Fahrul, ya que el rey ha muerto y la reina hace un llamamiento desesperado para que el caos no se expanda por todas partes. Tendremos que improvisar y ya sea en solitario o junto a otros amigos en cooperativo online tratar de salvar el reino de la inminente perdición.


*"For The King es una combinación desafiante de elementos de estrategia, JRPG Combate y Roguelike. Cada partida es única con mapas procedurales, misiones y eventos. Afronta los implacables elementos, lucha contra las malvadas criaturas, navega por los mares y ahonda en el oscuro inframundo. No hay nadie que haya regresado de su viaje. ¿Serás tú quien ponga fin al Caos?"*


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/7pw07YW2CZs" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 'For the King' se encuentra actualmente en fase de acceso anticipado solo para Windows y Mac, pero los desarrolladores han confirmado que el juego llegará a nuestro sistema favorito tras finalizar el acceso anticipado y sea lanzado oficialmente, lo que parece que sucederá a principios de Abril. Así nos lo han confirmado ellos directamente:



> 
> It sure is! It will be available at launch, early April.
> 
> 
> — IronOak Games (@IronOakGames) [March 1, 2018](https://twitter.com/IronOakGames/status/969301510084485120?ref_src=twsrc%5Etfw)



 Aún no sabemos qué requisitos necesitaremos para mover este 'For the King', pero si atendemos a lo que piden para otros sistemas, al menos podemos sacar en claro que necesitaremos al menos un procesador de doble núcleo de 1,4 a 2 Ghz, 4 - 8 Gb de RAM y como gráfica cualquier tarjeta integrada actual con más de 512 megas. En Jugando en Linux estaremos atentos a las novedades del juego así como la fecha de salida definitiva y los requisitos oficiales.


Si quieres saber más de este 'For the King' puedes visitar su [página web](http://www.ironoakgames.com/) oficial o visitar su página de Steam, donde estará disponible en español, como decimos a principios de Abril:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/527230/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 ¿Qué te parece este 'For the King'? ¿Te atrae la propuesta de un JRPG con tintes de acción y roguelike?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

