---
author: leillo1975
category: Rol
date: 2019-11-19 20:25:42
excerpt: "<p>El juego de @inXile_ent se lanzar\xE1 a mediados del pr\xF3ximo a\xF1\
  o.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Wasteland3/Wasteland3ingame.webp
joomla_id: 1132
joomla_url: wasteland-3-ya-tiene-fecha-de-salida-en-linux
layout: post
tags:
- rol
- inxile-entertainment
- wasteland-3
- brian-fargo
title: Wasteland 3 ya tiene fecha de salida en Linux.
---
El juego de @inXile_ent se lanzará a mediados del próximo año.


Gracias a [LinuxGameConsortium.com](https://linuxgameconsortium.com/wasteland-3-new-trailer-release-date/) nos llega la noticia de que la tercera parte de esta saga clásica de videojuegos verá la luz exactamente dentro de 6 meses, concretamente el 19 de Mayo del proximo año, el 2020.


Este juego, que sirvió de inspiración a otras sagas más conocidas como Fallout, o el reciente [Atom RPG](index.php/component/k2/7-rol/677-atom-rpg-post-apocalyptic-indie-game-llega-en-early-access), nació hace la friolera de 31 años, de la mano de [Brian Fargo](https://en.wikipedia.org/wiki/Brian_Fargo), entre otros. Este mismo desarrollador, fundador de [Interplay](https://es.wikipedia.org/wiki/Interplay_Entertainment) y posteriormente de [InXile Entertaiment](https://www.inxile-entertainment.com/) (Torment: Tides of Numeria, The Bards Tale), es el responsable tanto de la segunda parte, que vió la luz en 2014 y que dispone de soporte nativo, como ahora de la que en este artículo nos ocupa. Wasteland 3 contará con las siguientes [características](https://www.inxile-entertainment.com/wasteland3):


*-Un juego de rol basado en grupos, con un enfoque renovado en nuestra característica historia de reactividad compleja y combate estratégico.*


*-Añadiendo un vehículo de jugador, peligros ambientales y un sistema de acción renovado y más fluido, estamos evolucionando en el combate táctico por turnos de Wasteland 2 y el diseño de encuentro único.*


*-Juega solo o en cooperativo con un amigo en una experiencia basada en historias donde tus elecciones abrirán (o cerrarán) oportunidades de misión, áreas para explorar, arcos de historias y más.*


*-Su base de* *Ranger* *es una parte central de la experiencia. A medida que ayuda a la gente local y establece una reputación en Colorado, las misiones y la narrativa lo obligarán a tomar decisiones sobre cómo liderar.*


*-Ubicado en las tierras salvajes del congelado Colorado, donde la supervivencia es difícil y nunca se garantiza un resultado feliz. Los jugadores enfrentarán decisiones morales difíciles y harán sacrificios que cambiarán el mundo del juego.*


*-Wasteland 3 presentará una historia profunda y atractiva utilizando un nuevo sistema de diálogo, con toda su voz.*


Para ir abriendo el apetito os dejamos con el espectacular e intenso trailer del juego:



¿Has jugado a los anteriores juegos de Wasteland? ¿Qué te parece lo que podemos ver en el trailer?Cuéntanoslo en los comentarios, o en mensajes en nuestra comunidad de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

