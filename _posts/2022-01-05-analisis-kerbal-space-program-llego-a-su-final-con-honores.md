---
author: P_Vader
category: "An\xE1lisis"
date: 2022-01-05 20:23:49
excerpt: "<p>Hace unos meses que se anunci\xF3 la versi\xF3n final 1.12 y queremos\
  \ hacerle un merecido repaso a este hist\xF3rico juego.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/KerbalAnalisis/kerbal_space_program_portada.webp
joomla_id: 1404
joomla_url: analisis-kerbal-space-program-llego-a-su-final-con-honores
layout: post
tags:
- espacio
- simulador
- aviones
- construccion
- ksp
- kerbal-space-program
title: "AN\xC1LISIS: Kerbal Space Program lleg\xF3 a su final con honores"
---
Hace unos meses que se anunció la versión final 1.12 y queremos hacerle un merecido repaso a este histórico juego.


A lo largo de estos años me ha llamado la atención Kerbal Space Program (KSP) creado por el estudio mexicano Squad. Quien lo jugaba a menudo lo vanagloriaba y de hecho a día de hoy, después de una década, día tras día siempre hay retransmisiones del [juego en Twitch](https://www.twitch.tv/directory/game/Kerbal%20Space%20Program) e infinidad de vídeos en [Youtube](https://www.youtube.com/results?search_query=kerbal+space+program) con guías, análisis o partidas de lo mas variopintas.


Vuelvo a mi curiosidad inicial **¿Qué tenía este juego para que tanto ruido hiciese?** Hace unos cuantos años probé su demo sin mucho interés, completé los tutoriales y me agradó. No entendía la pasión que despertaba o quizás no estaba captando la idea. Reconozco que en una oferta finalmente lo compré y estuvo aparcado bastante tiempo.


Hace unos [meses **anunciaron**](https://www.kerbalspaceprogram.com/release/kerbal-space-program-1-12-on-final-approach-is-now-available/) **que después de 10 años, KSP llegaba a su versión final** y tan solo se dedicarían a corregir fallos. Parece que se van a centrar en el desarrollo de Kerbal 2, proyecto mucho mas ambicioso que el primero y que está requiriendo de mucho trabajo.


Continuando con mi experiencia, con el anuncio de su aniversario y final, **decidí analizarlo mas en profundidad y ver si realmente valía tanto la pena**.


![kerbal space program1](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/KerbalAnalisis/kerbal_space_program1.webp)


KSP se desarrolla en el planeta [Kerbin](https://wiki.kerbalspaceprogram.com/wiki/Kerbin/es), habitado por los kerbals. Unos seres algo estúpidos pero que no se niegan al avance como especie. Ya te podrás imaginar que **el juego tiene un tinte cómico, pero ojo, no quita que se base en verdadera y pura ciencia.** Su sistema solar se asemeja al nuestro y algunos planetas o satélites copian a los nuestros pero con ligeras diferencias, Kerbin que vendría a ser la Tierra, tiene dos satélites en lugar de uno.


En resumen **KSP trata de gestionar una agencia espacial. También tendrás de desarrollar y construir los prototipos que necesites y por último llevar a cabo las misiones necesarias con ellos. Una autentica carrera espacial.** Como podrás adivinar toca muchos géneros, gestión de recursos, construcción y simulación. Puede parecer abrumador, pero es manejable por cualquier usuario.


Dispone de tres modos de juego:


* El **modo carrera** que es el mas completo, en el que tienes que hacer de gestor de la agencia espacial e ir desarrollando el apartado científico y de ingeniería. Pasa por contratar astronautas, científicos, ampliar instalaciones, cumplir contratos, hacer investigación o por supuesto explorar el espacio exterior. Toda una carrera espacial.
* El **modo ciencia** solo se centra en ir desarrollando el apartado científico e la ingeniería del juego. Conforme exploras y experimentas vas acumulando puntos de ciencia y adquiriendo o descubriendo piezas mejores y complejas. Este sería el modo para quien le gusta mas la construcción y simulación, y no se preocupa tanto por el apartado de la gestión de recursos.
* El **modo sandbox**, es un modo donde todo está desbloqueado y sin limites. Este modo te puede ayudar a descubrir que vas a necesitar en un futuro para hacer según que cosa o simplemente para divertirte construyendo sin mas preocupación. Se pueden hacer autenticas maravillas.


El primer punto fuerte que me encontré con **KSP es que tiene una comunidad enorme de modders y fuertemente apoyada por sus desarrolladores**. El estudio ha sabido mantener en su web una **muy buena [base de mods](https://www.curseforge.com/kerbal/ksp-mods), un [enorme foro](https://forum.kerbalspaceprogram.com/) con mucha información y una [wiki](https://wiki.kerbalspaceprogram.com/wiki/Main_Page)** propia que es un lujo para quien se quiera interesar hasta en el mas ínfimo dato científico del juego.


También dentro del juego puedes acceder a **las naves que la comunidad crea y comparte en [steam wrokshop](https://steamcommunity.com/app/220200/workshop/).** Te podrás imaginar que encuentras autenticas genialidades ¿Quieres probar una estación espacial o el mismísimo halcón milenario?


![kerbal space program2](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/KerbalAnalisis/kerbal_space_program2.webp)


Y es que amig@s en este juego, con todo ese aspecto cómico, **deberás APRENDER a calcular y entender como funciona la ciencia aeroespacia**l, con mas o menos ayuda de los asistentes del juego ¡no necesitas ser un ingeniero de la NASA!. Y **recalco lo de aprender por que creo que es la grandeza de este juego, necesitas aprender ciencia real, física real, conceptos, maniobras, términos, que realmente se usan en una misión espacial ¡y lo mejor es que no es difícil!**Igualmente la parte de simulador es exactamente lo mismo, realmente es un simulador con sus físicas cercanas a la realidad. Lógicamente la dificultad se ha recortado para poder jugarlo sin llegar a la frustración.


**Grandísimo acierto de este juego**, **el saber despertar esa curiosidad por aprender jugando** ¿te suena? La necesidad innata en el humano de aprender mediante la curiosidad, o sea jugando. Lo han logrado. Un DIEZ. Con todo ello, este juego debería regalarse en las escuelas. ¿Tienes niños o sobrinos? Regálales este juego y ponte con ellos a jugarlo.


Os diŕe que **desde que juego a KSP, en alguna ocasión en casa hemos seguido algún despegue de la NASA o mas recientemente el lanzamiento del telescopio James Webb y ¡sabemos de que hablan! Comentamos el despegue como si de un evento deportivo se tratase**. ¿No es increíble que sepamos a que se están refiriendo cuando hablan de etapas, orbitas o Delta-V?


![kerbal space program5](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/KerbalAnalisis/kerbal_space_program5.webp)


Continuando con el contenido del juego tiene el plus de que **puedes hacer alguna misión [histórica de la ESA](https://www.esa.int/About_Us/Partnerships/Kerbal_includes_Ariane_5_and_real_ESA_missions_for_gamers) como la famosa misión Rosseta** donde tienes que llegar a aterrizar el pequeño modulo Philae en su superficie. Todo un reto sin lugar a dudas y que **recomiendo hacerlas una vez alcances cierta experiencia en el juego**.


El juego se puede complementar con dos DLC:


* **Breaking Ground Expansion**, con muchas piezas robóticas nuevas para desarrollo y amplia el desarrollo en las superficies. Un DLC muy bien valorado.
* **Making History** te permite recrear misiones historicas como las del famoso Apollo, editar tus propias misiones e igualmente un conjunto de nuevos añadidos para los astronautas.


En resumidas cuentas, **KSP no es el juego que mejores gráficos o música tenga**, puede ser un poco frustrante si no te lo tomas con paciencia, pero se suple con esa sensación plena que te produce cuando eres capaz de desarrollar algo grande. Las **dopaminas se disparan con ese placer de crear y aprender, te lo aseguro.** Que un tema tan minoritario como **la simulación espacial, hayan sido capaces de llevarlo al gran público es un éxito rotundo.**


Y por todo esto que os explicado, puedo decir ahora sin equivocarme, que **Kerbal Space Program es una de los mejores juegos que han pasado por mi ordenador.** Empecé a probarlo para analizarlo en JugandoEnLinux y llevo **mas de 100 horas sin parar.**


A modo de curiosidad os dejo los añadidos que tengo, incluidos nuestra espectacular insignia y bandera de JugandoEnLinux para poner en el juego:


* En **sonido uso Chatterer** que añade el tipico sonido de radio entre los astronautas y control. Ambiente espacial asegurado.
* **Mejoras visuales**, este conjunto de mods le da un aspecto y efectos visuales muy realistas. Los recomiendo si tienes un ordenador con CPU y GPU solventes, ya que pueden llegar a repercutir en los FPS. En mi caso uso EVE Redux, SVE y Module Manager
* **Mapa de Delta-V**, es un PDF muy util para hacerte un calculo de que gasto necesitarás en cada etapa de un viaje a cualquier punto.
* **Nuestras exclusivas banderas de JugandoEnLinux** para plantar en la [Muna](https://wiki.kerbalspaceprogram.com/wiki/Mun/es).


![kerbal JEL flag white](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/KerbalAnalisis/kerbal_JEL_flag_white.webp)


Archivos e instrucciones las puedes obtener en nuestro repositorio:   
<https://gitlab.com/jugandoenlinux/kerbal-en-jugandoenlinux>


¿Has jugado a Kerbal? ¿Qué tal fue tu expeirncia? Cuentanoslo en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org)


 

