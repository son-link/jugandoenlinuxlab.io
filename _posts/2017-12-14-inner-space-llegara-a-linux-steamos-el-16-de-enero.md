---
author: Pato
category: Aventuras
date: 2017-12-14 18:39:41
excerpt: "<p>El juego creado por&nbsp;PolyKnight Games ser\xE1 editado por Aspyr Media</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/fdde1da287ef645878d672d35edb2d9e.webp
joomla_id: 577
joomla_url: inner-space-llegara-a-linux-steamos-el-16-de-enero
layout: post
tags:
- aventura
- proximamente
- exploracion
title: "'Inner Space' llegar\xE1 a Linux/SteamOS el 16 de Enero"
---
El juego creado por PolyKnight Games será editado por Aspyr Media

Así es, 'Inner Space' es un juego con un cuanto menos vistoso apartado artístico y visual, que llegará a nuestro sistema favorito el próximo día 16 de Enero tal y como ha confirmado Aspyr Media en su tweet:



> 
> [#InnerSpaceGame](https://twitter.com/hashtag/InnerSpaceGame?src=hash&ref_src=twsrc%5Etfw) lands Jan. 16th! Check out the new trailer now and wishlist it on [@steam_games](https://twitter.com/steam_games?ref_src=twsrc%5Etfw) TODAY! <https://t.co/ukcr5FS5ik>
> 
> 
> — Aspyr Media (@AspyrMedia) [December 14, 2017](https://twitter.com/AspyrMedia/status/941367580123697153?ref_src=twsrc%5Etfw)



'Inner Space' es un juego de vuelo un tanto atípico según las palabras del estudio:



> 
>  *InnerSpace no es como otros juegos de vuelo. Queremos recompensar la exploración, tanto en términos de narración como en la mecánica de vuelo. Si esperas un simulador de vuelo tradicional con combate aéreo, puede que este juego no sea para ti. Si te atrae la idea de un avión que se transforma en un submarino y después se interna en el estómago de un antiguo semidiós, este es tu juego.*
> 
> 
> 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/PFrs_wHT1z8" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 Para hacernos una idea de lo que puede dar de si, sus características son:


* Descubre el Inverso: InnerSpace es un juego de exploración y de experiencias, tanto pequeñas como grandiosas, que crea el propio jugador. Tómate tu tiempo, surca los cielos y sumérgete en los océanos... los secretos del Inverso se revelarán ante ti.
* Reliquias del pasado: hay valiosas reliquias diseminadas entre las ruinas. Son los últimos mensajes de las civilizaciones extintas que en su día dominaron el Inverso.
* Fuselajes de los Predecesores: adapta tecnologías perdidas para construir fuselajes nuevos con capacidades únicas para volar por encima de las aguas o sumergirte en ellas.
* Un misterio colosal: el Inverso está al borde de la desaparición, pero no estás solo. Los semidioses siguen recorriéndolo, atesorando el poder que todavía conservan y guardando secretos tan antiguos como el propio Inverso. Vuela con cuidado.
* El arte del volar: con un estilo gráfico etéreo y una relajante banda sonora, InnerSpace ofrece una experiencia de vuelo evocadora e introspectiva diferente a todas las demás.


En cuanto a los requisitos del sistema, el juego pide un mínimo de:



+ **SO:** Ubuntu 16.04 / SteamOS
+ **Procesador:** AMD Phenom II X4 925 (4 Cores, 2.8 GHz) O Intel Core i3 3210 (2 Cores, 3.2 GHz)
+ **Memoria:** 8 GB de RAM
+ **Gráficos:** Nvidia GPU GeForce GTX 660 / AMD GPU Radeon HD 7870
+ **Almacenamiento:** 5 GB de espacio disponible


Si te interesan los juegos relajantes de exploración, tendrás este 'Inner Space' traducido al español disponible en su página de Steam donde ya puedes añadirlo a tu lista de deseados:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/347000/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>
 ¿Qué te parece 'Inner Space'? ¿Te gustan los juegos relajantes?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


