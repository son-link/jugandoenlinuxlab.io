---
author: P_Vader
category: Software
date: 2021-12-28 19:02:25
excerpt: "<p>La tienda alternativa de Epic lanza su versi\xF3n 2.0 celebrando su segundo\
  \ aniversario con muchas cambios.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/HeroicGamesLauncher/Heroic_Games_launcher_2.0.webp
joomla_id: 1400
joomla_url: llego-heroic-games-launcher-2-0
layout: post
tags:
- software-libre
- epic-game-store
- heroic-games-launcher
title: "Lleg\xF3 Heroic Games Launcher 2.0"
---
La tienda alternativa de Epic lanza su versión 2.0 celebrando su segundo aniversario con muchas cambios.


La tienda alternativa de Epic Games Store y software libre publica hoy **su versión 2.0, coincidiendo con su segundo aniversario desde** su primera publicación. Celebran además que han llegado a las 120.000 descargas de su tienda, suponemos que mayormente desde sistemas operativos no Windows.


También **tiene una nueva página web <https://www.heroicgameslauncher.com/>** actualmente con una información básica y que se irá mejorando poco a poco. Informan que el código de la página también es libre.



 Respecto a los cambios principalmente han sido en el aspecto visual, destacando sobre todo el **cambio de la barra de navegación superior por una barra lateral**. El cambio se acerca al estilo de la aplicación original de Epic. También **han remodelado la página para mostrar la ficha del** **juego**:
![Heroic Games Launcher juego](https://user-images.githubusercontent.com/26871415/147581885-7bd7a0f9-4bf7-4cbf-9dca-b00f247e9111.png)


Otro cambio importante a mi parecer es que **ya no será necesario obtener el código SID desde la web de epic para iniciar sesión.** Por fin podrás iniciar sesión como en la aplicación original con tu usuario y contraseña.
![Heroic Games Launcher inicio de sesión](https://user-images.githubusercontent.com/26871415/147582001-df640925-2536-4fe0-8885-a5be5c7af3d4.png)


Estos son los cambios desde la última versión:


* **Nuevo diseño con una barra lateral** en lugar de una barra superior.
* **Nuevo diseño para las páginas de juegos**
* **Nuevo sistema de inicio de sesión**, ya no es necesario utilizar el identificador SID. Inicias sesión directamente desde Epic Store dentro de Heroic. La tienda iniciará sesión automáticamente si usa el nuevo sistema de inicio de sesión (no lo hará si ya inició sesión en Heroic. Además, si desea cerrar la sesión de Heroic, primero cierre la sesión de la tienda; de lo contrario, Heroic iniciará sesión nuevamente usando sus credenciales en la tienda ya que ambas páginas usan las mismas cookies.
* Se modificó la forma en que se instalan los juegos. Ahora, tanto la página del juego como la tarjeta del juego abrirán el mismo cuadro de diálogo de instalación con la ruta para elegir dónde instalar o un botón para importar un juego.
* El Unreal Market está oculto de forma predeterminada ahora, hay un interruptor en la configuración para mostrarlo. (no hagas eso si tienes una gran selección de sockets, ahora mismo Heroic se congela si tienes 2000 sockets o más, si esto sucede, edita el archivo de configuración manualmente y configura la opción en false y luego reinicia Heroic.
* Los registros del juego ahora contendrán información del sistema (hardware, SO, etc.) y la configuración del juego.
* Se agregaron botones para borrar la caché de heroic y restablecer heroic por completo.
* Agrega los idiomas estonio, finlandés, búlgaro y farsi
* Se agregó la selección de Wine prefix en la instalación del juego.
* Accesibilidad mejorada para navegar por la interfaz con solo un teclado
* Legendary actualizado a la versión 0.20.22
* Varios remodelados, mejoras y optimizaciones llevan a Heroic a consumir menos recursos como CPU y RAM. **En Linux, Heroic consume alrededor de 100 MB o RAM, mientras que en Windows usa alrededor de 200 MB.**


Corrección de errores:


* No se estaba desinstalando DXVK al desactivar la configuración de instalación de DXVK.
* Winecfg, winetricks y 'ejecutar exe' no estaban usando el binario de wine correcto cuando usaban Proton.
* Corrige algunos juegos que muestran información de actualización falsa.


Verdaderamente esta tienda alternativa cada vez está más cerca de alcanzar a la original. ¿La habeis probado ya? Cuéntanoslo en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

