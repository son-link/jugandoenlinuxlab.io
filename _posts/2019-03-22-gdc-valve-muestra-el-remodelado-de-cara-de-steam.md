---
author: Pato
category: Software
date: 2019-03-22 18:10:58
excerpt: "<p>La plataforma avanza nuevas caracter\xEDsticas para los pr\xF3ximos meses</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/dcb1882b5d2da82c4f1ecb54d9b9de6d.webp
joomla_id: 1003
joomla_url: gdc-valve-muestra-el-remodelado-de-cara-de-steam
layout: post
tags:
- steam
- valve
- gdc
title: 'GDC: Valve muestra el remodelado de cara de Steam'
---
La plataforma avanza nuevas características para los próximos meses

Ayer tuvo lugar en la GDC la conferencia de Valve en la que se esperaban novedades sobre Steam y cualquier otra "línea de negocio" que tuvieran en mente. Si bien era una conferencia enfocada a los desarrolladores, si que desvelaron algunas novedades interesantes que vamos a repasar.


En primer lugar, Valve anunció que **abrirá su propia red a desarrolladores externos**, para que todos los asociados que lo deseen puedan beneficiarse de toda la potencia en enrutamiento y servidores que tienen y que supondrá una ventaja sustancial para el juego online al facilitar la carga de red, el enrutamiento prioritario y la protección contra ataques que ahora mismo dispone Steam.


Por otra parte, y entrando ya en materia **Valve anunció un** -necesario- **lavado de cara en cuanto a su tienda de Steam**, mostrando novedades importantes comenzando con el nuevo aspecto de lo que será nuestra librería de juegos:


![libraryoverviewgdc](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SteamStoreNew/libraryoverviewgdc.webp)


Como podéis ver, el cambio es evidente y muy enfocado en reducir elementos para mostrar cuanta más información mejor. Destaca la actividad de tu comunidad de amigos a la parte derecha. También es interesante ver que los juegos se agrupan por etiquetas quedando la interfaz mucho mas limpia. También podemos filtrar nuestros juegos por diversos elementos como las etiquetas para una búsqueda más adecuada y "certera"... ¿Quién busca gatos?...


![steamgdc2](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SteamStoreNew/steamgdc2.webp)


Para terminar con la sección de biblioteca, una vez que entramos en detalle de los juegos que tengamos, podremos ver mucha más información relevante para saber qué novedades hay al respecto, si tenemos amigos jugando, si podemos conseguir logros o cromos etc...


![steamgdc 18 33 16](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SteamStoreNew/steamgdc_18-33-16.webp)


Pasando ya a la novedad más destacada, Steam tendrá una sección "eventos" donde los desarrolladores podrán publicar todas las novedades respecto a sus juegos y los usuarios suscritos podrán recibir toda esa información relevante directamente en esa sección además de poder recibirla a través de email. Eventos como retransmisiones en vivo, torneos , actualizaciones etc, tendrán cabida en esta sección, además de próximos anuncios o lanzamientos etc...


![steamgdc 18 33 24](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SteamStoreNew/steamgdc_18-33-24.webp)


 


Entrando más en detalle, una vez que entremos en una sección de "eventos" tendremos a nuestra disposición toda la información que los desarrolladores de ese juego en concreto consideren interesante y podremos llevar a cabo diferentes interacciones...


![eventdetail](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SteamStoreNew/eventdetail.webp)


Por último, enseñaron como será una vista general de "próximos eventos" en los que se puede ver como estarán separados por categorías según nuestro seguimiento o posesión:


![steamgdc 18 33 30](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SteamStoreNew/steamgdc_18-33-30.webp)


Y eso fue todo. Lo cierto es que nos dejó una cierta sensación de que faltó cierto "punch", algo más de pegada que nos hiciera pensar en los planes de futuro de la compañía mas allá de un par de opciones para desarrolladores y un lavado de cara de Steam, del que no lo enseñaron todo, ya que por ejemplo no mostraron el aspecto que tendrá la propia tienda, el apartado de la comunidad o el perfil de usuario. Habrá que esperar a próximas fechas para saber más sobre los planes de Valve y Steam.


¿Qué te parecen las novedades que presentaron en la GDC? ¿Piensas que los cambios de Steam serán el principio de un cambio mayor?


Cuéntamelo en los comentarios, o en el canal de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux)

