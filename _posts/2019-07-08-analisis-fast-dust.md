---
author: leillo1975
category: "An\xE1lisis"
date: 2019-07-08 17:56:13
excerpt: "<p>Veamos con detalle el juego de Binary Giants</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/e1479c60fc7d7f4f708d42f869f837c8.webp
joomla_id: 1043
joomla_url: analisis-fast-dust
layout: post
tags:
- carreras
- analisis
- unity3d
- fast-dust
title: "An\xE1lisis: Fast Dust."
---
Veamos con detalle el juego de Binary Giants


Cuando se judga siempre hay que tener toda la información y variables bajo control. Por supuesto hay algo muy importante, que es el contexto. Si nos basáramos tan solo en las primeras impresiones puede que fuesemos objetivos, pero no justos. Y es que todo influye en la percepción que podemos tener de las cosas. Si me dejase llevar por mis primeras impresiones en "Fast Dust" os diría que no es más que un juego normalito con bastantes cosas que mejorar, pero no sería, como os dije antes justo. Incluso podría cebarme con la traducción de su nombre a nuestro idioma haciendo un chiste fácil. El caso es que al menos a mi no me gusta quedarme con un mal sabor de boca, y al igual que con [Apocryph]({{ "/posts/analisis-apocryph" | absolute_url }}), decidí ser constructivo y esperar un poco antes de escribir este análisis. 


Os preguntareis a que me refiero con esto, y basicamente es que trás jugar un rato encontré muchas cosas que no funcionaban como deberían y que debían ser corregidas. Si os fijais en el [video](https://youtu.be/zd_PUg1QAzg) que grabamos cuando el juego sacó una versión nativa para nuestro sistema, os dareis cuenta de lo que quiero decir. El caso es que antes de escribir decidí ponerme en contacto con el desarrollador, al cual **estamos muy agradecidos por facilitarnos la clave que nos ha permitido hacer este análisis**, y comentarle todas las cosas susceptibles de mejora en el juego. La respuesta fue inmediata y **de una forma muy proactiva fué corrigiendo poco a poco los problemas más acuciantes del juego**.


![FastDust Desierto](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/FastDust/analisis/FastDust_Desierto.webp)


*El diseño post-apocaliptico de los coches es uno de los aspectos más destacables del juego*


Estos problemas estában fundamentalmente en una traducción al español muy mejorable (en la que tomamos parte para corregirla), problemas con el salvado de los ajustes del sistema, detección correcta de dispositivos de control, habilitando los volantes Logitech en Linux, así como la asignación de ejes y botones. También se realizaron trabajos en el rendimiento, la habilitación de Vulkan ( **parámetro de lanzamiento "-force-vulkan"** ) para un desempeño más rápido y estable, y correcciones en la físicas en elementos tales como las suspensiones, la marcha atras..... Como veis el desarrollador se tomó muy en serio todo el feedback que le enviamos para mejorar en gran medida su juego antes de escribir este artículo, y creo que **todo ese esfuerzo hay que tenerlo en cuenta y valorarlo muy positivamente**. Después de toda esta aclaración previa, vamos ahora  a por el análisis propiamente dicho del juego. 


"Fast Dust" es un **juego de coches arcade** enmarcado en un futuro donde se ha electrificado la movilidad y los coches son autónomos, prohibiéndose los vehículos impulsados por combustibles fósiles y conducidos por personas. Debido a esto algunos "resistentes" deciden crear una especie de **liguilla clandestina de carreras con viejos coches de combustión modificados para tal efecto**. Bajo esta premisa se desarrolla el juego en el **modo historia** que nos permitirá avanzar en las posiciones del ranking de 155 conductores en diferentes tipos de pruebas. Como es de esperar empezaremos con un coche básico y al fondo de la tabla, donde comenzaremos nuestra andadura.


El juego se desarrollará basicamente en tres escenarios diferentes, como son el **bosque**, la **ciudad industrial** abandonada o el **desierto**. En cada uno de estos escenarios habrá varios trazados entre las diferentes fases, así como **multitud de atajos que tomar** que nos permitirán tomar ventaja en las carreras. Hay que decir que quizás los escenarios necesitarían un poco más de trabajo para personalizarlos un poco más y encontrar elementos que ofreciesen un poco más de variedad y vistosidad al jugador. Para ofrecer un poco más de diferenciación entre las carreras, además de los diferentes trazados, **se han habilitado efectos meteorológicos tales como la lluvia, la noche o la niebla**, elementos que están bastante bien resueltos que ofrecen un plus de dificultad en las carreras cuando están presentes.


![FastDust Nieve](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/FastDust/analisis/FastDust_Nieve.webp)


*Las condiciones meteorológicas nos impondrán mayor dificultad en las pruebas a superar*


Mientras superamos una a una las diferentes carreras de la historia iremos descubriendo los modos que componen el juego, pudiendo disputar carreras **estandar** contra otros competidores; **eliminación**, donde el último en pasar por los checkpoints queda eliminado; **cuenta atrás**, donde tendremos que llegar a meta antes de que se consuma un tiempo determinado; y **checkpoints**, donde tendremos que cruzarlos para que nos den tiempo extra para continuar hasta el final. Entre carrera y carrera, nuestro manager Harry nos irá dando información sobre nuestra clasificación y los datos de la próxima prueba.


En ciertas pruebas tendremos la opción de realizar **apuestas** que nos permitiran ganar un dinerito extra, y en ocasiones también **se puede apostar el coche**, aunque debemos saber que si fallamos perderemos el nuestro. El dinero que ganemos nos servirá para poder comprar **mejoras para nuestro coche** entre las que encontraremos **velocidad**, **aceleración** y **Turbo** (yo diría más bien Nitro). Con él también podremos comprar **mapas con atajos** para los circuitos que nos serán tremendamente útiles para arañar unos benditos segundos en las carreras. Gracias a los coches que compremos o ganemos y todas estas mejoras, poco a poco iremos avanzando en la tabla de clasificaciones hasta llegar así al ansiado primer lugar, donde finalizaremos este modo. Hay que decir que podremos ir salvando poco a poco nuestro progreso, lo que nos será muy útil si no queremos dar pasos atrás, perder dinero, o incluso la posibilidad de seguir avanzando.


En el juego también podremos encontrar el modo **Carrera Rápida**, donde podremos **configurar las pruebas a nuestro gusto**, escogiendo coche y circuito entre todos los disponibles; y también entre los diferentes modos (estandar, eliminiación, cuanta atrás y checkpoints), así como el **número de oponentes**. Tambien podremos jugar **partidas Online** de hasta siete jugadores, pudiendo unirte a partidas o hospedar las tuyas propias. He de decir que las veces que me conecté nunca encontré nadie con quien jugar, por lo que este modo no podría calificarlo.


![FastDust Vista](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/FastDust/analisis/FastDust_Vista.webp)


*Podremos escoger entre múltiples vistas según nuestros gustos y preferencias, incluida la interior*


En cuanto al **apartado técnico** hay que decir que los gráficos no son malos, pues tanto circuitos, como coches, como efectos no están mal diseñados, pero si **se echa en falta algo más de detalles en la  personalización** de estos que le quiten ese aspecto tan plano que tiene el juego, y que hace que en determinados momentos sea repetitivo. En cuanto el **rendimiento**, cierto que está creado con **Unity3D** y eso es un lastre, y aun más en nuestro sistema operativo; pero es aquí donde encontramos el mayor problema, pues **el nivel de frames por segundo no es muy alto**, mejorando muy poco cuando usamos vulkan. Sería muy **necesario un proceso de optimización** que consiguiese incrementar sustancialmente el desempeño del juego.


En el **apartado sonoro**, los efectos de sonido son correctos, pero el sonido del motor, a pesar de que fué mejorado, sigue siendo demasiado grave y poco realista. Si hablamos del apartado musical, la banda sonora es poco variada y demasiado repetitiva, aunque hay que decir en favor de ella que es obra también del propio desarrollador y que está poniendo cartas en el asunto para mejorarla y ampliarla.


Si hablamos de la **jugabilidad**, es aquí donde encontramos el **punto fuerte del juego**, pues la competitividad de los bots y las pruebas está bastante bien ajustada según vayamos subiendo en el ranking, siendo cada vez más dfícil superar las carreras  si no disponemos de coches más potentes y mejoras, y por supuesto de más habiidad. Hay que decir que **las reacciones y físicas de los coches**, que son completamente arcade, **han mejorado muchísimo** desde que jugamos la primera vez, corrigiendo el desarrollador muchos aspectos que hacían que el juego fuese mucho más difícil e incontrolable. Dispondremos también de varias cámaras para adaptar a nuestro gusto nuestra vista favorita o con la que nos sintamos más cómodos.


![FastDust Noche](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/FastDust/analisis/FastDust_Noche.webp)


*De noche no todos los gatos son pardos....*


Como os comentaba al principio, no sería justo juzgar de buenas a primeras, y aunque a Fast Dust no es un juego perfecto, hay que tener en cuenta que está **desarrollado por una sola persona**, la cual ha sido receptiva en todo momento al feedback que le hemos facilitado para mejorar en enorme medida su software. Puede que en un futuro no muy lejano podamos ver otro título de **Binary Giants** en nuestro sistema, y que muy probablemente gracias a lo aprendido en Fast Dust sea un juego mucho mejor acabado y redondo. Mientras tanto, si os gustan los juegos de coches arcade, Fast Dust os puede proporcionar unas cuantas horas de diversión.


Podeis comprar Fast Dust en la [tienda de Steam](https://store.steampowered.com/app/891330/Fast_Dust/) (ahora mismo de oferta). Os dejamos con este gameplay que hemos grabado exclusivamente para esta ocasión:


 <div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/ubHzNtrq9cU" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


 ¿Qué os parece este Fast Dust? ¿Lo habeis jugado? Cuéntanoslo en los comentarios o deja tus mensajes en nuestra comunidad de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

