---
author: leillo1975
category: "An\xE1lisis"
date: 2017-09-09 16:00:00
excerpt: "<p>Cuando era peque\xF1o no ten\xEDa videoconsola. Me ten\xEDa que conformar\
  \ con ir a jugar a casa de la vecina a una portentosa NES donde solo pod\xEDamos\
  \ jugar a Super Mario Bros, Kung Fu Master y poco m\xE1s, y es que si ahora los\
  \ juegos nos parecen caros, en aquella \xE9poca ya ni te cuento. De Wonderboy recuerdo\
  \ su recreativa, el chico rubio con taparrabos que tiraba hachas y saltaba de nube\
  \ en nube. Despu\xE9s con el paso de los a\xF1os, en los tardios noventa, me resarc\xED\
  a jugando a infinidad de ROMS entre los que se encontraba tanto este como su tercera\
  \ parte, The Dragon's Trap. Del nombre del emulador no me acuerdo, pero si de la\
  \ habilidad (o maldici\xF3n) que sufr\xEDa el protagonista de convertirse en diferentes\
  \ personajes. No se si eran mejores o peores tiempos, pero los recuerdo con mucho\
  \ cari\xF1o.</p>\r\n<p>&nbsp;</p>\r\n<p>...Y de eso va este remake de Wonder Boy\
  \ III, de rescatar del baul este gran juego y traerlo a nuestra \xE9poca con la\
  \ cara lavada. Casualmente. en esto son expertos los chicos de <a href=\"http://www.dotemu.com/\"\
  \ target=\"_blank\" rel=\"noopener noreferrer\">Dotemu</a>, y por eso hoy estamos\
  \ escribiendo este an\xE1lisis. Con el bagaje de traer a nuestros d\xEDas renovadas\
  \ versiones de juegos tan ilustres como <strong>Pang</strong>, <strong>Double Dragon\
  \ Trilogy</strong>, o <strong>R-Type</strong>, en esta ocasi\xF3n han encargado\
  \ la tarea de hacerle m\xE1s que un lifting al estudio franc\xE9s <a href=\"http://www.lizardcube.com/\"\
  \ target=\"_blank\" rel=\"noopener noreferrer\">Lizardcube</a>, formado principalmente\
  \ por <a href=\"https://twitter.com/ocornut?lang=es\" target=\"_blank\" rel=\"noopener\
  \ noreferrer\">Omar Cornut</a> (en otra hora conocido por su fant\xE1stico emulador\
  \ de Master System <a href=\"http://www.smspower.org/meka/\" target=\"_blank\" rel=\"\
  noopener noreferrer\">MEKA</a>) y el dise\xF1ador de comics y animador <a href=\"\
  https://twitter.com/benfiquet\" target=\"_blank\" rel=\"noopener noreferrer\">Ben\
  \ Fiquet</a>. Por cierto, muchas gracias a todos ellos por facilitarnos una copia\
  \ del juego para realizar este an\xE1lisis.</p>\r\n<p>&nbsp;</p>\r\n<p style=\"\
  text-align: center;\"><img src=\"images/Articulos/AnalisisWonderBoy/WonderBoyTheDragonsTrap-B000.jpg\"\
  \ alt=\"WonderBoyTheDragonsTrap B000\" width=\"800\" height=\"450\" class=\"img-responsive\"\
  \ style=\"display: block; margin-left: auto; margin-right: auto;\" /> <em>Los gr\xE1\
  ficos dibujados a mano son una verdadera obra de arte. \xA1Parece que vemos dibujos\
  \ animados!</em></p>\r\n<p>&nbsp;</p>\r\n<p>Hechas ya la presentaciones de tan meritorias\
  \ compa\xF1\xEDas y personajes vamos a pasar al juego. Cuando uno se enfrenta a\
  \ la tarea de actualizar un juego de esta edad a lo que tenemos en nuestros d\xED\
  as puede optar por dos caminos. El primero, uno f\xE1cil, que ser\xEDa ampliar resoluciones,\
  \ mejorar algunas texturas y meter alg\xFAn extra, y que todo ello funcione sin\
  \ problemas en m\xE1quinas actuales. El segundo camino es m\xE1s complejo, y consiste\
  \ en redise\xF1ar casi por completo casi todos los aspectos del juego excepto su\
  \ mec\xE1nica y manteniendo su jugabilidad y esencia. Este \xFAltimo y valiente\
  \ paso es lo que han hecho con resultado sobresaliente los desarrolladores. En Wonderboy:\
  \ The Dragon's Trap no encontraremos los mismos gr\xE1ficos, arte y sonido que en\
  \ el ochentero (aunque en realidad tambi\xE9n), sin\xF3 que veremos un juego \"\
  dibujado\" completamente de arriba a abajo.</p>\r\n<p>&nbsp;</p>\r\n<p>Si hablamos\
  \ de Wonder Boy: The Dragon's Trap, hablamos de <strong>un juego de plataformas,\
  \ con tintes de rol</strong>, donde deberemos mover a nuestro/s personaje/s por\
  \ multitud de escenarios con la ayuda de espada, saltos y diversos ataques especiales.\
  \ Para empezar, es necesario precisar que podremos escoger entre nuestro heroe de\
  \ siempre o jugar con una heroina (<strong>Wonder Girl</strong>). En los primeros\
  \ compases del juego moveremos a nuestro personaje por un castillo hasta alcanzar\
  \ el primer dragon (Mekadragon), que una vez derrotado nos lanzar\xE1 una maldici\xF3\
  n que nos convertir\xE1 en un peque\xF1o drag\xF3n que tiene como habilidad el lanzar\
  \ llamaradas a distancia. Nuestra primera misi\xF3n consiste en salir vivos del\
  \ castillo en llamas, y una vez abandonado este, llegar a la aldea, que ser\xE1\
  \ nuestra \"base\" y desde donde partir\xE1n el resto de aventuras del juego. En\
  \ ella encontraremos una armer\xEDa que nos vender\xE1 nuestras primeras espadas,\
  \ armaduras y escudos mejorados; una clinica donde podremos curarnos y una iglesia.</p>\r\
  \n<p>&nbsp;</p>\r\n<p style=\"text-align: center;\"><img src=\"images/Articulos/AnalisisWonderBoy/WonderBoyTheDragonsTrap-B001.jpg\"\
  \ alt=\"WonderBoyTheDragonsTrap B001\" width=\"800\" height=\"450\" class=\"img-responsive\"\
  \ style=\"display: block; margin-left: auto; margin-right: auto;\" /></p>\r\n<p\
  \ style=\"text-align: center;\"><em>Una \"terrible\" maldici\xF3n nos convertir\xE1\
  \ en los m\xE1s dispares animales.</em></p>\r\n<p>&nbsp;</p>\r\n<p>Como antes dije,\
  \ desde la aldea partir\xE1n todas las misiones del juego, y es desde aqu\xED desde\
  \ donde retomaremos la aventura si nos matan, perdiendo todos los avances y objetos\
  \ que hayamos encontrado por el camino. Esta \xFAltima caracteristica de la aventura,\
  \ es la que le otorga al juego un plus de dificultad y la que permite que el juego\
  \ nos dure un poco m\xE1s, ya que si no se nos har\xEDa excesivamente corto. No\
  \ perderemos el dinero que hayamos encontrado ni por supuesto los objetos de inventario\
  \ que hayamos comprado, lo cual nos va a permitir mejorarnos para afrontar de nuevo\
  \ la zona donde hayamos caido.</p>\r\n<p>&nbsp;</p>\r\n<p>En cuanto a zonas, aparte\
  \ del castillo inicial, podremos viajar por la playa, el desierto, la jungla ....\
  \ y en cada una de ellas habr\xE1 lugares donde solo podremos acceder si disponemos\
  \ del personaje adecuado, obligandonos a recorrer los mismos escenarios varias veces\
  \ al m\xE1s puro estilo Metroidvania. Y es en esta caracter\xEDstica donde encontramos\
  \ de verdad la \"chicha\" del juego. Cada personaje tiene unas habilidades \xFA\
  nicas que le permitir\xE1n realizar acciones muy diferentes. Podremos ser, adem\xE1\
  s de <strong>Wonderboy</strong> (o <strong>WonderGirl</strong>), como dije antes\
  \ un <strong>Drag\xF3n</strong> (escupe fuego a distancia y puede agacharse), un\
  \ <strong>Rat\xF3n</strong> (puede trepar por las paredes y entrar en zonas peque\xF1\
  as), una <strong>Pira\xF1a</strong> (puede moverse libremente en el agua), un <strong>Le\xF3\
  n</strong> (posee una fuerza sobrehumana que puede romper elementos del escenario),\
  \ y por \xFAltimo un <strong>Halc\xF3n</strong> (recorre los escenarios volando\
  \ para acceder a zonas alejadas facilmente).</p>\r\n<p>&nbsp;</p>\r\n<p><img src=\"\
  images/Articulos/AnalisisWonderBoy/WonderBoyTheDragonsTrap-B009.jpg\" alt=\"WonderBoyTheDragonsTrap\
  \ B009\" width=\"800\" height=\"450\" class=\"img-responsive\" style=\"display:\
  \ block; margin-left: auto; margin-right: auto;\" /></p>\r\n<p style=\"text-align:\
  \ center;\"><em>En ciertos momentos tendremos que cambiar nuestra forma para poder\
  \ afrontar los diferentes escenarios</em></p>\r\n<p>&nbsp;</p>\r\n<p>Dispondremos\
  \ como arma de una <strong>Espada</strong> (excepto el drag\xF3n que lanza fuego)\
  \ que nos permitir\xE1 golpear a los enemigos. Cada vez que lo hagamos, nuestro\
  \ personaje se quedar\xE1 quieto, lo cual puede ser un poco fustrante al principio\
  \ pues implica \"reaprender\" a jugar este tipo de juegos. Como antes comentaba,\
  \ tambi\xE9n tenemos <strong>ataques especiales</strong> que iremos recolectando\
  \ a medida que derrotamos enemigos. Estos pueden ser un<strong> tornado</strong>\
  \ (elimina los enemigos a nuestro alrededor), una <strong>llamarada</strong> (se\
  \ carga lo que tengamos delante en un radio), un <strong>boomerang</strong> (que\
  \ podemos recuperar si no acertamos),<strong> flechas</strong> (atacan a enemigos\
  \ que est\xE9n encima nuestra), o un <strong>rayo</strong> (todos los enemigos de\
  \ la pantalla).</p>\r\n<p>&nbsp;</p>\r\n<p>En cuanto a los gr\xE1ficos del juego,\
  \ es justo decir que el remake es realmente asombroso, ya que desprende belleza\
  \ en multitud de detalles. Con un aspecto<strong> inspirado en los comics</strong>\
  \ el juego posee un arte en el cual est\xE1n muy bien conjuntados tanto los personajes,\
  \ enemigos como los propios escenarios, encajando todo a la perfecci\xF3n. Hay que\
  \ decir que el trabajo de Ben Fiquet <strong>dibujando a mano</strong> todos estos\
  \ elementos es soberbio y constituye una de las mayores virtudes de este t\xEDtulo.\
  \ Tanto es as\xED que parece que est\xE1s viendo una serie o pel\xEDcula de dibujos\
  \ animados.</p>\r\n<p>&nbsp;</p>\r\n<p>Si algo se nota en esta obra es el respeto\
  \ con el que han tratado el original, y hablando de eso mismo, tan solo tenemos\
  \ que pulsar un bot\xF3n y de pronto <strong>nuestro fant\xE1stico juego se convierte\
  \ en el juego original</strong> que todos record\xE1bamos, con sus sprites enormes\
  \ con su recortada paleta de colores;&nbsp; y la m\xFAsica y efectos de 8 bits,\
  \ lo cual es algo muy de agradecer, ya que podemos terminarnos el juego de cabo\
  \ a rabo como si lo hiciesemos hace 30 a\xF1os. Puedes ver a que nos referimos en\
  \ el siguiente video:</p>\r\n<p>&nbsp;</p>\r\n<p><iframe src=\"https://www.youtube.com/embed/qwy351pLXas\"\
  \ width=\"800\" height=\"450\" style=\"display: block; margin-left: auto; margin-right:\
  \ auto;\" allowfullscreen=\"allowfullscreen\"></iframe></p>\r\n<p>&nbsp;</p>\r\n\
  <p>Si hablamos de a <strong>m\xFAsica y los efectos de sonido</strong>, en el modo\
  \ remasterizado, las melodias son las mismas que en el original, solo que se han\
  \ actualizado y mejorado, <strong>cambiando estas seg\xFAn el escenarios donde nos\
  \ encontremos</strong>. Por poner un ejemplo de esto, si vamos al escenario de la\
  \ playa, la m\xFAsica sonar\xE1 caribe\xF1a, y en el desierto tendr\xE1 tintes egipcios.\
  \ Podemos, por supuesto, comprobar la diferencia pulsando el bot\xF3n y compararlo\
  \ con el sonido original. El juego se encuentra <strong>completamente traducido\
  \ al espa\xF1ol en cuanto a textos</strong>, ya que no tenemos voces que doblar;\
  \ por lo tanto no tendremos ning\xFAn problema en seguir el juego, aunque realmente\
  \ tampoco supondr\xEDa ning\xFAn impedimento importante el que no estuviese en nuestro\
  \ idioma.</p>\r\n<p>&nbsp;</p>\r\n<p>Son muy pocos peros los que se le pueden poner\
  \ a este juego, pero si hay alguno es la duraci\xF3n de este y el tener que atravesar\
  \ los mismos escenarios varias veces hasta llegar a las zonas nuevas, pero esto,\
  \ como es l\xF3gico, forma parte de la restauraci\xF3n que se ha hecho, y si se\
  \ quiere ser fiel a original es necesario respetarlo. De todas formas, se trata\
  \ de un t\xEDtulo con un precio muy accesible, por lo que tampoco es un inconveniente.</p>\r\
  \n<p>&nbsp;</p>\r\n<p>Como conclusi\xF3n hay que decir que WonderBoy: The Dragon's\
  \ Trap es una <strong>m\xE1gnifica obra de arte</strong> donde podemos rememorar\
  \ tiempos pasados sin necesidad de sacrificar las posibilidades de la tecnolog\xED\
  a actual. Un gran juego para jovenes y \"viejos\" que nos transportar\xE1 a tiempos\
  \ un tanto diferentes. Felicidades y gracias, Lizardcube y DotEmu.</p>\r\n<p>&nbsp;</p>\r\
  \n<p>Os dejamos con el trailer del juego y un peque\xF1o gameplay que realizamos\
  \ para vosotros:</p>\r\n<p>&nbsp;</p>\r\n<p><iframe src=\"https://www.youtube.com/embed/jsPmoQeW6Qk\"\
  \ width=\"800\" height=\"450\" style=\"display: block; margin-left: auto; margin-right:\
  \ auto;\" allowfullscreen=\"allowfullscreen\"></iframe></p>\r\n<p>&nbsp;</p>\r\n\
  <p><iframe src=\"https://www.youtube.com/embed/15sm6OnWF9c\" width=\"800\" height=\"\
  450\" style=\"display: block; margin-left: auto; margin-right: auto;\" allowfullscreen=\"\
  allowfullscreen\"></iframe></p>\r\n<p>&nbsp;</p>\r\n<p>Puedes comprar Wonder Boy:\
  \ The Dragon's Trap en Steam:</p>\r\n<p>&nbsp;</p>\r\n<p><iframe src=\"https://store.steampowered.com/widget/543260/\"\
  \ width=\"646\" height=\"190\" style=\"display: block; margin-left: auto; margin-right:\
  \ auto;\"></iframe></p>\r\n<p>&nbsp;</p>\r\n<p>\xBFQu\xE9 os parece Wonderboy: The\
  \ Dragon's Trap? \xBFOs apetece jugar a esta aventura? Puedes dejar tus respuestas\
  \ en los comentarios o en nuestro grupo de <a href=\"https://t.me/jugandoenlinux\"\
  \ target=\"_blank\" rel=\"noopener noreferrer\">Telegram</a>.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/8b73a42f665d6a2641fcaae19abdf883.webp
joomla_id: 442
joomla_url: analisis-wonder-boy-the-dragon-s-trap
layout: post
tags:
- analisis
- retro
- dotemu
- lizardcube
- master-system
- remake
title: "An\xE1lisis: Wonder Boy - The Dragon's Trap"
---
Cuando era pequeño no tenía videoconsola. Me tenía que conformar con ir a jugar a casa de la vecina a una portentosa NES donde solo podíamos jugar a Super Mario Bros, Kung Fu Master y poco más, y es que si ahora los juegos nos parecen caros, en aquella época ya ni te cuento. De Wonderboy recuerdo su recreativa, el chico rubio con taparrabos que tiraba hachas y saltaba de nube en nube. Después con el paso de los años, en los tardios noventa, me resarcía jugando a infinidad de ROMS entre los que se encontraba tanto este como su tercera parte, The Dragon's Trap. Del nombre del emulador no me acuerdo, pero si de la habilidad (o maldición) que sufría el protagonista de convertirse en diferentes personajes. No se si eran mejores o peores tiempos, pero los recuerdo con mucho cariño.


 


...Y de eso va este remake de Wonder Boy III, de rescatar del baul este gran juego y traerlo a nuestra época con la cara lavada. Casualmente. en esto son expertos los chicos de [Dotemu](http://www.dotemu.com/), y por eso hoy estamos escribiendo este análisis. Con el bagaje de traer a nuestros días renovadas versiones de juegos tan ilustres como **Pang**, **Double Dragon Trilogy**, o **R-Type**, en esta ocasión han encargado la tarea de hacerle más que un lifting al estudio francés [Lizardcube](http://www.lizardcube.com/), formado principalmente por [Omar Cornut](https://twitter.com/ocornut?lang=es) (en otra hora conocido por su fantástico emulador de Master System [MEKA](http://www.smspower.org/meka/)) y el diseñador de comics y animador [Ben Fiquet](https://twitter.com/benfiquet). Por cierto, muchas gracias a todos ellos por facilitarnos una copia del juego para realizar este análisis.


 


![WonderBoyTheDragonsTrap B000](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisWonderBoy/WonderBoyTheDragonsTrap-B000.webp) *Los gráficos dibujados a mano son una verdadera obra de arte. ¡Parece que vemos dibujos animados!*


 


Hechas ya la presentaciones de tan meritorias compañías y personajes vamos a pasar al juego. Cuando uno se enfrenta a la tarea de actualizar un juego de esta edad a lo que tenemos en nuestros días puede optar por dos caminos. El primero, uno fácil, que sería ampliar resoluciones, mejorar algunas texturas y meter algún extra, y que todo ello funcione sin problemas en máquinas actuales. El segundo camino es más complejo, y consiste en rediseñar casi por completo casi todos los aspectos del juego excepto su mecánica y manteniendo su jugabilidad y esencia. Este último y valiente paso es lo que han hecho con resultado sobresaliente los desarrolladores. En Wonderboy: The Dragon's Trap no encontraremos los mismos gráficos, arte y sonido que en el ochentero (aunque en realidad también), sinó que veremos un juego "dibujado" completamente de arriba a abajo.


 


Si hablamos de Wonder Boy: The Dragon's Trap, hablamos de **un juego de plataformas, con tintes de rol**, donde deberemos mover a nuestro/s personaje/s por multitud de escenarios con la ayuda de espada, saltos y diversos ataques especiales. Para empezar, es necesario precisar que podremos escoger entre nuestro heroe de siempre o jugar con una heroina (**Wonder Girl**). En los primeros compases del juego moveremos a nuestro personaje por un castillo hasta alcanzar el primer dragon (Mekadragon), que una vez derrotado nos lanzará una maldición que nos convertirá en un pequeño dragón que tiene como habilidad el lanzar llamaradas a distancia. Nuestra primera misión consiste en salir vivos del castillo en llamas, y una vez abandonado este, llegar a la aldea, que será nuestra "base" y desde donde partirán el resto de aventuras del juego. En ella encontraremos una armería que nos venderá nuestras primeras espadas, armaduras y escudos mejorados; una clinica donde podremos curarnos y una iglesia.


 


![WonderBoyTheDragonsTrap B001](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisWonderBoy/WonderBoyTheDragonsTrap-B001.webp)


*Una "terrible" maldición nos convertirá en los más dispares animales.*


 


Como antes dije, desde la aldea partirán todas las misiones del juego, y es desde aquí desde donde retomaremos la aventura si nos matan, perdiendo todos los avances y objetos que hayamos encontrado por el camino. Esta última caracteristica de la aventura, es la que le otorga al juego un plus de dificultad y la que permite que el juego nos dure un poco más, ya que si no se nos haría excesivamente corto. No perderemos el dinero que hayamos encontrado ni por supuesto los objetos de inventario que hayamos comprado, lo cual nos va a permitir mejorarnos para afrontar de nuevo la zona donde hayamos caido.


 


En cuanto a zonas, aparte del castillo inicial, podremos viajar por la playa, el desierto, la jungla .... y en cada una de ellas habrá lugares donde solo podremos acceder si disponemos del personaje adecuado, obligandonos a recorrer los mismos escenarios varias veces al más puro estilo Metroidvania. Y es en esta característica donde encontramos de verdad la "chicha" del juego. Cada personaje tiene unas habilidades únicas que le permitirán realizar acciones muy diferentes. Podremos ser, además de **Wonderboy** (o **WonderGirl**), como dije antes un **Dragón** (escupe fuego a distancia y puede agacharse), un **Ratón** (puede trepar por las paredes y entrar en zonas pequeñas), una **Piraña** (puede moverse libremente en el agua), un **León** (posee una fuerza sobrehumana que puede romper elementos del escenario), y por último un **Halcón** (recorre los escenarios volando para acceder a zonas alejadas facilmente).


 


![WonderBoyTheDragonsTrap B009](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisWonderBoy/WonderBoyTheDragonsTrap-B009.webp)


*En ciertos momentos tendremos que cambiar nuestra forma para poder afrontar los diferentes escenarios*


 


Dispondremos como arma de una **Espada** (excepto el dragón que lanza fuego) que nos permitirá golpear a los enemigos. Cada vez que lo hagamos, nuestro personaje se quedará quieto, lo cual puede ser un poco fustrante al principio pues implica "reaprender" a jugar este tipo de juegos. Como antes comentaba, también tenemos **ataques especiales** que iremos recolectando a medida que derrotamos enemigos. Estos pueden ser un **tornado** (elimina los enemigos a nuestro alrededor), una **llamarada** (se carga lo que tengamos delante en un radio), un **boomerang** (que podemos recuperar si no acertamos), **flechas** (atacan a enemigos que estén encima nuestra), o un **rayo** (todos los enemigos de la pantalla).


 


En cuanto a los gráficos del juego, es justo decir que el remake es realmente asombroso, ya que desprende belleza en multitud de detalles. Con un aspecto **inspirado en los comics** el juego posee un arte en el cual están muy bien conjuntados tanto los personajes, enemigos como los propios escenarios, encajando todo a la perfección. Hay que decir que el trabajo de Ben Fiquet **dibujando a mano** todos estos elementos es soberbio y constituye una de las mayores virtudes de este título. Tanto es así que parece que estás viendo una serie o película de dibujos animados.


 


Si algo se nota en esta obra es el respeto con el que han tratado el original, y hablando de eso mismo, tan solo tenemos que pulsar un botón y de pronto **nuestro fantástico juego se convierte en el juego original** que todos recordábamos, con sus sprites enormes con su recortada paleta de colores;  y la música y efectos de 8 bits, lo cual es algo muy de agradecer, ya que podemos terminarnos el juego de cabo a rabo como si lo hiciesemos hace 30 años. Puedes ver a que nos referimos en el siguiente video:


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/qwy351pLXas" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


Si hablamos de a **música y los efectos de sonido**, en el modo remasterizado, las melodias son las mismas que en el original, solo que se han actualizado y mejorado, **cambiando estas según el escenarios donde nos encontremos**. Por poner un ejemplo de esto, si vamos al escenario de la playa, la música sonará caribeña, y en el desierto tendrá tintes egipcios. Podemos, por supuesto, comprobar la diferencia pulsando el botón y compararlo con el sonido original. El juego se encuentra **completamente traducido al español en cuanto a textos**, ya que no tenemos voces que doblar; por lo tanto no tendremos ningún problema en seguir el juego, aunque realmente tampoco supondría ningún impedimento importante el que no estuviese en nuestro idioma.


 


Son muy pocos peros los que se le pueden poner a este juego, pero si hay alguno es la duración de este y el tener que atravesar los mismos escenarios varias veces hasta llegar a las zonas nuevas, pero esto, como es lógico, forma parte de la restauración que se ha hecho, y si se quiere ser fiel a original es necesario respetarlo. De todas formas, se trata de un título con un precio muy accesible, por lo que tampoco es un inconveniente.


 


Como conclusión hay que decir que WonderBoy: The Dragon's Trap es una **mágnifica obra de arte** donde podemos rememorar tiempos pasados sin necesidad de sacrificar las posibilidades de la tecnología actual. Un gran juego para jovenes y "viejos" que nos transportará a tiempos un tanto diferentes. Felicidades y gracias, Lizardcube y DotEmu.


 


Os dejamos con el trailer del juego y un pequeño gameplay que realizamos para vosotros:


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/jsPmoQeW6Qk" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/15sm6OnWF9c" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


Puedes comprar Wonder Boy: The Dragon's Trap en Steam:


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/543260/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


¿Qué os parece Wonderboy: The Dragon's Trap? ¿Os apetece jugar a esta aventura? Puedes dejar tus respuestas en los comentarios o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

