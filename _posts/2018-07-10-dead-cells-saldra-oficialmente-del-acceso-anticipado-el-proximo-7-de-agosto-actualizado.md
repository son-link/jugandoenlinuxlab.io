---
author: Pato
category: Plataformas
date: 2018-07-10 17:12:31
excerpt: <p>Ya tenemos disponible el excelente juego de @motiontwin </p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/f59983a1dd56aec77dd53be09544bf10.webp
joomla_id: 801
joomla_url: dead-cells-saldra-oficialmente-del-acceso-anticipado-el-proximo-7-de-agosto-actualizado
layout: post
tags:
- accion
- indie
- plataformas
- proximamente
title: "'Dead Cells' saldr\xE1 oficialmente del acceso anticipado el pr\xF3ximo 7\
  \ de Agosto - Actualizado -"
---
Ya tenemos disponible el excelente juego de @motiontwin 

**(Actualización 9-8-18)**


Ya tenemos disponible '**Dead Cells**', el juego de Motion Twin que como prometieron llegó el pasado Martes para Linux/SteamOS. Las críticas que está recibiendo son excelentes, con lo que el juego merece mucho la pena. Si queréis saber más o comprarlo lo tenéis disponible en [Humble Bundle](https://www.humblebundle.com/store/dead-cells?partner=jugandoenlinux) (enlace de afiliado), [GOG](http://www.gog.com/game/dead_cells) o en su [web de Steam](https://store.steampowered.com/app/588650/Dead_Cells/) con un 20% de descuento por su lanzamiento.




---


**(Noticia original)**


Hace apenas un mes que [os contábamos](index.php/homepage/generos/plataformas/item/907-dead-cells-se-actualiza-y-prepara-su-lanzamiento-de-cara-a-agosto) que el juego de Motion Twin 'Dead Cells' recibía el soporte para mods y la actualización de su acceso anticipado, y ahora nos encontramos con el anuncio oficial de que el juego saldrá de acceso anticipado y **llegará oficialmente a nuestro sistema favorito el próximo día 7 de Agosto**.


Así lo han publicado [en un post](https://steamcommunity.com/games/588650/announcements/detail/1687044219367010627) en su página de Steam donde además publican un vídeo promocional para celebrarlo:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/02G3GUt6Nzo" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 A pesar del lanzamiento oficial, el juego no ha terminado realmente su desarrollo pues el estudio piensa seguir alimentando el juego con mas contenido y para mejorar el soporte de mods, aún en fase experimental.


Si quieres saber más sobre 'Dead Cells', o comprarlo a precio de acceso anticipado, puedes visitar [su página de Steam](https://store.steampowered.com/app/588650/Dead_Cells/). Como ya os comentamos, **el juego subirá de precio** el 7 de Agosto, así que ¡no os durmáis!

