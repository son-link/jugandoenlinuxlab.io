---
author: P_Vader
category: Software
date: 2022-03-07 12:50:26
excerpt: "<p>Esta navaja suiza para instalar f\xE1cilmente Proton-GE a\xF1ade soporte\
  \ en espa\xF1ol.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/protonup-qt/pupgui2.webp
joomla_id: 1445
joomla_url: protonup-qt-llega-a-la-version-2-6
layout: post
tags:
- proton
- protonup-qt
- proton-ge
title: "ProtonUP-Qt llega a la versi\xF3n 2.6"
---
Esta navaja suiza para instalar fácilmente Proton-GE añade soporte en español.


**¿Aún te lías para instalar las versiones especiales de [Proton-GE](https://github.com/GloriousEggroll/proton-ge-custom) o [Wine-GE](https://github.com/GloriousEggroll/wine-ge-custom) entre otros?** Debe ser que no conoces esta sencilla y eficaz herramienta llamada [ProtonUp-QT](https://davidotek.github.io/protonup-qt/).


Si eres usuario de Proton no te puede faltar. Ademas es realmente **fácil de instalar ya sea  a través de su [Appimage](https://github.com/DavidoTek/ProtonUp-Qt/releases) o mas sencillo aun desde [flathub](https://flathub.org/apps/details/net.davidotek.pupgui2).**


Las características que podemos destacar de este programa son:


* Puede instalar Proton-GE, Luxtorpeda, Boxtron o Roberta para **Steam**
* Igualmente puede instalar Wine-GE, Lutris-Wine o Vanilla Wine-Builds de Kron4ek para **Lutris**
* Y para **Heroic Games Launcher** también puede instalar Wine-GE o Proton-GE.
* **Optimizado para modo consola** de juegos o dispositivos portátiles, siendo compatible para usarse con mando.
* Probado en Ubuntu 18.04 y posterior, Fedora 34 y Manjaro 20.2


Esta nueva versión trae dos buenas novedades:


* Se añadió traducciones al alemán, finlandés y **español**.
* Se agregó un **nuevo cuadro de diálogo que lista los juegos de steam**. Muy útil para saber que versión de proton tiene cada juego.


 


Si todavía no sabes como funciona esta fantástica herramienta aquí te dejo un minitutorial que explica lo sencillo y útil que puede ser:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube.com/embed/aGTMozMbpPE" title="YouTube video player" width="720"></iframe></div>


¿Has probado ya Protonup-Qt? ¿Qué te parece? Cuentanoslo en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).


 

