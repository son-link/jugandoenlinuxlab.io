---
author: leillo1975
category: Carreras
date: 2018-05-18 13:14:33
excerpt: <p>@ansdor nos muestra novedades en su cuenta de twitter</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/fe9f389134b872478c99a836d55de88e.webp
joomla_id: 738
joomla_url: pronto-podremos-disfrutar-de-slipsteam
layout: post
tags:
- carreras
- retro
- slipstream
- ansdor
title: Ya podemos disfrutar de Slipstream (ACTUALIZADO 2)
---
@ansdor nos muestra novedades en su cuenta de twitter

**ACTUALIZACIÓN 25-6-18**: Tal y como había prometido en sus [anuncios](https://steamcommunity.com/games/732810/announcements/detail/1671278447873854468) en Steam, el desarrollador @ansdor acaba de compartir en twitter una imagen del juego donde se puede ver este siendo jugado a pantalla partida, o como comunmente se dice "a dobles". No se informa de la fecha de salida de esta característica, pero pone que será pronto. Podeis ver el tweet aquí:



> 
> split screen coming soon (I hope?) [pic.twitter.com/2DefQ4OE01](https://t.co/2DefQ4OE01)
> 
> 
> — ansdor (@ansdor) [25 de junio de 2018](https://twitter.com/ansdor/status/1011062887153045504?ref_src=twsrc%5Etfw)



 


 Hace algún tiempo, en JugandoEnLinux.com jugamos un buen rato él. Podeis verlo en este video:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/LqkZebwbYyY" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 




---


**ACTUALIZACIÓN 21-5-18**: Tal y como os comentamos hace unos días el juego finalmente ya está a la venta por el magnífico precio de 7.37€ (con el 10% de descuento por oferta de lanzamiento). Podeis comprarlo en Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/732810/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


Si quereis disfrutarlo no tendreis problemas por que los requisitos técnicos son realmente bajos.


**SO:** Ubuntu 16.04
**Memoria:** 2 GB de RAM
**Gráficos:** Intel HD Graphics 4000
**Almacenamiento:** 500 MB de espacio disponible



Le desamos toda la suerte del mundo a [Ansdor](http://www.ansdor.com/) con la aceptación y ventas de su juego.




---


 **NOTICIA ORIGINAL**: Gracias a [GamingOnLinux](https://www.gamingonlinux.com/articles/retro-inspired-arcade-racer-slipstream-to-release-may-21st-developed-on-linux.11793) tenemos la información sobre este juego de coches. Justo hace unos días os hablábamos del fantástico **Horizon Chase Turbo** ([noticia](index.php/homepage/generos/carreras/item/833-horizon-chase-turbo-tomara-la-salida-para-correr-el-15-de-mayo) y [video-gameplay](https://youtu.be/it7ckGPM-XQ)) , y hoy leemos la noticia de que este juego desarrollado por [Ansdor](http://www.ansdor.com/), un desarrollador brasileño que recientemente consiguió financiación para su juego en una campaña de [Kickstarter](https://www.kickstarter.com/projects/noctet/slipstream/description), nos traerá su trabajo el próximo Lunes, día 21 de Mayo.


El juego, que como podeis ver en el video (abajo), también trata de emular a los clásicos como **Out Run** está **desarrollado enteramente en Linux** y con herramientas libres (Krita, Gimp o Blender), con un motor gráfico propio; y en el encontraremos un juego desarrollado con sprites en un pseudo-3D. Constará de 20 fases, 3 modos de juego (Arcade, Carrera Rápida, Grand Prix) y 5 coches diferentes no solo en estética, si no en prestaciones y modo de conducción; una banda sonora original con 9 temas, y opciones gráficas para darle un toque más retro como filtros CRT, o modos NTSC.


Como podeis observar, el juego difiere de lo presentado por Aquiris en Horizon Chase Turbo, si bien en este último se le da un toque renovado y más poligonal al juego, en SlipStream tiene un tratamiento mucho más retro, retomando el 2D y los Sprites.


El juego será lanzado en [Steam](https://store.steampowered.com/app/732810/Slipstream/), donde hace algún tiempo había sido aprobado por Greenlight, y si quereis haceros una idea más ajustada de lo que os vais a encontrar, podeis ver este video:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/2955JiYgcQQ" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


¿Qué opinais de este "revival" de los juegos de antaño? ¿Os gusta el planteamiento de SlipStream? Déjanos tus opiniones en los comentarios, o en nuestros grupos de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

