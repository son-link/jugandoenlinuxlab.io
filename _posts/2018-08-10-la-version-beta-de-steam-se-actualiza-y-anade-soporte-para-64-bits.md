---
author: Pato
category: Noticia
date: 2018-08-10 16:05:10
excerpt: "<p>La \xFAltima actualizaci\xF3n del cliente se prepara para el futuro</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/6187e8e3b292df4bec7f162543eed86b.webp
joomla_id: 825
joomla_url: la-version-beta-de-steam-se-actualiza-y-anade-soporte-para-64-bits
layout: post
tags:
- steam
title: El cliente de Steam se actualiza y se prepara para los 64 bits
---
La última actualización del cliente se prepara para el futuro

El pasado día 8 la versión beta del cliente de Steam, y un día después la versión estable, recibieron una actualización que en principio no parecía gran cosa pero que va a resultar clave para el futuro de la plataforma. En concreto, el anuncio de la actualización dice así:


*Añadido soporte para enviar diferentes binarios a los sistemas operativos de 64 bits frente a los de 32 bits en Steam self-updater. Este soporte se añade como preparación para futuras actualizaciones.*


Esto sugiere que Valve está pensando en distribuir su cliente en versión de 64 bits en vez de la versión que todos utilizamos ahora de 32 bits y que necesita de multitud de archivos para cumplir con las dependencias en los sistemas modernos que utilizamos la mayoría. (Aún hay quien juega en sistemas de 32 bits, pero son una especie en extinción y más con la retirada de soporte de las grandes distros que se producirá de forma casi inminente).


El escueto anuncio puedes verlo [en este enlace](https://steamcommunity.com/groups/SteamClientBeta#announcements) o en [este](https://store.steampowered.com/news/42822/).


En la actualización del cliente en su versión estable también han añadido una corrección para que steam link deje de detectar dos veces los mandos de PS4.


¿Que te parecería que Steam se pasase a los 64 bits? ¿Crees que esto mejorará en algún aspecto a los juegos o al cliente en Linux?


Cuéntamelo en los comentarios o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

