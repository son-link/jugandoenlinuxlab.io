---
author: Pato
category: Estrategia
date: 2019-08-29 18:32:15
excerpt: <p>&nbsp;@feralgames ha puesto ya a la venta esta DLC.</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/e162e41c2016dbf9a4f7ef4fa6de229c.webp
joomla_id: 1104
joomla_url: la-expansion-de-total-war-warhammer-ii-the-hunter-the-beast-llegara-a-linux-steamos-proximamente
layout: post
tags:
- accion
- estrategia
title: "La expansi\xF3n de Total War: WARHAMMER II \"The Hunter & The Beast\" llegar\xE1\
  \ a Linux/SteamOS pr\xF3ximamente (ACTUALIZADO)."
---
 @feralgames ha puesto ya a la venta esta DLC.

**ACTUALIZACIÓN 3-10-19:**  
Tal y como nos prometieron desde Feral hace poco más de un mes, **la expansión "The Hunter & The Beast" ya está disponible** para que podamos instalarla en nuestros equipos con Linux. Podremos comprarla por tan solo **8,99€**  tanto directamente en la [tienda de Feral](https://store.feralinteractive.com/en/mac-linux-games/warhammer2tw/#warhammer2twdlchunterandbeast), como en la [**Humble Store (enlace patrocinado)**](https://www.humblebundle.com/store/total-war-warhammer-ii-the-hunter-and-the-beast?partner=jugandoenlinux) o [Steam](https://store.steampowered.com/app/1074320/Total_War_WARHAMMER_II__The_Hunter__The_Beast/). Podeis encontrar más información sobre esta DLC en el [Blog de Total War](https://www.totalwar.com/blog/total-war-warhammer-ii-the-hunter-the-beast-faq/). Os dejamos con un gameplay del juego, en este caso el de Markus Wulfhart:


  
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/E0NGSHT5KsY" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 




---


**NOTICIA ORIGINAL:**  
Seguimos con lanzamientos importantes, esta vez desde Feral Interactive nos llega la noticia de que la próxima expansión de **Total War: WARHAMMER II** llamada "**The Hunter & The Beast**" llegará a nuestro sistema favorito poco después de que llegue a sistemas Windows el día 11 de Septiembre.



> 
> The Hunter & The Beast prowl onto macOS and Linux shortly after Windows. ?? <https://t.co/endIp8K5Bd>
> 
> 
> — Feral Interactive (@feralgames) [August 29, 2019](https://twitter.com/feralgames/status/1167078275589562369?ref_src=twsrc%5Etfw)



 Esta expansión es además una gran actualización del juego, puesto que [según el comunicado oficial](https://www.totalwar.com/blog/total-war-warhammer-ii-the-hunter-the-beast-faq/) se incluirán distintos contenidos que hasta ahora no se encontraban en el juego base:


* Dos nuevos y legendarios señores legendarios: Markus Wulfhart lidera la Expedición de Huntsmarshal (Empire), y Nakai the Wanderer se opone a él como el líder del Spirit of the Jungle (Lizardmen)
* Dos nuevos tipos de Lord: Hunter Generals y Ancient Kroxigors
* Cuatro nuevas unidades de Héroe únicas para el Imperio (ver Cazadores de Wulfhart)
* Nuevas mecánicas de facción únicas, cadenas de misiones, objetos legendarios, árboles de habilidades y narrativa de campaña (solo Campaña Vortex)
* Nuevas unidades para el Imperio: arqueros, cazadores y vagones de guerra
* Nuevas unidades para los Hombres Lagarto: Saurios temibles, Kroxigors sagrados y paquetes de caza Razordon
* Nuevos regimientos de renombre tanto para el imperio como para los hombres lagarto


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/FnKhFaijBBI" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Si quieres saber más sobre esta expansión, puedes ver su [página de Steam](https://store.steampowered.com/app/1074320/Total_War_WARHAMMER_II__The_Hunter__The_Beast/) en la que ya se puede pre-comprar este "The Hunter & The Beast con un 10% de descuento (8,09€)

