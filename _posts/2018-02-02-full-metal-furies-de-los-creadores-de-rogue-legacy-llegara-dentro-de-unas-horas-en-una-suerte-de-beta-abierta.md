---
author: Pato
category: "Acci\xF3n"
date: 2018-02-02 16:28:37
excerpt: "<p>El port&nbsp; de este juego de acci\xF3n cooperativo corre a cargo de\
  \ Ethan Lee</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/bc4c6771b68b0ed330f1918b8b62d9c4.webp
joomla_id: 632
joomla_url: full-metal-furies-de-los-creadores-de-rogue-legacy-llegara-dentro-de-unas-horas-en-una-suerte-de-beta-abierta
layout: post
tags:
- accion
- indie
- aventura
- rol
- cooperativo
title: "'Full Metal Furies' de los creadores de Rogue Legacy llegar\xE1 dentro de\
  \ unas horas en una suerte de beta abierta"
---
El port  de este juego de acción cooperativo corre a cargo de Ethan Lee

Nuevas noticias de uno de los porters mas admirados por todos los que jugamos en Linux. Hablamos de Ethan Lee que ahora mismo se encuentra finalizando el port de 'Full Metal Furies', un juego de acción cooperativo para 4 jugadores ya sea en multijugador local u online **incluyendo el juego cruzado con otros sistemas**:


*FULL METAL FURIES [[web oficial](http://cellardoorgames.com/our-games/full-metal-furies/)] enfatiza el trabajo en equipo con un original sistema de combate en el que todos son importantes. Trabaja codo con codo para derrotar a enemigos especiales, encadena combos orgánicos para infligir un daño enorme y salva a un mundo devastado por la guerra que está al borde de la extinción. Juega en el sofá o en línea con amigos, ¡y disfruta de una fiesta para 4 jugadores! O juega en el modo para un jugador y alardea de tus habilidades con el sistema de cambio rápido de 2 personajes que conserva todos los matices del modo multijugador.*


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/6wUFjPdDo6Q" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 El juego es obra de los creadores del notable '[Rogue Legacy](http://store.steampowered.com/app/241600/Rogue_Legacy/)' también disponible en Linux y **aún no está disponible en nuestro sistema**, pero según el anuncio del propio Ethan Lee en el foro de Steam el juego estará disponible en las próximas horas en una suerte de beta pública en la que todo aquel que tenga el juego podrá probarlo en Linux y testear sobre todo la conexión online y el juego cruzado con los sistemas Windows y MacOS.


Por lo visto están encontrando algunos problemas a la hora de implementar el netcode para que el juego cruzado online funcione de manera óptima, por lo que piden ayuda a todo el que pueda testearlo y llevar a cabo un reporte de los problemas encontrados.


Toda la información de cómo llevar a cabo los test y los reportes para depurar el juego la podéis encontrar en el post de Ethan Lee de Steam [en este enlace](http://steamcommunity.com/app/416600/discussions/2/1700541698689782465/).


Full Metal Furies estará disponible en las próximas horas para GNU-Linux/SteamOS en su página de Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/416600/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 ¿Te gustan los buenos juegos de acción cooperativa? ¿Testearás este 'Full Metal Furies' para echarle una mano a Ethan Lee?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

