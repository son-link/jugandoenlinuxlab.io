---
author: leillo1975
category: "Simulaci\xF3n"
date: 2021-04-08 08:10:04
excerpt: "<div>Ya podemos recorrer Espa\xF1a y Portugal con esta expansi\xF3n de @SCSsoftware</div>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ETS2/Iberia/ETS2_Iberia_900.webp
joomla_id: 1297
joomla_url: iberia-dlc-disponible-para-euro-truck-simulator-2
layout: post
title: Iberia DLC disponible para Euro Truck Simulator 2
---
Ya podemos recorrer España y Portugal con esta expansión de @SCSsoftware
 Bien poco nos queda por decir ya de este esperadísimo trabajo de la compañía checa que no hayamos comentado ya en nuestra web. Tras años de espera desesperada, finalmente la más deseada expansión de [Euro Truck Simulator 2]({{ "/tags/euro-truck-simulator-2" | absolute_url }}) (al menos por los fans españoles y portugueses del juego), ya está disponible tal y como podeis ver en el siguiente tweet que [SCS Software](https://scssoft.com/) acaba de publicar: 

> 
> The Iberia DLC for Euro Truck Simulator 2 OUT NOW on Steam! 🇪🇸🇵🇹   
>   
> Now Available at: <https://t.co/CvdQL98jCd> 🙌🥳  
>   
> We can't wait for you to begin [#CruisingIberia](https://twitter.com/hashtag/CruisingIberia?src=hash&ref_src=twsrc%5Etfw) 🚛 [pic.twitter.com/h79q6K1gI6](https://t.co/h79q6K1gI6)
> 
> 
> — SCS Software (@SCSsoftware) [April 8, 2021](https://twitter.com/SCSsoftware/status/1380207475283099648?ref_src=twsrc%5Etfw)


Como os comunicamos con anterioridad, el camino hasta este lanzamiento no ha sido un camino de rosas. Como sabeis **la DLC tendría que llegar a finales del pasado año**, pero debido a diversos problemas, y el querer acompañarla por el esperado **nuevo sistema de iluminación** global de la [versión 1.40]({{ "/posts/euro-truck-simulator-2-se-actualiza-a-la-version-1-40" | absolute_url }}), hemos tenido que esperar unos meses más. Pero al fin ha llegado el día y todo esto ya no importa.


![Pueblo](https://scssoft.com/press/euro_truck_simulator_2_iberia/images/011.jpg)


La descripción oficial de lo que vamos a encontrar en esta DLC es la siguiente:


*La Península Ibérica está llena de paisajes ricos y diversos, desde los desiertos semiáridos del sureste hasta los verdes bosques de coníferas. Iberia alberga numerosos pueblos y ciudades históricas, calles estrechas, iglesias antiguas e impresionantes castillos. Visite la capital de España, Madrid, la capital costera de Portugal, Lisboa, una gran cantidad de ciudades costeras como Málaga y Olhão, y muchas de las ciudades del interior. Sea una parte importante de la fuerte economía de exportación y entregue la carga de Iberia a través de Europa.*  
*Iberia es encantadora, he aquí por qué:*


*-Atraviesa el desierto de Tabernas, a menudo apodado "el único desierto de la Europa continental".*  
 *-Conecta Iberia con el resto de Europa con envíos desde sus concurridos puertos*  
 *-Entrega piezas de automóviles y coches enteros desde las distintas fábricas de automóviles hasta las terminales de embarque*  
 *-Descansa en las paradas de camiones y las gasolineras, diseñadas para que se ajusten lo más posible a las de la vida real.*  
 *-Contempla lugares emblemáticos de todas las formas y tamaños, desde castillos medievales hasta estatuas de personajes históricos*  
 *-Disfruta de las espectaculares vistas de la costa e incluso del famoso "Peñón de Gibraltar", situado al otro lado de la bahía de la ciudad costera de Algeciras*  
 *-Sumérjete en el nuevo sonido ambiente, desde el sutil sonido del viento a través de los valles hasta el piar de los pájaros*  
 *-Explora zonas montañosas con una amplia gama de rocas de colores*  
 *-Visita lugares emblemáticos como el Puente Internacional del Guadiana y obtén una vista de pájaro sobre él con la función de mirador*  
 *-Desbloquea logros de Steam específicos de Iberia*

![Mapa](https://cdn.akamai.steamstatic.com/steam/apps/1209460/extras/iberia_map_thumb.jpg)


También debeis saber que para celebrar el estreno de Iberia, en [World of Trucks](https://worldoftrucks.com) han creado un evento especial llamado "**Cruising Iberia**", en el que podremos participar desde ya hasta el martes 11 a medianoche (UTC). Si quereis ver en que consiste consultad esta [noticia de su blog](https://blog.scssoft.com/2021/04/cruising-iberia-event.html).


Recordad que para que podais disfrutar de Iberia necesitareis además del [juego base original](https://www.humblebundle.com/store/euro-truck-simulator-2?partner=jugandoenlinux). Os dejamos de nuevo con el **hermosísimo trailer** que SCS Software ha lanzado hace unos días con motivo del anuncio de la fecha oficial de salida:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/MVEBdGZ7UcM" style="display: block; margin-left: auto; margin-right: auto;" title="YouTube video player" width="780"></iframe></div>


Podeis comprar Iberia DLC para Euro Truck Simulator 2 en Steam:


  
<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/1209460/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


Y tu, ¿a que esperas para comentarnos que te parece esta expansión? Dejanos tus impresiones en los comentarios de este artículo, o en nuestros grupos de JugandoEnLinux en [Telegram](https://twitter.com/JugandoenLinux) o  [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

