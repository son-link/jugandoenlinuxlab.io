---
author: Pato
category: Editorial
date: 2017-08-25 16:48:07
excerpt: "<p>Cumplimos un a\xF1o trayendo las novedades y noticias del mundillo del\
  \ juego en Linux. \xA1Qui\xE9n lo dir\xEDa!</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/d3787968271aadf66b69e2f7e02571e2.webp
joomla_id: 444
joomla_url: jugandoenlinux-com-cumple-un-ano
layout: post
title: "\xA1jugandoenlinux.com cumple un a\xF1o!"
---
Cumplimos un año trayendo las novedades y noticias del mundillo del juego en Linux. ¡Quién lo diría!

¡Cómo pasa el tiempo! Parece que fue ayer cuando publicamos [nuestro primer post](index.php/homepage/editorial/item/1-creacion-jugandoenlinux-fundamentos) con nuestro manifiesto de fundación y nuestras intenciones. Desde entonces hemos pasado de hacer un blog sin muchas pretensiones a consolidarnos como una de las webs de habla hispana de referencia en cuanto a juegos en Linux.


Cuando comenzamos no sabíamos a donde llegaríamos, ni si escribiríamos este blog únicamente para nosotros, ni si alguna vez llegaríamos a tener más de unas pocas visitas. Continuamos trabajando paso a paso de forma incansable para intentar ofrecer información de calidad a la vez que crear una comunidad de jugadores que permitiera mostrar que jugar en Linux no es imposible ni una utopía, demostrando día a día que el mundo del juego ha llegado a Linux para quedarse.


Hoy podemos decir que nuestra web sirve más de 2.600 páginas mensuales a más de 800 usuarios únicos repartidos por una veintena de países, desde Estados Unidos hasta España y parte de Europa pasando por toda Latinoamerica. Y esto es solo el principio.


Hemos consolidado un pequeño pero activo [grupo de Telegram](https://telegram.me/jugandoenlinux), un [canal de Twitter](https://twitter.com/JugandoenLinux) que va creciendo poco a poco y estamos luchando por consolidar desde [nuestro foro](index.php/foro/categorias) y el canal de Telegram sesiones de juego para que todo el que quiera pueda unirse a jugar con nosotros.


Gracias a las [donaciones](https://www.paypal.com/es/cgi-bin/webscr?cmd=_flow&SESSION=tp7zNnwQhuDZvHRwtxlBp8wLdgmCIVN8AOwNF_qay5hnoMLxCLZNXoHPBkq&dispatch=5885d80a13c0db1f8e263663d3faee8d795bb2096d7a7643a72ab88842aa1f54&rapidsState=Donation__DonationFlow___StateDonationLogin&rapidsStateSignature=fff2335c65f6d53eb8eaff5cfb0f6cc3ef49171c) que nos habéis hecho hemos podido comenzar a pensar en los cambios necesarios para mejorar nuestra web y sus "servicios" en la medida de lo posible, cosa  de la que hablaré más adelante. Agraceder a nuestros mecenas la ayuda que nos han prestado y esperamos que las mejoras que implementemos sean merecedoras de nuevas donaciones.


Pero ya basta de cumplidos y vamos a lo que nos ocupa: ¡Cumplimos un añito! ¿Lo celebramos?


Estos últimos días habréis notado que no he estado muy activo en el blog ni en los canales habituales, y no es casualidad. De cara a nuestro aniversario vamos a llevar a cabo algunos cambios en la web que esperamos ir implementando durante la próxima semana. Vamos a actualizar nuestro sistema del blog para poder implementar mejoras a la hora de publicar información, y a la vez trataremos de hacerle un lavado de cara para hacerlo más moderno y atractivo a la vez que trataremos de que muestre más y mejor información, y sobre todo mejor estructurada.


Y por último pero no menos importante... ¡vamos a sortear juegos a nuestro selecto grupo de miembros!


Durante la próxima semana **vamos a llevar a cabo varios sorteos de videojuegos** para todos los que nos seguís tanto en la web, en las redes sociales como en Telegram etc. con un sorteo especial además para todos los donantes de jugandoenlinux.com. Permanecer atentos a la web por que os iremos anunciando las bases y todo lo relacionado con los sorteos en próximos posts. Adelantar que los sorteos se realizarán en directo a través de nuestro canal de Twitch para que no hayan dudas de nuestra imparcialidad.


Para terminar, agradecer a todos los que habéis colaborado con jugandoenlinux.com, ya sea publicando artículos, enviando avisos e información o colaborando en el foro. Mención especial a Serjor y Leillo, mis manos derecha e izquierda en todo esto y sin los cuales nunca hubiera sido posible llegar hasta aquí. ¡Espero contar con vosotros por muchos años compañeros!. Todo esto es por y para vosotros. Gracias por este maravilloso año.


Esperemos que jugandoenlinux.com cumpla muchos mas. ¡Felicidades a todos!


El equipo de jugandoenlinux.com: Pato, Leillo y Serjor. 

