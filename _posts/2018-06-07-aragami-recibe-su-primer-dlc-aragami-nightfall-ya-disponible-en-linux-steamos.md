---
author: Pato
category: "Acci\xF3n"
date: 2018-06-07 16:19:22
excerpt: <p>@LinceWorks sigue apoyando su excelente juego de sigilo con nuevo contenido</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/b0d7d8c971b982fb6678172b9076bb96.webp
joomla_id: 758
joomla_url: aragami-recibe-su-primer-dlc-aragami-nightfall-ya-disponible-en-linux-steamos
layout: post
tags:
- accion
- indie
- steam
- sigilo
- gog
- tercera-persona
title: 'Aragami recibe su primer DLC ''Aragami: Nightfall'' ya disponible en Linux/SteamOS'
---
@LinceWorks sigue apoyando su excelente juego de sigilo con nuevo contenido

Ya hemos hablado otras veces del excelente 'Aragami' obra del estudio español "Lince Works". Ahora nos hacemos eco del [anuncio](https://steamcommunity.com/gid/[g:1:11863367]/announcements/detail/1662271882767082891) de que el estudio acaba de lanzar su nuevo DLC. Aragami mezcla la acción y el sigilo en un título que llama la atención por su estética colorida. El juego recuerda poderosamente a otros títulos que nunca han llegado a Linux como Assasins Creed y similares.


Juegas como un espíritu ninja invocado por una chica que tiene que utilizar sus habilidades para esconderse entre las sombras y elementos del entorno para pasar desapercibido y poder acercarse a sus enemigos y poder acabar con ellos. El objetivo es liberar a Yamiko, que ha sido aprisionada en la ciudad fortificada de Kyuryu. 


En esta nueva expansión tendremos que embarcarnos en una nueva campaña que sucederá en un momento anterior al de la campaña principal de Aragami, en la que tendremos que afrontar nuevos desafíos y escenarios más complejos para los jugadores expertos. También jugaremos con un nuevo personaje que será crucial en el devenir de la historia posterior de Aragami.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/wpsX8aWX5rg" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 **Características**


* Juega como Hyo o Shinobu y descubre el destino de los Asesinos de la última Sombra.
* Prueba 3 nuevas técnicas obscuras para ayudarte a completar tu misión.
* Juega 4 capítulos nuevos a través valles nevados y ciudades decadentes para parar un ritual.
* Juega la campaña de Aragami: Nightfall con un amigo en el modo de juego multijugador online multiplataforma.


Si quieres saber más sobre el mundo de Aragami, puedes visitar su [página web oficial](http://aragami-game.com/), o puedes visitar sus páginas de [Humble Bundle](https://www.humblebundle.com/store/aragami-nightfall?partner=jugandoenlinux) (enlace afiliado) o en [Steam](https://store.steampowered.com/app/771720/Aragami_Nightfall/) donde ya está disponible para su compra.

