---
author: Jugando en Linux
category: Apt-Get Update
date: 2022-03-11 20:42:44
excerpt: ''
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/CoreKeeper/Corekeeper.webp
joomla_id: 1449
joomla_url: nuevos-juegos-con-soporte-nativo-11-03-2022
layout: post
title: Nuevos juegos con soporte nativo 11/03/2022
---

 Una semana más, os traemos una selección de las novedades y ofertas que pensamos que os pueden interesar, como siempre con soporte nativo, empezando con Core Keeper, un juego en acceso anticipado que está pegando fuerte y está dando mucho que hablar:


### Core Keeper


Desarrolador: Pugstorm


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube.com/embed/E-7-QnU-gfc" title="YouTube video player" width="720"></iframe></div>


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/1621690/" style="border: 0px;" width="646"></iframe></div>




---


### The Last Cube


Desarrolador: Improx Games


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube.com/embed/ldkEU6NMP4M" title="YouTube video player" width="720"></iframe></div>


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/903630/" style="border: 0px;" width="646"></iframe></div>




---


### Goop Loop


Desarrolador: Lone Wulf Studio LLC


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube.com/embed/PKRnevJsId8" title="YouTube video player" width="720"></iframe></div>


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/1454550/" style="border: 0px;" width="646"></iframe></div>




---


### EN OFERTA


### Children of Morta


Desarrolador: Dead Mage


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube.com/embed/8NC6tQlAFIA" title="YouTube video player" width="720"></iframe></div>


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/330020/" style="border: 0px;" width="646"></iframe></div>




---


###  XCOM® 2


DESARROLLADOR: Firaxis Games, Feral Interactive
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube-nocookie.com/embed/VIRDb_O6qXA" title="YouTube video player" width="720"></iframe></div>


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/268500/" style="border: 0px;" width="646"></iframe></div>




---


### BATTLETECH


DESARROLLADOR: Harebrained Schemes
 
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube-nocookie.com/embed/tsIMfOo_VO0" title="YouTube video player" width="720"></iframe></div>


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/637090/245786/" style="border: 0px;" width="646"></iframe></div>


 La semana que viene, más novedades y ofertas aquí en jugandoenlinux.com