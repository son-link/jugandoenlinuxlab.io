---
author: Serjor
category: Ofertas
date: 2017-11-10 18:06:41
excerpt: "<p>Ir preparando las carteras, Aspyr ha puesto parte de su cat\xE1logo de\
  \ oferta</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/957f68251a4c401eb034febbf0fc418f.webp
joomla_id: 530
joomla_url: fin-de-semana-del-editor-en-steam-aspyr-nos-trae-un-buen-punado-de-ofertas
layout: post
tags:
- steam
- aspyr-media
- rebajas
title: "Fin de semana del editor en Steam, Aspyr nos trae un buen pu\xF1ado de ofertas"
---
Ir preparando las carteras, Aspyr ha puesto parte de su catálogo de oferta

No hay nada como iniciar Steam y que salga una noticia como esta:


![aspyr 20years](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Aspyr/aspyr-20years.webp)


 


Y es que este fin de semana uno de nuestras (¿ex?-)desarrolladoras de ports favoritas y editora por méritos propios ha lanzado una serie de ofertas de lo más interesantes para celebrar sus [20 años](http://store.steampowered.com/news/24225/).


De todos los que han puesto de oferta para GNU/Linux tenemos una larga lista de grandes juegos, que si no los tienes ya, deberías hacerte con ellos:


>observer_, del que podéis ver el unboxing que hicimos la noche de halloween [aquí](https://www.youtube.com/watch?v=X7khnBsOwRA)


<div class="steam-iframe"><iframe frameborder="0" height="190" src="https://store.steampowered.com/widget/514900/" width="646"></iframe></div>


Y en línea con este juego de terror psicológico, el popular Layers of fear:


<div class="steam-iframe"><iframe frameborder="0" height="190" src="https://store.steampowered.com/widget/391720/74835/" width="646"></iframe></div>


Civilization VI, del que podéis leer un estupendo análisis de nuestro compañero Leo [aquí](index.php/homepage/analisis/item/352-analisis-sid-meier-s-civilization-vi)


<div class="steam-iframe"><iframe frameborder="0" height="190" src="https://store.steampowered.com/widget/289070/123215/" width="646"></iframe></div>


Además de este Civilization VI, tenemos también disponibles los anteriores:


<div class="steam-iframe"><iframe frameborder="0" height="190" src="https://store.steampowered.com/widget/8930/6146/" width="646"></iframe></div>


<div class="steam-iframe"><iframe frameborder="0" height="190" src="https://store.steampowered.com/widget/65980/75573/" width="646"></iframe></div>


Y cómo no, los fantásticos e hilarantes Borderlands:


<div class="steam-iframe"><iframe frameborder="0" height="190" src="https://store.steampowered.com/widget/49520/32848/" width="646"></iframe></div>


<div class="steam-iframe"><iframe frameborder="0" height="190" src="https://store.steampowered.com/widget/261640/63481/" width="646"></iframe></div>


Y también podemos encontrar de oferta:


Bioshock Infinite:


<div class="steam-iframe"><iframe frameborder="0" height="190" src="https://store.steampowered.com/widget/8870/26595/" width="646"></iframe></div>


Fahrenheit: Indigo Prophecy


<div class="steam-iframe"><iframe frameborder="0" height="190" src="https://store.steampowered.com/widget/312840/" width="646"></iframe></div>


Geometry Wars 3: Dimensions Evolved


<div class="steam-iframe"><iframe frameborder="0" height="190" src="https://store.steampowered.com/widget/310790/" width="646"></iframe></div>


Recordar que las ofertas están disponibles solamente este fin de semana, así que no os lo penséis mucho si tenéis intención de haceros con alguno de estos juegos.


Y tú, ¿con cuál te quedas o cuál recomiendas? Cuéntanoslo en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD)

