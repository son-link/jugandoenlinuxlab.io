---
author: Pato
category: "Acci\xF3n"
date: 2018-12-26 15:53:28
excerpt: "<p>El juego \"free to play\" de Scavengers Studios aun no tiene confirmada\
  \ versi\xF3n Linux</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/e8b2abdaaba787535adb9b019745b649.webp
joomla_id: 940
joomla_url: es-posible-jugar-al-battle-royale-darwin-project-gracias-a-steam-play
layout: post
tags:
- accion
- battle-royale
- steam-play
title: Es posible jugar al Battle Royale 'Darwin Project' gracias a Steam Play
---
El juego "free to play" de Scavengers Studios aun no tiene confirmada versión Linux

Seguimos con la moda battle royale. Ahora gracias a [gamingonlinux.com](https://www.gamingonlinux.com/articles/recent-steam-play-updates-have-enabled-the-free-to-play-battle-royale-darwin-project-to-work-on-linux.13235/page=2#comments) tenemos noticias de que es posible jugar a '**Darwin Project**', el juego "free to play" de Scavengers Studio **gracias a Steam Play y proton**.


Según la información de Liam, después del lanzamiento de la última beta de Proton de la que [ya nos hicimos eco](index.php/homepage/generos/software/12-software/1057-nueva-version-de-steam-play-para-estas-navidades), los chicos de Valve consiguieron solucionar diversos problemas con los protocolos de red con lo que se solucionaron varios bugs relacionados con juegos que hasta ahora dependían de ciertas tecnologías para poder funcionar.


Es precisamente el caso que nos ocupa, ya que tras actualizar Steam Play a la última versión es posible descargar y lanzar el juego, teniendo en cuenta que **la primera vez que se ejecuta da un error de shaders y se cierra**. Tras esto tan solo hay que ejecutarlo de nuevo y ya arrancará sin mayores problemas.


Además de esto, para poder jugar online **hay que seleccionar una región**, ya que si se deja la opción automática por defecto el juego no conectará con los servidores.


*'Darwin Project' tiene lugar en un futuro apocalíptico distóptico en el norte de las islas de Canadá. Como preparación para una edad de hielo, se lanzará un nuevo proyecto mitad experimento científico, mitad entretenimiento que propondrá a 10 participantes sobrevivir al frío y la muerte en una arena traicionera.*


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/RllGB0j27f0" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Puedes jugar Darwin Project gratis, y es posible comprar objetos cosméticos dentro del juego al estilo de otros "free to play", pero dado que se ejecuta en Steam Play y Proton aún está en fase beta no recomendamos gastar dinero en el juego dado que en futuras actualizaciones ya sea del juego o de Proton es posible que el juego deje de funcionar.


Puedes ver toda la información de Darwin Project en su [página web oficial](http://www.scavengers.ca/) o en su [página de Steam](https://store.steampowered.com/app/544920/Darwin_Project/).


¿Qué te parece Darwin Project? ¿Te gustaría jugarlo con otros usuarios en Linux?


Cuéntamelo en los comentarios o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

