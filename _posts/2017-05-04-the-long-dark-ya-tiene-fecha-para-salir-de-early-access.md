---
author: Serjor
category: "Exploraci\xF3n"
date: 2017-05-04 20:45:08
excerpt: <p>El fin del early access coincide con el lanzamiento de los dos primeros
  episodios del nuevo modo historia</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/d26f2d3a8ff5583681ac68eec63fdc44.webp
joomla_id: 307
joomla_url: the-long-dark-ya-tiene-fecha-para-salir-de-early-access
layout: post
tags:
- supervivencia
- the-long-dark
- early-access
title: The Long Dark ya tiene fecha para salir de early access
---
El fin del early access coincide con el lanzamiento de los dos primeros episodios del nuevo modo historia

Otro juego más para confirmar el éxito de las campañas de croudfunding. Nos enteramos gracias a [PC Gamer](http://www.pcgamer.com/the-long-dark-story-mode-release-date-set/) que The Long Dark, un juego que consiguió la financiación necesaria en Kickstarter a finales del 2013, será publicado como versión oficial el 1 de agosto de este mismo año. Esta versión full estranará el modo historia, un modo reclamado por los jugadores, y contará con los dos primeros capítulos del juego, y por lo que comentan los desarrolladores, entre finales del 2017 y el 2018 llegarán los capítulos tres, cuatro y cinco.


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" frameborder="0" height="315" src="https://www.youtube.com/embed/eg3n3nBaKPw" width="560"></iframe></div>


 


Siempre es positivo ver que juegos que han conseguido superar sus campañas de financianción llegan a buen puerto, aunque hayan pasado casi cinco años, por lo que esperamos que el juego siga funcionando bien, y sobretodo que las ventas para Linux cumplan las espectativas de los desarrolladores.


<div class="steam-iframe"><iframe frameborder="0" height="190" src="https://store.steampowered.com/widget/305620/" width="646"></iframe></div>


Y tú, ¿vas a esperar a que el juego salga de early access o ya disfrutas de este juego de supervivencia? Cuéntanoslo en los comentarios o através de nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD).

