---
author: Pato
category: Noticia
date: 2019-09-04 17:36:34
excerpt: "<p>Ya podemos disfrutar de estas interesant\xEDsimas nuevas caracter\xED\
  sticas</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/24f13dfd02fd46bd71a7e0abea42e3b3.webp
joomla_id: 1108
joomla_url: la-remodelacion-de-la-biblioteca-de-steam-llegara-este-mes-de-septiembre-en-el-cliente-beta
layout: post
tags:
- steam
- beta
- biblioteca
title: "La remodelaci\xF3n de la biblioteca de Steam llegar\xE1 este mes de Septiembre\
  \ en el cliente beta (ACTUALIZADO)."
---
Ya podemos disfrutar de estas interesantísimas nuevas características

**ACTUALIZADO 17-9-19**:


Ya podeis disfrutar del nuevo aspecto y funcionalidades de la Biblioteca de Steam si participais en la **Beta de Steam**, Desde hoy mismo , tal y como os habíamos informado hace unos días, podeis utilizarla. Si no sois usuarios de la beta de Steam y quereis sacar partido de las nuevas características, debeis ir a los "**Parámetros**" de Steam, y en la **sección de cuenta** cambiar el estado de la participación en la beta. El cliente entonces se reiniciará y se instalará en nuestro ordenador. Podeis encontrar más información sobre esta nueva biblioteca en [la página que Steam ha dedicado a ello](https://store.steampowered.com/libraryupdate), y si encontrais fallos o teneis algún tipo de sugerencia podeis hacerlo a través de [Github](https://github.com/ValveSoftware/steam-for-linux).


 




---


**NOTICIA ORIGINAL:**


Al fin parece que las novedades en cuanto a la remodelación de la biblioteca de Steam que tanto hemos estado esperando comenzarán a llegar próximamente. Tanto como **el próximo día 17 de Septiembre** según el anuncio de la propia Steam en donde además exponen las novedades que nos traerá la tienda, eso sí aún en fase de beta abierta.


Según nos cuentan, la biblioteca de Steam tiene mucho potencial por explotar, y los cambios que se nos vienen están enfocados en dinamizar todo lo posible el interés para el usuario. Así pues, *una buena biblioteca no debería ser solo unas estanterías llenas de polvo; tiene que ser un lugar divertido para explorar y encontrar lo que sea que estés buscando. Si quieres mantenerte al día sobre lo que sucede con tus juegos, encontrar algo para jugar con los amigos o ver lo que pasa en la comunidad de tu juego, tu biblioteca debería echarte una mano.*


Lo primero que anuncian es una nueva página de inicio, con información relevante de las novedades de tus juegos, tu actividad reciente y la de tus contactos.


![steambiblio1](/https://steamcdn-a.akamaihd.net/steamcommunity/publichttps://gitlab.com/jugandoenlinux/imagenes/-/raw/main/clans/27766192/148a8930b8483b5c8a24546139d8409c8fd59f33.webp)


Por otra parte, se potenciará toda la información de tus juegos para que estés al tanto de todas sus novedades al igual que estar al tanto de los amigos con los que juegas.


![steamlibrary2](/https://steamcdn-a.akamaihd.net/steamcommunity/publichttps://gitlab.com/jugandoenlinux/imagenes/-/raw/main/clans/27766192/0e6906e3d0081c8cd30640f65b84c2f4a26d7eb8.webp)


Por otra parte, también se va a potenciar el apartado de "mass media" y socialización, con nuevas opciones para poder estar al tanto de eventos, retransmisiones o actualizaciones que sean relevantes y que hasta ahora pasaban muchas veces desapercibidas de cara a los usuarios. En este aspecto, Steam también [ha implementado nuevas herramientas dentro de Steamworks](https://steamcommunity.com/groups/steamworks#announcements/detail/1599256365693384810) para los desarrolladores de manera que puedan implementar mejor todas las mejoras en este aspecto.


Si quieres saber más sobre las novedades que vienen a Steam puedes visitar el anuncio oficial [en el blog de Steam](https://steamcommunity.com/games/593110/announcements).


#### Steam Lab sigue con los experimentos


Lanzado este pasado mes de Julio, Steam Lab es un programa de "desarrollo" en el que los desarrolladores pueden testear diferentes características y funcionalidades de cara a mejorar el cliente de Steam. De este modo, algunas de las características que se han desarrollado en Steam Labs son el sistema de recomendaciones interactivo o el programa de recomendaciones que resalta las novedades destacadas del mes. En este ámbito **Lars Doucet,** un desarrollador indie de renombre [ha anunciado](https://twitter.com/larsiusprime/status/1169295659524141063) que Valve le ha contratado para implementar su desarrollo de "[Steam Divin Bell](https://www.fortressofdoors.com/steam-diving-bell/)" dentro de Steam, un sistema de recomendaciones dinámico que no se basa en tu historial de juego, si no que emplea diferentes algoritmos con los que dinamizar este sistema.


Sea como sea, las novedades comenzarán a llegar este próximo día 17 de este mes.

