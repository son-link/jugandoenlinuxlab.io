---
author: leillo1975
category: Software
date: 2020-04-27 15:26:15
excerpt: "<p>@ogre3d_official lanza la versi\xF3n 2.1 con importantes caracter\xED\
  sticas.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/OGRE/Ogre3d-logo.webp
joomla_id: 1214
joomla_url: nueva-actualizacion-del-motor-de-renderizado-libre-ogre
layout: post
tags:
- open-source
- multiplataforma
- motor-grafico
- software-libre
- ogre
title: "Nueva actualizaci\xF3n del motor de renderizado libre OGRE"
---
@ogre3d_official lanza la versión 2.1 con importantes características.


 Probablemente algunos de vosotros no hayais oido hablar nunca de [OGRE](https://github.com/OGRECave/ogre) ("**O**bject-Oriented **G**raphics **R**endering **E**ngine", **Motor de renderizado de gráficos orientado a objetos** en Español), pero tal vez si os hablamos de algunos de los juegos que lo usan tendreis más presente su importancia. Títulos comerciales como [Torchlight II](https://www.humblebundle.com/store/torchlight-ii?partner=jugandoenlinux), [Running With Rifles](https://www.humblebundle.com/store/running-with-rifles?partner=jugandoenlinux) , Ankh y [BOMB](https://store.steampowered.com/app/301500/BOMB_Who_let_the_dogfight/), o libres como [Stunt Rally](https://stuntrally.tuxfamily.org/) y [Rigs of Rods](https://www.rigsofrods.org/) forman parte de su extensa lista de juegos que lo usan.


Las ventajas de este motor gráfico son muy interesantes, y a parte de ser **Libre** (Licencia MIT) y **multiplataforma** (Linux, Windows, Mac, Android, iOS...), destaca por que su diseño está pensado para ser **sencillo y rápido**, evitando al desarrollador el uso de capas inferiores de librerias gráficas como Direct3D o OpenGL. Ademas provee una **interfaz basada en objetos** y otras clases de alto nivel. Hace tan solo unas horas, a través de twitter nos llegaba el anuncio de esta nueva versión:



> 
> Ogre 2.1 Released!<https://t.co/1gUjmPuiwS> [#ogre3d](https://twitter.com/hashtag/ogre3d?src=hash&ref_src=twsrc%5Etfw) [#gamedev](https://twitter.com/hashtag/gamedev?src=hash&ref_src=twsrc%5Etfw)
> 
> 
> — Official Ogre Tweets (@ogre3d_official) [April 26, 2020](https://twitter.com/ogre3d_official/status/1254553902076383233?ref_src=twsrc%5Etfw)







En esta nueva actualización, con el sobrenombre "Baldur" encontraremos las siguientes mejoras con respecto a la anterior:


*-**Hlms** (High Level Material System) para generar sombreadores automáticamente. Sustituye al RTSS y a los sombreadores manuales*  
 *-**PBS** - Sombreado basado en la física*  
 *-**Nuevo compositor.** Más flexible, más rápido y poderoso*  
*-**OGRE 1.x refactorizado** para aumentar el rendimiento por varios factores; utilizando técnicas amigables con la caché (diseño orientado a datos), instrucciones SIMD, AZDO (Aproximación de controlador cero), creación de instancias automáticas y subprocesamiento múltiple  
-**Compatibilidad con Windows** Vista / 7/8/10, **macOS** a través de Metal y OpenGL, iOS a través de Metal, **Linux** a través de OpenGL (aunque **se está trabajando también en Vulkan**)  
-**Muchas características nuevas**: luces de área, mapas de cubo corregidos de paralaje, luces agrupadas hacia adelante, HDR, mapas de sombra exponenciales y más*


Como veis, un montón novedades que esperemos que tengan su traducción en nuevos y más avanzados proyectos, así como la mejora de los existentes. Teneis más información en su [página web oficial](https://www.ogre3d.org/). Podeis darnos vuestra opinión sobre este software en los comentarios de este artículo, en nuestra comunidad de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).


 

