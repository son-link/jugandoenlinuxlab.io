---
author: Pato
category: Estrategia
date: 2018-07-04 14:28:58
excerpt: "<p>Ya podeis adquirir este cl\xE1sico editado por <span class=\"username\
  \ u-dir\" dir=\"ltr\">@THQNordic</span></p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/ac9ff722ccee9f796dea38c810f03e02.webp
joomla_id: 795
joomla_url: desperados-wanted-dead-or-alive-puede-estar-en-camino-para-linux-steamos
layout: post
tags:
- accion
- estrategia
- rumor
- tactico
- western
- isometrica
title: '''Desperados: Wanted Dead or Alive'' puede estar en camino para Linux/SteamOS
  (ACTUALIZADO)'
---
Ya podeis adquirir este clásico editado por @THQNordic

**ACTUALIZADO 9-7-18**: Cuando el rio suena, agua lleva.... finalmente los rumores no eran infundados y desde hace unos dias es posible correr este clásico de la estrategia en nuestros equipos.  Si disfrutasteis con la saga Commandos, uno de los videojuegos más aclamados de la industria de nuestro pais e influencia de juegos más actuales como "[Shadow Tactics](index.php/homepage/generos/estrategia/item/242-shadow-tactics-ya-disponible)", con "Desperados: Wanted Dead or Alive" podreis tener una experiencia muy similar pero cambiando el escenario de la 2ª Guerra Mundial por el Salvaje Oeste. Se trata de uno de esos juegos atemporales que no le sienta mal el paso de los años. Además no os va a dejar tiesos por que tiene un precio más que interesante, ya que por menos de 5€ podreis disfrutarlo. Os dejamos con un trailer del juego:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/fchnfVYv8ws" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


Podeis adquirir "Desperados: Wanted Dead or Alive" en la [Humble Store](https://www.humblebundle.com/store/desperados-wanted-dead-or-alive?partner=jugandoenlinux) (**patrocinado**) o en [Steam](https://store.steampowered.com/app/260730/Desperados_Wanted_Dead_or_Alive/).


 




---


**NOTICIA ORIGINAL**: Por sorpresa acaba de aparecer el clásico 'Desperados: Wanted Dead or Alive' en SteamDB, lo que aunque no sea garantía de que el juego acabe llegando a nuestro sistema favorito si que supone un indicio de que puede terminar llegando.


Indagando en el repositorio, existe un apartado para linux del juego, pero aún no hay contenido... esperemos que Spellbound (THQ Nordic) esté moviendo ficha y nos traiga este y otros de sus clásicos a Linux.


**Sinopsis:**


'Desperados: Wanted Dead or Alive' es el juego clásico de estrategia en tiempo real que nos lleva al lejano oeste donde tendremos que llevar a cabo nuestras aventuras como mercenarios.


*"Desperados: Wanted Dead or Alive" es el primer juego de estrategia que combina una atmósfera basada en películas e historia de un juego de aventuras con el desafío intelectual de un juego táctico en tiempo real."*


El Paso, un pueblo típico del Lejano Oeste en el suroeste de los Estados Unidos, cerca de la frontera con México. Es el año 1881. Durante los últimos meses, los trenes pertenecientes a la famosa compañía de ferrocarriles Twinnings & Co han sido blanco de toda una serie de emboscadas. La dirección de la empresa ha decidido que ya es suficiente y tienen que poner fin a las redadas. Ha ofrecido una recompensa de $ 15,000 para cualquiera que capture al líder de la banda responsable.


Características:


* Más de 20 enemigos con diferentes perfiles, desde el bandido perezoso y cobarde hasta el temible mercenario.
* Un equipo de 6 personajes, cada uno con sus características muy distintas, para administrar en tiempo real.
* 25 niveles diferentes ofrecen una intrigante y mordaz emoción, con escenas de transición cinemáticas y paisajes que te dejarán sin aliento.
* Cada enemigo se clasifica en 10 criterios diferentes, que incluyen inteligencia, valor, puntería, resistencia al alcohol, etc.
* Una variedad de escenarios diferentes: día, noche, tormentas, crepúsculo, pueblos, pueblos fantasmas, fortalezas, minas de oro, salones, prisiones, embarcaciones 6 niveles de entrenamiento para permitir al jugador entrar realmente en la historia.


¿Que te parece que 'Desperados: Wanted Dead or Alive' llegue a Linux/SteamOS? ¿Te gustan los clásicos de estrategia?


Cuéntamelo en los comentarios o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

