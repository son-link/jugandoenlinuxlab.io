---
author: Pato
category: Arcade
date: 2019-07-26 11:30:09
excerpt: "<p>El juego de @SirlinGames se convierte por m\xE9ritos propios en el mejor\
  \ exponente de lucha en nuestro sistema</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/dbe2064f7b08a4acf308a99bd5078f1b.webp
joomla_id: 1081
joomla_url: fantasy-strike-sale-de-acceso-anticipado-y-ya-esta-disponible-de-forma-oficial-en-linux-steamos
layout: post
tags:
- accion
- indie
- multijugador
- arcade
- lucha
title: "Fantasy Strike sale de acceso anticipado y ya est\xE1 disponible de forma\
  \ oficial en Linux/SteamOS"
---
El juego de @SirlinGames se convierte por méritos propios en el mejor exponente de lucha en nuestro sistema

Tras mas de dos años de desarrollo y pasar por fase de acceso temprano, por fin tenemos disponible de forma oficial **Fantasy Strike**. Como ya sabréis se trata de un juego de lucha de estilo arcade con un estilo que recuerda mucho a otros títulos del género como puede ser Street Fighter. La pregunta ahora es si el juego estará a la altura.


De momento, ya podemos adelantaros que **Fantasy Strike es,** **desde nuestro punto de vista el nuevo exponente en cuanto a juegos de lucha disponibles de forma nativa en nuestro sistema favorito**, cosa que corroboraremos en un próximo análisis aquí, en jugandoenlinux.com.


Por lo pronto, y para celebrar el lanzamiento, los chicos de Sirlin Games nos obsequian con un trailer cinemático para presentar a los luchadores:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/yK2sx6Z1hsI" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Fantasy Strike es un juego diseñado por un veterano desarrollador de Street Fighter para satisfacer tanto al jugador novato u ocasional como al veterano ya que su jugabilidad basada en el ya típico "facil de jugar, difícil de dominar" que consigue que cualquiera pueda adaptar su juego para obtener la victoria, y con la suficiente profundidad de desarrollo como para incluir un "**modo torneo**" en el que organizar un evento y vernos las caras si así lo deseamos.


Las características más destacables del juego son:


- Juego online y online multiplataforma.


- Modo desafío a un amigo o espectador con un click.


- Partida rápida online.


- Modo arcade y supervivencia.


- Desafío diario donde se activa un evento al día para probar tu capacidad de supervivencia.


- Modo prácticas


- Local Versus, con batallas por equipos 1vs1 y 3vs3 donde cada jugador elige 3 personajes y lucha a ganar 3 de 5 combates.


- Tutorial de 5 minutos donde aprenderás todo lo necesario para comenzar a jugar.


- Spotlight videos de cada personaje donde podrás ver todo acerca de cada uno de ellos, sus movimientos y puntos fuertes o débiles.


- Controles simplificados: todos los movimientos se hacen con un solo botón por lo que el juego se adapta a cualquier mando.


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/0MCka4Z_fNQ" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Hemos de ser sinceros, ya que en Jugando en Linux hemos estado probando el juego y respecto a los mandos hemos corrido una suerte desigual ya que parece haber un pequeño bug con Unity, el motor que usa el juego y que hace que algunos mandos no sean detectados correctamente en algunas configuraciones, cosa que el estudio afirma estar al corriente para solucionarlo lo antes posible.


En cuanto a los requisitos, estos son los publicados:


**Mínimo:**


* Requiere un procesador y sistema operativo de 64-bit
* **OS:** Ubuntu 12.04 y superiores
* **Procesador:** Intel Core i5-4302Y @ 1.6GHz, Celeron G1840 @ 2.8 GHz / AMD Athlon II X3
* **Memoria:** 4 GB RAM
* **Graficos:** GeForce GT 555M, 9800 GTX / Radeon R7, HD 8500
* **HD:** 6 GB disponibles


Si te interesan los buenos juegos de lucha, puedes echarle un vistazo a Fantasy Strike en su página de Steam [en este enlace](https://store.steampowered.com/app/390560/Fantasy_Strike/), o visitar su [página web oficial](http://www.fantasystrike.com/) para mas información.


¿Qué te parece Fantasy Strike? ¿Te gustan los juegos de lucha?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux en [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

