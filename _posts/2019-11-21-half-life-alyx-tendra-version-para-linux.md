---
author: leillo1975
category: Realidad Virtual
date: 2019-11-21 20:34:21
excerpt: "<p>El t\xEDtulo de Realidad Virtual de @valvesoftware ser\xE1 lanzado proximamente.</p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/HalfLife/Alyx/HalfLifeAlyx.webp
joomla_id: 1134
joomla_url: half-life-alyx-tendra-version-para-linux
layout: post
tags:
- realidad-virtual
- valve
- htc-vive
- half-life-2
- valve-index
- alyx
title: "\xA1\"Half Life: Alyx\" (NO) tendr\xE1 versi\xF3n para Linux! (ACTUALIZADO)"
---
El título de Realidad Virtual de @valvesoftware será lanzado proximamente.


**ACTUALIZACIÓN:** Tras un aviso de un miembro de nuestra comunidad, tristemente tenemos que decir que el juego solo contará en un principio con soporte para Windows, tal y como pudimos comprobar en la [página de Steam](https://store.steampowered.com/app/546560/HalfLife_Alyx/) y la [web oficial](https://www.half-life.com/en/alyx/). Os pedimos disculpas por no comprobarlo antes y dejarnos llevar por la emoción (ni que fuésemos principiantes). Si es así, será si no me equivoco la primera vez desde hace años que Valve lanza un producto sin soporte para nuestro sistema, lo cual no deja de ser preocupante. Esperemos que pronto tengamos noticias más alagüeñas para nuestro sistema de este juego. Mientras seguiremos esperando con los dedos cruzados.




---


Ciertamente no es Half Life 3, pero es lo que más se le puede asemejar. Por fin un título de categoría por parte de la compañía que regenta Steam, y además dentro del universo Half Life. En esta ocasión **nos pondremos en la piel de "Alyx"**, la chica que acompañaba a Gordon Freeman en Half Life 2 e hija del doctor Eli Vance. Esta es la información que encontramos sobre el juego en steam:


*Half-Life: Alyx es el regreso de Valve en realidad virtual a la serie Half-Life. Es la historia de una lucha imposible contra una raza alienígena cruel conocida como la Alianza, **situada entre los eventos de Half-Life y Half-Life 2.***   
  
*Jugando como Alyx Vance, eres la única oportunidad de supervivencia de la humanidad. El control de la Alianza sobre el planeta desde el incidente de Black Mesa solo se ha fortalecido a medida que acorralan a los habitantes que quedan en las ciudades. Entre ellos se encuentran algunos de los mejores científicos de la Tierra: tú y tu padre, el Dr. Eli Vance.*   
  
*Como fundadores de una resistencia incipiente, habéis continuado vuestra actividad científica clandestina, realizando investigaciones críticas y construyendo herramientas invaluables para los pocos humanos lo suficientemente valientes como para desafiar a la Alianza.*   
  
*Todos los días, aprendes más sobre tu enemigo, y cada día trabajas para encontrar su punto débil.*   
  
***SOBRE LA FORMA DE JUGAR EN RV:***   
  
*El regreso de Valve al universo de Half-Life, la serie que lo empezó todo, se hizo desde cero para la realidad virtual. La realidad virtual se creó para permitir una jugabilidad que se encuentra en el corazón de Half-Life.*   
  
*Sumérgete en profundas interacciones ambientales, resolución de rompecabezas, exploración del mundo y combate visceral.*   
  
*Apóyate sobre una pared rota y debajo de una lapa para realizar un disparo imposible. Rebusca entre los estantes para encontrar una jeringa curativa y algunos cartuchos de escopeta. Manipula herramientas para hackear las interfaces alienígenas. Lanza una botella por una ventana para distraer a un enemigo. Quítate un cangrejo de la cabeza y lánzaselo a un soldado de la Alianza.*  
  
***Contenido adicional para propietarios de Index***  
  
*Quien haya comprado el hardware de Valve Index a finales del 2019 tendrá acceso a contenido adicional único a primeros del año que viene:*  
  



*Explora los entornos de Half-Life: Alyx en tu espacio de SteamVR Home*


*Alterna la apariencia de tu arma para adornar el arsenal de Alyx*


*Contenido especial con la temática Half-Life: Alyx para Counter-Strike: Global Offensive.*  
  
***Entornos construidos por la comunidad***  
  
*Con el juego, se lanzará un conjunto de herramientas de Source 2 para construir nuevos entornos, lo que permitirá a cualquier jugador construir y contribuir con nuevos entornos para que la comunidad disfrute. Hammer, la herramienta de creación de niveles de Valve, se ha actualizado con todas las herramientas y componentes de jugabilidad de realidad virtual en el juego.*


El juego **llegará a nuestros PC's en el mes de Marzo del próximo año**, por lo que en tan solo unos meses se podrá jugar. Por desgracia **serán solo los poseedores de un sistema de Realidad Virtual quienes podrán disfrutar de este juego**, siendo compatible con HTC Vive, Occulus Rift, Microsoft Mixed Reality y **Valve Index, siendo el juego en el caso de este último gratuito**. Para los demás tendrá un precio de **49.99€**. Para los que lo reserven ahora tendrán un [descuento del 10%](https://store.steampowered.com/app/546560/HalfLife_Alyx/). Podeis encontrar mucha más información en la [web ofcial del juego](https://www.half-life.com/en/alyx/). Os dejamos con el espectacular trailer del anuncio:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/O2W0N3uKXmo" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 ¿Esperabas que lo nuevo de Valve fuese un título de Realidad Virtual? ¿Qué te parece que en esta ocasión la protagonista sea Alyx? Cuéntanoslo en los comentarios, o en mensajes en nuestra comunidad de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

