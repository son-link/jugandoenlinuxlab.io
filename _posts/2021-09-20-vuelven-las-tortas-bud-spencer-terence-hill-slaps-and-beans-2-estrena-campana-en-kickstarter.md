---
author: Pato
category: "Acci\xF3n"
date: 2021-09-20 16:30:02
excerpt: "<p>El estudio Trinity Team confirma la versi\xF3n para Linux de su pr\xF3\
  ximo juego</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SlapsAndBeans/slapsandbeans2.webp
joomla_id: 1363
joomla_url: vuelven-las-tortas-bud-spencer-terence-hill-slaps-and-beans-2-estrena-campana-en-kickstarter
layout: post
title: "\xA1Vuelven las tortas!: Bud Spencer & Terence Hill - Slaps And Beans 2 estrena\
  \ campa\xF1a en Kickstarter"
---
El estudio Trinity Team confirma la versión para Linux de su próximo juego


¡Vuelven los reyes de las tortas a Linux!. Tras el éxito del primer título el estudio **Trinity Team** vuelve a la carga con **Bud Spencer & Terence Hill - Slaps And Beans 2** y la fórmula que les dió el éxito, y es que sin sobresalir en ningún apartado concreto, el primer Slaps and Beans **consigue su objetivo que es divertir**. Si quieres comprobarlo, puedes ver el [análisis del título que hicimos en Jugando en Linux]({{ "/posts/analisis-bud-spencer-terence-hill-slaps-and-beans" | absolute_url }}).


Es por esto que este estudio profesional cuyos miembros se definen como "fans" de Spencer y Hill quieren ampliar horizontes con un nuevo título de la serie que, como es normal anticipa más escenarios de sus films, más situaciones inverosímiles, más minijuegos y muchas, muchas tortas.


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/4x44ijZnM_Q" title="YouTube video player" width="560"></iframe></div>


 


**El primer Slaps and Beans** tuvo versión para Linux desde su salida, ya que fue anunciada en su primera y exitosa campaña en Kickstarter. Sin embargo para esta segunda aventura el estudio no lo dejó claro, pero gracias a la insistencia de los usuarios en la campaña el estudio [ha anunciado que Bud y Terence hablan "penguinese"](https://www.kickstarter.com/projects/1684041218/bud-spencer-and-terence-hill-slaps-and-beans-2/posts/3306668), y por tanto l**a versión Linux está confirmada para su lanzamiento**.


En el momento de escribir estas líneas el proyecto lleva recogidos algo más de 53.000€ de un objetivo de 220.000€. Se  antoja un tanto elevada esta cantidad, pero si quieres apoyar al estudio y que el juego llegue a buen puerto, puedes aportar tu granito de arena en su campaña en Kickstarter:


<div class="resp-iframe"><iframe height="420" src="https://www.kickstarter.com/projects/1684041218/bud-spencer-and-terence-hill-slaps-and-beans-2/widget/card.html?v=2" width="220"></iframe></div>


 Desde Jugando en Linux nos declaramos fans de Spencer y Hills, y ya estamos deseando echarle el guante a este nuevo beat'em up y liarnos a tortas con los malos en nuestro sistema favorito...


Si quieres amenizar la espera mientras llega esta nueva entrega de Slaps and Beans, tienes el primer título de oferta en Steam con un descuento del 75%:


 <div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/564050/" width="646"></iframe></div>


¿Qué te parece Slaps And Beans 2? ¿jugaste el primer juego?


Cuéntamelo en los comentarios o en los canales de Jugando en Linux de [Telegram](https://twitter.com/JugandoenLinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

