---
author: Pato
category: Rol
date: 2017-08-11 13:54:48
excerpt: "<p>Se trata de un ARPG que quiere recuperar la jugabilidad cl\xE1sica de\
  \ este tipo de juegos</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/617a2776f20d4e817afffbdbb257cffa.webp
joomla_id: 429
joomla_url: goken-ya-esta-disponible-en-linux-en-acceso-anticipado-gracias-al-apoyo-de-la-comunidad
layout: post
tags:
- accion
- rol
- acceso-anticipado
title: "GOKEN ya est\xE1 disponible en Linux en acceso anticipado gracias al apoyo\
  \ de la comunidad"
---
Se trata de un ARPG que quiere recuperar la jugabilidad clásica de este tipo de juegos

Muchas veces nos cuestionamos si el mostrar nuestro apoyo a los desarrolladores en los foros es adecuado para que nos traigan versiones de sus juegos a Linux. Sin embargo en multitud de ocasiones se ha demostrado que si, que a veces el mostrar apoyo ayuda a los desarrolladores a ver que hay interés por sus juegos y que la comunidad Linuxera quiere poder jugar a sus títulos. En este caso GOKEN llega a Linux precísamente gracias a las peticiones que los usuarios han hecho en los [foros del juego](http://steamcommunity.com/app/671260/discussions/0/1470840994962008530/?tscn=1502458603) en Steam. Al ver el fuerte apoyo de la comunidad el estudio comenzó a considerar seriamente el portar el juego a Linux, y tras unos días de espera hoy ya lo tenemos disponible para nuestro sistema favorito.


GOKEN [[web oficial](https://www.gianty.co.jp/goken/)] es un juego de rol y acción (ARPG) diseñado por el estudio GIANTY.Inc que quiere redefinir el género de los juegos de rol y acción de estilo japonés con mecánicas originales, gráficos preciosistas y una jugabilidad clásica en un mundo abierto.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/g0V0cwqM9k0" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/BfGdGQu8OV0" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


 El juego está aún en acceso anticipado, y no está disponible en español, pero si añoras este tipo de juegos (a mi me recuerda mucho a los juegos de rol que jugaba en mi juventud) lo tienes disponible a un precio bastante atractivo en Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/671260/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 ¿Te gustan los ARPG's de la vieja escuela? ¿Piensas jugar este GOKEN?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

