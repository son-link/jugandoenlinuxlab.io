---
author: Pato
category: Aventuras
date: 2017-06-12 08:59:35
excerpt: "<p>La aventura espacial de los creadores de Gone Home llegar\xE1 el pr\xF3\
  ximo 2 de agosto</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/161bb2cd9d87d4fb2583e55eca6a3af4.webp
joomla_id: 369
joomla_url: tacoma-pone-fecha-a-su-lanzamiento-y-parece-que-vendra-a-linux
layout: post
tags:
- indie
- aventura
- exploracion
title: "Tacoma pone fecha a su lanzamiento y parece que vendr\xE1 a Linux"
---
La aventura espacial de los creadores de Gone Home llegará el próximo 2 de agosto

'Tacoma' es de esos títulos llamados "walking simulators", o aventuras en las que vas "caminando" mientras se desarrolla la acción. Un buen ejemplo de este estilo de juegos es "[Proteus](http://store.steampowered.com/app/219680/Proteus/?snr=1_5_9__300)" o el propio "[Gone Home](http://store.steampowered.com/app/232430/Gone_Home/)". En este caso, en Tacoma la acción se desarrolla en una base espacial donde tendremos que "investigar" y profundizar en las diferentes historias de la tripulación para averiguar el por qué de su situación:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/osgba8G_LWw" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


 



> 
> *Tacoma es una aventura narrativa que tiene lugar en una estación espacial de alta tecnología en el año 2088. A lo largo de tu misión explorarás cada detalle de la vida y el trabajo de la tripulación de la estación y encontrarás las pistas que conformarán una apasionante historia de confianza, miedos y determinación frente al desastre.*
> 
> 
> *El sistema de vigilancia digital del centro, situado en el corazón de Tacoma, ha capturado las grabaciones 3D de los momentos decisivos de la vida de la tripulación en la estación. Conforme vayas explorando, te rodearán los ecos de estos momentos. Tendrás que emplear tus habilidades para retroceder, avanzar hacia delante y moverte por el espacio físico de estas complejas y entrecruzadas escenas para examinar los acontecimientos desde todos los ángulos, de forma que puedas reconstruir los distintos niveles de la historia a medida que explores.*
> 
> 
> 


En la [web oficial](https://tacoma.game/) no aparece ninguna mención a su lanzamiento en Linux, pero siendo Fullbright Studios quienes están detrás, y que tal y como puede verse en Steam el juego luce ya el icono de Linux/SteamOS el lanzamiento para nuestro sistema favorito está casi asegurado. Los requisitos sin embargo aún no están publicados.


Si quieres echar un vistazo a este Tacoma y saber más, o precomprarlo con un 10% de descuento si te gustan este tipo de juegos, lo puedes ver en su web de Steam donde llegará traducido al español el próximo 2 de Agosto:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/343860/" width="646"></iframe></div>


 

