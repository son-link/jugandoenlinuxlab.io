---
author: Pato
category: Aventuras
date: 2019-05-29 16:23:04
excerpt: "<p>El juego de <span class=\"css-901oao css-16my406 r-1qd0xha r-ad9z0x r-bcqeeo\
  \ r-qvutc0\" dir=\"auto\">@DevespressoG</span> lleg\xF3 ayer para retarnos en combate</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/ffc9a76e4f88b66c863df72bab4ef694.webp
joomla_id: 1052
joomla_url: vambrace-cold-soul-ya-disponible-con-soporte-en-linux-steamos-de-salida
layout: post
tags:
- indie
- aventura
- rol
- roguelike
title: 'Vambrace: Cold Soul ya disponible con soporte en Linux/SteamOS de salida'
---
El juego de @DevespressoG llegó ayer para retarnos en combate

Hoy nos hacemos eco del lanzamiento de **Vambrace: Cold Soul**, un juego de estilo roguelike con toques de aventura y lucha que recuerda mucho a Darkest Dungeon, aunque se separa de este en las mecánicas y dificultad haciéndolo algo más asequible para el que no le gusta estar "frustrándose" con el juego. Vambrace: Cold Soul fue lanzado ayer mismo con soporte para Linux/SteamOS de salida, lo que siempre es de agradecer.


*Vambrace: Cold Soul es un roguelike de aventura y fantasía en medio de un helado paisaje. Planea tus expediciones subterráneas, viaja a la superficie de la ciudad maldita con tu grupo de héroes. ¡Usa poderes únicos, evita peligrosas trampas, encuentra seres extraños y sobrevive al combate mortal!*


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/iAbsjz1AMB8" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


*Vambrace: Cold Soul es un juego guiado por la narrativa llena de personajes memorables, desafíos brutales y profunda estrategia. Para tener éxito, debes elegir líderes perspicaces, acampar para recuperarte, navegar por lugares extraños y sobrevivir a un combate mortal. ¿Hurgarás suministros para venderlos o los usarás para elaborar nuevos objetos para tu próxima expedición? La superficie de Icenaire es fría e implacable. Así que, prepara bien tu grupo antes de partir... no sea que te unas a los no-muertos de la ciudad maldita.*  
  
En este juego, la diferencia entre la vida y la muerte se reduce a una buena planificación e inteligentes tácticas.


Entre sus características destacan:


* Embárcate en una épica aventura de fantasía que abarca 7 intrigantes capítulos.
* Completa misiones secundarias para desbloquear hasta 26 nuevos atuendos temáticos.
* Crea tu grupo con 5 tipos de razas y 10 clases únicas de reclutas.
* Elabora objetos y armaduras de metales preciosos encontrados en tus viajes.
* Toma decisiones cruciales donde un pequeño error puede sellar el destino de tu grupo.
* Prepárate bien o condena a tus compañeros a una muerte permanente.
* Encuentra páginas del códice para ampliar la historia de Ethera y la tradición mítica.


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/VH2GtnaYft8" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


El estudio reconoce influencias en juegos como Castlevania, Skyrim o el propio Darkest Dungeon. Si quieres Saber más o te interesa comprar el juego, lo tienes disponible en su [página de Steam](https://store.steampowered.com/app/904380/Vambrace_Cold_Soul/) con un 10% de descuento por su lanzamiento.


¿Que te parece Vambrace: Cold Soul? ¿Te gustan los juegos de acción roguelike tipo "Darkest Dungeon"?


Cuéntamelo en los comentarios o en el canal de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux).

