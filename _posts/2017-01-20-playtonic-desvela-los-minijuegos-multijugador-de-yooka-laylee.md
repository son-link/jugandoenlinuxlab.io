---
author: Pato
category: "Exploraci\xF3n"
date: 2017-01-20 12:52:11
excerpt: <p>Los "Juegos arcade Rextro" llegan al mundo de Yooka Laylee</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/d382bd8ae87d9139df6458192532657c.webp
joomla_id: 183
joomla_url: playtonic-desvela-los-minijuegos-multijugador-de-yooka-laylee
layout: post
tags:
- plataformas
- aventura
- proximamente
- exploracion
title: Playtonic desvela los minijuegos multijugador de 'Yooka Laylee'
---
Los "Juegos arcade Rextro" llegan al mundo de Yooka Laylee

Creo que alguna vez lo he dicho: soy "backer" de Yooka Laylee y sigo el juego con mucha atención pues es uno de los títulos que más espero en este año. Ayer Playtonic nos desveló que el juego tendrá "minijuegos" donde podrás disfrutar solo o con hasta cuatro amigos de ocho juegos de estilo retro como "arena brawler Glaciators", "racer Kartos Karting" o "Jobstacle Course". [Aquí el anuncio](http://www.playtonicgames.com/rextros-arcade-powers/).  



<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/4VK2eCZYvlY" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


 Yooka Laylee llegará a nuestros sistemas el próximo 11 de Abril, y ya puedes hacer la reserva del juego en [GOG](https://www.gog.com/game/yookalaylee) o Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/360830/143749/" width="646"></iframe></div>


 ¿Qué te parece 'Yooka Laylee? ¿Piensas jugarlo?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux en [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

