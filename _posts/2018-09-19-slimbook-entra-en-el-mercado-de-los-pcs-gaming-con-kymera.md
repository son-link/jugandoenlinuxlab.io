---
author: Pato
category: Hardware
date: 2018-09-19 10:05:25
excerpt: "<p>Se tratar\xEDa del primer PC sobremesa \"preconfigurado\" para poder\
  \ jugar tomando como base un sistema Linux</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/d985bd01090113d5c682cff917f71c98.webp
joomla_id: 855
joomla_url: slimbook-entra-en-el-mercado-de-los-pcs-gaming-con-kymera
layout: post
tags:
- hardware
- gaming
- slimbook
- kymera
title: Slimbook entra en el mercado de los PCs "gaming" con KYMERA
---
Se trataría del primer PC sobremesa "preconfigurado" para poder jugar tomando como base un sistema Linux

Estamos en un momento "dulce" en cuanto al juego en Linux. De eso no cabe duda tras los recientes acontecimientos respecto a [SteamPlay](https://www.jugandoenlinux.com/index.php/homepage/generos/software/item/960-steam-play-de-valve-da-un-salto-de-gigante-y-amplia-enormemente-el-catalogo-para-linux-steamos-actualizado), y el impulso que está tomando el gaming con Vulkan, drivers para Linux etc.


Hasta ahora todo el que juega en Linux debe optar por jugar con PCs "custom", o sea montados por uno mismo o por una tienda a base de piezas, y luego instalar el sistema operativo u optar por uno de los modelos de portátiles "gaming" que tan de moda se están poniendo últimamente y que marcan tendencia ahora mismo y "cambiar" el sistema operativo por defecto. También podemos optar por equipos pre-ensamblados sin SO y luego montarle el sistema que queramos, pero si algo faltaba era una opción "todo incluido" que estuviese pensado desde la base para jugar con una distro Linux, y pensada para todo aquel que no quiera complicarse con instalaciones o ensamblaje.


Los chicos de [SLIMBOOK](https://slimbook.es/), hasta ahora conocidos por lanzar equipos portátiles y "All in One" preconfigurados y optimizados para sistemas Linux han sabido ver esta necesidad y han anunciado el lanzamiento de una línea de PCs de sobremesa pensados en "exprimir" la potencia necesaria para "tareas pesadas", incluyendo como no el juego en Linux.


Así pues, SLIMBOOK nos presenta la "familia" KYMERA. Y decimos familia por que realmente no se trata de un solo PC "preconfigurado" al uso, si no de varias configuraciones específicas que permiten incluso cierto grado de personalización. Tenemos como tales dos opciones **KYMERA VENTUS** y **KYMERA AQUA**.


#### KYMERA VENTUS: gama media configurable


Con **VENTUS** SLIMBOOK ha diseñado un sistema de base comenzando por procesadores Intel de octava generación, comenzando por un i3 8100 (4 núcleos, 4 hilos, 3,6 GHz) como novedad, ya que la compañía nunca había montado equipos por debajo de los i5. Sin embargo, para esta configuración podemos elegir también entre un i5 8500 o un i7 8700K (desbloqueado) con lo que podemos tener una configuración lo suficientemente potente como para jugar durante los próximos años con garantías.


![Ventus](https://slimbook.es/images/Product-Pages/Kymera-ventus/Testing/ventus.jpg)


En cuanto al resto de la configuración, se compone de base con 8GB de RAM Kingston a 2666MHz ampliable hasta 64 GB, o podemos optar por memoria de 4000 MHz de GSkill con luces led incluidas hasta 32GB, tarjeta gráfica integrada aunque para sacar partido al equipo es recomendable incluir la opción de una Geforce GTX 1060 de 3GB, una GTX 1070 de 8GB o una GTX 1080 de 11GB. También podemos optar por cambiar la refrigeración de stock del procesador por un disipador NOX H-190 o NOX H-312 e incluir diversas opciones de almacenamiento tanto con discos duros mecánicos, SSD o M.2. También podemos optar por distintos sistemas Linux, o un sistema dual-boot con Linux-Windows.


En cuanto a la caja, es una custom con aluminio cepillado de alta densidad y laterales de metacrilato transparente con acabado tintado. Toda la refrigeración de componentes de la gama VENTUS es por aire.


En Jugando en Linux hemos hecho una configuración de base para un KYMERA VENTUS con **configuración mínima para jugar por tan solo 824€** incluyendo el i3, gráfica GTX 1060 3GB, 8 GB de RAM y disco duro mecánico de 1 Tera.


Puedes ver la gama VENTUS y su configuración [en este enlace](https://slimbook.es/pedidos/slimbook-kymera/kymera-ventus-comprar).


#### KYMERA AQUA: Cuando el compromiso entre equilibrio y rendimiento no es opción.


Con AQUA SLIMBOOK nos presenta una configuración de gama alta con componentes premium empezando por una placa base MSI Z370M GAMING PRO AC con GAMING BOOST y **OVERCLOCK.**En este caso tendremos como base los procesadores i7 8700K y 9900K, gráficas GTX 1080Ti o RTX 2080Ti, y como su propio nombre indica la refrigeración correrá a cargo de un circuito RL custom con opción de refrigeración solo CPU o CPU+GPU.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/_Gl4Z_K0AsY" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Todo esto se puede complementar con opciones de RAM Kingston desde 8GB a 2666MHz hasta los 64GB de RAM GSkill a 4000MHz e iluminación LED, discos duros en distintas configuraciones de SSD, M.2 y mecánicos, tarjeta WIFI AC dual band y distintas configuraciones de sistemas operativos, incluyendo varias distribuciones Linux o sistema Dual-Boot Linux-Windows.


En cuanto al chasis, se trata del mismo para toda la familia KYMERA, en aluminio cepillado de alta densidad y paneles de metacrilato transparente con acabado tintado.


**El KYMERA AQUA parte con un precio de base de 1995€**


Puedes ver la gama AQUA y su configuración [en este enlace](https://slimbook.es/pedidos/slimbook-kymera/kymera-aqua-comprar).


#### ¿Solo procesadores Intel y gráficas Nvidia?


De momento es lo que hay. SLIMBOOK ha querido ceñirse a lo seguro y de momento ha dejado de lado otras opciones, pero viendo que en la comunidad Linuxera ya hay voces preguntando por una opción con Ryzen-Radeon, y conociendo a SLIMBOOK una empresa que sabe escuchar a sus clientes y usuarios no es descartable que podamos ver alguna configuración con hardware AMD en el futuro.


¿Qué te parecen las propuestas de SLIMBOOK? ¿Te gusta alguno de estos modelos para jugar?


Cuéntame lo que piensas en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [discord](https://discord.gg/ftcmBjD).


Os dejamos un vídeo de la fiesta Open Source que llevaron a cabo los chicos de SLIMBOOK en Madrid y donde se pudo ver el KYMERA AQUA en acción moviendo "The Witcher 3"


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/974pYia6KjU" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>

