---
author: Serjor
category: "Acci\xF3n"
date: 2018-04-02 13:23:03
excerpt: "<p>El parche 1.2.3 nos llevar\xE1 a volar otra vez</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/52c96c35c30d11cfb13711cfa4231ac4.webp
joomla_id: 699
joomla_url: everspace-tendra-su-primera-version-oficial-muy-pronto
layout: post
tags:
- roguelike
- unreal-engine
- everspace
- unreal-engine-4
- lanzamiento
title: "EVERSPACE tendr\xE1 su primera versi\xF3n oficial muy pronto"
---
El parche 1.2.3 nos llevará a volar otra vez

Leemos en [phoronix](https://www.phoronix.com/scan.php?page=news_item&px=EVERSPACE-Official-Linux) que EVERSPACE, el roguelike espacial del que os [hablamos](index.php/homepage/analisis/item/605-analisis-everspace) en su [momento](index.php/homepage/generos/exploracion/item/767-nueva-beta-disponible-para-everespace) y que tanto nos gustó a Leo y a mí, tiene ya fecha para su salida oficial en GNU/Linux, así que es de esperar que ya hayan solucionado sus problemas con los drivers de las gráficas de AMD.


Y es que según podemos ver en el último [anuncio](http://steamcommunity.com/games/396750/announcements/detail/1645377042186633284) de la desarrolladora, entre mediados y finales de abril esperan tener la versión 1.2.3 lista, donde habrán migrado a la versión 4.18 del motor Unreal Engine 4, y el parche traerá mejoras y correcciones, todo esto junto con la primera versión oficial para nuestro sistema.


Podéis ver aquí un par de gameplays del juego que hicimos Leo y yo:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/G6zj57jcJSM" width="560"></iframe></div>


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/-Qzk8XnR0wY" width="560"></iframe></div>


Os dejamos también con el ¿teaser? del lanzamiento en GNU/Linux:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/aEYhlrFnRUg" width="560"></iframe></div>


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/396750/" style="border: 0px;" width="646"></iframe></div>


Y tú, ¿te harás con EVERSPACE tras su salida oficial? Cuéntanoslo en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD)

