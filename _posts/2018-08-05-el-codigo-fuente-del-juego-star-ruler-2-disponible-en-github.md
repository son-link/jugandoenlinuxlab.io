---
author: Serjor
category: Estrategia
date: 2018-08-05 13:47:12
excerpt: "<p>Una iniciativa que permitir\xE1 al juego sobrevivir al paso del tiempo</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/15af91003feb7702e5dbaacbf2f17e89.webp
joomla_id: 819
joomla_url: el-codigo-fuente-del-juego-star-ruler-2-disponible-en-github
layout: post
tags:
- rts
- open-source
- star-ruler-2
- 4xrts
title: "El c\xF3digo fuente del juego Star Ruler 2 disponible en GitHub"
---
Una iniciativa que permitirá al juego sobrevivir al paso del tiempo

Sí, no son noticias frescas, y no hubiera sido por un miembro de nuestra comunidad de telegram, quizás se nos hubiera pasado para siempre, pero tal y cómo nos recuerda Diego:







Tal cómo véis, vía [maslinux](http://maslinux.es/el-juego-de-estrategia-star-ruler-2-pasa-a-ser-software-libre/) podemos leer que el juego Star Ruler 2 tiene su código fuente disponible. Start Ruler 2 es un 4X-RTS, con una ambientación espacial que recuerda a Stellaris.


Podemos encontrar el código fuente del juego en su repositorio de [GitHub](https://github.com/BlindMindStudios/StarRuler2-Source), donde ya nos advierten de que la música del juego no está incluida, pero si disponemos de la música de la versión comercial, se puede poner en el directorio correspondiente y el juego la reproducirá. También ha quitado todo el código para acceder a las APIs de SteamWorks, así que si la versión de steam soportaba mods, ahora ya no (quizás ahora que el código está disponible, tampoco sea imprescindible del todo).


Si queréis jugar a este Star Ruler 2 sin tener que compilar, podéis usar la versión para snap que han creado, con el siguiente comando:



> 
> sudo snap install starruler2
> 
> 
> 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/DRle_rkEqHI" width="560"></iframe></div>


Y tú, ¿probarás la versión Open Source de este Star Ruler 2? Cuéntanoslo en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD)

