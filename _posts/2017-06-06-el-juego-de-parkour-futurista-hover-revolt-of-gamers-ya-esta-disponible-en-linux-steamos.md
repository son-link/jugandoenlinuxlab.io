---
author: Pato
category: Plataformas
date: 2017-06-06 16:40:53
excerpt: "<p>Carreras fren\xE9ticas de habilidad en un mundo futurista</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/814d386f5c4138112e5fa70430be6661.webp
joomla_id: 359
joomla_url: el-juego-de-parkour-futurista-hover-revolt-of-gamers-ya-esta-disponible-en-linux-steamos
layout: post
tags:
- accion
- carreras
- indie
- mundo-abierto
- parkour
title: "El juego de parkour futurista 'Hover: Revolt Of Gamers' ya est\xE1 disponible\
  \ en Linux/SteamOS"
---
Carreras frenéticas de habilidad en un mundo futurista

A falta de Mirror's Edge o Jet Set Radio, ya tenemos disponible en nuestro sistema favorito este 'Hover: Revolt Of Gamers' [[web oficial](http://hover-rog.com/)] que recuerda mucho a aquellos, pero con un enfoque algo distinto.



> 
> *Hover: La Revolución de los Gamers es un electrizante juego de parkour en un mundo abierto y futurista para uno o varios jugadores.*
> 
> 
> *El juego se desarrolla en ECP17, una ciudad de alta tecnología también conocida como Ciudad Hover, y que se ubica en un planeta lejano. El Gran Administrador ha cortado toda comunicación con la Unión Galáctica y ha establecido una opresiva dictadura. Divertirse es ahora ilegal y se ha prohibido cualquier tipo de entretenimiento.*
> 
> 
> *Hover: La Revolución de los Gamers es una experiencia para uno o varios jugadores. En cualquier momento puedes pasar de jugar solo a jugar en línea y unirte a tus amigos o a otros jugadores del mundo para progresar en la aventura y cooperar o jugar contra ellos. No importa dónde estés y lo que estés haciendo, siempre podrás conectarte. Hover: La Revolución de los Gamers es, además, un juego centrado en su comunidad y ofrece a los jugadores las herramientas para crear misiones o minijuegos.*
> 
> 
> 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/Zr6KUgYj6-k" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


Los requisitos mínimos para moverlo son:


SO: Ubuntu 12.04+, SteamOS+64-bit  
Procesador: 3 GHz  
Memoria: 4 GB de RAM  
Gráficos: NVIDIA® GeForce® GTX 650 / AMD Radeon™ HD 6870  
Almacenamiento: 2 GB de espacio disponible


'Hover: Revolt Of Gamers' está disponible en Steam traducido al español, y además con un 20% de descuento:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/280180/" width="646"></iframe></div>


¿Qué te parece? ¿Te gustan los juegos de tipo parkour? ¿Piensas jugarlo?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 

