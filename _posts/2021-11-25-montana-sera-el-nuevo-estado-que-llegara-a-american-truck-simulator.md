---
author: leillo1975
category: "Simulaci\xF3n"
date: 2021-11-25 14:48:01
excerpt: "<p>Esta nueva #DLC est\xE1 siendo desarrollada por&nbsp;@SCSsoftware .&nbsp;</p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ATS/Montana/ATS_Montana_Announced.webp
joomla_id: 1391
joomla_url: montana-sera-el-nuevo-estado-que-llegara-a-american-truck-simulator
layout: post
tags:
- dlc
- american-truck-simulator
- ats
- scs-software
- montana
title: "Montana ser\xE1 el nuevo estado que llegar\xE1 a American Truck Simulator"
---
Esta nueva #DLC está siendo desarrollada por @SCSsoftware . 


 En primer lugar nos gustaría pediros disculpas por la tardanza en redactar este artículo, pues este anuncio ya ha sido hecho hace unos días, pero como sabeis los quehaceres diarios mandan. Como sabeis siempre que podemos nos hacermos eco de todas las expansiones de mapa que lanza la compañía checa para sus juegos de camiones. En este caso, como podeis ver, **el anuncio le toca a American Truck Simulator,** cuando aun estamos disfrutando de la última Wyoming, que si recordais [cubrimos ampliamente en nuestra web]({{ "/tags/wyoming" | absolute_url }}), así como en nuestro canal de [Youtube](https://www.youtube.com/playlist?list=PLmIplMAAvKnWh6x7dL8EMC1l0p0wn_GiF). 


Aunque **no hay ninguna fecha anunciada**, nosotros desde aquí humildemente podemos predecir que no llegará hasta probablemente el segundo cuarto del año, presumiblemente antes del verano, pues como sabeis **ahora mismo estará enfrascados terminando** [Texas]({{ "/posts/american-truck-simulator-llegara-a-texas-en-su-proxima-dlc" | absolute_url }}), que muy probablemente, y repito , es una especulación absoluta, llegará a finales de este año, osea dentro de nada.


![](https://scssoft.com/press/american_truck_simulator_montana/images/002.jpg)


 "El estado del Tesoro" (**The Treasure State**), es como se le conoce a este "rincón" de Norteamérica, **está al norte lindando con Canada, y teniendo como vecinos a las Dakotas, Wyoming e Idaho** (hay que ver la de geografía que se aprende con este juego). Según la descripción oficial facilitada por SCS, encontraremos:


*Montana alberga experiencias y momentos extraordinarios que le dejarán boquiabierto. Este estado del norte de Estados Unidos es indómito, salvaje y natural. Montana es la puerta de entrada a Glacier y Yellowstone -dos joyas de la corona del sistema de parques nacionales- y alberga relucientes lagos glaciares, picos montañosos nevados, arroyos con truchas de cinta azul y los poderosos ríos Missouri, Yellowstone y Flathead. Esto es sólo el principio; el impresionante paisaje montañoso de Montana no hace más que acentuar el encanto rústico de los pequeños pueblos que lo rodean. Y los senderos del interior del país conducen a espacios salvajes y aislados en los que todavía es posible estar completamente solo.*


*Extenso, virgen y salvaje, el "Estado del Gran Cielo" representa lo mejor del Oeste americano. Los rudos vaqueros aún ensillan para cabalgar en los ranchos y sorprender a las multitudes en los rodeos. Los pueblos fantasma, los parques estatales y la ruta de los dinosaurios, única en su género, conservan una deslumbrante variedad de tesoros históricos.*


![](https://scssoft.com/press/american_truck_simulator_montana/images/005.jpg)


Como veis de nuevo estaremos ante unos paisajes verdes y frondosos, con imponentes montañas y grandes riquezas naturales, no siendo extraño esto al tener de vecino al estado de Wyoming, que si recordais ya mostraba muchas caracteristicas semejantes en su parte norte.


Si quereis tener más información sobre este futuro lanzamiento de SCS, podeis echarle un ojo al [artículo de su Blog](https://blog.scssoft.com/2021/11/introducing-montana.html), y por supuesto también podreis mostrar vuestro interés en esta DLC añadiéndola a vuestros deseados en su página de [Steam](https://store.steampowered.com/app/1811080/American_Truck_Simulator__Montana/). Nosotros como siempre os seguiremos informando sobre este y otros trabajos de esta compañía. Os dejamos con el trailer del anuncio:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/i1x_PWFkR8w" title="YouTube video player" width="780"></iframe></div>

