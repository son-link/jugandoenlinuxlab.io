---
author: Serjor
category: "Acci\xF3n"
date: 2018-08-15 15:55:24
excerpt: <p>Se confirma la fecha del Early Access del juego</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/1148c0c947ba828ea344776aa0a78f01.webp
joomla_id: 832
joomla_url: ya-tenemos-fecha-para-el-lanzamiento-en-steam-de-crazy-justice
layout: post
tags:
- early-access
- crazy-justice
title: Ya tenemos fecha para el lanzamiento en Steam de Crazy Justice
---
Se confirma la fecha del Early Access del juego

Después de tantas fechas no confirmadas y de tantos rumores, Black Riddles Studio nos anuncia vía twitter la fecha de salida de su Crazy Justice:



> 
> [#CRAZYJUSTICE](https://twitter.com/hashtag/CRAZYJUSTICE?src=hash&ref_src=twsrc%5Etfw) FANS! Are you here?! No more “around” or “soon”! Let’s spill the beans! Let the cat out of the bag! [#STEAM](https://twitter.com/hashtag/STEAM?src=hash&ref_src=twsrc%5Etfw) EARLY ACCESS R.E.L.E.A.S.E. D.A.T.E. is AUGUST 23, 2018! HEY WORLD, ARE YOU READY?! [#GAME](https://twitter.com/hashtag/GAME?src=hash&ref_src=twsrc%5Etfw) [#GAMERS](https://twitter.com/hashtag/GAMERS?src=hash&ref_src=twsrc%5Etfw) [#GAMING](https://twitter.com/hashtag/GAMING?src=hash&ref_src=twsrc%5Etfw) [#PLAYSTATION4](https://twitter.com/hashtag/PLAYSTATION4?src=hash&ref_src=twsrc%5Etfw) [#XBOXONE](https://twitter.com/hashtag/XBOXONE?src=hash&ref_src=twsrc%5Etfw) [#NINTENDOSWITCH](https://twitter.com/hashtag/NINTENDOSWITCH?src=hash&ref_src=twsrc%5Etfw) [#LINUX](https://twitter.com/hashtag/LINUX?src=hash&ref_src=twsrc%5Etfw) [#MAC](https://twitter.com/hashtag/MAC?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/7ZuXqFtjOG](https://t.co/7ZuXqFtjOG)
> 
> 
> — Black Riddles Studio (@BlackRiddles) [15 de agosto de 2018](https://twitter.com/BlackRiddles/status/1029747654945898496?ref_src=twsrc%5Etfw)


  





 


La verdad, es que será un lanzamiento un tanto agridulce, ya que en un primer momento no iba a ser F2P, después el modo Battle Royale sí, el resto del juego no, que si para saldría mucho antes, que si el 15 de agosto, y encima estamos hablando un juego en Early Access... Esperemos que finalmente este 23 de agosto sea la fecha definitiva y que el juego sea lo suficientemente divertido como para que haya merecido la pena.


 

