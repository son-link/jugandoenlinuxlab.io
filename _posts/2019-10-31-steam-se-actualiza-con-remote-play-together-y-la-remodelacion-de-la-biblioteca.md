---
author: leillo1975
category: Software
date: 2019-10-31 08:07:59
excerpt: <p>Estas "novedades" llegan finalmente a la rama estable del cliente.</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/1784e00b0b60f953190c43cb5b6689b8.webp
joomla_id: 1126
joomla_url: steam-se-actualiza-con-remote-play-together-y-la-remodelacion-de-la-biblioteca
layout: post
tags:
- steam
- valve
- oficial
- biblioteca
- remote-play-together
title: "Steam se actualiza con \"Remote Play Together\" y la remodelaci\xF3n de la\
  \ Biblioteca."
---
Estas "novedades" llegan finalmente a la rama estable del cliente.

Aunque no son ninguna novedad, el cliente de Steam se ha actualizado ayer con importantes cambios y adiciones en su rama estable. Valve ha incluido en Steam dos importantes características que muchos ya estaban disfrutando en la rama beta de Steam. Si recordais hace casi dos meses os hablábamos de [la nueva Biblioteca](index.php/homepage/noticias/39-noticia/1230-la-remodelacion-de-la-biblioteca-de-steam-llegara-este-mes-de-septiembre-en-el-cliente-beta), y más recientemente de [Remote Play Together](index.php/homepage/noticias/39-noticia/1240-steam-anadira-una-nueva-caracteristica-para-jugar-juegos-con-multijugador-local-llamada-remote-play-together). Tal y como se puede ver en el [anuncio oficial](https://steamcommunity.com/games/593110/announcements/detail/1666821776739358716), **finalmente todos los usuarios de Steam pueden disfrutar a partir de ya de estas nuevas funcionalidades**. Para ello solo tienen que esperar a que el cliente se actualice automáticamente o hacerlo a mano en el menu, dándole a "Comprobar si existen nuevas actualizaciones del Cliente de Steam" y reiniciando el programa.


Para quien no esté muy al tanto de estas novedades recapitularemos un poco. La [nueva Bliblioteca](https://steamcommunity.com/games/593110/announcements/detail/1666821776739358716) de steam permite al usuario, a parte de un **notable cambio estético**, encontrar la información sobre sus juegos de una forma mucho más directa y ordenada. En ella encontraremos un **página de Inicio renovada**, donde veremos por ejemplo los juegos a los que hemos estado jugando recientemente o que títulos se han actualizado recientemente. También podremos ver la **actividad de nuestros amigos,** y así saber si están conectados y a que están jugando. **Los eventos además se muestran ahora de una forma mucho más amable y funcional**, en general todo está mucho más **ordenado** y su uso es más **intuitivo**.


![Steamnewlibrary](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Steam/Steamnewlibrary.webp)


En cuanto a [Remote Play Together](https://steamcommunity.com/games/593110/announcements/detail/3032537193879549687), se trata de una nueva funcionalidad que **nos permitirá jugar con nuestros amigos en multiplayer local, pero de forma online y cada uno en su ordenador**. Para ello solo hace falta que uno de los jugadores tenga el juego y el resto (hasta un total de 4) podrá unirse a la partida. Por poner ejemplos de esta funcionalidad, podremos jugar en compañia de amigos a juegos de Lucha, o juegos que solo permitan la **pantalla partida** como opción de multijugador. (mi cabeza se va indiscutiblemente a [Slipstream](index.php/homepage/generos/carreras/2-carreras/1005-slipstream-actualizado-con-soporte-multijugador) o [Horizon Chase Turbo](index.php/homepage/analisis/20-analisis/888-analisis-horizon-chase-turbo), por poner ejemplos). Para ello steam retransmite **Video, Audio, Entradas y Voz** a través de internet para que parezca que tus amigos están sentados a tu lado jugando, permitiendo que jugadores tanto de Linux, como de Windows y Mac puedan jugar sin ningún tipo de restricciones usando tanto teclado y ratón, como nuestros mandos favoritos.


![RemotePlayTogeteher](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Steam/RemotePlayTogeteher.webp)


Como veis, más funcionalidades y facilidades para que disfrutemos a tope de nuestros juegos favoritos de Steam, y un **nuevo valor añadido que diferencia claramente el producto de Valve de su competencia**, y por supuesto, como siempre, todo ello compatible con nuestro sistema operativo favorito, GNU-Linux.


Y tu.... ¿que opinas de estas novedades?, ¿has probado ya Remote Play Together?, ¿qué te parece el nuevo aspecto de la biblioteca? Cuéntanoslo en los comentarios o deja tus mensajes en nuestra comunidad de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

