---
author: Pato
category: "Acci\xF3n"
date: 2017-03-01 18:40:12
excerpt: "<p>As\xED nos lo ha confirmado el desarrollador durante su exitosa campa\xF1\
  a de Greenlight</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/edab5e545cc8cf6a49faaa8a9ea6a3d0.webp
joomla_id: 234
joomla_url: between-the-stars-un-juego-de-accion-y-estrategia-espacial-llegara-a-linux-tras-su-publicacion-en-otros-sistemas
layout: post
tags:
- accion
- indie
- espacio
- estrategia
title: "'Between the Stars' un juego de acci\xF3n y estrategia espacial llegar\xE1\
  \ a Linux tras su publicaci\xF3n en otros sistemas"
---
Así nos lo ha confirmado el desarrollador durante su exitosa campaña de Greenlight

'Between the Stars' es un ambicioso proyecto del estudio español Isolated Games [[web oficial](http://www.isolatedgames.com/)] en el que tomarás el mando de una nave espacial militar en la que tendrás que luchar para llegar a la capital de la república para defenderla de los ataques de otros mundos. Durante tu viaje tendrás que tomar decisiones que afectarán a tu nave y tu tripulación, comprar mejorar armamento y sistemas de tu nave, y luchar contra otras naves enemigas.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/PBffi_NqvSI" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>

Como puedes ver, el juego luce fantástico, con un apartado visual destacable y un apartado estratégico que le da un toque diferente. Según las propias palabras del estudio "*el estilo del juego se parece mucho a FTL. Hay un mapa estelar generado proceduralmente en el que tendremos que avanzar para acercarnos al portal mas cercano a la capital. Cada vez que lleguemos a una nueva sección se generará un nuevo evento. El resultado del evento dependerá de nuestras decisiones, el nivel de nuestra tripulación y otros factores*".


![nav](https://imageshack.com/i/poMQ3Bdtg.jpg)


'Between the Stars' ha conseguido el Greenlight en tan solo 8 días, y el desarrollador tiene previsto lanzar el juego a mediados de este mismo año tal y como expone en el [anuncio de su campaña](http://steamcommunity.com/sharedfiles/filedetails/updates/868350256/1488321639). Respecto a la versión en Linux, el desarrollador nos confirmó en los comentarios que su intención es lanzar una versión para nuestro sistema unos meses después del lanzamiento en Windows y Mac:

> 
> De manera inicial se lanzará para Windows y Mac. Posteriormente lo lanzaré para linux. Lo hago así para no lanzar 3 versiones de golpe y poder solucionar los bugs iniciales más rápidamente, una vez pulido lo portearé y me centraré en pulir los fallos específicos que pueda tener la versión de Linux. Quizás habrán unos 2 meses de diferencia entre la salida en Windows y la salida en Linux.
> 
> 
> 

Esperaremos con impaciencia para poder contar qué tal es este 'Between the Stars'. Por cierto, a continuación tienes una sesión de juego:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" frameborder="0" height="360" src="https://www.youtube.com/embed/Y-tjEcJlZ58" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


 Tienes todos los detalles en su [página de Greenlight](http://steamcommunity.com/sharedfiles/filedetails/?id=868350256).


¿Qué te parece este 'Between the Stars'? ¿Piensas jugarlo?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

