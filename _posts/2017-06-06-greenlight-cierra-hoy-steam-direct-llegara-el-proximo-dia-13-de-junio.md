---
author: Pato
category: Editorial
date: 2017-06-06 18:14:03
excerpt: <p>El sistema por el cual la comunidad daba su apoyo a los desarrollos indie
  deja de existir en la plataforma de Valve</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/464b3f74fd6601955ccb022f610c3111.webp
joomla_id: 360
joomla_url: greenlight-cierra-hoy-steam-direct-llegara-el-proximo-dia-13-de-junio
layout: post
tags:
- steam
- greenlight
title: "Greenlight cierra hoy; Steam Direct llegar\xE1 el pr\xF3ximo d\xEDa 13 de\
  \ Junio"
---
El sistema por el cual la comunidad daba su apoyo a los desarrollos indie deja de existir en la plataforma de Valve

Así es. Steam Greenlight deja de estar operativo desde hoy mismo. Así lo han anunciado oficialmente en los foros de Steam:



> 
> Steam Blog:  
> Closing Greenlight Today, Steam Direct Launches June 13 <https://t.co/Kzca4qoo3u>
> 
> 
> — Steam Database (@SteamDB) [June 6, 2017](https://twitter.com/SteamDB/status/872150812994269189)



 Steam Greenlight era el sistema por el cual la comunidad podía "votar" por los desarrollos que considerasen más interesantes para que llegasen a publicarse en Steam.


Greenlight fue inaugurado en 2012, y durante todo el tiempo que ha estado operativo ha servido como catapulta para desarrollos y juegos de éxito incontestable como [Rogue Legacy](http://store.steampowered.com/app/241600/Rogue_Legacy/), [Verdun](http://store.steampowered.com/app/242860/Verdun/) o [Stardew Valley](http://store.steampowered.com/app/413150/Stardew_Valley/). Sin embargo y por desgracia también hemos visto cómo han ido llegando cada vez más y más juegos de "dudosa calidad" que han hecho que la tienda de Valve se llene de "morralla" que a veces molesta más que otra cosa. Es por esto que Valve decidió cerrar Greenlight y sustituirlo por otro sistema que tratará de servir de "filtro" a la llegada de nuevos títulos que vayan "en serio" sin que ello suponga un obstáculo insalvable. Aquí es donde entra...


#### **Steam Direct**


Steam Direct será la nueva vía de entrada para los títulos "indies" que pretendan acceder a vender en Steam. ¿Como funciona?


Pues mediante el pago de una tasa de 100 dólares que serán recuperables una vez que el juego en cuestión facture 1000$. ¿Será esto suficiente para hacer "criba" de títulos "menores"? El tiempo lo dirá, pero desde luego Steam necesitaba tomar cartas en el asunto ya que la "avalancha" de juegos "morralla" empezaba a ser preocupante.


Steam Direct comenzará su andadura el próximo día 13 de Junio, y puedes ver el anuncio oficial [en este enlace](http://steamcommunity.com/games/593110/announcements/detail/1265922321514182595).


¿Qué te parece la decisión de Valve? ¿Te convence el sistema "Steam Direct" o preferías "Greenlight"?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 

