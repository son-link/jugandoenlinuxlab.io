---
author: Son Link
category: Estrategia
date: 2022-11-03 15:26:59
excerpt: "<p>Publicada una nueva versi\xF3n mayor de este veterano juego de estrategia.</p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Warzone2100/portada_4.3.1.webp
joomla_id: 1496
joomla_url: warznone-2100-4-3
layout: post
tags:
- estrategia
- actualizacion
- warzone-2100
- estrategia-en-tiempo-real
title: Warznone 2100 4.3 (Actualizado)
---
Publicada una nueva versión mayor de este veterano juego de estrategia.


**ACTUALIZADO 03-11-22:**


Ya ha sido publicada la nueva versión mayor del juego, exactamente la **4.3.1**, y estos son algunos de los cambios con respecto a la segunda beta:


* Nueva banda sonora adicional "**Aftermath**" (de **Lupus-Mechanicus**)
* Añadida la opción de gráficos "**Distancia LOD**", para ajustar la nitidez de las texturas a distancia (por defecto en "**Alta**")
* Añadidas nuevas opciones de vídeo ("**Minimizar al perder el foco**", "**Alt+Enter Alternar**")
* Añadido soporte para texto bidireccional usando **fribidi**
* Mostrar el rango que tendrá una unidad si se produce a continuación
* Añadir un mensaje en la consola cuando los límites cambian
* Mejoras en el renderizado de textos y efectos translúcidos
* Ahora se mantiene la relación de aspecto en las secuencias en pantalla completa


Tenéis una lista de todos los cambios en [esta entrada en su página](https://wz2100.net/news/version-4-3-1/)




---


  
Warzone 2100 sigue gozando de buena salud, y buena muestra de ello es que se ha publicado una nueva beta de la próxima versión del juego, la 4.3.0, la cual traerá una buena lista de novedades, y tras algo más de **540 commits** desde la ultima versión (la 4.2.7, que se lanzo el 7 de marzo de este año).


Entre una larga lista de cambios tenemos:


* Soporte para el formato de audio OPUS. Todos los audios se han convertido a dicho formato.
* El selector de idioma ahora es un dropdown con banderas
* Soporte para una compresión básica de las texturas
* Varias mejoras en Vulkan
* Se han simplificados las lógicas de las reparaciones para evitar "atascos"
* Varias refacciones/trabajos previos para futuras construcciones de Emscripten
* Fuego de artillería por encima de los muros
* Añadida una dificultad Súper Fácil a la campaña
* Compilaciones para Flatpak
* Mejorada una vez más la IA de Cobra (equilibrada contra la investigación defensiva / sistema cuando se juega en mapas de mucho petróleo en 1v1)
* Actualizado el equilibrio de la campaña (y correcciones) para: Alfa 2, Alfa 6, misiones Beta posteriores, Final Beta y Gamma 2
* Arma directa: no dispara cuando está bloqueada por un punto duro de defensa


La lista de cambios es muy larga tenéis el listado completo de cambios [aquí](https://raw.githubusercontent.com/Warzone2100/warzone2100/4.3.0-beta2/ChangeLog)  
  
Seguiremos atentos a las novedades de este lanzamiento, y como digo siempre, pásate por nuestro canal de Telegram, o las salas de Matrix o Discord si quieres jugar.  
  
Y antes de terminar dejo esta pequeña joya publicada en el canal oficial del juego en YouTube, donde han reescalado el vídeo de introducción de la campaña Alfa, con un resultado, para mi, bastante bueno:


 


 


 

