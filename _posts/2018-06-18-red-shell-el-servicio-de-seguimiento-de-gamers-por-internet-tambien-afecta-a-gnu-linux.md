---
author: Serjor
category: Noticia
date: 2018-06-18 21:30:51
excerpt: <p>El gran hermano ha llegado al mundo de los videojuegos</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/5778877be1a35482f319be586a4e5dc3.webp
joomla_id: 774
joomla_url: red-shell-el-servicio-de-seguimiento-de-gamers-por-internet-tambien-afecta-a-gnu-linux
layout: post
tags:
- red-shell
- spyware
title: "Red shell, el servicio de seguimiento de gamers por internet tambi\xE9n afecta\
  \ a GNU/Linux"
---
El gran hermano ha llegado al mundo de los videojuegos

Como bien nos comentaba Odín esta mañana en el canal de Telegram:







La que se está liando...


Vamos a intentar ponernos en situación, e intentar entender qué es esto de Red Shell, si nos afecta o no, y si se puede hacer algo más aparte de desinstalar los juegos afectados. En primer lugar os emplazamos al post original en [reddit](https://www.reddit.com/r/Steam/comments/8pud8b/psa_red_shell_spyware_holy_potatoes_were_in_space/) donde se ha montado un gran revuelo respecto a que bastantes juegos en steam (y es de suponer que en otras plataformas de distribución de juegos) hacen uso de los servicios de la página <redshell.io>, y como podréis ver en el enlace del comentario de telegram, también ha llegado a GNU/Linux.


Pero, ¿qué es realmente red shell? Es un servicio que se encarga de recopilar datos para los desarrolladores de los videojuegos, de tal manera que estos puedan tener más datos para evaluar el flujo más influyente, si por ejemplo un link en Twitch tiene más repercusión que un link en Facebook, por lo que los desarrolladores pueden afinar mejor sus promociones, campañas de marketing o cualquier movimiento que deseen hacer.


¿Es esto spyware? Bueno... Aquí que cada cuál saque sus propias conclusiones, pero tenemos que tener en cuenta que **según dicen** y **a día de hoy**, no recolectan ningún *dato personal* del jugador, simplemente en qué enlaces pulsa cuando navega, y para ello recopila, según [su página web](https://redshell.io/gamers), y no siendo únicamente estos los datos:


* IP, aunque hasheada
* Sistema operativo
* Navegador web y su versión
* Resolución de pantalla
* Un identificador de usuario (no me ha quedado claro si es uno que se crea para el juego y jugador en cuestión, o es el SteamId)
* Fuentes que usará el Navegador


Según dicen ellos, no registran información personal, lo que hacen registrar dispositivos, esto es, emparejar jugador de un juego y en qué enlaces relacionados con el juego ha usado y desde qué página web.


Una página web, que registra los movimientos que realizamos por internet, que recopila información sobre nuestros dispositivos, y tiene la capacidad de identificarnos a cada paso que demos navegando, y todo esto sin informar y pedir consentimiento al usuario, bueno, no seremos nosotros quienes valoremos si es spyware o no, pero como mínimo da que pensar.


Y tanto ha dado que pensar, que muchas desarrolladoras que hacían uso de los servicios de red shell ya han anunciado que van a dejar de trabajar con ellos y que eliminaran este "servicio" próximamente. No obstante, si somos desconfiados por naturaleza podemos tomar algunas medidas, podemos añadir las siguientes líneas a nuestro fichero /etc/hosts (las url de Unity3D es porque a este motor también le gusta llamar a casa y dar información)


[Card](https://www.reddit.com/r/linux_gaming/comments/8rs2d3/psa_red_shell_spyware_is_in_a_lot_of_popular/e0tt2xh)




Veremos a ver en qué queda todo, y si la comunidad gamer tiene que seguir avisando y denunciando estas invasiones a la privacidad. Quizás por cosas como esta nos guste tanto la filosofía Open Source.

