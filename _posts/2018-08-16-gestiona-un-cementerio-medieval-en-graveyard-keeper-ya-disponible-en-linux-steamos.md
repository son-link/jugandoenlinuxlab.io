---
author: Pato
category: Aventuras
date: 2018-08-16 11:12:55
excerpt: <p>El juego de @LazyBearGames nos propone un "negocio" muy lucrativo</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/873599ca0af52bdbb5630522d6563fd5.webp
joomla_id: 834
joomla_url: gestiona-un-cementerio-medieval-en-graveyard-keeper-ya-disponible-en-linux-steamos
layout: post
tags:
- indie
- aventura
- pixel
- rpg
title: Gestiona un cementerio medieval en 'Graveyard Keeper', ya disponible en Linux/SteamOS
---
El juego de @LazyBearGames nos propone un "negocio" muy lucrativo

¿Te gustan los juegos de gestión? Graveyard Keeper [[web oficial](https://www.graveyardkeeper.com/)] es el simulador medieval de gestión de cementerios más inexacto de todos los tiempos. Construye y administra tu propio cementerio, y amplía a otras empresas, mientras encuentras atajos para reducir costos. Usa todos los recursos que puedas encontrar. Después de todo, este es un juego sobre el espíritu del capitalismo, y hacer lo que sea necesario para construir un negocio próspero. Y también es una historia de amor.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/Zd5S5rlKf_M" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


**Características:**


* Enfréntate a dilemas éticos. ¿Seguro que quieres comprar carne buena para las hamburguesas del festival de la quema de brujas? ¿O aprovecharás los recursos que tienes a tu alrededor?
* Reúne valiosos recursos y fabrica objetos nuevos. Amplía tu cementerio para convertirlo en un negocio boyante, reúne recursos valiosos que están desperdigados por las zonas circundantes y explora lo que esta tierra puede ofrecerte.
* Misiones y cadáveres. Estos cadáveres no necesitarán todos esos órganos, ¿no? ¿Por qué no los trituras y se los vendes al carnicero? O, si te gustan los juegos de rol, no dudes en afrontar misiones.
* Explora mazmorras misteriosas. Ningún juego medieval estaría completo sin ellas. Viaja a lo desconocido y encuentra nuevos ingredientes útiles, que podrían envenenar (o no) a un montón de aldeanos cercanos.


Si quieres gestionar tu propio cementerio, tienes Graveyard Keeper en [Humble Bundle](https://www.humblebundle.com/store/graveyard-keeper?partner=jugandoenlinux) (enlace de afiliado), [GoG](https://www.gog.com/game/graveyard_keeper) o en su [página de Steam](https://store.steampowered.com/app/599140/Graveyard_Keeper/)

