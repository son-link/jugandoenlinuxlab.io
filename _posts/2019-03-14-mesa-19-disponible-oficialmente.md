---
author: leillo1975
category: Software
date: 2019-03-14 08:11:41
excerpt: "<p>La biblioteca gr\xE1fica de c\xF3digo abierto se actualiza</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/b5eb64b92ec2bc303a0ab3ae5d25b105.webp
joomla_id: 994
joomla_url: mesa-19-disponible-oficialmente
layout: post
tags:
- vulkan
- opengl
- amd
- intel
- mesa
- transform-feedback
- freesync
title: Mesa 19 disponible oficialmente.
---
La biblioteca gráfica de código abierto se actualiza

El avance imparable de los drivers gráfico abiertos es un hecho, pero todo esto no serviría de nada si no fuese acompañado de Mesa, una implementación genérica de renderizado 3D. Gracias a ella podemos disfrutar, entre otras cosas, de los juegos desarrollados con las **APIs OpenGL y Vulkan**. Como viene siendo [costumbre](index.php/homepage/generos/software/12-software/809-disponible-mesa-18), a estas alturas del año, se suele presentar la nueva versión, en este caso la 19, que ya lleva siendo usada desde hace meses, hasta con 7 versiones candidatas, con muy buenos resultados en modo de prueba. Ayer por la tarde se anunciaba oficialmente la llegada de Mesa 19, como se puede ver en el anuncio proporcionado por [FreeDesktop.org](https://lists.freedesktop.org/archives/mesa-dev/2019-March/216696.html). 


Como todos sabeis, si sois usuarios de soluciones gráficas de AMD o Intel la actualización a este nueva versión de Mesa es altamente recomendada pues su cantidad de novedades y optimizaciones es muy alta. Entre las principales características de esta nueva versión encontraremos:


-**FreeSync/AdaptiveSync** en tarjetas AMD, lo que permitirá usar monitores con estas tecnologías en gráficas Radeon.


-Soporte para **Transform Feedback** en soluciones de Intel. Esto especialmente útil cuando usamos [DXVK](index.php/homepage/generos/software/12-software/1102-dxvk-llega-finalmente-a-la-version-1-0).


-El **Soft FP64/INT64 fue finalmente fusionado** para ayudar a los controladores gráficos que carecen de ARB_gpu_shader_fp64


-En RadeonSI algunas **nuevas extensiones OpenGL** de la especificación 4.6


-**Optimizaciones** para las tareas en **AMD Zen** para el modo GLThread que ayudaría a mejorar el rendimiento en juegos


-Para gráficas **Vega**, RADV pemite el "**primitive binning**" por defecto


-Mejoras importantes en **GalliumNine** (D3D9)


-**Autotools ha sido desaprobado** a favor del sistema de construcción **Meson**.


-Multitud de mejoras menores en OpenGL y Vulkan.


Si no sois ya usuarios de veriones de experimentales no hay que decir muy alto que deberíais actualizar vuestros sistemas operativos para poder utilizar todas estas nuevas características. Si sois usuarios de Ubuntu, y quereis instalar esta última versión de Mesa disponeis del [repositorio de Padoka Estable](index.php/foro/drivers-para-graficas-amd-intel/7-ppa-drivers-mesa-no-oficial-de-paulo-dias), aunque en estos momentos no aparece aun disponible, se espera que en los próximos días se actualice y podais disfrutarla.


¿Sois usuarios de Mesa?¿Qué os parece el avance que ha tenido estos últimos años? Déjanos tu opinión en los comentarios, o charla con nosostros sobre este tema en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

