---
author: leillo1975
category: Hardware
date: 2019-04-15 08:42:04
excerpt: <p>@OpenRazer ofrece drivers para esta conocida marca</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/88f8f2b43d0833f0d7a890636dcf72fe.webp
joomla_id: 1020
joomla_url: configura-en-linux-tus-perfericos-razer
layout: post
tags:
- hardware
- openrazer
- personalizacion
- perifericos
title: "Configura en Linux tus perf\xE9ricos Razer."
---
@OpenRazer ofrece drivers para esta conocida marca

En la comunidad de PC existe una marca de periféricos bastante conocida que fabrica ratones, teclados, auriculares e incluso portátiles, siendo estos bastante conocidos por estar enfocados al publico más gamer, ofreciendo diseños exclusivos pensados especialmente para ellos. Esta marca es [Razer](https://www2.razer.com/es-es/), y por supuesto las funciones básicas de su hardware son perfectamente compatibles con nuestro sistema. Pero como jugadores que somos, aunque estemos del lado del pingüino, también nos gustaría disfrutar de la personalización que nos pueden ofrecer esta marca.


Gracias al artículo publicado por **@BreoganGal,** uno de lo miembros de nuestra comunidad, en esa manífica web que es [Lignux](https://lignux.com/como-configurar-tus-perifericos-razer-en-ubuntu-linux-mint-o-derivadas/), nos hemos enterado de la existencia de [OpenRazer](https://openrazer.github.io/), un [proyecto de software libre](https://github.com/openrazer/openrazer) independiente de la marca que amplia en Linux las capacidades de ese hardware, **permitiendonos configurar sus Leds RGB, así como otras funciones** reseñables como el modo de juego y la creación de perfiles para estos y otros extras. Para ello crea un demonio que se ejecuta en segundo plano para interactuar con tus dispositivos Razer, y una aplicación gráfica desde tendremos acceso a todas sus funciones.


![Razer perifericos gnu linux](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Razer-perifericos-gnu-linux.webp)


**El número de dispositivos soportados es muy amplio** y en este momento se puede instalar facilmente en la **mayoría de las distribuciones más importantes** (Arch, Fedora, Debian, Ubuntu, Solus...). Teneis las instrucciones para hacerlo en la [sección de descargas de su página](https://openrazer.github.io/#download). Así que ya sabeis, ya no teneis excusa para presumir de vuestros periféricos Razer en nuestro sistema.


¿Os gustan este tipo de periféricos gamer? ¿Qué opinión os merece este proyecto? Danos tu opinión en los comentarios o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

