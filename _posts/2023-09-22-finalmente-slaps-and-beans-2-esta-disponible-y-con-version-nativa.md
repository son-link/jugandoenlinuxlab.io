---
author: leillo1975
category: "Acción"
date: 2023-09-22 16:57:00
excerpt: "Trinity Team vuelve a la carga con la segunda parte de este juego"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SlapsAndBeans/slaps_and_beans_2.webp
joomla_id: 1544
joomla_url: video-veloren-0-15
layout: post
tags:
- slaps-and-beans
- mmorpg
- veloren
- voxel
title: "Finalmente Slaps and Beans 2 esta disponible, ¡y con versión nativa!"
---

Los que lleváis más tiempo con nosotros seguramente recordáis que hace algunos años cubrimos ampliamente el lanzamiento **nativo** de **Slaps and Beans**, donde aparte de las [noticias]({{ "/tags/slaps-and-beans" | absolute_url }}) de lanzamiento, también realizamos un extenso [análisis]({% link _posts/2018-01-08-analisis-bud-spencer-terence-hill-slaps-and-beans.md %}) donde describíamos las muchas virtudes del juego, además de una interesante [entrevista]({% link _posts/2017-01-31-entrevista-con-trinity-team-bud-spencer-terence-hill-slaps-and-beans.md %}) a sus creadores. El juego llegaba de la mano de [Trinity Team](https://www.trinityteamgames.com), una pequeña **compañía italiana** que usaba **Unity3d** entre otras herramientas para desarrollar el juego. También años después nos [anunciaron que habría segunda parte]({% link _posts/2021-09-20-vuelven-las-tortas-bud-spencer-terence-hill-slaps-and-beans-2-estrena-campana-en-kickstarter.md %}), y finalmente estamos aquí para hablar de su lanzamiento que ha tenido lugar hoy mismo.

![festin](https://cdn.cloudflare.steamstatic.com/steam/apps/2142330/extras/binge.gif?t=1695382691)

Después de una exitosa campaña de [Kickstarter](https://www.kickstarter.com/projects/1684041218/bud-spencer-and-terence-hill-slaps-and-beans-2) donde finalmente **consiguieron alcanzar sus objetivos de financiación**, y donde además anunciaron que [habría versión nativa de Linux](https://www.kickstarter.com/projects/1684041218/bud-spencer-and-terence-hill-slaps-and-beans-2/posts/3306668), el desarrollo del juego ha terminado y ya podemos hacernos con él. Para quien no conozca el juego, se trata de un **beat'em up en 2D** al estilo **retro** que tiene como protagonistas a **Bud Spencer y Terence Hill**, los conocidos protagonistas de las míticas películas de mamporros de los 70/80 que tantos buenos momentos nos hicieron y hacen pasar aun a día de hoy.

![Bruce](https://cdn.cloudflare.steamstatic.com/steam/apps/2142330/extras/ninja.gif?t=1695382691)

El juego comienza donde quedó en la primera parte, como si de una película de ellos mismos se tratase, y recorre **multitud de conocidos escenarios** que todos hemos visto en sus películas, además de toparse con **nuevos enemigos basados en su filmografía**. El juego comienza donde quedó en la primera parte, como si de una película de ellos mismos se tratase, y recorre \*\*multitud de conocidos escenarios\*\* que todos hemos visto en sus películas, además de toparse con \*\*nuevos enemigos basados en su filmografía\*\*. Se ha puesto énfasis en un **sistema revisado y mejorado de combate** que nos permitirá repartir "obleas" por doquier. Además el modo **mutijugador** nos permitirá , a parte de jugar la aventura acompañados en el **modo cooperativo**, el poder disfrutar de los **minijuegos en modo party con hasta cuatro jugadores**. El juego además estará completamente traducido, y **DOBLADO**, a nuestro idioma, lo que en principio remarca un esfuerzo por parte de la compañía en ofrecer una localización adecuada. También nos aseguran que como mínimo habrá el doble de tortas y judías que en el juego anterior, :)))

![jalai](https://cdn.cloudflare.steamstatic.com/steam/apps/2142330/extras/pelota2.gif?t=1695382691)

Para ilustraros un poco de lo que encontrareis el juego, os dejamos con el trailer oficial:

<div class="resp-iframe">
	<iframe src="https://store.steampowered.com/widget/2142330/" width="646" height="190"></iframe>
</div>

Si quereis haceros con Slaps and Beans 2 podeis hacerlo en Steam:

<div class="steam-iframe">
	<iframe src="https://store.steampowered.com/widget/2142330/" width="646" height="190"></iframe>
</div>

Por cierto, si estais interesados en el primer juego ahora mismo está a un [precio irresistible con un descuento del **70% a 5.99€**](https://store.steampowered.com/app/564050/Bud_Spencer__Terence_Hill__Slaps_And_Beans/)