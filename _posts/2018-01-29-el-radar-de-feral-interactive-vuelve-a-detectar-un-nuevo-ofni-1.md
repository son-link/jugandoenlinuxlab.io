---
author: Pato
category: Noticia
date: 2018-01-29 16:55:30
excerpt: "<p>Un nuevo contacto en el radar nos anuncia la pr\xF3xima llegada de un\
  \ nuevo t\xEDtulo. Y van...</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/c7a3fcdf1f8fdd8c2e3654d9a5cbc6a0.webp
joomla_id: 621
joomla_url: el-radar-de-feral-interactive-vuelve-a-detectar-un-nuevo-ofni-1
layout: post
tags:
- proximamente
- feral-interactive
- feral
title: El radar de Feral Interactive vuelve a detectar un nuevo OFNI
---
Un nuevo contacto en el radar nos anuncia la próxima llegada de un nuevo título. Y van...

Buenas noticias para todos los que esperamos nuevos títulos. Aún no hemos podido desvelar qué título se encuentra [tras el último avistamiento](index.php/homepage/noticias/item/593-el-radar-de-feral-interactive-vuelve-a-detectar-un-nuevo-ofni) del que ya os informamos hace ya unos meses, cuando ahora el radar de Feral ha vuelto a detectar un nuevo OFNI (Objeto Feral No Identificado) lo que significa que un nuevo título AAA está en ciernes para nuestro sistema favorito.


Así nos lo ha anunciado Feral con el acostumbrado Tweet:



> 
> Incoming projectiles! Take cover!  
> Oh, false alarm. It’s just a pair of blips on the Feral Radar. One for macOS, the other for macOS and Linux. Nothing to see here, nope. Move along – <https://t.co/v2d998T2GE> [pic.twitter.com/FBhhtUCEy4](https://t.co/FBhhtUCEy4)
> 
> 
> — Feral Interactive (@feralgames) [January 29, 2018](https://twitter.com/feralgames/status/958015341615091713?ref_src=twsrc%5Etfw)



Como podéis ver, se trata de una imagen de una galleta o bizcocho aparentemente de chocolate lleno de agujeros, y la leyenda:


It's just a shot away. (Esta solo a un disparo) y en el cuerpo del mensaje bromean con la situación: "*¡llegan proyectiles! ¡a cubierto!"*


¿Alguna idea de qué título puede ser el que tengan entre manos los chicos de Feral Interactive?¿Qué título te gustaría que Feral nos trajese a GNU-Linux esta vez?


En Jugandoenlinux aceptamos apuestas  ya sea en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

