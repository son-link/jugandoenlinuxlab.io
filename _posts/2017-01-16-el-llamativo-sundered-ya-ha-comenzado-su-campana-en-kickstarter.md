---
author: Pato
category: "Acci\xF3n"
date: 2017-01-16 19:14:19
excerpt: "<p>Thunder Lotus, el estudio creador de Jotun est\xE1 detr\xE1s de este\
  \ juego de estilo \"metroidvania\"</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/8c4e7ddecfb739ef5c33c55621b27630.webp
joomla_id: 180
joomla_url: el-llamativo-sundered-ya-ha-comenzado-su-campana-en-kickstarter
layout: post
tags:
- accion
- indie
- plataformas
- kickstarter
title: "El llamativo 'Sundered' ya ha comenzado su campa\xF1a en Kickstarter"
---
Thunder Lotus, el estudio creador de Jotun está detrás de este juego de estilo "metroidvania"

Thunder Lotus [[web oficial](http://thunderlotusgames.com/)] consiguió hacerse un nombre con un título de la calidad del notable y galardonado 'Jotun' también disponible en Linux [[Steam](http://store.steampowered.com/app/323580)], y ahora nos anuncian el lanzamiento de su campaña de Kickstarter para su próximo desarrollo: Sundered



> 
> Sundered's Kickstarter is now LIVE! Pls check out and share the campaign : <https://t.co/GrrNZf4jJG>[#ResistOrEmbrace](https://twitter.com/hashtag/ResistOrEmbrace?src=hash) [#indiedev](https://twitter.com/hashtag/indiedev?src=hash) [#indiegame](https://twitter.com/hashtag/indiegame?src=hash) [pic.twitter.com/bRGdpW9SmW](https://t.co/bRGdpW9SmW)
> 
> 
> — ThunderLotus (@ThunderLotus) [16 de enero de 2017](https://twitter.com/ThunderLotus/status/821038692936642560)



 Se trata de un juego de tipo "Metroidvania" con un lema de lo mas descriptivo: "una terrorífica lucha por la supervivencia y la cordura". Juegas como Eshe, un mago en un mundo en ruinas atrapado en cavernas que cambian proceduralmente llenas de horrores. Tendrás que aprovechar tus poderes para derrotar a tus enemigos y grandes jefes finales a costa de tu propia humanidad.


Tratándose de este estudio el juego ya luce de manera notable con las características que ya destacaron en Jotun, gráficos hechos a mano y enemigos gigantescos:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/bgdtkaXN8_0" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


 'Sundered' está anunciado oficialmente para Linux tal y como puedes ver en la web de Thunder Lotus. Si estás interesado en participar en su campaña de Kickstarter puedes hacerlo en el siguiente enlace:


<div class="resp-iframe"><iframe height="420" src="https://www.kickstarter.com/projects/thunderlotus/sundered-a-horrifying-fight-for-survival-and-sanit/widget/card.html?v=2" width="220"></iframe></div>


 

