---
author: Pato
category: "Acci\xF3n"
date: 2018-05-23 18:11:07
excerpt: <p>Se trata de un desarrollo de Binx Interactive bajo el paraguas de "Croteam
  Incubator"</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/639af23831732d3cec36ca36df86cefd.webp
joomla_id: 747
joomla_url: i-hate-running-backwards-esta-cerca-de-llegar-a-linux-steamos
layout: post
tags:
- accion
- indie
- proximamente
title: "'I Hate Running Backwards' est\xE1 cerca de llegar a Linux/SteamOS"
---
Se trata de un desarrollo de Binx Interactive bajo el paraguas de "Croteam Incubator"

¿Te gustan los juegos de acción de Croteam? ¿Sam el Serio?  si es que si, sabrás que en la mayor parte de esta saga te la pasas pegando tiros y esquivando monstruos a diestro y siniestro... bueno, realmente "retrocediento" para que no te pillen. Pues esa es la premisa de este desarrollo de Binx Interactive llamado '**I Hate Running Backwards**' (odio correr hacia atrás) y que por si no se nota lo suficiente, viene avalado por la propia Croteam bajo el sello "**Croteam Incubator**" con el que dan cobijo a estudios indie para que desarrollen juegos con su propia "identidad". En I Hate Running Backwards tendremos que enfrentarnos a hordas de enemigos que nos irán "persiguiendo" mientras nosotros acabamos con ellos al más puro estilo "Serious Sam"... y corremos hacia atrás, como es normal:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/dZ9shCxz_Cw" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


***I Hate Running Backwards** es un juego tipo shoot'em down que se enfoca no solo en las hordas sin cesar de enemigos, si no que ademas se enfoca en la destruccion de mundos que siempre son generados al azar. Elige a tu personaje y destruye a tus enemigos con un poderoso arsenal de armas a tu alcance: Escopetas, rifles laser, lanzacohetes, cañones de hoyos negros y mas. Consigue puntos de experiencia y desbloquea habilidades para derrotar no solo a enemigos sino a jefes que se encuentren a tu paso.*


El juego en sí ya está disponible para sistemas Windows, pero el estudio ya ha confirmado a preguntas de Liam de gamingonlinux en su [foro del juego](https://steamcommunity.com/app/575820/discussions/0/1693795812312784422/#c1693795812312797440) que llegará en breve a Linux/SteamOS.


Si quieres saber más detalles, puedes visitar su [página de Steam](https://store.steampowered.com/app/575820/I_Hate_Running_Backwards/) donde nos llegará con menús en español, o en la [página oficial](http://www.croteam.com/) de Croteam.


¿Te gustan los juegos de acción del tío Sam? ¿Que te parecen los juegos de Croteam? Desde luego se merecen un monumento por el apoyo a nuestro sistema favorito.


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

