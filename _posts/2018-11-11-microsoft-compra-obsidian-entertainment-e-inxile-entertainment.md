---
author: Serjor
category: Noticia
date: 2018-11-11 08:50:24
excerpt: "<p>Microsoft Studios fortalece su cat\xE1logo con compa\xF1\xEDas que dan\
  \ soporte nativo a GNU/Linux</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/96bf577e2682bc7925dad067c2022909.webp
joomla_id: 898
joomla_url: microsoft-compra-obsidian-entertainment-e-inxile-entertainment
layout: post
tags:
- obsidian-entertainment
- microsoft
- inxile-entertainment
- compra
title: Microsoft compra Obsidian Entertainment e inXile Entertainment
---
Microsoft Studios fortalece su catálogo con compañías que dan soporte nativo a GNU/Linux

Nos enteramos vía [Matrix](https://matrix.to/#/!ZVxLHXiEvpOkFWIllT:swedneck.xyz/$1541902115202456uGfAc:swedneck.xyz) de que Microsoft [ha comprado](https://gematsu.com/2018/11/microsoft-acquires-obsidian-entertainment-and-inxile-entertainment) Obsidian e inXile. Esto puede no significar nada para los jugadores del sistema del pingüino, porque recordemos que Dust: An Elysian Tail pertenece a Microsoft Studios y lo tenemos de manera nativa, pero por mucho que Microsoft predique que GNU/Linux es lo más, ya sabemos cómo se las gastan los de Redmond, y por ejemplo We Happy Few, que fue anunciado con soporte nativo para GNU/Linux, aún no está disponible...


Recordemos que juntando los juegos de ambas compañías tenemos de manera nativa títulos como Pillars of Eternity (I y II), Wasteland 2, Torment: Tides of Numenera o Tyranny entre otros, así que esperemos que sigan dándoles soporte y nos sigan trayendo sus futuros títulos.


 


Desde Obsidian por ejemplo, los propios empleados ya han opinado qué les parece esta adquisición:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/Suvxb0OfhpQ" width="560"></iframe></div>


Y tú, ¿qué piensas de estas adquisiciones? Cuéntanoslo en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD)


Por cierto, aprovechamos para recordaros de que tenemos una comunidad [no oficial en Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org), la cuál está unida mediante puentes con los canales de Discord, con lo que escribáis desde un servicio aparecerá en el otro.

