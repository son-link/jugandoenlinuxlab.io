---
author: Pato
category: Terror
date: 2017-10-13 15:59:50
excerpt: "<p>Terror y acci\xF3n en un pueblo maldito</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/2964cd8751427f56b2f06582b92bec36.webp
joomla_id: 485
joomla_url: hegis-grasp-llega-hoy-a-linux-steamos-dispuesto-a-atormentarte
layout: post
tags:
- accion
- indie
- aventura
- terror
- gore
title: '''Hegis'' Grasp'' llega hoy a Linux/SteamOS dispuesto a atormentarte'
---
Terror y acción en un pueblo maldito

Estamos próximos a las fechas de Halloween, y no está de más tener a mano un juego de temática adecuada. Así pués, hoy nos llega este 'Hegis' Grasp' para hacer las delicias de aquellos que prefieren jugar "pasando un mal rato".


'Hegis' Grasp' se basa en la historia narrada en un "e-book" (que tendremos disponible) donde encarnaremos a un periodista llamado Henry Wood que tras leer en los periódicos la terrible historia de un pueblo aparentemente apacible se decide a ir a investigar qué ocurrió junto con dos compañeros de viaje. Pronto se dará cuenta de que ha cometido un error y ha puesto su vida y la de sus compañeros en grave peligro.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/snuEaoFCIdA" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Como puntos a destacar, el juego tiene varios finales aunque solo dos se ajustan a la historia del libro. También se puede cambiar la perspectiva de primera a tercera persona, tiene ciclo día-noche, tiempo dinámico y diversos modos de juego.


Los requisitos recomendados son:


+ **SO:** Ubuntu 16.04 64-bit
+ **Procesador:** Intel i5 Dual-Core CPU 2.6 GHz o equivalente AMD CPU
+ **Memoria:** 8 GB de RAM
+ **Gráficos:** Nvidia GTX 730
+ **Almacenamiento:** 5 GB de espacio disponible


'Hegis' Grasp' no está disponible en español, pero si te va la temática y no es problema, lo tienes disponible en su página de Steam con un 10% de oferta de lanzamiento:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/644380/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


¿Piensas darte un paseo turístico por el pueblo de Hegis? 


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

