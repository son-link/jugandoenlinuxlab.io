---
author: leillo1975
category: "Acci\xF3n"
date: 2019-03-07 14:19:42
excerpt: "<p dir=\"ltr\"><span dir=\"ltr\">El juego de <span dir=\"ltr\">@KillPixelGames\
  \ ,</span> @3DRealms </span><span dir=\"ltr\">y </span><span dir=\"ltr\">@1C_Company\
  \ tiene nuevo contenido y fecha para el final del Acceso Anticipado.<br /></span></p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/2b6800ed3449797f0570e3909a6130fd.webp
joomla_id: 990
joomla_url: wrath-aeon-of-ruin-un-nuevo-fps-retro-llegara-a-linux
layout: post
tags:
- retro
- 3d-realms
- quake
- kill-pixel-games
- 1c-entertaiment
- wrath-aeon-of-ruin
title: "WRATH: Aeon of Ruin, un nuevo FPS Retro, llegar\xE1 a Linux (ACTUALIZACI\xD3\
  N 3) "
---
El juego de @KillPixelGames , @3DRealms y @1C_Company tiene nuevo contenido y fecha para el final del Acceso Anticipado.  



**ACTUALIZACIÓN 4-6-20**: La noticia tiene realmente una semana, pero al final se quedó en el tintero y, mea culpa, hasta el día de hoy no se me volvió a acordar escribir sobre ella. El caso es que el juego ha adquirido su segundo paquete de contenido, y también, y no menos importante, una **fecha para la salida del juego del periodo de Acceso Anticipado**, concretamente el **25 de Febrero de 2021** ,fecha en la que en teoría, los usuarios de Linux tendríamos que enmarcar para recibir la versión para nuestro sistema.


Además, los pedidos previos para reservar la "**Founder’s Edition Big Box**" acaban de empezar, por lo que si quereis haceros con una de las 500 copias exclusivas del juego tendreis que ser rápidos, pues quedan poco más de 100 unidades. Podeis reservarlas en la [página de pedidos](http://www.wrath.game/), y en ellas vais a encontrar lo siguiente:


*- Edición limitada de la Caja Grande de los Fundadores*  
*- "Wrath: Aeon of Ruin" en CD-ROM*  
*- Libro de Arte "The Art of Wrath"*  
*- Banda sonora original de Andrew Hulshult*  
*- Una unidad USB cargada con contenido digital exclusivo*  
*- 5 Figuras de enemigos de metal*  
*- Cartel reversible*  
*- Pegatinas exclusivas*


...Y hablando del **nuevo contenido:**


*-Añadido nuevo nivel "El Priorato"*  
*-Añadió un nuevo enemigo "El Opresor"*  
*-Añadida nueva arma "El cañón de la escoria"*  
*-Añadido nuevo item "El Signo del Vuelo"*  
*-Añadida la versión SDL2 del cliente del juego*  
*-Añadió un sistema de daño acumulativo a los estados de dolor del enemigo*  
*-Se ha añadido la opción de flash de pantalla al recoger los artículos*  
*-Se han añadido opciones de actualización*  
*-Añadida la opción "Always Run".*  
*-Añadida la opción "Toggle Run".*


También se han realizado **numerosas correcciones de errores**, **correcciones en el balance de enemigos y armas**, y otros cambios variados. Podeis consultar la lista completa en las notas del [anuncio que publicaron en Steam](https://store.steampowered.com/newshub/app/1000410/view/2208399306478623300), y como es habitual os dejamos con un video del trailer de este segundo paquete de contenido y la "Big Box":


  
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/oouU7zRop70" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


Estamos seguros que vosotros tampoco podeis aguantar más para echarle el guante a este juego, pero nos queda como consuelo que ya queda mucho menos para jugarlo en nuestro sistema.




---


**ACTUALIZACIÓN 27-2-20**:Hace poco nos ha llegado,  información nueva sobre este juego por parte de su departamento de prensa ([StridePR](https://www.stridepr.com/)) . Y es que permaneciendo en Acceso Anticiapado, hace nada ha recibido una actualización que añade bastantes [novedades](https://steamcommunity.com/app/1000410/eventcomments/1746772002633433510) muy interesantes al juego entre las que destacan:


-**Un nuevo mapa para explorar**, los **Jardines**, un lugar donde la vegetación está ahora podrida y encontraremos criaturas empeñadas en acabar con cualquiera que se atreva a entrar en su nuevo habitat.


-**Un nuevo enemigo**, el **Desdichado**,  y se trata de una rápida y musculosa criatura que utiliza un enorme cañón de energía cosido en el centro de su cuerpo. Podremos encontrarla en el nuevo nivel.


-Un **nuevo y poderoso artefacto**: **Attar de confusión**, un pequeño frasco de aceite desconcertante capaz de enfrentar a los enemigos entre sí, que nos permitirá aprovechar la situación.


-**Encuentros con el enemigo actualizados** en The Undercrofts y The Mire


-Soporte para la **resolución 4096x2160**


Según Frederik Schreiber, vicepresidente de 3D Realms y productor del juego, "WRATH: La primera actualización de Aeon of Ruin no es más que una muestra de lo que vendrá", "Es importante para nosotros sentir que cada actualización es un evento importante, introduciendo nuevos secretos para el satisfactorio juego de armas cinéticas de KillPixel".


Recordad que el juego durante el Acceso Anticipado no tendrá versión para nuestro sistema, la cual estará disponible según sus desarrolladores en el lanzamiento.


 




---


**ACTUALIZACIÓN 22-11-19**: Desde Dinamarca nos llegan noticias frescas de este shooter en primera persona elaborado con el motor del Quake 1. **Esta misma tarde a las 19:00 estará disponible en "Early Access"**. Durante este periodo el juego seguirá recibiendo actualizaciones de contenido que añaden aún más niveles, enemigos, armas, artefactos y **música del compositor de Quake Champions Andrew Hulshult**. En este lanzamiento **podremos jugar el primero de los 3 mundos** no lineales. Podreis comprar este juego por 20.99€ en Steam con un descuento del 5%, y un 10% si lo compramos conjuntamente con [Ion Fury](index.php/component/k2/5-accion/1221-lanzado-oficialmente-el-fps-retro-ion-fury). Os dejamos con el trailer del lanzamiento en Acceso Anticipado:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/eJPK1Tn7tTQ" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>




---


**NOTICIA ORIGINAL:** 


Si recordais, hace más o menos un año, os hablábamos de [ION Maiden](index.php/homepage/generos/accion/5-accion/786-3d-realms-vuelve-ion-maiden-llega-a-gnu-linux-steamos-en-acceso-anticipado), un juego de acción en primera persona retro desarrollado con el mismo motor que joyas como Duke Nukem o Shadow Warrior. Con ello la compañía [3D Realms](https://3drealms.com/) se ponía de nuevo en la brecha con un juego al estilo de los que en su día le dieron la fama. Hoy nos enteramos gracias a [GoL](https://www.gamingonlinux.com/articles/wrath-aeon-of-ruin-is-the-new-fps-from-3d-realms-coming-to-linux-this-summer.13710), que este verano nos traerán a nuestros escritorios "[WRATH: Aeon of Ruin](http://www.wrath.game/)", pero en este caso usando otro conocidisimo motor, el del incomensurable **Quake 1**, que también dió combustible a iconos como el primer **Half Life** o a **Hexen II**. Gracias a esta característica, el juego contará con la **capacidad de ser facilmente modificable**, además se ha anunciado que **se facilitarán las herramientas para la creación de contenido** compatible con el juego.


Su desarrollo corre a cargo de [Killpixel Games](http://killpixelgames.com/) y la producción pertenece a [1C Entertaiment](http://1ce.games/en/). En "Wrath: Aeon of Ruin" encontraremos según su descripción oficial:  
  




*![](https://steamcdn-a.akamaihd.net/steam/apps/1000410/extras/About.png)*  
  
*No eres de este mundo. Naufragaste en un mar más antiguo que el tiempo y fuiste a parar a las orillas de un mundo que agoniza. De la oscuridad que todo lo consume surge una figura vestida de blanco, una guía para las almas perdidas, que te encomienda la tarea de dar caza a los guardianes del viejo mundo. Deberás aventurarte en este mundo en penumbra, explorar ruinas antiguas, desvelar secretos olvidados y enfrentarte a los horrores que acechan.*  
  
*![](https://steamcdn-a.akamaihd.net/steam/apps/1000410/extras/Steam_Gif1.gif)*  
  
*WRATH está inspirado en la tecnología de Quake 1 y cuenta con la esencia de los juegos de disparos más populares de los años 90. WRATH utiliza elementos atemporales de títulos clásicos como DOOM, QUAKE, DUKE NUKEM 3D, BLOOD, UNREAL y HEXEN y los adapta al siglo XXI.*  
  
*![](https://steamcdn-a.akamaihd.net/steam/apps/1000410/extras/Steam_Gif2.gif)*  
  
*Equípate con un arsenal de 9 poderosas armas y un inventario de 10 artefactos místicos y atraviesa criptas antiguas, ruinas sumergidas, templos corruptos y bosques vivientes para dar muerte a tus adversarios. Pero nunca los subestimes, pues te superan en fuerza y en número. Deberás poner a punto tanto tu espada como tu mente si quieres sobrevivir a los peligros que te esperan.*  
  
*![](https://steamcdn-a.akamaihd.net/steam/apps/1000410/extras/Highlights.png)*  
  
*En el mundo de WRATH te esperan combates fluidos, escenarios de lo más diversos y una historia que te hipnotizará. Todos los elementos del mundo están conectados para crear una experiencia auténtica y tan atemporal como los juegos en los que está inspirado.*  
  
*![](https://steamcdn-a.akamaihd.net/steam/apps/1000410/extras/Steam_Gif3.gif)*  
  
*• **Explora** un vasto mundo sumido en la oscuridad creado por los nigromantes de Quake.*  
  
*• Viaja a los 3 núcleos y a los 15 niveles gigantes plagados de enemigos, historias y secretos antiguos.*  
  
*• Forja tu camino con un arsenal de **9 armas letales** con distintos modos de disparo.*  
  
*• **Conoce a tu enemigo**. En las sombras se ocultan horrores de todo tipo sedientos de tu sangre.*  
  
*• Encuentra los **artefactos** ocultos en los oscuros confines del mundo y somete a tus enemigos con su inmenso poder.*  
  
*• Explota al máximo la tecnología del legendario Quake 1, que hace de WRATH un auténtico FPS clásico en todos los sentidos.*  
  
*• Crea tus propios mundos. WRATH permite crear mods con facilidad. Construye tus propios niveles, armas y personajes. Las herramientas que se han usado para crear WRATH estarán a disposición de todo el mundo desde el primer día.*  
  
*• Reta a tus amigos en los modos multijugador con la afamada tecnología de QuakeWorld.*  
*¡Domina el mundo de WRATH con otras tres personas en el modo cooperativo para cuatro, ya sea online o mediante LAN!*  
  
*• Déjate hechizar por el sonido ambiental de las retorcidas mentes de **Andrew Hulshult** (Quake Champions, Rise of the Triad, Dusk y Amid Evil) y **Bjorn Jacobson** (CyberPunk 2077, Hitman, EVE Online).*  
  
*![](https://steamcdn-a.akamaihd.net/steam/apps/1000410/extras/Steam_Gif4.gif)*
 
No se conocen los requisitos técnicos necesarios para jugarlo en Linux/SteamOS, pero viendo las exigencias del motor de Quake 1 y lo que se necesita para jugarlo en Windows, está claro que funcionará en casi cualquier ordenador. El juego podrá ser adquirido en la [tienda de Steam](https://store.steampowered.com/app/1000410/WRATH_Aeon_of_Ruin/), donde si os gusta, ya **podeis ir añadiéndolo a vuestra lista de deseados**  para que sus desarrolladores sepan que sois Linuxeros. Os dejamos con el Trailer del anuncio del juego:
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/CgrLN9Wl4xU" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>
 


¿Qué os parece Wrath: Aeon of Ruin"? ¿Os gustan los FPS Retro? Danos tu opinión en los comentarios de este artículo o charla con nosotros sobre el juego en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

