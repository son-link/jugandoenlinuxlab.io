---
author: Pato
category: Aventuras
date: 2017-03-09 20:47:44
excerpt: "<p>Tendremos que sobrevivir en un mundo postapocal\xEDptico donde el canibalismo\
  \ estar\xE1 a la orden del d\xEDa</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/3dcbd8056f504532259e733a4b9e2ae3.webp
joomla_id: 241
joomla_url: lambs-on-the-road-un-juego-sobre-los-horrores-de-un-mundo-apocaliptico-esta-en-campana-en-greenlight
layout: post
tags:
- accion
- aventura
- horror
- puzzles
- arcade
title: "'Lambs on the Road' un juego sobre los horrores de un mundo apocal\xEDptico\
  \ est\xE1 en campa\xF1a en Greenlight"
---
Tendremos que sobrevivir en un mundo postapocalíptico donde el canibalismo estará a la orden del día

'Lambs on the road' promete ser uno de esos juegos que no te dejan indiferente. La propuesta de Flynn's Studios [[web oficial](http://www.flynnsarcades.com/?portfolio=lambs-on-the-road)] se basa en el best seller "La carretera" (The road) donde el cambio climático ha cambiado la faz de la Tierra, y la falta de alimentos ha empujado a la humanidad a recurrir a la ley del mas fuerte y al canibalismo.


Tendremos que vivir la historia de un padre y su hija que tendrán que sobrevivir en este juego, mezcla de plataformas y acción mezclados con pequeños puzzles:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/59jZUg4x-SM" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


Cada personaje tendrá distintas habilidades, y tendremos que manejarlos por separado hasta que lleguen a colaborar una vez avance el juego.


Lambs on the Road promete versión en Linux y traducción al español, tal y como anunciaron en su [web de Greenlight](http://steamcommunity.com/sharedfiles/filedetails/updates/863758996/1487184345).


Si te gusta la propuesta de 'Lambs on the Road' puedes apoyarlo en Greenlight [en este enlace](http://steamcommunity.com/sharedfiles/filedetails/?id=863758996).


¿Qué te parece 'Lambs on the Road? ¿Piensas apoyarlo?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux en [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

