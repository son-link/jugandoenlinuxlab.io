---
author: Pato
category: Software
date: 2022-02-23 08:39:10
excerpt: "<p>Valve lanza su propia herramienta para que puedas ver si tu biblioteca\
  \ de juegos es compatible</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Steamdeck/Steamdeckhands.webp
joomla_id: 1432
joomla_url: ya-puedes-verificar-la-compatibilidad-de-tus-juegos-con-steam-deck-de-forma-oficial-en-steam
layout: post
title: Ya puedes verificar la compatibilidad de tus juegos con Steam Deck de forma
  oficial en Steam
---
Valve lanza su propia herramienta para que puedas ver si tu biblioteca de juegos es compatible


A tan solo tres días del lanzamiento de Steam Deck, Valve va a marchas forzadas puliendo todo lo relacionado con su máquina.


En este sentido, acaban de lanzar una "herramienta" dentro de Steam para que puedas verificar si tu biblioteca de juegos es compatible con Steam Deck, y en qué grados tus juegos podrán ejecutarse en la máquina de Valve.



> 
> Solo una nota rápida antes de que nos volvamos a preparar para el lanzamiento (quedan tres días!): acabamos de lanzar una nueva herramienta en Steam Store para que verifique las clasificaciones de compatibilidad de Steam Deck de todos los juegos que ya posees. Esperamos que la página se explique por sí misma: inicia sesión con tu cuenta de Steam y te mostraremos la calificación de compatibilidad de cada título en tu biblioteca y qué tipo de experiencia puedes esperar tener si los ejecutas en Deck hoy.
> 
> 
> 


[Puedes ver esta nueva página aquí](https://store.steampowered.com/steamdeck/mygames).


Además, nos recuerdan que **el proceso de verificación es un proceso que está en evolución constante, y que continuará incluso después del lanzamiento de Steam Deck**, verificando nuevos títulos contínuamente. De hecho nos recuerdan que "*el hecho de que no se haya probado un juego no significa que no funcione bien en Steam Deck. Puedes instalar cualquier título de tu biblioteca, independientemente de su calificación de compatibilidad"*.


Si quieres saber en qué consiste el proceso de verificación, puedes visitar [este enlace](https://www.steamdeck.com/es/verified).


Por otra parte, también han configurado una opción para que podamos ver la página de inicio de la tienda con los títulos verificados y la apariencia tal y como se podrá ver en la Steam Deck, (como ellos mismos reconocen, ya se había descubierto la forma de verla de esta forma mediante user agent, así que no tenía sentido seguir escondiéndola) y nos animan a hacerles saber qué nos parece.


Puedes ver el anuncio [en este post de Steam](https://store.steampowered.com/news/app/1675180/view/3113679892632291880).

