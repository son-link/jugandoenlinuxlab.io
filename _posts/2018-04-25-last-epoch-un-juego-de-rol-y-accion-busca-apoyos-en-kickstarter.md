---
author: Pato
category: Rol
date: 2018-04-25 12:15:18
excerpt: "<p>De clara inspiraci\xF3n en juegos como la saga Diablo, tiene ya una demo\
  \ para Linux</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/07da00e383d0881ed3ca8a8acbeba107.webp
joomla_id: 719
joomla_url: last-epoch-un-juego-de-rol-y-accion-busca-apoyos-en-kickstarter
layout: post
tags:
- rol
- crowdfunding
- kickstarter
title: "'Last Epoch' un juego de rol y acci\xF3n busca apoyos en Kickstarter"
---
De clara inspiración en juegos como la saga Diablo, tiene ya una demo para Linux

Un nuevo proyecto está llamando nuestra atención. Gracias a [gamingonlinux](https://www.gamingonlinux.com/articles/the-action-rpg-last-epoch-thats-currently-crowdfunding-has-a-working-linux-demo-now.11653) nos enteramos de que 'Last Epoch', un juego de rol y acción con claras referencias en juegos de estilo "Diablo" está en campaña en Kickstarter, y aparte de tener una pinta de lo más atractiva y prometer una versión para Linux, acaban de lanzar incluso una demo jugable de la versión pre-alfa para nuestro sistema favorito.


*Last Epoch es un juego de rol y acción basado en loot, que combina viajes en el tiempo, configuración profunda de personajes, búsquedas de objetos y un sistema de items que garantiza una rejugabilidad infinita.*


<div class="resp-iframe"><iframe height="360" src="https://www.kickstarter.com/projects/lastepoch/last-epoch/widget/video.html" style="display: block; margin-left: auto; margin-right: auto;" width="640"> </iframe></div>


 Características principales:


* Acción multijugador offline y online
* Viaje en el tiempo y un mundo de gran fantasía
* 5 clases básicas que avanzan hacia 10 poderosas clases maestras
* Un sistema de habilidades profundo con árboles que aumentan para cada habilidad
* Sistema creciende de loot y crafting
* Contenido final y rejugabilidad masivas, comercio, PvP, temporadas y logros
* Narrativa épica, caracteres y entornos memorables, viajes, y secretos ocultos


Respecto a la campaña, el estudio tiene como primer objetivo algo más de 171.800$, y a falta aún de 21 días ya llevan recaudados unos 51.000$. Si nos animamos a aportar, **la opción más económica es de 10$** (**unos 8€**) lo que da derecho a una copia digital del juego, que tiene su salida prevista para el 2020 a un precio estimado de 14,99$, con lo que ahorramos dinero.


Si te gusta lo que ves, puedes incluso probarlo pues como hemos dicho, ya **hay posibilidad de descargar una demo** de la versión  pre-alfa del juego para nuestro sistema favorito. Tan solo tienes que visitar la página de su campaña en Kickstarter, [en este enlace](https://www.kickstarter.com/projects/lastepoch/last-epoch/) y descargarla.


¿Que te parece este 'Last Epoch'? ¿Has probado la demo? ¿Piensas aportar en la campaña?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 

