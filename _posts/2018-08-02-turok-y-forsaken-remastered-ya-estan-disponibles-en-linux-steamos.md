---
author: Pato
category: "Acci\xF3n"
date: 2018-08-02 17:30:35
excerpt: "<p>@NightdiveStudio nos sigue trayendo sus t\xEDtulos gracias a \" @icculus\
  \ \" Ryan Gordon</p>\r\n<h2 class=\"ProfileHeaderCard-screenname u-inlineBlock u-dir\"\
  \ dir=\"ltr\">&nbsp;</h2>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/ff9d018aae83fbc06b5feba5a75d8897.webp
joomla_id: 815
joomla_url: turok-y-forsaken-remastered-ya-estan-disponibles-en-linux-steamos
layout: post
tags:
- accion
- 3d
title: "'Turok' y 'Forsaken Remastered' ya est\xE1n disponibles en Linux/SteamOS"
---
@NightdiveStudio nos sigue trayendo sus títulos gracias a " @icculus " Ryan Gordon


Tenemos otro estudio que está ganándose la medalla de "Linux Friendly", y este es **Nightdive Studios**. El estudio está haciendo una labor encomiable resucitando viejas glorias y adaptándolas en la medida de los posible a los tiempos que corren. Hoy nos hacemos eco de la llegada de dos de sus títulos a nuestro sistema favorito: Turok y Forsaken Remastered.


**Turok** es un clásico de los juegos de acción, donde nos tendremos que enfrentar a dinosaurios, enemigos y toda clase de monstruos en un mundo hostil donde tendremos que realizar distintas acciones para poder sobrevivir.


*¡Turok ha vuelto y ningún dinosaurio está a salvo! Cuando se lanzó por primera vez en 1997, Turok nos presentó un mundo lleno de astutos enemigos, trampas, rompecabezas y armas mortales, todo dentro de un vasto entorno 3D listo para explorar. ¡Ahora el juego clásico ha sido restaurado y mejorado con un nuevo motor visual y nuevas características emocionantes para hundirle los dientes!*


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/0vTxsv3oK_s" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Tienes disponible Turok en su [página de Steam](https://store.steampowered.com/app/405820/Turok/).


**'Forsaken Remastered'** es un título de acción en 3d en primera persona donde tendremos que luchar contra hordas de naves en un futuro distóptico donde la tecnología está fuera de control.


*Forsaken es un shooter 3D en primera persona en el que el jugador tiene un rango completo de libertad de 360 grados. En un futuro lejano, juegas como un carroñero buscando en los túneles subterráneos armas y tesoros. Pero te enfrentas a los peligros de los sistemas de defensa automatizados y a otros carroñeros.  
Mientras montas tu hoverbike, prepárate para un combate mortal. Vas a aprender rápidamente que el futuro es ... ¡Forsaken!*


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/OfDSXz1HTYU" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Tienes disponible 'Forsaken Remastered' en su [página de Steam](https://store.steampowered.com/app/668980/Forsaken_Remastered/) con un 10% de descuento hasta el día 7 de Agosto.


Por otra parte, es preciso comentar que tras estos ports se encuentra un nombre ya conocido por todos como **Ryan C. Gordon**. Esperemos que Nightdive Studios le permitan seguir trayéndonos todos sus juegos a nuestro sistema favorito.


¿Te gustan los juegos de Nightdive Studios? ¿Juegas a Turok o Forsaken Remastered?


Cuentamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

