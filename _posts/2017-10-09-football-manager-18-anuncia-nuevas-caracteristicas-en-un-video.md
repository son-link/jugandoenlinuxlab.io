---
author: Pato
category: Estrategia
date: 2017-10-09 15:42:32
excerpt: "<p>La nueva entrega de esta veterana saga nos llegar\xE1 el pr\xF3ximo d\xED\
  a 10 de Noviembre</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/d88d434cd5d84c4c2d40d1b2675949da.webp
joomla_id: 476
joomla_url: football-manager-18-anuncia-nuevas-caracteristicas-en-un-video
layout: post
tags:
- proximamente
- simulador
- estrategia
title: "'Football Manager 18' anuncia nuevas caracter\xEDsticas en un v\xEDdeo"
---
La nueva entrega de esta veterana saga nos llegará el próximo día 10 de Noviembre

Estamos ya casi a las puertas de recibir una nueva entrega de Football Manager 18  y los desarrolladores han desvelado algunas de las nuevas características que estarán presentes en su próximo lanzamiento.


En concreto, en forma de parodia hacen un noticiario donde anuncian mejoras en la IA, nuevas opciones tácticas y un nuevo apartado gráfico para los partidos. Además, se ha rehecho el sistema de ojeador para hacerlo mas cercano a como los clubes lo hacen en realidad. A continuación el vídeo donde se puede ver el juego en acción:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/F_1gPUeY0EY" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 El equipo de desarrollo ya ha dicho que irán desvelando nuevos vídeos con nuevos anuncios sobre las características que están por llegar.



> 
> *Football Manager 2018 es la versión más reciente de esta increíble serie superventas. Podrás entrenar a cualquier club de fútbol en más de 50 países de todo el mundo, por lo que Football Manager 2018 es lo más parecido a dirigir un equipo en el mundo real.*
> 
> 
> 


 Puedes pre-comprar Football Manager 18 en su página de Steam donde llegará traducido al español:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/624090/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 ¿Qué te parece Football Manager 18? ¿Eres entrenador o te va mas manejar el club desde el despacho?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux en [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 

