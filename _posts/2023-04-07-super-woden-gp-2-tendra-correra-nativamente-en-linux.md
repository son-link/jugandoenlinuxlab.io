---
author: leillo1975
category: Carreras
date: 2023-04-07 21:19:27
excerpt: "<p>Su creador, <span class=\"css-901oao css-16my406 r-poiln3 r-bcqeeo r-qvutc0\"\
  >@ViJuDaGD </span>, acaba de lanzar adem\xE1s una demo del juego con concurso incluido.</p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SuperWodenGP2/SWGP2.webp
joomla_id: 1532
joomla_url: super-woden-gp-2-tendra-correra-nativamente-en-linux
layout: post
tags:
- carreras
- nativos
- vijuda
- super-woden-gp
- super-woden-gp-2
title: "Super Woden GP 2 correr\xE1 nativamente en Linux. "
---
Su creador, @ViJuDaGD , acaba de lanzar además una demo del juego con concurso incluido.


 Hace año y medio publicamos un [análisis de Super Woden GP]({{ "/posts/analisis-super-woden-gp" | absolute_url }}), y en él ensalzábamos las numerosas cualidades de este título que nos encandiló en cuanto pusimos nuestras manos en el gamepad para jugarlo. Por suerte para nosotros, eso ocurrió muchos meses antes de su lanzamiento, ya que en JugandoEnLinux.com tuvimos el honor de participar en su desarrollo como **betatesters de la versión Linuxera**. Al igual que en esa ocasión, Víctor, su creador ha confiado una vez más en nosotros para esa tarea y os podemos asegurar que su trabajo no os va a dejar indiferentes.


En esta nueva versión encontrareis un **apartado visual mucho más trabajado, donde el 3D toma protagonismo**, no solo por una mayor calidad gráfica, sino por una iluminación más compleja. diferentes **elevaciones del trazado**, o por las reacciones de los coches a estas (lo cual demuestra que las **físicas renovadas** toman un papel relevante). Por ejemplo, podemos ver **espectaculares saltos**, accidentes entre coches mucho más realistas, perdidas de adherencia en los cambios de rasante... Otra cosa que se percibe es una **IA más "cabrona"** que te bloquea el paso en los adelantamientos cambiando la trazada.


![SWGP2](https://cdn.cloudflare.steamstatic.com/steam/apps/2083210/ss_90e908704c2470de12e4fcbe73500b4b60b14bf0.1920x1080.jpg)


Se nota también **mucho esfuerzo en los menúes del juego**, donde al igual que en la anterior ocasión impera un **mapa al estilo Gran Turismo** como menú principal, pero con muchos más apartados, encontrando más marcas que en el anterior (con una cantidad de nuevos coches asombrosa), o con un **divertidísimo modo Arcade** al más puro estilo World Rally Championship, incluso un **nuevo taller** donde además de lavar y pintar nuestro coche, podemos realizarle **mantenimientos para que no pierda rendimiento** (y quizás en el futuro alguna cosilla más).


Para ir abriendo boca, hoy mismo se ha lanzado una **demo del modo arcade** del juego donde podréis comprobar por vosotros mismos algunas cosas de las que os hablo en una ruta de 8 etapas, y además con la **posibilidad de ganaros una copia del juego si conseguís estar entre los 5 mejores tiempos**. Teneis más info en este tweet de la cuenta oficial de ViJuDa:

>
> Demo Contest!
> This Friday the 7th, the -Arcade Demo- will be available, representing a small part of this game mode. As in SWGP1, the top 5 best times will get a free copy of the game when it is released. Conditions in the next tweet!
> Please, RT 😁<https://t.co/sKgSfuC6g7>> [pic.twitter.com/XyNF7dX59u](https://t.co/XyNF7dX59u)
>
>
> — ViJuDa (@ViJuDaGD) [April 5, 2023](https://twitter.com/ViJuDaGD/status/1643713702695149569?ref_src=twsrc%5Etfw)



El juego se espera para **algún momento entre el final del verano y el principio del otoño**. Nosotros desde JugandoEnLinux.com seguiremos, como es lógico el desarrollo de este título e os iremos informando a medida que tengamos más cosas que contaros. Es muy probable también que publiquemos algún gameplay de una versión previa muy pronto, por lo que os recomendamos que permanezcais atentos a nuestras redes sociales en [Mastodon](https://mastodon.social/@jugandoenlinux) y [Twitter](https://mobile.twitter.com/jugandoenlinux). Si quereis podeis ir poniendo en deseados el juego pinchando en el botón del Widget de Steam:

<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/2083210/" width="646"></iframe></div>
