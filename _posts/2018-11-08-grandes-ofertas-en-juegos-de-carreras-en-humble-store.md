---
author: leillo1975
category: Ofertas
date: 2018-11-08 19:28:06
excerpt: <p>@humble nos trae el "Festival Of Speed MK2"</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/1100929ce10c721cac00963df69d3ca3.webp
joomla_id: 892
joomla_url: grandes-ofertas-en-juegos-de-carreras-en-humble-store
layout: post
tags:
- carreras
- dirt-rally
- ofertas
- humble-store
- f1-2017
- grid-autosport
title: Grandes Ofertas en Juegos de Carreras en Humble Store.
---
@humble nos trae el "Festival Of Speed MK2"

Gracias a **@josegp26** de nuestro grupo de [Telegram](https://t.me/jugandoenlinux) nos acabamos de enterar que la conocida tienda de Humble Bundle **tiene en promoción gran parte de su catálogo de juegos de carreras** en lo que ellos llaman "[Festival of Speed MK2](https://www.humblebundle.com/store/promo/festival-of-speed-mk-2/?partner=jugandoenlinux)". Entre estas ofertas hay un buen puñado de juegos con soporte para nuestro sistema que si aun no están en vuestra biblioteca, teneis una buena oportunidad de haceros con ellos por muy poco dinero. Entre ellos podeis encontrar:



> 
> - [DIRT Rally](https://www.humblebundle.com/store/dirt-rally?partner=jugandoenlinux) (-80%) : **6.99€** ---> [Análisis de JugandoEnLinux](index.php/homepage/analisis/20-analisis/362-analisis-dirt-rally)
> 
> 
> - [F1 2017](https://www.humblebundle.com/store/f1-2017?partner=jugandoenlinux) (-75%) : **13.74€** ---> [Análisis de JugandoEnLinux](index.php/homepage/analisis/20-analisis/660-analisis-f1-2017)
> 
> 
> - [GRID Autosport](https://www.humblebundle.com/store/grid-autosport?partner=jugandoenlinux) (-75%) : **9.99€**
> 
> 
> - [F1 2015](https://www.humblebundle.com/store/f1-2015?partner=jugandoenlinux) (-75%): **8.74€**
> 
> 
> 


Tened en cuenta que estos juegos son muy populares en nuestra comunidad JugandoEnLinux.com, siendo jugados a menudo en multijugador, como en [nuestro campeonato de F1](index.php/foro/campeonato-f1-2018/104-mundial-f1-jugandoenlinux-com) o las Ligas [JugandoEnLinux](https://www.dirtgame.com/es/leagues/league/88825/jugandoenlinux) y [Yodefuensa](https://www.dirtgame.com/es/leagues/league/92549/yodefuensa) de **DIRT Rally**, por lo que si estais interesados en participar es un buen momento para adquirirlos. También sabed que aunque no tengan soporte nativo, muchos de los [juegos de esta oferta](https://www.humblebundle.com/store/promo/festival-of-speed-mk-2/?partner=jugandoenlinux) funcionan correctamente con Steam Play/Proton, como es el caso de Project Cars 2, o Assetto Corsa Competizione.


Los enlaces a Humble Store de este artículo son enlaces patrocinados, y cada vez que compreis a través de ellos estareis ayudándonos a pagar los gastos de Hosting, Dominios, etc... por lo que os damos las gracias de antemano. Recordad que esta promoción solo estará activa hasta el Lunes a las 19:00 CEST (hora española).

