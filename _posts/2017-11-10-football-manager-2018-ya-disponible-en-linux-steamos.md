---
author: Pato
category: Estrategia
date: 2017-11-10 07:42:52
excerpt: "<p>Otra nueva entrega de esta excelente saga de gesti\xF3n deportiva. \xA1\
  Demo disponible!</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/21615ff211c60b43d866a2b2bca320da.webp
joomla_id: 526
joomla_url: football-manager-2018-ya-disponible-en-linux-steamos
layout: post
tags:
- deportes
- estrategia
- simulacion
title: '''Football Manager 2018'' ya disponible en Linux/SteamOS'
---
Otra nueva entrega de esta excelente saga de gestión deportiva. ¡Demo disponible!

Como ya estaba anunciado [desde hace tiempo](index.php/homepage/generos/simulacion/item/553-football-manager-2018-debutara-el-10-de-noviembre) hoy por fin tenemos disponible esta nueva entrega del mánager de fútbol por excelencia.


*Dicen que el fútbol es un deporte de opiniones y todo el mundo tiene una, pero aquí es la tuya la que cuenta. Toma decisiones, desde a quién fichar y a quién traspasar, hasta cómo administrar el presupuesto.Depende de ti gestionarlo todo.Todas las decisiones tienen sus consecuencias.*


*Hazlo bien y te convertirás en la estrella del espectáculo, acapararás titulares y dominarás las discusiones en las redes sociales. Escribirán tu nombre en las leyendas del fútbol... si tienes éxito, claro. Con elecciones incomparables dentro y fuera del campo en un envolvente mundo futbolístico, es hora de decidir qué clase de entrenador quieres ser.*


* *Prueba suerte en 50 de los países futbolísticos más grandes*
* *Gana el título con cualquiera de los 2500 mejores clubes*
* *Participa en el mercado de traspasos con más de 600 000 jugadores y personal de la vida real*
* *Contempla cómo tu visión futbolística se convierte en realidad ante tus propios ojos*


Football Manager 2018 [[web oficial](http://www.footballmanager.com/)] viene con nuevas características, como nuevas dinámicas, sistema de ojeador realista, nuevo motor gráfico, nuevas técnicas y tácticas, nueva IA y mucho más.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/1Woak1Bl_KI" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/UgETAW5WEDc" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


**Los requisitos mínimos son:**  

+ **SO:** SteamOS, Ubuntu 14.04.5 LTS – 64-bit
+ **Procesador:** Intel Pentium 4, Intel Core o AMD Athlon – 2.2 GHz +
+ **Memoria:** 2 GB de RAM
+ **Gráficos:** Intel HD Graphics 3000/4000, NVIDIA GeForce 9600M GT o AMD/ATI Mobility Radeon HD 3650 – 256MB VRAM
+ **Almacenamiento:** 7 GB de espacio disponible
+ **Notas adicionales:** OpenGL: 2.1



  
Football Manager 2018 incluye una copia gratuita de Football Manager Touch 2018 para PC/Mac y Linux.


Si te gusta gestionar tu equipo de fútbol favorito y eres de los que no se arrugan hasta llevar al Club a lo más alto, tienes este 'Football Manager' traducido al español en su página de Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/624090/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 

