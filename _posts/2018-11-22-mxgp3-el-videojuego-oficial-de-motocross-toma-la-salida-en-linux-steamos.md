---
author: Pato
category: Arcade
date: 2018-11-22 17:23:48
excerpt: <p>Por fin un nuevo port de Virtual Programming @virtualprog&nbsp;</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/db4fc567ca6f38f6ec00bf19dfb7c044.webp
joomla_id: 910
joomla_url: mxgp3-el-videojuego-oficial-de-motocross-toma-la-salida-en-linux-steamos
layout: post
tags:
- carreras
- arcade
- motocross
title: MXGP3, el videojuego oficial de Motocross toma la salida en Linux/SteamOS
---
Por fin un nuevo port de Virtual Programming @virtualprog 

Hace ya un tiempo que [venimos siguiendo](index.php/homepage/generos/carreras/2-carreras/759-mxgp3-parece-que-llegara-a-linux-finalmente) MXGP3, el videojuego de motocross oficial de **Milestone** y que estaba siendo portado por **Virtual Programming** para nuestro sistema favorito. Pues ya no hace falta esperar mas, ya que [nos llega la notificación](https://steamcommunity.com/games/561600/announcements/detail/1709575532586127369) de que el port acaba de ser lanzado y ya está disponible para su compra.



> 
> *¡Vive toda la adrenalina del motocross con el único videojuego oficial del campeonato! MXGP3 - The Official Motocross Videogame ofrece la experiencia de juego más cautivadora hasta el momento, con una jugabilidad y gráficos completamente nuevos gracias a Unreal® Engine 4. ¡Compite en 18 circuitos oficiales y el emocionante MXoN con todos los pilotos y todas las motocicletas de la temporada 2016 de MXGP y MX2 y sé el primero en experimentar la emoción de pilotar una de las 10 motocicletas de dos tiempos disponibles! ¡Haz que tu piloto y tu motocicleta sean únicos con más de 300 componentes oficiales con los que crear una personalización completa!*
> 
> 
>   
> ***Inmersión total***  
> *Diseñado desde cero con el motor **Unreal® Engine 4**, dotado de gráficos y escenarios ultrarrealistas, increíbles detalles y espectaculares efectos visuales.*  
>   
> ***Solo contenido oficial***  
> *Licencias, circuitos y pilotos oficiales de la temporada **2016 del FIM Motocross World Championship**.*  
> *Disfruta de gran cantidad de opciones de personalización para tu moto y piloto utilizando **más de 300 elementos**.*  
>   
> ***Más diversión con los motores de 2 tiempos***  
> *Disfruta, por primera vez en la licencia, de la conducción con **motor de 2 tiempos**.*  
> *Elige entre **10 motos reales**, con sonido y movimientos increiblemente realistas.*
> 
> 
> 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/uUxmQqamN1k" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div> 


En cuanto a los requisitos, según los publicados en Steam, como mínimo necesitaremos:


**MÍNIMO:**  

+ Requiere un procesador y un sistema operativo de 64 bits
+ **SO:** Ubuntu 16.04 o superior, SteamOS 2.0
+ **Procesador:** Intel Core i5-2500K; AMD FX-6350 or equivalente
+ **Memoria:** 4 GB de RAM
+ **Gráficos:** NVIDIA GeForce GTX 760 con 2 GB VRAM o superior; AMD Radeon HD 7950 with 2 GB VRAM o superior
+ **Almacenamiento:** 13 GB de espacio disponible
+ **Notas adicionales:** AMD - mesa 18.1 o superior / Nvidia - nvidia 390.48 o superior / Intel Graphics no están soportadas



 
Lo cierto es que la propuesta de Milestone de momento es única en Linux, pues no tenemos un juego de motocross, ni siquiera de motos como referencia. Esperemos que el port esté a la altura y resulte provechoso para Virtual Programming y sobre todo para Milestone. Sería una gran noticia que se planteasen portar el resto de su catálogo de juegos de motor, como por ejemplo la saga MotoGP, Gravel o sus próximos desarrollos, ¿no creeis?


Si te van las motos y llenarte de barro puedes comprar MXGP3 en [Humble Bundle](https://www.humblebundle.com/store/mxgp3-the-official-motocross-videogame?partner=jugandoenlinux) (enlace de afiliado) o en su [página de Steam](https://store.steampowered.com/app/561600/MXGP3__The_Official_Motocross_Videogame/).

