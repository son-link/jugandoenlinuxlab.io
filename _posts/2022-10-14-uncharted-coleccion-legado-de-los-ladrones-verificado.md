---
author: Odin
category: Noticia
date: 2022-10-14 11:57:07
excerpt: "<p>Gracias a <a href=\"https://steamdb.info/app/1659420/history/?changeid=16358931\"\
  \ target=\"_blank\" rel=\"noopener\">SteamDB</a> se filtra que Uncharted ser\xE1\
  \ verificado para la Steam Deck desde el primer d\xEDa de su publicaci\xF3n en Steam.</p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Uncharted/uncharted.webp
joomla_id: 1495
joomla_url: uncharted-coleccion-legado-de-los-ladrones-verificado
layout: post
tags:
- proton
- steam-deck
title: " UNCHARTED: Colecci\xF3n Legado de los Ladrones verificado"
---
Gracias a [SteamDB](https://steamdb.info/app/1659420/history/?changeid=16358931) se filtra que Uncharted será verificado para la Steam Deck desde el primer día de su publicación en Steam.


Y es que si ya, con la publicación de **Horizon Zero Dawn, God of War y Spiderman Remastered** en Steam como verificados, la **Steam Deck** se estaba convirtiendo en la **PS Vita 2** que Sony nunca llego a lanzar al mercado, ahora tendremos otra de las franquicias insignia de PlayStation disponible para jugar en Linux gracias a **Proton**.


Aunque la conversión a PC se ha hecho esperar, tras las buenas sensaciones que dejaron las versiones de God of War y Spiderman en nuestras Steam Deck, desde **Jugando En Linux** esperamos que **Uncharted** funcione igual de bien que los anteriores.


**UNCHARTED: la Colección Legado de los Ladrones**, incluye tanto el videojuego original **UNCHARTED 4: El desenlace del ladrón** como la expansión **UNCHARTED: El legado perdido**. La fecha de publicación es el día 19 de Octubre en [Steam](https://store.steampowered.com/app/1659420/UNCHARTED_Coleccin_Legado_de_los_Ladrones/).


¿Qué te parece [Uncharted](https://store.steampowered.com/app/1659420/UNCHARTED_Coleccin_Legado_de_los_Ladrones/)? ¿Lo probarás? Cuéntanos como siempre tu opinión sobre esta noticia en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).


 

