---
author: Pato
category: "Acci\xF3n"
date: 2017-10-03 18:09:51
excerpt: "<p>El juego ten\xEDa el logo de SteamOS pero lo retiraron a pocos d\xED\
  as de su lanzamiento</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/349c990ab6c453afa79180a77c74b6f4.webp
joomla_id: 472
joomla_url: ruiner-llegara-a-linux-tras-su-lanzamiento-en-consolas
layout: post
tags:
- accion
- indie
- gore
- violento
- ciberpunk
title: "'RUINER' llegar\xE1 a Linux tras su lanzamiento en consolas"
---
El juego tenía el logo de SteamOS pero lo retiraron a pocos días de su lanzamiento

RUINER es otro de esos casos en los que tristemente vemos como un juego que estaba anunciado para su lanzamiento en nuestro sistema favorito se retrasa con respecto a otras plataformas. Según las declaraciones al portal Gamasutra y recogidas por [www.gamingonlinux.com](https://www.gamingonlinux.com/articles/ruiner-still-may-not-come-to-linux-encountering-technical-issues.10456) los desarrolladores encontraron problemas con el desarrollo tanto para Mac como para Linux, por lo que se han visto forzados a retrasar el lanzamiento para estos dos sistemas, y se plantean un lanzamiento en Linux posterior al de consolas. La versión de Mac por su parte es la que contaría con más problemas de desarrollo, y sería lanzada incluso después de la de Linux.


Se da la circunstancia de que en un principio el juego si parece que estaba programado para salir simultáneamente en los tres sistemas principales, e incluso tenía el logo de SteamOS en la tienda por lo que también daba a entender que estaría disponible en Linux llevando a cabo incluso una campaña de pre-compra sin que se avisase de que la versión de Linux no iba a salir al mismo tiempo. Fue tan solo unos días antes del lanzamiento oficial cuando se retiraron los iconos de Mac y Linux, dejando la incógnita de que estaba pasando, por lo que algunos de los usuarios que lo pre-compraron pidieron la devolución.


Poco después, en el [hilo de seguimiento](http://steamcommunity.com/app/464060/discussions/0/154644928862239593/?ctp=23#c1496741765127956846) del desarrollo para Linux en Steam, uno de los responsables confirmó los puntos anteriores y nos emplazó a estar atentos a las noticias sobre el lanzamiento en próximas semanas a la vez que anunció una "pequeña sorpresa" para el lanzamiento en nuestro sistema favorito. Estaremos atentos para saber de qué se trata.


RUINER [[web oficial](http://ruinergame.com/)] es un emocionante y brutal juego de disparos ambientado en el año 2091 en la cibermetrópolis de Rengkok. Un sociópata trastornado se rebela contra el corrupto sistema para rescatar a su hermano secuestrado y descubrir la verdad guiado por una misteriosa amiga hacker.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/XmfMkdfrnKY" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


RUINER está editado por Devolver Digital, y esperemos que esté disponible en Linux en un futuro próximo.


¿Que piensas sobre los juegos que anuncian su lanzamiento en Linux y luego lo retrasan? ¿has comprado algún juego en campaña de pre-compra y luego has tenido que devolverlo?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 

