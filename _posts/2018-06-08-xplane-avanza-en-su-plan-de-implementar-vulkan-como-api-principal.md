---
author: Pato
category: "Simulaci\xF3n"
date: 2018-06-08 11:08:11
excerpt: "<p>Laminar Research estar\xE1 en Las Vegas para la&nbsp; Flight Sim Expo\
  \ y hablar\xE1 sobre sus planes de futuro</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/0ee70163e29bae5db9d4be0d088a4c1c.webp
joomla_id: 760
joomla_url: xplane-avanza-en-su-plan-de-implementar-vulkan-como-api-principal
layout: post
tags:
- simulador
- vulkan
- simulacion
title: '''Xplane'' avanza en su plan de implementar Vulkan como API principal'
---
Laminar Research estará en Las Vegas para la  Flight Sim Expo y hablará sobre sus planes de futuro

Todo lo que sea el apoyo ala API Vulkan suele ser una buena noticia para el desarrollo en Linux. En este caso nos hacemos eco de los comentarios sobre el progreso en la implementación de esta API "multiplataforma" para el "simulador aeronáutico" por excelencia que tenemos disponible desde hace ya varias ediciones en Linux/SteamOS.


Hablamos de **Xplane**, un simulador "hardcore" desarrollado por Laminar Research, y del que ya nos hemos hecho eco en otras ocasiones y del que ya en Junio del año pasado [ya sabíamos](index.php/homepage/generos/simulacion/item/534-los-desarrolladores-de-xplane-11-estan-trabajando-para-implementar-vulkan-en-el-simulador) que tomaría el camino de **Vulkan** para sus desarrollos futuros.


Ahora, con las noticias sobre el [cese de soporte de OpenGL](index.php/homepage/editorial/item/876-opengl-sera-legacy-en-las-nuevas-versiones-de-macos-como-puede-afectar-al-juego-en-linux) por parte de la nueva versión de macOS el estudio va a acudir con gran parte de sus desarrolladores a la conferencia "[Flight Sim Expo](http://www.flightsimexpo.com/)" que se llevará a cabo este fin de semana en Las Vegas para hablar sobre el futuro desarrollo ya con la nueva API Vulkan en mente:



> 
> Austin is coming to [@FlightSimExpo](https://twitter.com/flightsimexpo?ref_src=twsrc%5Etfw) with the rest of the dev team to talk Vulkan integration and to show off what we have been working on recently! Saturday - 10:15a - see you then! [#XPlane](https://twitter.com/hashtag/XPlane?src=hash&ref_src=twsrc%5Etfw) [#AvGeek](https://twitter.com/hashtag/AvGeek?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/ExWQcGpIX0](https://t.co/ExWQcGpIX0)
> 
> 
> — X-Plane (@XPlaneOfficial) [5 de junio de 2018](https://twitter.com/XPlaneOfficial/status/1004096721423683584?ref_src=twsrc%5Etfw)



 Además de esto, anuncian que también mostrarán en lo que han estado trabajando últimamente. Estaremos atentos a las posibles noticias de interés al respecto.


*El nuevo X-PLANE 11, es un simulador completamente rediseñado, con una interfaz de usuario intuitiva que hará la configuración y edición de vuelo más fácil que nunca.*   
*Cabinas de mando en 3-D y modelos exteriores con un sensacional diseño en alta definición para todas las aeronaves disponibles que harán su experiencia más real.*   
*Un nuevo motor de efectos para iluminación, sonidos y explosiones.*  
*Aviónicas y sistemas de navegación realistas: todas las aeronaves están preparadas para volar IFR desde el inicio.*  
*Aeropuertos “con vida”, ocupados, donde se encontrará vehículos de remolque, camiones de combustible, que están listos para prestar servicio a todas las aeronaves del simulador, incluyendo la suya.*  
*Nuevas edificaciones, calles, avenidas que simulan mejor las ciudades europeas.*


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/Ov0Sm9GDha4" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 Tienes disponible Xplane 11 para Linux en su [página de Steam](https://store.steampowered.com/app/269950/XPlane_11/).

