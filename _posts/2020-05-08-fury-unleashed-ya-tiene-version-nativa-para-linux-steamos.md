---
author: Pato
category: "Acci\xF3n"
date: 2020-05-08 11:53:14
excerpt: "<p>El juego de acci\xF3n y plataformas de&nbsp;@AwesomeGamesStd a\xFAn en\
  \ acceso anticipado planea su lanzamiento para este pr\xF3ximo d\xEDa 8</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/FuryUnleashed/FuryUnleashed.webp
joomla_id: 1217
joomla_url: fury-unleashed-ya-tiene-version-nativa-para-linux-steamos
layout: post
tags:
- accion
- plataformas
- roguelike
- cooperativo
title: "Fury Unleashed ya tiene versi\xF3n nativa para Linux/SteamOS"
---
El juego de acción y plataformas de @AwesomeGamesStd aún en acceso anticipado planea su lanzamiento para este próximo día 8


 


Tal y como prometieron los chicos de Awesome Games Studio por fin tenemos disponible para nuestro sistema favorito su juego de acción y plataformas con toques "roguelike" **Fury Unleashed**, aunque el juego sigue, de momento en acceso anticipado.


Fury Unleashed es un juego con estilo de la "vieja escuela" donde la acción sin complicaciones hará las delicias de los aficionados al género.


*Fury Unleashed es un juego roguelite de plataformas y acción con combos. Cada vez que matas, aumentas tu combo. ¡Alcanza ciertos límites y se activarán tu resistencia al daño y tus poderes curativos! Es un juego que incluso puedes acabar con un solo combo definitivo. ¿Tienes lo que hay que tener?*
 
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/YRZwYLi1gpg" width="560"></iframe></div>
 
Fury Unleashed además ofrece juego cooperativo local y en remoto a través de Remote Play Together.
En cuanto a los requisitos mínimos, según su página son:
**MÍNIMO:**  

+ **SO:** Ubuntu 18.04 o superior
+ **Procesador:** Intel Core i5 3.0GHz
+ **Memoria:** 4 GB de RAM
+ **Gráficos:** Nvidia GeForce GTX 650 / Radeon HD 7510
+ **Almacenamiento:** 1600 MB de espacio disponible



 
Si qiueres saber más sobre el juego, puedes visitar su [página web oficial](http://www.furyunleashed.net).
Fury Unleashed planea salir de acceso anticipado y lanzarse oficialmente este próximo 8 de Mayo, sin embargo ya podemos comprarlo con un 10% de descuento en su página de Steam [en este enlace](https://store.steampowered.com/app/465200/Fury_Unleashed/).
 
Qué te parece Fury Unleashed? ¿Te gustan los juegos de acción y plataformas? 

Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).



 