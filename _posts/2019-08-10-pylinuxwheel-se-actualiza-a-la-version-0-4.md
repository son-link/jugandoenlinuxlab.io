---
author: Serjor
category: Software
date: 2019-08-10 14:46:10
excerpt: "<p>La utilidad alcanza la versi\xF3n 0.5.1 con multitud de cambios.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/5a05a447acfdf6fcc40548cc4c1cea8d.webp
joomla_id: 1098
joomla_url: pylinuxwheel-se-actualiza-a-la-version-0-4
layout: post
tags:
- logitech
- volantes
- pylinuxwheel
- force-feedback
title: PyLinuxWheel se actualiza de nuevo (ACTUALIZADO).
---
La utilidad alcanza la versión 0.5.1 con multitud de cambios.

**ACTUALIZACIÓN 5-10-19:** Si recordais hace bien poco @Bernat nos solprendía con la [nueva versión de Oversteer](index.php/homepage/generos/software/12-software/1139-la-nueva-version-de-oversteer-nos-trae-interesantes-anadidos), y ahora le toca el turno a @OdinTdh con [PyLinuxWheel](https://gitlab.com/OdinTdh/pyLinuxWheel). Al parecer le han sentado bien sus vacaciones y ha vuelto con energías renovadas, vitaminando a tope su aplicación para incluir numerosas mejoras y cambios importantes en esta [versión 0.5.1](https://gitlab.com/OdinTdh/pyLinuxWheel/-/releases). Serían las siguientes:


*-**Se ha añadido soporte para más volantes**: Driving Force (EX, RX), G920, Logitech Racing Wheel USB, WingMan Formula (amarillo, GP, Force GP) y volantes MOMO (Force, Racing).*  
 *-**Traducción al francés** por @Krafting. Gracias @Krafting por su ayuda!*  
 *-**Interfaz autoadaptable**. PyLinuxWheel ahora sólo permite aquellas opciones de su interfaz que son compatibles con el volante.*  
 *-Añadida una nueva **pestaña de prueba** donde el usuario puede comprobar el Force Feedback, los botones y los pedales del volante.*  
 *-**Nuevas opciones de Force Feedback** para ajustar la **fuerza general de los efectos** de Force Feedback y para configurar la **fuerza necesaria para girar el volante**.*  
 *-**Nueva Appimage** creada disponible en itch.io con todas las dependencias incluidas.*  
 *-PyLinuxWheel **puede recargar valores de Force Feedback entre sesiones**. Anteriormente sólo se podían guardar valores de alcance, pedales de combinación y modos alternativos. Este comportamiento es configurable en la pestaña de preferencias.*


![pyLinuxWheel05](/https://img.itch.zone/aW1hZ2UvNDY2NTk2LzI1NTE1NjEucG5n/347x500/aDMO8y.webp)


Como veis la actualización no es moco de pavo y merece mucho la pena actualizar. Para ello [podeis descargaros PyLinuxWheel de su página en Itch.io](https://odintdh.itch.io/pylinuxwheel).




---


**NOTICIA ORIGINAL:**


Nuestro dios nórdico favorito ha lanzado una nueva actualización de la utilidad pyLinuxWheel, la cuál nos permite configurar los volantes Logitech de una forma tremendamente sencilla.


En esta ocasión los cambios son:


* Soporte para el modo alternativo del driver del volante, de tal manera que si un juego no soporta nuestro volante, podemos pedirle que emule un modelo anterior y tener así soporte en el juego
* La aplicación ahora está traducida al castellano
* Soporte para el volante Driving Force Pro Logitech


 


Y además de esto cambios, esta versión ha sido publicada también en itch.io, así que puedes descargarte el AppImage de esta versión tanto desde su página en [itch.io](https://odintdh.itch.io/pylinuxwheel) como desde su repositorio en [gitlab](https://gitlab.com/OdinTdh/pyLinuxWheel/-/tags/0.4).


Y tú, ¿qué utilidad usas para configurar tu volante? Cuéntanoslo en los comentarios o en nuestros canales de [Telegram](https://t.me/jugandoenlinux) y [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org)

