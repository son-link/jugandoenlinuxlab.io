---
author: P_Vader
category: "Acci\xF3n"
date: 2021-05-28 15:41:50
excerpt: "<p>@moveordiegame de <span class=\"r-18u37iz\">@someawesomeguys</span> celebra\
  \ m\xE1s de 1 mill\xF3n de copias vendidas con la actualizaci\xF3n \"La Fiesta Latina\"\
  </p>\r\n"
image: https://cdn.akamai.steamstatic.com/steam/apps/323850/header_alt_assets_13.jpg?t=1622135901
joomla_id: 1312
joomla_url: move-or-die-llega-al-millon-de-copias
layout: post
tags:
- accion
- plataformas
- multijugador
- actualizacion
title: "Move or Die llega al mill\xF3n de copias"
---
@moveordiegame de @someawesomeguys celebra más de 1 millón de copias vendidas con la actualización "La Fiesta Latina"


 ![Move or die update](https://cdn.akamai.steamstatic.com/steamcommunity/public/images/clans/6909713/609a7c18385327dd815424b8156c1a035073b317.png)

Si aun no has probado [Move or Die](https://www.humblebundle.com/store/move-or-die?partner=jugandoenlinux) no sabes lo que te estas perdiendo, sobre todo si te gusta jugar acompañado y echarte muchas risas, incluso ¡llorar de risa!, porque el juego es desternillante ¡te lo aseguro!


Hoy estamos de enhorabuena ya que los chicos de [thoseawesomeguys](https://thoseawesomeguys.com/) **cumplen 5 años del lanzamiento de su juego con mas de 1 millon de copias vendidas** y además han dedicado una pequeña actualización al mundo latino, con una **revisión de las traducciones latinoamericanas y 3 nuevos personajes**:


![Nuevos personajes de Move or Die](https://mcusercontent.com/5db105e138032e564b4758e4b/images/f967d190-0859-d2da-783d-9f5bf0f7e306.jpg)


 


Por si aún no sabes que trae Move or Die, algunas de sus características son:


* **Modo Multijugador local/online** para hasta cuatro jugadores y modo de entrenamiento offline con Bots que no lo ponen nada fácil (y si, son unos cachondos).
* **Decenas de mini juegos** que puedes ir ampliando conforme avanzas con su sistema de no-compras en su no-tienda ¿?\*#! ¡JA! Está gente es así, se ríen hasta de los micro pagos sin pagos.
* Conforme avanzas subes de nivel o ranking y obtienes monedas.
* **Vibrantes gráficos 2D** y una animada banda sonora firmada por Jacob Lincke.
* Soporte completo para **Gamepad y Teclado**
* **DLC gratis para siempre**, como no podía ser menos.


 


Hace algún tiempo os enseñé una partida de tantas que nos echamos en familia:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/zFuoI-9AFYM" title="YouTube video player" width="780"></iframe></div>


 


Ahora mismo lo he vuelto a probar y me he encontrado que he ganado.....


![moveordie](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Moveordie/moveordie.webp)


**¡Un precioso ¿BananaCat? de personaje! x'D**


 


Lo puedes conseguir **en [humblebundle](https://www.humblebundle.com/store/move-or-die?partner=jugandoenlinux) (patrocinado) o** [**Steam**.](https://store.steampowered.com/app/323850/Move_or_Die/) Cuentanos que te parece este loco juego en nuestros canales habituales de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).


 

