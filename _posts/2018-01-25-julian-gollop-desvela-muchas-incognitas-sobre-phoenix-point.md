---
author: leillo1975
category: Estrategia
date: 2018-01-25 11:32:16
excerpt: <p>Tenemos nuevos detalles sobre el juego</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/ae252c0654e8edaca099dba64773f3f2.webp
joomla_id: 616
joomla_url: julian-gollop-desvela-muchas-incognitas-sobre-phoenix-point
layout: post
tags:
- phoenix-point
- snapshot-games
- julian-gollop
- ama
title: "Julian Gollop desvela muchas inc\xF3gnitas sobre Phoenix Point (ACTUALIZACI\xD3\
  N 2)"
---
Tenemos nuevos detalles sobre el juego

**ACTUALIZACIÓN 28-3-18**: Snapshot Games nos acaba de informar vía mail que el próximo **30 de Abril** pondrán a disposición de los contribuyentes de su campaña de crowfounding la "**Build One**". Piden disculpas por el retraso, pues esta **se esperaba para el primer cuarto de este año y llegará un mes tarde**, pero querían lanzar la mejor build posible de esta versión de prueba. Al parecer han estado bastante ocupados con la implementación del sistema de inventario, lo cual es algo clave en el juego. También comentan en la entrada de su blog información sobre eventos a los que asistirán a presentar su juego, y más **información sobre las mutaciones de los aliens**, las cuales permitirán multiples combinaciones que darán características diferenciadas a los enemigos. Si quereis entrar más a fondo en esto último os recomendamos que sigais este enlace. También os dejamos con un nuevo video donde podreis ver la jugabilidad de Phoenix Point con más detalle:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/ROUVN8zpiWA" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 




---


**ACTUALIZADO:** Snapshot games nos acaba de comunicar (felizmente) que la versíón de Linux no está cancelada. Concretamente han dicho:



> 
> *"No, la versión de Linux no ha sido cancelada! Julian se equivocó al hablar en la AMA. Quiso decir que estamos trabajando en las versiones "PC y Mac", no en las versiones "Windows y Mac"."*
> 
> 
> 


Agradecemos a los desarrolladores haberse puesto en contacto con nosotros tan rapidamente, y quedamos a la espera de nuevas noticias.




---


**NOTICIA ORIGINAL**: En el mes de Abril [os hablamos de un interesante proyecto](index.php/homepage/generos/estrategia/item/420-phoenix-point-interesantisimo-juego-del-creador-de-xcom-esta-en-crowfunding) creado por [Snapshot Games](http://www.snapshotgames.com), la compañía de [Julian Gollop](https://en.wikipedia.org/wiki/Julian_Gollop#List_of_video_games), el **creador original de la saga XCOM**, y participante también de la creación de juegos de las sagas de Ghost Recon y Assasins Creed. En esta nueva etapa como CEO de esta compañía ya lanzaron [Chaos Reborn](http://store.steampowered.com/app/319050/Chaos_Reborn/), un juego que obtuvo muy buenas críticas hace poco más de dos años. Este pasado Lunes realizó un **AMA** (Ask Me Anything) en su página de [Facebook](https://www.facebook.com/PhoenixPointGame), donde contestó muchas y variadas preguntas sobre su último desarrollo, un juego de estrategia por turnos muy similar a XCOM. Podeis verlo en este video:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="350" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2FPhoenixPointGame%2Fvideos%2F554827661564145%2F&amp;show_text=0&amp;width=560" style="overflow: hidden; display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


La charla fué interesante y reveladora en muchos aspectos, pero entre ellos habría que resaltar los siguientes:



> 
> -El juego se lanzará a finales de año, pero muy probablemente tenga una etapa de **Acceso anticipado**. Además también los pre-compradores de la edición Digital de Lujo y contribuyentes de su campaña de Crowfunding, podrán acceder a una versión **Pre-Alfa** a finales de Marzo o principios de Abril.
> 
> 
> -Al igual que el XCOM original de 1994, el juego tendrá **fases autogeneradas, entornos destruibles, inventarios de soldado y fisicas de bala**. Sobre este último punto hay que decir que al igual que el XCOM original, **el daño es por proyectil, no por ráfaga**. Puedes herir tanto al objetivo, como a las unidades adyacentes con cada una de las balas, incluso con fuego amigo.
> 
> 
> -Habrá muchos tipos de Alien diferentes y **todos podrán mutar.**
> 
> 
> -Cada una de las 3 facciones tendrá su **árbol de tecnología**, la cual podrás comprar o robar. También tendrás por supuesto tu propio arbol tecnológico.
> 
> 
> -Podremos **personalizar** las unidades en lo relativo a la armadura, equipamiento y apariencia.
> 
> 
> -**Las bases** no se crean, sino que **se descubren**, pero se pueden mejorar y ampliar. Estas podrán ser atacadas por el enemigo. **Podremos además tener más de una**.
> 
> 
> -Estando cerca del objetivo no se va a poder errar un ataque, incluso a pocas casillas se tendrá el 100% de efectividad.
> 
> 
> -Será posible saquear armas y munición durante la batalla.
> 
> 
> -Con respecto a XCOM **tiene un enfoque de mundo abierto** con facciones y subfacciones con su propia agenda y recursos. Además **habrá menos complejidad y gestión**.
> 
> 
> -Habrá un **DLC gratuito después del lanzamiento** en el que podremos tener bases flotantes, un tipo de misión adicional y alguna nueva tecnología. Existen planes para más DLC's, pero aun no están definidas pues están en desarrollo.
> 
> 
> -Al igual que en XCOM con el proyecto AVATAR, los alienígenas tendrán su propia agenda y planes, y podrán ganar la partida si la completan. Nosotros podremos realizar acciones que logren detenerlo.
> 
> 
> -Los **niveles** estarán enfocados al combate principalmente, pero también **habrá algunos de exploración**.
> 
> 
> -El juego podrá ser adquirido digitalmente tanto en Steam como en GOG.com.
> 
> 
> -A la pregunta de si se va a portar el juego en PS4, Julian Gollop contestó que les supondría un esfuerzo muy grande de tiempo y trabajo. **Sorpresivamente dijo que su prioridad estaba centrado en las versiones de Windows y Mac**.
> 
> 
> 


En cuanto a esto último, hay que destacar que no ha negado el desarrollo para Linux, pero tampoco lo ha mencionado, por lo que esperemos que simplemente se le haya olvidado o lo dejen para el final. Lo que es seguro es que en su [campaña de crowfunding en FIG](https://www.fig.co/campaigns/phoenix-point?media_id=v207) (la cual fué superada con creces) **se incluía Linux como plataforma soportada**. Desde JugandoEnLinux **intentaremos aclarar este último punto** para ofreceros la información lo antes posible, aunque podemos deciros que en la última comunicación que tuvimos con ellos hace unos días su respuesta fué *"We intend to launch the Windows, Linux and Mac OS versions at the same time." (*"Tenemos la intención de lanzar las versiones de Windows, Linux y Mac OS al mismo tiempo."). Si quieres más información (en inglés) sobre esta AMA solo tienes que [pinchar aquí](https://phoenixpoint.info/blog/2018/1/22/julians-live-question-and-answer-session-on-facebook-live?utm_source=Phoenix+Point&utm_campaign=fc9775c984-DEV_BLOG_2017_1_24&utm_medium=email&utm_term=0_0f91b4b9df-fc9775c984-178016385&mc_cid=fc9775c984&mc_eid=1f7eb10941).


![PhoenixPoint02](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/PhoenixPoint/PhoenixPoint02.webp)


 


¿Que os ha parecido todos estos datos sobre este juego? ¿Os gusta este tipo de estrategia? Déjanos tus impresiones sobre lo leido en este artículo en los comentarios, o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

