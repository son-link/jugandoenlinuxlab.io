---
author: Serjor
category: "Acci\xF3n"
date: 2017-03-27 18:52:15
excerpt: "<p>\xA1Corred insesatos! que la oferta acaba el 28 de marzo</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/c07bdbd398dd4ac563358e095fc5d2e0.webp
joomla_id: 265
joomla_url: chivalry-medieval-warfare-gratuito-de-forma-temporal
layout: post
tags:
- oferta
- chivalry-medieval-warfare
title: 'Chivalry: Medieval Warfare gratuito de forma temporal'
---
¡Corred insesatos! que la oferta acaba el 28 de marzo

Nos enteramos vía Twitter de que Chivalry: Medieval Warfare está en steam disponible de manera gratuita durante [tiempo limitado](http://store.steampowered.com/news/28406/), hasta el 28 de marzo, aunque una vez adquirido, a pesar de que la oferta expire, será nuestro para siempre.


 



> 
> Now Free on Steam for a Limited Time - Chivalry: Medieval Warfare <https://t.co/AuXAM35EGE> [pic.twitter.com/dqhvbRR0bR](https://t.co/dqhvbRR0bR)
> 
> 
> — Steam (@steam_games) [March 27, 2017](https://twitter.com/steam_games/status/846414382053670913)



 


Chivalry: Mediaval Warfare es un juego multijugador, con soporte para Steam Workshop, en el que tendremos que asediar o defender castillos y pueblos con armas típicas de la época medieval, como espadas, lanzas, escudos, ballestas...


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" frameborder="0" height="315" src="https://www.youtube.com/embed/JcNuz63JbKc" width="560"></iframe></div>


 


 Si estáis interesados podéis adquirirlo desde aquí:


<div class="steam-iframe"><iframe frameborder="0" height="190" src="https://store.steampowered.com/widget/219640/164655/" width="646"></iframe></div>


 


Así que si aprovecháis esta más que suculenta oferta, no olvidéis comentarnos qué os parece el juego, para ello podéis usar los comentarios de este post o nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD).

