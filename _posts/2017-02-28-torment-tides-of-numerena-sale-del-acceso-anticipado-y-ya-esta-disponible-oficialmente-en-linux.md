---
author: Pato
category: Rol
date: 2017-02-28 07:52:28
excerpt: "<p>El juego de inXile Entertainment por fin ve la luz con soporte en Linux\
  \ desde hoy, d\xEDa de lanzamiento</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/7081cca2f9cd0c06f2cce9e93d01dda9.webp
joomla_id: 232
joomla_url: torment-tides-of-numerena-sale-del-acceso-anticipado-y-ya-esta-disponible-oficialmente-en-linux
layout: post
tags:
- indie
- aventura
- rol
- steam
title: "'Torment: Tides of Numerena' sale del acceso anticipado y ya est\xE1 disponible\
  \ oficialmente en Linux"
---
El juego de inXile Entertainment por fin ve la luz con soporte en Linux desde hoy, día de lanzamiento

Por fin hoy nos llega 'Torment: Tides of Numerena', el juego de rol desarrollado por inXile Entertainment, estudio que ha trabajado en juegos como 'Wasteland 2'. Hasta ahora tan solo estaba disponible en acceso anticipado y para sistemas Windows, pero hoy mismo ya se puede jugar en Linux.


'Torment: Tides of numerena' es un juego de rol en vista isométrica para un jugador en el que vivirás una historia dentro del universo de Numerena y su "Monte Cook" . De hecho el juego es sucesor de la temática del juego de Rol 'Planescape: Torment'. "*Has nacido cayendo como una nueva mente dentro de un cuerpo ocupado por el Dios Cambiante, un ser que ha esquivado a la muerte durante milenios. Si sobrevives, tu camino a través del noveno mundo será cada vez más extraño, y mortal.*"


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/V7QKH891Huc" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


Dentro del juego vivirás una historia con una temática filosófica dentro de un mundo rico y vivo como ningún otro, y donde tus elecciones y acciones serán tus principales armas. Viajarás junto a extrañas compañías que según sus propios intereses podrán ayudarte o dañarte, y tus decisiones serán decisivas en el transcurso de una historia personal donde según tus elecciones nada será bueno o malo, si no que dependerá de ti mismo.


Desde luego, tiene toda la pinta de convertirse en un clásico de los juegos de rol narrativos.


Los requisitos para jugar Torment: Tides of Numerena son:


Mínimos:


SO: Ubuntu 16.04 o superior (64-bit), SDL 2.0 o superior  
Procesador: Intel Core i3 o equivalente  
Memoria: 4 GB de RAM  
Gráficos: NVIDIA GeForce GTX 460 o equivalente  
Almacenamiento: 15 GB de espacio disponible  
Tarjeta de sonido: Pulse Audio compatible


Recomendados:


SO: Ubuntu 16.10 o superior (64-bit), SDL 2.0.5 o superior  
Procesador: Intel i5 series o superior  
Memoria: 8 GB de RAM  
Gráficos: NVIDIA GeForce GTX 680 or equivalente (Driver propietario 375.26 o superior)  
Almacenamiento: 15 GB de espacio disponible  
Tarjeta de sonido: Pulse Audio compatible


'Torment: Tides of Numerena' está disponible en español en su página de Steam, en varias opciones de compra empezando por la más básica:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/272270/36640/" width="646"></iframe></div>


¿Qué te parece este 'Torment: Tides of Numerena? ¿Piensas jugarlo? 


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux en [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

