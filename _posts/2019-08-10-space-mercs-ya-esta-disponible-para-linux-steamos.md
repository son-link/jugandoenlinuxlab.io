---
author: Pato
category: "Acci\xF3n"
date: 2019-08-10 14:46:00
excerpt: "<p>Bearded Giant Games <span class=\"css-901oao css-16my406 r-1qd0xha r-ad9z0x\
  \ r-bcqeeo r-qvutc0\">@zapakitul nos trae acci\xF3n espacial a raudales</span></p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/6f09fa9c4773d7976c7311cc7936300c.webp
joomla_id: 1094
joomla_url: space-mercs-ya-esta-disponible-para-linux-steamos
layout: post
tags:
- accion
- indie
- espacio
title: "Space Mercs ya est\xE1 disponible para Linux/SteamOS (Actualizado)"
---
Bearded Giant Games @zapakitul nos trae acción espacial a raudales

**Actualización 2019-08-10**: El juego ya está disponible también en [itch.io](https://zapakitul.itch.io/space-mercs) 


 


Buenas noticias para los que gustan de la acción espacial "masiva". Y hablamos de masiva por que realmente lo es. **Space Mercs** es acción espacial sin contemplaciones donde tendrás que hacerte un hueco con tu nave en batallas campales que pueden ser desde unas pocas decenas de enemigos hasta miles de naves disparándose en un festival de rayos laser.


*"Space Mercs es un juego de combate espacial arcade extremo donde la cantidad de proyectiles y láseres en la pantalla solo es superada por la cantidad de estrellas en el universo. ¿Serás capaz de completar todas las misiones mercenarias y convertirte en el mejor piloto de la galaxia?"*


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/6DCQ6GRmlOc" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Como ya hemos dicho en anteriores ocasiones, este juego está **desarrollado por un solo programador**, por lo que el resultado tiene aún mas mérito. Según sus propias palabras *"A lo largo de la campaña de la misión del juego, esquivarás, perseguirás, dispararás, escoltarás y destruirás absolutamente a enemigos que van desde pequeñas naves no tripuladas hasta enormes acorazados mientras estás sujeto a una sola regla: "Si se mueve, muere"*


Además, Bearded Gian Games [ha anunciado](https://steamcommunity.com/games/1088600/announcements/detail/1616148671798060229) el lanzamiento de un creador de batallas "**Battle Designer**" que va incluido con el juego y que permite configurar batallas masivas para que puedas poner a prueba tu pericia, y de paso tu PC. "Si alguna vez te has preguntado a cuántas naves de un tipo determinado puedes enfrentarte, ¡ahora puedes hacerlo! Configura tu propia batalla personalizada de 1000 naves ligeras contra 200 naves pesadas o haz todo lo posible y simplemente configura una guerra de acorazados: ¡depende de ti!


*"Diseñé este juego porque quería sentir que soy parte de una guerra a gran escala, quería volar alrededor de naves que luchan entre sí y creo que "Battle Designer" acerca el juego a ese objetivo."*


El modo "Battle Designer" se desbloqueará una vez hayamos superado las misiones de la campaña principal del juego.


Si quieres saber mas sobre Space Mercs o comprarlo, lo tienes disponible en su [página de Steam](https://store.steampowered.com/app/1088600/Space_Mercs/) a un precio inmejorable de **solo 8,19€**.


¿Te embarcarás en una batalla espacial contra miles de naves en un reto sin igual? ¿Te gustan las batallas campales en el espacio?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux en [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

