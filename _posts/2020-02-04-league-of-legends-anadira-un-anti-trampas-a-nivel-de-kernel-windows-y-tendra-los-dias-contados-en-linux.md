---
author: Pato
category: Estrategia
date: 2020-02-04 10:45:56
excerpt: "<p>Hasta ahora el juego de Riot Games se pod\xEDa ejecutar sin problemas\
  \ mediante Wine, pero si cumplen lo anunciado esto pronto cambiar\xE1</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/leagueoflegends/Anti_Cheat_Kernel.webp
joomla_id: 1160
joomla_url: league-of-legends-anadira-un-anti-trampas-a-nivel-de-kernel-windows-y-tendra-los-dias-contados-en-linux
layout: post
title: "League Of Legends a\xF1adir\xE1 un anti-trampas a nivel de kernel Windows\
  \ y tendr\xE1 los d\xEDas contados en Linux"
---
Hasta ahora el juego de Riot Games se podía ejecutar sin problemas mediante Wine, pero si cumplen lo anunciado esto pronto cambiará


Hace tan solo unas horas que [saltó la noticia](https://www.reddit.com/r/leagueoflegends/comments/eybl03/new_anticheat_system_in_lol_and_other_upcoming/fghcjjr/?utm_source=share&utm_medium=web2x) de que Riot Games, los responsables de juegos como el archiconocido [**League of Legends**](https://play.euw.leagueoflegends.com/en_GB) están trabajando para implementar un sistema anti-trampas **a nivel del Kernel Windows** tanto para este juego como para todos los que la compañía está desarrollando y que implicará un cambio drástico de cara a su ejecución en nuestro sistema favorito.


Si bien League Of Legends es un juego que no está disponible en Linux de forma oficial al no contar con una versión nativa **hasta ahora sí era posible ejecutar el popular juego gracias a Wine**, la capa de compatibilidad que tantos juegos "no nativos" nos permite disfrutar. De hecho, tan solo en [la web del lanzador](https://lutris.net/games/league-of-legends/) de League Of Legends **en Lutris** aparecen más de 13.000 jugadores, con lo que no es un juego que sea "poco popular" en Linux.


Esta situación está a punto de cambiar a tenor del [anuncio que la compañía ha hecho](https://eune.leagueoflegends.com/en-pl/news/dev/dev-null-anti-cheat-kernel-driver/), y es que **Riot Games está desarrollando un sistema anti-chetos a nivel de kernel Windows** que se sitúa en el mismo nivel que otros anti-chetos "populares" como *Easy Anti Cheat* que al estar actuando a bajo nivel dentro del sistema operativo detectan cuando se modifican archivos o se ejecutan los juegos con sistemas "modificados". Esto impide a los tramposos poder hacer trampas de forma sencilla, y supone un control férreo en la ejecución de los programas de forma que en cualquier momento la compañía responsable puede saber en qué circunstancias se está ejecutando el juego en cuestión. Sin embargo, al mismo tiempo, impide que los juegos protegidos con dichos sistemas puedan ejecutarse con capas de compatibilidad, como puede ser Wine.


Hay que tener en cuenta que **Wine no es Windows** si no una especie de "capa de compatibilidad", y no posee muchas de las características que el Kernel del sistema sí tiene por lo que los juegos que cuentan con anti-chetos normalmente no se pueden ejecutar en nuestro sistema favorito **aunque esté más que demostrado que podemos ejecutarlos sin ningún problema y con mucha solvencia.** Ejemplos tenemos unos cuantos, como Fortnite o Apex Legends por citar solo dos de los más conocidos.


En cuanto al anti-chetos de Riot, no solo están trabajando para implementarlo en League Of Legends si no en todo su catálogo de juegos en desarrollo, por lo que podemos ir despidiéndonos de la posibilidad de jugar a cualquier juego que lance la compañía.


Veremos si algún día conseguimos que las compañías y los sistemas anti-trampas nos tengan en cuenta y permitan la ejecución de los juegos mediante Wine añadiendo esta posibilidad a sus sistemas. Como ya hemos dicho en otros artículos, Valve parece estar en ello con el EAC para Proton. Por otra parte, ya hay voces que apuntan a "*dejar los juegos que no nos tienen en cuenta y apoyar a los que sí soportan nuestro sistema favorito*" pasándose a los juegos de la competencia.


Y tu, ¿Tirarás de dual-boot para jugar al popular juego de Riot Games o te pasarás a [DOTA 2](https://store.steampowered.com/app/570/Dota_2/)?


Cuéntanoslo en mensajes en nuestra comunidad de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).


 

