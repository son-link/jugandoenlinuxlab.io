---
author: Serjor
category: Apt-Get Update
date: 2017-08-04 16:23:34
excerpt: <p>Un resumen refrescante para combatir este calor</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/7a0c9d8bd9eadf879f0011817e8c2396.webp
joomla_id: 425
joomla_url: apt-get-update-veraniego
layout: post
tags:
- aptget
title: Apt-get update --warm --holiday --relax --lotsofgames
---
Un resumen refrescante para combatir este calor

Se nota que el verano ha llegado con él los editores de <jugandoenlinux.com> hemos aprovechado para relajarnos un poco. Eso no quita para que desde nuestra exclusiva isla caribeña no nos acordemos de vosotros y no os traigamos un resumen de lo acontecido en los últimos días.


En esta ocasión os traemos unos benchmarks cortesía de [phoronix](https://www.phoronix.com):


* Seguro que Bill Gates ha empezado a temblar al ver esta [comparativa](https://www.phoronix.com/scan.php?page=article&item=radeonsi-beats-wingl&num=1) entre Windows 10 y Ubuntu con el kernel 4.13 y mesa 17.2
* La aparición de los nuevos Ryzen 3 han dado mucho de que hablar, y podemos ver porque [aquí](https://www.phoronix.com/scan.php?page=article&item=radeonsi-beats-wingl&num=1)
* Los drivers libres de AMD [plantan cara](https://www.phoronix.com/scan.php?page=article&item=amdgpu-radeonsi-384&num=1) a NVIDIA en su terreno


En [reddit](https://www.reddit.com/) leemos cosas tan interesantes como uno de los motores de videojuegos más queridos por los linuxeros, godot, ha añadido el soporte para [glTF 2.0](https://www.reddit.com/r/godot/comments/6rj953/why_we_should_all_support_gltf_20_as_the_standard/?ref=share&ref_source=link), un estándar que se usa para importar y exportar modelos gráficos, sobretodo 3D, y liberado hace unos meses por Khronos Group, los responsables de entre otros Vulkan, OpenGL, SPIR-V... y todas esas siglas que hacen a Tux jugar feliz.


![https://godotengine.org/storage/app/media/dh.jpg](https://godotengine.org/storage/app/media/dh.jpg)


En [linuxgamenews.com/](http://linuxgamenews.com/) nos encontramos con:


* [Interplanetary: Enhanced Edition](http://linuxgamenews.com/post/163765546109/interplanetary-enhanced-edition-launches-linux) debuta en Linux. Estrategia interplanetaria por turnos.   
 <div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/t_9o9MWO20g" width="560"></iframe></div>
* [Slime Rancher](http://linuxgamenews.com/post/163727020045/slime-rancher-officially-releases-with-a-discount) hace su aparición oficial, y con descuento hasta el 8 de agosto   
 <div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/jDZUhN8pU9c" width="560"></iframe></div>
* [The Long Dark](http://linuxgamenews.com/post/163688248760/the-long-dark-launches-first-two-episodes) sale de Early Access y lo hace con los dos primeros capítulos de la primera temporada, y sí, también con un descuento hasta el 8 de agosto.   
 <div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/syoDWIt9yZY" width="560"></iframe></div>


En [gamingonlinux.com](https://www.gamingonlinux.com) nos encontramos con estos interesantes artículos:


* La gente detrás de [Bounty Battle](https://www.gamingonlinux.com/articles/bounty-battle-a-2d-fighting-game-that-sounds-like-an-awesome-indie-game-character-mashup.10038), un brawl con personajes de diferentes juegos indie, están a punto de superar su campaña de croudfunding y han prometido versión para Linux. No dudes en echarles una mano si el juego te interesa.   
 <div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/M0Xe9nzdkxU" width="560"></iframe></div>
* [Pressure Overdrive](https://www.gamingonlinux.com/articles/drive-a-customizable-steambuggy-in-pressure-overdrive-now-on-linux.10053) nos promete diversión a raudales conduciendo un buggy customizado por nosotros con el que tendremos que disparar a todo lo que se mueva.   
 <div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/rJaEs5GrxM8" width="560"></iframe></div>
* Los chicos de Double Fine nos traen [Full Throttle Remastered](https://www.gamingonlinux.com/articles/full-throttle-remastered-rides-onto-linux.10059)   
 <div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/cNFkmjC6-aI" width="560"></iframe></div>
* Y este debe ser el mes de los motores para videojuegos, porque Epic se suma a la moda de hacer anuncios y [nos dice](https://www.gamingonlinux.com/articles/epic-games-looking-to-make-vulkan-the-default-api-for-linux-games-in-unreal-engine.10088) que intentarán que en Linux los juegos usen siempre Vulkan
* Además los chicos de GOL en su canal de YouTube nos traen una [preview](https://www.gamingonlinux.com/articles/sudden-strike-4-a-short-teaser-video-of-it-running-on-linux.10096) de lo que será el próximo Sudden Strike 4 corriendo bajo Linux.   
 <div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/YUk2gz8oPe4" width="560"></iframe></div>


Por si os perdisteis os recomendamos que le echéis un vistazo a la entrevista que hizo la gente de [Boiling Steam](http://boilingsteam.com/icculus-ryan-gordon-tells-us-everything-part-1/) a [Icculus](https://twitter.com/icculus) (Ryan Gordon), el ya famoso porter de juegos a Linux y uno de los principales desarrolladores de la librería SDL a la que le debemos una inmensidad de videojuegos, y no solo en Linux. También podéis escucharlo [aquí](http://boilingsteam.com/wp-content/uploads/BoilingSteam-Podcast-7.ogg)   

Your browser does not support the`audio`element.



Y por ahora esto esto todo, nos volvemos a nuestra hamaca a seguir disfrutando del verano ;-)


Y tú, ¿cómo piensas pasar este verano? ¿Trabajando? ¿De vacaciones? ¿Disfrutando con tus juegos favoritos? Coméntanos cuáles son tus recomendaciones estivales en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD)

