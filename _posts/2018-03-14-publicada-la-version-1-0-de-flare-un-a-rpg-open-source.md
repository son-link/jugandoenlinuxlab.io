---
author: Serjor
category: Rol
date: 2018-03-14 22:33:57
excerpt: "<p>Incluye un modo campa\xF1a y soporte para mods</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/c2521bfee169c226099b9168e0b93ff0.webp
joomla_id: 680
joomla_url: publicada-la-version-1-0-de-flare-un-a-rpg-open-source
layout: post
tags:
- open-source
- flare
- arpg
title: "Publicada la versi\xF3n 1.0 de Flare, un A-RPG open source"
---
Incluye un modo campaña y soporte para mods

Leemos en [La mirada del replicante](http://lamiradadelreplicante.com/2018/03/13/flare-1-0-interesante-juego-de-rol-open-source/), que Flare, un juego open source con similitudes al clásico Diablo, ha lanzado la versión 1.0 después de 4 años de desarrollo.


Podéis leer las notas sobre el lanzamiento [aquí](http://flarerpg.org/index.php/2018/03/12/flare-1-0/), y podéis encontrar los enlaces de descarga para las distintas distros y sistemas [aquí](http://flarerpg.org/index.php/download/)


Hemos probado brevemente el juego y por lo que hemos podido observar se puede jugar con ratón y teclado o mando, pero el soporte de este último es un poco limitado. Por ejemplo al atacar el personaje se gira siempre hacia la cámara, que es isométrica por cierto, por lo que si tenemos un enemigo detrás nuestro no podremos atacarlo y tendríamos que bordearlo para ponerlo entre nosotros y la cámara. Así mismo para recoger los ítems y usar las pociones hace falta usar el teclado y ratón, así que esperemos que solucionen este punto, porque el movimiento y el ataque, al margen del tema de la cámara, es muy cómodo.


El juego nos da opción a ponerlo en castellano, aunque por lo que hemos podido comprobar, por el momento solamente están disponibles en nuestro idioma los textos de los menús. No obstante, al ser un proyecto open source podemos colaborar en la traducción del juego, ya que podemos encontrar el código fuente en [GitHub](https://github.com/clintbellanger/flare-game)


No hemos visto la opción multiplayer, pero sería un añadido muy interesante.


De todos modos, al margen de todo esto, al menos en el estado actual el juego ha funcionado sin problemas, y para ser un proyecto de código abierto, texturas a parte, no tiene nada que envidiar a juegos como Torchlight o Victor Vran, que son más grandes y con mejor apartado visual, pero en general el juego es más que correcto y la prueba rápida ha sido sorprendentemente positiva, por lo que no descartéis que hagamos un directo en Twitch y YouTube para comentar las bondades del juego mientras jugamos.


Además, el juego es altamente moddable. Tanto es así que la historia del juego es un mod en sí mismo, y esto abre las puertas a expansiones e historias desde la comunidad, haciendo que flare pase de ser un juego a una plataforma.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/OLrUYYulcss" style="border: 0px;" width="560"></iframe></div>


Y a tú, ¿qué opinas de este Flare? Cuéntanoslo en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

