---
author: Pato
category: Hardware
date: 2022-12-01 10:03:00
excerpt: "<p style=\"text-align: justify;\">El inter\xE9s por el evento donde se premian\
  \ los mejores videojuegos de cada categor\xEDa sube varios pelda\xF1os</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Steamdeck/steamdeckGameAwards.webp
joomla_id: 1512
joomla_url: valve-sorteara-una-steam-deck-por-minuto-durante-the-game-awards
layout: post
tags:
- steam-deck
- the-game-awards
title: "Valve sortear\xE1 una Steam Deck por minuto durante \"The Game Awards\""
---
El interés por el evento donde se premian los mejores videojuegos de cada categoría sube varios peldaños


Para los que no lo conozcan, el evento "[The Game Awards](https://thegameawards.com)" es un evento en el que se premian los juegos nominados entre la comunidad que han sido lanzados durante el último año en cada una de las categorías. Bueno, eso y un buen puñado de anuncios sobre los próximos lanzamientos que vendrán en las siguientes temporadas, por lo que normalmente y aunque estés muy interesado en los videojuegos se hace largo sinceramente.


Si habéis seguido las recientes [rebajas de otoño de Steam](https://store.steampowered.com/news/app/593110/?emclan=103582791457287600&emgid=3475118496195190446) habréis podido ver que se podía votar por los juegos que quisiéramos que fuesen premiados en dicho evento. Pues bien, ahora Valve quiere promocionar el éxito de su Steam Deck (que [llegará a varios países de Asia en próximas fechas](https://steamdeck.komodo.jp/product/steam-deck/?lang=en)) y no contenta con los altos números de ventas de la máquina, **ahora va a regalar una Steam Deck de 512GB** cada minuto que dure el evento. Ahí es nada.


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/AvokyBOYe8Y" title="YouTube video player" width="560"></iframe></div>


 "The Game Awards" se llevará a cabo **el próximo viernes 9 de Diciembre** (en la web del evento ponen el día 8, pero la diferencia horaria hace que en España sea ya 9) **a las 2 AM hora peninsular.** Son horas intempestivas, es cierto, pero una Steam Deck puede estar esperándote. ¿Te merece la pena?


Si quieres probar suerte a ver si te toca, tienes que cumplir ciertos requisitos: 


* Para poder optar al sorteo debes residir en Estados Unidos, Canadá, Reino Unido o la Unión Europea.
* Debes haber realizado una compra en Steam entre el 14 de Diciembre de 2021 y el 14 de Diciembre de 2022.
* Tu cuenta no puede estar limitada.
* Debes registrarte para el evento en Steam [en este enlace](https://store.steampowered.com/sale/thegameawardssteamdeckdrop2022).
* Tienes que visualizar el evento en directo en Steam. Los premiados se irán anunciando en el chat del streaming durante el evento.


También se podrá seguir el evento en directo en <https://steam.tv>


Además, todos los que os apuntéis para ver el evento recibiréis una pegatina digital exclusiva para vuestro perfil de Steam.


Puedes ver el anuncio oficial en el post oficial en Steam [en este enlace](https://store.steampowered.com/sale/thegameawardssteamdeckdrop2022).


¿Vas a seguir en directo el "The Game Awards"? **¿te gustaría seguirlo con nosotros?** (¿un chat paralelo... quizás?)


Cuéntame que te parece, y sobre todo, cuando haya pasado el evento cuéntanos si te ha tocado una Steam Deck en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

