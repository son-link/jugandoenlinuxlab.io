---
author: Pato
category: Apt-Get Update
date: 2017-02-10 17:40:54
excerpt: <p>Terminamos la semana como es habitual</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/bdc7d0f30e0c420b2ac279d6a1c096e4.webp
joomla_id: 215
joomla_url: apt-get-update-bleed-2-quetoo-killing-floor-2-cossacks-3-sudden-strike-4
layout: post
title: Apt-Get Update Bleed 2 & Quetoo & Killing Floor 2 & Cossacks 3 & Sudden Strike
  4...
---
Terminamos la semana como es habitual

Otra semana que se acaba, y como siempre hay un buen puñado de cosas que se nos quedaron por el camino sin poder publicar, pero ahora aprovechamos para repasarlas y poneros al día de todo lo que se ha movido en torno a los juegos en Linux:


Desde [www.gamingonlinux.com:](http://www.gamingonlinux.com)


- Sudden Strike, el RTS de Kalipso Media llegará a Linux próximamente. Puedes verlo [en este enlace](https://www.gamingonlinux.com/articles/sudden-strike-4-looks-like-an-amazing-rts-that-will-have-linux-support.9048).


- Mesa ha protagonizado un buen culebrón con algunos desarrolladores diciendo que haría falta meter el multihilo en los drivers OpenGL, y otros diciendo que eso introduce un buen puñado de problemas. La cuestión es que para algunos juegos esto supone un incremento considerable de rendimiento pero por otra parte supondría un empeoramiento en otros juegos que no lo soportasen. Por su parte Valve estaría considerando hacer una "lista blanca" con el fin de dar soporte a los juegos que necesitasen "multihilo". Tienes los enlaces [aquí](https://www.gamingonlinux.com/articles/mesa-developer-marek-is-looking-to-finally-sort-out-threaded-gl-dispatch.9057), [aquí](https://www.gamingonlinux.com/articles/threaded-opengl-in-mesa-will-not-help-ferals-linux-ports-and-probably-others-too.9064) y [aquí](https://www.gamingonlinux.com/articles/getting-the-threaded-gl-dispatch-code-into-mesa-is-causing-some-issues-valve-might-use-a-white-list.9089).


- Killing Floor 2 estaría siendo portado por Knockout Games, aunque no hay anuncio oficial. Puedes verlo [en este enlace](https://www.gamingonlinux.com/articles/looks-like-killing-floor-2-may-be-coming-to-linux-from-knockout-games.9059).


- Quetoo, el FPS open source antes conocido como 'Quake2World' estaría en fase beta y en campaña de Greenlight para su lanzamiento en Steam. Puedes verlo en este enlace.


- Bleed 2, el juego de acción rápida ya está disponible en Linux gracias a Ethan Lee. Puedes verlo [en este enlace](https://www.gamingonlinux.com/articles/bleed-2-a-relentless-2d-action-game-is-now-on-linux-ported-by-ethan-lee.9074).


- The Dark Mod, un juego a lo 'Thief' está en campaña de Greenlight para su lanzamiento. Puedes verlo [en este enlace](https://www.gamingonlinux.com/articles/the-dark-mod-the-free-stealth-game-inspired-by-thief-is-looking-to-get-on-steam.9080).


- Day of Infamy el juego de acción en la 1ª GM recibe un buen pulido. Tienes la información y un vídeo gameplay [en este enlace](https://www.gamingonlinux.com/articles/day-of-infamy-the-wwii-fps-has-a-huge-update-with-new-maps-performance-fixes-and-more.9088).


Desde [www.linuxgameconsortium.com](http://www.linuxgameconsortium.com):


- Cossacks 3 estará disponible en Linux y SteamOS el próximo día 15 de Marzo. Puedes verlo [en este enlace](https://linuxgameconsortium.com/linux-gaming-news/cossacks-3-gets-steam-workshop-support-linux-release-date-47348/).


- River City Ransom llegará a Linux tras su lanzamiento en Windows pero sin fecha determinada. Tienes la información [en este enlace](https://linuxgameconsortium.com/linux-gaming-news/river-city-ransom-underground-release-date-trailer-linux-comes-later-47418/).


- Xcom 2 Recibe soporte para el mod 'The Long War' en Linux. Puedes verlo [en este enlace](https://linuxgameconsortium.com/linux-gaming-news/xcom-2-long-war-2-mod-gets-linux-support-feral-interactive-47634/).


- Expeditions Viking, el juego de estrategia en tiempo real llegará a linux el 27 de Abril. Puedes verlo [en este enlace](https://linuxgameconsortium.com/linux-gaming-news/expeditions-viking-turn-based-strategy-rpg-official-release-date-linux-mac-pc-47650/).


 


A continuación los vídeos destacados de la semana:


Bleed 2:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/kUloJK7nw7g" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


 Nuevo trailer de Torment Tides of Numerena:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/qVAMuk-CV18" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


 Sudden Strike 4:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/nHroEoNuVS0" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


 


¿Que te ha parecido el Apt-Get Update de hoy? Seguro que me he dejado muchas cosas en el tintero. 


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

