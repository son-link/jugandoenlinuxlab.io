---
author: leillo1975
category: Rol
date: 2019-05-03 13:48:53
excerpt: "<p>Rol y Acci\xF3n son las se\xF1as de identidad <span class=\"username\
  \ u-dir\" dir=\"ltr\">@LastEpoch</span></p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/380d25d8d08b30f6a93b74397bf8e996.webp
joomla_id: 1037
joomla_url: last-epoch-entra-en-acceso-anticipado-lanzando-una-beta
layout: post
tags:
- accion
- acceso-anticipado
- beta
- rpg
- last-epoch
- eleventh-hour-games
title: '"Last Epoch" entra en Acceso Anticipado lanzando una beta.'
---
Rol y Acción son las señas de identidad @LastEpoch

Si llevais siguiendonos un tiempo, quizás recordeis que hace un año [os hablábamos de este juego en nuestra web](index.php/homepage/generos/rol/7-rol/841-last-epoch-un-juego-de-rol-y-accion-busca-apoyos-en-kickstarter), en aquella ocasión para anunciaros que [el juego buscaba financiación en Kickstarter](https://www.kickstarter.com/projects/lastepoch/last-epoch/). Tras **superar la cifra de los 210.000$** que se proponían en un principio [Eleventh Hour Games](https://lastepoch.gamepedia.com/Eleventh_Hour_Games), el equipo de desarrollo de [Last Epoch](https://lastepochgame.com/), se ha puesto manos a la obra, y **hace unos días entraba en fase de Acceso Anticipado**, anunciando también el **lanzamiento de una beta**.


Para quien no conozca este juego hay que decir que encontrará un **juego de Rol de Acción al estilo Diablo o Torchlight**, donde tendremos que masacrar a hordas de enemigos y hacer acopio de multitud de objetos que encontraremos en nuestra aventura. Como veis, la temática es garantía de éxito. Las características que encontrareis en este juego según Steam serán las siguientes:


  
*Last Epoch combina viajes en el tiempo, emocionantes mazmorras, una apasionante personalización de personajes y una capacidad de repetición sin fin para crear un RPG de acción tanto para veteranos como para recién llegados. Viaja a través del mundo del pasado de Eterra y enfréntate a oscuros imperios, dioses iracundos y salvajes e inmaculados, para encontrar la forma de ahorrar tiempo del Vacío.*


***Características principales***


*-15 Clases de Maestría*  
 *Comienza tu aventura como una clase base que luego se puede especializar en una de las tres Clases de Maestría. Al especializarte en una determinada maestría podrás acceder a nuevas habilidades y especializarte en tu estilo de juego!*  
 *-Personaliza cada habilidad*  
 *Cada habilidad activa tiene su propio árbol de aumento que puede cambiar completamente el funcionamiento de la habilidad. Transforma tus esqueletos en arqueros, tus relámpagos en relámpagos de cadena, o haz que tu serpiente golpee a las serpientes para que luchen a tu lado!*  
 *-Piérdete en la búsqueda de objetos*  
 *Llena tu arsenal de objetos mágicos que crearas a la perfección, cambia las reglas de tu creación con poderosos objetos únicos y fijos, y siempre tendrás esa próxima actualización en el horizonte con el sistema de botín aleatorio de Last Epoch.*  
 *-Explora un vasto mundo a través del tiempo*  
 *El mundo de Eterra es el hogar de muchas facciones y secretos. Viaja a diferentes puntos en el tiempo para cambiar el destino del mundo, y lucha para ponerlo en un nuevo camino.*  
 *-Rejugabilidad sin fin*  
 *Con una gran cantidad de clases y habilidades para personalizar, sistemas de juego profundos, botín aleatorio y desarrollo continuo, Last Epoch es un juego que te hará volver.*  
 *-Fácil de aprender, difícil de dominar*  
 *Estamos comprometidos a hacer nuestro juego accesible a través de la ruptura del aprendizaje requerido y ser transparentes. Al mismo tiempo, la superación de algunos de los contenidos más desafiantes requerirá un conocimiento profundo y llevar su construcción a sus límites.*


También debes saber que **comprar el juego en estado Beta te dará acceso al "Ancient Gladiator Supporter Pack"** que te dará las siguientes ventajas:


*-Acceso a Last Epoch Beta y Full Release (Beta estará en directo hasta el lanzamiento completo)*  
 *-"Juvenille Skullen cosmetic companion" para seguirte a través del mundo de Eterra.*  
 *-Insignia del foro de Ardent Gladiator que muestra su apoyo inicial*  
 *-350 Cosmetic Coins para comprar artículos de cosmética en el juego al salir a la venta*  
 *-Banda sonora digital completa en el lanzamiento*


Es importante reseñar que **los jugadores que actualmente posean una copia del juego pueden vincular sus cuentas de Last Epoch a Steam y también canjear su clave** a través de un sencillo proceso de sincronización de cuentas. Si quieres comprar Last Epoch puedes hacerlo en [Steam](https://store.steampowered.com/app/899770/Last_Epoch/) por tan solo 30$, un muy buen precio teniendo en cuenta las horas de diversión que suelen proporcionar este tipo de juegos. Ahora te dejamos en compañía del fantástico trailer de esta beta:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/IA4auxVRQYw" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


¿Conocías ya Last Epoch? ¿Te gustan este tipo de juegos de Rol tipo diablo? Cuéntanoslo en los comentarios o charla sobre el juego en nuestro canal de [Telegram](https://t.me/jugandoenlinux).

