---
author: Jugando en Linux
category: Apt-Get Update
date: 2022-03-25 13:53:04
excerpt: ''
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ShatteredPixelDungeon/shattered_pixel_dungeon.webp
joomla_id: 1452
joomla_url: nuevos-juegos-con-soporte-nativo-25-03-2022
layout: post
title: Nuevos juegos con soporte nativo 25/03/2022
---

Otra nueva selección de las novedades y ofertas que pueden interesaros con soporte nativo:


 


### Slidetracked


Desarrolador: Axis Aligned Games


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube.com/embed/0T4Fyvko7dc" title="YouTube video player" width="720"></iframe></div>


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/1836680/" style="border: 0px;" width="646"></iframe></div>


 




---


### Solares


Desarrolador: Orbit Games


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube.com/embed/rMQlLIN32rE" title="YouTube video player" width="720"></iframe></div>


**[Solares en Itch a 1,49$ (Disponible demo gratuita)](https://orbit-games-inc.itch.io/solares)**


 




---


 


### Shattered Pixel Dungeon


Desarrolador: Shattered Pixel Dungeon


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube.com/embed/_w9uRX4_Vj0" title="YouTube video player" width="720"></iframe></div>


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/1769170/" style="border: 0px;" width="646"></iframe></div>


 




---


### EN OFERTA


### Paint the Town Red


Desarrolador: South East Games


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube.com/embed/dOzCeL_sV3Q" title="YouTube video player" width="720"></iframe></div>


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/337320/" style="border: 0px;" width="646"></iframe></div>


 




---


### Steel Assault


Desarrolador: Zenovia Interactive


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube.com/embed/fMJQopDAvsI" title="YouTube video player" width="720"></iframe></div>


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/1280300/" style="border: 0px;" width="646"></iframe></div>


 




---

