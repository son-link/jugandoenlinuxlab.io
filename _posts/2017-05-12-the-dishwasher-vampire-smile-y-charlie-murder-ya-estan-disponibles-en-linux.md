---
author: Pato
category: "Acci\xF3n"
date: 2017-05-12 17:29:04
excerpt: "<p>Los ports son obra del conocido programador Ethan Lee que ya nos ha traido\
  \ otros t\xEDtulos a Linux</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/2ee6f501a6569cf213987ad1c50a88aa.webp
joomla_id: 319
joomla_url: the-dishwasher-vampire-smile-y-charlie-murder-ya-estan-disponibles-en-linux
layout: post
tags:
- accion
- indie
- aventura
title: "'The Dishwasher: Vampire Smile' y 'Charlie Murder' ya est\xE1n disponibles\
  \ en Linux"
---
Los ports son obra del conocido programador Ethan Lee que ya nos ha traido otros títulos a Linux

Ya informábamos de que estos dos juegos estaban siendo portados por Ethan Lee (que ya nos trajo [Salt and Sanctuary](http://store.steampowered.com/app/283640/Salt_and_Sanctuary/) a Linux) [hace ya algún tiempo](https://www.jugandoenlinux.com/index.php/homepage/generos/accion/item/101-ethan-lee-esta-portando-the-dishwasher-y-charlie-murder-a-linux), y hoy por fin los tenemos ya disponibles. El propio Ethan nos lo ha anunciado en un tweet:



> 
> Charlie Murder + The Dishwasher: Vampire Smile are now available for PC: <https://t.co/z3WLKVxEGm>
> 
> 
> — Ethan Lee (@flibitijibibo) [May 12, 2017](https://twitter.com/flibitijibibo/status/863077918033018880)



 


Se trata de dos juegos de Ska Studios con su sello personal, con mucho gore, sangre, vísceras y temática oscura.


**Sobre 'The Dishwasher: Vampire Smile':**



> 
> *¡Quedan tres marcas! Desbloquea The Dishwasher: Vampire Smile para adentrarte aún más en la sangrienta sed de venganza de Yuki. La versión completa del juego posee la línea de dialogo de la campaña de Dishwasher, modo cooperativo el línea y local, 50 desafíos Arcade y el Desafío Dish, que incluye toneladas de terribles armas nuevas y técnicas.*
> 
> 
> 


A continuación, vídeo no apto para todas las sensibilidades:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/c1NJoEJQ7RE" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


 


**Sobre 'Charlie Murder':**



> 
> *Es un apocalipsis de punk rock! Une fuerzas con amigos y lidera la banda de punk rock Charlie Murder en su épica misión para salvar el mundo de sus archienemigos metaleros Gore Quaffer y sus legiones de malvados seguidores. Disfruta del mejor RPG saqueando botines para mejorar a tu personaje, gana fans para desbloquear poderosas armas y movimientos de equipo, consigue tatuajes para canalizar tu Anark-ki-a y destierra misteriosas reliquias.*
> 
> 
> 


A continuación vídeo no apto para todas las sensibilidades:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/ZwFC_FoTpDA" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


 


Los dos juegos vienen con todas sus características originales, multijugador local y en línea, y están traducidos al español. Viniendo el port de quien viene se les supone una calidad excelente en cuanto a rendimiento, exigiendo poco equipo para funcionar. En recomendados tenemos:


SO: glibc 2.15+, 32/64-bit  
Procesador: Intel Core 2 Duo o AMD Athlon 64 X2 5600+  
Memoria: 2 GB de RAM  
Gráficos: OpenGL 3.2+   
Almacenamiento: 2 GB de espacio disponible  
Notas adicionales: Controladores SDL totalmente soportados


Si te van los juegos de este estilo, puedes comprarlos en sus respectivas páginas de Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/268990/" width="646"></iframe></div>


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/405290/" width="646"></iframe></div>


 


¿Qué te parecen estos juegos? ¿Te gustaría echar unas partidas a alguno de ellos?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 

