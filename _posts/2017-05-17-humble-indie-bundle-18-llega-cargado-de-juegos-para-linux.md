---
author: Pato
category: Ofertas
date: 2017-05-17 19:01:57
excerpt: <p>Paga lo que quieras y aporta para la caridad</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/fc2062aef352f80dc2215f346ba9ce28.webp
joomla_id: 326
joomla_url: humble-indie-bundle-18-llega-cargado-de-juegos-para-linux
layout: post
tags:
- humble-bundle
- ofertas
title: '''Humble Indie Bundle 18'' llega cargado de juegos para Linux'
---
Paga lo que quieras y aporta para la caridad

A estas alturas creo que todos conocéis lo que es Humble Bundle, y como nunca vienen mal unas ofertas a las que hincarle el diente, ahora nos traen en su edición 18 un buen puñado de juegos "indies" con soporte en nuestro sistema favorito.


En concreto, si pagamos lo que queramos nos ofrecen **Ziggurat**, **Windward** y **Steamworld Heist**.


Si pagamos mas de la media, que en el momento de escribir estas líneas está en 6,76€, además de los anteriores nos darán **Kentucky Route Zero**, **Beholder**, **Goat Simulator GOATY Edition**, y además las sorpresas que vayan anunciando próximamente conforme vaya pasando el tiempo.


Por último, si pagamos 11,76€ o más, nos haremos con **Owlboy**... tentador ¿no?


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/0N4lp01tFwg" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


 


Podéis ver este Humble Indie Bundle 18 [en su página web](https://www.humblebundle.com/humble-indie-bundle-18).


Como ya sabéis, cuando compráis un Humble Bundle tenéis la posibilidad de decidir a donde se destina vuestro dinero, si a los desarrolladores, a Humble Bundle o a asociaciones benéficas. En este caso a "Electronic Frontier Foundation", a "Child's Play Charity" o a otra de vuestra elección.


No lo dejéis escapar!

