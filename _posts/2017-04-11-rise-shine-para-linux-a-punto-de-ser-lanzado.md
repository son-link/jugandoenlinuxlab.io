---
author: leillo1975
category: Arcade
date: 2017-04-11 15:53:11
excerpt: "<p>El atrevido arcade del estudio espa\xF1ol Super Mega Team est\xE1 a punto\
  \ de aterrizar en nuestro sistema</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/bc61c35998920c79a57e03ad91265e8b.webp
joomla_id: 287
joomla_url: rise-shine-para-linux-a-punto-de-ser-lanzado
layout: post
tags:
- rise-shine
- super-mega-team
title: Rise & Shine para Linux a punto de ser lanzado
---
El atrevido arcade del estudio español Super Mega Team está a punto de aterrizar en nuestro sistema

Hace casi 3 meses, mi colega Pato [nos daba la exclusiva](index.php/homepage/generos/accion/item/279-super-awesome-hyperdimensional-mega-team-esta-investigando-soportar-rise-and-shine-en-linux-steamos) de que el estudio [Super Mega Team](http://supermegateam.com/) estaba estudiando la posibilidad de traer a nuestra plataforma su original propuesta, poco después actualizariamos informando que el juego  finalmente llegaría a nuestros escritorios. Hace escasos minutos [acaban de confirmar en los foros de Steam](http://steamcommunity.com/app/347290/discussions/0/364041776192513286/?ctp=3#c135514151378677243) que el juego está listo y están esperando tener la confirmación por parte de su editor **Adult Swim Games** para que de luz verde a la salida oficial.


 


Para quien no lo conozca, Rise & Shine es, como bien comenta **Pato** un "Run & Gun con caracteristicas novedosas" como plataformas y rompecabezas. La mejor forma que os hagais una idea de lo que es, es que veais el video de su trailer:


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/O6AOqzqjyDg" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Esta es la descripción oficial del juego:



> 
> **Dispara rápido, piensa más rápido**  
> Rise & Shine es un auténtico juego de disparos "pero con cabeza" que combina elementos de los shooters de las recreativas, los infiernos de balas y los plataformas con rompecabezas, creando así una combinación de estrategia y vísceras alucinante. Elige entre los diversos complementos de Shine para resolver rompecabezas que redefinen los posibles usos que se le pueden dar a una bala. Guía los proyectiles por laberintos intrincados, electrifica equipamiento dañado para descubrir rutas nuevas y haz malabares con frutas de 8 bits porque... ¿por qué no?
> 
> 
> **Prepárate para reaparecer**  
> No permitas que los diseños de los adorables personajes te engañen; salvar a Gamearth no va a ser fácil. Tendrás que lanzar ráfagas de misiles y esquivar trampas mientras luchas para evitar la destrucción del planeta. Vas a tener que poner toda la carne en el asador para sobrevivir mientras te enfrentas a flotas de robots letales, jefes de gran tamaño, ruedas mortales gigantescas y (por supuesto) zombis con ganas de mascar cerebro. Chaval, no vas a salir de esta de una sola pieza.
> 
> 
> **Detalles de primera clase**  
> Participa en un mundo donde todos los niveles son auténticas obras de arte. Cada uno de ellos se ha creado con ilustraciones diseñadas a mano con múltiples capas de desplazamiento lateral, por lo que cada paso que des será completamente distinto. ¡Nada de imágenes repetidas! Todos los detalles se han cuidado al máximo para que cada plano fluya de forma impecable; con esta calidad desearás que el juego no se acabe nunca.
> 
> 
> 


Si en cuanto esté disponible, quereis comprar este fantastico título podeis pasaros por la tienda en Steam:


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/347290/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


Nosotros actualizaremos en cuanto esté disponible oficialmente, por lo que os recomendamos que esteis atentos si quereis haceros con él. Si quieres dejar cualquier impresión sobre este juego puedes escribir un comentario o usar nuestros grupos de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

