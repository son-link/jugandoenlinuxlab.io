---
author: Serjor
category: "An\xE1lisis"
date: 2017-04-18 17:25:00
excerpt: "<p>Despu\xE9s de la aparici\xF3n del Hitman GO para Linux, Feral nos trae\
  \ la versi\xF3n adulta del Agente 47.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/fb5c27012bf7bd2b528a5fe03304bd91.webp
joomla_id: 245
joomla_url: analisis-hitman
layout: post
tags:
- analisis
- hitman
title: "An\xE1lisis: Hitman"
---
Después de la aparición del Hitman GO para Linux, Feral nos trae la versión adulta del Agente 47.

Disclaimer: La clave de juego ha sido proporcionada por [Feral Interactive](http://www.feralinteractive.com/es/)


Tras su aparición para Windows en marzo del 2016, y como ya [comentamos](index.php/homepage/generos/accion/item/345-hitman-ya-disponible-en-linux-steamos) el día de su lanzamiento el pasado mes de febrero, Hitman hizo su aparición para el sistema del pingüino, y tras probarlo os traemos este análisis de lo que nos hemos encontrado.


En primer lugar hay que decir que estamos ante la temporada uno del juego, y que esta se divide en diferentes episodios. Si bien podemos hacernos con la primera temporada al completo, también podemos hacernos con el "Intro pack" donde podremos llevar a cabo unas misiones de entrenamiento y el primer episodio como tal a un precio más reducido.


Cuando empezamos la partida, el juego lanza una intro en la que nos dan un poco de background, donde vemos cómo el agente 47 llega a las instalaciones de la ICA, y nos piden que llevemos a cabo una misión de entrenamiento para que mostremos nuestras aptitudes. En esta misión, situada en un falso escenario, debemos eliminar a un objetivo designado sin que nadie se de cuenta, y para ello, como es habitual en este tipo de juegos, y como lo será a lo largo de la evolución de este Hitman, tenemos diferentes caminos para llevar a cabo el encargo.


 ![Uno que lo va a pasar mal...](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisHitman/objetivo.webp "Uno que lo va a pasar mal...")


 


Entre sus puntos fuertes el juego hace gala de una variedad enorme de opciones, ya sea para llegar hasta el objetivo, como para cumplir con nuestro cometido. Según vayamos andando por el escenario podemos oír conversaciones que nos proporcionarán pistas para poder avanzar, o incluso nos encontraremos con lo que el juego llama "oportunidades". Estas oportunidades son secuencias de acciones que deberemos ir siguiendo y que nos acercarán notablemente al destino, dándonos así vías de entrada preestablecidas, como por ejemplo hacerse con el vestuario de un personaje, ir hasta cierto punto, allí hablar con otro o conseguir cierto item para luego... y así hasta llegar al final de la oportunidad.


Además de estas opciones que podremos seguir o no según queramos, tenemos también diferentes técnicas para acabar con los enemigos, desde el famoso cable para estrangular, hasta armas de fuego, venenos... No nos faltarán herramientas para hacer lo que mejor se nos da, que es matar, además de que contamos con una habilidad, que nos permite detectar donde están nuestros enemigos, donde están los objetivos, quién nos puede descubrir en función del disfraz que llevamos... Muy similar a la vista detective de Batman.


 


![modo detective](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisHitman/modo_detective.webp "Soy Batman")


 


Pero a pesar de esta variabildad el juego peca de monótono, donde se premia la paciencia. El juego se basa constantemente en la misma premisa una y otra vez. Busca una manera de entrar, normalmente disfrazado de alguien, para ello tendrás que quedarte a solas con el personaje, hacerte con su vestuario, ocultar el cuerpo y pasar desapercibido hasta que llegues a una zona donde puedas cumplir con tu misión, para después salir sin levantar sospechas.


Esta es su mayor virtud y mayor debilidad, donde salvo que queramos explorar las diferentes oportunidades, la rejugabilidad es muy limitada, sobretodo porque al más mínimo error los NPC nos reconoceran, nos seguirán y nos descubriran, así que comenzaremos las misiones, ya sea desde el inicio o desde los puntos de guardado, varias veces (o al menos en mi caso, ha sido así), por lo que el interés en volver a completar una misión se va desvaneciendo.


No obstante, si el planteamiento del juego nos gusta, tenemos la oportunidad de rejugar todas las misiones pero cambiando la posición desde la que partimos, aunque esto solamente se nos permitirá si tenemos la experiencia necesaria acumulada, además también podremos seleccionar el inventario con el que empezaremos las misiones e incluso si queremos que la agencia nos deje en algún lugar escondido algún otro arma que más tarde podremos recoger si nos interesa. Esto da una cierta sensación de variedad al poder cambiar el planteamiento inicial de la misión.


![Editor de posiciones iniciales](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisHitman/posiciones_inicio.webp "Editor de posiciones iniciales")


 


Así mismo, el juego se actualiza periodicamente con nuevas misiones secundarias que van al margen de la historia principal, ocurren en paralelo a la misma y transcurren en los mismos escenarios, aunque eso sí, cada misión se debe realizar por separado, no pudiendo cumplir la misión principal y una secundaria a la vez.


![Misiones secundarias](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisHitman/secundaria.webp "Informe preeliminar")


 


Mecánicas del juego a parte, el port para Linux es muy sólido. Durante las horas de juego acumuladas no he tenido ningún problema técnico realmente destacable. El control, que en mi caso ha sido con un Steam Controller, no me ha dado ningún problema, tampoco he tenido ningún tipo de cuelgue o caídas de frames, aunque sí que hay que destacar dos pequeños fallos gráficos, uno es el que acompaña a estas letras y otro es el que podéis ver en el gameplay que os enlazamos, aunque no sé si puede afirmar que sea por culpa del port, o es un fallo propio del motor, pero lo dicho, quitando esto, en Ubuntu 16.10 y en Solus, con un i5 Kaby Lake y 16Gb de RAM, y con una gráfica Nvidia GTX 1060 6Gb, haciendo uso de los drivers privativos, el juego se ejecuta sin ningún tipo de problemas.


 


![Agilidad extrema para el Agente 47](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisHitman/hitman_glitch.webp)


 


Técnicamente el juego luce muy bien, si bien, las animaciones durante la partida son un poco toscas y poco variadas, sobretodo cuando se trata de interactuar con objetos como puertas o cambiarse de disfraz, y aunque en este tipo de juegos no es que suponga ningún problema a la hora de jugar, sí que personalmente, sacan un poco del juego. También hay que comentar que aunque todos los textos están completamente traducidos, las voces están en un perfecto inglés, algo que a veces es un poco molesto cuando nos están informando acerca de la misión, o tenemos que escuchar las conversaciones de la gente para poder obtener alguna pista o comenzar alguna oportunidad. No es el fin del mundo y tampoco es que la historia que se cuenta sea tan profunda como para que el nivel de inglés tenga que ser muy alto si tenemos que apartar la vista de los subtítulos, pero hubiera sido de agradecer, sobretodo porque no es un RPG con un árbol de conversaciones enorme donde se entiende que el esfuerzo por doblar el juego al completo es enorme.


Al margen de estos peros, poca queja más podemos apuntarle al juego. Un buen port de la gente de Feral para un juego que disfrutarán enormemente los seguidores de la franquicia, y que nos ofrecerá muchas horas de juego y muchas opciones dentro de las mecánicas que propone, pero para aquellos que estaban buscando un Metal Gear o un Splinter Cell no encontrarán aquí su juego ideal.


 


Os dejamos con un gameplay del juego que podréis encontrar en nuestro [canal de YouTube](https://www.youtube.com/channel/UC4FQomVeKlE-KEd3Wh2B3Xw):


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" frameborder="0" height="315" src="https://www.youtube.com/embed/O1_n7e695Ww" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Tienes 'Hitman' disponible en su página de Steam:


<div class="steam-iframe"><iframe frameborder="0" height="190" src="https://store.steampowered.com/widget/236870/71223/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


Y tú, ¿ya has planificado tu próxima ejecución? Cuéntanos qué te parece este Hitman en los comentarios o a través de nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD).

