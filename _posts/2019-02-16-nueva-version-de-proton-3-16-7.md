---
author: Serjor
category: Noticia
date: 2019-02-16 09:17:21
excerpt: "<p>Valve sigue a\xF1adiendo mejoras y correcciones a su fork de Wine</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/2db073658ef98ff1d00d15e09c24406a.webp
joomla_id: 973
joomla_url: nueva-version-de-proton-3-16-7
layout: post
tags:
- steam
- wine
- valve
- steam-play
- proton
title: "Nueva versi\xF3n de Proton, 3.16-7"
---
Valve sigue añadiendo mejoras y correcciones a su fork de Wine

Esta mañana hemos amanecido en nuestro canal de Telegram con un mensaje de LordGault (suele hacer directos en twitch jugando bajo linux en su canal <https://www.twitch.tv/lordgault>:







Y es que como podéis ver, tenemos nueva beta de Protón disponible, la 3.16-7, con los siguientes cambios:


* Corregido el problema de la pantalla completa en Into The Breach
* Corregidos algunos crashes en algunos juegos d3d9 cuando se ejecutan bajo Mesa
* Corregidos algunos errores al lanzar ciertos juegos, como Path of Exile, la serie Los Bloons y los juegos de Naruto Shippuden
* Corrección para juegos con caracteres especiales en su ruta, entre otros LEGO Harry Potter
* Mejorado el comportamiento del mando en algunos juegos, especialmente en los juegos desarrollados con Unity, como Subnautica e INSIDE
* DXVK actualizado a la versión 0.96
* FAudio actualizado a la versión 19.02
* Restauradas algunas funcionalidades que habían dejado de funcionar en el cliente de UPlay
* Nueva variable de entorno para aquellos juegos viejos que no pueden manejar la longitud del nombre de algunas extensiones GL más nuevas. PROTON_OLD_GL_STRING limita la longitud del nombre de la extensión
* Nueva variable de entorno para deshabilitar el soporte a d3d10, PROTON_NO_D3D10.
* Soporte mejorado para juegos que usan versiones muy antiguas del SDK de steamworks, como por ejemplo Lost Planet.
* Solucionados varios problemas con el sistema de compilación y han añadido además un fichero Makefile para hacer compilaciones simples de manera más sencilla


La verdad es que no está nada mal, sobretodo con las nuevas versiones de DXVK y FAudio.


Y tú, ¿qué juegos vas a probar con esta nueva versión de Proton? Cuéntanoslo en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD)

