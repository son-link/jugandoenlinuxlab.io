---
author: Pato
category: Arcade
date: 2017-10-20 17:06:59
excerpt: <p>Nos llega otra entrega de la saga arcade de naves de Infinite Dreams</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/094d739d968df36a87418643dcb9d4ac.webp
joomla_id: 496
joomla_url: skyforce-reloaded-llegara-a-linux-steamos-el-proximo-dia-30-de-noviembre
layout: post
tags:
- accion
- proximamente
- arcade
- shoot-em-up
title: "'Skyforce Reloaded' llegar\xE1 a Linux/SteamOS el pr\xF3ximo d\xEDa 30 de\
  \ Noviembre (actualizado)"
---
Nos llega otra entrega de la saga arcade de naves de Infinite Dreams

**(actualización 30/11/2017)**


Ya tenemos disponible 'Skyforce Reloaded' tal y como estaba previsto en su página de Steam.




---


**(Artículo original)**


Después de traernos el notable '[Skyforce Anniversary](http://store.steampowered.com/app/355050/Sky_Force_Anniversary/)' a Linux/SteamOS Infinite Dreams vuelve a la carga con este nuevo 'Skyforce Reloaded' que nos traerá toda la jugabilidad y acción del primero, corregida, aumentada y mejorada en esta nueva entrega de esta 'saga' arcade disponible el próximo día 30 de Noviembre.


*Sky Force Reloaded es una experiencia "shoot 'em up" clásica presentada sobre un espléndido entorno visual y con una excelente jugabilidad. Disfruta de explosiones cegadoras, bonitos escenarios y jefes gigantescos en el modo de un jugador o en modo local cooperativo.*


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/broXSmOMgxw" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


* Domina 15 hermosos e inmersivos niveles repletos de desafíos y misiones que completar.
* Planta cara a enormes y amenazantes jefes, búrlate de ellos al verlos explotar y lamenta tu muerte cuando seas tú el que explote.
* Desbloquea nuevos modos de dificultad, desde Normal hasta Pesadilla.
* Ponte en peligro para rescatar a operativos del campo de batalla.
* Ensambla y prueba un total de 9 naves espaciales diferentes. Escoge a tu favorita según sus ventajas y estilo de juego único.
* Hazte con 30 cartas de bonificación, que son difíciles de conseguir, pero que añaden más profundidad a la experiencia de juego. Algunas te otorgarán beneficios permanentes, mientras que otras incrementan tus habilidades de manera temporal.
* Instala cientos de mejoras en tus armas, escudos y demás equipo. Convierte tu nave en una fortaleza volante.
* Completa objetivos durante el juego para desbloquear hasta un total de 8 asistentes técnicos, cada uno con su propia habilidad especial.
* Recupera los restos de tus compañeros caídos y consigue recompensas por recogerlos.
* Participa en los Torneos de Fin de Semana para desafiar a tus amigos en uno de los 5 escenarios especialmente diseñados para ello.
* Disfruta de un pulido sistema de juego y de su equilibrada curva de dificultad, ya te consideres un jugador casual o un fanático de los juegos de disparos.
* Deléitate con la calidad de sus voces y con una increíble banda sonora de corte electrónico.
* ¡Lleva tus expectativas de diversión al siguiente nivel invitando a tus amigos a unirse a tu tripulación en el modo local cooperativo!


Si te van los arcades de naves, incluso para jugar en cooperativo este es un buen juego para disfrutar.


'Sky Force Reloaded' estará disponible el próximo día 30 de Noviembre traducido al español en su página de Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/667600/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


¿Que te parece 'Skyforce Reloaded'? ¿te van los arcades?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

