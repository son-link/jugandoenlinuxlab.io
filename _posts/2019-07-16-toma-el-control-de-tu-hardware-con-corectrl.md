---
author: leillo1975
category: Software
date: 2019-07-16 07:41:00
excerpt: "<p>El software Open Source de @jpalaciosdev tiene nueva versi\xF3n. </p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/e0cf1cb2df2eeff97a260e0ea0561b69.webp
joomla_id: 1076
joomla_url: toma-el-control-de-tu-hardware-con-corectrl
layout: post
tags:
- amd
- open-source
- software-libre
- corectrl
- gpu
- cpu
- perfiles
title: Toma el control de tu Hardware con CoreCtrl (ACTUALIZADO)
---
El software Open Source de @jpalaciosdev tiene nueva versión. 


**ACTUALIZADO 4-6-20**: Tiempo llevábamos sin publicar nada de este fantástico software en nuestra web, pero eso no quiere decir que su creador se haya estado quieto mientras. A lo largo de estos meses ha ido creciendo en nuevas características, así como multitud de errores corregidos, y hace unos días a alcanzado la [versión 1.1.0](https://gitlab.com/corectrl/corectrl/-/tags/v1.1.0) que tiene la siguiente lista de **cambios**:


* *Añadida la traducción al francés. Gracias a DarkeoX ABYSSION Tar Valantinir ([#30](/corectrl/corectrl/-/issues/30 "[CORECTRL][TRANSLATION] Translation for French Language")).*
* *Añadida la traducción al catalán. Gracias a bla6 ([#72](/corectrl/corectrl/-/issues/72 "Translation for Catalan language")).*
* *Se añadió una solución para la carga alta del ordenador en algunos equipos...([#29](/corectrl/corectrl/-/issues/29)).*
* *Añadido soporte Navi ([#41](/corectrl/corectrl/-/issues/41 "5700 XT no overclocking control")).*
* *Los controles avanzados de administración de energía del Vega20 (navi... y el nuevo hardware) han sido rediseñados.([#37](/corectrl/corectrl/-/issues/37 "Locked frecuency on Radeon VII when using advanced controls")).*


### *Arreglos*


* *Arreglada compilación con nuevas versiones de gcc ([#54](/corectrl/corectrl/-/issues/54), [#62](/corectrl/corectrl/-/issues/62)).*


Pero como os solemos decir, la mejor forma de que "cateis" las bondades de esta aplicación es que lo probeis en vuestro ordenador. Para ello teneis las instrucciones en la [página de su proyecto](https://gitlab.com/corectrl/corectrl).  
  





---


**NOTICIA ORIGINAL:**  
Sorprende ver un programa de estas características tan bien rematado y así de golpe, pero lo cierto es que la versión 1.0 fué presentada hace un par de días, y gracias a [GamingOnLinux](https://www.gamingonlinux.com/articles/corectrl-a-new-foss-linux-tool-to-help-you-control-your-pc-with-application-profiles.14581/page=4#comments) nos enterábamos de su existencia. Su desarrollador, [Juan Palacios](https://twitter.com/jpalaciosdev), lo hacia en su cuenta de twitter tal que así:


 



> 
> CoreCtrl 1.0.0 Released  
> Project repository: <https://t.co/WbSIpnLd5E>  
> Main features overview: <https://t.co/9Qyp1TXcEp>[@gamingonlinux](https://twitter.com/gamingonlinux?ref_src=twsrc%5Etfw) [#linux](https://twitter.com/hashtag/linux?src=hash&ref_src=twsrc%5Etfw) [#gaming](https://twitter.com/hashtag/gaming?src=hash&ref_src=twsrc%5Etfw) [#amd](https://twitter.com/hashtag/amd?src=hash&ref_src=twsrc%5Etfw)
> 
> 
> — Juan Palacios (@jpalaciosdev) [14 de julio de 2019](https://twitter.com/jpalaciosdev/status/1150459533010980865?ref_src=twsrc%5Etfw)


  





"Buceando" un poco en los detalles de su proyecto, CoreCtrl, es una **aplicación Open Source para GNU/Linux que nos permitirá "controlar con facilidad el hardware de su ordenador utilizando perfiles de aplicaciones"**. Tal y como menciona en la descripción, es cierto que existen multitud de apliaciones que permiten hacer lo que hace CoreCtrl, pero su enfoque es mucho más complejo, y pensado en usuarios avanzados. **El diseño de este software apunta a los usarios comunes**, presentando una **interfaz clara y vistosa** que nos permitirá crear perfiles de Aplicaciones o juegos (incluso de Windows) para poder realizar cambios en las configuraciones de forma automática en el PC cuando estas se ejecuten, y no tener que activar "a mano" cada una de estas. En cierta medida recuerda ligeramente a [GameMode](index.php/buscar?searchword=gamemode&ordering=newest&searchphrase=all), el proyecto Open Source  de Feral para optimizar los juegos, pero en este caso se va mucho más allá. De esta forma, por poner ejemplos, podríamos aumentar la velocidad de nuestros ventiladores en nuestra placa base, aumentar la frecuencia en nuestra tarjeta gráfica AMD (en un futuro se espera tener soporte para más marcas) o cambiar el "gobernador" de nuestro sistema de "Powersave" a "Performance". CoreCtrl se podrá programar para que se ejecute en cada inicio y además tendremos fácil acceso a ella colocándose en la bandeja del sistema....  pero como se suele decir, más vale una imagen que mil palabras, y en este fantástico video podemos ver todas sus virtudes:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/6uchS6OiwiU" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


La aplicación está en desarrollo y aún quedan muchas funciones y hardware que soportar, pero como podeis ver tiene muy buenos mimbres. En este momento tan solo tenemos el código y unas [instrucciones de instalación](https://gitlab.com/corectrl/corectrl/wikis/Installation), pero es muy probable que pronto la comencemos a ver por los repositorios de las distribuciones más conocidas (en [Arch](https://aur.archlinux.org/packages/corectrl/) ya está disponible en tiempo record). Sería muy positivo también poder instalarla con paquetes Snap, Flatpak o AppImage, ya que al tratarse de un software enfocado al usuario normal, el tema de la complilación se puede atragantar un poco, pero con el tiempo seguro que eso no es un problema. Da gusto ver que de la comunidad nacen proyectos como este que hacen mucho más accesible nuestro sistema operativo a todo tipo de usuarios, creando soluciones que van mucho más allá de la consola y democratizando el manejo avanzado de nuestro PC. Nosotros en JugandoEnLinux, permaneceremos atentos a este proyecto e iremos informándoos de futuras actualizaciones. Si quereis descargar CoreCtrl podeis hacerlo desde la [página de su proyecto](https://gitlab.com/corectrl/corectrl).


¿Que os parece la idea de este desarrollador? ¿Os parece útil CoreCtrl? Cuéntanoslo en los comentarios o deja tus mensajes en nuestra comunidad de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).


 

