---
author: P_Vader
category: "V\xEDdeos JEL"
date: 2023-07-02 16:18:29
excerpt: "<p>Probamos Veloren una vez mas en su ya famosa fiesta de lanzamiento.</p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Veloren/Veloren_see_logo_900.webp
joomla_id: 1544
joomla_url: video-veloren-0-15
layout: post
tags:
- mundo-abierto
- mmorpg
- veloren
- voxel
title: "V\xEDdeo: Veloren 0.15"
---
Probamos Veloren una vez mas en su ya famosa fiesta de lanzamiento.


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube.com/embed/0tlk4sGcPiQ" title="YouTube video player" width="720"></iframe></div>


 


Un directo accidentado pero con buenas novedades:


* Primer jefe del mundo: "giga de escarcha"
* Aeronaves pueden ser manejadas por jugadores.
* El botín enemigo se comparte entre jugadores.
* Sistema de reputación: si cometes crímenes los NPC lo recordarán
* IA mejorada: los NPC hablarán con los jugadores y entre ellos sobre que pasa en Veloren.
* Los NPC viajan y cuentan rumores.
* Puedes elegir la ciudad inicial de tu personaje.


Mas detalles en su [changelog](https://gitlab.com/veloren/veloren/-/blob/master/CHANGELOG.md#0150-2023-07-01).

