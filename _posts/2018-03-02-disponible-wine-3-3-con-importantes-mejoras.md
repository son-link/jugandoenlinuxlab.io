---
author: leillo1975
category: Software
date: 2018-03-02 19:45:53
excerpt: "<p>Incluye por primera vez caracter\xEDsticas hasta ahora experimentales</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/e1a325047b148c9441be2b14c42dc196.webp
joomla_id: 669
joomla_url: disponible-wine-3-3-con-importantes-mejoras
layout: post
tags:
- wine
- vulkan
- wrapper
- csmt
- winevulkan
title: Disponible Wine 3.3 con importantes mejoras.
---
Incluye por primera vez características hasta ahora experimentales

Poco después de comenzar el año fué lanzada con gran expectación la [versión 3](index.php/homepage/generos/software/item/732-lanzado-wine-3-0-oficialmente) de este conocido "wrapper", incluyendo importantes mejoras en el soporte a DirectX10 y 11, entre otros muchos avances. En el día de hoy ha sido lanzada la 3ª revisión de esta versión, que como decimos en el titular incluye importantes mejoras.


La primera y más llamativa es el principio del soporte **Vulkan en Wine** (**winevulkan**), disponible gracias al trabajo principalmente de **Roderick Colenbrander**. Este nuevo código permite que la mayoría de las funcionalidades de Vulkan 1.0.51 funcionen en Wine y está implementado como una capa de Vulkan con soporte de controlador ICD en lugar de la biblioteca anterior de Vulkan que no es ICD usada previamente en Wine-Staging.


La segunda y no menos importante es la inclusión definitiva y por defecto de una de las opciones más características de Wine-stagin, la secuencia de comandos Direct3D con multithreading (**Direct3D CSMT**). Gracias a esto se mejorará  el rendimiento de ciertos juegos de forma considerable.


También se han puesto por defecto las **texturas multisampleadas**, se ha añadido el **soporte de controladores a través de SDL**, o el soporte para cargar binarios .Net de solo-CIL. Numerosos juegos como Max Payne 2, Just Cause 2 o The Witcher 3 han recibido mejoras y correciónes. Puedes consultar la lista completa de cambios en la página de [WineHQ.](https://www.winehq.org//announce/3.3)


Da gusto ver como este proyecto crece y crece, y cada vez es más completo y funcional. Cierto es que no hay nada como un programa o juego nativos, pero a falta de pan buenas son patatas. ¿Que opinas de Wine? Deja tu respuesta en los comentarios o discútelo en nuestro canal de [Telegram](https://t.me/jugandoenlinux).


 

