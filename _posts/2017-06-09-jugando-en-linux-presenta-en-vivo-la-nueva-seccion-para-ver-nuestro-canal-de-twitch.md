---
author: Pato
category: Web
date: 2017-06-09 16:47:24
excerpt: <p>Metemos nuestro canal de Twitch en la web, para ver los eventos con mayor
  comodidad</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/2039f9c06c46d5e4b5d871c0089d076b.webp
joomla_id: 367
joomla_url: jugando-en-linux-presenta-en-vivo-la-nueva-seccion-para-ver-nuestro-canal-de-twitch
layout: post
tags:
- streaming
- twitch
title: "Jugando En Linux presenta: \"En Vivo\" la nueva secci\xF3n para ver nuestro\
  \ canal de Twitch"
---
Metemos nuestro canal de Twitch en la web, para ver los eventos con mayor comodidad

Gracias a la capacidad de Twitch de poder ser incrustado, ¡ahora metemos [nuestro canal de Twitch](https://www.twitch.tv/jugandoenlinux) en la web!


Desde ahora tan solo con visitar nuestro apartado "[En Vivo](index.php/en-vivo)" en el menú principal podrás ver nuestros eventos en directo, como partidas online, eventos, torneos, y todo lo que organicemos comenzando por...


¡**Esta misma noche**! Estaremos retransmitiendo la primera toma de contacto con el juego "Warhammer 40.000: Dawn Of War" a las 22:30h (GTM +2)


Además, para hacerlo más fácil, ¡hemos implementado una cuenta atrás para el comienzo de cada evento, por lo que es imposible perdérselos! ¿Te apuntas?


Si es así, puedes pasarte por nuestro [canal de voz de Discord](https://discord.gg/ftcmBjD) donde estaremos comentando la partida (y de paso lo malo que soy jugando ;-D)


#### **Y además no te puedes perder...**


Los [dos streamings en vivo](index.php/homepage/editorial/item/480-el-e3-esta-a-una-semana-de-comenzar-eventos-horarios-y-lugar) que veremos también **en nuestro canal de Twitch** o "En Vivo" donde estaremos también comentando todas las novedades que vayan surgiendo. ¡No te los pierdas!

