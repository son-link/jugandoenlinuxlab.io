---
author: Pato
category: Noticia
date: 2020-04-16 20:22:27
excerpt: "<p>La plataforma sigue evolucionando poco a poco, no solo en juegos si no\
  \ en caracter\xEDsticas</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Stadia/stadia.webp
joomla_id: 1211
joomla_url: stadia-se-actualiza-a-su-version-1-45-y-trae-novedades-al-navegador
layout: post
title: "Stadia se actualiza a su versi\xF3n 1.45 y trae novedades al navegador"
---
La plataforma sigue evolucionando poco a poco, no solo en juegos si no en características


 


Debemos reconocer que **Stadia** no empezó con muy buen pié, pero parece que paso a paso van añadiendo características al servicio. Desde nuestro punto de vista a la plataforma aún le queda un buen trecho para llegar a ser un bazar con las funcionalidades que otros ya poseen, pero parece que poco a poco irán implementándolas. Y de eso mismo hablamos en esta noticia.


Y es que Stadia anuncia su **actualización 1.45** (sea lo que sea que eso signifique) para traernos mejoras en el navegador que es precísamente donde a nosotros más nos interesa pues no en balde es donde jugamos en esta plataforma.


Así pues, según [el comunicado](https://community.stadia.com/t5/Stadia-Community-Blog/Stadia-Update-1-45/ba-p/20103) a partir de ahora los que tengan acceso a Stadia Pro (todo el mundo actualmente?) **y jueguen desde el navegador** (nuestro caso) podrán disfrutar de **sonido envolvente 5.1 surround** y además si tienes conectado un mando tendrás a tu disposición un teclado en pantalla para poder usarlo en las situaciones que lo requieran.


Aparte de esto, también anuncian que habrán notificaciones en los móviles respecto a la calidad de la conexión.


Y eso es todo, de momento. Esperemos que los avances se sigan sucediendo también en otros aspectos como el soporte para más mandos (y volantes), mejora en las funcionalidades del bazar y latencias en los juegos.


Fuente: [Blog Stadia](#link_tab)

