---
author: leillo1975
category: "An\xE1lisis"
date: 2017-06-29 13:37:00
excerpt: "<p>Desgranamos en profundidad este cl\xE1sico reci\xE9n llegado a nuestro\
  \ sistema</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/802a9daf23bff040c546f525d4bd22bc.webp
joomla_id: 365
joomla_url: analisis-total-war-shogun-2-y-fall-of-the-samurai
layout: post
tags:
- feral-interactive
- sega
- total-war
- shogun-2
- fall-of-the-samurai
title: "An\xE1lisis: Total War: Shogun 2 y Fall of The Samurai"
---
Desgranamos en profundidad este clásico recién llegado a nuestro sistema

Hace poco más de dos semanas [os anunciabamos](index.php/homepage/generos/estrategia/item/456-total-war-shogun-2-y-fall-of-the-samurai-disponibles-para-linux-steamos) la disponibilidad de "Total War: Shogun 2" y su expansión independiente "Fall of the Samurai". También os prometiamos un análisis de estos títulos, y aquí está para explicaros lo mejor posible todo lo referente a él y su funcionamiento en nuestros sistemas.


 


Lo primero que tenemos que decir es que como todos los juegos de la serie Total War, está desarrollado por [Creative Assembly](http://www.creative-assembly.com/), compañía de la que ya tenemos varios juegos en nuestro sistema (Empire, Medieval II, Warhammer, Attila y ahora Shogun 2). El juego está editado por la mítica [SEGA](http://www.sega.com/) y portada a GNU-Linux/SteamOS por nuestros queridos [Feral Interactive](https://www.feralinteractive.com/es/), a los que tenemos que agradecer las claves facilitadas tanto para la elaboración de este análisis, como para la grabación de los videos en Youtube (al final del artículo).


 


![Shogun2Menu](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisShogun2/Shogun2Menu.webp) *En el menú del juego accederemos a las diferentes opciones del juego (campaña, multijugador, batallas, etc)*


 


Todos los que en alguna ocasión hemos jugado a algún Total War sabemos que son títulos de estrategia donde tenemos una parte de gestión económica, militar y diplomática que se desarrolla por turnos, muy similar a lo que podemos ver en la Saga [Civilization](index.php/homepage/analisis/item/352-analisis-sid-meier-s-civilization-vi); y otra de estrategia en tiempo real donde dirigiremos nuestros ejercitos en el campo de batalla. TW: Shogun 2 y su expansión, como es obvio no se salen de ese guión que tan buenos resultados le da a Creative Assembly, y que casualmente inició con [la primera parte de este juego](https://es.wikipedia.org/wiki/Shogun:_Total_War) ya hace 17 años. Para empezar es necesario decir que la trama se desarrolla en el primero de los títulos en el Japón Feudal del siglo XVI donde las luchas entre clanes por el **Shogunato de Kioto** (antigua capital Nipona) eran constantes. En cuanto a su expansión independiente, **Fall Of The Samurai**, la acción pasa al siglo XIX con la llegada de los ejercitos modernos a la tierra del sol naciente, y la lucha de los Samurais por evitar lo inevitable.


 


Empezando con Shogun 2, hay que decir, de que a pesar de que el juego llega 6 años tarde a nuestros escritorios, se conserva y luce de maravilla, manteniendo intactas las sensaciones de estar ante un juego moderno. Para jugar a él, lo primero que deberemos hacer es tomar parte por uno de los 9 clanes principales del juego (Chosokabe, Shimazu, Date, Hojo, Mori, Oda, Takeda, Tokugawa y Uesugi), teniendo cada uno de estos unas características diferenciadas (territorio, unidades, recursos, posición en el mapa, relaciones con otros clanes, etc) y una mayor o menor dificultad. Cada uno de los clanes tendrá un video introductorio que veremos al empezar la campaña, que por cierto es personalizable, y a partir de ahí todo queda bajo nuestra decisión, ya que el juego no tratará de imponernos una linea a la cual seguir, por lo que tendremos total libertad de afrontar la partida como nosotros estimemos oportuno. Lo único que puede romper ese libre albedrío del que disponemos  es la obligatoriedad de capturar Kioto, y el dominio de cierto número de provincias según el nivel que escojamos. También ciertos objetivos que nos proporcionarán un plus si los cumplimos (recursos, habilidades, unidades...), pero que no son obligatorios.


 


![Shogun2Clan](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisShogun2/Shogun2Clan.webp)


*En la pantalla de nuestro clan veremos toda la información referente a el, así como los miembros de la familia y consejo*




---


En cuanto a la parte de estrategia por turnos del juego, podremos movernos por el mapa de Japón con nuestras tropas y agentes, además de gestionar nuestras ciudades-castillo, puertos, recursos. Estos se moveran en grupo con una figura representativa de cada uno de ellos. Podremos agruparlos, separarlos, o cambiar unidades con otros ejercitos, y dependiendo de las caracteristicas de cada uno,  tendrán más o menos alcance en cada turno. En esto último también influye, por ejemplo las mejoras que hagamos de los caminos en nuestro territorio. Obviamente también podremos utilizar el mar para mover nuestras tropas y agentes por el mapa, lo que nos dará acceso más rápido a lugares remotos o inaccesibles de otra manera.


 


Nuestras tropas, por supuesto acumularán experiencia, así como nuestros agentes y generales, que con cada acción militar o de espionaje verán incrementadas sus estadisticas y posibilidades. Por poner un ejemplo, si uno de nuestros ninjas sube de nivel podrá optar a especializarse en un tipo de acciones de espionaje, pudiendo ser más ducho en esas "artes", y teniendo más posibilidades de éxito. Nuestros generales también tendrán un arbol de habilidades que los hará más efectivos en batalla, permitirá moverse más distancia en un turno o gestionar mejor las unidades, por ejemplo, con bonuses en cierto tipo de unidades.


 


![Shogun2Mapa](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisShogun2/Shogun2Mapa.webp)


*En el mapa de juego veremos los diferentes terrotorios, ciudades, recursos y unidades*


 


Podremos incidir en los impuestos incrementando o bajando estos, algo que tendremos que hacer con mucho cuidado ya que pude provocar revueltas en nuestra población. Si los bajamos demasiado también corremos el riesgo de quedarnos sin dinero para financiar nuestros costosos ejercitos, con lo que eso puede suponer. En este sentido debemos encontrar un equilibrio que nos permita margen de maniobra para acometer mejoras en nuestras ciudades, contratar unidades y agentes, o financiar operaciones de espionaje o contraespionaje.


 


En más de una ocasión nos encontraremos en la encrucijada de tener que escojer el ahogar a nuestro pueblo a impuestos para poder disponer de recursos para avanzar en la campaña, o bajarlos para evitar que se alcen contra nosotros. Esto puede producir cierta fustración en el jugador, ya que quedarse sin blanca es bastante común en las partidas, especialmente en los niveles más avanzados. Normalmente, como todo en la vida, en la mesura suele estar la solución, por lo que no es recomendable llegar a situaciones en las que nos quedemos sin dinero y siempre es recomendable dejar un fondo para imprevistos. No conviene por lo tanto gastar todo de golpe o invertir todo nuestro dinero en ejercitos y descuidar los avances civiles, o a la inversa.


 


![Shogun2Finazas](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisShogun2/Shogun2Finazas.webp)


*Podremos incidir en la recaudación de impuestos en la pantalla de finazas*


 


En cuanto a la diplomacia, podremos entablar relaciones con otros clanes, siendo esto una parte muy destacable del juego. Estas nos permitiran tener aliados, vasallos, y por supuesto enemigos. Podremos declarar la guerra a un clan con el apoyo de otros, fomentar matrimonios entre miembros de los diferentes clanes para establecer alianzas, realizar pagos y regalos, solicitar acceso militar a través de un territorio... multiples opciones que enriquecen en gran medida la experiencia y la hacen mucho más creible. Para poder gestionarla, tendremos un panel donde encontrar todas estas opciones, así como la relación con cada uno de los clanes.


 


![Shogun2Diplo](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisShogun2/Shogun2Diplo.webp)


*Las relaciones diplomáticas son un aspecto esencial del juego*


 


En el arbol de habilidades (dominio de las artes) podremos tomar dos caminos, el militar (Bushido) y el tecnológico (camino del Chi), por lo que debemos buscar un camino que se adapte lo mejor posible a nuestro estilo de juego o las caracteristicas de nuestro clan. Estas habilidades van a proporcinar mejoras a nivel económico, militar o diplomático, y en muchos casos van a desbloquear opciones que sin haber conseguido previamente la habilidad necesaria, no podremos utilizar o construir. Hablando de la construcción, nuestros edificios y castillos podrán ser mejorados y ampliados, permitiendonos albergar más población, tener mejores defensas, producir unidades de ejercito diferentes o incrementar nuestra producción y comercio.




---


Las habilidades en Shogun 2 son uno de los aspectos más cuidados y mejor compensados del juego, ya que escoger un camino u otro no nos va a reportar ventajas o inconvenientes a la hora de jugar. Simplemente va a cambiar nuestro estilo de juego, que tendremos que adaptar a las circunstancias y situación en la partida.


 


 ![Shogun2Artes](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisShogun2/Shogun2Artes.webp)


*Mediante el dominio de las artes obtendremos y desbloquearemos mejoras*


 


Durante el juego dispondremos de consejeros que nos darán instrucciones de como jugar, tanto a nivel civil, como militar, incluso en batalla. Estas indicaciones pueden ser tremendamente últiles en los primeros compases del juego, y vienen acompañadas de voces perfectamente dobladas al español, a parte de su texto correspondiente en la esquina superior izquierda de la pantalla. Por supuesto podremos anular tanto las voces como los textos en las opciones del juego. Otro elemento tremendamente interesante que se ha incluido en el juego, y que nos será super-útil será la enciclopedia donde podremos ver las caracteristicas de unidades, edificios, agentes, etc, así de los requisitos necesarios para poder acceder a ellos.


 


Elemento crucial para desestabilizar tanto los recursos, como unidades militares del resto de clanes serán los agentes, que se pueden distribuir en 4 tipos. Los primeros serán los **ninjas**, que nos permitirán espiar, asesinar o sabotear al enemigo.  En segundo lugar podremos disponer de **Metsukes**, que se encargarán de las labores de contraespionaje, pudiendo juzgar a los agentes enemigos, como de sobornar a ejercitos. También podremos contratar a **Geishas**, perfectas para el asesinato, pero mucho más débiles frente a agentes enemigos. Por último, los **monjes** podrán convertir a una determinada población a nuestra religión , fomentando la la paz en nuestros asentamientos o provocando inestabilidad y revueltas en nuestros contrarios. Todas estas acciones se realizarán desde la pantalla de mapa (turnos) y dispondrán de una cinemática correspondiente.


 


En cuanto a los ejercitos, estos podrán estar comandados por nuestros **generales**, que nos otorgarán bonificaciones, además de insuflar moral a las tropas. Forman parte de la caballería y suelen ser bastante poderosos con respecto a otras unidades. Pero hay que cuidarlo, ya que si cae en batalla habrá una bajada de moral que nos afectará muy negativamente. Estos generales pueden formar parte de la familia y consejo de nuestro clan, e incluso pueden maniobrar en nuestra contra para hacerse con el poder o ser sobornados por el enemigo, lo cual nos puede incluso obligar a proponerle un suicidio por honor (seppuku o harakiri)




---


Ahora que hablamos de los ejercitos creo que es el momento adecuado para empezar a hablar de la **parte en tiempo real**. Cuando presentemos batalla a otro ejercito, se nos mostrará una pantalla donde podremos comparar nuestras tropas con las de nuestro enemigo, y en consecuencia tomar las diversas decisiones. En la pantalla aparecerá  una barra con dos colores que nos indicará si tenemos superioridad, estamos empatados o lo contrario. Si estamos atacando una ciudad podemos asediarla durante varios turnos para así debilitarla y tener ventaja. También **tenemos la opción de realizar un combate automático**, lo cual puede ser útil si queremos ahorrar tiempo y centrarnos en la parte de gestión por turnos, pero que no es muy recomendable si estamos en inferioridad. Por supuesto podremos ir a la batalla o retirarnos de ella.


 


![Shogun2PreBatalla](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisShogun2/Shogun2PreBatalla.webp)*Antes de la batalla podremos comparar nuestro ejercito con el del enemigo*


 


 Antes comenzar la batalla se nos presentará en la pantalla de carga el mapa sobre el que vamos a combatir. inmediatamente despues tendremos la oportunidad de posponer la batalla para más tarde si las condiciones climatológicas son adversas para esta. Después es probable que veamos una **cinemática hecha con el motor del juego** donde nuestro general arengará a nuestras tropas. En cuanto comencemos tendremos opción a colocar y ordenar nuestros ejercitos como nosotros deseemos, así como **realizar formaciones con ellas**. Una vez terminado todo este proceso podremos darle a comenzar y empezará la contienda en si.


 


Durante las batallas **podremos mover la cámara con total libertad por el escenario** haciendo uso del ratón y el teclado, facilitandonos esto en gran medida la visión de la acción y planificación, así como la selección de los diversos grupos de unidades. Dispondremos también de una cámara subjetiva que nos permitirá vivir la batalla desde los ojos de una unidad. Esto no es especialmente útil, pero si bastante espectacular.


 


![Shogun2VSubjet](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisShogun2/Shogun2VSubjet.webp)


*La vista subjetiva no es muy útil en batalla pero le da al juego un toque de espectacularidad*


 


Hablando de las unidades, podremos realizar diversas acciones según el tipo de unidad que tengamos seleccionada (andar, correr, lanzar flechas de fuego, atacar cuerpo a cuerpo, lanzar gritos de guerra...). Cada unidad tendrá unas características determinadas (velocidad, armadura, resistencia, arma utilizada) y será adecuada para combatir a otros tipos de unidades. En esto no nos encontramos muchas diferencias con respecto a otros juegos de la saga Total War, por ejemplo los arqueros son efectivos a distacia pero cuerpo a cuerpo son débiles, los lanceros son adecuados para combatir a la caballería, y estos últimos son buenos para cargar contra el enemigo cuando está desprevenido y hacer que huyan.


 


Dentro de cada una de las categorías de unidades podremos encontrar a parte de la propia experiencia de la que disponga la unidad, diversos niveles que previamente desbloquearemos en el mapa estratégico. También dispondremos de máquinas de guerra que nos serán útiles sobre todo en los ataques a fortíficaciones. Es importante decir que también tendremos a nuestra disposición unidades heróicas que pueden decantar la batalla a nuestro favor gracias a su efectividad en la contienda.


 


![Shogun2Batalla](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisShogun2/Shogun2Batalla.webp)


*Durante la batalla veremos grandes cantidades de unidades luchando*


 


Dentro del apartado de las batallas en tiempo real también **tendremos la oportunidad de dirigir batallas navales, aunque estas, a mi juicio, no están todo lo bien resueltas**, ya que disponen de muy **pocas opciones tácticas** y resultan un tanto **monótonas y repetitivas**. Por ejemplo, resulta un tanto complicado realizar los abordajes, y conseguir poner en paralelo a los barcos tiene cierta dificultad. Además no son tan vistosas como las luchas en tierra, ya que veremos por norma general menos unidades en liza, y el campo de batalla será el "ancho mar". Sin duda este es uno de los pocos aspectos mejorables (que no negativos) del juego, y que desentona un poco con la calidad general de este.




---


En cuanto al apartado técnico hay que decir que **el juego luce muy bien a pesar de los años**, aunque también es cierto que en este momento existen juegos de estrategia en tiempo real con mejor detalle. Es muy de destacar, como es habitual en los juegos de Total War, ver las cantidades ingentes de unidades moviendose por el escenario a la vez. Solo cuando acercamos demasiado la cámara a estas podemos percibir la calidad del trabajo de modelado de Creative Assembly, así como las animaciones de los soldados que son magníficas.


 


En cuanto al rendimiento en nuestro sistema (OpenGL), no he tenido la oportunidad de probar el juego en su versión de Windows, pero en un equipo medio como el mio, **el rendimiento es suficiente, aunque no espectacular**. En la configuración más alta el juego sufre de bajadas moderadas de frames sobre todo cuando tiene que representar grandes cantidades de soldados en el escenario de la contienda, y muy especialmente cuando acercamos la cámara a estas. Esto sería un defecto grave si se tratase de un juego de acción o conducción, pero en el caso de un juego de estratégia como el que nos atañe carece de mucha importancia. Solo si estamos obsesionados con los FPS va a suponer un problema a la hora de jugar. Al no poder comparar la conversión a GNU-Linux/SteamOS con el juego original no estoy en condiciones de decir si se trata de un un buen o mal port, aunque por la sensación general que me produce, creo que el resultado final está más cerca de lo primero.


 


![Shogun2Ejercitos](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisShogun2/Shogun2Ejercitos.webp)


*El detalle individual de las unidades cuando acercamos la cámara es simplemente soberbio*


 


Si hablamos de la calidad artística de este título nos podemos quedar cortos, ya que el juego está perfectamente ambientado en el Japón feudal y desprende belleza por los cuatro costados. Solo hay que arrancar el juego para apreciar la calidad de las animaciones del menú. En el mapa táctico apreciaremos detalles como niebla, reflejos del agua, movimientos de unidades... También están muy cuidados los diferentes diálogos que se nos muestran durante la partida (enciclopedia, diplomacia, finanzas, etc) que concuerdan a la perfección con el estilo general del juego. No vamos a encontrar nada descuidado ni fuera de lugar, consiguiendo sumergirnos aun más si cabe en el juego.


 


A nivel sonoro, las melodías nos sumergen de forma maestra en la epoca y localización del juego, siendo estas de gran calidad y consiguiendo no ser repetitivas y tediosas. Las voces que escucharemos están en Japonés, excepto las de los tutoriales que están perfectamente dobladas al castellano. En cuanto a los efectos sonoros no encontramos nada especial que decir ni que desentone con el juego.




---


**EXPANSIÓN INDEPENDIENTE Fall Of The Samurai**


 


Junto al juego original, Feral Interactive también nos trae su expansión independiente Fall Of The Samurai. En ella, como comentaba al principio se nos narra **la lucha entre lo moderno y lo antiguo en el Japón del sigo XIX**. En esta eṕoca tuvo lugar la corta guerra Boshin que sacudió al pais entre 1868 y 1869. Con la influencia y llegada de occidentales a la tierra del sol naciente, el pais se ve sumido en unas profundas disputas donde se confrontan la tradición (shogunato) y la modernidad (Imperiales), por lo que el pais se ve nuevamente sumido en una nueva guerra. Esta historia tiene un inevitable final que la historia nos ha demostrado, pero que ha dejado unas raices imborrables en la población de Japón. Todo el mundo sabe que la conjunción de lo nuevo y lo viejo es una de las señas identitarias de las tierras niponas.


 


![FOTS Ejercitos](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisShogun2/FOTS_Ejercitos.webp)


*Tradición contra modernidad, la eterna lucha*


 


Entrando en lo que es el juego, hay que decir que no hay muchas diferencias radicales con lo visto en Shogun 2. Obviamente las mecánicas y aspecto del juego son las mismas, pero si tiene muchas novedades interesantes que hacen de esta expansión algo imprescindible si hemos disfrutado del juego base. Obviamente, tres siglos de diferencia en la cronología tienen que notarse, y la inclusión de la influencia extranjera también.


 


En Fall of the Samurai vamos a encontrar considerables diferencias, como es lógico, en cuanto al las unidades de ejercito, en las que encontraremos 39 nuevas unidades terrestres y 10 navales. Vamos a poder usar, como es lógico, nuevas unidades con armas de fuego y artillería para bombardeos. En los primeros compases del juego, el uso de mosquetes no supondrá una ventaja a la hora de atacar a ejercitos de con espadas o caballeria, ya que estos son muy lentos a la hora de cargar y su alcance es limitado y falto de precisión, por lo que será muy fácil acabar con ellos. En fases más avanzadas del juego, con la mejora de tecnologías serán casi insuperables. Además podremos hacer uso de ametralladoras Gatling que harán estragos en las lineas enemigas. El uso de unidades extranjeras también es algo a resaltar y será crucial en muchas ocasiones.


 


![FOTS Ferrocarril](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisShogun2/FOTS_Ferrocarril.webp)


*El uso del ferrocarril nos hará movernos mucho más rápido*


 


Durante la partida nos encontraremos en una constante lucha interna, especialmente si hemos escogido el camino de la tradición, ya que se hace difícil e inevitable el acabar usando armas de fuego. **Tendremos que elegir constantemente si "progresar" o seguir los preceptos del Bushido**. La elección del camino "fácil" es dejarte llevar y avanzar hacia un ejercito moderno, pero esto va a traer problemas, ya que las unidades de nuestras tropas serán más caras de conseguir y de mantener. Además hay un aspecto social que debemos tener en cuenta, ya que el descontento será más dificil de controlar y las revueltas de la población serán algo con lo que tengamos que lidiar a menudo. No progresar será más barato de mantener y nos dará el favor de la población, ya que se verán respetadas sus tradiciones.


 


Algo en lo que **se ha hecho especial incapié es en los combates navales**, ya que estos pasan a tener especial importancia, dontándolos no solo de nuevas unidades (barcos de vapor y acorazados), sinó también de nuevas acciones a realizar, entre las que destaca bastante la **posibilidad de atacar y asediar puertos**, que por cierto podremos reforzar con fortificacaciones. También **no se ha olvidado el uso de los agentes**, que aparte de los que teníamos en el juego base, y que han sido dotados de nuevas habilidades, podremos disponer de agentes estranjeros que mejoran nuestras tropas, y activistas políticos que pueden ayudarnos a mantener el orden en la población poniendolos a favor de nuestras políticas. Otra novedad importante es la **inclusión del ferrocarril**, que nos permitirá mover nuestras tropas mucho más rápido por el tablero de juego.


 


![FOTS naval](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisShogun2/FOTS_naval.webp)


*Las batallas navales ganan en espectacularidad, interés y opciones*




---


**Como conclusión final,** tanto Shogun 2 como su expansión Fall of the Samurai **son juegos imprescindibles** que no debemos dejar escapar si nos gustan los juegos de Estrategia, especialmente si somos fans de Total War. A pesar del retraso evidente, ya que como comentabamos al principio se trata de un juego del  2011, es de agradecer a Feral Interactive el trabajo que han hecho por traenos tanto a Shogun 2 como Fall of the Samurai. Unos títulos **altamente rejugables** y unas auténticas joyas hechas videojuego que nos va a sumergir en la historia de Japón de forma sobresaliente y que nos va a otorgar incontables horas de diversión en nuestros PC's.


 


A continuación os dejamos un par de videos de nuestro canal de Youtube donde podeis ver a grandes rasgos como jugar a Shogun 2:


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/vTmejDls3LY" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/bLToCNyykh8" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


Si quieres comprar Total War: Shogun 2 Collection puedes hacerlo en la [tienda de Feral](https://store.feralinteractive.com/es/mac-linux-games/shogun2twcollection/) (recomendado), o en Steam:


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/201270/18408/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


Si quieres hacerte con la expansión independiente Fall of the Samurai Collection puedes hacerlo en la [tienda de Feral](https://store.feralinteractive.com/es/mac-linux-games/shogun2twfotscollection/) (recomendado), o en Steam:


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/201271/17095/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


¿Que os ha parecido el análisis? ¿Teneis pensado comprar Shogun 2? Deja tu opinión en los comentarios de este artículo o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

