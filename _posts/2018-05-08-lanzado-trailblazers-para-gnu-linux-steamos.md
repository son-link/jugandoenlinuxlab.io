---
author: leillo1975
category: Carreras
date: 2018-05-08 08:29:20
excerpt: <p class="ProfileHeaderCard-screenname u-inlineBlock u-dir" dir="ltr"><span
  class="username u-dir" dir="ltr">@supergonkgames acaba de estrenar este vistoso
  juego</span></p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/320b783b13f6b591849dc82b8df81be8.webp
joomla_id: 725
joomla_url: lanzado-trailblazers-para-gnu-linux-steamos
layout: post
title: Lanzado "Trailblazers" para GNU-Linux/SteamOS
---
@supergonkgames acaba de estrenar este vistoso juego

Si recordais, a finales de Febrero [os hablábamos](index.php/homepage/generos/carreras/item/781-trailblazers-llegara-esta-primavera-a-gnu-linux-steamos) de este llamativo título desarrollado por la británica [SuperGonk Games](http://www.trailblazersgame.com/) y editado por [Rising Star Games](http://www.risingstargames.com/eu/). Desde un principio se confirmaba su soporte para nuestros sistemas, lo cual era de esperar pues está desarrollado con el versátil motor Unity, pero que aun así es digno de agradecer. Finalmente el juego a sido puesto a la venta hoy mismo, tal y como refleja el tweet del anuncio:



> 
> Trailblazers is out now on [#Steam](https://twitter.com/hashtag/Steam?src=hash&ref_src=twsrc%5Etfw) and [#PS4](https://twitter.com/hashtag/PS4?src=hash&ref_src=twsrc%5Etfw)! [#Trailblazers](https://twitter.com/hashtag/Trailblazers?src=hash&ref_src=twsrc%5Etfw) [#TrailblazersGame](https://twitter.com/hashtag/TrailblazersGame?src=hash&ref_src=twsrc%5Etfw) [#PS4](https://twitter.com/hashtag/PS4?src=hash&ref_src=twsrc%5Etfw) [#Steam](https://twitter.com/hashtag/Steam?src=hash&ref_src=twsrc%5Etfw) [#OutNow](https://twitter.com/hashtag/OutNow?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/DM9Ms29ubJ](https://t.co/DM9Ms29ubJ)
> 
> 
> — Trailblazers (@TrailblazersEN) [May 8, 2018](https://twitter.com/TrailblazersEN/status/993804736699781120?ref_src=twsrc%5Etfw)



Para recordaros de que iba este juego, se trata de una especie de mezcla de arcade de coches al estilo WipeOut pero con toques del conocido Splatoon de Nintendo, donde tendremos que pintar la carretera para poder "dominarla" y alcanzar mayor velocidad. El juego cuenta con un modo historia, así como de completo multiplayer que nos permitirá jugar tanto online, como a través de LAN, e incluso pantalla compartida. Si quereis una descripción más completa podeis ver la oficial de Steam:



> 
> *""¡Este es el torneo de Trailblaze! Emociones a gran velocidad con una explosión de color en Trailblazers, el primer juego de carreras de arcade cooperativo segundo a segundo. Pinta la pista con líneas de carrera, usa el turbo por el color de tu equipo y llega a la meta antes que los oponentes. ¡Pinta más para correr más!*  
>    
>  *Trailblazers es un juego de carreras cooperativo con una jugabilidad en pista innovadora. En equipos de tres contra tres, los jugadores pintan la pista para crear la mejor línea de carrera y después usan la pintura para acelerar a velocidades de vértigo. ¡Conduce con estilo y elegancia para ganar puntos de habilidad y hazte con la victoria! Vive la acción a gran velocidad con hasta seis jugadores en línea o con cuatro de forma local mediante pantalla partida con oponentes extra en línea o controlados por la IA.*   
>    
>  *• Pinta la pista mientras corres y crea tu propia línea de carrera en la que usar el turbo... ¡no hay dos carreras iguales!*  
>    
>  *• Coopera con tus compañeros para pintar, usar el turbo y superar al otro equipo, ganando puntos por conducir con habilidad y estilo.*  
>    
>  *• Varios modos de juego: 3 contra 3, carreras con compañeros, carreras en solitario y más, incluyendo una campaña entera que descubrir.*  
>    
>  *• Juega por tu cuenta o en pantalla partida, tanto con tus amigos de forma local como en línea contra oponentes de todo el mundo, ¡incluso en otras plataformas!*  
>    
>  *(El juego multiplataforma conecta a jugadores de PC/Mac/Linux con oponentes de PlayStation®4 o Xbox One/Nintendo Switch. Consulta las opciones para saber más.)""*
> 
> 
> 



Los requisitos técnicos no son excesivos y son los siguientes:


* **SO:** Ubuntu 12.04 **64Bits**
* **Procesador:** Intel Core i5
* **Memoria:** 8 GB de RAM
* **Gráficos:** Nvidia Geforce GTX 650 or equivalent
* **Almacenamiento:** 3 GB de espacio disponible
* **Tarjeta de sonido:** Any compatible soundcard
* **Notas adicionales:** Gamepad required


El juego puede ser adquirido en Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/621970/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


Si quereis tener una idea más aproximada de lo que es el juego podeis ver su último trailer:


<div class="resp-iframe"><iframe allow="autoplay; encrypted-media" allowfullscreen="" frameborder="0" height="450" src="https://www.youtube.com/embed/NzRtDUMdu1c" width="800"></iframe></div>
 


Gracias a Rising Star Games y SuperGonk, en JugandoEnLinux.com probablemente os ofreceremos un Stream del juego probando sus virtudes y proximamente un completo análisis, que se publicará en unas semanas.


 


 ¿Qué os parece Trailblazers? ¿Os animariais a echar una partida? Déjanos tus impresiones en los comentarios, o en nuestros canales de [Telegram](https://t.me/jugandoenlinux) y [Discord](https://discord.gg/fgQubVY).

