---
author: Serjor
category: "Acci\xF3n"
date: 2017-08-16 21:29:00
excerpt: "<p>As\xED lo indican en los foros de Steam</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/16e33b8fd7ad7ca58b03c6bbca1f0b81.webp
joomla_id: 432
joomla_url: los-desarrolladores-de-astrokill-buscan-betatesters-para-la-version-de-gnu-linux
layout: post
tags:
- beta
- early-access
- astrokill
title: "Los desarrolladores de Astrokill buscan betatesters para la versi\xF3n de\
  \ GNU/Linux"
---
Así lo indican en los foros de Steam

Leemos en [reddit](https://www.reddit.com/r/linux_gaming/comments/6u1ck7/astrokill_developer_looking_for_beta_testers_need/?ref=share&ref_source=link) que los desarrolladores de ASTROKILL están buscando betatesters para GNU/Linux.


Afortunadamente el juego, actualmente en Early Access, está siendo desarrollado con Unreal Engine 4 y por lo visto tras la última actualización que han hecho del motor ya pueden exportar a GNU/Linux, por lo que ahora necesitan testers con máquinas medianamente potentes.


El juego en sí es un arcade tipo TIE Fighter, y que personalmente me ha recordado a las escenas de combate espacial de Battlestar Galactica, donde pilotaremos una nave de combate, ya sea en primera o tercera persona.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/0A9O9BPFa-A" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/376880/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


Así que ya sabéis, si queréis colaborar en que este juego llegue a GNU/Linux, no dudéis en echar una mano a los desarrolladores que de buena gana os facilitarán una llave.


Y tú, ¿Emularás a Luke en este juego? Cuéntanoslo en los comentarios o en los canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD)

