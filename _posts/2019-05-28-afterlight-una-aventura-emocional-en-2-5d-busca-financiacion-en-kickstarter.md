---
author: Pato
category: Aventuras
date: 2019-05-28 07:26:02
excerpt: "<p>El juego de los espa\xF1oles <span class=\"css-901oao css-16my406 r-1qd0xha\
  \ r-ad9z0x r-bcqeeo r-qvutc0\" dir=\"auto\">@SilentRoadGame</span>s Promete soporte\
  \ en Linux y saldr\xE1 tanto en Steam como en GOG</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/0ea760dfa8da32111175f3c77ef13d15.webp
joomla_id: 1051
joomla_url: afterlight-una-aventura-emocional-en-2-5d-busca-financiacion-en-kickstarter
layout: post
tags:
- indie
- aventura
- puzzles
- kickstarter
title: "Afterlight, una aventura emocional en 2,5D busca financiaci\xF3n en Kickstarter"
---
El juego de los españoles @SilentRoadGames Promete soporte en Linux y saldrá tanto en Steam como en GOG

Dentro de la escena indie cada vez es mas difícil encontrar proyectos que tengan el potencial de atraer la atención de los mecenas en campañas de crowdfunding, en parte debido al descrédito que están sufriendo estas plataformas ante la falta de credibilidad o de seguridad a la hora de cumplir con lo prometido.


Sin embargo de vez en cuando encontramos proyectos que pueden merecer la pena, y en este caso hablamos de **Afterlight**, un juego obra del estudio español **Silent Road Games** compuesto por algunos veteranos de la escena indie y que buscan financiación para terminar este proyecto. Según la descripción de la campaña:


*Afterlight se centra en el entorno, los elementos emocionales y psicológicos, y la animación para proporcionar una experiencia de juego rica, así como una historia convincente.* 


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/g0tYs_T9brU" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Entre sus características destacan:


* Una interpretación creativa de una gran documentación sobre el entorno de Titán: un entorno de ensueño y belleza.
* Una visión emocional de los trastornos mentales, no centrada en aspectos cliché, sino para hablar de sus consecuencias en las relaciones y el crecimiento personal.
* Animaciones cuidadosamente elaboradas para contar a partir de gestos y pequeños detalles.
* Vastas posibilidades en los puzzles e incluso llegar a controlar robots gigantes.


En cuanto a nuestro sistema favorito, el estudio afirma en su campaña que el juego llegará a todos los sistemas de PC incluyendo Linux, dejando las versiones de consola como objetivos de la campaña prometiendo incluso su lanzamiento en GOG y otras tiendas sin DRM.


![press](/http://silentroadgames.com/img/press/gifs/GIF_PRESENTATION_680p.webp)


Afterlight necesita algo mas de 31.000€ para llegar a la meta, y a falta de 17 días ya lleva alcanzado algo más de la mitad de su objetivo. Si te convence la propuesta, quieres aportar o saber más sobre el proyecto puedes visitar su página web de Kickstarter [en este enlace](https://www.kickstarter.com/projects/bodero/afterlight-an-emotional-journey-inside-the-mind/faqs).


¿Que te parece la propuesta de Afterlight? ¿Te gustan los juegos con temática emocional?


Cuéntamelo en los comentarios o en el canal de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux).

