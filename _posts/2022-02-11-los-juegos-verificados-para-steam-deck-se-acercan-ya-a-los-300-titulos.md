---
author: Pato
category: Software
date: 2022-02-11 19:28:22
excerpt: "<p>El proceso de verificaci\xF3n de Steam va acelerando el ritmo</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Steamdeck/SteamDeck1.webp
joomla_id: 1427
joomla_url: los-juegos-verificados-para-steam-deck-se-acercan-ya-a-los-300-titulos
layout: post
title: "El n\xFAmero de juegos verificados para Steam Deck se acerca ya a los 300\
  \ t\xEDtulos"
---
El proceso de verificación de Steam va acelerando el ritmo


Hace apenas una semana estábamos celebrando que habíamos [pasado los 130 títulos verificados]({{ "/posts/los-juegos-verificados-para-steam-deck-superan-ya-los-130-con-grandes-titulos-en-la-lista" | absolute_url }}) para Steam Deck, y ahora **nos encontramos a punto de pasar la barrera de los 300 títulos**. El proceso de verificación de Steam parece que va a buen ritmo aunque, salvo sorpresa no parece que vayamos a llegar a la mayoría de títulos de la enorme biblioteca de Steam, al menos sí tendremos un buen número de títulos disponibles. Además, si a los títulos seleccionados como "Verificados Steam Deck" **sumamos los que tienen categoría de jugables,** ahora mismo ya estamos hablando de **488 títulos** jugables de una u otra forma. No está mal.


A continuación, el listado de juegos verificados a fecha 11/02/2022:




| 8Doors: Arum's Afterlife Adventure |
| ACE COMBAT™ 7: SKIES UNKNOWN |
| Aeterna Noctis |
| Aliens: Fireteam Elite |
| Amnesia: Rebirth |
| APE OUT |
| art of rally |
| Automobilista 2 |
| Baba Is You |
| Babble Royale |
| BattleBlock Theater® |
| Bayonetta |
| Behind the Frame: The Finest Scenery |
| Blue Fire |
| Boomerang X |
| Bug Fables: The Everlasting Sapling |
| Business Tour - Board Game with Online Multiplayer |
| Call of Juarez: Gunslinger |
| Carto |
| CarX Drift |
| Castle Crashers® |
| Cat Quest |
| Cat Quest II |
| Caveblazers |
| Celeste |
| Chernobylite |
| Chorus |
| Circuit Superstars |
| Clue/Cluedo: The Classic Mystery Game |
| Crash Bandicoot™ N. Sane Trilogy |
| Crush Crush |
| Cuphead |
| Curse of the Dead Gods |
| Cyber Hook |
| Danganronpa 2: Goodbye Despair |
| Dark Deity |
| DARK SOULS™ II: Scholar of the First Sin |
| DARK SOULS™ III |
| Darksiders Genesis |
| Darksiders II Deathinitive Edition |
| Daymare: 1998 |
| Dead Cells |
| Dead Estate |
| DEATH STRANDING |
| Death Trash |
| Death's Door |
| DEATHLOOP |
| Demon Slayer -Kimetsu no Yaiba- The Hinokami Chronicles |
| Demon Turf |
| Desperados III |
| Despot's Game: Dystopian Army Builder |
| Devil May Cry 5 |
| Disco Elysium - The Final Cut |
| Dishonored |
| Dishonored®: Death of the Outsider™ |
| DmC: Devil May Cry |
| DOOM II |
| DRAGON QUEST® XI S: Echoes of an Elusive Age™ - Definitive Edition |
| DRAGON QUEST® XI: Echoes of an Elusive Age™ - Digital Edition of Light |
| Drawful 2 |
| Duke Nukem 3D: 20th Anniversary World Tour |
| Dungeons & Dragons: Dark Alliance |
| Dying Light |
| Eastward |
| Edge Of Eternity |
| ELDERBORN |
| ENSLAVED™: Odyssey to the West™ Premium Edition |
| Enter the Gungeon |
| Epic Battle Fantasy 5 |
| Evil Genius 2: World Domination |
| Evoland |
| Evoland 2 |
| Fallout Shelter |
| Farm Together |
| Fell Seal: Arbiter's Mark |
| FIGHT KNIGHT |
| FINAL FANTASY |
| FINAL FANTASY II |
| FINAL FANTASY III |
| FINAL FANTASY IV |
| Final Fantasy IV (3D Remake) |
| FINAL FANTASY VIII - REMASTERED |
| Fire Pro Wrestling World |
| Firewatch |
| Five Nights at Freddy's |
| Football, Tactics & Glory |
| Fox Hime Zero |
| FTL: Faster Than Light |
| Gang Beasts |
| Gas Station Simulator |
| Genital Jousting |
| Ghost Exorcism Inc |
| Ghostrunner |
| God of War |
| Graveyard Keeper |
| GRID |
| Griftlands |
| Grim Clicker |
| GRIME |
| Guacamelee! 2 |
| Gunfire Reborn |
| Hades |
| Happy's Humble Burger Farm |
| Haven |
| Heave Ho |
| Heavenly Bodies |
| Hellblade: Senua's Sacrifice |
| Hellish Quart |
| Hentai Bad Girls |
| Hentai Girl Hime |
| HITMAN™ |
| Hollow Knight |
| Horizon Zero Dawn™ Complete Edition |
| HOT WHEELS UNLEASHED™ |
| Human: Fall Flat |
| HUNTDOWN |
| HYPERCHARGE: Unboxed |
| Idle Champions of the Forgotten Realms |
| Idle Wasteland |
| INSIDE |
| Into the Breach |
| Intravenous |
| ISEKAI QUEST |
| Journey To The Savage Planet |
| Jump King |
| Katamari Damacy REROLL |
| Katana ZERO |
| Kingdom Rush Vengeance - Tower Defense |
| Kitaria Fables |
| Last Evil |
| LEGO® Harry Potter: Years 1-4 |
| LEGO® Jurassic World |
| LEGO® Marvel™ Super Heroes |
| LEGO® Star Wars™ - The Complete Saga |
| LET IT DIE |
| Life is Strange 2 |
| Life is Strange 2 - Episode 2 |
| Life is Strange Remastered |
| LIMBO |
| Littlewood |
| Loop Odyssey |
| Luck be a Landlord |
| Mad Max |
| Manifold Garden |
| Mark of the Ninja: Remastered |
| Mega Man 11 |
| METAL GEAR RISING: REVENGEANCE |
| Metal Unit |
| Middle-earth™: Shadow of War™ |
| Minoria |
| Mitsurugi Kamui Hikae |
| MO:Astray |
| Mortal Kombat Komplete Edition |
| Mortal Shell |
| MudRunner |
| Muv-Luv (マブラヴ) |
| Muv-Luv Alternative (マブラヴ オルタネイティヴ) |
| My Friend Pedro |
| NEKOPARA Extra |
| NEKOPARA Vol. 0 |
| NEKOPARA Vol. 4 |
| Ni no Kuni™ II: Revenant Kingdom |
| Nickelodeon All-Star Brawl |
| Nidhogg |
| Nobody Saves the World |
| Noita |
| Nuclear Throne |
| OCTOPATH TRAVELER™ |
| Okami HD |
| Orcs Must Die! 3 |
| Ori and the Blind Forest |
| Overcooked! 2 |
| Paint the Town Red |
| Paradise Killer |
| Pathologic 2 |
| PAYDAY 2 |
| PictoQuest |
| Pit People® |
| Portal 2 |
| Potion Craft: Alchemist Simulator |
| Power Rangers: Battle for the Grid |
| PowerWash Simulator |
| Prey |
| Prince of Persia®: The Sands of Time |
| Project CARS 3 |
| Project Warlock |
| Psychonauts 2 |
| RAD |
| Realm Grinder |
| Record of Lodoss War-Deedlit in Wonder Labyrinth- |
| Redout: Enhanced Edition |
| Remnant: From the Ashes |
| Renai Karichaimashita: Koikari - Love For Hire |
| Resident Evil 2 |
| Resident Evil Village |
| Return of the Obra Dinn |
| RIDE 4 |
| Ring of Pain |
| Risk of Rain 2 |
| Rocket League® |
| Rogue Legacy 2 |
| Roundguard |
| Ryse: Son of Rome |
| Sable |
| Sakuna: Of Rice and Ruin |
| Sakura Clicker |
| Sam & Max Save the World |
| Sayonara Wild Hearts |
| SCARLET NEXUS |
| Seed of the Dead: Sweet Home |
| Sekiro™: Shadows Die Twice - GOTY Edition |
| Session: Skateboarding Sim Game |
| Shadow Man Remastered |
| Shadow Tactics: Blades of the Shogun |
| Shining Resonance Refrain |
| Shop Titans |
| Shovel Knight Pocket Dungeon |
| Shovel Knight: Treasure Trove |
| Siralim Ultimate |
| Sniper Ghost Warrior Contracts 2 |
| Sonic Generations Collection |
| South Park™: The Stick of Truth™ |
| Spelunky 2 |
| Spiritfarer®: Farewell Edition |
| Stardew Valley |
| SteamWorld Quest: Hand of Gilgamech |
| Stick Fight: The Game |
| Styx: Master of Shadows |
| Supaplex |
| Super Meat Boy Forever |
| Super Mega Baseball 3 |
| Super Monkey Ball Banana Mania |
| Super Robot Wars 30 |
| SUPERHOT |
| SUPERHOT: MIND CONTROL DELETE |
| Supraland Six Inches Under |
| Tales of Arise |
| Tap Wizard 2 |
| Tetris® Effect: Connected |
| The Banner Saga |
| The Beast Inside |
| The Binding of Isaac: Rebirth |
| The Evil Within |
| The Falconeer |
| The Jackbox Party Pack |
| The Jackbox Party Pack 2 |
| The Jackbox Party Pack 5 |
| The Jackbox Party Pack 6 |
| The Jackbox Party Pack 7 |
| The Jackbox Party Pack 8 |
| THE KING OF FIGHTERS '98 ULTIMATE MATCH FINAL EDITION |
| THE KING OF FIGHTERS XIV STEAM EDITION |
| The Lands of Eldyn |
| The LEGO® Movie - Videogame |
| The LEGO® NINJAGO® Movie Video Game |
| The Life and Suffering of Sir Brante |
| The Messenger |
| The Room |
| The Sexy Brutale |
| The Wonderful 101: Remastered |
| Tom Clancy's Splinter Cell Chaos Theory® |
| Travellers Rest |
| Treasure of Nadia |
| Tricky Towers |
| Trine 4: The Nightmare Prince |
| TSIOQUE |
| Tunche |
| Twelve Minutes |
| TY the Tasmanian Tiger |
| UnderMine |
| Unpacking |
| UNSIGHTED |
| Until We Die |
| Untitled Goose Game |
| Vampire Survivors |
| Webbed |
| West of Dead |
| What Remains of Edith Finch |
| WHAT THE GOLF? |
| Windjammers 2 |
| WORLD OF HORROR |
| WWE 2K19 |
| Wytchwood |
| Yakuza 3 Remastered |
| Yakuza Kiwami |
| Yakuza Kiwami 2 |
| Yakuza: Like a Dragon |
| Yonder: The Cloud Catcher Chronicles |
| Yooka-Laylee and the Impossible Lair |
| Your Chronicle |
| Yu-Gi-Oh! Duel Links |
| 古剑奇谭三(Gujian3) |
| 战地指挥官/Battleboom |


 ¿Ves algún título que te llame la atención? 


Cuéntamelo en los comentarios o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

