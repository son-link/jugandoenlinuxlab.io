---
author: Pato
category: Software
date: 2022-05-17 22:07:16
excerpt: "<p>El ya popular lanzador lanza una versi\xF3n mayor con temas, auto-actualizaciones,\
  \ favoritos y mas</p>\r\n"
image: https://user-images.githubusercontent.com/26871415/168448771-accc3f8f-f800-49d4-a1ea-41e9d0745793.png
joomla_id: 1468
joomla_url: heroic-games-laucher-llega-a-la-version-2-3-1-con-multitud-de-nuevas-mejoras-y-caracteristicas
layout: post
tags:
- heroic-games-launcher
title: "Heroic Games Laucher llega a la versi\xF3n 2.3.1 con multitud de nuevas mejoras\
  \ y caracter\xEDsticas"
---
El ya popular lanzador lanza una versión mayor con temas, auto-actualizaciones, favoritos y mas


Heroic Games Launcher se ha convertido por méritos propios en un imprescindible, y no para de introducir mejoras y características nuevas. Ahora nos llega su última versión mayor, la 2.3.1 con las siguientes novedades:


#### Temas


Se trata de una de las características más solicitadas desde que comenzó el proyecto y ahora ya tenemos 5 temas integrados, algunos de ellos con distintas variaciones.


![hgl1](https://user-images.githubusercontent.com/26871415/168448759-dce78842-20e5-47f8-ac9b-10361900f685.png)


#### Selección de instalación por plataformas


Se ha añadido la capacidad de instalar versiones de Windows de un juego nativo, ya que algunas de ellas funcionan mejor con Wine / Proton que con la nativa, que a veces requiere algunas libs obsoletas.  
Esta función está disponible **solo para GOG en Linux**, ya que no hay juegos nativos para Linux en Epic actualmente.


![hgl2](https://user-images.githubusercontent.com/26871415/168445768-176c5583-3075-4567-987c-5a092a03d053.png)

#### Auto-actualizaciones

A partir de ahora, si estás usando la versión Appimage Heroic te avisará cuando haya una nueva versión estable disponible para instalar y la descargará en segundo plano. Después de reiniciar Heroic ya tendrás instalada la versión mas reciente.

![hgl3](https://user-images.githubusercontent.com/26871415/168445972-d05cddcb-ffca-42a2-8306-0009f62f8801.png)

#### Añade tus juegos a Favoritos o escóndelos

Ahora puedes organizar tus bibliotecas agregando juegos a favoritos o incluso ocultando juegos que hayas terminado o que no quieras volver a jugar.  
También se ha añadido una nueva configuración para que elijas qué mostrar en la fila de destacados de la biblioteca. Ahora puedes elegir mostrar favoritos, juegos jugados recientemente o incluso no mostrar nada.

![hgl4](https://user-images.githubusercontent.com/26871415/168445275-0767a331-89ac-4c9c-9a11-8d638d3bdb57.png)

#### Pantalla de accesibilidad

Se han añadido una pantalla para ayudar a las personas con discapacidad visual con algunas opciones como un Zoom a la interfaz y seleccionar fuentes alternativas. Más adelante añadirán nuevos temas de alto contraste.

![hgl5](https://user-images.githubusercontent.com/26871415/168445351-41a34011-8e6e-4e08-a835-ba52b067744e.png)

#### Mejoras QoL

Se han añadido mejoras en la interfaz como:

* Ahora se puede ordenar la lista de juegos alfabéticamente o por estado instalado
* Al instalar un juego, Heroic mostrará cuánto espacio queda en el dispositivo seleccionado para la instalación.
* Otras mejoras para hacer la interfaz de usuario más consistente.


Además de todo esto, se han llevado a cabo multitud de correcciones y mejoras. Si quieres verlo más en profundidad puedes visitar el proyecto en su [página de lanzamientos en Github](https://github.com/Heroic-Games-Launcher/HeroicGamesLauncher/releases).

