---
author: leillo1975
category: Plataformas
date: 2017-06-15 13:15:58
excerpt: "<p>El juego est\xE1 siendo portado por <strong>Ethan Lee</strong> para nuestro\
  \ sistema</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/45245952fc8c5c5e099a3e444bf8f32a.webp
joomla_id: 377
joomla_url: wonder-boy-the-dragon-s-trap-esta-siendo-portado-a-linux-steamos
layout: post
tags:
- wonder-boy
- the-dragon-s-trap
- ethan-lee
- dotemu
title: "Wonder Boy: The Dragon's Trap est\xE1 siendo portado a Linux/SteamOS (ACTUALIZACI\xD3\
  N)"
---
El juego está siendo portado por **Ethan Lee** para nuestro sistema

 **ACTUALIZACIÓN 12-7-17:** Acabamos de recibir la noticia via mail que el juego llegará finalmente a GNU-Linux y Mac el martes día **18 de este mes**. Una magnífica noticia para amenizarnos el verano. También está disponible en Steam a partir de hoy mismo la [banda sonora del juego](http://store.steampowered.com/app/649330/Wonder_Boy_The_Dragons_Trap__Original_Soundtrack/) por 4.99€ o 3.99€ si se compra junto con el juego.




---


 


Que tiempos!, y que buena noticia! Dando la ronda matutina a Twitter nos hemos encontrado con que el **conocido porter de juegos a Linux y Mac**, [Ethan Lee](https://twitter.com/flibitijibibo) (SuperHexagon, Capsized, Fez, Dust, Mercenary Kings, y [muchos más](http://www.flibitijibibo.com/index.php?page=Portfolio/Ports)), está trabajando codo con codo con la gente de [DotEmu](http://www.dotemu.com/game/wonder-boy-the-dragons-trap/) para traernos esta actualización del clásico Wonder Boy, el conocido juego de Master System, consola de la que también son desarrolladores de su emulador [MEKA](http://www.smspower.org/meka/) estos últimos. Aquí teneis el tweet donde se confirma:


 



> 
> I thought Omar said they were doing the port by themselves? So they got someone else involved?
> 
> 
> — Ekianjo (@BoilingSteam) [14 de junio de 2017](https://twitter.com/BoilingSteam/status/874888275655262208)



 


Siguiendo el tweet nos encontramos con este [artículo de **Boiling Steam**](http://boilingsteam.com/ethan-lee-is-porting-wonderboy-iii-to-maclinux/), por cierto, página muy recomendada (saludos [Ekianjo](https://twitter.com/BoilingSteam)), donde nos facilita más información sobre el tema. Por el momento no hay fechas de lanzamiento, pero todo pronostica que no estará muy lejos en el tiempo el poder disfrutar de este juego. La verdad es que hace ya un par de meses que [se comentó esta posibilidad en reddit](https://www.reddit.com/r/linux_gaming/comments/65odt9/wonder_boy_the_dragons_trap_linux_porting_advices/), pero nunca pensé que Ethan Lee tomaría las riendas del proyecto. Apostaba más por un port del propio [desarrollador (LizardCube)](http://www.lizardcube.com/). De todas formas, confiar el trabajo a este es una muy buena noticia dado el excelente trabajo que siempre ha demostrado.


 


En el juego nos pondremos en la piel del héroe, que tendrá la **habilidad de mutar en 5 animales diferentes**, teniendo cada uno características que nos ayudarán a superar la aventura. Durante el juego, aparte de luchar con incontables enemigos, deberemos derrotar a **varios tipos de dragones** como el Dragón Zombie, el Dragón Samurai o la Mamá Dragón. Todo ello aderezado con un inconfundible **estilo retro y gráficos pintados a mano**. Nosotros en JugandoEnLinux.com no vemos el momento de ponerle las zarpas encima. Sin más os dejo con este demostrativo trailer del título:


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/ibKf66tVoFw" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


¿Qué os parece esta remasterización del clásico? ¿Conoces los juegos portados por Ethan Lee?  Respondenos en los comentarios o en nuestro canales de [Telegram](https://telegram.me/jugandoenlinux) o [Discord.](https://discord.gg/ftcmBjD)

