---
author: Pato
category: "Acci\xF3n"
date: 2017-01-13 17:40:35
excerpt: "<p>El juego comenz\xF3 ayer su campa\xF1a de Kickstarter. Tambi\xE9n est\xE1\
  \ en Greenlight.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/f8a458e18503c86603dc02339a944feb.webp
joomla_id: 177
joomla_url: undungeon-un-juego-de-rol-y-accion-pixel-art-esta-en-campana-para-su-lanzamiento
layout: post
tags:
- accion
- indie
- rol
- greenlight
- kickstarter
title: "'UnDungeon' un juego de Rol y Acci\xF3n \"pixel art\" est\xE1 en campa\xF1\
  a para su lanzamiento"
---
El juego comenzó ayer su campaña de Kickstarter. También está en Greenlight.

Últimamente tenemos una pequeña avalancha de indies que aprovechando el tirón dela moda "pixel art" intentan hacerse un hueco en nuestras bibliotecas. Uno de los últimos juegos en llamar mi atención ha sido este 'UnDungeon' que según sus desarrolladores "se trata de un bonito y complejo juego de acción y rol "pixel art" con un buen transfondo y una inmersiva historia con 7 personajes únicos".


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/7GnALBvFKW0" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


Actualmente el juego está en campaña en Greenlight donde puedes apoyarlo para que llegue a publicarse en Steam:


<http://steamcommunity.com/sharedfiles/filedetails/?id=840553603> 


Como puedes ver tanto en el vídeo como en la campaña de Greelight el juego tendrá versión Linux. Además justo ayer comenzó su campaña en Kickstarter donde buscan fondos para el juego. Si quieres aportar puedes hacerlo [en este enlace](https://www.kickstarter.com/projects/laughingmachines/undungeon-pixelart-action-rpg-with-roguelike-eleme?ref=nav_search).


¿Que te parece este "UnDungeon"? ¿Te gustan los juegos "pixel art"?


Cuéntamelo en los comentarios o en los canales de Jugando en Linux en [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 

