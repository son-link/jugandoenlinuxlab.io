---
author: Pato
category: Aventuras
date: 2017-12-19 09:57:18
excerpt: "<p>Nuevas entradas en SteamDB as\xED lo apuntan, y algunos de ellos ya aparecen\
  \ con el logo de SteamOS</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/1c50d52dbe9254e500a5390a4a3fecb7.webp
joomla_id: 578
joomla_url: parece-que-disney-esta-trayendo-algunos-de-sus-clasicos-a-linux-steamos
layout: post
tags:
- accion
- aventura
title: "Parece que Disney est\xE1 trayendo algunos de sus cl\xE1sicos a Linux/SteamOS"
---
Nuevas entradas en SteamDB así lo apuntan, y algunos de ellos ya aparecen con el logo de SteamOS

Si te gustan los juegos de Disney de la vieja escuela, estás de enhorabuena. Parece que Disney está trayendo algunos de sus títulos clásicos a Linux/SteamOS (si bien algunos de ellos ya estaban disponibles en GOG) ya que en SteamDB han aparecido diversas entradas con juegos que en algunos casos ya muestran el logo de SteamOS en sus respectivas páginas de Steam.


Para hacernos una idea, hablamos de **títulos que aparecieron en la década de los 80 o 90**. En concreto, han aparecido 'El Rey León' (Disney's The Lion King), 'El Libro de la Jungla' (Disney's The Jungle Book) y el clásico del 87 'Maniac Mansion', que aunque fue obra de LucasArts, como bien es sabido ésta ahora es propiedad de Disney.


Por otra parte, también ha aparecido en SteamDB el clásico (en su tiempo un título notable) 'Aladdin' (Disney's Aladdin), que aún no tiene el logo de SteamOS, pero que dado que ha aparecido en el mismo momento que los deḿas títulos y que en la base de datos de Steam tiene su correspondiente contenedor Linux con información supongo que será cuestión de tiempo que acabe mostrando el soporte a nuestro sistema favorito.


Si estás interesado en sumergirte en la nostalgia y volver a jugar a estos clásicos retro, o si no los jugaste y quieres "vivir esa experiencia", tienes estos títulos con un 33% de descuento en sus respectivas páginas de Steam (en inglés, eso sí).


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/561120/" style="display: block; margin-left: auto; margin-right: auto;" width="646"><p> </p></iframe></div>


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/561110/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/529890/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 ¿Qué te parece la llegada de estos clásicos a Steam? ¿Será señal esto de que Disney esté pensando en traernos nuevos títulos a nuestro sistema favorito? ¿Cuales de los títulos de Disney te gustaría que llegaran a Linux/SteamOS?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

