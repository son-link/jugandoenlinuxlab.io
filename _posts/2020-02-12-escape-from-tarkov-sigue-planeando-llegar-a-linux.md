---
author: Pato
category: "Acci\xF3n"
date: 2020-02-12 13:34:31
excerpt: "<p>El juego de acci\xF3n online en primera persona est\xE1 actualmente de\
  \ moda</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Escapefromtarkov/Escapefromtarkov.webp
joomla_id: 1169
joomla_url: escape-from-tarkov-sigue-planeando-llegar-a-linux
layout: post
tags:
- accion
- indie
- online
title: Escape from Tarkov sigue planeando llegar a Linux
---
El juego de acción online en primera persona está actualmente de moda


Hace ya unos cuantos años que Escape from Tarkov del estudio ruso Battlestate Games está en desarrollo, y ya en 2016 [dio muestras](https://www.gamingonlinux.com/articles/escape-from-tarkov-the-new-russian-survival-mmo-fps-looks-like-its-heading-to-linux.6549) de poder llegar a Linux cosa que hasta ahora no ha sucedido.


 ¿Y por que es noticia esto? - En el contexto actual, es raro ver grandes desarrollos anunciar que planean su lanzamiento para nuestro sistema favorito, sin embargo eso mismo es lo que ha sucedido en la última [retransmisión de podcast](https://forum.escapefromtarkov.com/topic/1717-linux-support/page/16/) del estudio. Tal y como puede verse, un usuario preguntó si era posible el lanzamiento del juego para sistemas Linux, a lo que el responsable del estudio responde un escueto "está planeado".


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="378" src="https://clips.twitch.tv/embed?clip=CrowdedClearMarrowEleGiggle" width="620"></iframe></div>


Es cierto que este escueto comentario no significa gran cosa ya que puede que los planes del estudio puedan retrasarse o incluso cancelarse en un futuro, pero al menos es significativo que a día de hoy y tras cuatro años el estudio aún piense en lanzar su juego para nuestro sistema favorito.


Escape from Tarkov está actualmente de moda y bate records de audiencia en las plataformas de visionado online debido a su **acertada mezcla de acción, realismo, supervivencia y mecánicas jugables** que lo hacen acercarse casi más al género del terror que a la propia acción.


En Escape from Tarkov tendremos que tomar el papel de un mercenario que lucha por sobrevivir en la dura ciudad de Tarkov donde dos facciones tienen la ciudad dividida, y donde el objetivo es conseguir recoger los objetos necesarios para realizar nuestra misión y salir con vida de la zona enemiga.


Y ahí es donde reside el gran atractivo del juego ya que si te cazan **perderás todo el equipamiento que lleves encima**. Absolutamente todo, lo que junto a que los daños que recibimos son de lo más realistas da lugar a un juego de acción donde **el miedo a morir y perderlo todo acecha en cada esquina** dando lugar a situaciones y combates de lo más tensos que se pueden ver en un juego de acción.


Si tienes curiosidad, tienes toda la información sobre Escape from Tarkov [en su web oficial](https://www.escapefromtarkov.com/).


 

