---
author: leillo1975
category: "An\xE1lisis"
date: 2018-05-28 15:22:15
excerpt: "<p>Profundizamos en el \xFAltimo port de <span class=\"username u-dir\"\
  \ dir=\"ltr\">@AspyrMedia</span></p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/5d231997759398fd54d0c1f438d55347.webp
joomla_id: 707
joomla_url: analisis-civilization-vi-rise-fall-dlc
layout: post
tags:
- dlc
- analisis
- civilization-vi
- rise-fall
title: "An\xE1lisis: Civilization VI - Rise & Fall DLC."
---
Profundizamos en el último port de @AspyrMedia

En la vida hay momentos buenos y momentos malos, etapas en las que es mejor no asomarse por que solo se reciben capotes, y otros en donde parece que tienes el viento a favor.... y al igual que en nuestra vida, la historia de la humanidad está plagada de altos y bajos, de épocas doradas y siglos oscuros. En esto se fundamenta la última gran expansión de Civilization VI, en el Auge y la Caida (Rise & Fall). Una vez más [Firaxis Games](http://firaxis.com/) ha vuelto a rizar el rizo y propone nuevas reglas y añadidos a su particular tablero de juego, consiguiendo, aun más si cabe otorgar más profundidad y una nueva vuelta de tuerca al último capitulo de su saga Civilization.


Al igual que en el juego base, Rise & Fall ha sido portado por los tejanos [Aspyr Media](http://www.aspyr.com/), que tan fenomenal trabajo han hecho hasta ahora con todas sus actualizaciones. Cierto es que ha habido un pequeño retraso desde su salida en Windows, pero la espera ha valido la pena y ya podemos disfrutar enteramente de todas las nuevas y flamantes características de esta expansión. Antes de continuar nos gustaría **agradecer a Aspyr la clave facilitada para la elaboración de este análisis**.


En primer lugar, nos gustaría aclarar que no vamos a analizar los aspectos comunes que tiene la expansión con el juego base, sinó que vamos a centrarnos principalmente en las diferencias y añadidos que presenta, por lo que si no conoceis Civilization VI, [os animamos a que leais previamente el análisis](index.php/homepage/analisis/item/352-analisis-sid-meier-s-civilization-vi) que realizamos hace poco más de un año de esta obra maestra de la estrategia por turnos.... pues nada chicos, vamos allá.


Uno de los primeros elementos que vamos a encontrar en Rise & Fall nada más comenzar una nueva partida va a ser el añadido un buen puñado de nuevas civilizaciones donde poder escoger. Cada una obviamente tendrá sus puntos fuertes, por lo que decidir bien antes de empezar va a ser crucial, teniendo **buscar la que más se asemeje a nuestro estilo de juego** o **intentar adaptar este a sus características diferenciadas**. Estas nuevas civilizaciones son  Escocia, el pueblo Mapuche, Georgia, Nación Cri, India, Mongolia, Paises Bajos y Corea. Por supuesto, irá acompañada de su lider (Roberto I, Lautaro, Tamara, Poundmaker, Chandragupta, Gengis Khan, la reina Guillermina o Seondeok). Cada una de estas civilizaciones tendrá una unidad exclusiva potenciada que será superior a sus homólogas en el resto de civilizaciones, así como Estructuras, Habilidades de Lider o Civilización. Hasta ahora el juego se había actualizado grauitamente añadiendo nuevas Civilizaciones, pero nunca en esta cantidad. Quizás se puede achacar al juego **cierta complejidad a la hora de tomar la decisión de decantarnos por una u otra facción**, ya que la cantidad de estas es abrumadora, y a un jugador no experto puede confundirlo; pero probablemente los más avezados en esta saga, que serán la gran mayoría, aprecien esta variedad y cantidad de opciones para decidir.


![Personajes Históricos](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisCiv6/RiseFall/Civ6RFPersonajes.webp)


Este mayor número de facciones donde elegir no es la principal característica que da forma a esta expansión, ya que basicamente es lo mismo que habíamos visto hasta ahora en el juego base. Es en las nuevas mecánicas y añadidos donde estará el "intríngulis" de esta DLC. Rise & Fall se basa, como decíamos antes en las épocas de explendor y decadencia, y es ahí donde va a girar todo. Durante el desarrollo de la partida nuestro pueblo vivirá periodos de gloria en las edades de oro, y de depresión en los siglos oscuros. Cada vez que pasemos de una era a otra, según los "meritos" que hayamos cosechado en la actual podremos entrar en uno de estos dos estados. Para ello **debemos vigilar un contador circular situado en el botón de turno**. Este contador se rellenará a medida que vayamos cumpliendo diversos hitos que quedarán impresos en nuestra historia, y que podremos consultar en nuestra **Cronología**. Al comenzar una nueva época podremos seleccionar más o menos áreas en las que obtener ventajas.


Como es lógico, **en la edad de oro**, nuestra facción lo tendrá todo mucho más fácil, pudiendo aprovechar esta para expandir nuestro imperio o mejorar nuestras estructuras. Por lo contrario, **en los siglos oscuros** nuestras oportunidades se verán mermadas y tendremos que luchar contra la deslealtad, la falta de recursos o el avance de nuestros competidores. Los cambios de edad además son también estéticos, pues cuando estemos en una edad dorada los colores se verán mucho más vivos en pantalla, al contrario que en la época oscura que se mostrará con una tonalidad más ténue y apagada.


Pero sin duda, y a mi juicio, es en **la lealtad** donde encontraremos la característica más decisiva e importante de esta expansión. Gracias a esta **podremos influenciar o ser influenciados a/por otras naciones**, pudiendo conseguir anexionarnos una ciudad estado o de otra civilización. Por lo contrario, si la perdemos nuestras ciudades pueden correr la misma suerte. Obviamente, en épocas doradas, nuestra influencia será mayor en las ciudades colindantes a nuestro imperio y podremos expandir nuestro territorio sin derramar una sola gota de sangre. En cambio, en épocas oscuras, tendremos que lidiar con la deslealtad de nuestros ciudadanos y sofocar, si podemos, las posibles revueltas que se produzcan.


![Lealtad](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisCiv6/RiseFall/Civ6RFLealtad.webp)


Para ver la influencia y lealtad de una ciudad simplemente pincharemos en el icono correspondiente y veremos en la interfaz indicado con más o menos flechas (anterior pantallazo) . **Existen múltiples factores que determinan la lealtad**, entre los que se encuentra por supuesto la prosperidad y desarrollo de esta ciudad, pero también si tenemos guarnicionadas tropas en ella, si colocamos otra ciudad de nuestro imperio cerca o si tenemos ciertos edificios o proyectos como "Pan y Circo". También es posible aumentar la lealtad de la cuidad posicionando un Gobernador, y de eso vamos  a hablar en el próximo párrafo.


**Los gobernadores** son otra  parte importante de esta DLC, ya que estableciéndolos en una de nuestras ciudades no solo vamos a obtener mayor lealtad, como acabamos de comentar, sino que también vamos a obterner diversos beneficios, tales como mejor defensa de la ciudad, mejores relacciones diplomáticas con una ciudad estado, mayor crecimiento territorial del imperio, mejores condiciones financieras y comerciales, mayor influencia religiosa..... En total existen un máximo 7 gobernadores diferentes que podremos establecer en otras tantas urbes. A medida que vayamos jugando recibiremos puntos que nos permitirán reclutar un nuevo gobernador o mejorar el nivel de uno existente. Deberemos ser muy cautos en su elección y colocación para sacar el máximo provecho de ellos.


![Gobernadoress](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisCiv6/RiseFall/Civ6RFGobern.webp)


Otra característica importante del juego reside en el **área diplomática**, donde cada cierto tiempo surgirán estados de **Emergencia** donde tendremos que echar una mano a otra nación, por ejemplo para contrarrestar la expansión de otro imperio, todo ello en un número de turnos limitado . Si acudimos a ellas recibiremos importantes recompensas, y todo lo contrario si las obviamos, ya que será la facción que haya provocado la emergencia quien se lleve estas bonificaciones. También se ha trabajado enormemente en el **sistema de alianzas** del juego abriendo mucho más el abanico de ambitos en donde establecerlas para sacar provecho de ellas.


En cuanto al **apartado técnico** no difiere en nada al juego base, siguiendo sus estándares gráficos, artísticos, sonoros, etc... la misma tónica que Civilization VI, aunque como es lógico es algo que era de esperar. No hemos notado tampoco ninguna mejora destacable en cuanto al **rendimiento** en nuestro sistema, estando en la misma situación que anteriormente, donde la versión de Windows es claramente superior. Tampoco no se ha podido avanzar nada en la interoperabilidad entre los diferentes sistemas operativos en el apartado **multijugador**, no pudiendo crear partidas con jugadores de Windows. Por lo que sabemos, Aspyr afirma que sigue trabajando en ese aspecto pero hasta ahora aún no ha podido conseguirlo.


Rise & Fall **no es un cambio radical en la forma de jugar**, sinó que viene a pulir aun más si cabe la compleja jugabilidad de Civilization VI. Esta expansión tiene como misión **enriquecer lo ya visto**, proponiendo a los experimentados jugadores nuevos retos , pero sin seguir una linea rupturista con el juego base. Los que busquen una experiencia totalmente nueva y una revolución a la hora de plantear las partidas no encontrarán lo que buscan. Sin embargo, esto no quiere decir que no esté justificada la inversión, ya que todos los fans de Civilization VI os aseguro que van a saber apreciar los cambios introducidos por la gente de Firaxis. Desde nuestro punto de vista, y aunque suene a argumento repetido, si has disfrutado de la experiencia inicial, no te vas a arrepentir de adquirir Rise & Fall, pues te aseguro que va a hacer que vuelvas a disfrutar nuevamente el juego como cuando lo compraste.


En JugandoEnLinux.com hemos grabado un video donde repasamos algunas de las novedades más importantes que hemos desgranado en este análisis para que tengais una idea más gráfica de los cambios introducidos en esta expansión. Puedes verlo aquí: <div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/u6KjCDsAY8k" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


Si quieres  comprar Civilization VI: Rise & Fall DLC puedes hacerlo en Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/645402/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


¿Qué te ha parecido esta nueva expansión de Firaxis Games? ¿Te parecen adecuados e interesantes los cambios introducidos en ella? Déjanos tu opinión sobre Rise & Fall en los comentarios o en nuestro grupos de [Telegram](https://t.me/jugandoenlinux) o [Discord](https://discord.gg/fgQubVY).

