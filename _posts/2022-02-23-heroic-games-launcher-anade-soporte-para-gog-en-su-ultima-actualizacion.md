---
author: Pato
category: Software
date: 2022-02-23 20:48:56
excerpt: "<p>El gestor ampl\xEDa su cat\xE1logo de tiendas, aunque a\xFAn en modo\
  \ beta</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/HeroicGamesLauncher/heroic-gog.webp
joomla_id: 1433
joomla_url: heroic-games-launcher-anade-soporte-para-gog-en-su-ultima-actualizacion
layout: post
title: "Heroic Games Launcher a\xF1ade soporte para GOG en su \xFAltima actualizaci\xF3\
  n"
---
El gestor amplía su catálogo de tiendas, aunque aún en modo beta


Según leemos en su última actualización, **Heroic Games Launcher** el gestor de bibliotecas de juegos conocido por su soporte para la Epic Game Store sigue evolucionando y ahora acaba de ampliar su soporte a **GOG** (Good Old Games) si bien avisan que **aún consideran el soporte en fase beta** y puede que aún surjan problemas, por lo que si lo usas sabes a que atenerte, y de paso si ves cualquier problema lo puedes reportar para solucionarlo.


Entre las características incluidas, destacan:


* Características para juegos GOG
* Descarga de juegos con Galaxy API (Windows y macOS)
* Descarga de juegos con instaladores fuera de línea (Linux)
* Capacidad para elegir un lenguaje de juego para juegos que lo admitan
* Descarga de DLC de juegos (podría no funcionar correctamente con los antíguos repositorios V1)
* La capacidad de ejecutar juegos nativos de Linux usando Steam Runtime Scout (corrige problemas con el error no encontrado de Baldur's Gate libssl) requiere Steam
* De ahora en adelante se requiere al menos una cuenta (no tienes que usar GOG o Epic si no lo deseas)
* Se agregó el indicador "corredor" (tienda de juegos) en GamePage


 En cuanto a los cambios:


* [General] Beta: implementación de GOG por @imLinguin en # 872
* [General] Reparado el reinicio heroic + actualización Electron por @flavioislima en # 995
* [General] Agregueado el enlace de la tienda GOG en barra lateral por @flavioislima en el # 997
* [General] Modo fuera de línea solo alternable si lo permite @Nocccer en # 970
* [General] Habilitado useUnknownInCatchVariables para prohibir las llamadas de registro con variables de captura de cadena de ningún tipo por @Nocccer en # 998
* [General] Correjido la verificación de la opción para la actualización al inicio por @Nocccer en # 980
* [Windows] Arreglado la creación del archivo de registro por @Nocccer en # 979
* [Mac] Mejor instalador en Mac por @ olek-arsee en # 960
* [Windows] Creación de ruta de manifiesto fija para sincronización EGS por @Nocccer en # 983
* [Fix] URL externas que no se abren con @imLinguin en # 994
* Actualización de traducciones de Weblate alojado por @weblate en # 988
* Electrón actualizado a v17.0.1


Lo cierto es que Heroic como proyecto está cogiendo carrerilla, y a este paso desbancará a otros proyectos del estilo a poco que se lo proponga.


Toda la información y descargas en su página web del proyecto [en este enlace](https://github.com/Heroic-Games-Launcher/HeroicGamesLauncher/releases/tag/v2.2.0).

