---
author: Serjor
category: Ofertas
date: 2018-10-28 07:54:27
excerpt: "<p>Fieles a su cita anual con el d\xEDa de los desarrolladores, Humble Bundle\
  \ nos trae una nueva promoci\xF3n</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/75b44f26b45cdcf3e7750518b026a835.webp
joomla_id: 882
joomla_url: humble-day-of-the-devs-bundle-2018
layout: post
tags:
- humble-bundle
title: Humble Day of the Devs bundle 2018
---
Fieles a su cita anual con el día de los desarrolladores, Humble Bundle nos trae una nueva promoción

Tengo que seros sincero, creo que desde que la gente de Humble Bundle sacó el tema de los partners, solamente veo tweets y artículos con el partner de rigor por cada bundle que sacan. Así que por sumarnos a la moda del resto del mundo, os traemos el enlace del último bundle con nuestro partner: [HUMBLE DAY OF THE DEVS BUNDLE 2018](https://www.humblebundle.com/games/day-of-the-devs-2018?partner=jugandoenlinux).


En este bundle, que tiene su origen el ["Day of the Devs"](http://www.dayofthedevs.com/) que tendrá lugar en San Francisco el próximo 11 de noviembre,  podemos encontrar títulos nativos para GNU/Linux como:


* Full Throttle Remastered
* Hotline Miami 2: Wrong Number
* Yooka-Laylee
* Minit
* Hyper Light Drifter


Y tú, ¿te harás con algún juego del bundle o viajarás a San Francisco? Cuéntanoslo en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD)

