---
author: leillo1975
category: Entrevistas
date: 2017-09-15 16:09:50
excerpt: <p>Uno de los desarrolladores de este juego libre responde a nuestras preguntas.</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/a069d384dea5a245079c87294bb310e9.webp
joomla_id: 465
joomla_url: entrevista-a-flavio-calva-de-ya2-yorg
layout: post
tags:
- entrevista
- open-source
- yorg
- ya2
- flavio-calva
title: Entrevista a Flavio Calva de Ya2 (YORG).
---
Uno de los desarrolladores de este juego libre responde a nuestras preguntas.

Recientemente descubriamos gracias a la colaboración de nuestra comunidad  la existencia de **YORG**. Nuestro colega Serjor le dedicaba [un excelente artículo](index.php/homepage/generos/carreras/item/580-yorg-un-juego-de-carreras-open-source), y también grabábamos [un video para nuestro canal de Youtube](https://youtu.be/6AgXGZyasss) donde os lo enseñábamos. Como os describiamos en el artículo YORG es un juego Open Source que en este momento está en pleno desarrollo. Vimos que el juego llamó mucho la atención entre nuestros lectores y nos decidimos a ponernos en contacto con [Ya2](http://www.ya2.it/), que es al grupo de personas que lo desarrollan. Por sorpresa hemos recibido una pronta contestación por parte de Flavio Calva, fundador de Ya2 y programador de YORG. Esta es la entrevista:


 


**-JugandoEnLinux (JeL):** Hola, primero voy a presentarme, soy leillo1975 y escribo en <https://jugandoenlinux.com/>. Nosotros escribimos sobre todo lo relaccionado con los videojuegos en Linux/SteamOS en español. recientemente hemos hablado en nuestra web sobre vuestro juego, y me gustaría haceros una entrevista para nuestros lectores sobre vuestro interesante proyecto.


**-Flavio Calva (FC):** *Hola leillo! Muchas gracias por vuestro artículo, video y esta entrevista! Estoy MUY feliz por ello!. He visto vuestra web, felicidades por ello, vuestros contenidos son geniales.*


 


**-JeL:** ¿Puedes darnos una pequeña explicación sobre vuestro proyecto?


**-FC:** *Yorg empezó en 2015, y es mi segundo juego. Dicidí desarrollar un juego de carreras porque es mi género favorito (suelo juegar este tipo de juegos con mis dos pequeños sobrinos). Después de un par de meses, **Luca** (el artista) contactó para colaborar conmigo: él estaba aprendiendo Blender y quería implementar sus estudios. Así que empezamos a trabajar juntos en un prototipo, y este tenía buena pinta. Luego,decidimos empezar a desarrollar YORG. Después de algunos meses entraron en el proyecto **Jay** (un músico que se nos acercó para darnos una banda sonora original para Yorg), **Jeff** (nuestro colega del Blog)  y  **Dario** (nuestro testeador interno).*


 


 ![yorg 06 1](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/YORG/yorg_06_1.webp)


 


**-JeL:**  ¿Habeis trabajado antes en otros proyectos (juegos, software)?


**-FC:** *Me gano la vida programando. Yorg es el único juego que estoy desarrollando ahora mismo. Luca y Jay trabajan en varios proyectos. Jeff y Dario trabajan en tecnologías de la información (IT).*


 


**-JeL:** ¿Qué distribuciones de Linux usais para desarrollar YORG?


**-FC:** *Yorg esta desarrollado usando Ubuntu (debo usarlo por mi trabajo). De todos modos mi distribución favorita es Debian. ;)*


 


**-JeL:** ¿Qué motor y herramientas usais principalmente para desarrollar YORG?


**-FC:** *Yorg esta desarrollado usando **Panda3D**, y usa **Bullet** para las físicas. Los modelos están hechos usando **Blender**, y las texturas con **GIMP**.*


 


![yorg 06 8](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/YORG/yorg_06_8.webp)


  
**-JeL:** ¿Como es el proceso de desarrollo? Hablanos un poco sobre ello.


**-FC:** *Actualmente no tenemos un proceso estricto de trabajo. Resumiendo, en cada versión recopilamos las opiniones de los usuarios y añadimos lo que decidimos considerar (generalmente intentamos considerar cada sugerencia) en nuestra [hoja de ruta](https://github.com/cflavio/yorg/blob/master/project.py) , clasificando y agregando tareas para versiones importantes. Luego,  yo desarrollo los elementos de esa hoja de ruta. La comunicación se realiza en [nuestra lista de correo](http://www.ya2.it/feed-following/).* 


*Para los gráficos, Luca prepara un borrador del circuito, y  lo testeamos juntos. Nosotros lo modificamos hasta que él satisface algunos requerimientos (tamaño, diversión, ...). Luego Luca lo pule, añade modelos, texturas, .. después, el trabajo de Luca es la entrada para el trabajo de Jay, donde el compone una canción viendo los elementos de arte, produciendo algo que cuadre adecuadamente. Mientras, Jeff prepara los artículos del Blog y el material de comunicación. Darío, a la vez, hace algunos tests del juego.*


 


**-JeL:** ¿Cual es el estado actual del proyecto en este momento?


**-FC:** *Acabamos de lanzar la versión 0.7, en ella hay siete coches, cuatro circuitos, daños en los vehiculos, Inteligencia Artificial y armas.*


 


**-JeL:** ¿Con qué frecuencia lanzais actualizaciones? ¿Teneis algún plan de lanzar alguna pronto? Hablanos sobre las futuras características.


**-FC:** *"Cuando este listo" (cit: "When it's done") :D  De todos modos soy fan del principio [RERO](https://es.wikipedia.org/wiki/Release_early,_release_often), por lo que nosotros intentamos hacer lanzamientos a menudo , incluso versiones imperfectas, ya que consideramos los comentarios de los usuarios como la más importante entrada de información para dirigir nuestro proyecto. En el futuro, puliremos algunos características, haremos campeonatos y el modo multijugador.*


 


**-JeL:** ¿Cuales son vuestras metas a corto y largo plazo? ¿Cual es el objetivo final del proyecto?


**-FC:** *Ohh, no lo se! :D Estoy haciendo esto para aprender y divertirme, principalmente.  No se cual es el objetivo final del proyecto. Vendrá determinado por las sugerencias de los jugadores, ¡confio enteramente en ellos! :)*


 


![yorg 06 6](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/YORG/yorg_06_6.webp)


 


**-JeL:** Hemos visto en un [video "viejo"](https://youtu.be/diuNJIwUbno) que vuestro juego tenía una cámara con perspectiva trasera. ¿Incluirá el juego esta característica en un futuro? Creo que sería interesante que incluyerais esto como una opción.


**-FC:** *Ohh, Me gustaría insertar montones de opciones de cámara, si! De todos modos, hay características más importantes en este momento, por lo que será pospuesto. Nosotros implementamos una característica de cada vez, por lo que lo incluiremos cuando sea lo más importante para el proyecto en ese momento.*


  
**-JeL:** ¿Teneis algún tipo de ayudas en vuestro proyecto? Quiero decir cosas como contribuciones económicas, feedback de los usuarios, etc.


**-FC:** *Tenemos un montón de comentarios útiles de los usuarios, nunca pensé que generaramos tanto interés.  Recibimos también algunas donaciones hasta ahora, pero nuestro [Patreon](https://www.patreon.com/ya2) y la campaña de [Salt](https://salt.bountysource.com/teams/ya2) no han terminado de despegar todavía (sabemos que aun somos bastante desconocidos). Por lo tanto, estamos pagando el alojamiento web y los servidores con nuestro dinero y las donaciones que recibimos ... Espero que podamos recibir más apoyo en el futuro, sería una verdadera ayuda para nosotros.*


 


**-JeL:** ¿Como estais viendo la escena de juegos libres en Linux?


**-FC:** *Ohh, me encanta!. Hay montones de maravillosos proyectos, con muchos programadores y artistas muy talentosos detrás de ellos. Muchos de ellos son los mejores que he visto. Por lo tanto, si hay talento, lo que debemos esperar todos es que mantengan sus proyectos en marcha. Obtener "combustible" finaciero para proyectos de código abierto puede ser más duro que para los de código cerrado, por lo que muchos desarrolladores deben asignar su tiempo en otros proyectos (también estoy hablando de proyectos sobre motores y middleware). Pero hay buenas noticias aquí y allá; por ejemplo, La campaña de Petreon de **Godot** está yendo muy bien , y estoy muy contento por ello. Esto demuestra que los desarrolladores pueden asignar más tiempo en sus proyectos abiertos si ellos lo merecen.*


 


**-JeL:** ¿Cual es tu opinión sobre la llegada de los juegos privativos/cerrados  a Linux? ¿Lo ves como algo positivo o negativo?


**-FC:** *Definitivamente positivo. Incluso el código del juego es cerrado, un montón de subsistemas o librerias son abiertas, por lo que ellos también van a beneficiar a los proyectos de software libre (con reportes de bugs, contribuciones...). Además más juegos y usuarios implican mejor soporte de drivers. Por supuesto preferiría más juegos de código abierto en Linux, pero puedo ver montones de razones por las que alguien quiera cerrar el código. En mi caso, mi primer juego ([Plith](http://www.ya2.it/plith/)) fué cerrado, ya que sería embarazoso para mi abrirlo (su código no es bueno y no ayudaría a nadie). Otros desarrolladores pueden preferir monetizar sus proyectos de la "forma clásica"*


 


![yorg 06 9](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/YORG/yorg_06_9.webp)


 


**-JeL:** Recientemente Super Tux Kart fué aprobado por la comunidad en Steam Greenlight, y el será lanzado en esta plataforma en el futuro. ¿Estás planeando usar plataformas como Steam o GOG para distribuir tu juego? ¿Qué piensas sobre estas plataformas?


**-FC:** *Bien, nosotros consideraremos publicar Yorg en otras plataformas de distribución si/cuando haya suficiente demanda para ello. Yo creo que esas plataformas están hechas a medida para proyectos cerrados, y los proyectos libres son una "bestia" diferente. Los juegos libres nunca están acabados, ellos tienen un ciclo de vida diferente: ellos no son vendidos al terminar el desarrollo, son desarrollados con sus comunidades. El sistema de clasificación de estas plataformas (que penaliza al juego publicado demasiado pronto - podría ser mejor si los votos fueran compensados considerando como es de reciente el juego), y la falta de un modo de acceso anticipado (donde el jugador sabe que está jugando un proyecto inacabado), son aspectos que, si se modifican, pueden mejorar el soporte a los juegos abiertos mucho.*


 


**-JeL:** ¿Qué clase de juegos teneis como referencia o inspiración para vuestros desarrollos? ¿Cuales son vuestros juegos favoritos?


**-FC:** *Mi ordenador favorito fué un AMIGA 500, por lo que estoy influenciado por los juegos de esa época. En especial, adoro **Micromachines** (1.0! :D) y **Super Skidmarks**, así que creo que están definiendo Yorg más o menos conscientemente. También me encantanlos juegos de plataformas (**Superfrog** y **Zool** son maravillosos). Obviamente yo jugué algunas joyas de otros juegos, por ejemplo, **The Secret of Monkey Island**, **Civilization** o **Football Manager**. Hablando sobre juegos de código abierto, estoy jugando en este momento a **Super Tux Kart** y **Super Tux**, y me encantan. Pero también descargo y juego a otros juegos libres en cuanto tengo algún momento.*


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/TdUYLLH5Byg" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


*El primer Micromachines y Super Skidmaks son claras influencias para el desarrollo de YORG*


 


**-JeL:** Si tu quieres, puedes decir lo que quieras sobre vuestro proyecto u vosotros mismos, o dejar un mensaje a nuestros lectores.


**-FC:** *En primer lugar, gracias por leer esto! Y gracias por acogernos en vuestra gran web. Estoy muy orgulloso y feliz por ello! Gracias por la oportunidad de daros un mensaje: si tu estás leyendo esto es por que amas los juegos para Linux, ¡apóyalos! Comparte sus noticias, ayuda a los desarrolladores con tus sugerencias! Todo el mundo puede ayudar a mejorar los juegos para Linux, tanto desarrolladores como jugadores!*


 


...Y con esto terminamos esta entrevista a los creadores de YORG. Espero que os haya gustado. Como os dice el, vosotros también sois parte de esto, por lo que **seguid apoyando este movimiento**, ya que de nada vale que ellos creen si nosotros no jugamos. Desde JugandoEnLinux.com nos gustaría agradecer a Flavio Calva por haber respondido con tanta pasión a nuestras preguntas. Nosotros por supuesto seguiremos informando de todas las noticias que genere este proyecto y esperamos con impaciencia la posibilidad de poder jugar en un futuro todos juntos en modo multijugador. También os recomendamos que os [descargueis](http://www.ya2.it/yorg/) y jugueis a este juego, y **sigamos ayudando con nuestro feedback** a sus desarrolladores. Os dejamos con el video que grabamos hace unos días comentando el juego:


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/6AgXGZyasss" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


¿Qué os parece esta entrevista? ¿Habeis probado YORG? Dejanos tus impresiones en los comentarios de esta entrevista o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

