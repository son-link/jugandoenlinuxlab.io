---
author: Pato
category: "Acci\xF3n"
date: 2017-05-19 17:11:21
excerpt: "<p>Si te van los juegos de acci\xF3n y plataformas \"nihilista\" \xA1est\xE1\
  s de enhorabuena!. \xBFQue significa \"nihilista\"?... Buena pregunta.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/f84d217853d263e771f2d4ffc4c6fcef.webp
joomla_id: 332
joomla_url: nongunz-un-juego-de-accion-y-plataformas-nihilista-ya-esta-disponible-en-linux-steamos
layout: post
tags:
- accion
- plataformas
- pixel
- gore
- violento
title: "'Nongunz' un juego de acci\xF3n y plataformas \"nihilista\" ya est\xE1 disponible\
  \ en Linux/SteamOS"
---
Si te van los juegos de acción y plataformas "nihilista" ¡estás de enhorabuena!. ¿Que significa "nihilista"?... Buena pregunta.

Estamos ante uno de esos juegos que no dejan indiferente. Nongunz [[web oficial](http://www.nongunz.com/)] se apoya en un llamativo y característico estilo visual para adentrarnos en un juego de acción y gore como pocos. A primera vista, me recuerda mucho a juegos de mi infancia, cuando jugaba en los monitores de tubo con los gráficos monocromáticos de los Amstrad y Spectrum. ¿A vosotros no?


Moriremos mucho, realizaremos muchos combos y los tiros, vísceras y pulsos acelerados están garantizados.. Juzga por ti mismo:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/i-u1IMpcEEM" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


 


#### **Sinopsis de Steam:**



> 
> Nongünz es un juego de plataformas de acción nihilista que combina frenéticos tiroteos con la gestión de un misterioso juego inactivo.
> 
> 
> Cada partida te hará salir de un cementerio en blanco y negro y adentrarte en una mazmorra gótica siempre cambiante y llena de pesadillas y visceras humanas. Sobrevivir a este roguelike no sólo requerirá habilidad si no también estilo, ya que Nongünz te recompensa por realizar largos combos y desafíos ... además, la puntuación jugará un papel clave en el progreso del juego: la formación de tu propia banda de Almas perdidas y adoradores de la muerte.
> 
> 
> 


'Nongünz' es obra del estudio "Brainwash Gang" y está editado por "Sindiecate Arts". Si te va la marcha, puedes conseguirlo en español en su página de Steam con un 15% de descuento por su lanzamiento:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/633130/" width="646"></iframe></div>


¿Qué te parece 'Nongünz'? ¿Te llama la atención?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 

