---
author: Pato
category: Noticia
date: 2020-04-08 17:42:04
excerpt: "<p>Adem\xE1s, ofrece su servicio Stadia Pro gratis para los nuevos suscriptores\
  \ durante dos meses</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Stadia/stadia.webp
joomla_id: 1203
joomla_url: stadia-por-fin-abre-su-servicio-de-juego-por-streaming-a-todo-el-que-quiera-registrarse
layout: post
title: Stadia por fin abre su servicio de juego por streaming a todo el que quiera
  registrarse
---
Además, ofrece su servicio Stadia Pro gratis para los nuevos suscriptores durante dos meses


 


Ha llegado el momento que muchos estábamos esperando. Y es que Phil harrison, **vicepresidente de Stadia** ha anunciado hoy que el servicio de streaming de videojuegos de Google comienza a estar disponible para nuevos suscriptores sin necesidad de adquirir el paquete Premiere Edition. 



> 
> To help people connect with friends online & have some fun during these challenging times, we're opening up Stadia for everyone. You'll also get 2 months of free access to Stadia Pro with some free games to play!  
>   
> Check out our blog for all the details → <https://t.co/n27t4Pmnn4>
> 
> 
> — Stadia (@GoogleStadia) [April 8, 2020](https://twitter.com/GoogleStadia/status/1247918226530418694?ref_src=twsrc%5Etfw)


  





Para poder disfrutar de Stadia tan solo tendremos que entrar en la [página web de Stadia](https://stadia.google.com) y registrarnos. Eso sí, **es posible que aún no esté disponible el registro en nuestra zona**, ya que Harrison avisa en el comunicado que el servicio se irá abriendo en los 14 países disponibles de forma escalonada en las próximas 48 horas, por lo que tenemos que armarnos de paciencia si aún no está disponible el registro.


Por otra parte, **para todos los nuevos suscriptores del servicio, Stadia ofrece dos meses de Stadia Pro gratuitos** con los que podemos jugar a los juegos gratis que ofrece dicha suscripción además de la resolución 4K y sonido envolvente para los juegos que lo soporten, y con permiso de nuestro ancho de banda de nuestra conexión a internet, claro está.


Una vez que se nos acaben los dos meses gratis, **seremos libres de [darnos de baja](https://support.google.com/stadia/answer/9609879?hl=es) del servicio Pro** con lo que nos quedaremos con el servicio básico de juego a 1080p y sonido estéreo, y como es lógico perderemos el acceso a los juegos **gratuitos** a los que da derecho esta suscripción, pero **seguiremos pudiendo jugar a los juegos que hayamos comprado en la plataforma sin ningún problema**. Si en un futuro decidimos renovar la suscripción a Stadia Pro volveremos a disponer de nuestros juegos gratuitos y los progresos que hubiésemos hecho en ellos.


Por otra parte, la oferta de los nuevos suscriptores también afectará a los que ya lo eran, de modo que **Stadia no les cobrará durante dos meses la suscripción**. Genial, ¿no?


Con esta apertura e iniciativas de promoción esperemos ver crecer el servicio.


Recordaros que Stadia es un servicio de juego en Streaming desde los servidores de Google Stadia con sistema base Debian y API Vulkan, y que podemos disfrutar desde nuestros sistemas Linux de escritorio con un navegador con base Chromium.


Puedes ver el comunicado [en este enlace](https://blog.google/products/stadia/try-stadia-free-today).


 


 

