---
author: leillo1975
category: Carreras
date: 2020-03-09 15:12:15
excerpt: "<p><span class=\"css-901oao css-16my406 r-1qd0xha r-ad9z0x r-bcqeeo r-qvutc0\"\
  >@ya2tech nos desgrana el trabajo de estos \xFAltimos meses<br /></span></p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/YORG/yorg.webp
joomla_id: 1187
joomla_url: yorg-anuncia-montones-de-novedades-para-su-proxima-version
layout: post
tags:
- open-source
- yorg
- ya2
- flavio-calva
title: "YORG anuncia montones de novedades para su pr\xF3xima versi\xF3n (ACTUALIZADO)"
---
@ya2tech nos desgrana el trabajo de estos últimos meses  



**ACTUALIZADO 4-5-20**: Hace unos días nuestro amigo [Flavio Calva](index.php/component/search/?searchword=Flavio%20Calva&searchphrase=all&Itemid=828) nos hacía llegar a nuestro correo [nueva información](https://www.ya2.it/posts/may_2020.html?utm_source=feedburner&utm_medium=email&utm_campaign=Feed%3A+ya2tech+%28Ya2+%C2%BB+News%29) sobre el desarrollo de YORG, y una vez más nos ponía los dientes largos con la esperada siguiente versión. Además de reiterar el trabajo sobre el juego que nos comentaba en la anterior ocasión, tambiér ha estado trabajando en lo que será su próximo proyecto que reutilizará el código de YORG.


Aunque aun **no sabemos cual será su nombre**, si nos avanza que **será de código abierto**, al igual que  YORG. De lo poco que sabemos da la impresión que será un proyecto más ambicioso que YORG, al menos en el aspecto técnico, pues tal y como menciona en la comunicación, ha añadido **PBR** (Renderizado Basado en Físicas) gracias a [SimplePBR](https://github.com/Moguri/panda3d-simplepbr) (Moguri). También, gracias a esto, ha empezado a modelar algunos objetos del juego como el modelo preliminar de coche llamado [Panda](https://www.panda3d.org/), en honor al Framework de gráficos que usa para crear los juegos :  
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/WY89_KAAfdU" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


Seguiremos pendientes como siempre a toda la información relativa a este nuevo proyecto y por supuesto a [YORG](https://www.ya2.it/pages/yorg.html).




---


**NOTICIA ORIGINAL:**  
 Medio año llevábamos sin noticias desde Italia, concretamente desde la salida de su [última versión oficial, la 0.11](index.php/component/k2/2-carreras/1228-ya-esta-aqui-yorg-0-11), que ya supuso un hito en cuanto a novedades, principalmente encaminadas a mejorar la jugabilidad y experiencia del usuario. Ahora, mientras esperamos por la siguiente versión, [Flavio Calva](index.php/component/k2/35-entrevista/587-entrevista-a-flavio-calva-de-ya2-yorg), de Ya2 nos da un avance de los trabajos que está realizando en este divertido juego Open Source. Estos [cambios](https://www.ya2.it/posts/march_2020.html?utm_source=feedburner&utm_medium=email&utm_campaign=Feed%3A+ya2tech+(Ya2+%C2%BB+News)) son los siguientes:


*- **Nueva web** impulsada por Sphinx, lo que le permite tener mayor control sobre ella y su código.*


*- En Yorg se ha añadido **vibración en las armas y choques*** 


*- Se pueden **usar simultaneamente mandos y teclado**.*


*- Existe una nueva sección en el **menú para configurar los botones de los mandos***


*- Habrá **soporte para AppImage**, lo que facilitará enormemente su distribución*


*- Se han hecho **correcciones** en el **uso de la CPU**, la **inteligencia artificial** y la **inercia de la cámara**.*


*- **Se han quitado ciertas dependencias** como bson, feedparser, yaml*


*- **Se ha optimizado el uso de la red**.*


*- **Se ha creado un módulo yRacing***


En cuanto al último punto, es hay donde encontramos una importante noticia, y es que **YORG se divide en módulos**, yorg, yyagl y yracing. Esto se hace para poder **reutilizar el código en otros proyectos**. El primer módulo, **yorg,  contendría código específico del juego** , y donde **yyagl es una biblioteca que contiene algunas utilidades**, e **yracing contiene código para juegos de carreras**. La razón de esto es obvia, y es poder realizar otros juegos aprovechando estos módulos ya hechos. Con respecto a esto último, Flavio nos confirma que a parte de seguir trabajando y manteniendo YORG, **comenzará a elaborar un nuevo juego de carreras usando las nuevas características de las últimas versiones de Panda3D**. Para ello está adaptando el código de Yorg para no tener que "reinventar la rueda".


La verdad es que estas noticias nos llenan de alegría y ya estamos deseando incarle el diente tanto a la nueva versión de YORG como tener más información sobre ese futuro juego, y por supuesto en cuanto nos enteremos estaremos aquí para contártelo.


¿Qué te parece el proyecto YORG? ¿Como crees que será el proximo juego de Ya2? Podeis darnos veustra opinión sobre esto en los comentarios de este artículo, en nuestra comunidad de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).


 


 

