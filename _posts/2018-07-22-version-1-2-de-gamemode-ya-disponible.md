---
author: Serjor
category: Software
date: 2018-07-22 07:39:28
excerpt: <p>Poco a poco esta interesante herramienta va adquiriendo forma</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/3a67af25f1556a3ef945893aea5519d9.webp
joomla_id: 806
joomla_url: version-1-2-de-gamemode-ya-disponible
layout: post
tags:
- feral-interactive
- feral
- gamemode
title: "Versi\xF3n 1.2 de GameMode ya disponible"
---
Poco a poco esta interesante herramienta va adquiriendo forma

Nuestro compañero LordGault nos dejaba en el canal de telegram el siguiente aviso:


 







Cómo podemos leer en las [notas de la versión](https://github.com/FeralInteractive/gamemode/releases/tag/1.2) la lista de cambios aunque no es muy larga, es interesante:


* Se puede activar el servicio a través de D-BUS en vez de tener que usar systemctl
* Se puede guardar en  la configuración el ¿gobernador? (governor para que nos entendamos) por defecto y deseado
* Al cambiar de modo se guarda cuál era el governor antes de cambiar
* Para aquellos kernels que soporten SCHED_ISO en su scheduler, gamemode cambiará aumentará la prioridad del proceso del videojuego (hará un renice para que gane prioridad frente otras tareas)


 


Por ahora se puede obtener el código desde github: <https://github.com/FeralInteractive/gamemode/releases/tag/1.2>


Y tú, ¿usas GameMode en tu sistema? ¿Notas alguna mejora? Cuéntanoslo en los comentarios o en los canales de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

