---
author: Pato
category: Realidad Virtual
date: 2017-06-22 12:54:17
excerpt: "<p>Los nuevos mandos se asemejan a los que tiene Oculus con sus caracter\xED\
  sticas propias. El documento parece destinado a desarrolladores</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/a786836489dab4f04d53706ec376ba50.webp
joomla_id: 384
joomla_url: steam-desvela-los-nuevos-controladores-para-realidad-virtual-en-su-ultima-actualizacion-de-drivers-beta
layout: post
tags:
- realidad-virtual
- hardware
title: "Steam desvela sus nuevos controladores para Realidad Virtual en un documento\
  \ de configuraci\xF3n"
---
Los nuevos mandos se asemejan a los que tiene Oculus con sus características propias. El documento parece destinado a desarrolladores

Steam sigue apostando fuerte con la Realidad Virtual, y con el desarrollo de hardware para entornos virtuales. Después de añadir a LG para su próximo dispositivo HMD ahora nos llegan noticias gracias a [uploadvr.com](https://uploadvr.com/valves-steamvr-kunckles-controllers-revealed-setup-guide/) de su nuevo hardware para realidad virtual. Se trata de unos nuevos mandos que han sido desvelados en un [documento de configuración](http://steamcommunity.com/sharedfiles/filedetails/?id=943406651) destinado a desarrolladores en los que se puede ver el aspecto y funcionalidades de estos.


![Knuckes](http://cloud-3.steamusercontent.com/ugc/847089277720042940/C70077FF647E71BDCB41611653DCA757E88617F5/)


En cierto modo recuerdan mucho a los que emplea Oculus en su Rift, salvo que en este caso hacen uso de tecnologías propias de Valve como los paneles hápticos como los de los Steam Controllers. Puedes ver todos los detalles en el documento de configuración.


Además de esto, ya hay una rama de drivers en desarrollo dentro de Steam para estos dispositivos, tal y como puede verse desde [SteamDB](https://steamdb.info/sub/172948/).


Está claro que la apuesta de Valve va muy en serio con la realidad virtual. Esperemos que sigan evolucionando en el entorno y los drivers en Linux para poder estar a la par que en otros sistemas.


¿Que piensas de los nuevos mandos? ¿Te parecen ergonómicos? ¿Piensas probar la Realidad Virtual en Linux?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 

