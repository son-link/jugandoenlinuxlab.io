---
author: leillo1975
category: Estrategia
date: 2017-09-13 18:48:38
excerpt: "<p>Acaban de anunciar que no habr\xE1 versi\xF3n de su juego para nuestro\
  \ sistema.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/77df7a7d1a333dea87cc5a7a24bfa2c8.webp
joomla_id: 463
joomla_url: los-desarrolladores-de-banished-abandonan-el-port-de-linux
layout: post
tags:
- cancelacion
- banished
- shining-rock-software
- vergueenza
title: Los desarrolladores de Banished abandonan el port de Linux.
---
Acaban de anunciar que no habrá versión de su juego para nuestro sistema.

Acabo de leer en [GamingOnLinux](https://www.gamingonlinux.com/articles/city-building-strategy-game-banished-is-no-longer-having-a-linux-port.10343) que [Shining Rock Software](http://www.shiningrocksoftware.com/), la compañía que está detrás del desarrollo del "city-builder" Banished, ha anunciado que finalmente **ha abandonado el desarrollo** del prometido port para Linux. Como todos sabreis la versión para nuestro sistema de este juego ha sufrido idas y venidas, [parones en el desarrollo](index.php/homepage/editorial/item/234-lo-que-pudo-haber-sido-y-no-fue), y los más diversos problemas. [Hace unos meses dabamos cuenta](index.php/homepage/generos/estrategia/item/331-banished-para-linux-da-senales-de-vida) de la intención de los desarrolladores de terminar la transformación de su juego a OpenGL para poder sacar un port.  La compañía aduce a que el ciclo de desarrollo de Banished ha acabado y que solo sacarán correcciones de problemas graves, por lo que **paran el desarrollo de una build para Linux y Mac** para centrarse en nuevos proyectos. Podeis ver el [mensaje completo en su blog.](http://www.shiningrocksoftware.com/2017-09-13-1-0-7-released/)


 


A partir de este momento **voy a dar mi opinión personal** sobre todo esto. Me parece que **tirar a la basura todo el trabajo que han hecho hasta ahora para ejecutar su juego en Linux es algo que no tiene nombre**, y me hace plantearme seriamente si en realidad en algún momento este estudio ha tenido una versión del juego tan avanzada como ellos decían. No se por que se han molestado tanto en dar esperanzas a la gente en tantas ocasiones como lo han hecho, y me da verdadera pena que las explicaciones que den sean tan parcas y no pidan disculpas por ilusionar a tanta gente y finalmente cancelarlo. Realmente es una pena y **siento una gran decepción** al tener que dar esta noticia, pues es un juego al que sigo desde hace algún tiempo y en el que tenía la esperanza de ver algún día en mi biblioteca.


 


No voy a comentar mucho más sobre el tema, por que la verdad es que no tengo más que añadir. Ahora lo que me gustaría es conocer vuestra opinión sobre esto. Podeis expresarla en los cometarios de este artículo o en nuestro canal de [Telegram](https://t.me/jugandoenlinux).

