---
author: Pato
category: "Simulaci\xF3n"
date: 2018-01-25 18:18:31
excerpt: "<p>El desarrollo corre a cargo de&nbsp;Masterbrain Bytes y ya est\xE1 en\
  \ fase de acceso anticipado</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/9af1826b772c4d0c3b40cc25c52f7637.webp
joomla_id: 618
joomla_url: cattle-and-crops-es-un-nuevo-simulador-agricola-hardcore-y-tiene-version-gnu-linux
layout: post
tags:
- indie
- simulador
- kickstarter
- agricultura
title: "'Cattle and Crops' es un nuevo simulador agr\xEDcola \"hardcore\" y tiene\
  \ versi\xF3n GNU-Linux (ACTUALIZADO)"
---
El desarrollo corre a cargo de Masterbrain Bytes y ya está en fase de acceso anticipado

**ACTUALIZACIÓN 13-2-18:** Acabamos de recibir la [notificación de Steam](http://steamcommunity.com/games/704030/announcements/detail/1661135835619269326) de que el juego ya está disponible en esta tienda. También ha sido anunciado a través de Twitter como podeis ver aqui debajo:



> 
> Cattle and Crops v0.1.1.0 is now available on Steam!  
>  <https://t.co/sCA1VkgzyH> <https://t.co/jHmDoHKkUy> [pic.twitter.com/E89B85neH9](https://t.co/E89B85neH9)
> 
> 
> — Cattle And Crops (@CattleAndCrops) [13 de febrero de 2018](https://twitter.com/CattleAndCrops/status/963470006544629761?ref_src=twsrc%5Etfw)



 Podeis comprar el juego usando el siguiente widget:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/704030/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 




---


**NOTICIA ORIGINAL:** No hace mucho que publicamos un artículo sobre [otro juego](index.php/homepage/generos/simulacion/item/612-real-farm-el-simulador-de-granjas-llegara-a-linux-steamos-este-proximo-viernes) de "simulación agrícola" que tiene versión para Linux/SteamOS pero que está pasando con más pena que gloria. Sin embargo, gracias a [gamingonlinux.com](https://www.gamingonlinux.com/articles/cattle-and-crops-a-farming-simulation-and-management-game-that-has-linux-support.11079) hemos sabido que tenemos otro simulador de este tipo en camino, y con una pinta verdaderamente excelente. Hace solo unos meses no teníamos ningún juego de este tipo en nuestro sistema favorito y ahora tendremos dos, ¿no es para estar contentos?


Hablamos de 'Cattle and Crops' [[web oficial](https://www.cattleandcrops.com/#page-top)] un juego que según sus propios desarrolladores se define como "simulador detallado, tan cercano a la realidad como sea posible. *"Nuestro objetivo es tener un juego escalable, tanto para jugadores casuales como para entusiastas de las granjas."*


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/Bq9upQNVxn0" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


El juego corre a cargo de Masterbrain Bytes, una compañía alemana afincada en Bremen con el único objetivo de desarrollar este simulador y del que lanzaron una campañe en Kickstarter que recaudó casi 174.000€ a lo que hay que sumar las aportaciones desde Paypal y además la [campaña de financiación](https://www.cattleandcrops.com/es/kickstarter) que siguen manteniendo en su propia página web, en la que aún se puede aportar y conseguir acceso a su fase de acceso anticipado que ya está en marcha. Por otra parte, está programado su lanzamiento en Steam para próximas fechas en una especie de "segunda fase" del acceso anticipado donde esperan implementar un modo multijugador y además estará a la venta también directamente desde su página web donde están centrando todo el proceso de acceso anticipado y desarrollo ahora mismo.


 El juego cuenta con algunas de las marcas de maquinaria agrícola mas conocidas como CLAAS o Mercedes Benz, a las que irán sumando más conforme vayan avanzando en el desarrollo.


Algunas de las características que implementará el juego son:


* Tiempo dinámico
* Personalización de la simulación para jugadores de todo tipo
* Soporte para mods
* Multijugador
* Modo campaña y sandbox
* Sistema de tierra y vegetación dinámico
* Gestión de granja de animales


No negaremos que tiene una pinta excelente. ¿Te gustan los juegos de gestión y simulación agrícola? ¿Piensas probar el juego en acceso anticipado?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

