---
author: P_Vader
category: Aventuras
date: 2021-10-30 16:26:38
excerpt: "<p>Ambos t\xEDtulos de culto publicados en los 90 han sido lanzado bajo\
  \ la licencia GPL por <a href=\"https://twitter.com/2point21\" target=\"_blank\"\
  \ rel=\"noopener\">@2point21</a></p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/LittleBigAventure/little_big_adventure.webp
joomla_id: 1385
joomla_url: little-big-aventure-1-y-2-abre-su-codigo
layout: post
tags:
- aventura
- software-libre
- clasico
- '90'
title: "Little Big Adventure 1 y 2 abre su c\xF3digo"
---
Ambos títulos de culto publicados en los 90 han sido lanzado bajo la licencia GPL por [@2point21](https://twitter.com/2point21)


La casualidad nos trae de nuevo Little Big Adventure (LBA) por estos lares. Si hace muy poco nuestro compañero CansecoGPC nos presentaba la [actualización de ScummVM]({{ "/posts/scummvm-2-5-20-aniversario-desde-la-version-0-0-1" | absolute_url }}) con una captura de LBA mientras el mismo lo andaba jugandolo de nuevo, ahora nos enteramos que el nuevo estudio que se está encargando de relanzar el juego ha puesto ha disposición de la comunidad el código fuente del juego bajo la licencia GPL 2.


Personalmente este juego me trae gratos recuerdos, **es una aventura muy divertida llena de acción y puzles, que tenía una jugabilidad brillante y enganchaba mucho.** La combinación de escenarios dibujados con gráficos poligonales lo mezclaron de una manera extraordinario, dando un mejor resultado que muchos otros juegos de aquella época.


 


Según [anunciaba](https://blog.2point21.com/p/devlog-releasing-lba-open-source) el estudio **2point2:** 



> 
> **"queremos preservar esta obra maestra e innovadora de software y devolvérselo a la comunidad que mantuvo vivo el juego durante tantos años".**
> 
> 
> 


 


![little big adventure 2](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/LittleBigAventure/little_big_adventure_2.webp)


Puedes encontrar en **github el codigo la [primera](https://github.com/2point21/lba1-classic) y [segunda](https://github.com/2point21/lba2-classic) parte,** para gozo de sus fans sin duda.


Siempre es una satisfacción que estos clásicos se pongan a disposición de la comunidad y mas cuando se trata de títulos tan de culto como son LBA. Es una extraordinaria manera de preservar un legado digital que en algunos casos es difícil de mantener ya sea por la no disposición o por las trabas legales.


Hay que aclarar que **como en otras ocasiones, lo que se pone a disposición es el motor del juego en si, quedando excluido gráficos, audios, etc.** Como explican en su web, el juego lo puedes obtener en GOG ([1](https://www.gog.com/game/little_big_adventure) y [2](https://www.gog.com/game/little_big_adventure_2)) y Steam ([1](https://store.steampowered.com/app/397330/Little_Big_Adventure__Enhanced_Edition/) y [2](https://store.steampowered.com/app/398000/Little_Big_Adventure_2/)).


2point21 también anunciado que van a **[traer de vuelta LBA](https://blog.2point21.com/p/devlog-and-the-winner-is) en forma de nuevo juego haciendo uso del motor unreal**. Esperemos que el regreso de nuestros aventureros se mantenga a la altura de sus predecesores.


¿Jugaste alguna de las aventuras de su protagonista Twinsen? Cuéntanoslo en nuestros canales habituales de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).


 

