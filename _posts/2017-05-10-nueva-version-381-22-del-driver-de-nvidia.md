---
author: leillo1975
category: Hardware
date: 2017-05-10 07:24:40
excerpt: "<p>En el d\xEDa de ayer, Nvidia ha lanzado la nueva versi\xF3n de su ultimo\
  \ driver gr\xE1fico para GeForce, Quadro y Tesla. En esta ocasi\xF3n, esta versi\xF3\
  n (381.22)<strong> corrige multitud de errores</strong> y trae <strong>novedades\
  \ en el soporte de Vulkan</strong>, activando nuevas extensiones previamente vistas\
  \ en el hom\xF3nimo driver beta. Tambi\xE9n es importante resaltar que <strong>se\
  \ han desactivado por defecto las optimizaciones multihilo para OpenGL</strong>\
  \ debido a que estaban causando inestabilidad y errores.</p>\r\n<p>&nbsp;</p>\r\n\
  <p>Tambi\xE9n se ha <strong>desactivado el logo de Nvidia</strong> que pod\xEDa\
  \ verse en ocasiones al iniciarse el servidor gr\xE1fico, se ha dado <strong>soporte\
  \ para versiones m\xE1s recientes del Kernel</strong> de Linux (4.11); y se ha corregido\
  \ un error que dibujaba mal las ventanas con contenido OpenGL cuendo estas se redimensionaban.\
  \ En este \xFAltimo caso esto llegu\xE9 a notarlo con Virtualbox, cuando emulaba\
  \ alg\xFAn sistema operativo.</p>\r\n<p>&nbsp;</p>\r\n<p>Si quereis descargar esta\
  \ \xFAltima versi\xF3n, podeis pasaros por la <a href=\"http://www.nvidia.com/Download/driverResults.aspx/118524/en-us\"\
  \ target=\"_blank\" rel=\"noopener noreferrer\">p\xE1gina de Nvidia</a>, aunque\
  \ yo recomiendo esperar sobre un par de dias a que est\xE9 disponible en los respositorios\
  \ oficiales por la facilidad de la instalaci\xF3n. Si quereis hacer esto \xFAltimo\
  \ podeis pasaros por <a href=\"index.php/foro/drivers-graficas-nvidia/19-instalar-el-ultimo-driver-propietario-de-nvidia-en-ubuntu-y-derivadas\"\
  \ target=\"_blank\" rel=\"noopener noreferrer\">este tema del foro</a> donde explicamos\
  \ como instalar este repositorio en Ubuntu y derivadas.</p>\r\n<p>&nbsp;</p>\r\n\
  <p><strong>FUENTES:</strong> <a href=\"http://phoronix.com/scan.php?page=news_item&amp;px=NVIDIA-381.22-Linux-Driver\"\
  \ target=\"_blank\" rel=\"noopener noreferrer\">Phoronix</a>, <a href=\"http://www.nvidia.com/Download/driverResults.aspx/118524/en-us\"\
  \ target=\"_blank\" rel=\"noopener noreferrer\">NVIDIA</a></p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/66f461a82eb04828d9f9f9abb89cb004.webp
joomla_id: 312
joomla_url: nueva-version-381-22-del-driver-de-nvidia
layout: post
tags:
- nvidia
- privativo
- driver
- '38122'
title: "Nueva versi\xF3n 381.22 del driver de Nvidia"
---
En el día de ayer, Nvidia ha lanzado la nueva versión de su ultimo driver gráfico para GeForce, Quadro y Tesla. En esta ocasión, esta versión (381.22) **corrige multitud de errores** y trae **novedades en el soporte de Vulkan**, activando nuevas extensiones previamente vistas en el homónimo driver beta. También es importante resaltar que **se han desactivado por defecto las optimizaciones multihilo para OpenGL** debido a que estaban causando inestabilidad y errores.


 


También se ha **desactivado el logo de Nvidia** que podía verse en ocasiones al iniciarse el servidor gráfico, se ha dado **soporte para versiones más recientes del Kernel** de Linux (4.11); y se ha corregido un error que dibujaba mal las ventanas con contenido OpenGL cuendo estas se redimensionaban. En este último caso esto llegué a notarlo con Virtualbox, cuando emulaba algún sistema operativo.


 


Si quereis descargar esta última versión, podeis pasaros por la [página de Nvidia](http://www.nvidia.com/Download/driverResults.aspx/118524/en-us), aunque yo recomiendo esperar sobre un par de dias a que esté disponible en los respositorios oficiales por la facilidad de la instalación. Si quereis hacer esto último podeis pasaros por [este tema del foro](index.php/foro/drivers-graficas-nvidia/19-instalar-el-ultimo-driver-propietario-de-nvidia-en-ubuntu-y-derivadas) donde explicamos como instalar este repositorio en Ubuntu y derivadas.


 


**FUENTES:** [Phoronix](http://phoronix.com/scan.php?page=news_item&px=NVIDIA-381.22-Linux-Driver), [NVIDIA](http://www.nvidia.com/Download/driverResults.aspx/118524/en-us)

