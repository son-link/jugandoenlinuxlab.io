---
author: Serjor
category: Estrategia
date: 2018-03-21 21:45:04
excerpt: "<p>Valve ha hecho oficial la fecha. Saldr\xE1 el 28 de Noviembre</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/513d7a0ab11e38f7bd117d760146fed3.webp
joomla_id: 685
joomla_url: ya-hay-fecha-de-lanzamiento-para-artifact-el-juego-de-cartas-de-valve
layout: post
tags:
- steam
- valve
- cartas
- artifact
title: "Ya hay fecha de lanzamiento para Artifact, el juego de cartas de Valve - Actualizaci\xF3\
  n -"
---
Valve ha hecho oficial la fecha. Saldrá el 28 de Noviembre

**(Actualización 1-08-2018)**:


Gracias a [Phoronix](https://www.phoronix.com/scan.php?page=news_item&px=Artifact-28-November-Plans) nos llega la noticia de que Valve ha hecho oficial la fecha de lanzamiento de su nuevo juego 'Artifact'. Al parecer, el juego ha sufrido un pequeño retraso ya que de la fecha hecha pública por SteamDB no ha terminado cumpliéndose. Así pues, ahora ya de manera oficial sabemos que Artifact llegará a nuestro sistema favorito el próximo 28 de Noviembre a un precio estimado de 19,99 dólares (suponemos unos 19,99€ al cambio).


Si quieres saber más, puedes visitar su [página web oficial](http://playartifact.com/) o su página en la [tienda de Steam](https://store.steampowered.com/app/583950/Artifact/). Por otra parte, os hacemos llegar un vídeo de los compañeros de [Xataka](https://esports.xataka.com/otros-juegos/estas-ultimas-filtraciones-artifact-parte-comentarista-dota-2) donde se desvelan algunos detalles más:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/Ml8pmv8cxIE" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 




---


**(Artículo original):**


Ya os lo comentábamos en su [momento](index.php/homepage/generos/puzzles/item/549-artifact-un-nuevo-juego-de-cartas-de-la-mano-de-valve), Valve está desarrollando Artifact, un juego de cartas basado en el universo de Dota 2, el cuál será el [primero](index.php/homepage/generos/software/item/795-parece-que-valve-va-a-volver-de-nuevo-al-software) de una remesa de juegos nuevos por parte de la desarrolladora.


El caso es que a pesar del famoso "Valve Time", gracias a SteamDB nos hemos enterado de que la fecha oficial de salida será el 27 de septiembre:




> 
> .[@PlayArtifact](https://twitter.com/PlayArtifact?ref_src=twsrc%5Etfw) now has a [@steam_games](https://twitter.com/steam_games?ref_src=twsrc%5Etfw) page <https://t.co/zewLhxZTzy> [pic.twitter.com/HAVNREKsRZ](https://t.co/HAVNREKsRZ)
> 
> 
> — Steam Database (@SteamDB) [March 21, 2018](https://twitter.com/SteamDB/status/976531810904666114?ref_src=twsrc%5Etfw)




Os recordamos que este juego está siendo desarrollado con la colaboración de [Richard Garﬁeld](https://es.wikipedia.org/wiki/Richard_Garfield), el creador del famoso juego de cartas Magic: The Gathering, que será de pago, se basará en el motor Source 2, tendrá por supuesto versión para GNU/Linux y sus mecánicas serán, según dice la gente de Valve, novedosas para este tipo de juegos, ya que de alguna manera han querido llevar las mecánicas del famoso MOBA a un juego de mesa.


Será curioso ver como un juego de este tipo se hace un hueco en un género copado por juegos F2P como Hearthstone, que ya tiene una gran base de usuarios detrás.


Y hasta que llegue, os dejamos con un gameplay de sus mecánicas, cortesía de [Gameinformer](https://www.youtube.com/redirect?redir_token=wcp70JnFrHXp0S0nyKsxQTFYAAp8MTUzMzIzNTA4NkAxNTMzMTQ4Njg2&q=http%3A%2F%2Fwww.gameinformer.com%2Fgames%2Fartifact%2Fb%2Fpc%2Farchive%2F2018%2F03%2F09%2Fhow-do-you-play-it-and-how-will-you-pay-for-it.aspx&v=VajtUzxSVkk&event=video_description):


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/VajtUzxSVkk" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Y tú, ¿qué opinas de este Artifact? Cuéntanoslo en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD)

