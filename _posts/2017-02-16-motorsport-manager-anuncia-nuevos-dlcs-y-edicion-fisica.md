---
author: Pato
category: "Simulaci\xF3n"
date: 2017-02-16 18:11:10
excerpt: "<p>Playsport lanzar\xE1 la edici\xF3n f\xEDsica en diversos pa\xEDses incluido\
  \ Espa\xF1a. Si, \xA1Linux tambi\xE9n!</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/fe4bbe81600a40063594e597e00eb05b.webp
joomla_id: 222
joomla_url: motorsport-manager-anuncia-nuevos-dlcs-y-edicion-fisica
layout: post
tags:
- carreras
- steam
- simulador
- manager
title: "'Motorsport Manager' anuncia nuevos DLCs y edici\xF3n f\xEDsica"
---
Playsport lanzará la edición física en diversos países incluido España. Si, ¡Linux también!

Buenas noticias para los aficionados del excelente manager deportivo de escuderías. 'Motorsport Manager' [[web oficial](http://www.motorsportmanager.com/)] [[Steam](http://store.steampowered.com/app/415200)] lanzará nuevos DLCs próximamente para expandir el juego. En concreto lanzarán un DLC con la esperada serie GT donde podrás gestionar tu escudería en dos nuevas series, la GT Challenger Series y la prestigiosa International GT Championship. Ambas categorías tendrán nuevos equipos, corredores y escuderías.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/lHnYWoUKB5I" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Este DLC estará disponible el próximo día 22 de Febrero a un precio de 7,99€.


Por otra parte, también el día 22 lanzarán otro DLC gratuito para los poseedores del juego que permitirá crear tu propio equipo. Este DLC te permitirá personalizar los colores, el nombre y el uniforme de tu equipo. Esto permitirá personalizar el juego de modo que no creo que tarden en aparecer "mods" con los colores que todos esperamos.


Todo esto lo podrás descargar desde la tienda de Steam de Motorsport Manager, donde también se descargará el parche 1.3 del juego que traerá nuevo balanceo y parches de estabilidad al juego.


Por último, Playsport ha anunciado una edición física de Motorsport Manager que según dicen será lanzada en España el próximo día 22 de Marzo y que traerá consigo todos los contenidos y actualizaciónes lanzadas hasta esa fecha. Y si, ¡en la caja vendrá la edición para Linux!.


![MMBox](http://d1yt7fro2zr3ai.cloudfront.net/_live/imce/mm_pc_3dpack_web_uk_lo_res.jpg)


Tienes todos los detalles del anuncio [en este enlace](http://www.motorsportmanager.com/es/content/huge-new-dlc-packs-announced) (en inglés).


¿Qué te parecen los nuevos contenidos de Motorsport Manager? ¿piensas jugarlos? ¿Te comprarás la edición física?


Cuéntamelo en los comentarios o en los canales de Jugando en Linux de Telegram o Discord.

