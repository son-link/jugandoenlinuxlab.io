---
author: leillo1975
category: Estrategia
date: 2021-01-20 16:39:50
excerpt: "<p>El juego se acaba de actualizar recientemente.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/HardVacuun/HardVacuun.webp
joomla_id: 1270
joomla_url: descubre-openhv-un-rts-clasico-libre-y-gratuito
layout: post
tags:
- rts
- open-source
- mod
- openra
- estrategia-en-tiempo-real
- openhv
- hard-vacuun
- westwood-studios
title: "Descubre OpenHV, un RTS cl\xE1sico libre y gratuito."
---
El juego se acaba de actualizar recientemente.


 Es cierto que ultimamente la estrategia en tiempo real no es uno de mis géneros más jugados, pero hubo una época en que era el tipo de juego que más disfrutaba. Quizás parte de culpa la tienen los juegos que a mediados de los 90 hacían furor, con permiso de los shooters. Clásícos como Warcraft II, Starcraft, Age of Empires o todos los juegos de [Westwood Studios](https://es.wikipedia.org/wiki/Westwood_Studios) (Dune, Command & Conquer, Red Alert...) se vendían churros y se jugaban ávidamente en los PCs de la época. Hablando de Westwood Studios, el juego que hoy nos toca, tiene mucho que ver con él, pues usa como base [OpenRA]({{ "/posts/la-actualizacion-de-openra-nos-trae-la-campana-harkonnen-completa" | absolute_url }}), que como sabeis es un motor que recrea y moderniza los juegos de este estudio.


Hecha la introducción vamos ahora a hablar de **OpenHV**. Haciendo un poco de historia **el juego se basa en el nunca lanzado "[Hard Vacuun](https://lostgarden.home.blog/2005/03/27/game-post-mortem-hard-vacuum/)"**, un título que por desgracia se quedó en el tintero y que pretendía hacerse un hueco en el género. Para ello contaría con algunas características novedosas en la época, como podemos ver a continuación:


-**Lineas de Suministro**: los recursos no se recolectan por unidades, sinó que se contruye una torre extractora que usa drones automáticos para llevar los recursos a la base. El enemigo puede atacar estas lineas de sumnistro automáticas.


-**Altura del terreno variable**: esto permite que las unidades que están en posiciones más elevadas tengan ventaja táctica sobre las que están por debajo.


-**Terreno deformable con agua corriente y lava**: con la artillería puedes realizar enormes agujeros en el mapa para desviar agua o lava, así como lanzar bombas de agua o tierra para que se lleve por delante toda una tropa o crear una montaña.


-**Creación de muros defensivos**: Puede que ahora parezca una chorrada, pero en aquella época era novedoso.


-Con todo esto, **el jugador puede modificar el mapa en su beneficio** creando situaciones de combate únicas e interesantes.


 Tomando estas ideas y los multiples diseños y recursos que liberó uno de sus creadores, Daniel Cook, los creadores de OpenHV están desarrollando este **mod de [OpenRA](https://www.openra.net/)**, con unos resultados más que prometedores. El juego permite los modos escaramuza y multijugador (LAN e Internet), e incluye además un editor de mapas. Por la cadencia de actualizaciones, parece que se lo están tomando en serio y la [ultima "release"](https://github.com/OpenHV/OpenHV/releases/tag/20210109) de hace unos días incluye los siguientes cambios:


*-Se corrigió que el globo de exploración no se pudiera construir para todas las facciones.*  
*-Arreglados sprites de contorno dorados.*  
 *-Se redujo el daño del arma de plasma de Banshee.*  
 *-Se ha movido al constructor y al minero en la cola de construcción.*  
*-Arreglado el almacenamiento que es una opción de construcción cuando no hay torres de minería.*  
 *-Aumento del costo de los mineros nuevamente.*  
 *-Se redujo la velocidad de los mineros desplegables.*  
 *-Reducción de la producción de daño de la artillería dual.*  
 *-Aceleración de producción modificada: 4 pasos con una reducción del tiempo de construcción del 25%.*  
 *-Cambió la IA para que no dependa principalmente de aviones.*  
 *-Se agregó la capacidad de recoger cubos a los robots de IA. cubo de hielo*


 Si os interesa el juego podeis pasaros tanto por su [página del proyecto](https://github.com/OpenHV/OpenHV), como por su correspondiente sección en la web de [Itch.io](https://openhv.itch.io/openhv). En ellas podreis encontrar las descargas para los diferentes sistemas operativos, entre los que se encuentra, por supuesto, el ejecutable en formato AppImage para nuestro querido y amado GNU/Linux. **P.D.: Gracias @OdinTdh por el chivatazo.**


¿Que te parece este proyecto? ¿Te gustan los juegos de estratégia de los 90? Cuéntanoslo en los comentarios o en nuestros grupos de JugandoEnLinux en [Telegram](https://twitter.com/JugandoenLinux) o  [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

