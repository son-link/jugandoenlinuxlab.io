---
author: Serjor
category: Web
date: 2017-11-13 22:17:40
excerpt: "<p>En esta ocasi\xF3n la/s partida/s del viernes ser\xE1n del genial Borderlands\
  \ 2.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/5c856eb25b3e1fc73f244a7686864411.webp
joomla_id: 533
joomla_url: este-viernes-en-la-partida-de-la-comunidad-especial-juegos-cooperativos
layout: post
tags:
- comunidad
- cooperativo
- eventos
title: 'Este viernes en la partida de la comunidad: Especial juegos cooperativos (ACTUALIZADO
  2)'
---
En esta ocasión la/s partida/s del viernes serán del genial Borderlands 2.

**ACTUALIZACIÓN 18-11-17:** Finalmente esta noche hemos jugado a Borderlands 2 en modo cooperativo. Jaime, Leagnur, Francisco y leillo1975 han hecho unas pocas misiones en este trepidante juego acción con tintes de rol desarrollado por Gearbox y editado por 2K Games, que fué portado a nuestro sistema por los chicos de Aspyr. Esperemos que se repita, pues es un juego muy divertido.


Durante la partida hemos experimentado una caida del servicio de voz Discord, por lo que encontrareis un salto en la reproducción que fué cortado por razones obvias. Podeis ver el video aquí:


 <div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/ZDVjEUL0Ke4" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 




---


**ACTUALIZACIÓN 16-11-17:** En la encuesta que se realiza todas las semanas para decidir el título que jugaremos el viernes ha salido ganador **Borderlands 2**. Como muchos sabreis el juego está limitado a 4 jugadores simultaneos en modo cooperativo, por lo que si la afluencia es superior montaremos más de una partida. A partir de las 22:30 estad atentos a nuestro grupo de [Telegram](https://t.me/jugandoenlinux), y a nuestro canal de [Discord](https://discord.gg/ftcmBjD), donde podremos charlar durante la partida. Otra cosa, aunque no hayais votado, podeis participar si quereis, os esperamos con los puertos de red abiertos.


Si no podeis uniros a la/s partida/s siempre podreis seguirnos en directo a través de nuestro canal en "[Twitch](https://www.twitch.tv/jugandoenlinux). Nos vemos!

---

Algunos ya sabréis que todas las semanas [Odin](https://steamcommunity.com/id/odintdh) organiza una votación en la comunidad de telegram de jugandoenlinux.com para elegir qué juego se jugará ese viernes en las partidas de la comunidad. Esta semana, por ser la semana 46 del año, y para celebrar tan aclamado evento, en las partidas de la comunidad tenemos algo especial, y es que todos los juegos de la votación son cooperativos.


La lista de los candidatos es:


No more room in hell


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/224260/" style="border-style: none;" width="646"></iframe></div>


Payday 2


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/218620/" style="border-style: none;" width="646"></iframe></div>


Left 4 Dead 2


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/550/" style="border-style: none;" width="646"></iframe></div>


Serious Sam Fussion


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/41070/" style="border-style: none;" width="646"></iframe></div>


Sanctum 2


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/210770/27722/" style="border-style: none;" width="646"></iframe></div>


Borderlands 2


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/49520/16724/" style="border-style: none;" width="646"></iframe></div>


Y de manera extra-oficial, si hay problemas técnicos, Torchlight II, que también es cooperativo, es el juego que se usa de comodín.


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/200710/16831/" style="border-style: none;" width="646"></iframe></div>


Para votar podéis acceder al canal de [Telegram](https://telegram.me/jugandoenlinux) y el mensaje fijado es el de la votación de esta semana.


Además, si os animáis a participar os recordamos que para coordinarnos hacemos uso de los canales de voz que tenemos en [Discord](https://discord.gg/ftcmBjD), y os recomendamos encarecidamente que os apuntéis al [grupo de Steam](https://steamcommunity.com/groups/jugandoenlinux), ya que se crea un evento de Steam con la hora a la que comenzará la sesión, la cuál suele ser los viernes a las 22:30 hora peninsular, aunque puede variar.


Así que ya sabéis, apuntaros a jugar con nosotros, pero si por algún motivo no podéis participar, normalmente solemos retransmitir las partidas en nuestros canales de [Twitch](https://www.twitch.tv/jugandoenlinux) y [YouTube](https://www.youtube.com/channel/UC4FQomVeKlE-KEd3Wh2B3Xw) simultáneamente.

