---
author: Pato
category: Hardware
date: 2017-02-22 18:21:16
excerpt: "<p>Publican la primera versi\xF3n beta de SteamVR para Linux en Github</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/68497d6cb194485d2759fde9466457b7.webp
joomla_id: 229
joomla_url: valve-comienza-a-dar-soporte-a-la-realidad-virtual-en-linux
layout: post
tags:
- steamos
- realidad-virtual
- linux
title: Valve comienza a dar soporte a la Realidad Virtual en Linux
---
Publican la primera versión beta de SteamVR para Linux en Github

Todo comenzó con el tweet de Pierre Loup, uno de los mandamases del desarrollo de Steam para Linux:



> 
> Destinations and The Dota VR Hub are now available for Linux on Steam. A working Vulkan setup is required to run interactively in VR.
> 
> 
> — Pierre-Loup Griffais (@Plagman2) [February 22, 2017](https://twitter.com/Plagman2/status/834210182808735744)



 A partir de entonces todo ha ido en cascada. En seguida han comenzado a surgir las informaciones sobre lo que estaba pasando en diversas webs como [Phoronix](http://www.phoronix.com/scan.php?page=news_item&px=Valve-SteamVR-Linux-Beta) o [Gamingonlinux](https://www.gamingonlinux.com/articles/steamvr-for-linux-is-now-officially-in-beta.9156). Valve por fin ha publicado la primera beta para Linux de SteamVR, el entorno para Realidad Virtual de Steam y HTC Vive. De hecho, todo puede verse en la página de [Github de SteamVR](https://github.com/ValveSoftware/SteamVR-for-Linux/blob/master/README.md), donde además del anuncio también pueden verse las especificaciones.


Tal y como explican, se trata aún de un lanzamiento en fase beta destinado a que los desarrolladores puedan comenzar a programar aplicaciones para el entorno Linux. Tanto es así que para poder ejecutar SteamVR en Linux hay que optar al Cliente Beta de Steam para cumplir con las dependecias necesarias.


#### Requisitos necesarios:


SteamVR se asienta sobre la API Vulkan y requiere los últimos drivers Vulkan. Las gráficas Nvidia necesitan la versión de desarrollo del driver Beta 375.27.10 que puede descargarse [desde este enlace](https://developer.nvidia.com/vulkan-driver). Hay también un PPA para distribuciones Debian con estos drivers en: <https://launchpad.net/~mamarley/+archive/ubuntu/nvidia-dev/+packages>


![Vulkan](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/banners/Vulkan.webp)


Los drivers Nvidia solo soportan el "modo directo", lo que supone que la imagen del visor no aparecerá en tu escritorio.


Por su parte, las gráficas AMD necesitan una versión "pre-lanzamiento" del driver radv. En el mismo repositorio de SteamVR hay unos drivers compilados a tal efecto. Las gráficas de Intel no están soportadas en estos momentos.


Aparte de esto, SteamVR necesita acceder a los dispositivos USB del HTC Vive, por lo que tendrás que repasar las reglas udev de tu sistema para verificar que esto es así.


#### Desarrollo con Unity


Linux OpenVR requiere la versión 5.6 de Unity. Se puede descargar desde aquí:<https://unity3d.com/unity/beta>


Por supuesto solo el renderizado Vulkan está soportado en Linux.


#### Aplicaciones ya disponibles


Tal y como publicaba Pierre Loup, si cumples los requisitos ya puedes probar dos aplicaciones disponibles para SteamVR en Linux: 


[Destinations](http://store.steampowered.com/app/453170/): Se trata de una aplicación donde podrás explorar lugares reales o imaginarios con tus amigos.


[DOTA 2 VR Hub](http://store.steampowered.com/app/570/): Se trata de un modo visual del MOBA DOTA 2 donde podrás ver partidas online en una pantalla como si de un cine se tratara.


 


Por fin, parece que la espera va llegando a su fin. Valve sigue "presionando" el desarrollo para Linux y sigue apostando por nuestro sistema favorito. Esperemos que pronto tengamos novedades dentro del cliente de Steam y/o de su propio sistema SteamOS al respecto. Además esto supone otro buen empujón al desarrollo y expansión de Vulkan.


¿Que te parece la llegada de SteamVR a Linux? ¿Piensas utilizar las HTC Vive en un futuro?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

