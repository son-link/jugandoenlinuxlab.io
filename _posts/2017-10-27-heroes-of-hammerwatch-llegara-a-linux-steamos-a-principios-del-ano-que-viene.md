---
author: Pato
category: "Acci\xF3n"
date: 2017-10-27 14:40:01
excerpt: <p>Se trata de la secuela del notable Hammerwatch</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/6d00c12bde6a8ef539621cd606ac11ea.webp
joomla_id: 508
joomla_url: heroes-of-hammerwatch-llegara-a-linux-steamos-a-principios-del-ano-que-viene
layout: post
tags:
- accion
- indie
- proximamente
- rol
title: "'Heroes of Hammerwatch' llegar\xE1 a Linux/SteamOS a principios del a\xF1\
  o que viene"
---
Se trata de la secuela del notable Hammerwatch

Buenas noticias para los amantes de los juegos de rol y roguelike. La secuela del notable Hammerwatch también disponible en Linux llegará a nuestro sistema favorito a principios del próximo año.


Heroes of Hammerwatch es un juego de acción y aventuras de tipo rogue-lite ambientado en el mismo universo que Hammerwatch. Enfréntate a interminables hordas de enemigos, trampas, puzzles, secretos y mucha exploración a la vez que combates en niveles generados proceduralmente.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/_tv7OTxm4Wc" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Estaremos atentos a las novedades que nos lleguen acerca del juego.


 

