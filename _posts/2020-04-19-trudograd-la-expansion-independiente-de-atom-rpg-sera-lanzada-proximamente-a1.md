---
author: leillo1975
category: Rol
date: 2020-04-19 12:16:21
excerpt: "<p>Los desarrolladores de <span class=\"css-901oao css-16my406 r-1qd0xha\
  \ r-ad9z0x r-bcqeeo r-qvutc0\">@atomrpg</span> han publicado la versi\xF3n final\
  \ despu\xE9s del periodo de Acceso Anticipado.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AtomRpg/trudograd/Trudograd01_.webp
joomla_id: 1212
joomla_url: trudograd-la-expansion-independiente-de-atom-rpg-sera-lanzada-proximamente-a1
layout: post
tags:
- acceso-anticipado
- unity3d
- expansion
- atom-rpg
- atom-team
- trudograd
title: "Trudograd, la expansi\xF3n independiente de ATOM RPG, lanzada oficialmente"
---
Los desarrolladores de @atomrpg han publicado la versión final después del periodo de Acceso Anticipado.


****ACTUALIZADO 15-6-20:**** Llevábamos mucho tiempo sin hablar del proceso de creación de esta DLC independiente para ATOM RPG, algo que no quiere decir para nada que el proceso de desarrollo del juego haya estado parado, ni mucho menos. Hoy tras casi un año y medio después desde que os informamos del lanzamiento del juego en EA, celebramos el lanzamiento definitivo de la [versión 1.0](https://store.steampowered.com/news/app/1139940/view/2979683242494334431), o lo que es lo mismo, el final del periodo de Acceso Anticipado. En Trudograd encontraremos:  



*- Posibilidad de seguir jugando como tu personaje ATOM RPG (para esto, debes guardar un archivo después de vencer al último jefe de ATOM RPG y subirlo a Trudograd a través de un útil menú)*  
*- Un vasto mundo abierto, que contiene más de 45 ubicaciones pobladas, desde una megalópolis posapocalíptica nevada y sus afueras hasta búnkeres militares soviéticos secretos, un gran petrolero pirata en el mar helado y una isla misteriosa, entre muchos más;*  
*- Más de 30 ubicaciones solo de combate donde los jugadores podrán luchar contra decenas de variedades de enemigos, desde mercenarios hasta mutantes despiadados;*  
*- Más de 300 personajes, cada uno con un retrato único y un diálogo ramificado;*  
*- Más de 200 misiones, la mayoría con múltiples soluciones y resultados;*  
*- Misiones de texto visual totalmente expresadas con tramas ramificadas y obras de arte únicas hechas a mano;*  
*- Más de 100 modelos de armas distintas con más de 75 modificaciones de armas para una mayor personalización;*  
*- 3 trajes de armadura de exoesqueleto de estilo soviético con potencia única, con más de 20 formas de personalizarlos y modificarlos para cualquier estilo de juego;*  
*- Más de 45 horas de juego con mayor capacidad de reproducción en comparación con el primer juego de nuestro estudio;*  
*- Banda Sonora Original;*


Así mismo, esta expansión podrá ser adquirida de dos formas:


-**Por separado**, a un precio de 10.99€ ([Steam](https://store.steampowered.com/app/1139940/ATOM_RPG_Trudograd/) y [GOG.com](https://www.gog.com/game/atom_rpg_trudograd))


-**2-in-1 Edition Pack**, con ATOM RPG incluido y un 10% de descuento, a 23.38€ ([Steam](https://store.steampowered.com/bundle/22413/ATOM_RPG_2in1_Edition/))


También podrás adquirir:


-**War Supply Pack**, que permite más opciones de personalización del personaje, una nueva arma y más opciones en la creación de munición; por 3.99€ ([Steam](https://store.steampowered.com/app/1741760/ATOM_RPG_Trudograd__War_Supply_Pack/?curator_clanid=37498939) y [GOG.com](https://www.gog.com/game/atom_rpg_trudograd_war_supply_pack))


-**Supporter Pack**, para apoyar un poco más este pequeño estudio con items exclusivos y algunas nuevas caracteríticas, por 4.99€ ([Steam](https://store.steampowered.com/app/1713870/ATOM_RPG_Trudograd__Supporter_Pack/) y [GOG.com](https://www.gog.com/game/atom_rpg_trudograd_supporter_pack))


 


Pues ya sabeis, si os mola el Rol de toda la vida, con rollito postapocaléptico, este es un título que no puede faltar en vuestra biblioteca, por lo que desde JugandoEnLinux.com os lo recomendamos sin ningún tipo de duda. Os dejamos con este recien estrenado trailer, descriptivo de todo cuanto encontrareis en esta expansión independiente:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/QX-LvgaOXbs" title="YouTube video player" width="780"></iframe></div>


 




---


 **ACTUALIZADO 15-6-20:** Tal y como os habíamos comentado la última vez, trás poco más de un mes, ATOM Team ha lanzado una versión para nuestro sistema, conjuntamente con la versión de MacOS. Además de esto, esta actualización añade los siguientes [cambios](https://store.steampowered.com/newshub/app/1139940/view/2408810742522294514):  
  
*-Se agregó soporte para Mac y Linux;  
-La cámara ya no entra en las texturas del techo;  
-Importación y creación de interfaz de usuario mejorada;  
-Comportamiento de IA mejorado;  
-Crasheo y congelación en la carga del juego corregido;  
-Modificaciones de armas arregladas;  
-Nuevo encuentro aleatorio de comerciantes;  
-La misión de Samuil Brothers ahora puede completarse hasta el final;  
-Nuevos íconos de ítems;  
-Problemas de ruta solucionados;  
-Comportamiento durante el pánico arreglado;  
-Se agregaron nuevas animaciones;  
-Los movimientos mano a mano ya no aparecen como artículos de inventario;  
-Se corrigió alguna lógica de diálogo;  
-Se corrigieron errores relacionados con misiones;  
-Los personajes ahora reaccionan al estado de alucinación.*


Recordad que a pesar de poder comprar y jugar el juego nativamente en GNU/Linux, Trudograd es un juego en Acceso Anticipado, con lo que eso implica. Podeis comprarlo por tan solo 10.99€ en [Steam](https://store.steampowered.com/app/1139940/ATOM_RPG_Trudograd/) y [GOG.com](https://www.gog.com/game/atom_rpg_trudograd). Os dejamos con la intrigante cinemática del inicio del juego:


  
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/hIaIBOyC7T8" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>




---


**ACTUALIZADO 11-5-20:** Acaba de llegarnos la notificación a través de Steam de que el juego ya [ha sido lanzado finalmente](https://store.steampowered.com/newshub/app/552620/view/2202767889665886025) hace escasos minutos en Early Access, aunque por el momento aun no está disponible para GNU/Linux. tal y como os comentábamos hace unas semanas, está llegará entre 3 y 6 semanas a partir de hoy. Esperemos que sea más pronto que tarde. Mientras podeis entreteneros jugando la [primera parte del juego en español]({{ "/posts/ya-se-puede-empezar-a-jugar-a-atom-rpg-en-espanol" | absolute_url }}). Permaneceremos atentos a cualquier noticia sobre este [Trudograd](https://store.steampowered.com/app/1139940/ATOM_RPG_Trudograd/?snr=1_2108_9__1601).




---


**NOTICIA ORIGINAL**:


Desde su [lanzamiento en Acceso Anticipado](index.php/component/k2/7-rol/677-atom-rpg-post-apocalyptic-indie-game-llega-en-early-access) hace casi ya 2 años y medio, ATOM RPG ha consechado un enorme éxito entre los amantes del género de Rol. El juego , que **no oculta sus influencias por clásicos como Wasteland o Fallout**, tiene personalidad propia, presentando un yermo que en esta ocasión no está localizado en los EEUU , sinó en la antigua Union Soviética, con lo que ello supone en cuanto a ambientación e ideosincrásia.


Ayer mismo asisitíamos al [anuncio en Steam](https://store.steampowered.com/newshub/app/1139940/view/3503180470698761105) y redes sociales de la llegada de **su primera expansión independiente, Trudograd, en Acceso Anticipado** también, tal y como sucedió en su precuela, por lo que todos aquellos que quieran disfrutar de ella podrán empezar a hacerlo **el proximo día 11 de Mayo**, como podemos ver en el siguiente tweet:



> 
> Atom RPG: Trudograd is coming out into Early Access on May 11th 2020![#indiedev](https://twitter.com/hashtag/indiedev?src=hash&ref_src=twsrc%5Etfw) [#gamedev](https://twitter.com/hashtag/gamedev?src=hash&ref_src=twsrc%5Etfw) [#Trudograd](https://twitter.com/hashtag/Trudograd?src=hash&ref_src=twsrc%5Etfw) [#atomrpg](https://twitter.com/hashtag/atomrpg?src=hash&ref_src=twsrc%5Etfw) [#crpg](https://twitter.com/hashtag/crpg?src=hash&ref_src=twsrc%5Etfw) [#May](https://twitter.com/hashtag/May?src=hash&ref_src=twsrc%5Etfw) [#earlyaccess](https://twitter.com/hashtag/earlyaccess?src=hash&ref_src=twsrc%5Etfw) <https://t.co/wccGVqGRTP>
> 
> 
> — ATOM RPG - DEV BLOG (@atomrpg) [April 18, 2020](https://twitter.com/atomrpg/status/1251554976607932416?ref_src=twsrc%5Etfw)


 



 


 En esta ocasión, el Atom Team, nos indica que dicha expansión se lanzará con un nivel de desarrollo más amplio que en la anterior ocasión, por lo que se podrá disfrutar mejor que  con el juego original en su salida en acceso anticipado. Encontraremos localizaciones más grandes que en su predecesor, con más de 100 personajes, una cantidad enorme de misiones, nuevas armas, items y habilidades, así como un nuevo tipo visual de misiones. En la descripción de Steam podemos encontrar la siguiente información:


*En Atom RPG: Trudograd tu objetivo es viajar a una gigantesca metrópoli post-apocalíptica que resistió las pruebas de la destrucción nuclear y el colapso social. Allí debes encontrar lo que se cree que es la última esperanza de la humanidad para defenderse de la amenaza del espacio exterior.*


*Características de Trudograd:*


*Una gran ciudad con su propia tradición y docenas de lugares para luchar, comerciar, hablar y explorar;*  
 *Una experiencia dura y por turnos con grandes cantidades de caminos de desarrollo de personajes, estilos de combate, habilidades, ventajas y distinciones;*  
 *Personajes únicos, cada uno con su propia personalidad y un robusto diálogo ramificado;*  
 *Búsquedas de soluciones múltiples, que cambian activamente la ciudad que te rodea;*  
 *Capacidad de transferir a tu héroe de nuestro juego anterior (Atom RPG);*  
 *Un juego fácil de aprender. Aunque Trudograd es una continuación de la trama del primer juego, también es amigable con los nuevos jugadores, y rápidamente te pondrá al corriente de la tradición y los principales desarrollos que te hayas podido perder;*  
 *El resultado de un esfuerzo comunitario de dos años, hecho por jugadores para jugadores. Este juego abarca muchos cambios con respecto a nuestro título anterior, que se inspiraron en los comentarios de los usuarios.*


**El juego no estará disponible desde el principio del Acceso Anticipado para nuestro sistema operativo**, pero [según han comentado sus desarrolladores](https://translate.google.ru/translate?hl=ru&sl=ru&tl=en&u=https%3A%2F%2Fsteamcommunity.com%2Fapp%2F1139940%2Fdiscussions%2F0%2F3971555000776782901%2F), **entre 3 y 6 semanas despues del 11 de Mayo**, tendrá versión nativa al igual que su predecesor. Si quereis añadir esta expansión a vuestra lista de deseados o seguir su desarrollo, podeis hacerlo en su página de Steam. Nosotros por nuestra parte seguiremos informándoos de cualquier información importante con respecto a este juego. Os dejamos mientras con un video introductorio a lo que nos vamos a encontrar en Trudograd:


  
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/Da3TgvyNzTo" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


¿Habeis jugado a ATOM RPG? ¿Os gustan los juegos de rol postapocalípticos? Podeis darnos vuestra opinión sobre este juego en los comentarios de este artículo, en nuestra comunidad de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

