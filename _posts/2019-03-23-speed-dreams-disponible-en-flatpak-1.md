---
author: leillo1975
category: "Simulaci\xF3n"
date: 2019-03-23 12:54:23
excerpt: "<p>El paquete ha sido actualizado a la \xFAltima versi\xF3n oficial, la\
  \ 2.2.2</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/b2817edf79750dc373157d782cce30ec.webp
joomla_id: 872
joomla_url: speed-dreams-disponible-en-flatpak-1
layout: post
tags:
- open-source
- flatpak
- speed-dreams
- codigo-abierto
title: Speed Dreams disponible en Flatpak (ACTUALIZADO 2)
---
El paquete ha sido actualizado a la última versión oficial, la 2.2.2


**ACTUALIZADO 23-3-19:** Gracias a la ayuda de un miembro del equipo de desarrollo de Speed Dreams, **BeagleJoe**, y por supuesto del trabajo previo de **SonLink**, tras algunos problemas iniciales, finalmente está disponible para todos vosotros la **version final 2.2.2** con el soporte para el nuevo motor **OpenSceneGraph** incluido, por lo que de esta forma **el paquete flatpak se pone a la par con la release oficial**. El método de instalación es el mismo que el descrito en la noticia original (abajo). En Flathub.org podeis encontrarlo en este enlace:


<https://flathub.org/apps/details/org.speed_dreams.SpeedDreams>


Desde JugandoEnLinux.com os recomendamos encarecidamente este juego, ya que a parte de ser un **juego libre**, tiene una gran cantidad de trabajo detrás y una buena dosis de diversión si os gustan los simuladores de conducción. También nos gustaría **agradecer una vez más a @SonLink y @BeagleJoe su trabajo y paciencia** para que este gran juego esté disponible para todos los usuarios de Linux con una forma fácil y sencilla de instalar.




---


**ACTUALIZADO 29-10-18:** Si bien el juego está disponible hace días en el repositorio de Flathub, este aun no había sido incluido en su página. Desde hoy podemos encontrarlo ya con su sección correspondiente e información de como instalarlo y ejecutarlo. Felicidades @SonLink por tu trabajo!!! **[Podeis acceder a la página del Flatpak de Speed Dreams en Flathub](https://flathub.org/apps/details/org.speed_dreams.SpeedDreams)**.


![SpeedDreamsFlatpak](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SpeedDreams/SpeedDreamsFlatpak.webp)




---


**NOTICIA ORIGINAL:** Poco a poco vamos viendo algunos proyectos interesantes que crecen a tenor de nuestra comunidad. Si recapitulamos un poco, hace no mucho tiempo, **@OdinTdh**, nos regalaba [PyLinuxWheel](index.php/homepage/generos/software/item/971-pylinuxwheel-una-pequena-gran-utilidad-para-configurar-los-grados-de-nuestros-volantes-logitech), una apliación de configuración de volantes Logitech que permitía cambiar los grados facilmente. Ahora podemos usar esta aplicación con otro proyecto que es el que nos ocupa, un **FlatPak de Speed Dreams**, el conocido simulador de coches libre del que realizamos [una jugosa entrevista](index.php/homepage/entrevistas/item/784-entrevista-speed-dreams-un-simulador-de-coches-libre) a dos de sus integrantes hace unos meses.


[Speed Dreams](http://www.speed-dreams.net) presentaba un problema ultimamente, y es que los paquetes precompilados que podíamos encontrar tanto en la desaparecida Desura, en Playdeb o el [PPA para Ubuntu](https://launchpad.net/~speed-dreams/+archive/ubuntu/ppa), estaban completamente desfasados (en el mejor de los casos). Si queríamos hacernos con la última versión publicada oficialmente ([2.2.2 RC2](https://sourceforge.net/projects/speed-dreams/files/2.2.2/2.2.2-rc2/)), teníamos que [compilar manualmente](index.php/foro/tutoriales/108-como-compilar-la-ultima-version-de-speed-dreams-gracias-gnuxero26) (al contrario que en Windows y Mac) los diferentes paquetes para poder disfrutar de él... Pero ahora todo esto se acabó gracias al trabajo de **@SonLink**, otro miembro más de nuestra comunidad que en su día también se encargó de hacer lo propio con otro gran juego libre, [Warzone 2100](https://flathub.org/apps/details/net.wz2100.wz2100), del que también teneis un cómodo Flatpak para instalar este gran título de estratégia.


Las **ventajas de poder disfrutar de esta última versión de Speed Dreams son múltiples**, ya que las mejoras realizadas en estos últimos tiempos permiten por ejemplo disfrutar de **Force Feedback** en volantes Logitech, usar el **nuevo motor gráfico OSG** que permite un mejor rendimiento y un nuevo hud, un **webserver** que registra nuestros tiempos, y por supuesto múltiples librerias actualizadas y bugs corregidos. Como podeis ver, compensa bastante...


Como muchos de vosotros sabreis, Flatpak es un sistema de paquetería que al igual que Snap,  pretende unificar y facilitar la distribución de paquetes en las diferentes distribuciones de Linux, "independizando" las temidas dependencias en contenedores aislados que permiten su ejecución sin problemas. Si quereis instalarlo simplemente [seguid este enlace](https://flatpak.org/setup/) para poder usarlo en vuestra distribución. Una vez realizados estos cambios podreis instalar sin complicaciones el paquete de [Speed Dreams](https://flathub.org/apps/details/org.speed_dreams.SpeedDreams) y echar unas fantásticas carreras con sus múltiples pistas y coches. Para instalar el flatpack de Speed Dreams abrid una terminal y escribid:



> 
> `flatpak install flathub org.speed_dreams.SpeedDreams`
> 
> 
> 


Si quereis que el paquete quede instalado en nuestro espacio de usuario (/home), añadid primero el repositorio en el espacio de usuario:



> 
> `flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo --user`
> 
> 
> 


Y después:



> 
> `flatpak install --user flathub org.speed_dreams.SpeedDreams`
> 
> 
> 


Si por un casual no os arranca y os da un error parecido a este:  
  
`*libGL error: No matching fbConfigs or visuals found*`  
`*libGL error: failed to load driver: swrast*`  
  
Teneis que instalar esta librería gráfica desde flathub:   
  
`flatpak install flathub org.freedesktop.Platform.GL.nvidia-410-66`


Donde pone 410-66 es por que tengo el driver 410.66 de nvidia, si tuviese otra versión tendríamos que ponerla de la misma forma. Información sacada de [LinuxUprising.com](https://www.linuxuprising.com/2018/06/how-to-get-flatpak-apps-and-games-built.html?m=1)


Si no conoceis este fantástico simulador libre podeis ver el último video que grabamos para nuestro canal de Youtube donde podeis ver algunas de estas nuevas características incluidas en esta versión:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/UbM_wo1SD6o" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


¿Qué os parecen este tipo de iniciativas de nuestra comunidad? ¿Sois partidarios de este tipo de distribución de paquetes? Déjanos tu opinión en los comentarios o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

