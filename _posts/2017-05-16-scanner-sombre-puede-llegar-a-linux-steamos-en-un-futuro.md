---
author: Pato
category: "Exploraci\xF3n"
date: 2017-05-16 16:35:59
excerpt: <p>El juego de Introversion Software se basa en juegos como "Dear Esther"
  o "Gone Home"</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/4246b121d2dc949b8f082c5f57840a3b.webp
joomla_id: 322
joomla_url: scanner-sombre-puede-llegar-a-linux-steamos-en-un-futuro
layout: post
tags:
- indie
- aventura
- terror
- exploracion
title: '''Scanner Sombre'' puede llegar a Linux/SteamOS en un futuro'
---
El juego de Introversion Software se basa en juegos como "Dear Esther" o "Gone Home"

'Scanner Sombre' es lo que se suele decir un "walking simulator". Si has jugado a 'Dear Esther' sabes lo que es. Básicamente se trata de ir "caminando" por el juego e ir descubriendo la historia y sus secretos.


En este caso, 'Scanner Sombre' [[web oficial](http://www.introversion.co.uk/scannersombre/)] está reconocidamente inspirado en Dear Esther o Gone Home, y es posible que acabe llegando a Linux. Gracias a SteamLinuxDatabase nos enteramos de que han abierto una rama en su báse de datos de Steam para Linux:



> 
> New Game:  
> Scanner Sombre<https://t.co/K5bObHPdWl>
> 
> 
> — SteamDB Linux Update (@SteamDB_Linux) [May 16, 2017](https://twitter.com/SteamDB_Linux/status/864424457699561472)



 Realmente hay que tomarlo con cautela, puesto que han habido casos en los que juegos que han aparecido en la base de datos de Steam para Linux no han terminado apareciendo para nuestro sistema, pero si es señal de que al menos algo se está moviendo, y en lo que respecta a este juego, su aparición en Linux sería muy buena señal.


#### **Acerca de Scanner Sombre:**



> 
> Recuperas la conciencia, hueles la humedad. Al abrir los ojos se ven las paredes de piedra de la cámara parpadeando a la luz del fuego; Tropiezas, y golpeas con el pie un casco que restalla por el suelo. Poco a poco el dolor en la cabeza comienza a disminuir y ves el comienzo de una cueva. Después de unos pasos la oscuridad te consume. Volviendo a la seguridad del fuego se ve un escáner LIDAR en el suelo - al presionar el gatillo aparece en un tenue resplandor que viene de dentro del casco. Te pones el casco, ajustas el ancho del haz y entras en el abismo ....
> 
> 
> 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/8NFlo9FEfaY" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


 ¡No me dirás que no es interesante la propuesta de este 'Escanner Sombre'!


Cuéntame que te parece en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 

