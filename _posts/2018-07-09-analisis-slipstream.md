---
author: leillo1975
category: "An\xE1lisis"
date: 2018-07-09 14:30:15
excerpt: <p>Profundizamos en este juego de @ansdor</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/4146e53ad3135f7b51a909904808e7b3.webp
joomla_id: 793
joomla_url: analisis-slipstream
layout: post
tags:
- analisis
- retro
- ansdor
- herramientas-libres
title: "An\xE1lisis: Slipstream"
---
Profundizamos en este juego de @ansdor

Si recordais, hace poco menos de un mes análizabamos para todos vosotros "[Horizon Chase Turbo](index.php/homepage/analisis/item/888-analisis-horizon-chase-turbo)", un juego desarrollado por la compañía brasileña Aquiris y claramente basado en clásicos como Out Run. Con las mismas premisas encontramos esta pequeña-gran joyita hecha videojuego desarrollada por el también brasileño [Ansdor](http://www.ansdor.com/), aunque con un tratamiento rotundamente diferente. Si en el primero nos encontrábamos ante una actualización completa del género de los arcades de conducción de los 80-90, en este caso la idea es la contraria, ofreciéndonos un juego que pasaría perfectamente por uno de aquella época.... y es ahí donde realmente ha dado en el clavo.


Permitidme que me ponga pesado con el tema de la nostalgia y lo [retro](index.php/homepage/analisis/itemlist/tag/Retro). Sabeis que ultimamente, no se si por falta de ideas nuevas, o por aprovechar el tirón, estamos observando un resurgimiento de juegos al estilo de décadas pasadas. No voy a juzgar si esto es bueno o malo, por que además siendo subjetivo a mi personalmente son videojuegos que disfruto bastante. Obviamente los estudios saben que muchos de los actuales jugadores llevan siendolo ya muchos años y son adultos que normalmente tienen más poder adquisitivo que un adolescente o usuarios jóvenes. Usar estos reclamos es atacar directamente la "patata" y transportarnos a nuestra niñez o juventud. Además está el tema de la moda... no hay más que salir a la calle para ver por donde van los tiros en cuanto a la tendencias se refiere.


![Escenarios](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SleepStream/Escenarios.webp)


*El juego luce un gran colorido y un rendimiento más que aceptable*


Puestos ya en "situación y antecedentes" vamos a empezar a hablaros en primer lugar de su desarrollador. [@ansdor](https://twitter.com/ansdor?lang=es) (Sandro Luiz de Paula) como os dije hace un momento es un joven desarrollador de Belo Horizonte (Brasil) que ya ha trabajado con anterioridad en otros dos juegos, [Ultra Type-R](https://ansdor.itch.io/ultratyper) y [Meow Sushi Night](https://ansdor.itch.io/meow), ambos con la misma estética retro-pixel que le caracteriza. Tras conseguir financiación para su proyecto en [Kickstarter](https://www.kickstarter.com/projects/noctet/slipstream?lang=es) y un proceso de desarrollo no exento de problemas el juego finalmente ha conseguido ver la luz hace un par de meses.


Antes de comenzar con el análisis propiamente dicho del juego convendría aclarar que **el juego ha sido casi completamente desarrollado usando software libre**, empezando por los sistemas en que fué desarrollado (Ubuntu y Arch) y continuando con herramientas como [libGDX](https://libgdx.badlogicgames.com/), Krita, Gimp, Blender o Intellij IDEA CE, algo que creo que debemos tener muy en cuenta, especialmente nosotros, usuarios de software libre. El "casi completamente" de antes viene dado por que la [banda sonora](https://effoharkay.bandcamp.com/album/slipstream-original-soundtrack) (y efectos sonoros) del juego, han creados por norteamericano **Stefan Moser** usando un sintetizador [Yamaha DX-21](https://en.wikipedia.org/wiki/Yamaha_DX21) y otras herramientas.


Bueno... dejémonos de prefacios y hablemos de lo que más importa. Si por algo destaca Slipstream es por su **endiablada  y rotunda jugabilidad** ...No chicos, no nos vamos a devanar los sesos con los controles ni las mecánicas. Para jugar a Slipstream solo vamos a necesitar girar a izquierda y derecha, acelerar y frenar, aunque es cierto que con una pequeña particularidad, pues **tendremos que derrapar muy a menudo para poder superar las curvas más cerradas**, que son mayoría en el juego. Para ello simplemente daremos un toquecito al freno un y volveremos a acelerar rapidamente mientras giramos el volante. El dominio de la técnica de derrapaje es básica para poder jugar y al principio puede costar un poco hacerse con ella, pero luego es automática. Me resulta curioso que a la vez que jugaba a Slipstream estaba jugando a Horizon Chase Turbo y luego trataba de emular el derrape en este último. Otra de las caracteristicas de la jugabilidad es la cualidad que le da nombre al juego, es decir, **el rebufo (en inglés, slipstream), que nos será de gran ayuda para aumentar nuestra velocidad y adelantar coches más rapidamente**. Para ello bastará con que nos coloquemos detrás de otro coche y esperemos a que se rellene una barra que pone "slipstream" para que experimentar un subidón de velocidad.


![Derrape](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SleepStream/Derrape.webp)


*Tendremos que derrapar muy a menudo para superar las numerosas curvas del juego*


Siguiendo con los paralelismos con el juego de Aquiris, algo que usaré a menudo durante este análisis, Slipstream ha acertado, desde mi punto de vista donde "Horizon Chase Turbo" (**HCT a partir de ahora**) dejó un pequeño hueco. Si habeis jugado a Out Run sabreis que comenzábamos la partida en la playa y al terminar la fase teníamos que escoger camino en una bifurcación para llegar a otro escenario. En HCT no teníamos esta posibilidad, algo que eché muy en falta, pues se trataba de circuitos que no estaban interconectados. Por lo contrario, **Slipstream tiene esta posibilidad en el "Arcade Mode" pudiendo jugar del tirón hasta llegar a meta**. Para ello debemos llegar antes de que se nos termine el tiempo al inicio del siguiente escenario. En cada uno de ellos **nos enfrentaremos a un rival que competirá con nosotros para ver quien llega antes al final de la fase**. Estos personajes están diseñados siguiendo los clichés del "ochenteo" y nos recordarán a personajes de aquella época. Nos mandarán "picadas" durante la carrera en forma de textos cada vez que los adelantemos o nos adelanten ellos a nosotros. Los escenarios, además, se pueden correr tanto en un sentido como en el contrario.


Slipstream cuenta, además del modo arcade, con el **modo de carrera rápida** y "Grand Prix". En el primero tendremos la posibilidad de escoger el escenario que nos apetezca entre los 20 disponibles que conforman el juego, además del número de oponentes, vueltas y posición de inicio. Hayaremos la victoria solo si conseguimos superar a todos nuestros competidores antes de que se cumplan todas las vueltas que establezcamos. Este modo está muy bien si queremos practicar una fase que se nos resista o nos guste en particular.


El modo "**Grand Prix**" es un auténtico acierto, pues en él podremos disputar hasta 3 copas diferentes, encadenando cada una de ellas varios escenarios en modo circuito, tal y como lo vimos en la carrera rápida; pero con la **particularidad de poder mejorar nuestro coche con el dinero de los premios** que vamos ganando. Los coches en esta ocasión parten de una posición inicial con las mismas características todos, y solo se diferencian esteticamente. Es, por decirlo de alguna manera, una especie de "modo Carrera" donde podemos personalizar nuestro vehículo a nuestro gusto, en cuanto a **velocidad**, **aceleración** y **manejo**. En todos los modos de juego, incluido el arcade, podremos escoger entre 3 dificultades diferentes (fácil, medio y difícil).


![Carrera](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SleepStream/Carrera.webp)


*En los modos Carrera Rápida y Grand Prix tendremos que dar varias vueltas al mismo escenario y adelantar a nuestros adversarios*


En todos los modos podremos escoger automóvil, y excepto en el modo Grand Prix, que como os dije parten todos de las mismas caracteríticas, cada uno tiene sus particularidades, siendo algunos equilibrados, y otros más rápidos o manejables; habiendo un total de 5 para escoger. Todos ellos tienen **diseños que recuerdan a coches típicos de los años 80** y se les puede cambiar la apariencia (usando arriba y abajo) entre una gama de tres colores cada uno.


Como comentaba antes, Slipstream cuenta con 20 escenarios diferentes, que son de lo más variado, encontrandonos lugares conocidos como el Valle de los Reyes en Egipto, Monument Valley en EEUU, Akagiyama en Japón o el Partenón en Grecia. Hay que destacar también la inclusión de Villa Rica, en Brasil donde el apartado artistico se nota ligeramente diferente al resto de localizaciones , ya que se trata de una localidad turístiico-historica del estado de Minas Gerais al que pertenece el desarrollador. Después también podemos encontrar lugares más "estandar" como playas, paisajes helados o diferentes ciudades.


Entrando en cuestiones técnicas, y empezando por el **apartado gráfico**, el juego es puramente retro. No se trata de una adaptación del género con gráficos actualizados como HCT, sinó todo lo contrario, presentando al jugador un aspecto igual al que se encontraría hace 30 años. El juego está hecho enteramente a golpe de pixel, usando un modo de **pseudo3D** como los juegos en los que se basa y aprovechando las características de un motor gráfico creado por el autor para este juego. A diferencia de los juegos de antaño **el título aprovecha las capacidades de los ordenadores actuales para ofrecer unas tasas de 60 fps**, por lo las visuales se muestran suaves, mostrando incluso filtro de texturas. Es de remarcar también los difentes modos gráficos a los que podemos acceder pudiendo **modificar la pantalla para simular los viejos televisores de tubo metiendo bordes redondeados y "scanlines"**, si lo que queremos es tener la sensación de estar jugando a una recreativa de las de toda la vida. En cuanto a los menús tienen una apariencia claramente influenciada por los juegos de la mascota de SEGA, Sonic: The Hegdehog, algo que también vemos en los nombres de los escenarios. Conviene  decir que las caracteristicas del juego permiten que este se pueda ejecutar con garantías en cualquier ordenador que le pongamos delante actualmente sin mucho problema, pues **es un juego que consume muy pocos recursos**.


![Monument](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SleepStream/Monument.webp)


*A lo largo del juego recorreremos escenarios de lo más variado, incluyendo lugares icónicos reales*


Como comentamos anteriormente el **apartado sonoro** es obra de Stefan Moser, destacando en gran medida la banda sonora por su calidad y "originalidad". A golpe de sintetizador contiene una **buena cantidad de temas musicales completamente ochenteros** que recuerdan constantemente esa época. Para mi este es uno de los aspectos más destacables del juego. Es curioso también que podremos cambiar de tema facilmente durante las carreras pulsando "ESC" o el botón "Start", y moviendonos por un menú donde veremos una serie de cintas de cassette con los nombres, y con un efecto que emula a una cinta VHS cuando está pausada, lo cual ha quedado bastante original. Si hablamos de los efectos de sonido tales como acelacion, frenazos, derrapes, golpes, etc... son más que correctos y encajan a la perfección en el juego.


 En el momento de realizar este análisis **el juego no presenta ningún modo multijugador**, lo cual en los tiempos que corren se echa bastante en falta, y además estamos seguros que sería muy divertido. Aunque conviene decir que Ansdor no lo ha descartado en un futuro, al menos en modo local a pantalla partida, además ya ha dado muestras en recientes [mensajes de twitter](https://twitter.com/ansdor/status/1011062887153045504) que está trabajando en ello. Ojalá en un futuro pudiese añadir un modo Online para poder medirnos con otros jugadores a través de internet. Los controles son bastante precisos y responden bien. Nosotros hemos podido jugarlo sin ningún problema con teclado, mando de XBOX360 y Steam Controller.


Como conclusión hay que decir claramente y con rotundidad que Slipstream es una obra maestra de juego retro, con un acabado impoluto y una jugabilidad que engancha al momento. Un juego perfecto para disfrutarlo sin más pretensiones que te hará gozar como lo hacías hace años en los salones de arcade, pero esta vez desde el sillón o sofá de tu casa. En JugandoEnLinux no somos de poner notas a los videojuegos en los análisis, pero esta vez nos atrevemos a decir que **Ansdor se merece un sobresaliente por su trabajo**, pues el juego no esconde la cantidad de tiempo y esfuerzo que ha debido de perder para sacar un juego tan pulido y redondo como Slipstream. Además está el tema del precio, pues se trata de un juego que vale poco más de 8€, por lo que no hay excusas para no comprarlo si disfrutas con este tipo de títulos.


Puedes comprar Slipstream en [Itch.io](https://ansdor.itch.io/slipstream) y [Steam.](https://store.steampowered.com/app/732810/Slipstream/) Os dejamos con el video que grabamos hace algún tiempo estrenándolo:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/LqkZebwbYyY" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


¿Qué os ha parecido este análisis de Slipstream? ¿Lo habeis jugado ya? ¿Os parece tan divertido como a nosotros? Escribe tus impresiones sobre este juego en los comentarios, o en nuestro grupos de [Telegram](https://t.me/jugandoenlinux) y [Discord](https://discord.gg/fgQubVY).

