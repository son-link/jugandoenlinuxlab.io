---
author: leillo1975
category: Carreras
date: 2017-08-18 18:35:00
excerpt: "<p>Feral sondea al p\xFAblico con esta directa pregunta.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/878af4972e07916863ae909a162bf74b.webp
joomla_id: 438
joomla_url: f1-2017-podria-llegar-a-linux-steamos-si-hay-suficiente-demanda
layout: post
tags:
- feral-interactive
- codemasters
- f1-2016
- f1-2017
- f1-2015
title: "F1 2017 podr\xEDa llegar a Linux/SteamOS si hay suficiente demanda"
---
Feral sondea al público con esta directa pregunta.

De todos es sabida la decepción del público Linuxero cuando en su día [Feral Interactive anunció que no portaría F1 2016](index.php/homepage/generos/carreras/item/400-no-habra-version-para-linux-del-f1-2016). Las injustas malas ventas de la versión de 2015 hicieron que el juego no vendiese lo suficiente para que la compañía no portase el título de la temporada pasada a nuestro sistema. F1 2015 no es obviamente uno de los mejores títulos de esta licencia, ya que no contaba con muchas de las opciones que poco después volvió a añadir Codemasters a su versión de 2016. Además el juego contó en su salida con muchos bugs y mal rendimiento que hundieron el juego en un reguero de malas críticas. **Es justo decir que estos fallos fueron corregidos con posteriores actualizaciones**, y que el juego cambió mucho desde su lanzamiento, pero el mal ya estaba hecho. Si a todo eso unimos que la F1 es un nicho de juegos no tan populoso como otro tipo de juegos de condución (sagas GRID o DIRT), el esfuerzo de Feral no compensó con las ventas. Personalmente, y como amante de la F1 y los juegos de coches, considero que F1 2015 es uno de los mejores títulos AAA que tenemos para GNU-Linux/SteamOS, a pesar de sus evidentes "fallos".


 


 ![F12017 KEYART LANDSCAPE GLOBAL](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisF12017/F12017-KEYART-LANDSCAPE-GLOBAL.webp)


 


Pero parece que los chicos de Feral quieren volver a intentarlo y han lanzado una pregunta a todos los jugones linuxeros, dejando claro que si ven una demanda suficiente por parte de los usuarios, podrían portar este gran juego a nuestro sistema. Para ello han dejado un mensaje en Twitter y Facebook donde esperan recoger el Feedback que necesitan para decidirse. El Tweet al que debeis contestar demandando el juego en cuestión es el siguiente:


 



> 
> Should Linux re-join our FORMULA ONE team with F1 2017? It won’t be this month if they do, but show us there's demand! [#THEREQUESTINATOR](https://twitter.com/hashtag/THEREQUESTINATOR?src=hash) [pic.twitter.com/MLuN2IJAyv](https://t.co/MLuN2IJAyv)
> 
> 
> — Feral Interactive (@feralgames) [August 18, 2017](https://twitter.com/feralgames/status/898557270962847744)



 


Si por el contrario preferís Facebook [pinchaz aquí](https://www.facebook.com/feralinteractive/posts/1624447540913076). No cabe duda decir que mejor que lo hagais en los dos sitios. F1 2017 saldrá el 25 de Agosto (la semana que viene) para Windows y Mac, y tendrá multitud de novedades importantes con respecto a anteriores versiones, como la inclusión de coches de F1 de los últimos 30 años, un modo carrera mucho más completo donde tendremos muchas mas posibilidades de gestión, trazados alternativos, nuevos modos de juego, multijugador mejorado...


 


Esperemos que en Feral Interactive obtengan una buena respuesta, **algo que también depende de vosotros**; y pronto veamos este magnífico juego de carreras en nuestros equipos linuxeros. Si quereis comprar la versión de F1 2015 para Linux podeis hacerlo en la [**tienda de Feral (recomendado)**](https://store.feralinteractive.com/en/mac-linux-games/f12015/) o en Steam:


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/286570/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


Te dejamos con el sugerente video de F1 2017 para que lo tengas aun más claro:


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/-sBemDAr_IE" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


**Actualización:** He creado un pequeño video donde probamos un rato la anterior entrega para Linux de la serie, F1 2015. Podeis verlo aquí:


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/pWwMIdLyZeY" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


¿Que te parece esta iniciativa de Feral Interactive? ¿Te gustaría poder jugar a este título en Linux? ¿Te gusta la Formula 1? Deja tus impresiones en los comentarios o en nuestro canal de [Telegram](https://t.me/jugandoenlinux).

