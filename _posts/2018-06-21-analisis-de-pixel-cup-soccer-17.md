---
author: Pato
category: "An\xE1lisis"
date: 2018-06-21 08:14:36
excerpt: "<p>F\xFAtbol pixelado con sabor retro: Goooooooooolll!!!</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/51b8ab8a165aa1fb5a57daf7b2513248.webp
joomla_id: 781
joomla_url: analisis-de-pixel-cup-soccer-17
layout: post
tags:
- deportes
- indie
- analisis
- futbol
- soccer
title: "An\xE1lisis de Pixel Cup Soccer 17"
---
Fútbol pixelado con sabor retro: Goooooooooolll!!!

Aprovechando que durante estas fechas se está celebrando el Mundial de Fútbol (soccer para otros) y debido a la falta de los grandes títulos del género en Linux (léase FIFA o PES) nos pusimos manos a la obra de buscar un título que al menos nos permitiese disfrutar de la fiesta del fútbol y poder mitigar las ganas de recurrir a wine o al "dual boot" para quitarnos el mono de balón virtual.


Lo cierto es que actualmente la oferta de juegos de fútbol para nuestro sistema favorito digamos que no está en su mejor momento. Sin embargo rebuscando un poco hemos dado con 'Pixel Cup Soccer 17', un juego de [Batovi Games Studio](http://batovi.com/games/pixelcupsoccer/) que sin pretender ser un juego puntero si que puede resultar interesante para lo que pretendemos: pasarlo bien jugando a un juego de fútbol.


Para cumplir con este cometido Pixel Cup Soccer 17 ofrece una jugabilidad sencilla, con unos gráficos resultones al estilo de los juegos de décadas pasadas y con una jugabilidad que sin ser nada del otro mundo si aporta lo más importante: Diversión.


![pixelcup7](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisPixelcupS/pixelcup7.webp)


*Las competiciones con equipos nacionales* 


#### Diversión sobre simulación: la vía arcade


¿Que necesita un juego de fútbol para ser divertido? movimiento y goles. Y de ambas cosas tenemos en Pixel Cup Soccer 17. Si bien no tenemos una gran variedad de movimientos, al menos tenemos lo más básico: sprints, entradas, saltos... pases largos y cortos, chutes a puerta, remates... el juego se siente fluido en todo momento y permite jugar los partidos con agilidad. En cuanto a la dificultad, digamos que el juego en sí es bien sencillo, con pocos controles básicos y una dificultad de la IA muy baja donde es posible robar el balón, irte del contrario y marcar goles casi desde el campo contrario. Si estás buscando un juego de fútbol desafiante posiblemente este no es tu juego.


Sin embargo, si lo que buscas es divertirte marcando goles entonces te puedes hartar. Extrañamente, siendo un título algo limitado en este aspecto, mas de una vez nos encontramos jugando un partido y ganando por goleada y tras cada gol seguimos pensando "va, venga... vamos a por un gol más".


![pixelcup3](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisPixelcupS/pixelcup3.webp)


#### Configuraciones


Aquí estamos ante uno de los aspectos del juego que menos nos gusta. El juego en si viene con un número fijo y limitado de jugadores y equipos, y no podemos cambiarlos. Los nombres de los jugadores y equipos, salvo las selecciones nacionales son ficticios y los nombres de los jugadores inventados, aunque dado el juego que es tampoco será mucho problema. En cuanto a las selecciones nacionales tenemos un buen número de ellas y aunque los jugadores siguen siendo fijos y los nombres inventados al menos sí tienen los nombres de los países y las banderas.


![pixelcup2](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisPixelcupS/pixelcup2.webp)


*Tendremos a nuestra disposición un buen número de equipos, pero sin nombres conocidos*


#### Modos de juego


Uno de los aspectos mas completos del juego son los modos. Podemos elegir entre un partido amistoso, donde elegiremos nuestro equipo y el del contrario, o diversos campeonatos como Mundial de selecciones, competiciones entre equipos tipo ligas **ya sea de jugadores o jugadoras** (sí, podemos jugar con "jugadoras") o un campeonato "a medida".


![pixelcup6](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisPixelcupS/pixelcup6.webp)


*tendremos a nuestra disposición distintos tipos de competiciones basadas en las reales*


En cualquier caso, tanto los equipos como los emparejamientos ya estarán predeterminados, sin darnos opción a personalizar aspectos como jugadores, emparejamientos, número de equipos participantes etc.


![pixelcup8](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisPixelcupS/pixelcup8.webp)


Lo que sí podremos configurar son los estadios (limitados a 18 y ficticios también), el tiempo de juego y la climatología. En cuanto a la posibilidad de jugar contra un contrincante, podremos elegir entre jugar contra la CPU u otro jugador. Poco más.


#### Diversión en compañía: Multijugador


¿Qué sería de un juego de fútbol si no podemos jugar con amigos? Pixel Cup Soccer 17 nos permite jugar en **modo multijugador local** y es donde pensamos que el juego puede cumplir mejor su cometido. Jugar contra otro jugador humano permite conseguir el desafío que no tenemos jugando contra la IA y que a buen seguro deparará horas de piques y partidos de alta tensión. En cuanto a un modo multijugador en línea, en su momento el estudio anunció que lo desarrollarían, pero dado el tiempo transcurrido desde que lo lanzaron es de suponer que la opción se quedó en el tintero.


#### Sonido


Este es otro de los aspectos "flojos" del juego, aunque es de entender que estando ante un juego de fútbol básico y sin muchas pretensiones tampoco "desentona" en exceso. La música es algo repetitiva y machacona, pero tenemos la ventaja de poder desactivarla en las opciones del juego, y los sonidos de los estadios se limitan a un ruido de fondo que imita al ruido de la multitud. Poco mas. Luego están los típicos sonidos de chutes, silbato... en este aspecto nada del otro mundo. Cumple sin mas.


#### Conclusión


Estamos ante un juego en el más puro sentido indie sencillo y sin pretensiones que fue lanzado en 2016 y que a fecha de hoy aún sigue apareciendo como "acceso anticipado". Esto casi lo dice todo. Es cierto que si el juego contase con algunas opciones básicas de configuración como edición de equipos, cambios entre los jugadores o el modo multijungador online, el juego ganaría muchos enteros. Si bien el estudio anunció mediante un teaser que estas opciones estaban siendo desarrolladas, dado el tiempo transcurrido no parece que vayan a estar disponibles a corto plazo.


Dejando de lado lo limitado de sus opciones o lo básico de su dificultad, si nos centramos en su jugabilidad sencilla y sin complicaciones y en la parte de disfrutar jugando a un juego arcade de fútbol y marcar goles, puede resultar un juego divertido que cumple con su cometido. Para buscar algo mas ajustado al deporte rey, mejor nos vamos a otras opciones, aunque suponga tener que lidiar con otros sistemas.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/PT2Bh_895_s" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


Si quieres jugar a este Pixel Cup Soccer 17, lo tienes disponible en su [página de Steam](https://steamcommunity.com/linkfilter/?url=http://www.pixelcupsoccer.com/).

