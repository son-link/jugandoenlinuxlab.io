---
author: Pato
category: Noticia
date: 2020-09-17 09:20:35
excerpt: "<p>Nos ponemos al d\xEDa con la plataforma de streaming de Google</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Stadia/TWOS-Sept15-Updated.webp
joomla_id: 1255
joomla_url: novedades-stadia-vengadores-inmortals-fenyx-rising-chronos-nba-2k21-hitman-2-3-uno-y-mas
layout: post
title: "Novedades Stadia: Vengadores, Inmortals Fenyx Rising, Chronos, NBA 2K21, Hitman\
  \ 2/3, UNO y m\xE1s..."
---
Nos ponemos al día con la plataforma de streaming de Google


Volvemos con las novedades de Stadia presentando esta semana multitud de contenidos. En primer lugar, la plataforma estrena deportes con el nuevo [**NBA 2K21**](https://stadia.google.com/store/details/b0a2ab7cdc194582bc4d111c07c2e30drcp1/sku/ae31d6ce393f4896b9b9d1f2f1ac676ep), la nueva entrega de la franquicia que nos llevará a conquistar el anillo de la NBA con nuestro equipo favorito. También llega [**HITMAN 2**](https://stadia.google.com/store/details/990ec302c2cd4ba7817cedcf633ab20frcp1/sku/70d2750d0c614a01afbb34e5c4f3a9bc), que ya se puede adquirir y jugar, y nos anuncian la próxima llegada de **[HITMAN 3](https://stadia.google.com/store/details/990ec302c2cd4ba7817cedcf633ab20frcp1/sku/a31dabb87e2441978da11fd60f3197dfp) el 20 de Enero de 2021**.


Pasamos a los pesos pesados, con los Vengadores de [**Marvel's Avengers**](https://stadia.google.com/store/details/232ff8abc7f74421a477e9e09dbf487drcp1/sku/cfc62fc3307a4ef1930e4644646c8179p) donde tendremos la oportunidad de ponernos el traje de nuestro héroe favorito, y que está ya disponible desde el día 1 de Septiembre.


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/JfZqWsPufxo" width="560"></iframe></div>


 


El siguiente en la lista es el nuevo Hit de Ubisoft, [**Immortals Fenyx Rising**](https://stadia.google.com/store/details/161f276db10947a199cd0260ed4dc248rcp1/sku/7f69e006dede4c8eab019dede177c57dp) anteriormente conocido como Gods & Monsters y ambientado en la Grecia mitológica nos traerá acción y aventura a raudales:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/I5oznog-Jxk" width="560"></iframe></div>


Immortals Fenyx Rising llegará a Stadia el próximo día 3 de Diciembre aunque ya está disponible para su pre-compra en la plataforma, y además habrá disponible una demo del juego en próximas fechas.


Hablando de Ubisoft, para los que gustan de bailar en casa, al igual que la versión 2020, **Just Dance 2021** también llegará a Stadia:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/su6AUy0zTus" width="560"></iframe></div>


Y otro de Ubi, **Ryders Republic**, que también llegará en Febrero de 2021:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/Iwfo87gSwHw" width="560"></iframe></div>


 


Seguimos, **Scott Pigrim Vs The World** es otro título de acción arcade que estará disponible para estas Navidades:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/Yagr17GAT0U" width="560"></iframe></div>


 


Por otra parte, si te gusta la lucha libre tendrás la posibilidad de partir huesos con **WWE 2K Battlegrounds**, disponible mañana 18 de Septiembre, y además ya está disponible [**République**](https://stadia.google.com/store/details/65864a95f9e74129845bda0467486413rcp1/sku/2a4af85a84974cdba4d61df620305335), un juego de acción y sigilo que ya tiene unos cuatro años pero que sigue siendo de lo más espectacular, aunque si lo que te va es jugar en familia, quizás el [**UNO**](https://stadia.google.com/store/details/1694aaa3968344228424092f180a3e0ercp1/sku/c147fbe82f394c11bbe41ee134d4f350) sea el juego que buscas, ya disponible por cierto.


En cuanto a los **anuncios**, próximamente llegará a Stadia:


**Risk of Rain 2**


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/PnsVotWCZmw" width="560"></iframe></div>


**CHORUS** (anunciado en la Gasmescom)


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/M9nNOnSTASs" width="560"></iframe></div>


 


**Chronos: Before the Ashes**


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/OVuHLCLbYsQ" width="560"></iframe></div>


 


Por último, vamos con las **ofertas** de la plataforma:


* Assassin’s Creed® Odyssey - $29.99 USD, €34.99
* Borderlands®3 - $29.99 USD, €29.99
* Borderlands®3 Deluxe Edition - $39.99 USD, €39.99
* Borderlands®3 Super Deluxe Edition - $49.99 USD, €49.99
* Borderlands®3 Season Pass - $39.99 USD, €39.99
* Darksiders Genesis - $25.99 USD, €25.99
* Get Packed - $13.99 USD, €13.99
* GRID - $9.99 USD, €9.99
* GRID Ultimate Edition - $14.99 USD, €13.74
* Lara Croft and the Temple of Osiris - $9.99 USD, €9.99
* Monopoly - $17.99 USD, €17.99
* Rise of the Tomb Raider: 20 Year Celebration - $10.49 USD, €10.49
* Red Dead Redemption 2 - $40.19 USD, €40.19
* Red Dead Redemption 2: Special Edition - $53.59 USD, €50.24
* Red Dead Redemption 2: Ultimate Edition - $59.99 USD, €53.99
* Shadow of the Tomb Raider Definitive Edition - $19.79 USD, €19.79
* Tomb Raider: Definitive Edition - $9.99 USD, €9.99
* Tom Clancy’s Ghost Recon Breakpoint - $29.99 USD, €29.99
* Tom Clancy’s Ghost Recon Breakpoint: Year 1 Pass - $19.99 USD, €19.99
* Trials Rising - $12.49 USD, €12.49


Y para los suscriptores de Stadia PRO:


* Assassin’s Creed® Odyssey - $14.99 USD, €20.29
* Assassin’s Creed Odyssey - Stadia Season Pass - $19.99 USD, €14.99
* Tom Clancy’s Ghost Recon Breakpoint - $19.79 USD, €20.29
* Trials Rising - $7.49 USD, €9.99
* Trials Rising - Expansion Pass - $7.99 USD, €7.99


Recuerda, todo esto lo tienes o tendrás disponible para jugarlo **de manera oficial en tu escritorio Linux** mediante tu navegador basado en Chromium. Te dejo con los títulos que puedes jugar gratis este mes con tu suscripción:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/ePjpgzJ4TVo" width="560"></iframe></div>


 

