---
author: leillo1975
category: "Simulaci\xF3n"
date: 2023-03-16 09:20:00
excerpt: "<p>El equipo de desarrollo acaba de publicar esta nueva versi\xF3n cargadita\
  \ de novedades.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SpeedDreams/230Release.webp
joomla_id: 1528
joomla_url: la-version-2-3-0-de-speed-dreams-lanzada-oficialmente
layout: post
tags:
- open-source
- speed-dreams
- simracing
title: "La versi\xF3n 2.3.0 de Speed Dreams, lanzada oficialmente"
---
El equipo de desarrollo acaba de publicar esta nueva versión cargadita de novedades.


  
Hace poco más de un año y medio, en pleno verano, estábamos celebrando el lanzamiento de la [versión 2.2.3]({{ "/posts/speed-dreams-alcanza-oficialmente-la-version-2-2-3" | absolute_url }}) de este singular simulador de carreras libre. En aquel momento se empezaban a abrir paso nuevas características en el juego que con esta nueva release se confirman y amplían. Como veis en el siguiente tweet se acaba de hacer pública en sus redes sociales (twitter y mastodon) y por fin podemos disfrutarla:



> 
> [#SpeedDreams](https://twitter.com/hashtag/SpeedDreams?src=hash&ref_src=twsrc%5Etfw) 2.3.0 is finally here full of new features. You can find more info and downloads for [#Windows](https://twitter.com/hashtag/Windows?src=hash&ref_src=twsrc%5Etfw), [#Linux](https://twitter.com/hashtag/Linux?src=hash&ref_src=twsrc%5Etfw) and [#MacOS](https://twitter.com/hashtag/MacOS?src=hash&ref_src=twsrc%5Etfw) on our official website:<https://t.co/rGI3DN0DNN>[#OpenSource](https://twitter.com/hashtag/OpenSource?src=hash&ref_src=twsrc%5Etfw) [#NewRelease](https://twitter.com/hashtag/NewRelease?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/gxVzNXnULd](https://t.co/gxVzNXnULd)
> 
> 
> — Speed Dreams Official - Open Motorsport Simulator (@speeddreams_oms) [March 16, 2023](https://twitter.com/speeddreams_oms/status/1636282740692639744?ref_src=twsrc%5Etfw)


 



  
El juego, previamente a esta versión final ha pasado por un periodo de [Beta]({{ "/posts/ya-esta-aqui-la-primera-beta-de-la-nueva-version-de-speed-dreams" | absolute_url }}) y de [Release Candidate]({{ "/posts/speed-dreams-2-3-0-release-candidate" | absolute_url }}), donde además de corregir múltiples errores, se han mejorado incluso algunas de sus caracteríticas.  
  
Para quien aun no conozca Speed Dreams (si es que queda alguien), se trata de un Simulador de carreras de coches Open Source, donde es posible realizar carreras en diferentes tipos de pistas (circuitos, tierra, carretera, ovales...) con coches de multiples categorias (deportivos de calle, clásicos, monoplazas, Rally, GTs...). Se trata de un fork del conocidisimo TORCS, pero en este caso mucho más evolucionado y enfocado al jugador (TORCS está enfocado a las carreras de IA). El juego dispone de trabajados gráficos en 3D, una física realista, múltiples tipos de control, includo el soporte de Force Feedback en volantes , mutijugador local a traves de LAN y pantalla partida, y más cosas que dejamos para que vosotros mismos descubrais


 


En esta nueva versión encontrareis entre otras muchas, las siguientes novedades:


* Se ha añadido la **temperatura y degradación** de los neumáticos en las categorías Supercars, 67GP, MPA11 y MPA12, además de la ya existente MP1.
* Nuevos **widgets para el HUD en OSG**, como neumáticos, fuerzas, delta o controles, además de los ya existentes.
* Nuevos **sonidos** en los **menús del juego**, así como en el **cambio de marchas**, el **derrape** de los neumáticos o algunos **motores** de los coches.
* Nuevas **opciones** en el **menú de sonido** y ajuste del **volumen en el juego**.
* Nuevos **interiores y pilotos animados** en algunos modelos de **Supercoches**, como el Boxer 96, el Cavallo 360, el FMC GT4 y el Kanagawa Z35 GT-S.
* Ahora es posible **cambiar el Setup del coche dentro del juego**. También se puede elegir el **combustible inicial** y los **grados de giro del volante que aparecen en la pantalla** del juego.
* Nuevos **coches** como el **Mango MS7** y el **Murasama 37A**, ambos en la categoría "Grand Prix 1967".
* Nuevos **coches** y **robots** en la categoría **MPA12**: **Deckard** y **Spirit** añadidos a los coches Murasama existentes
* Nueva **pista** basada en el circuito de **Melbourne**.
* Añadidos montones de **nuevos skins** para varios coches, especialmente en la **categoría LS-GT1**
* Un enorme trabajo en **trackeditor** con toneladas de nuevas características, correcciones y optimizaciones
* Añadida la **temperatura en el widget de la rueda** en el motor gráfico **SSG**.
* Montones de **correcciones y optimizaciones en el modelo de neumáticos**. Ahora es más fácil conseguir que los neumáticos alcancen la temperatura óptima.
* Un montón de **nuevos robots en muchas categorías**
* Mejor **frenado** en los robots **shadow** y **dandroid**
* Mejor **soporte** para sistemas **multipantalla**.
* Nueva **web oficial y dominio**: <https://speed-dreams.net>(gracias a @SonLink)
* Puesta en marcha del nuevo "**Master Server**" (<https://speed-dreams.net/masterserver/>). Es necesario registrarse de nuevo para poder usarlo y enviar nuestros mejores tiempos.
* Nuevas cuentas en **Redes Sociales** de **Twitter** y **Mastodon**: <https://twitter.com/speeddreams_oms> y <https://mastodon.social/web/@speed_dreams_official>
* Nuevos **canales de vídeo** en **Youtube** y **Peertube** (LinuxRocks): <https://www.youtube.com/@speeddreams_oms> y <https://peertube.linuxrocks.online/c/speed_dreams_oms/videos>
* También puedes descargar Speed Dreams en la **nueva web de Itch.io**: <https://speed-dreams.itch.io/speed-dreams>
* Muchas correcciones, optimizaciones y, por supuesto, limpieza de código


Como veis el equipo de desarrollo no ha parado en todo este tiempo y los que habeis jugado a la anterior versión notareis la diferencia enseguida. Ahora solo teneis que haceros con el juego en su [página web oficial](https://www.speed-dreams.net/), y para ello hay varias opciones, como simplemente bajaros la **AppImage** y ejecutarla, instalar el **Flatpak** (ambos desarrollados por @Sonlink), o los que teneis Ubuntu y derivadas, el usar el **PPA** de [Xtradeb.net](https://xtradeb.net/play/speed-dreams/).  
Os dejamos con un video que han publicado mostrándonos algunas de estas novedades:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/bowQ4etGDzg" title="YouTube video player" width="780"></iframe></div>


 Si queréis comentar algo o tenéis cualquier duda sobre Speed Dreams, podéis poneros en contacto con nosotros en nuestra [sala de Matrix "Zona Racing"](https://matrix.to/#/#zona_racing-jugandoenlinux:matrix.org) o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

