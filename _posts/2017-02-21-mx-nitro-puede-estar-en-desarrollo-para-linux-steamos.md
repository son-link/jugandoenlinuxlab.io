---
author: Pato
category: Carreras
date: 2017-02-21 21:56:10
excerpt: <p>El juego ha aparecido en SteamDB Linux con apartado para nuestro sistema</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/f9a9b7c9f33a923e5475b478a62125ae.webp
joomla_id: 228
joomla_url: mx-nitro-puede-estar-en-desarrollo-para-linux-steamos
layout: post
tags:
- deportes
- carreras
- rumor
title: MX Nitro puede estar en desarrollo para Linux/SteamOS
---
El juego ha aparecido en SteamDB Linux con apartado para nuestro sistema

Después de repasar las novedades atrasadas, ayer SteamDB Linux twitteó otra novedad interesante:



> 
> New Game:  
> MX Nitro<https://t.co/4c1DBS0yYS>
> 
> 
> — SteamDB Linux Update (@SteamDB_Linux) [February 20, 2017](https://twitter.com/SteamDB_Linux/status/833681884047618048)



 Si [visitas la web](https://steamdb.info/app/488690/depots/) puede verse un nuevo apartado llamado MX Nitro LNX con el logotipo del sistema operativo Linux. ¿Significa esto que llegará con seguridad a Linux/SteamOS?


Pues no. No es la primera vez que aparece un apartado para Linux en la base de datos de Steam y luego el juego no ha llegado a salir para nuestro sistema. Sin embargo, este MX Nitro pertenece a un género (motos y carreras arcade) del que sinceramente salvo 'Road Redemption' y alguna otra excepción menor no abunda en Linux/SteamOS.


MX Nitro [[web oficial](http://mxnitro.game/)] es un juego de carreras de motocross, donde la velocidad y las acrobacias se dan la mano. Tendrás que conducir rápido, volar alto y lograr combos haciendo las acrobacias más arriesgadas, para conseguir Nitros necesarios para ganar:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/C3x_XUmWWdE" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


 ¿Qué te parece MX Nitro? ¿Te gustaría que llegase a Linux/SteamOS?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

