---
author: leillo1975
category: Software
date: 2018-09-23 19:30:00
excerpt: "<p>Acaba de publicarse la versi\xF3n 0.80</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/35dbf9f9868cc61eca6b56ad6668243d.webp
joomla_id: 857
joomla_url: grandes-avances-en-la-ultima-version-de-dxvk
layout: post
tags:
- wine
- vulkan
- steam-play
- dxvk
- proton
title: "Importantes avances en la \xFAltima versi\xF3n de DXVK."
---
Acaba de publicarse la versión 0.80

Si es que queda alguien que no sepa aun a estas alturas que rayos es [DXVK](https://github.com/doitsujin/dxvk), por decirlo de una forma fácil y entendible por todos, se trata de un complemento, plugin o como querais llamarlo que basicamente lo que hace es traducir DirectX10/11 usando la potencia de Vulkan, para poder ejecutar apliaciones/juegos con Wine de una forma mucho más rápida y fiel al original.


Los avances que ha experimentado este proyecto en los último tiempos han sido tan rápidos y efectivos que incluso la todapoderosa Valve a contratado a su creador [doitsujin](https://github.com/doitsujin) para "energizar" su genial [Steam Play - Proton](index.php/homepage/generos/software/item/960-steam-play-de-valve-da-un-salto-de-gigante-y-amplia-enormemente-el-catalogo-para-linux-steamos-actualizado). Probablemente sin los esfuerzos de este programador Valve no daría el paso o al menos no lo haría tan pronto.


Volviendo al titular de esta noticia, hace tan solo unas horas se acaba de publicar la versión 0.80 que trae [importantes mejoras](https://github.com/doitsujin/dxvk/releases) que os detallamos a continuación (traducción online):



> 
> ***Mejoras***
> 
> 
> *Añadida Caché de Estado (detalles abajo).*  
>  *Direct3D Feature Level 11_1 está ahora soportado.*  
>  *Menor reducción general de la sobrecarga de la CPU.*
> 
> 
> ***Corrección de errores***
> 
> 
> *Corregidos los fallos en algunos sistemas APU sin memoria de vídeo dedicada ([#640](https://github.com/doitsujin/dxvk/issues/640))*  
>  *Assetto Corsa: Corregidos los fallos y artefactos cuando los reflejos están deshabilitados ([#648](https://github.com/doitsujin/dxvk/issues/648))*  
>  *Project Cars 2: Arreglado el fallo al cargar el juego ([#375](https://github.com/doitsujin/dxvk/issues/375), [#641](https://github.com/doitsujin/dxvk/issues/641))*
> 
> 
> ***Caché de estado***
> 
> 
> *Para reducir la ralentización en las ejecuciones posteriores de una aplicación, DXVK ahora almacena en caché el estado de las tuberías, lo que le permite compilar shaders antes de lo que lo hace actualmente, incluso si la caché del shader del controlador se invalida después de una actualización. Esto puede causar temporalmente una carga de CPU muy alta.*
> 
> 
> *De forma predeterminada, esta función está activada y los archivos de caché se crean normalmente en el mismo directorio donde se encuentra el ejecutable del juego. Consulte el LÉAME para más detalles.*
> 
> 
> 


Como veis los cambios introducidos en esta nueva versión no son moco de pavo, especialmente en el caso de la **Caché de estado**, que en diversas [pruebas](https://www.reddit.com/r/linux_gaming/comments/9i1k6b/more_dxvk_performance_coming_soon/) realizadas ha incrementado sustancialmente los frames por segundo mostrados en pantalla, especialmente aumentando los mínimos. Por ahora podemos disfrutar de todas las mejoras usando Wine (o Lutris), pero **esperemos que Valve lo incluya lo antes posible en Proton** para que podamos disfrutarlo directamente con nuestros juegos de Windows en Steam para Linux.


 


¿Que os parece el proyecto DXVK? ¿Habeis comprobado ya estas mejoras? Dejanos tus opiniones y pruebas en los comentarios o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

