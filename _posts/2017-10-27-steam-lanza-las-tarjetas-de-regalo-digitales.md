---
author: Pato
category: Noticia
date: 2017-10-27 14:54:40
excerpt: "<p>Ahora ser\xE1 m\xE1s f\xE1cil hacer regalos</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/e6b24df417ad7ceb7a489b8a35382a8c.webp
joomla_id: 509
joomla_url: steam-lanza-las-tarjetas-de-regalo-digitales
layout: post
tags:
- steamos
- steam
title: Steam lanza las tarjetas de regalo digitales
---
Ahora será más fácil hacer regalos

Reproducimos el post que Steam han publicado en su blog:


*"Las tarjetas regalo digitales están ahora disponibles, haciendo que sea más fácil dar el regalo perfecto a tus amigos.*


*Las tarjetas regalo digitales de Steam hacen que sea fácil y rápido enviar fondos a la cartera de Steam de tus amigos para que puedan comprar lo que quieran en Steam, desde software a hardware, incluyendo juegos y objetos dentro del juego.*


*Para comprar una tarjeta regalo digital, debes entrar a tu cuenta de Steam, seleccionar la cantidad y a un amigo, y Steam enviará tu regalo con un mensaje personalizado directamente a su cuenta de Steam."*


Para más información, puedes visitar [este enlace](http://store.steampowered.com/digitalgiftcards/).


¿Eres usuario de las tarjetas de Steam?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

