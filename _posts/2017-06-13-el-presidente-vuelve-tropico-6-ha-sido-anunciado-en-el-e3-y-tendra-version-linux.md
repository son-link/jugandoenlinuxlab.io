---
author: Pato
category: Estrategia
date: 2017-06-13 14:14:02
excerpt: "<p>El juego llegar\xE1 en alg\xFAn momento del pr\xF3ximo a\xF1o 2018</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/3a4499a4c30d6f082d254099c90f2c24.webp
joomla_id: 372
joomla_url: el-presidente-vuelve-tropico-6-ha-sido-anunciado-en-el-e3-y-tendra-version-linux
layout: post
tags:
- proximamente
- simulador
- estrategia
title: "'El Presidente' vuelve! Tropico 6 ha sido anunciado en el E3 y tendr\xE1 versi\xF3\
  n Linux"
---
El juego llegará en algún momento del próximo año 2018

Seguimos con las alegrías del E3, que empiezan a llegar a Linux/SteamOS. En este caso nos llegan noticias gracias a [gamingonlinux.com](https://www.gamingonlinux.com/articles/el-presidente-returns-tropico-6-announced-and-linux-will-be-supported.9827?utm_content=buffer6a1e4&utm_medium=social&utm_source=twitter.com&utm_campaign=buffer) de que Tropico 6 llegará a Linux tal y como [han anunciado](http://www.worldoftropico.com/en/el-presidente-returns-tropico-6-revealed-at-e3-2017/) Kalypso Media y Haemimont Games en su [web oficial](http://www.worldoftropico.com/). 


Para el que no la conozca, la saga Tropico es una serie de juegos de estrategia y gestión política en la que debemos gobernar una nación al modo "dictatorial". Vamos, que eres el jefe, y punto:



> 
> *En tiempos de agitación política y malestar social, los ciudadanos necesitan líderes visionarios que dirijan el destino de su país con diligencia e ingenio. Demuestra una vez más tu valía como temible dictador u hombre de estado pacifista en la isla de Tropico y erige el futuro de tu nación durante cuatro eras emblemáticas. Enfréntate a nuevos retos a nivel internacional y ten siempre en cuenta las necesidades de tu pueblo.*
> 
> 
> *Por primera vez en esta saga, podrás gobernar grandes archipiélagos, construir puentes para conectar tus islas y utilizar nuevos medios de transporte e infraestructuras. Envía hordas de tropicanos a robar las maravillas del mundo, la Estatua de la Libertad y la Torre Eiffel incluidas. Decora tu palacio a tu gusto y pronuncia discursos electorales desde el balcón para ganarte el apoyo de tus súbditos.*
> 
> 
> 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/GfzoQJKwZNg" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


 Algunas características:


*Por primera vez en esta saga, podrás jugar en enormes archipiélagos. Gobierna varias islas a la vez y enfréntate a los nuevos retos.*


 *Envía a tus agentes a tierras extranjeras para robar las maravillas del mundo y otros monumentos para tu colección.*


 *Levanta puentes, construye túneles y transporta a tus ciudadanos y turistas en taxis, autobuses y teleféricos. Tropico 6 ofrece medios de transporte e infraestructuras completamente nuevas.*


 *Personaliza tu palacio y escoge entre todos los extras disponibles.*


 *Tropico 6 incluye un sistema de investigación revisado que se centra en los aspectos políticos de ser el mayor dictador del mundo.*


 *¡Vuelven los discursos electorales! Dirígete a los ciudadanos y haz promesas que no podrás cumplir.*


En cuanto a la versión de Linux, dejan muy claro que el juego llegará a nuestro sistema favorito en [su hilo de preguntas y respuestas](http://steamcommunity.com/app/492720/discussions/0/1354868867730368405/) en Steam. Aún no se saben los requisitos del juego, pero si quieres echar un vistazo y saber más, pásate por la web oficial o por su [página de Steam](http://store.steampowered.com/app/492720/Tropico_6/).


¿Te gusta la saga Tropico? ¿Eres más dictador o te va el rollo pacifista? 


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 

