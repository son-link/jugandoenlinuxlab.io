---
author: leillo1975
category: Rol
date: 2017-06-07 07:22:48
excerpt: "<p>Esta edici\xF3n rinde tributo al m\xEDtico grupo.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/8835fed4de3847a26822c5cc338a0cab.webp
joomla_id: 361
joomla_url: victor-vran-overkill-edition-disponible-para-gnu-linux-steamos
layout: post
tags:
- lemmy
- overkill
- motoerhead
- fractured-worlds
- victor-vran
- haenimont-games
title: 'Victor Vran: Overkill Edition disponible para GNU-Linux/SteamOS'
---
Esta edición rinde tributo al mítico grupo.

Hace más o menos un año y medio todos los que somos seguidores del Heavy Metal y el Rock en general recibíamos la triste noticia de la muerte de uno de su iconos más reconocibles. El inconbustible [Lemmy](https://es.wikipedia.org/wiki/Lemmy_Kilmister)  de [Motörhead](https://es.wikipedia.org/wiki/Mot%C3%B6rhead) había fallecido, algo que parecía imposible conociendo su historial. Detrás de su alargada y controvertida sombra nos dejaba su genial legado de archiconocidos temas que formaron parte de la banda sonora de muchas generaciones: Ace of Spades, Hellraiser, Iron Fist y por supuesto **Overkill**.


 


En el día de ayer, los chicos de [Haenimont Games](https://www.haemimontgames.com/) (Saga Trópico, Omerta, Tzar) han lanzado al público dos nuevas expansiones para su juego [Victor Vran:](https://www.victorvran.com/) [**Motörhead Through the Ages**](http://store.steampowered.com/app/462290/Victor_Vran_Mtorhead_Through_The_Ages/) y [**Fractured Worlds**](http://store.steampowered.com/app/493880/Victor_Vran_Fractured_Worlds/), formando conjuntamente la [**Overkill Edition**](http://store.steampowered.com/bundle/3288/Victor_Vran_Overkill_Edition/) que las incluye junto con [el juego base](http://store.steampowered.com/app/345180/Victor_Vran_ARPG/). Las expansiones se pueden adquirir también por separado si ya tenemos Victor Vran. Aquí os dejamos el tweet con el que anunciaban su salida:


 



> 
> Victor Vran: Overkill Edition is out now! Watch the full trailer here: <https://t.co/rRgxJipgzk> [pic.twitter.com/HrTZM98RJk](https://t.co/HrTZM98RJk)
> 
> 
> — Victor Vran (@victorvran) [6 de junio de 2017](https://twitter.com/victorvran/status/872045419940925441)



 


Para quien no lo conozca, Victor Vran es un juego de los que se pueden englobar como Action RPG's , al estilo de Diablo o Torchlight, donde debemos enfrentarnos a hordas de monstruos, zombies, vampiros, arañas, etc. Con estas expansiones el juego amplia su universo, siendo la primera de las ampliaciones una immersión en el mundo de Motörhead, con 3 fases donde con nuevas armas como revolveres y guitarras, deberemos derrotar a demonios, escorpiones, perros del infierno o sugerentes súcubos; todo ello por supuesto en escenarios con la típica iconografía y música de la banda.


 


En el caso de "Fractured Worlds" encontraremos cuatro mazmorras generadas aleatoriamente que cambian cada día, además de "la Grieta", donde nos podremos poner a prueba cada vez que nos adentramos más y más en sus eternas profundidades. Podremos también hacer uso de los Talismanes que modificarán el aspecto y habilidades de nuestro heroe. Estos también podrán ser creados mediante recetas, permitiendonos ser más poderosos.


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/bqA3JSMcdL0" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


Podeis comprar Victor Vran: Overkill Edition en [GOG](https://www.gog.com/game/victor_vran) y Steam:


 [![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/VVOverkillSteam.webp)](http://store.steampowered.com/bundle/3288/Victor_Vran_Overkill_Edition/)


 


¿Eres fan de la mítica banda? ¿Que te parecen estas expansiones? ¿Has jugado ya a Victor Vran? Puedes responder a estas preguntas a los comentarios o usar nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD).

