---
author: Pato
category: Software
date: 2022-02-16 09:31:24
excerpt: "<p>Nueva versi\xF3n de esta popular herramienta #OpenSource de @ValveSoftware</p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Proton/valve-Proton.webp
joomla_id: 1429
joomla_url: steam-play-se-actualiza-con-proton-7-0-a3
layout: post
tags:
- valve
- steam-play
- proton
- codeweavers
- steam-deck
title: Steam Play se actualiza con  Proton 7.0-3
---
Nueva versión de esta popular herramienta #OpenSource de @ValveSoftware


**ACTUALIZADO 15-6-22:**


Volvemos una vez más a traeros información sobre los [últimos cambios](https://github.com/ValveSoftware/Proton/releases/tag/proton-7.0-3) introducidos en Proton por el equipo de desarrolladores, que como veis vuelven a ser importantes tanto en cantidad como en calidad, ampliando aun más la compatibilidad de los juegos en GNU-Linux, y por ende en la Steam Deck. Aquí os dejamos el listado:



* *Ahora se pueden jugar los siguientes juegos:*  



	+ - Age of Chivalry
		- Beneath a Steel Sky
		- Chrono Cross: The Radical Dreamer Edition
		- Cities XXL
		- Cladun X2
		- Cursed Armor
		- Flanarion Tactics
		- Gary Grigsby's War in the East
		- Gary Grigsby's War in the West
		- Iragon: Prologue
		- MechWarrior Online
		- Small Radios Big Televisions
		- Split/Second
		- Star Wars Episode I Racer
		- Stranger of Sword City Revisited
		- Succubus x Saint
		- V Rising
		- Warhammer: End Times - Vermintide
		- We Were Here Forever
* *Agregado soporte para Windows.Gaming.Input.*
* *Mejora el FPS en Street Fighter V durante las partidas en línea.*
* *Mejora el rendimiento de Sekiro: Shadow Die Twice en ciertas áreas.*
* *Repara el bloqueo de Elden Ring durante un juego prolongado.*
* *Repara el nuevo lanzador de Final Fantasy XIV Online.*
* *Soluciona el bloqueo de DEATHLOOP después de una suspensión prolongada del sistema.*
* *Soluciona el bloqueo de The Turing Test al iniciar el Capítulo 4.*
* *Arreglado la compatibilidad con el controlador en Mini Ninja.*
* *Arreglado Resident Evil Revelations 2 que no se inicia en Steam Deck.*
* *Corrige la reproducción de video en: Disintegration, Dread X Collection: The Hunt, EZ2ON REBOOT : R, El Hijo - A Wild West Tale, Ember Knights, Outward: Definitive Edition, POSTAL4: No Regerts, Power Rangers: Battle for the Grid, Solasta: Crown of the Magister, Street Fighter V, The Room 4: Old Sin. Reparada la reproducción de video en Ghostwire: Tokyo y otros juegos usando los códecs VP8 y VP9.*
* *Arreglada la compatibilidad con el controlador sin volante en WRC10.*
* *Solucionado que S&box no encuentra ningún juego para unirse.*
* *Arreglado The Legend of Heroes: Zero no Kiseki Kai fallando al iniciar por primera vez.*
* *Solucionado el bloqueo de Mortal Kombat Komplete cuando hay dispositivos de audio con nombres largos.*
* *Arreglado el manejo de enlaces externos en Castle Morihisa.*
* *Mejorada la representación de texto en Rockstar Launcher.*
* *Mejorada la detección del volante.*
* *Admitido el reordenamiento del controlador xinput en Steam Deck.*
* *Actualizado Wine Mono a [7.3.0](https://github.com/madewokherd/wine-mono/releases/tag/wine-mono-7.3.0) .*
* *Actualizado dxvk-nvapi a [v0.5.4](https://github.com/jp7677/dxvk-nvapi/releases/tag/v0.5.4) .*
* *Actualizdo dxvk a v1.10.1-57-g279b4b7e.*





---


**ACTUALIZADO 22-4-22:**


Han pasado ya más de dos meses desde que los desarrolladores de Valve y Codeweavers lanzasen la primera revisión de la versión 7.0 de Proton. Como sabeis la cadencia de estas suele ser un poco menos espaciada en el tiempo, pero cierto es que continuamente han estado trabajando en la [rama experimental](https://github.com/ValveSoftware/Proton/wiki/Changelog) añadiendo y corrigiendo cosas. En esta ocasión, la actualización viene bien cargada de novedades, sobre todo en lo tocante a la compatibilidad, y es que contentar a los felices usuarios de la Steam Deck exige esfuerzos en permitirles poder jugar a cada vez más y más variados juegos. En esta ocasión la [lista de cambios](https://github.com/ValveSoftware/Proton/releases/tag/proton-7.0-2) es la siguiente:


* Ya se puede jugar:  
  
 *Atelier Ayesha  
 Devil May Cry HD Collection  
 Dragon Quest Builders 2  
 Una salida  
 Fall in Labyrinth  
 King of Fighters XIII  
 Montaro  
 ATRI -My Dear Moments-  
 Guilty Gear Isuka  
 INVERSUS Deluxe  
 Metal Slug 2 y 3 y X  
 One Shot y One Shot: Fading Memory  
 Call of Duty Black Ops 3  
 Saint Seiya: Soldiers' Soul  
 Dinastía Medieval  
 Memoria Brillante: Infinite  
 Trilogía de Double Dragon  
 Baseball Stars 2  
 Elden Ring*
* Arreglar que The Last Campfire no se inicie en el Steam Deck.
* Arreglar que STAR WARS Jedi Knight - Jedi Academy no muestre nada en el Steam Deck.
* Arreglar los juegos de Unity que se bloquean al iniciarse con ciertos periféricos conectados (por ejemplo, el receptor unificador de Logitech).
* Se ha corregido el fallo de Microsoft Flight Simulator tras una actualización reciente del juego.
* Corregir el bloqueo de Quake Champions tras una actualización reciente del juego.
* Corregir el multijugador en UNO.
* Arreglar algunos juegos antiguos, como Deus Ex GOTY, Prey 2006, Quake 4 y Chaser, que tienen bandas visibles, especialmente en las sombras, en comparación con los Proton más antiguos.
* Se ha corregido la regresión en el rendimiento de Swords of Legends Online.
* Se ha corregido la reproducción de vídeo en Atelier Meruru, Cook-out, DJMAX RESPECT V, Gloomhaven, Haven, Rust, Rustler, The Complex, TOHU, Monster Train, Hardspace: Shipbreaker, Car Mechanic Simulator 2021, Nine Sols Demo.
* Arreglar que DiRT Rally 2 y DiRT 4 no puedan conectarse al servidor del juego.
* Arreglar el bloqueo de Cyberpunk 2077 con configuraciones de audio 4.0.
* Corregir el cuelgue aleatorio al salir en Little Nightmares 2.
* Arreglar el bloqueo de Civilization VI y Chicken Invaders Universe a los pocos minutos de empezar la partida.
* Arreglar el problema de Assassin's Creed Odyssey, que muestra un aviso de controlador no compatible en la superposición.
* Arreglar la falta de audio en las escenas de Persona 4 Golden.
* Corregir el bajo rendimiento de Forza Horizon 5 con fsync activado.
* Arreglar la falta de actualización de Uplay / Ubisoft Connect con fsync activado.
* Arreglar STAR WARS: Squadrons mostrando una advertencia sobre los controladores obsoletos.
* Se ha corregido el fallo de Devil May Cry 5 y Capcom Arcade Stadium al desplazarse por los vídeos demasiado rápido.
* Corregir el bloqueo aleatorio de GTA V en algunos gestores de ventanas.
* Corrección del fallo aleatorio de Teardown.
* Corrección de Melty Blood: Type Lumina se cuelga en el vídeo de introducción.
* Arreglar el lanzador de Arma 3.
* Arreglar el chat de VR que no maneja bien la suspensión/reanudación.
* Arreglar que Vampyr y The Beast Inside muestren los menús con unos pocos píxeles de ancho en las cubiertas de Steam.
* Corregir que Apex Legends a veces se inicia minimizado en Steam Decks.
* Arreglar que Quake Live no pueda conectarse a las partidas multijugador.
* Se ha corregido el problema de que Killing Floor 2 no se conecta al servidor de objetos.
* Corrección de la ventana de inicio de sesión de Xbox que se retrasa al actualizar el contenido de la ventana.
* Arreglar que Horizon Zero Dawn se ejecute en cámara lenta.
* Corregir el fallo de Age of Chivalry al iniciar una partida.
* Arreglar que Ride 3 tenga el menú principal aplastado horizontalmente en el Steam Deck.
* Corregir el tartamudeo del vídeo de introducción de Chrono Trigger.
* Corregir el fallo de Divinity: Original Sin - Enhanced Edition se bloquea al cambiar la resolución o la sincronización.
* Actualización de **dxvk** a la [v1.10.1](https://github.com/doitsujin/dxvk/releases/tag/v1.10.1).
* Actualizar **vkd3d-proton** a la [versión 2.6](https://github.com/HansKristian-Work/vkd3d-proton/releases/tag/v2.6).
* Actualización de **dxvk-nvapi** a la [versión 0.5.3](https://github.com/jp7677/dxvk-nvapi/releases/tag/v0.5.3)


 




---


 **NOTICIA ORIGINAL 16-2-22:** Hoy nos encontramos con una nueva versión mayor de Proton, la **7.0** de esta capa de compatibilidad para ejecutar juegos en Linux y Steam Deck.


Según [el anuncio](https://twitter.com/Plagman2/status/1493854504290119682) de Pierre Loup **Proton 7.0** viene con mejoras en audio para **Skyrim y Fallout**, soporte para **decodificación local H264,** nuevos juegos considerados jugables y la base para soporte de **Easy Anti Cheat heredado**. Esto supone que juegos con versiones anteriores de EAC ahora pueden ser jugables. Como ejemplo, Pierre pone a SW: Squadrons y Knockout City como jugables con soporte EAC, y anuncia el soporte de más juegos muy pronto.


Estos son los juegos jugables que se indican:


* Anno 1404
* Call of Juarez
* DCS World Steam Edition
* Disgaea 4 Complete+
* Dungeon Fighter Online
* Epic Roller Coasters XR
* Eternal Return
* Forza Horizon 5
* Gravity Sketch VR
* Monster Hunter Rise
* NecroVisioN
* Nights of Azure
* Oceanhorn: Monster of the Uncharted Seas
* Order of War
* Persona 4 Golden
* Resident Evil 0
* Resident Evil Revelations 2
* Rocksmith 2014 Edition
* SCP: Secret Laboratory
* Wargroove
* Wartales
* Yakuza 4 Remastered


En cuanto a las mejoras, incluyen:


* Añadido soporte para EasyAntiCheat si el juego ha habilitado un módulo Linux
* Añadido soporte para la decodificación local de videos H264
* Arreglado el chat de voz de Sea of Thieves.
* Solucionado el bloqueo de Sea of Thieves atascado en "buscando suministros frescos" al unirse al modo multijugador.
* Mejorado el soporte del controlador Steam Input para juegos que se ejecutan a través de Origin.
* Arreglado el crasheo de Beacon cuando se encuentra en un desafío diario / semanal.
* Mejorado el soporte de audio en Skyrim y Fallout 4.
* Solucionado el parpadeo en el lanzador de Mount & Blade II: Bannerlord.
* Arreglado Age of Empires IV y Marvel's Avengers mostrando advertencias de controladores gráficos.
* Arreglado el audio en Mass Effect 1 de Mass Effect Legendary Edition.
* Mejora la estabilidad de Runescape.
* Solucionado el guardado rápido en Castlevania Advance Collection.
* Mejorado el soporte de Paradox Launcher.
* Arreglado el cuelgue al salir en Pathfinder: Wrath of the Righteous.
* Arreglado la regresión de Far Cry: el juego se puede jugar nuevamente.
* Arreglado el modo multijugador de Doom Eternal.
* Diversas mejoras en el rendimiento en torno a la entrada, las ventanas y la asignación de memoria que anteriormente estaban en Experimental.
* Actualizado Wine a 7.0.
* Actualizado DXVK a 1.9.4.
* Actualizado vkd3d-proton a 2.5-146-g33f17cc7.
* Actualizado wine-mono a 7.1.2.


 Todas las notas de versión y la descarga de Proton [en este enlace](https://github.com/ValveSoftware/Proton/wiki/Changelog#available-in-proton-70).


Ya puedes seleccionar Proton 7.0 en tu cliente de Steam, en la sección *"Parámetros - Steam Play"*

