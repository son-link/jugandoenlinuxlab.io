---
author: Pato
category: "Acci\xF3n"
date: 2020-05-12 18:52:32
excerpt: "<p>Al menos as\xED lo afirman desde la cuenta oficial de&nbsp;@TheGameKitchen\
  \ en Kickstarter</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Blasphemous.webp
joomla_id: 1219
joomla_url: blasphemous-llegara-a-linux-junto-a-su-primer-dlc
layout: post
tags:
- accion
- indie
- plataformas
- proximamente
title: "Blasphemous llegar\xE1 a Linux junto a su primer DLC"
---
Al menos así lo afirman desde la cuenta oficial de @TheGameKitchen en Kickstarter


Tras muchos meses sin noticias sobre **Blasphemous**, el aclamado título desarrollado por los sevillanos The Game Kitchen parece que por fin tendremos la posibilidad de jugarlo en nuestro sistema favorito.


Siguiendo la noticia de [gamingonlinux.com](https://www.gamingonlinux.com/2020/05/linux-version-of-blasphemous-still-coming-due-to-arrive-with-the-first-dlc), sabemos que en respuesta a una pregunta de un aficionado sobre cuando estaría disponible el título para aquellos que lo apoyaron en campaña por su promesa de lanzar una versión nativa para Linux, la respuesta oficial es: *"[las versiones] Linux y Mac están previstas para aparecer (finalmente!) coincidiendo con el DLC1"*. Puedes ver el comentario [en este hilo](https://www.kickstarter.com/projects/828401966/blasphemous-dark-and-brutal-2d-non-linear-platform/comments?comment=Q29tbWVudC0yNzcyNzQ0Mw%3D%3D&reply=Q29tbWVudC0yNzc0NzcyOA%3D%3D).


Sin embargo, aún no hay fecha estimada para este lanzamiento, por lo que tocará esperar a ver si tras sucesivos retrasos finalmente podemos echarle el guante a este excelente título.


*Blasphemous es un juego de acción y plataformas 2D con un brillante apartado artístico "pixel art" y un transfondo pseudo-religioso que nos pone en la piel del "Penitente", un personaje atormentado que luchará contra criaturas diabólicas y seres grotescos atormentados por una sociedad ultra-religiosa. En la tierra de Ortodoxia se ha desatado la corrupción, y "el Penitente" tedrá que luchar para contener las hordas de seres que han corrompido su alma.*


A continuación puedes ver el último trailer del juego:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/1hV28NDRj3E" width="560"></iframe></div>


Si quieres saber más, puedes visitar su [página web oficial](https://thegamekitchen.com/blasphemous/), o su [página de Steam](https://store.steampowered.com/app/774361/Blasphemous/).


 

