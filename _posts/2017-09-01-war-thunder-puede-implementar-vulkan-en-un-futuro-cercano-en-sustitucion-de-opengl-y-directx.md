---
author: Pato
category: "Simulaci\xF3n"
date: 2017-09-01 21:56:18
excerpt: <p>La desarrolladora Gaijin es una colaboradora de la API en Kronos Group</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/14539468928e429f2da74154ccea470c.webp
joomla_id: 450
joomla_url: war-thunder-puede-implementar-vulkan-en-un-futuro-cercano-en-sustitucion-de-opengl-y-directx
layout: post
tags:
- accion
- simulador
- vulkan
title: "'War Thunder' puede implementar Vulkan en un futuro cercano en sustituci\xF3\
  n de OpenGL y DirectX"
---
La desarrolladora Gaijin es una colaboradora de la API en Kronos Group

No hemos hablado de War Thunder en Jugando en Linux, pero es prácticamente el único exponente que tenemos en cuanto a simulación aérea (y de tanques) de combate bélico de la 2ª Guerra Mundial y épocas cercanas a esta.


Hoy, gracias a [www.gamingonlinux.com](https://www.gamingonlinux.com/articles/war-thunder-may-replace-opengl-and-directx-9-with-vulkan-in-the-near-future.10266/#comments) nos llegan noticias de que Gaijin, el estudio detrás del juego está pensando en la posibilidad de sustituir OpenGL y DirectX tanto en Linux como en Windows respectivamente por la API Vulkan de la que son colaboradores en Kronos Group. Todo dependerá de la implementación de la API en los drivers de las distintas tarjetas gráficas, aunque al parecer ven bastantes posibilidades. Puedes ver el hilo oficial del juego donde hablan de los planes de futuro para el juego [en este enlace](https://warthunder.com/en/news/4933-development-answers-from-developers-en).


War Thunder [[página oficial](https://warthunder.com/es/)] es un "MMO free to play" propiamente dicho que fue lanzado en 2013 y ofrece soporte en Linux desde hace ya bastante tiempo, con un aspecto visual bastante aceptable y un grado de dificultad progresivo donde pueden darse cabida desde los que buscan una simulación mas realista hasta los que buscan enfrentamientos mas "casuales".


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/tSxO5NrN0zI" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Desde luego, es muy positivo que cada vez más estudios importantes estén pensando en migrar a Vulkan con lo que ello significa para poder tener desarrollos multiplataforma.


Si quieres probarlo, puedes descargar el [cliente para Linux](https://yupmaster.gaijinent.com/launcher/current.php?id=WarThunderLauncherLinux) desde la página oficial o directamente desde su página de Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/236390/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 

