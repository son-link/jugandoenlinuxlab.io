---
author: yodefuensa
category: "An\xE1lisis"
date: 2018-04-21 22:02:16
excerpt: <p>Nuestro colaborador nos repasa el excelente juego de Bombservice</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/b77c990e633dac0ff65890b0db32f237.webp
joomla_id: 718
joomla_url: analisis-momodora-reverie-under-the-moonlight
layout: post
tags:
- analisis
- momodora
- bombservice
title: 'Analisis: Momodora: Reverie Under The Moonlight'
---
Nuestro colaborador nos repasa el excelente juego de Bombservice

Momodora: Reverie Under The Moonlight, es el cuarto título de la saga Momodora, saga producida por el pequeño estudio de tan solo tres integrantes, [Bomb Service](http://www.bombservice.com/) tras el cual se encuentra [Rdein](https://rdein.itch.io/), director y diseñador principal.



> 
> "En "Momodora: Reverie Under the Moonlight" explorarás una tierra maldita en plena decadencia. El mal se propaga, los muertos se alzan y la corrupción reina. La esperanza es un recuerdo lejano para todos, excepto una sacerdotisa llamada Kaho, venida de la aldea de Lun. Una audiencia con su majestad, la reina, posiblemente salvaría las tierras--pero el tiempo escasea y cada noche es mas oscura que la anterior"
> 
> 
> 


Para quien no lo sepa nos encontramos ante una precuela. Aun así no hace falta jugar los títulos anteriores para entender la historia. La trama no es muy compleja, pero será un buen hilo conductor que nos guiara de principio a fin. El juego no es especialmente largo, con aproximadamente unas 6 horas de juego. Una longitud comedida y una dificultad acorde a un reto pero sin llegar a resultar imposible. Nos ofrece la oportunidad de sumarle horas gracias a los logros y a niveles de dificultad endiablada, para quien así lo desee.


La jugabilidad esta claramente inspirada en la saga Metroid, y si has disfrutado a estos juegos en la nes o en la gba vas a disfrutar de Momodora. Nuestro personaje ira desbloqueando o comprando mejoras, como en cualquier plataformas. De igual manera, la exploración, el uso del mapa o las zonas de guardado son muy similares a lo ya visto en otros juegos del mismo genero. 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/9wJI_VjFqlg" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Siguiendo con el apartado de la jugabilidad, me gustaría resaltar las zonas de guardado y es que no disponemos de un guardado automático si no que deberemos acercarnos a unas campanas para guardar el progreso. Para quien no este acostumbrado a esto, deberá saber lo que esto implica. Y es que si nos matan después de desbloquear una mejora o haber descubierto medio mapa significará volver a donde estábamos, teniendo que desbloquearlo de nuevo. Esto supone lo más frustrante del juego pero no es para nada criticable. Las zonas de guardado esta bien repartidas y separadas unas de otras, sin ser abundantes y siempre alguna cerca de un enemigo final, lo que consigue equilibrar la dificultad.


En cuanto a las mecánicas de juego, parece obvio pero no todos los juegos cumplen. El control del personaje es rápido, cómodo y responde a lo que le pedimos. Las mejoras del personaje son bastante tibias, si bien es cierto que son mejoras, no da la impresión de que realmente sea lo que va a marcar la diferencia entre ganar y perder. Por poner un ejemplo, tenemos a nuestra disposición, flechas que hacen más daño, que atraviesan a los enemigos, daño para envenenar o quemar, entre otros. Pero solo podemos llevar a la vez 2 efectos activos y 3 pasivos. Por lo que tenemos que decidir que es lo que más nos conviene. Si, son una ayuda, pero el hacer un poco más de daños tampoco añade nada nuevo, solo va a ser dar un golpe más a los enemigos y dos minutos más contra los jefes finales. 


Los enemigos no presentan excesivo peligro una vez dominemos los movimientos basta con rodar en el momento oportuno, si bien no es difícil recorrer el mapa, es difícil mantener la constate perfección y el "timing" para hacerlo. En ese sentido los jefes finales son iguales. Cada boss cuenta con 3 o 4 mecánicas que deberemos aprender si queremos salir con vida, una vez aprendamos esto, debemos hacer acopio de nuestros reflejos y habilidades al mando. Puede parecer que los jefes finales son simples, pero la gran cantidad de ellos y la dificultad que entrañan, muestran un buen ejemplo de prueba y error que sin llegar a abrumar, puede resultar desesperante en algunos casos pero no imposible.


En el aspecto negativo es que a mi parecer el juego se deja cosas en el tintero. Si es verdad que desbloqueamos mejoras pero no son de gran peso ni añaden algo diferente a la jugabilidad. El hecho de poder convertinos en gato en algún momento del juego, realmente sorprende pero no aporta nada, no cambia las mecánicas, ni añade puzzles, solo pequeños atajos y secretos coleccionables. 


En el aspecto gráfico, encontramos lo que es el punto fuerte del juego. Destaca en su estetica pixel art muy bien construida, y aunque los enemigos sean una repetición unos de otros sin mucha variedad, no podemos decir lo mismo de los escenarios. Las diferentes zonas tendrán su ambientación. Las zonas del bosque como los jardines del palacio rebosan vida. Mientras que los interiores de la ciudad, con su toque apagado consiguen dar un efecto lúgubre muy conseguido. En algunos momentos los escenarios pueden llegar a parecer vacíos, pero son momentos puntuales. 


![https://steamcdn-a.akamaihd.net/steam/apps/428550/ss_051cd90e0d4329259efe71cba64c439cdd607beb.600x338.jpg?t=1518694123](https://steamcdn-a.akamaihd.net/steam/apps/428550/ss_051cd90e0d4329259efe71cba64c439cdd607beb.600x338.jpg?t=1518694123)


La banda sonora de este juego es bastante buena. Pero en ningún momento se sube el volumen para dar más protagonismo a este elemento, por lo tanto no destaca y las canciones pasan bastante desapercibidas. En general está por encima de la media de otros juegos, y dar mayor énfasis a este elemento hubiera hecho que tuviera mejor valoración (a mi forma de ver), porque el trabajo estaba ahí. Mencionar también lo bien implementados que están los efectos de sonido al golpear a cualquier enemigo, no desentonan y suenan en el momento justo.


En rasgos generales es un buen juego, del que todo fan de las aventuras 2D o del genero metroidvania va a disfrutar, que evita tener puntos débiles para que sus virtudes salgan reforzadas. Disponible desde 9,99€ sin oferta, esta más que recomendado. Lo tienes disponible tanto en la tienda de [Humble Bundle](https://www.humblebundle.com/store/momodora-reverie-under-the-moonlight?partner=jugandoenlinux), en [GOG,](https://www.gog.com/game/momodora_reverie_under_the_moonlight) como en su página de Steam en español:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/428550/88600/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


Este artículo ha sido escrito por un invitado [[Yodefuensa](index.php/homepage/analisis/itemlist/user/210-yodefuensa)]. Si quieres, puedes mandarnos tus propios artículos y reviews y nosotros las prublicaremos. Puedes hacerlo [desde este enlace](index.php/contacto/envianos-tu-articulo).

