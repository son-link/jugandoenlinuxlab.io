---
author: Serjor
category: Ofertas
date: 2018-08-06 18:30:37
excerpt: <p>Una manera de poder ponernos al otro lado del videojuego</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/cdb07fe52edfc220cecf3f84017ef78e.webp
joomla_id: 821
joomla_url: bundles-de-programacion-en-humble-bundle
layout: post
tags:
- humble-bundle
- humble-store
- programacion
title: "Bundles de programaci\xF3n de videojuegos en Humble Bundle"
---
Una manera de poder ponernos al otro lado del videojuego

¿Alguna vez has querido desarrollar tu propio videojuego? Yo sí, y la verdad, no es nada fácil, y hay que tener talento en todos los ámbitos, programando, dibujando, animando, componiendo música y sonidos fx... Un árbol de habilidades más complejo que el de cualquier RPG de hoy en día, pero la gente de Humble Bundle quiere aportar su granito de ayuda y nos trae un par de bundles centrados en el diseño y desarrollo de videojuegos (links de afiliado):


* [Game Design & Puzzlecraft](https://www.humblebundle.com/books/puzzlecraft-books?partner=jugandoenlinux)
* [Program your own games](https://www.humblebundle.com/books/program-your-own-games-books?=partner=jugandoenlinux)


Así que ya sabéis, si os interesa el desarrollo de videojuegos, echar un vistazo a los bundles, y si os hacéis con ellos a través de los enlaces de afiliado, nos ayudaréis a pagar los costes del host.


Y tú, ¿qué tipo de juego te gustaría desarrollar? Cuéntanoslo en los comentarios y en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD)

