---
author: leillo1975
category: Estrategia
date: 2017-06-13 14:02:34
excerpt: "<p>En unos meses podremos disfrutar de esta expansi\xF3n del m\xE1gnifico\
  \ juego de estrategia</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/eb037833adde6dd3469d047f4c1e5ad0.webp
joomla_id: 371
joomla_url: feral-nos-traera-xcom-2-war-of-the-chosen
layout: post
tags:
- feral-interactive
- dlc
- xcom-2
- war-of-the-chosen
- expansion
title: "Feral nos traer\xE1 XCOM 2 : War of the Chosen (ACTUALIZACI\xD3N)"
---
En unos meses podremos disfrutar de esta expansión del mágnifico juego de estrategia

**ACTUALIZADO (29-8-17):** Feral acaba de anunciar en twitter que esta DLC se lanzará en el mes de Septiembre, sin confirmar una fecha concreta. Como habían prometido, esta expansión saldrá poco después de salir para Windows (donde ha salido hoy mismo). Si quereis ver el Tweet podeis hacerlo aquí:


 



> 
> Greetings, Commander. We have new intel to report. [#Xcom2](https://twitter.com/hashtag/Xcom2?src=hash): War of the Chosen will be released for macOS and Linux this September! [pic.twitter.com/Pe9i0jTVJt](https://t.co/Pe9i0jTVJt)
> 
> 
> — Feral Interactive (@feralgames) [August 29, 2017](https://twitter.com/feralgames/status/902469076181803008)



 


Lo reconozco, XCOM es una de mis Sagas favoritas de videojuegos, y ayer me llevaba un sorpresón a enterarme **mediante el PC Gaming Show** que habría una nueva expansión para su segunda parte. Hoy me he llevado una sorpresa también, aunque quizás no tanto, porque me daba en la nariz que Feral Interactive no iba a dejar escapar la oportunidad de portar tan suculenta DLC. Aquí está el anuncio oficial:


 



> 
> Soon the real war begins with [#XCOM2](https://twitter.com/hashtag/XCOM2?src=hash): War of the Chosen for macOS and Linux. The full briefing on our minisite – <https://t.co/lbh4CUds8v> [pic.twitter.com/gqbdhFC0cn](https://t.co/gqbdhFC0cn)
> 
> 
> — Feral Interactive (@feralgames) [June 13, 2017](https://twitter.com/feralgames/status/874552574862663681)



 


El juego se espera para Windows el 29 de Agosto, pero según nos comunica Feral en sus [noticias](http://www.feralinteractive.com/es/news/772/), **el lanzamiento para Linux y Mac será "poco después"**, sin aclarar si será poco-poco o poco-mucho. De todas formas, en mi caso seguro que la espera se me hace laaarga.  Pero vamos a lo que nos atañe y hablemos un poco de las novedades que presenta.


 


XCOM 2 : War of the Chosen será una expansión dependiente, por lo que **necesitareis del juego base para poder jugarla** (que por cierto, [teneis ahora mismo en Steam al 60% de descuento por menos de 20€](http://store.steampowered.com/app/268500/XCOM_2/)), y en ella encontraremos múltiples y suculentas novedades, entre las que encontraremos ([minisitio de Feral](http://www.feralinteractive.com/es/games/xcom2/xcom2warofthechosen/)):



> 
> 
> * ***NUEVAS FACCIONES Y CLASES DE HÉROES:** Surgieron tres nuevas facciones para reforzar la resistencia de la Tierra: Segadores, Guerrilleros y Templarios, cada una con habilidades diferentes y filosofías opuestas. Estas facciones proporcionan soldados heroicos poderosos para ayudar en las misiones y ofrecen más oportunidades en el plano de la estrategia.*
> * ***LOS ELEGIDOS:** Los Elegidos son los enemigos más astutos a los que jamás se haya enfrentado XCOM y cada uno tiene sus propios puntos fuertes y débiles, que se verán en cada nueva campaña. Los Elegidos van a la caza del comandante y secuestrarán, interrogarán y matarán a los soldados de XCOM para conseguir su objetivo. Los Elegidos también pueden irrumpir en el plano estratégico y desbaratar las operaciones globales de XCOM. Encuentra y asalta las fortalezas de los Elegidos para derrotar al enemigo de una vez por todas.*
> * ***NUEVAS AMENAZAS ALIENÍGENAS Y DE ADVENT:** Un alienígena nuevo y mortal –conocido como Espectro y que es capaz de crear dobles oscuros de los soldados de XCOM– se ha infiltrado en el campo de batalla. Adopta nuevas tácticas para contrarrestarlo, además de los ataques explosivos del Purificador de ADVENT y del Sacerdote de ADVENT, que rebosa poder psíquico.*
> * ***NUEVOS ENTORNOS Y OBJETIVOS DE MISIONES:** Lleva a cabo misiones tácticas en nuevos entornos; desde ciudades abandonadas y asoladas por armas biológicas alienígenas durante la invasión original hasta túneles subterráneos y apartadas regiones xenoformas.*
> * ***MEJORAS EN EL PLANO ESTRATÉGICO:** Gestiona las relaciones de XCOM con las facciones y contrarresta las operaciones de los Elegidos desde el Avenger. Utiliza las nuevas órdenes de la resistencia para dar prioridad a tu estrategia personal. Ahora puedes desplegar soldados, científicos e ingenieros en operaciones encubiertas que conceden suministros y aumentan la simpatía de las facciones al completarlas.*
> * ***MAYOR PERSONALIZACIÓN Y REJUGABILIDAD:** Los soldados pueden desarrollar vínculos con los compañeros compatibles y lograr así nuevas habilidades y mejoras. El sistema de los informes de situación añade nuevos modificadores al plano táctico para cerciorarse de que cada misión ofrezca un reto único. Las opciones avanzadas de campaña permiten ajustar con mayor precisión la duración y la dificultad del juego.*
> * ***MODO RETO:** Sigue la estrategia perfecta en los nuevos retos de la comunidad para conseguir el primer puesto en el marcador global. ¡Solo tendrás una oportunidad!*
> * ***COMPARTE LA RESISTENCIA:** Personaliza a tus soldados y hazles posar y luego añade filtros, textos y fondos para crear tus propios carteles de la resistencia para que aparezcan en el juego y compartirlos con tus amigos.*
> 
> 
> 
> 


 


Sin más os dejo con el trailer de adelanto del juego para que se os haga la boca agua:


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/n3bENBYSucQ" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


 ¿Os gustan los juegos de XCOM? ¿Qué os parecen las novedades que presenta esta nueva expansión? Deja tu respuesta en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Discord.](https://discord.gg/ftcmBjD)

