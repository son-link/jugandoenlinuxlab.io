---
author: Pato
category: "Acci\xF3n"
date: 2019-06-11 14:03:56
excerpt: "<p><span class=\"css-901oao css-16my406 r-1qd0xha r-ad9z0x r-bcqeeo r-qvutc0\"\
  \ dir=\"auto\">@kalypsomedia</span> lanzar\xE1 el juego proximamente. #LinuxGaming</p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/7779d18a61414ff94a179235d0ab0ca4.webp
joomla_id: 1062
joomla_url: commandos-2-hd-remaster-uno-de-los-clasicos-de-pyro-studios-llegara-a-linux
layout: post
tags:
- accion
- estrategia
- tactico
title: "Commandos 2 HD Remaster, uno de los cl\xE1sicos de Pyro Studios llegar\xE1\
  \ a Linux (ACTUALIZACI\xD3N)"
---
@kalypsomedia lanzará el juego proximamente. #LinuxGaming


**ACTUALIZADO 11-12-19:**


Según leemos en el [Blog de Kalipso Media](https://blog.kalypsomedia.com/en/kalypso-media-founds-third-internal-studio-for-new-commandos-game/), se acaba de anunciar que el **proximo 24 de Enero** llegará finalmente la esperada remasterización del aclamado Commandos 2, precedida de una versión Beta que se podrá disfrutar a partir de este mismo viernes, día 13 de Diciembre, aunque no queda claro si esta Beta  incluirá el soporte para nuestro sistema. Para acceder a ella será necesario precomprar el juego ([Humble Store](https://www.humblebundle.com/store/commandos-2-hd-remaster?partner=jugandoenlinux) y [Steam](https://store.steampowered.com/app/1100410/Commandos_2__HD_Remaster/)).


Aprocechando la ocasión, Kalipso Media anunció que está poniendo en marcha un nuevo estudio dentro de [Gaming Minds](http://www.gamingmindsstudios.com/) en Frankfurt para trabajar en nuevos títulos de esta afamada saga de juegos, y donde según su director, Jürgen Reußwig (anteriormente en Sunflowers y Games Distillery), desarrollarán un digno sucesor para Commandos con un lanzamiento simultaneo tanto en PC como en consolas, por lo que están a la [busca de personal cualificado](https://www.kalypsomedia.com/uk/studio-jobs) que quiera participar en el proyecto. Habrá pues, que estar atentos a las noticias que nos lleguen desde Kalipso y cruzar los dedos para que el próximo Commandos llegue a nuestro sistema.




---


**NOTICIA ORIGINAL:**


Los mas viejos del lugar recordarán la saga Commandos como una de las obras maestras de la edad de oro del videojuego español. Pues bien, estamos de enhorabuena, ya que **Kalypso Media ha confirmado vía twitter** la llegada de **Commandos 2 HD Remaster** a nuestro sistema favorito.


 



> 
> Strategie-Legenden in neuem Glanz: [#Commandos2](https://twitter.com/hashtag/Commandos2?src=hash&ref_src=twsrc%5Etfw) – [#HDRemaster](https://twitter.com/hashtag/HDRemaster?src=hash&ref_src=twsrc%5Etfw) und [#Praetorians](https://twitter.com/hashtag/Praetorians?src=hash&ref_src=twsrc%5Etfw) – HD Remaster kommen noch dieses Jahr für PC, PS4 und Xbox One!   
> Die [#Commandos2HD](https://twitter.com/hashtag/Commandos2HD?src=hash&ref_src=twsrc%5Etfw) Remaster erscheint außerdem für Mac, Linux, Nintendo Switch & iPad sowie Android Tablets. [pic.twitter.com/Y9PLygvskm](https://t.co/Y9PLygvskm)
> 
> 
> — Kalypso Media Group (@kalypsomedia) [June 11, 2019](https://twitter.com/kalypsomedia/status/1138426121542864896?ref_src=twsrc%5Etfw)



Y es que la editora adquirió los derechos de la exitosa saga de la legendaria **Pyro Studios** y ahora quiere remasterizarlos para traerlos a nuestros días con un mas que bienvenido lavado de cara, para lo cual han contado con el estudio de desarrollo **Yippee Entertainment**. Además, Kalypso Media ha anunciado el remaster de otro clásico como es Praetorians HD, aunque este no tendrá versión nativa en Linux al estar siendo desarrollado por otro estudio por lo que tendremos que esperar que tenga buen soporte por parte de Steam Play/Proton.


El anuncio viene además con un vídeo comentado por el mismísimo Ignacio Perez Dolset, leyenda viva del videojuego donde las haya (en inglés):


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/mYxYiTYKEI8" width="560"></iframe></div>


**Características:**


* Reimaginado en HD con controles modificados, interfaz de usuario modernizada y un tutorial revisado.
* Entornos interactivos: roba uniformes y armas del enemigo, escala postes, colúmpiate de cables, nada, utiliza vehículos y sube o baja de edificios, barcos y aviones.
* Controla nueve comandos únicos, cada uno con diferentes habilidades y especializaciones que incluyen boina verde, francotirador, experto en demoliciones, buzo, seductora y ladrón.
* La primera aparición de la serie Commandos en un moderno motor 3D: gira el entorno 360 grados, muévete dentro y fuera de los edificios, barcos, aviones y submarinos, y haz zoom dentro y fuera del entorno.
* Escenarios auténticos de la Segunda Guerra Mundial: 10 misiones que abarcan 9 entornos diferentes en día y noche, con efectos meteorológicos realistas.
* La elección es tuya: cómo abordas cada misión depende de ti. Experimenta con habilidades y armas en un desafiante estilo de juego "contra todas las probabilidades".
* Vehículos y armas de la Segunda Guerra Mundial, incluidos jeeps, tanques, camiones, barcos, bazucas y lanzallamas.


En cuanto a los requisitos para nuestro sistema favorito, aún no se sabe nada, pero siendo el juego que es tampoco creemos que sea muy exigente. Commandos 2HD Remaster llegará en el último cuarto de este mismo año vía Steam, por lo que si quieres saber más puedes visitar su página en la tienda.


¿Qué te parece la llegada de la remasterización de Commandos 2 HD Remaster? ¿Has jugado a algún juego de la saga con anterioridad?


Cuéntamelo en los comentarios, o en el canal de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux).

