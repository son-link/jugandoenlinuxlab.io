---
author: Pato
category: "Acci\xF3n"
date: 2018-04-13 17:08:53
excerpt: <p>El juego por ahora sigue en fase de acceso anticipado solo para Windows</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/dc7259495409db483f53803df58f17e8.webp
joomla_id: 710
joomla_url: se-viene-el-primer-battle-royale-para-linux-darwin-project-aparece-en-steamdb
layout: post
tags:
- accion
- rumor
- battle-royale
title: "\xBFSe viene el primer \"Battle Royale\" para Linux? Darwin Project aparece\
  \ en SteamDB"
---
El juego por ahora sigue en fase de acceso anticipado solo para Windows

Con la moda de los "battle royale" en todo su auge, y **no disponemos ninguno de los "grandes" del género en nuestro sistema favorito**. Es cierto que alguno hay, pero son juegos "menores" de tipo "indie" o ejecutados en navegadores, pero un "Battle Royale" al uso, estilo PUBG o Fortnite, de eso no tenemos.... al menos de momento, por que hace tan solo unas horas [ha aparecido en SteamDB](https://steamdb.info/app/544920/depots/?branch=linux) **'Darwin Project'** incluyendo dos contenedores, uno llamado "Linux" a secas y otro "devellinuxshipping".


'Darwing Project' es juego que ha estado en desarrollo con una fuerte consideración en las valoraciones y sugerencias de los usuarios, incluyendo modos de juego tipo "cazador" y "supervivencia", un "director"del show que "hostea" e influye en la partida con diferentes "power ups" e interacciones durante la partida por parte de los espectadores de las partidas.


Desde un primer momento los desarrolladores dijeron que las puertas a una versión Linux no estaban cerradas, eso sí, no de salida. Tanto es así que en su [primera ronda de preguntas](https://steamcommunity.com/app/544920/discussions/0/2949168687328773283/) y respuestas dijeron que uno de sus "arquitectos", un tal Thierry le encantaría hacer una versión para Linux. El caso es que parece ser que tras su lanzamiento el pasado 9 de Marzo en fase de acceso anticipado, parece que han tomado interés en lanzarlo en nuestro sistema favorito.


*'Darwin Project' tiene lugar en un futuro apocalíptico distóptico en el norte de las islas de Canadá. Como preparación para una edad de hielo, se lanzará un nuevo proyecto mitad experimento científico, mitad entretenimiento que propondrá a 10 participantes sobrevivir al frío y la muerte en una arena traicionera.*


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/RllGB0j27f0" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


Estaremos atentos a las novedades de este "Battle Royale" y su posible llegada a Linux/SteamOS.


Si quieres saber más, puedes visitar su página web oficial [en este enlace](http://www.scavengers.ca/).


¿Que te parece 'Darwin Project' como juego "battle royale"? ¿Crees que será el primero en llegar a Linux? ¿Te gustaría que llegaran otros "battle royales"?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 

