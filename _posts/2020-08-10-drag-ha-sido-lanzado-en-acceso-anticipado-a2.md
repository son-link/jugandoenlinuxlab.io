---
author: leillo1975
category: Carreras
date: 2020-08-10 19:53:54
excerpt: "<p><span class=\"css-901oao css-16my406 r-1qd0xha r-ad9z0x r-bcqeeo r-qvutc0\"\
  >@OrontesG</span> acaba de publicar la versi\xF3n 0.2.8.1 con importantes cambios.\
  \ </p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/DRAG/drag_3_EA-Release.webp
joomla_id: 1247
joomla_url: drag-ha-sido-lanzado-en-acceso-anticipado-a2
layout: post
tags:
- demo
- drag
- thorsten-folkers
- orontes-games
- hoja-de-ruta
title: DRAG ha sido lanzado en Acceso Anticipado (ACTUALIZADO 2)
---
@OrontesG acaba de publicar la versión 0.2.8.1 con importantes cambios. 


 **ACTUALIZADO 28-1-21:** Llevábamos unos cuantos meses sin informar sobre el desarrollo de este juego, pero eso no quiere decir que la gente de [Orontes Games](https://orontesgames.com/) haya estado parada ni mucho menos. **El desarrollo ha estado muy activo con constantes actualizaciones y revisiones** en su frenético juego. Un servidor, que ha estado probando las nuevas versiones a lo  largo de todo este periplo,  ha podido constatar que el juego ha ganado mucho a lo largo de estos meses, especialmente en el terreno de la maniobrabilidad, con importantísimas mejoras en el terreno del Force Feedback.   



Hace tan solo unas horas el estudio alemán publicaba una **nueva versión, la 0.2.8.1** con multitud de [nuevas características](https://store.steampowered.com/news/app/773840/view/3032580220550351499), tal y como podemos ver en el siguiente tweet:



> 
> DRAG version 0.2.8.1 is out with revised online race mode, new track variants, full force feedback for steering wheels, a new special event and much more! 🥳[#gamedev](https://twitter.com/hashtag/gamedev?src=hash&ref_src=twsrc%5Etfw) [#indiedev](https://twitter.com/hashtag/indiedev?src=hash&ref_src=twsrc%5Etfw) [#indiegame](https://twitter.com/hashtag/indiegame?src=hash&ref_src=twsrc%5Etfw) <https://t.co/X9Baw2Hk8q>
> 
> 
> — Orontes Games (@OrontesG) [January 27, 2021](https://twitter.com/OrontesG/status/1354547647944814593?ref_src=twsrc%5Etfw)


  





 En la lista de cambios podemos encontrar las siguientes novedades, que sin duda mejorarán la jugabilidad de este título:


***Se ha revisado completamente el apartado Multijugador:***


*Ahora puedes reaparecer en cualquier momento durante una carrera online. El r**espawn tiene un pequeño retraso de tiempo dependiendo de la posición actual.** Cuanto más adelante en el terreno, más tarda el respawn.*


*En los Campeonatos en línea **ahora obtienes puntos por las calificaciones y las carreras, dependiendo de tu resultado**. El campeonato termina cuando un piloto tiene 125 puntos. El piloto con más puntos gana el campeonato y comienza un nuevo campeonato. Además **la parrilla de salida de las carreras está ahora invertida.** Esto hace que las carreras sean más emocionantes y reñidas. Funciona bien junto con el nuevo sistema de puntos:*



*Se han añadido **tres nuevas variantes de circuitos de carreras para el online y dos de ellas para la contrarreloj** (Zona-B trazado inverso, Zona-A ovalada con vuelta joker, Zona-A trazado corto inverso). También hemos añadido 10 etapas de eventos especiales anteriores como calificaciones en línea. Junto con los 20 desafíos clasificatorios anteriores, ahora hay 30 etapas clasificatorias para elegir.*


***Force Feedback completo para los volantes:***


***La retroalimentación de fuerza ahora transfiere las fuerzas físicas correctas de las ruedas al volante**. Puedes reducir o aumentar la retroalimentación de fuerza con el ajuste de intensidad de la vibración.* 


*Para el ajuste de rango, 540° con una linealidad del 50% ha demostrado ser un buen punto de partida. Ahora **se está trabajando en la compatibilidad con los frenos de mano analógicos**, que llegará con una futura actualización.*


***Añadido nuevo evento especial 13:*** 


*El nuevo Evento Especial acaba de comenzar y durará hasta el 9 de febrero. Hay una tabla de clasificación actualizada en directo que también captura los dispositivos de entrada utilizados en nuestra comunidad de Discord. También se comparten vídeos de los intentos de récord y de los mejores tiempos personales.* 


***Se han añadido contenedores móviles y paredes de neumáticos** en los circuitos y desafíos. Esto también incluye a la pista que aparece en la demo*


***Otros cambios en la versión 0.2.8.1***


- *La configuración de tu vehículo desde el garaje se utiliza ahora en los eventos especiales*
- *La configuración del volumen de audio ahora se gradúa más finamente*
- *Se ha corregido un error de colisión con el muro de los neumáticos en las carreras online*
- *Se ha corregido un raro fallo en la primera entrada de dispositivo*
- *Se ha corregido un fallo poco frecuente al cargar el nivel en línea.*
- *Los mensajes del chat se muestran ahora durante más tiempo.*


![](https://cdn.akamai.steamstatic.com/steamcommunity/public/images/clans/31529403/97f803e912d2ab5d4e25131f80be1bc425b9ac9c.jpg)


Como podeis ver los cambios no son pocos y añadidos a todos los que se han ido incluyendo a lo largo de estos meses mejoran en gran medida la experiencia jugable de DRAG, que ya de por si era muy buena. Sin duda un título recomendadisimo para todos los que os gustan los juegos de conducción y los nuevos desafios. Seguimos informando....

---


**ACTUALIZADO 17-8-20**: Acabamos de saber por una [noticia publicada en Steam](https://store.steampowered.com/newshub/app/773840/view/4598691868981756374) que **la Demo de DRAG está otra vez disponible** en la tienda de [Steam](https://store.steampowered.com/app/773840/DRAG/) para todos aquellos que no hayais probado el juego o querrais volver a jugarla desde el Steam Summer Festival.  No menos interesante (o quizás más incluso) es también la **publicación de la hoja de ruta para el desarrollo del juego durante el Acceso Anticipado** que podeis ver en esta imagen:


  
![roadmap](https://cdn.cloudflare.steamstatic.com/steamcommunity/public/images/clans/31529403/ebe9942bd3f9bc65cb8eb871d5926ab41224b6c8.jpg)


Si no conseguis ver bien las letras pinchad en la imagen. De todas formas **os las dejamos aquí traducidas** para que no hay problema:


  
**Multiplayer Online:**


* Creación de Salas
* Sistema de Ranking Online
* Modo Campeonato


**Pantalla Dividida:**


* Soporte para cuatro jugadores
* Pantalla dividida para dos jugadores con teclado (WSAD y teclas de flecha)
* Opción de dificultad más fácil para la pantalla dividida
* Partidas Online en pantalla dividida


**General:**


* Force Feedback en Volantes
* Modo Foto
* Editor de Fases (crea tus propias pruebas)
* Repeticiones
* Salvar y cargar coches fantasma
* Experimentos con Realidad Virtual


**Pistas:**


* Zona D con 8 pruebas (incluyendo una pista más facil para el online)
* Vegetación destruible
* Zona E con 8 pruebas
* Zona F con 8 pruebas


**Vehículos:**


* Vehículo especial para la "Zona X"
* Lanzamiento de barro de los neumáticos
* Más personalización de los vehículos
* Sonido de colisiones de los vehículos


**Idiomas:**


* Traducciones del Ruso, Italiano, Portugués, Turco, Portugués de Brasil, Chino, Koreano y Japonés.


Como veis tienen un largo camino cargado por delante, por lo que estos dos hermanos les queda trabajar duro hasta el lanzamiento definitivo del juego. Como siempre, nosotros en JugandoEnLinux.com seguiremos pendientes de cualquier información para traerosla lo antes posible.

