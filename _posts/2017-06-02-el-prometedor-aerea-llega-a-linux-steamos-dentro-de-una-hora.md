---
author: Pato
category: Rol
date: 2017-06-02 12:25:23
excerpt: "<p>As\xED lo ha anunciado la editora Soedesco \"hace una hora\"</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/cef09c4dc3d0d8d265ab7ea9482b510d.webp
joomla_id: 355
joomla_url: el-prometedor-aerea-llega-a-linux-steamos-dentro-de-una-hora
layout: post
tags:
- accion
- indie
- aventura
- rol
title: El prometedor 'AereA' llega a Linux/SteamOS dentro de una hora
---
Así lo ha anunciado la editora Soedesco "hace una hora"

Así por sorpresa nos ha llegado la noticia de que 'AereA' [[web oficial](http://aerea-game.com/)], el prometedor juego de rol y acción obra de "Triangle Studios" llegará a Linux/SteamOS en tan solo una hora (desde que escribimos estas líneas):



> 
> [#Surprise](https://twitter.com/hashtag/Surprise?src=hash)! [#AereA](https://twitter.com/hashtag/AereA?src=hash) will release in THREE HOURS as a timed [#exclusive](https://twitter.com/hashtag/exclusive?src=hash) on [#Steam](https://twitter.com/hashtag/Steam?src=hash)! Get it with 15% launch [#discount](https://twitter.com/hashtag/discount?src=hash)! <https://t.co/YpMvyD0aZG> [pic.twitter.com/Uk3Zuf3Lnt](https://t.co/Uk3Zuf3Lnt)
> 
> 
> — SOEDESCO (@SOEDESCO) [June 2, 2017](https://twitter.com/SOEDESCO/status/870596013085601792)



 


*'AereA' es un juego de rol y acción de temática musical en el que juegas como uno de los discípulos del Gran Maestro Guido y exploras Aezir, una isla flotante que se fracturó. Tu misión es encontrar y devolver los nueve instrumentos esenciales para restaurar el equilibrio y la paz al universo. Tendrás que explorar cada rincón de las islas dispersas, completar misiones, resolver puzles, derrotar a jefes y descubrir la verdad que encierran las islas.*


*Juega como Wolff el Arquero Arpista, Jacques el Caballero del Violonchelo, Jules el Mago Lutier, Claude el Bombardero Trompetero. También puedes formar un equipo con tus amigos para jugar en modo cooperativo local. Recoge partituras para aprender nuevas habilidades y adaptar tus armas a tu estilo de juego.*


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/BK-LFCdoSso" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


####  **Características:**


* *Disfruta de unos gráficos de temática musical maravillosamente dibujados a mano*
* *Completa la historia para averiguar qué ocurrió en la isla flotante*
* *Juega con 4 personajes distintos, cada uno con un conjunto exclusivo de habilidades*
* *Enfréntate a 9 jefes especiales, inspirados en instrumentos musicales específicos*
* *Explora varias islas con numerosos biomas, enemigos y puzles distintos*
* *Cambia en cualquier momento a modo cooperativo local para formar un equipo con hasta 4 amigos*


En cuanto a los requisitos para Linux, son los siguientes:


Mínimo


* SO: Ubuntu o Steam OS
* Procesador: Intel® Core™ 2 Duo 2,6Ghz / AMD Athlon™ 64 X2 3800+
* Memoria: 4 GB de RAM
* Gráficos: NVIDIA® GeForce® GTX 560 Ti / ATI® Radeon™ HD 7850 o superior
* Red: Conexión de banda ancha a Internet
* Almacenamiento: 4 GB de espacio disponible


Recomendado


* SO: Ubuntu o Steam OS
* Procesador: Intel® Core™ i5 4th Gen / AMD A10 series or better
* Memoria: 8 GB de RAM
* Gráficos: NVIDIA® GeForce® GTX 760 / ATI® Radeon™ R9 M295X o superior
* Red: Conexión de banda ancha a Internet
* Almacenamiento: 4 GB de espacio disponible


'AereA' estará disponible "ya mismo" en español en su página de Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/573660/" width="646"></iframe></div>


¿Qué te parece 'AereA'? ¿Te gustan los juegos de rol de este tipo?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 

