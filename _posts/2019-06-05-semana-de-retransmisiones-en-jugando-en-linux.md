---
author: Pato
category: Noticia
date: 2019-06-05 20:14:40
excerpt: "<p>Google Stadia, E3... se avecinan unos d\xEDas en los que los eventos\
  \ de videojuegos tendr\xE1n todo el protagonismo</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/7bc8cb8699a452430e95a37b47c70175.webp
joomla_id: 1056
joomla_url: semana-de-retransmisiones-en-jugando-en-linux
layout: post
title: Semana de retransmisiones en Jugando en Linux
---
Google Stadia, E3... se avecinan unos días en los que los eventos de videojuegos tendrán todo el protagonismo

Estamos a las puertas del E3, y como otros años en **Jugando en Linux** os queremos preguntar sobre vuestro interés en los eventos que allí se van a celebrar. A priori, los eventos que creemos mas interesantes son los siguientes, **comenzando desde mañana mismo** con el que ya os hemos anunciado de [Google Stadia](index.php/homepage/noticias/39-noticia/1179-google-desvelara-manana-los-detalles-de-su-plataforma-de-juego-en-la-nube-stadia) (horario de eventos según hora española (CEST+1)):


**Google Stadia** - 6 junio 2019 - 6:00 pm


**EA Play** - 8 junio 2019 - 6:15 pm


**Microsoft** - 9 junio 2019 - 10:00 pm


**Devolver Digital** - 10 junio 2019 - 4:00 am


**The First Ever E3 VR Showcase** - 10 junio 2019 - 6:00 pm


**PC Gaming Show - 10 junio 2019** - 7:00 pm


**Ubisoft - 10 junio 2019** - 10:00 pm


**Square Enix - 11 junio 2019** - 3:00 am


Como veis tenemos donde elegir. Nos gustaría verlas todas por cuanto aunque algunas de ellas son de compañías que pueden no tener ningún interés en traer sus juegos a Linux, es probable que algunos de los juegos que presenten podamos jugarlos en nuestro sistema favorito gracias a Wine/Proton, por lo que siempre es interesante estar al tanto de las novedades.


Como en otras ocasiones, y para que no os tengáis que buscar la vida buscando las retransmisiones, **en Jugando en Linux las podréis ver todas en nuestro canal de** [Twitch](https://www.twitch.tv/jugandoenlinux) donde nosotros también estaremos viéndolas (o al menos hasta que el sueño nos pueda) con lo que podréis comentar lo que vaya sucediendo en nuestros chats con nosotros y toda la comunidad de Jugando en Linux. (twitch cuando sea posible o Telegram, lo mas probable, aunque estamos abiertos a usar lo que prefiráis)


¿Qué os parecen las conferencias de este año? ¿Esperáis algún título en especial? ¿Donde preferís que comentemos las conferencias?


Cuéntamelo en los comentarios o en el canal de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux).

