---
author: P_Vader
category: "V\xEDdeos JEL"
date: 2022-01-07 19:06:42
excerpt: "<p><span class=\"style-scope yt-formatted-string\" dir=\"auto\">Vamos a\
  \ plantar una bandera de JugandoenLinux en la Muna.</span></p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/KerbalAnalisis/kerbal_space_program5.webp
joomla_id: 1407
joomla_url: video-nos-la-jugamos-en-kerbal
layout: post
tags:
- espacio
- simulador
- construccion
- youtube
- kerbal-space-program
title: "V\xCDDEO: Nos la jugamos en Kerbal"
---
Vamos a plantar una bandera de JugandoenLinux en la Muna.


<iframe width="780" height="440" src="https://www.youtube.com/embed/9y9OiLHzD74" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

