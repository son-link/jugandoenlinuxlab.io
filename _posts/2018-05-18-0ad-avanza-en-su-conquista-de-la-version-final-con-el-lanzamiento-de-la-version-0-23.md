---
author: Serjor
category: Estrategia
date: 2018-05-18 16:02:58
excerpt: "<p>Con el subt\xEDtulo de Ken Wood, en homenaje a uno de los dise\xF1adores\
  \ iniciales del juego, nos llega una nueva versi\xF3n</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/e6f7eb86455f00eeac074fd25095dcde.webp
joomla_id: 739
joomla_url: 0ad-avanza-en-su-conquista-de-la-version-final-con-el-lanzamiento-de-la-version-0-23
layout: post
tags:
- rts
- open-source
- 0ad
title: "0AD avanza en su conquista de la versi\xF3n final con el lanzamiento de la\
  \ versi\xF3n 0.23"
---
Con el subtítulo de Ken Wood, en homenaje a uno de los diseñadores iniciales del juego, nos llega una nueva versión

Hoy a la mañana nuestro compañero Fran nos despertaba a todos los miembros del canal de [Telegram](https://t.me/jugandoenlinux) con la siguiente noticia:







Y es que como podemos leer, hay nueva versión del conocido juego de estrategia en tiempo real 0ad. Como se puede ver en la noticia, esta nueva versión trae una serie de novedades muy interesantes:


* Una nueva civilización: Los Cusitas
* Un gestor de descargas de mods, el cuál hace uso de la nueva plataforma de código abierto <mod.io>, que viene a ser una especie de Steamworks pero sin cerrarse a un sistema.
* Nuevos modelos y texturas
* Condiciones de victoria combinadas (aquí soy sincero, como no juego al juego no sé cómo influye, pero podéis jugar con los diferentes parámetros con los que el juego dará por ganador a alguien)
* Visualización del rango de ataque al colocar edificios o estructuras
* Colores "diplomáticos", que viene a ser que ahora podemos ver si nos vamos a meter en terreno aliado (gris) o enemigo (rojo)
* Daño al ser destruido (o morir matando). Por ejemplo ahora un barco al hundirse puede generar algo de daño en las unidades enemigas cercanas.
* Cambios en la IA, la cuál ahora permite la configuración de comportamientos, como ataque o defensa.
* Autenticación en las partidas a través del lobby. Por lo visto antes se podía entrar en una partida impersonando a un jugador porque no había un mecanismo que asegurara que el jugador dentro del juego y en el lobby fueran el mismo.
* Nuevos mapas y mejoras en los ya existentes


De todos modos, aquí tenéis un vídeo con las novedades:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/SjDNRgVAdkc" width="560"></iframe></div>


Además, es posible que los integrantes del clan 0ad de <jugandoenlinux.com> nos hagan este domingo una vídeo (¡¡incluso un directo!!) donde nos comenten cosas de esta nueva versión.


Y a ti, ¿qué te parece esta nueva versión? En esta ocasión déjanos tus comentarios en el [foro del clan de 0ad](index.php/foro/clan-0ad-jel), y si tienes un clan o te gusta jugar a 0ad, ¡rétalos! (o únete a ellos).

