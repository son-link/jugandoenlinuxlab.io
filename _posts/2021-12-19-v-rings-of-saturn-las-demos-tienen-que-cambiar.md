---
author: P_Vader
category: "Exploraci\xF3n"
date: 2021-12-19 19:21:52
excerpt: "<p>El desarrollador independiente KoderaSoftware relata su experiencia con\
  \ la demo de su juego.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/RingsofSaturn/rings_of_saturn_thumb.webp
joomla_id: 1398
joomla_url: v-rings-of-saturn-las-demos-tienen-que-cambiar
layout: post
tags:
- indie
- espacio
- demo
- exploracion
title: "\u0394V: Rings of Saturn \"Las demos tienen que cambiar\""
---
El desarrollador independiente KoderaSoftware relata su experiencia con la demo de su juego.


Si no conocías ΔV: Rings of Saturn es un increíble juego hecho con Godot, Blender y Gimp entre otros, de KoderaSoftware, un indie de verdad como bien se define a si mismo: *"Estoy yo que hago los juegos y tú que los juegas, nadie más en medio"*.   
Es un juego espacial de ciencia ficción en 2D cuyo manejo hace uso de verdaderas físicas, ΔV = Delta-V que en astrodinámica es el esfuerzo que se necesita para alcanzar una órbita, a priori parece un juego con una dificultad considerable, pero está gustando mucho por su éxito desde que lanzó su acceso anticipado en [Steam](https://store.steampowered.com/app/846030/V_Rings_of_Saturn/) e [Itch](https://koder.itch.io/dv-rings-of-saturn).


 


Quizás hayas oído hablar también de KoderaSoftware por un interesante artículo que compartió en [reddit](https://www.reddit.com/r/gamedev/comments/qeqn3b/despite_having_just_58_sales_over_38_of_bug/) en el que explicaba que de las 12.000 copias que había vendido de su juego, 700 eran para linux, poco más de un 5%. **En cambio de los más de 1.000 fallos que se habían reportado del juego hasta la fecha, 400 eran de jugadores Linux**, osea un 650% más de reportes desde este sistema. Aunque no lo parezca de esos cuatrocientos reportes **tan sólo 3 eran específicos de Linux**. Por si no fuera poco **la calidad de los reportes comentaba que eran espectaculares:**



> 
> tener un equipo de control de calidad de cien personas a tu lado es un bien incalculable para un estudio indie [como el mio]
> 
> 
> 


De nuevo **su desarrollador principal nos regala un interesantísimo artículo en [indiedb](https://www.indiedb.com/games/v-rings-of-saturn/news/the-demo-of-my-game-used-to-have-almost-everything-in-it-but-now-im-changing-that) y en esta ocasión detalla la experiencia de su demo** desde que lanzó su juego.


Personalmente las demos siempre me han parecido un gran recurso para ayudar a la venta y difusión de un juego y nunca he entendido la reticencia para acompañar un juego de una buena demo, al igual que se suele acompañar de un buen trailer. Algunos de los juegos que he presentado en esta web últimamente han sido gracias a demos como las que se presentan en los Festivales de Steam. Vamos a desgranar la experiencia de KoderaSoftware:


**El autor nos explica que su juego se financia con las ventas en acceso anticipado**. Es cierto que en general hay reticencias para comprar juegos en este estado, principalmente por la desconfianza que generan (productos con errores, inacabados, etc). Desde el primer momento que lanzó el juego anticipadamente quiso que fuese perfectamente jugable en todo momento. Para ello ideó un sistema de lanzamientos estables y completos. **Y que mejor manera de que confíes en su juego qué regalar una demo**.


![Rings of saturn screenshot](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/RingsofSaturn/Rings_of_saturn_screenshot.webp)


**La primera demo que lanzó venía bastante limitada**, con una misión, sin acceso a muchos recursos y con un tiempo limitado de 10 a 15 minutos. Técnicamente su demo siempre es una copia exacta del juego original con alguna limitación insertada en el código, con lo cual siempre se está probando el producto original.


Unos **meses después por error introdujo un fallo que hacía que las limitaciones de la demo no se activaran**, con lo cual podías jugar la misión de la demo por tiempo ilimitado. Durante meses no se dio cuenta de este problema por una sencilla razón, **la ausencia de limitación no impactó para nada en las ventas de su juego.**


 Este hecho le hizo recapacitar y **decidió arriesgar a lanzar la demo sin ningún tipo de restricción, exactamente igual que el juego original pero con un ligero recorte** y es que **no puedas cargar partidas guardadas**. En ese momento le pareció un trato justo, puedas probar todo lo que quieras su juego y si en algún momento quieres cargar tu partida quiere decir que te gusta lo suficientemente el juego como para comprarlo.


**Pero no, estaba equivocado, esta no era una buena experiencia para un jugador que lo prueba por primera vez, sobre todo por que no es un juego fácil** y es probable que se necesite cargar partidas guardadas. **Así que una vez mas, va arriesgar a cambiar la demo y permitir que se carguen partidas**, aunque en esta ocasión la limitación será que caducan a los 30 días.


**¿Os dais cuenta que prácticamente tienes el juego original por treinta días? ¿y como afecto todo esta trayectoria a las ventas?**


![Rings of saturn grafica ventas](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/RingsofSaturn/Rings_of_saturn_grafica_ventas.webp)  
La gráfica es bastante clara, puedes ver las descargas de la demostración junto a las venta. **Y la conclusión general es que quien jugó la demo terminó comprando el juego**. Incluso la eliminación de las restricciones en la demo en mi opinión pudo ayudar con las ventas del juego**.**


 


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/tD882MP7Pjg" title="YouTube video player" width="780"></iframe></div>


No puedo estar mas de acuerdo con este magnifico desarrollador ¿qué os parece a vosotros? Cuéntanoslo en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).


 

