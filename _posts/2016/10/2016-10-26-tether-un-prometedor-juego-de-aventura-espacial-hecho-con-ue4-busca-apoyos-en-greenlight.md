---
author: Pato
category: Terror
date: 2016-10-26 18:43:10
excerpt: "<p>Los desarrolladores han levantado cierta pol\xE9mica con su campa\xF1\
  a. El juego promete versi\xF3n en Linux.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/954fb0ebf1d84fb921bfb0b6e045d57f.webp
joomla_id: 76
joomla_url: tether-un-prometedor-juego-de-aventura-espacial-hecho-con-ue4-busca-apoyos-en-greenlight
layout: post
tags:
- indie
- aventura
- steam
- terror
title: '''Tether'' un prometedor juego de aventura espacial hecho con UE4 busca apoyos
  en Greenlight'
---
Los desarrolladores han levantado cierta polémica con su campaña. El juego promete versión en Linux.

Tether se define como un juego de aventura para un jugador basado en un mundo de ciencia ficción devastado. La Tierra está muriendo debido al colapso de varios continentes tras la destrucción de la Luna y una federación emprende la labor de terraformar el planeta mas cercano que pueda ser habitable: Marte.


Con esta premisa juegas como Lesleigh Hayes, una bióloga a bordo de la nave Sonne camino de Marte. La nave tendrá un accidente y Lesleigh tendrá que enfrentarse a sus terrores psicológicos enmedio del espacio profundo.


Varios usuarios se han quejado de que supuestamente la campaña del juego incumple las condiciones de Greenlight ya que el vídeo no muestra el juego como tal. Juzgad vosotros mismos:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" seamless="seamless" src="https://www.youtube.com/embed/BK9-khsd-GE" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


Según los desarrolladores ya han testeado alguna versión del juego en Linux sin problemas.


¿Apoyarás la campaña de este juego? Cuéntamelo en los comentarios.


Puedes ver la campaña en su [web de Greenlight](http://steamcommunity.com/sharedfiles/filedetails/?id=785421343).

