---
author: Pato
category: Estrategia
date: 2016-10-25 18:21:53
excerpt: <p>Se trata de un juego de estrategia y combate con cartas con una pinta
  excelente.</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/0548677e6432786dd8df61eb3aaec139.webp
joomla_id: 75
joomla_url: siege-un-juego-de-estrategia-con-cartas-ya-disponible-en-linux
layout: post
tags:
- indie
- steam
- estrategia
- juego-de-cartas
title: '''Siege'' un juego de estrategia con cartas ya disponible en Linux'
---
Se trata de un juego de estrategia y combate con cartas con una pinta excelente.

Nunca he sido un seguidor de los juegos de cartas, pero he de reconocer que este 'Siege' ha llamado mi atención.


El concepto de juego de combate estratégico con cartas desde luego es llamativo. Juzga por tí mismo:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" seamless="seamless" src="https://www.youtube.com/embed/BeWAz3_1UMI" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


Siege es un juego para un jugador, y no está disponible en español. Si esto no es un problema para ti, lo tienes disponible en Steam:


<div class="steam-iframe"><iframe height="190" seamless="seamless" src="https://store.steampowered.com/widget/427050/" width="646"></iframe></div>

