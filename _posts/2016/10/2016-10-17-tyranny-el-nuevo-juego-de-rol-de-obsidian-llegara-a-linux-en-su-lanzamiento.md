---
author: Pato
category: Rol
date: 2016-10-17 20:36:18
excerpt: "<p>El pr\xF3ximo d\xEDa 10 de Noviembre experimentaremos el mundo en el\
  \ que el diablo venci\xF3.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/6f43b5263fbba79c5962514b85d34738.webp
joomla_id: 59
joomla_url: tyranny-el-nuevo-juego-de-rol-de-obsidian-llegara-a-linux-en-su-lanzamiento
layout: post
tags:
- proximamente
- rol
- steam
title: "Tyranny, el nuevo juego de Rol de Obsidian llegar\xE1 a Linux en su lanzamiento"
---
El próximo día 10 de Noviembre experimentaremos el mundo en el que el diablo venció.

Los fans de los buenos juegos de Rol a la antígua usanza  están de enhorabuena. Obsidian, los padres de "Pillars of Eternity" nos traen este nuevo juego en el que tendremos que enfrentarnos a un mundo en el el diablo ha ganado y el jugador tiene que pacificar los territorios conquistados. Los diarios de desarrollo y vídeos enfatizan en que las elecciones pueden tener unas consecuencias a largo plazo en el mundo del juego con lo que la extensión y la rejugabilidad prometen ser interesantes. La configuración y el lore también parecen bastante interesantes. 


Paradox y Obsidian han publicado un vídeo con contenido del juego:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" seamless="seamless" src="https://www.youtube.com/embed/0LGj3TpEBZ8" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


El título tendrá versión Linux de salida, tal y como confirmaron los desarolladores a Liam, de GOL:



> 
> [@gamingonlinux](https://twitter.com/gamingonlinux) [@PdxInteractive](https://twitter.com/PdxInteractive) [@YouTube](https://twitter.com/YouTube) The lord Kyros wouldn't have it any other way!
> 
> 
> — Tyranny (@TyrannyGame) [13 de octubre de 2016](https://twitter.com/TyrannyGame/status/786608520691081216)







Puedes hacer precompra en la [[web de Paradox](https://www.paradoxplaza.com/games?franchise=160)], en [[GOG](https://www.gog.com/game/tyranny_commander_edition_preorder)] o en [[Steam](http://store.steampowered.com/app/362960/)] en sus distintas opciones. El día 10 de Noviembre, ¿vas a jugar a Tyranny?


Cuéntamelo en los comentarios.


 

