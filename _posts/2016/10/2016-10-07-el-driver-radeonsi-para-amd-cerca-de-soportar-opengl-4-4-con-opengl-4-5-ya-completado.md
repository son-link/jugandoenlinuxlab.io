---
author: Pato
category: Software
date: 2016-10-07 14:30:14
excerpt: "<p>El driver open source Radeonsi para AMD est\xE1 muy cerca de tener soporte\
  \ para OpenGL 4.4 con OpenGL ya completado.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/233826a67be66a810b23a263230da62e.webp
joomla_id: 42
joomla_url: el-driver-radeonsi-para-amd-cerca-de-soportar-opengl-4-4-con-opengl-4-5-ya-completado
layout: post
tags:
- software
- drivers
- controladores
title: El driver Radeonsi para AMD cerca de soportar OpenGL 4.4, con OpenGL 4.5 ya
  completado
---
El driver open source Radeonsi para AMD está muy cerca de tener soporte para OpenGL 4.4 con OpenGL ya completado.

El avance de los drivers ·Open Source" ha experimentado un crecimiento absolutamente fantástico en el último año o año y medio. Tanto es así que prácticamente tienen terminado el driver para AMD de OpenGL. Tan solo les quedan unos pocos elemenos para tener terminadas las especificaciones para OpenGL 4.4, con OpenGL 4.5 ya terminado.


Puedes ver el avance de los drivers en la web: <https://mesamatrix.net/>


Es un gran avance, ya que hasta ahora no hemos tenido alternativa de calidad en cuanto a rendimiento y soporte por parte de AMD en el apartado gráfico para Linux. Esperemos que con las expecificaciones completadas los desarrolladores puedan centrarse en la optimización y podamos tener competencia en el ámbito de los gráficos, cosa que siempre nos conviene y beneficia a nosotros los usuarios.


Además sería un avance más que positivo el tener unos drivers "Open source" a la altura para que tengamos alternativa en AMD y software libre.


¿Que piensas al respecto? cuéntamelo en los comentarios.

