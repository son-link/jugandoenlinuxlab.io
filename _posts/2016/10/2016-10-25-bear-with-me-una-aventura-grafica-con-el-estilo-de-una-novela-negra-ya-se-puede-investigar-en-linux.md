---
author: Pato
category: Aventuras
date: 2016-10-25 17:59:27
excerpt: <p>Si te van los juegos de intriga con un toque 'noir' este juego te va a
  gustar.</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/d3b3799d6611d677944f5f86a500beb3.webp
joomla_id: 74
joomla_url: bear-with-me-una-aventura-grafica-con-el-estilo-de-una-novela-negra-ya-se-puede-investigar-en-linux
layout: post
tags:
- indie
- aventura
- steam
title: "'Bear With Me' una aventura gr\xE1fica con el estilo de una novela negra ya\
  \ se puede investigar en Linux"
---
Si te van los juegos de intriga con un toque 'noir' este juego te va a gustar.

'Bear with Me' es una aventura gráfica por episodios con un toque de novela negra según define Exordium Games, creadores de este juego donde tendrás que ayudar a la protagonista a encontrar a su hermano perdido. 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" seamless="seamless" src="https://www.youtube.com/embed/mUe3SovOyTE" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


Sinopsis del juego:


Abrumada por sus pesadillas, Amber se despierta en mitad de la noche para descubrir que su hermano Flint ha desaparecido. Decide entonces acudir a su osito de confianza, Ted E. Bear, un viejo detective gruñón ya jubilado.   
La pintoresca pareja se lanza en busca de pistas e interroga a todos los posibles testigos y sospechosos, inconscientes de los peligros que desde hace poco perturban la paz de Ciudad Papel.  
La trama se complica cuando entra en escena el misterioso "hombre de rojo", sospechoso de varios incendios en Ciudad Papel y que además va tras la pista de la pequeña Amber.


'Bear with Me' está localizado al español (subtítulos) y puedes encontrarlo en Steam:


<div class="steam-iframe"><iframe height="190" seamless="seamless" src="https://store.steampowered.com/widget/492630/" width="646"></iframe></div>

