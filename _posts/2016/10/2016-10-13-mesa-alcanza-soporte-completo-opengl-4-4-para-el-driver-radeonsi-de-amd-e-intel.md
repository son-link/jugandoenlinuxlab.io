---
author: Pato
category: Software
date: 2016-10-13 18:16:57
excerpt: "<p>Hace tan solo unos d\xEDas se anunciaba estar cerca de lograrlo.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/f710044bf79a4b1f5d8b085e5e5d9711.webp
joomla_id: 52
joomla_url: mesa-alcanza-soporte-completo-opengl-4-4-para-el-driver-radeonsi-de-amd-e-intel
layout: post
tags:
- software
- drivers
title: Mesa alcanza soporte completo OpenGL 4.4 para el driver Radeonsi de AMD e Intel
---
Hace tan solo unos días se anunciaba estar cerca de lograrlo.

Tal y como [nos hicimos eco](index.php/item/24-el-driver-radeonsi-para-amd-cerca-de-soportar-opengl-4-4-con-opengl-4-5-ya-completado) en jugnadoenlinux.com, Mesa ha alcanzado el soporte para OpenGL 4.4, con OpenGL 4.5 ya terminado para los drivers de AMD e Intel (i965/gen8+).


Ahora solo falta pasar los test de certificación por parte de Kronos. Sin embargo, este proceso puede llevar algo de tiempo pues como puede verse en la [web](https://www.khronos.org/adopters) de "Adopters" de Kronos, obtener la certificación cuesta un dinero.


Mesa seguirá mostrando como último soporte OpenGL 4.3 hasta que Mesa (o alguien en su lugar) pague el dinero necesario para la certificación.


Puedes ver el avance de los drivers Mesa en la web: <https://mesamatrix.net/>


Fuente: [gamingonlinux](https://www.gamingonlinux.com/articles/mesa-has-now-hit-full-opengl-44-support-for-amd-radeonsi-and-intel.8321)

