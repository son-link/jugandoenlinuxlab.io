---
author: Pato
category: "Acci\xF3n"
date: 2016-10-05 18:04:58
excerpt: <p>El juego de Mind Grown Software pide apoyos en Steam para lanzar el juego.&nbsp;</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/f4b6dca0e2911082f0eb6e1df1a0e11d.webp
joomla_id: 37
joomla_url: convicted-galaxy
layout: post
tags:
- accion
- espacio
- greenlight
- 2-5d
title: "Convicted Galaxy, un interesante juego espacial en 2.5D est\xE1 en Greenlight"
---
El juego de Mind Grown Software pide apoyos en Steam para lanzar el juego. 

Convicted Galaxy es un interesante juego en 2.5D y perspectiva en tercera persona con elementos de acción, exploración y disparos que tiene un llamativo apartado visual. El juego está recabando apoyos en su (iniciada el día 8 de septiembre) campaña en Greenlight para lanzar el juego de forma simultánea en Windows, Mac y Linux. De hecho están en fase de beta cerrada que a tenor de las [informaciónes](https://www.gamingonlinux.com/articles/convicted-galaxy-a-procedurally-generated-space-exploration-game-needs-votes-on-steam-greenlight.8148?utm_content=buffer7c01d&utm_medium=social&utm_source=twitter.com&utm_campaign=buffer) de quien la ha probado en Linux tiene buena pinta.Juzgar vosotros mismos:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" seamless="seamless" src="https://www.youtube.com/embed/EMa6kky4_90" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


Podéis apoyar la campaña [en este enlace](http://steamcommunity.com/sharedfiles/filedetails/?id=760069811) y toda la información del proyecto [en este otro](http://www.convictedgalaxy.com/).

