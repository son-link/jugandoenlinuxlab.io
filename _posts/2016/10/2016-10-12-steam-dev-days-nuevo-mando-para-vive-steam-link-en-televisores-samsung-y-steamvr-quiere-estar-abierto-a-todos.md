---
author: Pato
category: Hardware
date: 2016-10-12 21:16:53
excerpt: "<p>M\xE1s informaci\xF3n desde la convenci\xF3n de Valve en Seattle.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/fc34f61d23b74be53ee07d469bd32064.webp
joomla_id: 47
joomla_url: steam-dev-days-nuevo-mando-para-vive-steam-link-en-televisores-samsung-y-steamvr-quiere-estar-abierto-a-todos
layout: post
tags:
- steam
- realidad-virtual
- steamvr
title: 'Steam Dev Days: Nuevo mando para Vive, Steam Link en Televisores Samsung y
  SteamVR quiere estar abierto a todos'
---
Más información desde la convención de Valve en Seattle.

Todo parece indicar que Valve está apostando muy fuerte por la Realidad virtual.


Llegan informaciones de que estarían trabajando en un mando renovado para utilizar en entornos vituales. Gracias a un [tweet](https://twitter.com/downtohoerth/status/786264416392130560) de uno de los asistentes podemos ver una imagen significativa:


![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Steamdevdays/newvrcontroler.webp)


Por otra parte, en una de las conferencias han dicho que hay una media de unos 1000 nuevos usuarios de Realidad Virtual diarios en Steam, lo que suponen unos 360.000 usuarios nuevos al año. No es que sea una cifra muy elevada pero al menos muestra la posible base de usuarios que puede haber en estos momentos.


Otra de las [noticias](https://twitter.com/Steam_Spy/status/786264720063856640) que han surgido es que Valve ha invertido una significativa cantidad de dinero en una compañía especializada en tecnología de transferencia de datos inalámbrica a 60 GHz, lo que ssugiere que estarían pensando en eliminar los cables de los cascos de realidad virtual.


Por otra parte, a diferencia de lo que hace Facebook con Oculus Valve quiere hacer SteamVR lo mas abierto posible. Para empezar va a licenciar su tecnología de posicionamiento para intentar hacerla tan ubicua como la Wifi. También abrirá OpenVR API/SDK para que esté disponible no solo en Steam si no en cualquier otro entorno, y por último Valve está invirtiendo en componentes estandar para la RV, de modo que todo el ecosistema pueda realizar prototipos. (Gracias a [Marty Caplan](https://twitter.com/d33vle/) por la info)


Suena interesante. ¿Verdad?


Por último, Se ha anunciado que Samsung dará soporte a la tecnología de Steam Link en sus próximas series de televisores, por lo que Valve se anota un punto a favor ya que no será necesario adquirir ningún aparato adicional para hacer "streaming" desde el PC a la televisión. Configurar, conectar el Steam Controller y listo! Esperemos que otras marcas se sumen a la iniciativa e incluyan esta característica por defecto.


Ah!, y para aquellos escépticos de que Valve no siga apoyando a SteamOS y las Steam Machines, en la convención tienen un departamento con varias de ellas a disposición de los asistentes para que las puedan probar...


![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Steamdevdays/steam-machines.webp)


 


Seguimos informando...

