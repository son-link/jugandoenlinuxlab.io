---
author: Pato
category: "Simulaci\xF3n"
date: 2016-10-10 21:53:06
excerpt: <p>Laminar Research ha anunciado el desarrollo de este nuevo simulador dentro
  de su familia X-Plane con grandes novedades.</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/787ae9ec9023a82f5aa7e4c1a64f73cb.webp
joomla_id: 45
joomla_url: anunciado-x-plane-11-el-simulador-aereo-saldra-este-mismo-ano-con-version-linux-confirmada
layout: post
tags:
- proximamente
- simulador
- aviones
title: "Anunciado X-Plane 11, el simulador de aviaci\xF3n saldr\xE1 este mismo a\xF1\
  o con versi\xF3n Linux confirmada"
---
Laminar Research ha anunciado el desarrollo de este nuevo simulador dentro de su familia X-Plane con grandes novedades.

Cualquier juego triple A que se lanza en Linux es una buena noticia, más si cabe si es de un género tan exclusivo y minoritario como es la Simulación de aviación donde tan solo un par de títulos o tres a lo sumo copan todo el mercado.


Siendo así, que Laminar Research siga apoyando a Linux como una de las plataformas soportadas es digno de mención. Se trata esta vez de X-Plane 11 [[página oficial](http://www.x-plane.com/)], su nuevo simulador de aviación que nos trae novedades como una nueva e intuitiva interfaz de usuario, cabinas clickeables en 3D, modelos exteriores de alta definición para todos los aviones incluidos en el título, un nuevo motor de efectos de luz, sonidos y explosiones, aviónica realista con todos los aparatos preparados para IFR de salida, aeropuertos llenos de vida con camiones cisterna, nuevos edificios y calles para simular mejor las ciudades europeas, y mucho mas.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" seamless="seamless" src="https://www.youtube.com/embed/4bpSR-AKncQ" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


Ha sido la propia compañía respondiendo a un usuario en [Facebook](https://www.facebook.com/XPlaneOfficial/posts/10153831751361283?comment_id=10153831886576283&comment_tracking=%7B%22tn%22%3A%22R9%22%7D) quien ha confirmado el desarrollo para Linux, y su lanzamiento en Steam.


En las próximas semanas irán publicando más detalles de este nuevo simulador en su [página web](http://www.x-plane.com/2016/10/x-plane-11-coming-holiday-season/). El lanzamiento se espera para fechas navideñas, según el comunicado.


¿Eres fan de los simuladores? ¿Piensas comprar Xplane-11?


Cuéntamelo en los comentarios.

