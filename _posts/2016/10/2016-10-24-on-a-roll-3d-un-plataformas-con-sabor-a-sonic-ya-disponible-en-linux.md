---
author: Pato
category: Plataformas
date: 2016-10-24 20:42:09
excerpt: <p>Se trata de un juego en 2.5D que recuerda bastante a los niveles de Sonic.</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/90701d02ae3da0e5a21abbd900c25748.webp
joomla_id: 71
joomla_url: on-a-roll-3d-un-plataformas-con-sabor-a-sonic-ya-disponible-en-linux
layout: post
tags:
- plataformas
- steam
title: '''On a Roll 3D'': un plataformas con "sabor" a Sonic ya disponible en Linux'
---
Se trata de un juego en 2.5D que recuerda bastante a los niveles de Sonic.

On a Roll 3D es un juego de plataformas que recuerda por momentos a Sonic por su paleta de colores y sus niveles. El juego se asemeja mucho al del erizo azul, salvo que esta vez la protagonista será una pelota que tendrás que manejar para poder llegar al final del recorrido esquivando los obstáculos.


Su desarrollo no es tan rápido como el de Sonic, pero igualmente es entretenido. Durante los 24 niveles y 6 temas que consta el juego tendrás que sortear obstáculos como minas, púas, láseres, sierras, fuego etc. y tratar de llegar al final de cada nivel de una pieza en el menor tiempo posible.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" seamless="seamless" src="https://www.youtube.com/embed/j3UvanXlX4E" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


No es un juego muy vistoso, pero para jugar los más pequeños de la casa puede estar bien.


Puedes encontrar On a Roll 3D en Steam:


<div class="steam-iframe"><iframe height="190" seamless="seamless" src="https://store.steampowered.com/widget/341090/" width="646"></iframe></div>

