---
author: Pato
category: Aventuras
date: 2016-10-05 18:12:42
excerpt: "<p>Es un juego gratuito con gr\xE1ficos hechos a mano.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/c889234799e865bbe90cee71f6cd2e53.webp
joomla_id: 38
joomla_url: disturbed-un-juego-de-horror-aventuras-y-puzzles-ya-disponible-en-linux-steamos
layout: post
tags:
- aventura
- horror
- puzzles
title: 'Disturbed: un juego de horror, aventuras y puzzles ya disponible en Linux/SteamOS'
---
Es un juego gratuito con gráficos hechos a mano.

[D](http://jupiterhell.com)isturbed es un juego gratuito de aventura, puzzles y horror inspirado por experiencias de depresión y adicción. Sus gráficos hechos a mano están concebidos para incrementar la sensación de horror del juego.


Acerca del juego:


"Eres un granjero que intenta llevar adelante una granja arruinada. Llegas a un punto en que no hay esperanza y debes hacer algo. Te das cuenta que morirás y es cuestión de como y donde.  ¿Que diferencia hay entre intentar algo o no? ¿que puede hacer un simple granjero? Solo tu puedes contestar a estas preguntas."


El juego no está disponible en español, pero es gratis y tiene valoraciones positivas. ¿Le darás una oportunidad? Podéis encontrarlo en su [página de Steam](http://store.steampowered.com/app/529780)

