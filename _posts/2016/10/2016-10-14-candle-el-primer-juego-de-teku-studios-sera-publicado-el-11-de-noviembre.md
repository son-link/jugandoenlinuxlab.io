---
author: Pato
category: Aventuras
date: 2016-10-14 11:09:34
excerpt: "<p>Se trata de un juego de aventuras muy particular de este estudio \xED\
  ndie espa\xF1ol. Estar\xE1 en Linux/SteamOS en su lanzamiento.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/0b1ad7a7b79268a1f4558db78e092446.webp
joomla_id: 53
joomla_url: candle-el-primer-juego-de-teku-studios-sera-publicado-el-11-de-noviembre
layout: post
tags:
- indie
- aventura
- proximamente
title: "Candle, el primer juego de Teku Studios ser\xE1 publicado el 11 de noviembre"
---
Se trata de un juego de aventuras muy particular de este estudio índie español. Estará en Linux/SteamOS en su lanzamiento.

Candle es sin duda un juego muy particular. Se trata de una aventura gráfica pintada a mano con acuarelas, por lo que su aspecto es lo primero que llama la atención. Es como si estuvieras mirando un cuadro que cobra vida.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" seamless="seamless" src="https://www.youtube.com/embed/LgdhdGh6Luw" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


Teku Studios ha trabajado duro durante cuatro años durante los cuales llevaron a cabo con éxito una campaña en Kickstarter y otra en Greenlight. Candle es su primer juego y llegará a las tiendas digitales Humble Store, [[GOG](https://www.gog.com/game/candle)] y [[Steam](https://www.gog.com/game/candle)] el próximo 11 de noviembre, traducido al español y editado por Daedalic Entertainment.


Sinopsis:


Candle es una aventura con puzles desafiantes. Ponte en la piel de Teku, un joven novicio que emprende un peligroso periplo para rescatar al chamán de su tribu de la malvada tribu de los wakcha. Pero su camino está plagado de trampas siniestras y obstáculos difíciles de sortear. Para superar estos retos, has de ser sagaz y tener conciencia de tu entorno; de lo contrario, el siguiente paso podría ser el último.


Sin embargo, Teku tiene un don especial: su mano izquierda es una vela. Deja que sea una baliza resplandeciente que ahuyente a tus enemigos o que arroje luz sobre lugares oscuros.


Los gráficos pintados a mano con acuarelas otorgan a Candle un toque especial, pues todos los fondos y personajes se han dibujado detenidamente para luego ser escaneados imagen a imagen. El juego parece un cuadro en constante movimiento.


¿Que os parece la propuesta de Candle? cuéntamelo en los comentarios

