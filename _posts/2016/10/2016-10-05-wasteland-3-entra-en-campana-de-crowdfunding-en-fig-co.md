---
author: Pato
category: Rol
date: 2016-10-05 17:57:28
excerpt: "<p>InXile entertainment lleva recogidos mas de 1.800.000 $ en el momento\
  \ de escribir estas l\xEDneas y en pocas horas alcanzar\xE1 su objetivo. Est\xE1\
  \ asegurado el desarrollo para Linux en la campa\xF1a.</p>\r\n<p>&nbsp;</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/4965657af186b9092c7a96976ffe881c.webp
joomla_id: 36
joomla_url: wasteland-3-entra-en-campana-de-crowdfunding-en-fig-co
layout: post
tags:
- proximamente
- rol
- financiacion-colectiva
title: "Wasteland 3 entra en campa\xF1a de financiaci\xF3n colectiva en fig.co"
---
InXile entertainment lleva recogidos mas de 1.800.000 $ en el momento de escribir estas líneas y en pocas horas alcanzará su objetivo. Está asegurado el desarrollo para Linux en la campaña.


 

Wasteland 3 será el nuevo título del estudio que inició su andadura con Torment Tides of Numerena y Wasteland 2. Ahora han iniciado una campaña de financiación colectiva con el objetivo de conseguir 2.750.000$ para hacer realidad su nuevo proyecto.


Wasteland 3 se apoyará en la base de Wasteland 2 para traernos un juego de Rol basado en partidas por turnos y un complejo sistema de combate estratégico. Además podrás jugar solo o con un amigo en su modo mutijugador síncrono o asíncrono.


Tendrá nuevas características de entorno, vehículos, sistemas de combate y diálogos, y se desarrollará en el nuevo mapa de las tierras salvajes del congelado Colorado.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" seamless="seamless" src="https://www.youtube.com/embed/xxYUgRdp0EE" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


Según afirman los desarrolladores en la propia campaña, el lanzamiento del título será simultáneo en windows Mac y Linux.


¿Que piensas de este proyecto? ¿apoyarás la campaña de financiación?... cuéntamelo en los comentarios.


Tienes toda la información del título y la campaña en:


<https://www.fig.co/campaigns/wasteland-3>

