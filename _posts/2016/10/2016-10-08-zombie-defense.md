---
author: Serjor
category: "Acci\xF3n"
date: 2016-10-08 19:30:30
excerpt: "<p>Actualmente, 7 mil millones de humanos viven en nuestro planeta. En el\
  \ pasado, m\xE1s de 100 mil millones vivieron y murieron en \xE9l. \xBFQu\xE9 pasar\xED\
  a si una d\xE9cima parte de ellos volvieran... a por nosotros?</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/af2ef6a0e2c9c528b09655df79f3b312.webp
joomla_id: 43
joomla_url: zombie-defense
layout: post
tags:
- zombie-defense
- tower-defense
- free-to-play
title: Zombie Defense
---
Actualmente, 7 mil millones de humanos viven en nuestro planeta. En el pasado, más de 100 mil millones vivieron y murieron en él. ¿Qué pasaría si una décima parte de ellos volvieran... a por nosotros?

El pasado 30 de septiembre de 2016 fue publicado en steam Zombie Defense, una mezcla de tower defense con estrategia en tiempo real desarrollado por los chicos de Hot Net Games, donde la mecánica del juego se basa en ir añadiendo unidades sobre el terreno, las cuales se pueden reposicionar, algo que le da un toque de dinamismo no habitual en los tower defense, y que podremos ir mejorando según avanza la partida, aportando ese pequeño toque de estrategia en tiempo real.


El rendimiento del juego es bastante bueno, pero en parte esto es debido a que fue lanzado inicialmente en plataformas móviles y ha dado el salto al PC, y eso se nota en la calidad gráfica del título. No obstante si el aspecto adiovisual no te importa, es un juego que te ayudará a pasar esos tiempos muertos si este género es lo tuyo.


 Os dejamos con la primera pantalla del juego:


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="405" seamless="seamless" src="https://www.youtube.com/embed/PrLaDIDIw28" width="720"></iframe></div>


 


Sinopsis de steam:



> 
> Un híbrido moderno entre el clásico juego de estrategia en tiempo real (RTS) y el moderno tower defense.  
>  Recluta soldados, sitúalos en los círculos y deja que disparen por sí solos sobre las oleadas de zombis.  
>  Mueve a los soldados de un círculo a otro, actualiza círculos y soldados, haz pedidos de suministros,   
>  lanza granadas y cócteles molotov, ordena ataques aéreos y maneja gigantescos bulldozers.  
>  Una mezcla perfecta de estrategia, táctica y microgestión.  
>    
>  Emplea el dinero que ganes en las misiones para comprar tecnologías permanentes, mejoras, armas, etc.  
>    
>  Características  
>  - 49 misiones.  
>  - 18 clases de reclutas.  
>  - 150 actualizaciones permanentes entre cada misión.  
>  - 6 clases básicas de enemigos, desde criaturas que caminan torpemente y horrores reptantes, hasta rápidos depredadores.  
>  - Gigantescos jefes zombis mutantes que dan más acción al juego.  
>  - Todo un armamento: desde el .38 de toda confianza a las letales escopetas, pasando por los súper precisos rifles de francotirador.  
>  - Potentes explosivos para convertir a los zombis en gelatina rosa.  
>  - Cócteles molotov para detener a los zombis mediante un infierno abrasador.  
>  - Vehículos pesados para aplastar a cualquier zombi que se interponga en su camino.  
>  - Torretas defensivas activadas por láser.   
>  - Devastadores ataque aéreos.  
>  - Envíos de provisiones.  
>  - Actualizaciones de soldados en tiempo real, reclutamientos y ventas.  
>  - Un modo infinito para jugadores duros que ansían emociones sinfín.  
>  - Especiales modos desafío.  
>  - Su gran rejugabilidad te permite completar cada misión y regresar a ella cuando quieras para obtener más dinero y aumentar la puntuación.  
>  - Entorno 3D, autopistas, ciudades y desiertos.  
>  - Actualizaciones gratuitas de manera regular con más misiones y mejoras.
> 
> 
> 


 


<div class="steam-iframe"><iframe height="190" seamless="seamless" src="https://store.steampowered.com/widget/533780/" width="646"></iframe></div>

