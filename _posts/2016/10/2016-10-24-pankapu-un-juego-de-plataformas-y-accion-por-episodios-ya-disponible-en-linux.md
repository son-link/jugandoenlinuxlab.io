---
author: Pato
category: Plataformas
date: 2016-10-24 21:14:59
excerpt: "<p>Ya est\xE1n disponibles los episodios 1 y 2 del juego.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/a27a3b73d355048c6bab885897085f62.webp
joomla_id: 72
joomla_url: pankapu-un-juego-de-plataformas-y-accion-por-episodios-ya-disponible-en-linux
layout: post
tags:
- accion
- plataformas
- steam
title: "'Pankapu' un juego de Plataformas y acci\xF3n por episodios, ya disponible\
  \ en Linux"
---
Ya están disponibles los episodios 1 y 2 del juego.

Pankapu es un llamativo juego de plataformas y acción que se va publicando por episodios y en el que la narrativa tiene un gran peso. El juego se desarrolla en dos historias paralelas entre sueño y realidad y podrás elegir entre tres "égidas" o clases de personaje en tiempo real.


Sinopsis del juego:


Pankapu es un juego de acción de plataformas narrativo y episódico que se desarrolla en los sueños de Djaha’rell, un niño afligido por un trágico incidente. Juegas con Pankapu, el pequeño guerrero elegido por el dios de los sueños para liberar a su mundo de las pesadillas que lo atenazan.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" seamless="seamless" src="https://www.youtube.com/embed/u5ESEoYLS8o" width="640"></iframe></div>


 


Puedes encontrar Pankapu en Steam:


<div class="steam-iframe"><iframe height="190" seamless="seamless" src="https://store.steampowered.com/widget/418670/84785/" width="646"></iframe></div>

