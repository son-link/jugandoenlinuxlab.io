---
author: Pato
category: "Acci\xF3n"
date: 2016-10-21 09:43:45
excerpt: "<p>Hoy lo han anunciado. Feral no nos da descanso en cuanto a lanzamientos.\
  \ \xBFLe habr\xE1n cogido vicio a esto?.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/a522a6005d1cb428ea34ef1769cd7452.webp
joomla_id: 68
joomla_url: deux-ex-mankind-divided-llegara-a-steamos-y-linux-el-3-de-noviembre
layout: post
tags:
- accion
- steamos
- proximamente
- feral-interactive
title: "Deux Ex Mankind Divided llegar\xE1 a SteamOS y Linux el 3 de Noviembre"
---
Hoy lo han anunciado. Feral no nos da descanso en cuanto a lanzamientos. ¿Le habrán cogido vicio a esto?.

Apenas acabamos de recibir Mad Max en Linux tras haber lanzado Dawn of War y Feral Interactive nos trae otro triple A. Esta vez es Deus Ex Mankind Divided el que llegará a SteamOS y Linux el próximo 3 de Noviembre.



> 
> On November 3, become Human 2.0 as Deus Ex: Mankind Divided arrives on Linux. [pic.twitter.com/clhYuYejVW](https://t.co/clhYuYejVW)
> 
> 
> — Feral Interactive (@feralgames) [21 de octubre de 2016](https://twitter.com/feralgames/status/789392785073770496)







Desde luego Feral está haciendo un esfuerzo extraordinaro portando mas y mas juegos a Linux/SteamOS. Tanto es así que probáblemente este 2016 sea el mejor año de la historia en cuanto a lanzamientos de juegos en Linux.


#### Sinopsis del juego:


Conviértete el 3 noviembre en un Humano 2.0 con *Deus Ex: Mankind Divided* para Linux.


Elige entre estrategias de combate, sigilo, sociales o de pirateo para lograr tus objetivos siendo Adam Jensen, un agente encubierto aumentado. Tus elecciones abrirán puertas, generarán nuevos desafíos y forjarán el destino de la humanidad.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" seamless="seamless" src="https://www.youtube.com/embed/7LG4AQVeWGw" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


Puedes visitar ya la [Miniweb del juego](http://www.feralinteractive.com/es/games/deusexmd/) en la web de Feral.

