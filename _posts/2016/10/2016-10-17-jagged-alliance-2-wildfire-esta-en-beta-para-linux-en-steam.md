---
author: Pato
category: Estrategia
date: 2016-10-17 17:13:10
excerpt: "<p>Topware Interactive continua trayendo sus juegos ant\xEDguos a Linux\
  \ gracias a Wine.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/39eee751af30032eeece2f48de2de4ba.webp
joomla_id: 55
joomla_url: jagged-alliance-2-wildfire-esta-en-beta-para-linux-en-steam
layout: post
tags:
- rol
- steam
- estrategia
- wine
- beta
title: "Jagged Alliance 2 - Wildfire est\xE1 en beta para Linux en Steam"
---
Topware Interactive continua trayendo sus juegos antíguos a Linux gracias a Wine.

Ahora es el turno de Jagged Alliance 2 - Wildfire, un juego que mezcla estrategia con elementos de rol. Al igual que anteriores trabajos utiliza wine para traernos este juego a Linux, de momento en fase beta en Steam.


¿Como acceder a la beta?


Si tienes Jagged Alliance2 - Wildfire en tu biblioteca y quieres probarlo, solo tienes que ir a las "propiedades del juego", entrar en la pestaña "beta" e introduce la clave " ja2wflinuxtestbeta" y luego seleccionar la opción "Linux Beta Test".


Aquí teneis el anuncio con toda la información (en inglés): <http://steamcommunity.com/games/215930/announcements/detail/805408149926855512>


Podéis encontrar el juego en la página de Steam:


<div class="steam-iframe"><iframe height="190" seamless="seamless" src="https://store.steampowered.com/widget/215930/" width="646"></iframe></div>


Desde mi punto de vista, que las compañías utilicen Wine para portear sus juegos en vez de hacer un port nativo no está mal. Como se suele decir, cuantos más juegos nos traigan a Linux mejor. Otra cuestión es utilizar Wine con juegos que no están en Linux. Esto ya no es plato de mi gusto, pero cada cual toma sus decisiones.


¿Que piensas sobre esto? cuéntamelo en los comentarios 


 

