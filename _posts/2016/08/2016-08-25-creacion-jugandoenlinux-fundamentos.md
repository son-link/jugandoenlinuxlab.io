---
author: Jugando en Linux
category: Editorial
date: 2016-08-25 18:24:09
excerpt: "<p><em>El mundo ha cambiado. </em></p>\r\n<p><em>Lo siento&nbsp;en el agua.\
  \ </em></p>\r\n<p><em>Lo siento en la tierra. </em></p>\r\n<p><em>Lo huelo en el\
  \ aire.</em></p>\r\n<p>&nbsp;(Film: El Se\xF1or de los Anillos: La comunidad del\
  \ anillo)</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/2fa67f482133f1c934235b73c2a03954.webp
joomla_id: 19
joomla_url: creacion-jugandoenlinux-fundamentos
layout: post
title: "Creaci\xF3n de Jugandoenlinux.com: Nuestros fundamentos. Una web colaborativa"
---
*El mundo ha cambiado.* 


*Lo siento en el agua.* 


*Lo siento en la tierra.* 


*Lo huelo en el aire.*


 (Film: El Señor de los Anillos: La comunidad del anillo)

Cuando en febrero del año 2013 Valve lanzó su cliente de Steam para Linux algo cambió en el mundo del videojuego. El lanzamiento vino acompañado de 50 títulos disponibles para el recién llegado a la plataforma.


Poco después, en noviembre de ese mismo año Valve anunció la creación de su propia distribución Linux basada en Debian y enfocada a los videojuegos: SteamOS. A partir de entonces quedó claro que el juego en Linux había llegado para quedarse. Tanto es así que podemos decir que antes de Steam para Linux el juego en este sistema era poco menos que inexistente.


Desde ese momento el crecimiento del videojuego en Linux ha sido espectacular, involucrando a empresas de software y hardware en un mercado inexplorado hasta entonces. Empresas de desarrollo de hardware como Nvidia, AMD, o Intel han realizado un esfuerzo en desarrollo tanto de tarjetas, chips y drivers, y empresas y desarrolladores tanto indies como grandes corporaciones de software han realizado esfuerzos para adaptar software de desarrollo, APIs y videojuegos para el sistema del pingüino. No solo eso: Han surgido nuevas plataformas de distribución y venta de videojueos que incluyen a Linux entre las plataformas soportadas. Ejemplos como GOG, Humble Bundle, Feral Interactive etc. son buena muestra de ello.


Solo tres años después, hoy podemos decir que solo en Steam encontramos alrededor de 2500 juegos para Linux, y la cifra sigue aumentando día a día. Es obvio que hay un mercado emergente. Tantas empresas invirtiendo tiempo, esfuerzo de desarrollo y dinero en hacer viable el juego en Linux es síntoma de ello. O todo el mundo ha perdido el juicio. (tampoco sería descartable, aunque poco probable)


14 de Febrero de 2013. Esa fecha marcó un antes y un después. Para muchos significó un súbito interés por conocer Linux, y empezar a hacer las primeras incursiones serias en ese Sistema Operativo. A muchos de nosotros que veníamos de otros Sistemas Operativos nos marcó profúndamente la madurez y estabilidad que había alcanzado Linux respecto a años anteriores. A otros con más años de experiencia en el, simplemente fue el cámbio que necesitaban para dejar de depender de otros sistemas para poder jugar a sus juegos favoritos. Pero para todos supuso la constatación de que había alternativa. Linux comenzó un lento pero imparable ascenso en cuanto a instalaciones y uso.


Y ¿si hay un interés creciente en el juego en Linux, por que no hay una web de información de videojuegos en Linux?


Si bien hay blogs personales que ofrecen cierta información en español, las webs de referencia del juego en Linux están en inglés. ¿Por qué no crear entonces una?


Así, un grupo de usuarios con intereses similares nos propusimos crear esta web. Pretendemos que sea una web de información, promoción y ayuda a todo el mundo hispanohablante que tenga interés en jugar en Linux. Creemos firmemente en la web colaborativa. Creemos firmemente en crear una comunidad sana, con intereses comunes, colaborando para que la información, la comunicación y la relación con los demás sea de forma respetuosa, responsable y cordial. Sin importar la experiencia, uso de otros sistemas o postura respecto al software libre o privativo de cualquier otro miembro. Con la madurez que se nos supone a la comunidad linuxera, que lleva décadas ayudando a los demás.


Para ello contamos con todos vosotros. Esperamos daros herramientas para que podáis crear junto a nosotros la web de referencia del juego en Linux para todos.


Bienvenidos a vuestra web.


www.jugandoenlinux.com

