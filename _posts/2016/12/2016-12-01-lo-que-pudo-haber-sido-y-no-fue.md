---
author: leillo1975
category: Editorial
date: 2016-12-01 11:55:45
excerpt: <p>Grandes juegos que por desgracia se quedaron en el tintero</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/e7b279be6a862d254f0e7cc4dde2874e.webp
joomla_id: 134
joomla_url: lo-que-pudo-haber-sido-y-no-fue
layout: post
tags:
- steamos
- darksiders
- batman
- witcher
- linux
- banished
- project-cars
- rome-ii
title: "Lo que pudo haber sido y no fu\xE9"
---
Grandes juegos que por desgracia se quedaron en el tintero

Hace bien pocos años poder usar nuestros flamantes escritorios GNU/Lunux como centro de entretenimiento era una auténtica quimera. Los linuxeros que amamos los videojuegos teníamos que escoger entre usar nuestro sistema operativo favorito y usar Wine, con lo que eso representa; o bien cambiar de sistema (Windows o consolas). De repente Valve entró en escena, prometiendo que haría una versión de su cliente Steam para los hijos de Tux, así como portar y potenciar juegos y drivers para nuestro sistema. Pero no se quedó ahí, sino que incluso comenzó a desarrollar su propia distribución de GNU/Linux basada en Debian (SteamOS) para poder convertir nuestros queridos ordenadores en consolas de videojuegos. Al poco tiempo, la mayoría de su software estaba portado a Linux/SteamOS funcionando con excelentes resultados. Detrás de su estela comenzaron a aparecer juegos más o menos conocidos, portados con diferente éxito y calidad, que poco a poco fueron llenando nuestra estantería virtual de títulos compatibles. Esta situación aun está lejos de ser la soñada, pero ha tenido mucho más éxito del que sinceramente creí en un principio.


A esta carrera se han unido desde pequeños estudios a grandes editoras, portando directamente ellos o encargando el trabajo a otras desarrolladoras especializadas en estos menesteres (Feral, Virtual Programming, Aspyr...). Los métodos utilizados para realizar estas conversiones pueden ser más o menos controvertidos (hay desarrolladoras que directamente usan Wine o sucedaneos para convertir), y los resultados también pueden ser discutibles en cuanto a calidad y rendimiento; pero por norma general nos han permitido disfrutar de un catálogo bastante extenso de videojuegos en nuestro sistema operativo; algo que como comentaba antes hace bien poco era impensable.


Quien nos iba a decir que podríamos jugar a Tomb Raider, Civilization V, Deus EX, F1 2015, Saints Row 4, Euro Truck Simulator 2, Half Life 2, Bioshock Infinite.... sin tener que reiniciar ni despeinarnos. Se puede decir que desde hace unos meses el cambiar de sistema para jugar, al menos en mi caso, ha sido cada vez menor, pudiendo decir con total tranquilidad como ejemplo, que en este momento solo lo hago para jugar a GTA V y Fallout 4.


Aún así, y como reza el título de este artículo, multitud de juegos que prometieron o dieron señales de que iban a llegar a nuestros flamantes equipos, continuan sin dar muestras de avance, cancelandose oficialmente o simplemente dando la callada por respuesta, que es lo que, a mi , personalmente más me fastidia. Algunos de los juegos que por desgracia están en esta lista maldita son:


**-Darksiders I y II:** recientemente ha habido noticias sobre este port, y no son buenas, ya que llevan bastante tiempo queriendo portarlo y no son capaces de corregir algunos bugs. El caso es que el port de este título fué anunciado hace ya más de dos años y no hace más que pasar por multiples problemas, entre ellos el cambio de desarrolladores, ya que en un principio el port fué empezado por [Leszek Godlewski](https://twitter.com/theinequation) ([The Farm 51](http://www.thefarm51.com/eng/), con ports como [Painkiller Hell & Damnation](http://store.steampowered.com/app/214870/?l=spanish) y [Deadfall Adventures](http://store.steampowered.com/app/231330/?l=spanish)) y ahora está en manos de la desarrolladora original del juego, [Vigil Games](http://www.vigilgames.com/).


![darksiders](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/darksiders.webp "Imagen tomada de http://www.descarga2.me")

```
Aún me queda un resquicio de esperanza
```

**-Rome II: Total War:** Cuando se anunciaron las Steam Machines y SteamOS este fué uno de los primeros juegos en anunciarse. El caso es que llevamos años esperando por él y tras idas y venidas, no hay nada oficialmente confirmado. Lo último que se supo es que los desarrolladores se hicieron el loco como que no habían prometido nada. Vale, podemos disfrutar de Medieval II, de Empire, de Attila y recientemente de Warhammer; pero para un servidor no es lo mismo, ya que la temática que más me atrae es esa. A ver si algún estudio se anima a traerlo (¿verdad [Feral](http://www.feralinteractive.com/en/mac-games/rometw/)?).


**-Evolve:** Pertenece al mismo club que el anterior, anunciado para SteamOS cuando aun estaba en desarrollo. De los mismos creadores que Left4Dead ([Turtle Rock](https://www.turtlerockstudios.com/)), no cosechó el éxito que se le suponía. Ahora vive una segunda oportunidad como free-to-play con micropagos, y con otros desarrolladores, por lo que se ve aun más difícil que lo veamos en Linux.


**-The Witcher III Wild Hunt:** Más de lo mismo, quizás Valve esperó demasiado de la palabra de los desarrolladores, o quizás las espectativas de éxito de SteamOS fuesen más altas, pero lo cierto es que el estudio polaco también prometió su juego a la comunidad y en [un escueto mensaje](http://steamcommunity.com/app/258970/discussions/0/535151589889765396/) nos falló diciendo "Donde dije digo, digo Diego".

![Witcher3 Linux](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Witcher3_Linux.webp "Imagen tomada de http://wccftech.com")

```
Los fans Linuxeros de Geralt de Rivia se tienen que conformar con su segunda parte
```

**-Batman Arkham Knight:** Otro que tal baila, como los anteriores. En este caso es muy probable que la razón de su no-salida en SteamOS se debe a que la propia versión original de Windows es una patata. Incluso fué retirado de Steam al poco de salir debido a la terribe cantidad de bugs y su pobre rendimiento. Warner Bros tuvo que hacer frente a una retahila de devoluciones debido a lo terrible de la versión de Windows. Una pena, la verdad, ya que me hacía mucho tilín este juego.


**-Street Fighter V:** Otro de los que fué anunciado a bombo y platillo y al final nada de nada. Quizás le paso como a Batman, que las espectativas eran muy altas y luego el juego resultó ser un Blufff.... entonces para que matarse a hacer un port. El caso es que incluso se rumoreó que el juego sería de los primeros en hacer uso de Vulkan. Bueno, la verdad es que realmente aun nadie ha dicho que esté cancelado, pero en teoría saldría esta pasada primavera. Sinceramente yo no espero nada de él.


![steamos sale](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/steamos-sale.webp "Imagen tomada de  http://www.gameois.com")

```
El muro de la vergüenza
```

**-Gauntlet:** La "reconstrucción" del clásico Gauntlet también estaba prometida cuando se hizo el anuncio de SteamOS y las Steam Machines, pero finalmente los desarrolladores decidieron poner todo el énfasis en la versión de Windows y abandonar el port con [un escueto mensaje](http://steamcommunity.com/app/258970/discussions/0/535151589889765396/).... total para lo que les sirvió...


**-Project Cars:** Este juego prometía muuuucho, recuerdo que cuando veía imágenes en su desarrollo casi lloraba de lo detallado y lo bien realizado que estaba todo. Pero luego el juego no cumplió con las espectativas y no tuvo el exito esperado, probablemente debido a la dificultad y por estar enfocado unicamente a la simulación; y esto seguro que condenó definitivamente nuestro port al ostracismo. Ya se que mal de muchos consuelo de pocos, pero [los usuarios de WiiU también siguen esperando](http://web.archive.org/web/20151222075307/http://www.projectcarsgame.com/faq.html/) por él.


**-Banished:** Para terminar con la lista de  "Éxitos Fallidos" me gustaría resaltar este juego que en teoría segun sus desarrolladores estaba practicamente terminado hace más de medio año, pero que por lo de ahora no acaba de ver la luz en nuestro sistema. Al parecer según informaban en [este post de su blog](http://www.shiningrocksoftware.com/2016-03-30-three-month-update/) el juego era completamente jugable, corría rápido en OpenGL y solo quedaban pequeños bugs y detalles por corregir. Realmente no está cancelado, pero si parece olvidado, ya que no dan noticias de ningún tipo. Una auténtica pena, por que a pesar de no ser un AAA, este juego me llamaba mucho la atención y tenía bastante interés en él.


![Banished](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Banished.webp "Imagen tomada de http://www.shiningrocksoftware.com")

```
 ¿Nos darán una buena noticia algún día?
```

Gracias a dios, otros títulos que tenían todas las trazas de unirse a este fatídico club finalmente vieron su versión para Linux/SteamOS, como fué el caso de **Rocket Leage** o **Payday 2,** por cierto con excelentes ports. Esperemos que la mayoría de los aquí citados y todos lo que seguro se me han quedado en el tintero, podamos disfrutarlos en un futuro; aunque por desgracia me temo que casi con seguridad podemos ir olvidandonos de ellos.

Para rematar la faena os dejo un gran tema de otro "grupito" de mi ciudad con un título muy ilustrativo:


 <div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/hptUE57xH74" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>

Fuentes: [PCWorld](http://www.pcworld.com/article/3112692/linux/broken-promises-games-that-never-made-it-to-steamos-and-linux.html), [Lezsek Godlewski](https://twitter.com/theinequation),[web.archive.org](http://web.archive.org/web/20151222075307/http://www.projectcarsgame.com/faq.html/), [shiningrocksoftware.com](http://www.shiningrocksoftware.com/2016-03-30-three-month-update/)

