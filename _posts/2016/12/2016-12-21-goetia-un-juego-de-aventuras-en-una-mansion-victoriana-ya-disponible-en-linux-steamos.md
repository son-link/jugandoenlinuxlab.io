---
author: Pato
category: Aventuras
date: 2016-12-21 11:59:47
excerpt: "<p>Se trata de un juego en 2D en el que tendr\xE1s que explorar una mansi\xF3\
  n para resolver el misterio.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/aee44fc32e47f07b5fe3050745ad94ac.webp
joomla_id: 158
joomla_url: goetia-un-juego-de-aventuras-en-una-mansion-victoriana-ya-disponible-en-linux-steamos
layout: post
tags:
- indie
- aventura
- terror
- point-and-clic
title: "'Goetia' un juego de \"aventuras\" en una mansi\xF3n Victoriana ya disponible\
  \ en Linux/SteamOS"
---
Se trata de un juego en 2D en el que tendrás que explorar una mansión para resolver el misterio.

De nuevo nos llegan gracias a [SteamDB Linux](https://twitter.com/SteamDB_Linux) noticias de un nuevo juego de tipo "point and clic" en el que vivirás una aventura como el fantasma de una joven muerta dentro de una siniestra mansión victoriana. 


#### Sinopsis del juego:


Goetia es un juego de aventuras de tipo point and click y, como en cualquier otro, tienes que buscar pistas, encontrar objetos y averiguar cómo usarlos o combinarlos para progresar por distintas zonas, puzles y encuentros.   
Sin embargo, hay algo que lo diferencia de los juegos de aventuras a los que estás acostumbrado. En Goetia juegas en el papel del fantasma de una jovencita llamada Abigail, y como tal puedes atravesar las paredes, volar por el techo y explorar cada rincón sin límites. Solo hay una pega: para manipular objetos tienes que poseerlos como un poltergeist, y cuando habitas un objeto ya no es posible atravesar las paredes.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" frameborder="0" height="360" src="https://www.youtube.com/embed/v7uJVNp7ps4" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


 Goetia no está traducido al español, pero si te van este tipo de juegos puedes encontrarlo en su página de Steam:


<div class="steam-iframe"><iframe frameborder="0" height="190" src="https://store.steampowered.com/widget/421740/" width="646"></iframe></div>


¿Te gustan los juegos de tipo "point and clic"? ¿Piensas jugar a este 'Goetia'? 


Cuéntamelo en los comentarios.

