---
author: leillo1975
category: "An\xE1lisis"
date: 2016-12-09 17:50:12
excerpt: "<p>SCSSoft completa el mapa de Francia con esta expansi\xF3n</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/258ee2700b8562b5d51ebf2117179b3d.webp
joomla_id: 143
joomla_url: analisis-ets2-vive-la-france-dlc
layout: post
tags:
- simulador
- ets2
- dlc
- vive-la-france
- analisis
title: "An\xE1lisis: ETS2 - Vive la France! DLC"
---
SCSSoft completa el mapa de Francia con esta expansión

 He de reconocer antes de empezar que soy un auténtico fan de este juego (y de su hermano American Truck Simulator). Más de 270 horas de juego creo que pueden daros una idea de lo mucho que disfruto de él. También me gustaría decir que a pesar de eso, trataré de ser lo más objetivo posible que, dentro de lo que cabe, pueda ser.


Lo primero que creo que hay que hacer es describir [Euro Truck Simulator 2](index.php/top-linux/item/94-euro-truck-simulator-2), sobre todo para los que no lo conozcais. Como bien reza su título, se trata un simulador de trailers por carreteras europeas, y cuando digo simulador, lo digo con todas las consecuencias. El juego consiste en conducir y gestionar nuestra propia empresa de transporte, y esto significa llevar a un gigante de innumerables ruedas por carreteras de todo tipo a través de Europa durante horas y horas. Olvidaos de correr o hacer el cabra, llevais una carga valiosa enganchada en la quinta rueda de vuestro camión que tiene que llegar sana y salva a su destino, de lo contrario habrá consecuencias económicas para vosotros...


 <div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/xm3T-3IzigM" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


Vale, así de primeras no parece muy divertido, y vale, no es para todo el mundo. A Euro Truck Simulator 2 o lo adoras o lo odias. Creo que la mejor forma de que lo entendais es que no es un juego para competir, para excitarte.... no amigos, este juego es para disfrutarlo tranquilo, con nuestra música favorita de fondo, mientras conducimos durante horas y horas por interminables carreteras a través del extenso mapa de Europa, relajarse viendo el paisaje, reconociendo lugares...; pero también controlando los radares, semáforos, cruces y coches de la IA, que en más de una ocasión nos pueden hacer perder los nervios y el dinero.


Otra cosas que ha otorgado un valor añadido a este juego son sus expansiones ([Going East](http://store.steampowered.com/app/227310/?l=spanish), [Scandinavia](http://store.steampowered.com/app/304212/) y ahora [Vive la France!](http://store.steampowered.com/app/531130/)) y DLC's varias, como pinturas, llantas, adornos, etc. También la enorme cantidad y variedad de mods de aficionados (mapas, camiones, iluminación, Tráfico IA...) le da una vida practicamente infinita a este videojuego, siendo encomiables los trabajos realizados por la gente de [ProMods](http://promods.net/), entre otros.


Bueno, vamos al grano. Una vez hecho el prologo vamos a hablar de la expansión en si. Lo primero que hay que decir es que para poder instalarla SCSSoft sacó la actualización 1.26, que ya de por si trae [importantes novedades](http://blog.scssoft.com/2016/11/ets2-update-126-is-now-live.html), como por ejemplo el remarcable trabajo de **renovación de toda la zona noreste de Francia** que está en el juego base, entre lo que destaca el magnífico trabajo hecho con París; **nuevas transmisiones** para todos los camiones, herramienta de **busqueda de partes** en las mejoras, **tres modos de aparcamiento**, como habíamos visto en American Truck Simulator (manual, automático y sencillo), **posibilidad de ajustar el FOV** (campo de visión), entre otras muchas.


![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisETS2ViveLaFrance/ETS2Parking.webp "Aparcar")


En primer lugar me gustaría decir es **agradecer a SCSSoft y a Pavel Medek la clave facilitada**, y que para la realización de este análisis **he desactivado todos los mods**, para que la experiencia, rendimiento y calidad del juego fuese 100% verídica. También he ajustado la calidad en modo Ultra a una resolución de 1920x1080.


Lo primero que podemos observar en esta nueva expansión es que solo abarca el sur y el centro de Francia, pero que eso no nos lleve a engaño. Puede que en otras DLC's se incluyesen más paises y más extensión de terreno, pero la cantidad de Kilómetros a recorrer en VLF! es enorme, concretamente, más de 20.000, y disponemos de 15 nuevas ciudades más detalladas que nunca, donde realizar nuestros trabajos, ubicar nuestros garajes, y comprar o arreglar nuestros camiones.


Como todos sabreis, el ciclismo en la tele es aburrido, pero si por algo vale la pena ver el Tour de Francia es por disfrutar de sus paisajes, algo que también podemos hacer en esta expansión. Las, por desgracia, pocas veces que he pisado el suelo galo me bastan para ver que lo que aquí tenemos delante es realmente Francia. La calidad de los acabados en todos los elementos del mapa es soberbia, encontrándonos sin duda ante el mejor trabajo de modelado por parte de SCSSoft hasta la fecha, incluso superando,si ya es difícil, al de su anterior expansión "Scandinavia". Esto es especialmente palpable si hacemos uso de las carreteras secundarias donde podemos apreciar con más detalle la calidad de los bosques, los campos, los paisajes... Los pueblos del interior de Francia están fielmente retratados, con sus flores, sus casas, muros, etc.


![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisETS2ViveLaFrance/ETS2vegetacion.webp "vegetación")


 Pero no solo paisaje nos vamos a encontrar en VLF!, ya que otros elementos del mapa han sido mejorados y pulidos. Las conocidas áreas de servicio francesas están hechas con detalle, así como las estaciones de servicio, que ahora tienen otra distribución diferente, más amplia y moderna. Otros elementos como señales de tráfico, indicadores y radares (de muy diversos tipos), están perfectamente copiados de los originales siendo facilmente reconocibles del país.


![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisETS2ViveLaFrance/ETS2Seales.webp "Señales")


Tal y como comentaba antes con París, las ciudades ahora no son un conglomerado de tres o cuatro calles con un paisaje urbano al fondo. Si viajamos, por ejemplo a Marsella, Montpelier o Toulouse podemos encontrar edificios, torres, parkings, centros comerciales, estaciones de autobuses o tren... digamos que ahora sí tenemos la sensación de entrar en una urbe, y no como antes que lo veíamos de lejos y solo podíamos ver zonas industriales. Hablando de zonas industriales, también hay que decir que hay nuevas empresas donde poder cargar, nuevos tipos de remolques, centrales nucleares; lo cual obviamente repercute en la variedad de los trabajos y en el número de opciones disponibles de estos cuando estamos en una localidad. También durante el camino encontraremos monumentos y paisajes famosos que nos harán sumergirnos aun más y recrearán nuestro viaje virtual por Francia.


![Paris](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisETS2ViveLaFrance/ETS2Paris.webp "Paris")
Pero si hay un elemento a destacar que afecta directamente a la conducción es el sistema de peajes. Esta característica se había comenzado a implementar en algunos de estos en "Scandinavia", pero es en VLF! donde realmente se les saca partido y se asemeja a lo que es la realidad. Antes teníamos que parar cada dos por tres en estos para recoger ticket o pagar, lo cual era un auténtico incordio. Ahora tenemos además otra opción, y es que simplemente con pasar despacio por la zona de peaje indicada se nos cobrará la cantidad correspondiente, pero recordad, pasando despacio (30-40), por que **si vais demasiado rápido os comereis la barra**. La verdad es que esta característica es algo que agradecer a los desarrolladores y estaría bien que fuese implementado en el juego base y otras expansiones en un futuro, ya que es algo común en las autopistas modernas el no tener que parar a pagar gracias a los sistemas telemáticos.


![Peaje](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisETS2ViveLaFrance/ETS2Peaje.webp "Peaje")


 


Pero no todo iban a ser halagos y alabanzas, ya que aunque la mayoría no sean algo estrictamente relacionado con la DLC en si, sino más bien con el Euro Truck Simulator 2 (y también con American Truck Simulator), los usuarios de GNULinux/SteamOS seguimos sufriendo algunas incidencias a las que no se le ponen solución por parte de SCSSoft. Estas se pueden enumerar en las siguientes:


-Si la expansión es el mapa de Francia, no se entiende como ciudades como Bayonne, Perpignan o Pau no están reflejados. **El sur de Francia en este sentido está claramente incompleto**. Estaría bien que fuesen añadidas al menos en posteriores actualizaciones.


-**El Steam controller sigue sin funcionar correctamente** después de demasiado tiempo en Linux. Es un error reportado en los foros de Steam y SCS y aun no han sabido o querido ponerle solución. Si queremos usarlo , es necesario configurarlo como un teclado + ratón, por lo que el stick analógico no se puede usar de forma precisa, incluso con la configuración oficial. Lo más curioso es que en Windows si funciona correctamente.  Yo encontré una forma de poder usarlo correctamente y es usando [sc-controller](https://github.com/kozec/sc-controller) para emular un mando de XBOX con apuntado de alta precisión.


-**La diferencia gráfica entre OpenGL (Linux) vs DirectX (Windows) sigue siendo demasiado evidente, así como su rendimiento**. La versión de Windows además de ser más rápida tiene más calidad gráfica. Convendría un repaso a la de Linux para optimizarla y pulirla un poco. No es justo que haya diferencias notables entre una y otra. También estaría bien ir pensando en el futuro y ver que posibilidades y beneficios podría dar usar la API de Vulkan.


-**Seguimos sin contar con Radio online en la versión de Linux**. No podemos entender cual es problema de implementar la radio online en el juego. Vale, poner el música fuera del juego es una solución, pero que yo sepa los usuarios de linux pagamos lo mismo que los de Windows.


![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisETS2ViveLaFrance/ETS2Rotonda.webp)


 


Pero que estos problemas no empañen la calidad y buen hacer de SCSSoft con esta expansión. Los fans de este juego la disfrutaremos como enanos recorriendo Kilómetros  y kilómetros de carreteras Francesas, y seguiremos soñando que en un futuro, quizás podamos hacerlo por la península Ibérica.... y para ambientaros un poco os dejo con una canción que le va al pelo de uno de mis grupos favoritos:


*"""* *Otra noche*   
*en otra ciudad*   
*horas y horas*   
*carreteras sin final*   
*pesa el sueño*   
*y no puedes descansar*   
*y en la plaza*   
*esperan mil cajas*   
*para descargar. """"* 


 <div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/8zJmJFnR2lc" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


 Podemos comprar Euro Truck Simulator 2: Vive la France! en Steam:


 <div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/531130/" width="646"></iframe></div>


 


Fuentes: [Blog de SCSSoft](http://blog.scssoft.com/2016/11/ets2-update-126-is-now-live.html)

