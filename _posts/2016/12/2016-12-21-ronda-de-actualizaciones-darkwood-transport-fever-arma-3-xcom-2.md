---
author: Pato
category: Software
date: 2016-12-21 16:39:01
excerpt: <p>Repasamos las actualizaciones mas relevantes que se han liberado hoy.</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/665e3353c5a0a1298b58f0408e39e998.webp
joomla_id: 160
joomla_url: ronda-de-actualizaciones-darkwood-transport-fever-arma-3-xcom-2
layout: post
tags:
- accion
- primera-persona
- simulador
- estrategia
- actualizacion
title: 'Ronda de actualizaciones: Darkwood, Transport Fever, ArmA 3, XCOM 2'
---
Repasamos las actualizaciones mas relevantes que se han liberado hoy.

Se ve que los estudios se han puesto de acuerdo y hoy estamos teniendo una avalancha de actualizaciones. Tendrá algo que ver con la inminente llegada de las rebajas?...


Comenzamos con '**Darkwood**'. Se trata de un juego más que interesante aunque lleve en acceso anticipado desde hace un par de años. Acid Wizard nos lo anunciaba en un tweet:



> 
> Ho ho ho! We have a new Darkwood update for you just before Christmas! <https://t.co/TaMYEedJc0> [pic.twitter.com/DTCFXDt0F9](https://t.co/DTCFXDt0F9)
> 
> 
> — Acid Wizard Studio (@TheAcidWizard) [21 de diciembre de 2016](https://twitter.com/TheAcidWizard/status/811592988820381697)







Darkwood es un juego de terror en vista cenital con un aspecto visual muy cuidado y con un juego de luces y sombras muy acertado que te hará dar más de un salto de tu silla. Sustos y taquicardias garantizadas.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/2fyhUWP2l00" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


La actrualización que nos llega hoy justo antes de "Navidad" (ho ho ho!) es la Alpha 9.0 y podeis ver el log de la actualización [aquí](http://steamcommunity.com/games/274520/announcements/detail/600500712284245512).


Darkwood no está disponible en español y aún está en fase alfa pero si aún así quieres darle una oportunidad puedes conseguirlo en su web de Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/274520/" width="646"></iframe></div>


Personalmente le tengo muchas ganas.


Seguimos con **ArmA 3**, el simulador deguerra que recibe una nueva actualización del cliente para Linux que según su tweet nos trae la versión 1.64:



> 
> Linux & Mac users can now try [#Arma3](https://twitter.com/hashtag/Arma3?src=hash) v1.64, including Apex (thx [@virtualprog](https://twitter.com/virtualprog))! More on these experimental ports: <https://t.co/BdQzkTwFOC> [pic.twitter.com/YJOdoAGmB1](https://t.co/YJOdoAGmB1)
> 
> 
> — Arma 3 (@Arma3official) [21 de diciembre de 2016](https://twitter.com/Arma3official/status/811588674387329024)







Como sabéis, el port de ArmA 3 corre a cargo de Virtual Programming y trabajan sobre la rama "[experimental](https://dev.arma3.com/ports)" del simulador, es decir no tiene soporte oficial. La actualización que nos traen es vastante grande (pesa entre 1 y 5,6 gigas dependiendo de lo que tengas actualizado) y  viene con nuevo protocolo de dificultad APEX, mejoras en las rutas de los vehículos y mucho más. Teneis el log de actualización [aquí](https://dev.arma3.com/post/spotrep-00059). (¿el log tiene fecha de septiembre? WOW)


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/M1YBZUxMX8g" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


Como ya he dicho, ArmA 3 en Linux no tiene soporte oficial y en cualquier momento puede dejar de recibir actualizaciones, pero si aún así quieres probarlo lo tienes en su página de Steam (tendrás que acceder a la rama experimental)


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/107410/95419/" width="646"></iframe></div>


 


Continuamos repasando actualizaciones, con **Transport Fever**. Se trata de un juego de "gestión" donde tendrás que levantar un imperio alrededor del mundo del transporte ya sea por tierra, mar o aire. Gracias a los chicos de [Linuxgameconsortium.com](https://linuxgameconsortium.com/linux-gaming-news/transport-fever-gets-new-patch-improve-performance-45170/) nos enteramos que se ha mejorado sustancialmente el rendimiento en el renderizado, simulación y construcción, además de otras mejoras menores. Tienes el log de la actualización [aquí](http://steamcommunity.com/app/446800/discussions/1/224446432325393106/).


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/pAbaLPKFAdg" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


Si te van los juegos de gestión y estrategia puedes comprar Transport Fever en su página de Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/446800/" width="646"></iframe></div>


No se vayan todavía, ¡aún hay mas!... seguimos con **XCOM 2**, el gran juego de estrategia porteado por Feral para nuestro sistema. Gracias a [gamingonlinux.com](https://www.gamingonlinux.com/articles/xcom-2-for-linux-updated-officially-supports-mesa.8772) sabemos que los chicos de Feral lo han actualizado y ahora soporta oficialmente los drivers Mesa 13.0.1, soporte para mandos de juego y otros cambios menores. (Por cierto... ¡[en el foro tenemos](index.php/foro/tutoriales/7-ppa-drivers-mesa-no-oficial-de-paulo-dias) como instalar los drivers Mesa!)


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/VIRDb_O6qXA" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


Si quieres echar un vistazo a esta actualización y ver como se mueve en tu equipo con los drivers Mesa tienes XCOM 2 en su página de Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/268500/" width="646"></iframe></div>


 


¿Nos dejamos alguna actualización importante en el tintero? Seguro que si. ¿Por qué no nos lo cuentas en los comentarios?

