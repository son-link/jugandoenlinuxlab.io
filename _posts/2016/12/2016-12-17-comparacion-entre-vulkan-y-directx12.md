---
author: Serjor
category: Editorial
date: 2016-12-17 15:13:52
excerpt: "<p>Quiz\xE1s la nueva API de Microsoft no es lo que los desarrolladores\
  \ estaban esperando</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/5fd45095a868b007b05013834dba7a95.webp
joomla_id: 152
joomla_url: comparacion-entre-vulkan-y-directx12
layout: post
tags:
- vulkan
title: "Comparaci\xF3n entre Vulkan y DirectX12"
---
Quizás la nueva API de Microsoft no es lo que los desarrolladores estaban esperando

Gracias a The Linux Gamer nos enteramos vía [twitter](https://twitter.com/thelinuxgamer/status/810115780734029824) de un nuevo vídeo hecho por [Penguin Recordings](https://www.youtube.com/user/PenguinRecordings), el cuál  nos trae una comparación de las nuevas APIs gráficas disponibles para los desarrolladores. Si bien el vídeo está en inglés, activando los subtítulos se puede seguir sin problemas.


Esperemos que análisis como este convenzan a los desarrolladores que Vulkan es una opción más interesante y desarrollen más juegos con esta API en mente, lo cuál facilitaría la migración a Linux.


Os dejamos con el vídeo:


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" frameborder="0" height="315" src="https://www.youtube.com/embed/ApvTaSAG--4" width="560"></iframe></div>


 


Y tú, ¿qué opinas de las nuevas APIs gráficas? ¿Crees que cumplen lo que prometen? Deja tu opinión en los comentarios.

