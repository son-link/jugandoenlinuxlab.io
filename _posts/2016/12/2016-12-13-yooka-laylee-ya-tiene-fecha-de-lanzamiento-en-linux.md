---
author: Pato
category: "Exploraci\xF3n"
date: 2016-12-13 16:01:29
excerpt: "<p>Playtonic games lo ha hecho p\xFAblico mediante un comunicado oficial.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/8ee107fb8e11fa27c5eb0c84c03d7dff.webp
joomla_id: 151
joomla_url: yooka-laylee-ya-tiene-fecha-de-lanzamiento-en-linux
layout: post
tags:
- plataformas
- aventura
- proximamente
- exploracion
title: Yooka Laylee ya tiene fecha de lanzamiento en Linux
---
Playtonic games lo ha hecho público mediante un comunicado oficial.

Yooka Laylee es ese tipo de juegos a los que no puedo resistirme. Banjo Kazooie siempre fue un juego al que quise jugar y con el que mis amigos "nintenderos" siempre me daban (sana) envídia.


Así que cuando Playtonic Games anunció su kickstarter para llevar a cabo el juego y encima traerlo a Linux no me lo pensé mucho y aporté para el proyecto. (Si, ya se... nunca pongas dinero en promesas... pero no pude resistirme). Y es que son los mismos creadores de BK... ¿que más se podía pedir?


Pues saber la fecha de salida, y hoy por fin me ha llegado el aviso (al ser backer me llegan todas las novedades) de que el juego llegará a todos los sistemas el día 11 de Abril de 2017. (Excepto a WiiU, que se ha cambiado por su salida en Switch). Puedes ver el anuncio oficial en la [web de Playtonic](http://www.playtonicgames.com/yooka-laylee-reptile-rolls-towards-release/).


La verdad es que la pinta del juego es muy buena y espero poder ofreceros mis impresiones de primera mano en cuanto pueda echarle el guante.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/u5BqQXuzAAQ" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


 ¿Que te parece este Yooka Laylee? ¿Piensas jugarlo?


Cuéntamelo en los comentarios.

