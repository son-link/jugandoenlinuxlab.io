---
author: leillo1975
category: Editorial
date: 2016-12-30 14:54:56
excerpt: <p>La tienda digital nos brinda estos dias juegos enormes por muy poco dinero</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/32088387da419227f20729e6cf7687d8.webp
joomla_id: 166
joomla_url: autenticos-juegazos-por-menos-de-5-en-steam
layout: post
tags:
- steam
- oferta
- '5'
title: "Aut\xE9nticos juegazos por menos de 5\u20AC en Steam"
---
La tienda digital nos brinda estos dias juegos enormes por muy poco dinero

Tal y como acabo de leer en [GamingOnLinux](https://www.gamingonlinux.com/articles/some-absolute-crazy-deals-are-available-on-steam-for-linux-for-under-4.8819) en estas fechas podemos hacernos con verdaderas gangas por muy poco precio. Si disponemos de poco "cash" y queremos hacernos con grandes juegos quizás [esta lista](http://store.steampowered.com/search/?category1=998&os=linux&filter=ut2&page=5) te pueda ayudar, ya que en ella podrás encontrar una cantidad absurda de juegos por debajo de los 5€ entre los que podemos destacar los siguientes:


-[Euro Truck Simulator 2](http://store.steampowered.com/app/227300/?snr=1_7_7_ut2_150_1) con un descuento del 75% (4.99€)


-[Tomb Raider (2013)](http://store.steampowered.com/app/203160/) con un descuento del 75% (4.99€)


-[XCOM](http://store.steampowered.com/app/200510/?snr=1_7_7_ut2_150_1) con un descuento del 75% (4.99€)


-[The Witcher 2](http://store.steampowered.com/app/20920/?snr=1_7_7_ut2_150_2) con un descuento del 85% (2.99€)


-[Saints Row: The Third](http://store.steampowered.com/app/55230/?snr=1_7_7_ut2_150_2) con un descuento del 75% (2.49€)


-[Outlast](http://store.steampowered.com/app/238320/?snr=1_7_7_ut2_150_2) con un descuento del 75% (4.99€)


-[Spec Ops: The Line](http://store.steampowered.com/app/50300/?snr=1_7_7_ut2_150_4) con un descuento del 80% (3.99€)


-[Dirt Showdown](http://store.steampowered.com/app/201700/?snr=1_7_7_ut2_150_5) con un descuento del **90% (1.49€)**


-[Serious Sam 3](http://store.steampowered.com/app/41070/?snr=1_7_7_ut2_150_5) con un descuento del 90% (3.69€)


-[Victor Vran](http://store.steampowered.com/app/345180/?snr=1_7_7_ut2_150_5) con un descuento del 75% (4.99€)


-[Brütal Legend](http://store.steampowered.com/app/225260/?snr=1_7_7_ut2_150_6) con un descuento del **90% (1.49€)**


-[Shadow Warrior](http://store.steampowered.com/app/233130/?snr=1_5_9__412) con un descuento del 90% (3.49€)


 


Estos son solo algunos ejemplos, pero [la lista](http://store.steampowered.com/search/?category1=998&os=linux&filter=ut2) está plagada de juegos tremendamente buenos. ¿Vas a comprar alguno? Si ves alguna oferta reseñable que no esté resaltada en este artículo indícalo en los comentarios.

