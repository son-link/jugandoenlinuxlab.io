---
author: Pato
category: Apt-Get Update
date: 2016-12-09 20:10:59
excerpt: "<p>Repasamos lo que ha sucedido esta semana y que no hemos tratado en Jugando\
  \ En Linux. \xBFNos acompa\xF1as?</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/4047dc2f08c422a43c6430815243ead6.webp
joomla_id: 146
joomla_url: apt-get-update-vulkan-api-drivers-mesa-grave-danger-stellaris-oneshot
layout: post
title: Apt-Get update Vulkan API & Drivers Mesa & Grave Danger & Stellaris & OneShot...
---
Repasamos lo que ha sucedido esta semana y que no hemos tratado en Jugando En Linux. ¿Nos acompañas?

La semana se nos acaba y es el momento de volver a repasar un nuevo Update. Comenzamos:


- Comenzamos con la confirmación de que los próximos drivers propietarios para tarjetas de Nvidia vendrán con soporte para la API Vulkan sin necesidad de renderizado en el servidor X.org, lo que significa que la API podrá ser independiente de un servidor o compositor de escritorio. [Nos lo contaban en Phoronix](http://www.phoronix.com/scan.php?page=news_item&px=NVIDIA-Vulkan-Without-X).


- Por otra parte, el consorcio Kronos que se encarga precísamente del desarrollo de la API Vulkan está empezando a sentar las bases de lo que será un nuevo estandar abierto para la Realidad Virtual en donde tendrán cabida prácticamente todos los dispositivos existentes, ya que dentro del consorcio están Valve, Oculus y Razer que ha sido la última en incorporarse para dar soporte a sus gafas de RV. Valve ya ha dicho que está ya trabajando para convertirlo en su estandar. [Nos lo contaba Liam en Gamingonlinux](https://www.gamingonlinux.com/articles/khronos-are-working-on-an-open-standard-for-vr-valve-will-use-it.8671). ¿Y esto que tiene que ver con Linux?... bueno, en algún momento en el futuro se supone que nos darán soporte para la Realidad Virtual... al menos por parte de Valve.


- Desde [linuxgameconsortium.com](https://linuxgameconsortium.com/linux-gaming-news/grave-danger-team-based-action-platformer-coming-soon-42373/) nos llegaba la noticia de que el juego "Grave Danger" nos llegará próximamente a nuestros sistemas. El juego promete acción y diversión multijugador. ¿La fecha?: el 19 de este mes.


- También en [linuxgameconsortium.com](https://linuxgameconsortium.com/linux-gaming-news/horizon-signal-dlc-available-free-for-all-stellaris-players-42351/) nos cuentan que Stellaris ha recibido un DLC gratuito para todos los jugadores.


- Volvemos a los drivers, y esta vez en [gamingonlinux.com](https://www.gamingonlinux.com/articles/amd-working-on-an-updated-driver-that-will-support-freesync-on-linux-and-wider-gpu-support.8672) nos decían que AMD está trabajando en sus controladores para dar mejor soporte a una gama mas ámplia de sus tarjetas gráficas, y posteriormente [nos anunciaron](https://www.gamingonlinux.com/articles/amdgpu-pro-1650-released-with-freesync-and-wider-gpu-support.8691) que ya están disponibles.


- Para terminar con el tema drivers, otra vez [Liam de Gamingonlinux.com](https://www.gamingonlinux.com/articles/ubuntu-now-has-a-community-built-ppa-for-stable-versions-of-mesa.8688) nos anunció que ya hay un PPA mantenido por la comunidad con los drivers Mesa estables para Ubuntu. Si tienes tarjetas AMD o Intel ya puedes tener los drivers actualizados sin tener que quebrarte la cabeza con compilaciones.


- En [Linuxgameconsortium.com](https://linuxgameconsortium.com/linux-gaming-news/oneshot-launches-pixel-art-rpg-pulls-puzzles-42460/) nos decían que ya está disponible el juego OneShot, un RPG pixel art con puzzles.


Y de momento eso es todo por esta semana. La semana que viene más y mejor en otro Apt-Get Update.


¿Piensas que me he dejado algo importante? seguro que si. Cuéntamelo en los comentarios.


 

