---
author: Pato
category: Aventuras
date: 2016-12-12 18:11:57
excerpt: "<p>As\xED parece tras las informaciones tanto en SteamDB como de Ethan Lee,\
  \ que parece estar al cargo del port.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/3a9297ca78720986dccc8a65ce802a33.webp
joomla_id: 149
joomla_url: owlboy-esta-muy-cerca-de-llegar-a-linux-steamos
layout: post
tags:
- indie
- plataformas
- aventura
title: "Owlboy est\xE1 muy cerca de llegar a Linux/SteamOS"
---
Así parece tras las informaciones tanto en SteamDB como de Ethan Lee, que parece estar al cargo del port.

Todo parece indicar que Owlboy, el aclamado y galardonado juego de aventuras (Análisis extremadamente positivos en Steam) está cerca de llegar a Linux/SteamOS a tenor de los comentarios de Ethan Lee en su Tweet, con imágenes incluidas:



> 
> Oh yeah also this is probably going to show up on SteamDB over the weekend so here this is being a real thing coming soon (well, maybe soon) [pic.twitter.com/TgxnSMExlV](https://t.co/TgxnSMExlV)
> 
> 
> — Ethan Lee (@flibitijibibo) [8 de diciembre de 2016](https://twitter.com/flibitijibibo/status/806964300678131713)







Al parecer Ethan es el encargado de portear el juego a nuestros sistemas. Además de esto, en SteamDB Linux han twitteado también la aparición del juego en la base de datos:



> 
> New Game:  
> Owlboy<https://t.co/GwGbzlRnkG>
> 
> 
> — SteamDB Linux Update (@SteamDB_Linux) [12 de diciembre de 2016](https://twitter.com/SteamDB_Linux/status/808344939532787712)







 Blanco y en botella, ¿no?


Owlboy es un juego de aventuras con un aspecto "pixel art" donde tendrás que moverte y volar entre plataformas.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/p9VwGaycmCQ" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


 Esperemos que la espera valga la pena. ¿Piensas jugar a Owlboy?


Cuéntamelo en los comentarios.

