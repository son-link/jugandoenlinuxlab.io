---
author: leillo1975
category: "An\xE1lisis"
date: 2016-12-15 17:30:00
excerpt: "<p>King Art Games nos trae en esta ocasi\xF3n un RPG bastante notable</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/b320775de3c297425b69dccc362220a9.webp
joomla_id: 147
joomla_url: analisis-the-dwarves
layout: post
tags:
- rol
- rpg
- dwarves
title: "An\xE1lisis: The Dwarves"
---
King Art Games nos trae en esta ocasión un RPG bastante notable

Probablemente a muchos de vosotros os pase como a mi. Si sois seguidores de la obra de Tolkien, hay que reconocer que la raza que mejor suele caer son los enanos. Son rudos, duros, obstinados, pero a su vez también son "campechanos", fieles, nobles y simpáticos. Obviamente, si tuviese que confiar en una raza de este mundo fantástico, los enanos serían con claridad mis preferidos. Pero no estamos hablando en este caso de la obra de Tolkien, aunque es seguro que para [Markus Heitz](https://en.wikipedia.org/wiki/Markus_Heitz) esta ha sido una gran influencia a la hora de escribir sus libros; y es estos en los que se basa "The Dwarves", siendo esta saga en la que transcurre el juego.


Nos encontramos ante un título que se engloba en el genero del Rol en tiempo real, pero con la particularidad de poder parar el tiempo para tomar decisiones en los combates y poder usar varios heroes simultaneamente. En estas características recuerda a Baldur's Gate y derivados, pero esto es de lo poco en lo que se parece. El juego está traducido en textos al Español (entre otros muchos idiomas) y en cuanto a las voces pueden ser en Inglés y en Alemán. No dispone de modo multijugador y el modo de un solo jugador se ciñe unicamente a la campaña, al menos de momento, ya que se especula con la posibilidad de hacer alguno más en futuras actualizaciones.


![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisDwarves/tungdil.webp)*El protagonista, Tüngdil, es herrero de profesión*
 


El juego gira en torno a las peripecias de Tüngdil, un enano huérfano que es adoptado por un Mago maestro, y que vive en una especie de bunker donde se enseña a otros magos. Tu historia comienza cuando tu "padre" te envía de viaje en busca de otro mago para entregarle una bolsa. A partir de ese momento empezarán a ocurrir cosas a tu alrededor y no tendrás más remedio que involucrarte. Por el camino conocerás a otros personajes de lo más variado que te acompañarán en tu aventura y ayudarán en los combates.


Es ahí casualmente donde más destaca el juego, ya que según avance la historia podremos usar los diferentes héroes en las escaramuzas. Estos pueden ser de diferentes razas (enanos y humanos principalmente), y de diferentes tipos; pudiendonos encontrar con guerreros, magos, picaros, etc. Cada uno de estos tendrá sus propias habilidades que con la experiencia subirán de nivel y podrán adquirir más. El número de héroes es muy grande, pero en las batallas solo podremos acompañar a Tüngdil de 3 compañeros, siendo estos los que más experiencia acumularán al finalizar. Según usemos unos héroes u otros variará nuestro estilo de juego, pudiendo este ser más directo o más táctico. En cuanto al diseño artístico de estos heroes como del resto de personajes decir que el juego destaca, siendo en el caso de los enanos sobresaliente, ya que estos están retratados con todo lujo de detalles.


![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisDwarves/DiseoPers.webp)*Los chicos de King Arts tenían claro como tenía que ser un enano*

Los enemigos son poco variados, y basicamente son hordas de Orcos de diferentes tipos y tamaños, Ogros y no-muertos. También nos encontramos de vez en cuando con algún jefe, normalmente Albos y Magos, siendo estos más escurridizos, teniendo algún tipo de habilidad especial y más cantidad de vida, con lo que obviamente cuesta más acabar con ellos.


En cuanto al inventario, es ahí donde quizás a mi juicio más cojea el juego, ya que por ejemplo todo lo que podemos equipar es un amuleto o poción que podemos usar en los combates. También necesitaremos raciones para aguantar en el camino, y podremos obtener tanto dinero como objetos valiosos para poder comprar en las diferentes tiendas que nos encontraremos en nuestra aventura. Las armas y armaduras por ejemplo no se pueden cambiar ni mejorar, ya que vienen dadas por la historia, quitándole al juego la posibilidad de personalizar a los héroes. En cuanto a las habilidades estas no son tampoco muy numerosas y no se pueden cambiar ni escojer, como mucho elegir entre dos diferentes. Estas habilidades pueden ser de todo tipo, desde golpes y barridos a lanzamientos de hechizos, bombas, proyectiles, curaciones ...


 


![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisDwarves/habilidades.webp)*Al subir de nivel tenemos la posibilidad de aumentar nuestras habilidades*

Para ir de un lado a otro del juego utilizaremos el mapa donde nos movemos por turnos y donde veremos también los movimientos de los Orcos. Si nos cruzamos con ellos entraremos en modo batalla y tendremos que acabar con ellos o llegar a la salida para continuar la aventura. En ciertos puntos del mapa nos encontraremos con aldeas, fortificaciones y pueblos; que por norma general nos darán la posibilidad de encontrar pistas para nuestra historia, momentos de exploración y pequeños problemas que resolver mediante diálogos con diferentes opciones. Es en la exploración del mapa donde encontramos mayor profundidad al juego, permitiéndonos adentrarnos en el mundo creado por el autor de los libros, el conocimiento a nivel personal de nuestros heroes, y alguna que otra misión secundaria entre la que destaca la del Guardian Ciego (Blind Guardian). En cuanto al aspecto artístico del mapa decir que este no es un dibujo estático  sino que está hecho en 3D y tiene relieve, pájaros, niebla, etc.


![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisDwarves/mapaniebla.webp)*El mapa está realizado con bastante detalle y tiene efetos como la niebla que vemos*
 


La parte relaccionada con los combates al principio puede parecer confusa, ya que estas son casi siempre multitudinarias. Nos enfrentaremos a hordas de enemigos que no serán fáciles de afrontar, pero poco a poco y haciendo uso de la pausa se le va cogiendo el truco y vamos aprendiendo a usar las diferentes habilidades y características de cada héroe. Las batallas son largas  y nos exigirán pensar bastante a menudo las decisiones que tomemos. Un detalle a tener en cuenta en estas es la física, que puede jugar tanto a nuestro favor como en contra. Por ejemplo, podemos aprovechar el entorno tirando con nuestros golpes a los enemigos por precipicios, pero cuidado, ya que usar mal una de nuestras habilidades puede provocar que nuestros amigos sufran la misma suerte (hay fuego amigo) y tengamos que empezar de nuevo la batalla. También es un tanto molesto a veces el no poder moverse por estar rodeado tanto por enemigos como por nuestros compañeros, por lo que si por ejemplo queremos apartar un rato a uno de nuestros héroes de la batalla para que se recupere, debemos "hacerle pasillo" para que pueda moverse, si no se quedará bloqueado con el riesgo de morir. Esta última característica también se puede entender como una virtud, ya que nos obliga a planificar nuestra posición y movimientos.


![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisDwarves/combate.webp)*Los combates multitudinarios pueden agobiar al principio, pero con el tiempo resultan divertidos*

En cuanto al apartado sonoro, tanto las voces escogidas para los personajes, como el resto de efectos son correctos, cumpliendo sobradamente con su cometido. La banda sonora hay que resaltar que es excelente y tiene una gran calidad, ya que incluso se ha usado una orquesta sinfónica para su grabación, aunque en determinados momentos puede llegar a ser un tanto repetitiva. Hay que comentar que en esta última colabora el grupo alemán de Heavy Metal Épico [**Blind Guardian**](http://www.blind-guardian.com/) con una canción que podemos disfrutar en los títulos de crédito. Si quereis escucharla teneis el video de esta al final del artículo.


Sobre a la calidad gráfica, el juego tiene un buen acabado en general, más que suficiente para este tipo de juegos, pero no destaca por nada. Las cinemáticas por ejemplo están muy bien hechas, pero a veces da la impresión que los personajes son de plástico. Sobre el rendimiento gráfico del juego decir que cumple, pero que no es excelente. En calidad buena (no la mejor) el juego se movía entre los 35-50 fps a una resolución de 1920\*1080 con una Nvidia GTX950. No se si es un problema exclusivo de su versión en Linux, o por el contrario los usuarios de Windows tienen el mismo problema. De todas formas, estos pequeños problemas no impiden para nada la jugabilidad del título ya que en este tipo de juegos no es necesario que el juego vaya tan fluido, como por ejemplo en un FPS, pero hay juegos similares que se mueven mucho más rápido. Quizás en parches posteriores puedan pulir un poco más el rendimiento.


 


![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisDwarves/GuardianCiego.webp)*El guardian ciego será una de las misiones secundarias que tendremos que afrontar*
 


En cuanto a la duración del juego esta puede variar, pero a mi juicio el completar la historia principal puede llevar entre 10 o 15 horas, según la dificultad que escojamos o lo meticulosos que seamos con nuestras tácticas. Puede parecer un desarrollo corto, pero es en la exploración del mapa y en las misiones secundarias donde podemos sacarle mas "chicha" al juego pudiéndose multiplicar el tiempo de juego.


Hay que comentar que el juego se ha estrenado con algunos problemas en la versión de GNU/Linux. Yo particularmente he encontrado las siguientes:


**-** En ocasiones he tenido algún "Crash to Desktop". Este se ha producido de forma inesperada y en diferentes situaciones, por lo que no pude adivinar cual podría ser el desencadenante.


**-** En alguna batalla he notado como se bajaba solo el volumen de la música y no se podía subir en los ajustes.


**-** En el mapa de la campaña los nombres de los lugares aparecían en ocasiones en portugués. También he notado que hay partes de los diálogos hablados que no se ven traducidas en los textos.


**-** A veces, en las pantallas de selección de Heroes, subida de nivel y nuevas habilidades; había parpadeos que impedían ver por ejemplo quien usaba un objeto o el icono de la habilidad escogida.


**-** Por último, y aunque no es un error si no una sugerencia, se podría poner en el menú un botón de "Continuar Campaña" que cargase el último guardado, ya que si pulsamos el botón campaña comienza una nueva partida otra vez.


Supongo que estos pequeños problemas se irán corrigiendo en sucesivas actualizaciones y con ello también se mejorará la experiencia de juego.


![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisDwarves/heroes.webp)*Poco a poco, el grueso de nuestras fuerzas irá aumentando, proporcinandonos más posibilidades de elección*
 


Como conclusión pienso que con un par de toques en la gestión del inventario, más profundidad en las mecánicas del juego, y cuatro pinceladas más; podríamos tener un  "The Dwarves" que se pudiese equiparar con los grandes del género. Quizás si los desarrolladores tienen la oportuniad de realizar una segunda parte (espero que si), podamos ver un juego mucho más completo y redondo. Pero no os dejeis llevar por los toques negativos que he mencionado, ya que **en conjunto nos encontramos ante un muy buen juego,** en el que se nota que se han puesto muchas ganas por parte de sus creadores, y que nos hará disfrutar unas cuantas horas de lo lindo pegando mamporros con nuestros héroes de baja estatura. **¡¡¡¡¡POR VRACCAS!!!!!**


 


Sin más os dejo con el tema de Blind Guardian para el juego:


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/F5X7MXfLxEg" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


 Si quereis comprar The Dwarves podeis hacerlo en Steam:


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/403970/79083/" width="646"></iframe></div>


 


FUENTES: [Wikipedia](https://en.wikipedia.org/wiki/Markus_Heitz), [The Dwarves](http://www.dwarves-game.com/)

