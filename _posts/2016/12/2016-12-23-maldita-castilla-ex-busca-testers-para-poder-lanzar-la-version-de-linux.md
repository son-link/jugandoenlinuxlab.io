---
author: Pato
category: Arcade
date: 2016-12-23 18:58:09
excerpt: "<p>El juego es una versi\xF3n extendida del cl\xE1sico desarrollado por\
  \ \"Locomalito\".</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/47359a90eed3ee35f2dab5a3c718abb3.webp
joomla_id: 164
joomla_url: maldita-castilla-ex-busca-testers-para-poder-lanzar-la-version-de-linux
layout: post
tags:
- accion
- indie
- plataformas
- steam
- arcade
title: "'Maldita Castilla Ex' busca testers para poder lanzar la versi\xF3n de Linux"
---
El juego es una versión extendida del clásico desarrollado por "Locomalito".

Maldita Castilla es ya un clásico dentro del software español. Se trata de un juego que homenajea a otros juegos del género arcade como Ghost and Goblins. Su responsable "Locomalito" lo desarrolló en un principio y ahora junto a Abylight Studios han lanzado este "Maldita Castilla Ex", una versión extendida.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" frameborder="0" height="360" src="https://www.youtube.com/embed/uYDkao8UJw0" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


Como es habitual cuando un juego es de buena calidad, algunos usuarios preguntaron por la posibilidad de un port a Linux en los foros de Steam, por lo que Abylight Studios ha respondido solicitando ayuda para testear el port. En concreto comentan:



> 
>  Due to we'll need tester for Mac & Linux, subscribe writing an email to [info@abylight.com](mailto:info@abylight.com) with the subject "Cursed Castilla - Linux" or "Cursed Castilla - Mac" and when we have available those versions, we'll contact you to initiate the process.
> 
> 
> "*Debido a que necesitaremos testers para Mac & Linux, suscríbete enviando un email a [info@abylight.com](mailto:info@abylight.com) con el asunto ""Cursed Castilla - Linux" o "Cursed Castilla - Mac" y cuando tengamos disponibles esas versiones, contactaremos contigo para iniciar el proceso"*
> 
> 
> 


 Podéis ver el post en el foro de Steam [en este enlace](http://steamcommunity.com/app/534290/discussions/0/348293292500651601/?tscn=1482456374#c340412122405421842) (En inglés):


[Y](http://steamcommunity.com/app/534290/discussions/0/348293292500651601/?tscn=1482456374#c340412122405421842)a sabes, si estás interesado puedes contactar con ellos.


¿Piensas hacerlo? ¿te gustaría que Maldita Castilla Ex llegara a Linux?


Cuentanoslo en los comentarios.

