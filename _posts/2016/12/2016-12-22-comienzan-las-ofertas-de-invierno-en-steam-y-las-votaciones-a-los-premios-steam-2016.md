---
author: Pato
category: Software
date: 2016-12-22 17:53:17
excerpt: "<p>Hasta el d\xEDa 2 de Enero grandes descuentos en multitud de t\xEDtulos\
  \ en la tienda de Valve.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/5a61d31ed794cb758475f6c89477dfed.webp
joomla_id: 162
joomla_url: comienzan-las-ofertas-de-invierno-en-steam-y-las-votaciones-a-los-premios-steam-2016
layout: post
tags:
- steamos
- steam
title: Comienzan las Rebajas de invierno en Steam y las votaciones a los Premios Steam
  2016
---
Hasta el día 2 de Enero grandes descuentos en multitud de títulos en la tienda de Valve.

Como ya es tradición al llegar estas fechas navideñas Steam celebra las rebajas de invierno. Casi sin tiempo de recuperarnos de las de otoño la tienda de Valve nos vuelve a tentar para adquirir a precio reducido esos títulos que tanto estamos deseando. ¿Quién puede resistirse a unas buenas rebajas?.


Yo como es normal algo picaré, [como os contaba ayer](index.php/item/261-las-rebajas-de-invierno-de-steam-son-inminentes-que-juegos-rebajados-esperais-con-mas-ilusion) tengo una buena lista de la compra, por si queréis echarle un vistazo.


 


#### Premios Steam 2016


Como ya sabéis, durante las rebajas de Otoño se hicieron las nominaciones a los premios Steam de este año, por lo que ahora no toca elegir cada día un ganador de cada categoría de entre los nominados. ¡No olvides hacerlo! con cada votación te obsequiarán con un cromo especial de los Premios de Steam para realizar la correspondiente insignia.


Ya sabéis la dirección, pero por si acaso: [store.steampowered.com](http://store.steampowered.com/)


¿Que tal si me cuentas lo que tienes pensado comprar? o mejor... ¿lo que ya has comprado?


Nos vemos en los comentarios.

