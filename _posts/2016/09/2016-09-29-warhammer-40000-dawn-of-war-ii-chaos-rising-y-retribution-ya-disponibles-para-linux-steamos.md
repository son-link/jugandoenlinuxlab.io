---
author: Pato
category: Estrategia
date: 2016-09-29 18:16:46
excerpt: "<p>Feral Interactive nos trae nuevos ports a Linux/SteamOS. Esta vez se\
  \ trata no de uno, si no de tres. Y de los buenos!</p>\r\n<p>Dawn of War II, Chaos\
  \ rising y Retribution llegan para saciar nuestra sed de estrategia!</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/e31ace2a15a7c70645ad83df9ecd43b0.webp
joomla_id: 28
joomla_url: warhammer-40000-dawn-of-war-ii-chaos-rising-y-retribution-ya-disponibles-para-linux-steamos
layout: post
tags:
- accion
- rts
title: Warhammer 40000 Dawn of War II, Chaos Rising y Retribution ya disponibles para
  Linux/SteamOS
---
Feral Interactive nos trae nuevos ports a Linux/SteamOS. Esta vez se trata no de uno, si no de tres. Y de los buenos!


Dawn of War II, Chaos rising y Retribution llegan para saciar nuestra sed de estrategia!

 


Seguimos recibiendo ports de grandes juegos en Linux, y esta vez los maestros del port de Feral Interactive nos traen tres juegos de la serie Warhammer 40,000 para nuestro deleite.


Los requisitos recomendados son:


Sistema de 64 bits


Procesador de 2.0 Ghz o mas


4GB de RAM


Gráficas: mínimo Nvidia serie 6XX de 1GB, AMD serie 6XXX de 1GB o Intel serie Iris pro


Recomendadas: Nvidia serie 7XX 2GB o AMD serie R9, o superiores


Las gráficas Nvidia requieren Drivers 367.35


AMD e Intel requieren MESA 11.2


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" seamless="seamless" src="https://www.youtube.com/embed/2-94aTa6ygU" width="640"></iframe></div>


A tenor de los comentarios, parece que los chicos de Feral Interactive han hecho un buen trabajo en la optimización y el rendimiento en Linux/SteamOS es bastante bueno y estable, comparable al juego en Windows!


Tienes el juego ya en la tienda propia de Feral Interactive y Steam


Grand Master Collection (los tres juegos, mas DLCs): 


[Tienda Feral](https://store.feralinteractive.com/en/mac-linux-games/dawnofwar2grandmastercollection/)


[Steam](http://store.steampowered.com/sub/44367/)


Master Collection (los tres juegos):


[Tienda Feral](https://store.feralinteractive.com/en/mac-linux-games/dawnofwar2mastercollection/)


[Steam](http://store.steampowered.com/sub/44367/)


Warhammer 40,000: Dawn of War II:


[Tienda Feral](https://store.feralinteractive.com/en/mac-linux-games/dawnofwar2/)


[Steam](http://store.steampowered.com/app/15620/)


Warhammer 40,000: Dawn of War II Chaos Rising:


[Tienda Feral](https://store.feralinteractive.com/en/mac-linux-games/dawnofwar2chaosrising/)


[Steam](http://store.steampowered.com/app/20570/)


Warhammer 40,000: Dawn of War II Retribution:


[Tienda Feral](https://store.feralinteractive.com/en/mac-linux-games/dawnofwar2retribution/)


[Steam](http://store.steampowered.com/app/56437/)

