---
author: Pato
category: "Acci\xF3n"
date: 2016-09-16 17:58:13
excerpt: <p>Es un divertido juego multijugador casual para divertirte con amigos.</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/184b7cb84d7b456c96a0bdfbbeaa5f14.webp
joomla_id: 26
joomla_url: marooners-un-juego-de-accion-multijugador-ya-disponible-en-linux-steamos
layout: post
tags:
- accion
- indie
- multijugador
- casual
title: "Marooners, un juego de acci\xF3n multijugador ya disponible en Linux/SteamOS"
---
Es un divertido juego multijugador casual para divertirte con amigos.

 


El juego consiste en jugar a una serie de divertidos minijuegos donde deberás luchar por conseguir los tesoros mientras evitas los ataques de tus amigos y las trampas del entorno. Además, de regalo te echas unas risas mientras lo intentas... 


Lo cierto es que es visualmente muy llamativo y las opiniones en su página de Steam están siendo de lo más positivas.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" seamless="seamless" src="https://www.youtube.com/embed/GHuvqZVQWBs" width="640"></iframe></div>


Lo tienes en la tienda de Steam: <http://store.steampowered.com/app/423810/>


### Info del juego:


Juega a una mezcla de divertidos juegos de fiesta  y alterna de uno a otro. ¿Sabrás apañártelas entre tanto caos? Únete a tus amigos en multijugador local y en línea y disfruta con sadismo de sus desgracias. ¡Corre, salta y golpea para hacerte con la victoria!

