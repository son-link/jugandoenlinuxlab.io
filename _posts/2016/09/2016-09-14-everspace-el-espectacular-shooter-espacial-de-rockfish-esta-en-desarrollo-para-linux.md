---
author: Pato
category: "Acci\xF3n"
date: 2016-09-14 15:58:10
excerpt: "<p>Aunque los desarrolladores est\xE1n encontrando m\xE1s dificultades de\
  \ las esperadas, el juego de acci\xF3n espacial deber\xEDa estar disponible en fase\
  \ beta en pr\xF3ximas fechas.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/2ff2ba0051687eef5ca0459cf942940c.webp
joomla_id: 22
joomla_url: everspace-el-espectacular-shooter-espacial-de-rockfish-esta-en-desarrollo-para-linux
layout: post
tags:
- accion
- disparos
- espacio
title: "Everspace, el espectacular shooter espacial de Rockfish est\xE1 en desarrollo\
  \ para Linux"
---
Aunque los desarrolladores están encontrando más dificultades de las esperadas, el juego de acción espacial debería estar disponible en fase beta en próximas fechas.

 


Según se desprende de los comentarios del propio desarrollador en los [foros de Steam](http://steamcommunity.com/app/396750/discussions/0/353916981477131900/?ctp=2) el juego ya es funcional en Linux, aunque sigue teniendo problemas con los shaders (utilizan Unreal Engine), aunque confían en que con el paso a OpenGL 4.0 puedan resolver los fallos de forma satisfactoria.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="480" seamless="seamless" src="https://www.youtube.com/embed/WlJrokiuSO4" width="853"></iframe></div>


Confían en tener una versión funcional durante la fase de pruebas o acceso anticipado que ya [está en marcha en Steam](http://store.steampowered.com/app/396750).

