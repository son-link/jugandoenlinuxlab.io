---
author: Pato
category: Rol
date: 2016-11-25 16:45:33
excerpt: "<p>Antes de empezar el port el desarrollador admiti\xF3 no saber gran cosa\
  \ sobre Linux, y tras recibir ayuda de algunos programadores y usuarios ahora lo\
  \ tenemos disponible para nuestro sistema favorito.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/f60a47f7792630a3a86bc14c0657e89c.webp
joomla_id: 124
joomla_url: the-living-dungeon-ya-esta-disponible-en-linux-steamos-gracias-a-la-ayuda-de-la-comunidad
layout: post
tags:
- indie
- rol
- steam
- estrategia
title: "'The Living Dungeon' ya est\xE1 disponible en Linux/SteamOS con la ayuda de\
  \ la comunidad"
---
Antes de empezar el port el desarrollador admitió no saber gran cosa sobre Linux, y tras recibir ayuda de algunos programadores y usuarios ahora lo tenemos disponible para nuestro sistema favorito.

La historia de 'The Living Dungeon' [[web oficial](http://thelivingdungeon.com/)] es de esas que te hacen sentir orgulloso de la comunidad Linuxera.


El concepto del juego nació de la base de un original juego de rol de mesa donde solo o con hasta 9 compañeros o adversarios debes desarrollar tu estrategia y cinco movimientos correspondientes a sus respectivos dados en tandas de dos minutos.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" frameborder="0" height="360" src="https://www.youtube.com/embed/BUFDEp8lzLQ" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


 


Los desarrolladores se lanzaron a la aventura de programarlo, y con talento comenzaron a diseñar todo el juego en 3D, las figuritas, el tablero, personajes etc. dándole un aspecto único. Incluso [lanzaron una campaña](https://www.kickstarter.com/projects/927742767/the-living-dungeon) en Kickstarter sin mucho éxito. Ellos mismos reconocen que su fuerte no es el márketing por lo que tras retirarse de la campaña decidieron lanzar el juego en Steam para Windows y en XBox, pasando sin pena ni gloria. Sin embargo el juego a tenor de los comentarios de todo el que es fan de los juegos de tablero y lo ha probado tiene muy buena calidad. 


En febrero un usuario preguntó por la posibilidad de que el juego fuese portado a Linux a lo que el desarrollador respondió que vería esa posibilidad si la comunidad mostraba interés. Y vaya si lo mostró. Tras reconocer que no tenían mucha idea de como llevar a cabo el port a Linux comenzó un proceso en el que tanto usuarios, otros programadores y beta testers han estado involucrados en hacer posible que el juego viese la luz en Linux y hoy por fin tenemos este excelente juego disponible para nosotros.


Toda esta historia la tienes en el hilo donde se ha ido siguiendo el desarrollo de este port en los foros del juego en Steam:


<http://steamcommunity.com/app/362290/discussions/0/358415738205660849/?tscn=1480081391>


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/BUFDEp8lzLQ" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


Sinceramente, pienso que es un claro ejemplo de cómo una comunidad interesada ayuda a quien no sabe o puede a llevar a cabo un proyecto. Personalmente pienso darle una buena oportunidad y ya está de los primeros de mi lista de próximas compras.


'The Living Dungeon' no está traducido al español, pero si eso no es problema para ti tienes disponible el juego en su página de Steam:


<div class="steam-iframe"><iframe frameborder="0" height="190" src="https://store.steampowered.com/widget/362290/" width="646"></iframe></div>


¿Que te parece la historia de 'The Living Dungeon'? ¿Piensas darle una oportunidad? ¿Te gustan este tipo de "juegos de tablero"?


Cuéntamelo en los comentarios.


¿Tienes 'The Living Dungeon' y quieres que publiquemos tu review? ¡[Mándanosla](https://www.jugandoenlinux.com/contacto/envia-un-articulo)!

