---
author: Pato
category: Software
date: 2016-11-23 18:58:45
excerpt: "<p>\xBFA\xFAn no estais leyendo este art\xEDculo o entrando en Steam? \xA1\
  Corred insensatos!</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/d722f2a14a84b7ad8053262f61a6106b.webp
joomla_id: 118
joomla_url: steam-comienza-sus-rebajas-de-otono-y-convoca-los-primeros-premios-de-steam-de-la-historia
layout: post
tags:
- steamos
- steam
- rebajas
title: "Steam comienza sus rebajas de Oto\xF1o y convoca los primeros 'premios de\
  \ Steam' de la Historia"
---
¿Aún no estais leyendo este artículo o entrando en Steam? ¡Corred insensatos!

Tal y como [publicamos esta semana](https://www.jugandoenlinux.com/item/105-filtradas-las-fechas-de-las-proximas-rebajas-de-otono-e-invierno-de-steam), la filtración de las fechas de las rebajas de Steam parece que es verídica. Hoy han comenzado las rebajas de Otoño de Steam con cientos de juegos en rebajas hasta el día 29 de este mes.


Como es costumbre habrán insignias que conseguir realizando ciertas tareas que luego os darán cromos con los que conseguirlas. 


Y como en Jugando En Linux queremos haceros las cosas fáciles, aquí tenéis un enlace al listado de todas las ofertas disponibles en Linux/SteamOS:


<http://store.steampowered.com/search/?os=linux&specials=1>


Por cierto, el hardware de Steam también está de rebajas, así que si estabas esperando para poder pillar un Steam Controller o un Steam Link ahora es buen momento. Puedes ver el hardware de Steam [en este enlace](http://store.steampowered.com/hardware).


Por otra parte, este año Steam se ha sacado de la manga los "primeros" premios de Steam de la Historia, que se fallarán en Diciembre pero durante estas rebajas de Otoño se llevarán a cabo las nominaciones por parte de los usuarios que quieran participar y votar.


 


![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Steamoto16/Steamotoñopre16.webp)


 


Por Nominar también te darán insignias, así que ¡no pierdas la oportunidad!


Como es normal en todo certamen de nominaciones, existen varias categorías, a cual mas "original":


Premio «La prueba del tiempo»  
Premio «No estoy llorando, es que se me ha metido algo en el ojo»  
Premio «Solo cinco minutos más»  
Premio «¡Qué alucinante!»  
Premio «Villano más necesitado de un abrazo»  
Premio «Juego dentro de un juego»  
Premio «Yo ya pensaba que este juego era bueno antes de que ganara un premio»  
Premio «Lo mejor que se puede hacer con un animal de granja»  
Premio «No hemos pensado en todo»


Puedes ver todos los premios con sus explicaciones y participar en las nominaciones en este enlace:


<http://store.steampowered.com/SteamAwardNominations/>


¿Que juegos piensas comprar estas rebajas? ¿has comprado alguno ya? ¿Que te parecen los premios de Steam?


Cuéntamelo en los comentarios.

