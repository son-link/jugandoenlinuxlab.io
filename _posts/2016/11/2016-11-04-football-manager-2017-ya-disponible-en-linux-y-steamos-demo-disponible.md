---
author: Pato
category: Deportes
date: 2016-11-04 18:03:21
excerpt: "<p>La nueva entrega del m\xE1nager de Futbol de Sega llega simult\xE1neamente\
  \ a todos los sistemas, pero no est\xE1 cosechando buenas cr\xEDticas.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/d6086de322f98f66cc694f32ea284557.webp
joomla_id: 88
joomla_url: football-manager-2017-ya-disponible-en-linux-y-steamos-demo-disponible
layout: post
tags:
- deportes
- steamos
- steam
- simulador
- estrategia
title: Football Manager 2017 ya disponible en Linux y SteamOS, Demo disponible
---
La nueva entrega del mánager de Futbol de Sega llega simultáneamente a todos los sistemas, pero no está cosechando buenas críticas.

La serie de juegos 'Football Manager' ya lleva unas cuantas entregas con soporte para sistemas Linux, y esta versión 2017 no es una excepción. Sega nos trae un manager deportivo en el que poder gestionar nuestro própio Club de Futbol en el (según los desarrolladores) "juego de gestión deportiva más realista e inmersivo creado hasta la fecha".


Con unos 2500 clubes y 500.000 futbolistas reales tendrás que gestionar transferencias, traspasos, estrategias y decidir quien juega cada fin de semana. También podrás seguir los partidos "en vivo" gracias a su motor 3D que te permitirá ver los partidos a pie de campo.


Football Manager 2017 tiene disponible una demo para que puedas probar el juego antes de comprarlo, pero pese a todo está cosechando malas críticas en Steam. El hecho de que este Football Manager 2017 venga con pocas novedades respecto a las versiones de años anteriores y el precio un tanto desorbitado que Sega le ha puesto están haciendo mella en los posibles compradores. Además también hay algunos reportes de que la versión demo del juego en Linux tiene problemas no reproduciendo los sonidos del juego. No tengo información en estos momentos de si la versión definitiva del juego tiene el mismo problema.


Si llegas de nuevas al mundo de Football Manager o hace unas cuantas ediciones que no lo pruebas este puede ser un buen juego (siempre que no tenga problemas con la reproducción de sonido), pero si tienes las últimas entregas posíblemente te convenga esperar a que el precio baje un poco y haya alguna actualización.


¿Que te parece este Football Manager 2017? ¿Tienes problemas con el sonido?


Cuéntamelo en los comentarios.


Puedes comprar Football Manager 2017 en Steam:


<div class="steam-iframe"><iframe height="190" seamless="seamless" src="https://store.steampowered.com/widget/482730/" width="646"></iframe></div>

