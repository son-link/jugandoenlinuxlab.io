---
author: Pato
category: "Acci\xF3n"
date: 2016-11-02 14:31:46
excerpt: "<p>Seg\xFAn lo publicado por Feral Interactive las \xFAnicas gr\xE1ficas\
  \ con soporte oficial ser\xE1n las Nvidia.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/4d8c9898b5bb88437f053c8b957f47f3.webp
joomla_id: 82
joomla_url: deus-ex-mankind-divided-ya-tiene-requisitos-minimos-y-recomendados
layout: post
tags:
- accion
- proximamente
- feral-interactive
title: "Deus Ex Mankind Divided ya tiene requisitos m\xEDnimos y recomendados"
---
Según lo publicado por Feral Interactive las únicas gráficas con soporte oficial serán las Nvidia.

Ya anunciamos que Deus Ex Mankind Divided [llegará mañana a Linux y SteamOS](item/50-deux-ex-mankind-divided-llegara-a-steamos-y-linux-el-3-de-noviembre) y hoy Feral ha hecho públicos los requisitos mínimos y recomendados.


Las únicas tarjetas soportadas oficialmente son las Nvidia gracias a su soporte de las últimas versiones de OpenGL. Al parecer la falta de especificación oficial por parte de Mesa de OpenGL 4.4/4.5 ha supuesto esta decisión. Esto no significa que el juego no vaya a funcionar en tarjetas de AMD o Intel, si no que pueden tener problemas de bajo rendimiento o errores a la hora de mover el juego o las texturas.


Los requisitos publicados son:


**Mínimos:**


 Procesador Intel Core i3-4130 o AMD FX8350


8GB RAM


Tarjeta Nvidia 680 2GB(driver version 367.57)


**Recomendados:**


Procesador Intel Core i7-3770K


16GB RAM


Tarjeta Nvidia 1060 6GB (driver version 367.57)


 


Como puedes ver Deus Ex Mankind Divided pide bastante equipo, al igual que lo hace para otros sistemas. Veremos que tal está de optimizado por parte de Feral.


Ya puedes visitar el [minisite del juego](http://www.feralinteractive.com/es/games/deusexmd/story/) en la página de Feral Interactive.


¿Que piensas sobre los requisitos? ¿Vas a jugar a este Deus Ex? Cuéntamelo en los comentarios.

