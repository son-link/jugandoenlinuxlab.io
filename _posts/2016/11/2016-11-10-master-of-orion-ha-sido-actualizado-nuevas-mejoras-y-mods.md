---
author: Pato
category: Estrategia
date: 2016-11-10 19:40:12
excerpt: "<p>El juego de estrategia espacial por turnos de NGD Studios recibe una\
  \ actualizaci\xF3n mejorando caracter\xEDsticas y contenido.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/1d73e13563b8be946c0f00bab252d7ea.webp
joomla_id: 99
joomla_url: master-of-orion-ha-sido-actualizado-nuevas-mejoras-y-mods
layout: post
tags:
- espacio
- steam
- estrategia
- workshop
title: 'Master of Orion ha sido actualizado: Nuevas mejoras y Mods'
---
El juego de estrategia espacial por turnos de NGD Studios recibe una actualización mejorando características y contenido.

Master of Orion es un juego de estrategia por turnos ambientado en el espacio que está basado en el Master of Orion original, que ya ha cumplido nada menos que 20 años.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" seamless="seamless" src="https://www.youtube.com/embed/5XPF69SjMF4" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


Esta remasterización salió en febrero de este mismo año y acaba de ser actualizado añadiendo nuevas características y mejoras como la inteligencia artificial, combate táctico, algunos nuevos controles, nuevas cinemáticas y animaciones.


Aunque lo más reseñable de la actualización es la llegada de los mods al juego mediante Steam Workshop, por lo que los jugadores ya pueden crear y compartir contenido, realizar modificaciones etc.


Aunque las modificaciones que se pueden realizar ahora mismo son limitadas, el estudio piensa en que sea una mejora sustancial respecto a la jugabilidad de Master of Orion. También han anunciado que esto solo es el principio y que mas adelante añadirán mas posibilidades respecto a los mods.


Tienes toda la información sobre el parche en las [notas de la actualización](http://steamcommunity.com/gid/[g:1:9085117]/announcements/detail/912370373274417168K1Ner6touqGhGo21aw&m=1).


#### Sinopsis del juego:


*¡Vuelve el apasionante juego de estrategia y viajes espaciales 4X! El nuevo capítulo de la épica saga Master of Orion está listo para volver a conquistar la imaginación de millones de jugadores.*


*Descubrid un Master of Orion fiel a su concepto original: una mezcla cuidadosamente orquestada de combates y exploración interestelar en galaxias lejanas con animaciones cuidadosamente diseñadas. Enfrentaos a civilizaciones hostiles, negociad con misteriosos extraterrestres, compartid conocimientos con vuestros aliados y descubrid este universo renovado.*


#### Características:


*Las 10 especies del Master of Orion original, que vuelven a la vida con una irresistible inteligencia artificial y la voz de actores galardonados con diversos premios.*


* *Más de 75 avances tecnológicos que se pueden desarrollar.*
* *Vastas galaxias con hasta 100 sistemas solares distintos, compuestas de una miríada de planetas y estrellas.*
* *Naves personalizables con diversos estilos para cada especie.*
* *Múltiples maneras de alcanzar la victoria, entre las que se encuentra la conquista, la tecnología, la diplomacia, etc.*
* *Una dinámica de juego cautivadora que te hará decir «solo un turno más» una y otra vez*


Master of Orion tiene menús y subtítulos en español y puedes comprarlo en Steam:


<div class="steam-iframe"><iframe height="190" seamless="seamless" src="https://store.steampowered.com/widget/298050/" width="646"></iframe></div>


¿Te gustan los juegos de estrategia espaciales? ¿Que te parecen las nuevas características de Master of Orion?


Cuéntamelo en los comentarios.


¿Tienes Master of Orion y quieres que publiquemos tu review? ¡[Envíanosla](contacto/envia-un-articulo)!

