---
author: Pato
category: Aventuras
date: 2016-11-15 17:00:07
excerpt: <p>Se trata de un juego de aventuras de primer nivel. Llega a Linux de salida
  hoy mismo.</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/63503204d18160dd6a733e989b70c404.webp
joomla_id: 106
joomla_url: silence-de-daedalic-entertainment-ya-disponible-en-linux
layout: post
tags:
- indie
- aventura
- fantasia
title: '''Silence'' de Daedalic Entertainment ya disponible en Linux'
---
Se trata de un juego de aventuras de primer nivel. Llega a Linux de salida hoy mismo.

'Silence' es un juego de aventuras y fantasía con una jugabilidad y un aspecto visual de primer nivel a tenor de los comentarios de las previews que se han publicado. En concreto Liam de GoL [ha tenido acceso](https://www.gamingonlinux.com/articles/silence-from-daedalic-entertainment-is-an-absolutely-beautiful-story-rich-adventure-game-my-thoughts-on-it.8531) al juego y según sus palabras ha quedado bastante impresionado por su calidad.


En Silence jugarás como Noah, un muchacho de 16 años que junto a su hermana pequeña Renie buscan protección en un refugio durante un ataque aéreo. Allí no solo se encuentran a salvo de un bombardeo mortal, sino también en la encrucijada de un mundo entre la vida y la muerte: Silence. Cuando Noah pierde a su hermana, se ve obligado a adentrarse en el idílico y desconocido mundo de Silence con el objetivo de encontrarla.


Acompaña a Noah y Renie en un apasionante viaje a través de Silence y vive una intensa aventura en un mundo de contrastes entre la armonía y el peligro. Conoce a fascinantes personajes como Spot, la oruga mágica que ayudará a Noah y Renie. Y vive el momento en el que los hermanos descubren que el amor que sienten el uno por el otro es lo único que puede salvarles.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" seamless="seamless" src="https://www.youtube.com/embed/B_qWm1CBtw0" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


Silence llega a Linux con menús y subtítulos traducidos al español. Tienes disponible Silence en [GOG](https://www.gog.com/game/silence), [Humble Store](https://www.humblebundle.com/store/silence) y en su página de Steam:


<div class="steam-iframe"><iframe height="190" seamless="seamless" src="https://store.steampowered.com/widget/314790/" width="646"></iframe></div>


¿Que te parece la propuesta de Silence? ¿piensas jugarlo?


Cuéntamelo en los comentarios.


¿Tienes Silence y quieres que publiquemos tu review? ¡[Envíanosla](contacto/envia-un-articulo)!

