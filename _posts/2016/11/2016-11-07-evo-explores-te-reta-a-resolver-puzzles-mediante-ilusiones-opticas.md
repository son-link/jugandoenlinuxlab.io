---
author: Pato
category: Puzzles
date: 2016-11-07 20:20:36
excerpt: "<p>El juego es un cont\xEDnuo reto visual. Ya disponible en Linux/SteamOS.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/924e149af069b8ea323a809fbb1171d4.webp
joomla_id: 92
joomla_url: evo-explores-te-reta-a-resolver-puzzles-mediante-ilusiones-opticas
layout: post
tags:
- indie
- steam
- puzzles
title: "'Evo Explores' te reta a resolver puzzles mediante ilusiones \xF3pticas"
---
El juego es un contínuo reto visual. Ya disponible en Linux/SteamOS.

Evo Explores me llamó la atención en cuanto lo vi. El concepto de puzzle mediante ilusiones ópticas aparte de original me parece un reto no ya solo para el jugador, si no para el desarrollador que tiene que tener en cuenta todas las vistas posibles del puzzle.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" seamless="seamless" src="https://www.youtube.com/embed/km1iVNK5GFo" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


#### Sinopsis del juego:


*• En Evo Explores tú debes confiar en tus ojos. Si algo parece real - es real! Si no ves un problema - no hay ningún problema en absoluto.*


*• Manipula imposibles estructuras surrealistas, explora ilusiones ópticas y resuelve puzzles alucinantes.*


*• Cada nuevo nivel irá desvelando la historia del Planeta Byte y sus ciudadanos.*


*• Evo es un explorador del espacio. Cada planeta que visita está lleno de misterios. Pero el planeta Byte es único.*


*• Las leyes de la física no funcionan aquí. Incluso la gravedad está bajo el control de tu imaginación.*


Puedes comprar Evo Explores en Steam (durante estos días esté de rebajas al 50%):


<div class="steam-iframe"><iframe height="190" seamless="seamless" src="https://store.steampowered.com/widget/467210/" width="646"></iframe></div>


¿Que te parece el concepto del juego? ¿Piensas darle una oportunidad?


Cuéntamelo en los comentarios.


¿Tienes el juego y quieres que publiquemos tu review? ¡[Mándanosla](contacto/envia-un-articulo)!

