---
author: leillo1975
category: "An\xE1lisis"
date: 2016-11-18 20:34:00
excerpt: "<p>Feral Interactive nos vuelve a traer otro gran t\xEDtulo. En este art\xED\
  culo desgranaremos las virtudes y defectos de este fant\xE1stico juego</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/75b44b0e9c2e5d305fa323c6c51d3476.webp
joomla_id: 110
joomla_url: analisis-mad-max
layout: post
tags:
- accion
- feral-interactive
- sandbox
- mad-max
- conduccion
title: "An\xE1lisis: MAD MAX"
---
Feral Interactive nos vuelve a traer otro gran título. En este artículo desgranaremos las virtudes y defectos de este fantástico juego

*""La gasofa dónde está*   
 *dónde está la gasolina*   
 *si tuvieras combustible*   
 *cuantas millas andarías.*   
 *comes latas para perros*   
 *que, ñam! ñam!, están muy buenas*   
 *esta noche hay masacre*   
 *y mañana adiós muy buenas*


*Y mírate Max, estás hecho una pena Max,*  
 *estás hecho una pena""*


 


Recordando *viejos y mejores tiempos*, pienso que esta canción de **Siniestro Total**  le va al pelo para dar entrada al siguiente artículo:


Poco más de un año después de su salida en Windows y consolas, llega a nuestros libres escritorios un título AAA con todas las de la ley.  Mad Max está desarrollado por los estudios Avalanche (Saga Just Cause, Renegade Ops) y se trata de un sandbox (nunca mejor dicho lo de caja de arena, pues hay bastante) . El port a Linux/SteamOS y Mac está realizado, como suele ser habitual ultimamente con títulos de este calibre, por [Feral Interactive](http://www.feralinteractive.com/es/) (F1 2015, Shadow of Mordor, Deus Ex: Mankind Divided, Tomb Raider...)


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" seamless="seamless" src="https://www.youtube.com/embed/-mXWT-f9FAU" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


Por si queda alguien que no conozca la saga de cine Mad Max, está ambientada en un mundo postapocalíptico donde los pocos humanos que aun sobreviven se abren paso en un desierto sin recursos, y la violencia desenfrenada es la tónica general. Como sabreis, a parte de la última película estrenada el año pasado, está la trilogía original interpretada por Mel Gibson.


Pero vamos a lo que nos importa, que es el juego. Como comentaba antes, en el resto de sistemas se lanzó hace más o menos un año con opiniones para todos los gustos. Estas venían motivadas por lo repetitivo de las mecánicas de juego principalmente. Como en mi caso esto no es algo que me importe mucho, decidí lanzarme, y bien que hice, porque me he encontrado un juego absorbente que invita a jugar horas y horas.


 


![MadMax06](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisMadMax/MadMax06.webp)


 


Pero vamos por partes, y para ello comenzaremos con las odiosas comparaciones, ya que las influencias son claras. Tiene elementos claros de Tomb Raider, sobre todo en lo tocante al completado de items en las diferentes zonas, progreso del personaje, combate... esto ya lo habíamos visto antes en otro juego de Warner Bros, como es Shadow of Mordor. Obviamente también encontramos influencia de otros sandbox como GTA en el tema de conducción, mapa...


El juego está localizado con textos y subtítulos en castellano, pero las voces están en Inglés. Esto puede ser bueno o malo segun se mire. Personalmente a mi no me estorba si se trata de una cinemática o en una escena en la que puedas pararte a leer, pero por ejemplo, cuando vas conduciendo y "Chum" se pone a hablar contigo, cuesta un poco más. Este "defecto" también lo podemos encontrar en juegos como GTA IV y V.


 


![MadMax05](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisMadMax/MadMax05.webp)
*La calidad del páramo por donde nos movemos es asombrosa*



En [Mad Max](http://www.feralinteractive.com/es/games/madmax/) todo gira alrededor de los coches, y en esa faceta es donde el juego sobresale, permitiendo con las mejoras y personalización crear nuestra máquina de guerra a nuestro gusto. Las diferentes partes de los coches se pueden conseguir completando misiones y subiendo niveles. Estas pueden ser mejoras de prestaciones del coche, blindajes, armas, carrocerías, pinturas, etc. También podemos desbloquear opciones preconfiguradas de "Magnum Opus" llamadas "Arcángeles" con características optimizadas para diferentes facetas del juego. Podemos hacernos con los vehículos  enemigos y coleccionarlos, o usar el Buggie de nuestro  compañero y mecánico "Chum"  para, junto con nuestro perro, buscar y desenterrar minas en diferentes partes del mapa. Con nuestro Magnun Opus, a parte de desplazarnos por el mapa, podremos atacar otros coches y estructuras, utilizando para ello diferentes armas, entre las que sobresale el gancho. También podemos usar nuestra escopeta, cañones de llamas y el rifle francotirador. En el caso de este último tendremos que detener el coche, como es lógico, para poder utilizarlo. Nuestro coche, por supuesto, consume gasolina, algo que en este mundo en el que jugamos es difícil de conseguir, y que en los primeros compases del juego nos dara algún que otro problema.


 


![MadMax02](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisMadMax/MadMax02.webp)
*La customización del coche es uno de los aspectos más sobresalientes del juego*



Las mejoras no solo atañen a los vehículos, ya que también hay espacio para el "crecimiento personal".  A medida que vamos avanzando en el juego iremos subiendo de nivel y consiguiendo puntos de "Griffa" que nos permitirán mejorar nuestras aptitudes, como si de un juego de rol se tratase. Estas aptitudes pueden ser los puntos de vida, la eficacia y duración de las armas cuerpo a cuerpo, la habilidad para extraer agua, metabolizar comida, optimizar la gasolina, encontrar más munición o chatarra.... También podemos desbloquear o comprar mejoras de nuestra equipación, como guantes, armas, armadura, ataques... o simplemente diferentes aspectos o skins (pelo, barba, gafas, etc)


 


![MadMax03](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisMadMax/MadMax03.webp)
*Max podrá mejorar sus habilidades y personalizar su apariencia.*



Chatarra... esa es la palabra mágica en este juego. La chatarra es la moneda de cambio en Mad Max. La podemos conseguir de diferentes formas, extendida por el mapa en campamentos, en lugares ocultos, como recompensa por completar misiones, como envíos regulares por ir liberando campamentos, o personajes repartidos por el mapa que si les ayudas reparten el botín contigo. También podemos conseguir chatarra destrozando vehículos enemigos o derribando espantapájaros, unas estructuras repartidas por el mapa para recordar el dominio de nuestro principal enemigo, "Scabrous Scrotus".  Esta chatarra puede servirnos para comprar mejoras para el Magnum Opus o para aumentar las habilidades de Max.


 


![MadMax11](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisMadMax/MadMax11.webp)


*El mapa está dividido en Regiones gobernadas desde una fortaleza y divididas en diversas zonas*


 


El mapa en Mad Max es increiblemente grande, y está dividido en diferentes regiones, cada una de ellas controlada por diferentes caudillos, de los que tenemos que ser merecedores de su favor para poder acceder a sus fortalezas. En un principio comenzaremos en el santuario de Chum, nuestro compañero de andanzas, pero poco después tendremos que abandonarlo y buscar cobijo en la fortaleza de Jeet. En las fortalezas nos otorgaran misiones y también podremos ir desbloqueando proyectos que facilitarán a vida en estas, como depósitos de petroleo, de agua, criadero de gusanos (comida), armería, etc. Estas mejoras nos permitiran por ejemplo llenar  nuestra cantimplora de agua, el tanque de nuestro vehículo, la munición, etc. Alrrededor de estas fortalezas, en región controlada por el caudillo, habrá diferentes zonas con diferentes misiones, campamentos, lugares que saquear, campos de minas... en los que tendremos que ir explorando poco a poco. Algo importante en el juego es reducir la amenaza de Scrotus en cada una de estas regiones, derribando espantapajaros, acabando con francotiradores, liberando campamentos, acabando con convoyes de transporte de gasolina, o desenterrando minas antipersonales. Reduciendo la amenaza conseguiremos el favor de los caudillos y desbloquearemos mejoras. También podremos desbloquear "Atalayas", que son campamentos con un globo aerostático que nos permite ver las diferentes localizaciones que hay en la región usando nuestros prismáticos; y también nos permite el desbloquearla para poder realizar viajes rápidos por el mapa.


 


![MadMax01](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisMadMax/MadMax01.webp)
*Nuestro perro nos ayudará a localizar minas antipersonales*



En cuanto a ciertos aspectos técnicos del juego, es necesario decir que hay aspectos muy bien diseñados y estructurados, y otros que no tanto. Por ejemplo, en lo tocante al tema de la condución, decir que es totalmente arcade, tanto lo que es el moverse por el mapa, como lo que son los combates con otros vehículos; además se presta mucho a "hacer el cabra".  Podremos usar el turbo,  aprovechar el entorno o rampas para realizar saltos espectaculares. En este aspecto se puede decir que Avalanche Studios hizo un trabajo soberbio. A parte de la conducción, los combates contra otros vehículos son geniales, pudendo hacer embestidas para destrozarlos, usando armas que iremos adquiriendo y mejorando durante el desarrollo del juego.


 


![MadMax04](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisMadMax/MadMax04.webp)*Acabar con los convoyes enemigos bajará el nivel de amenaza* 
Si la condución es excelente, no se puede decir lo mismo de los combates cuerpo a cuerpo, donde la gestión que hace la cámara puede jugarnos alguna que otra mala pasada. Además da la impresión de que, por ejemplo, los rechaces o los remates no son todo lo precisos que deberían cuando pulsamos las teclas correspondentes. Aun así se le acába pillando el punto y estos son más llevaderos.


Los escenarios también pueden pecar de repetitivos, pero teniendo en cuenta el mundo donde nos movemos, es hasta cierto punto completamente normal. Digamos que un desierto da para bien poco en cuanto a variedad del entorno. Aún así pienso que el retrato que hacen de este es fantástico y inmersivo. En cuanto al aspecto de las estructuras y personajes también son muy repetitivas. En las primeiras, a excepción de las fortalezas y alguna localización que otra son todas semejantes. Del lado de los personajes, excepto los que entran en la historia, son todos parecidos, por ejemplo los NPC's.


 


![MadMax10](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisMadMax/MadMax10.webp)


*El sistema de combates, sin ser malo es bastante mejorable*


 


Hablando de los enemigos, también son todos muy iguales en aspecto, y hay poca diferencia entre ellos a la hora de realizar ataques. Especialmente sangrante es el caso de los Jefes de cada región, que más o menos son todos semejantes y atacan de la misma forma (por lo menos hasta donde yo llegué): Un fulano enorme y calvo con una maza o algo similar en la mano que por norma general embiste como un toro y solo tenemos que esquivarlo y atacarlo por la espalda o con armas. La dificultad basicamente radica en el número y el tipo de séquito que lleve.


En cuanto el tema de las misiones, decir que excepto ciertas principales, son poco inspiradas y no son muy variadas. Estas siguen el típico cliché de ser "el chico de los recados" del resto de personajes. La historia también no es un derroche de originalidad, aunque en lineas generales el argumento es bastante convincente y tiene algún que otro giro, que aunque no es imprevisible, si consigue engancharnos para saber que va a ocurrir después.


 


![MadMax09](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisMadMax/MadMax09.webp)
*Las latas de comida para perros son lo menos asqueroso que vamos a comer*



A nivel técnico, a pesar de los elevados requisitos técnicos del título, se puede decir que Feral Interactive hizo un buen trabajo, ya que el juego se mueve con soltura en un equipo medio, habiendo solamente muy de vez en cuando alguna bajada de frames poco importante. Por poner un ejemplo, en mi equipo, que es bastante normalito (i5 a 3.3Ghz, GTX950, 16GB de RAM), el juego en calidad media-alta no baja nunca de los 60 FPS y la media es más o menos de 70-80 FPS , por lo que no tenemos que tener miedo de que el rendimiento sacrifique la jugabilidad.


También encontré una incidencia con el Steam Controller, ya que este tiene un problema con la configuración por defecto, la cual hace que no podamos usar la cámara como si de un ratón se tratase mientras nos estamos moviendo. Según el soporte de Feral esto se soluciona remapeando a mano todos los controles. No os puedo decir que esta solución funcione porque al final conseguí jugar utilizando el control de cámara como un trackball, y me acostumbré a esa forma de jugar.


Pero no nos dejemos llevar por la negatividad, ya que en lineas generales estamos ante **un muy buen juego** que no defraudará a casi nadie; y las ocasiones de disfrutar de un juego de este calibre en nuestros escritorios, por ahora, tenemos pocas. En Mad Max tenemos la posibilidad de jugar horas y horas para completar todos los retos y localizaciones por expoliar o liberar, de conducir libremente por el páramo, de enzarzarnos en combates con nuestro coche... En lineas generales estamos ante un juego que sin llegar a la excelencia consigue capturar al jugador con su ambientación, capacidad de personalización y ciertas mecánicas de juego muy bien conseguidas.


ACTUALIZO:


Os dejo con este fantástico video realizado por [Peka](https://www.youtube.com/channel/UCuLftNXu4ElrhYsqQRjHTlg), para que os hagais una idea de como es y se mueve el juego en Linux:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/n43-9aoprWM" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


Si quieres comprar este juego puedes pasarte por la Tienda de Feral:


<https://store.feralinteractive.com/es/mac-linux-games/madmax/>


También puedes comprarlo en Steam:


 


<div class="steam-iframe"><iframe height="190" seamless="seamless" src="https://store.steampowered.com/widget/234140/" width="646"></iframe></div>


 


 


Puedes ver este artículo en Gallego en mi Blog Personal, [O BLOG DE LEO](http://www.oblogdeleo.es/?p=1543)


 

