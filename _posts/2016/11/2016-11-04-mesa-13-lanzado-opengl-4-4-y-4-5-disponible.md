---
author: leillo1975
category: Software
date: 2016-11-04 10:50:00
excerpt: "<p>Gracias a este desarrollo los usuarios de drivers libres disponen de\
  \ soporte total en OpenGL. AMD permite adem\xE1s Vulkan</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/3749aaa8ee129d7e919bddcc7e09cd36.webp
joomla_id: 86
joomla_url: mesa-13-lanzado-opengl-4-4-y-4-5-disponible
layout: post
tags:
- vulkan
- opengl
- amd
- intel
- gallium3d
- mesa
- nvidia
title: Mesa 13 lanzado, OpenGL 4.4 y 4.5 disponibles
---
Gracias a este desarrollo los usuarios de drivers libres disponen de soporte total en OpenGL. AMD permite además Vulkan

En primer lugar me gustaría presentarme, soy **leillo1975**, actualmente también "regento" un blog sobre Linux en mi idioma, el gallego ( [www.oblogdeleo.es](http://www.oblogdeleo.es/) ). Este es mi primer post aquí, por lo que no seais muy duros conmigo :). Bueno, al lio:


El pasado día 1 de Noviembre fué lanzado oficialmente Mesa 13 tras muchos meses de desarrollo como la rama 12.1, lo cual es un hito en la historia de los drivers y librerías gráficas libres por múltiples razones. Las más significativas son las siguientes:


**Soporte completo OpenGL 4.5 para Intel, Radeon y Nvidia:** En el caso de **Intel** podrán disfrutar de soporte todas aquellas gráficas a partir de Broadwell en adelante (i3,i5,i7 serie 5xxx y superiores) . La arquitectura anterior, Haswell, en cambio permanecerá en OpenGL 3.3. En el caso de las gráficas **AMD,** el soporte está permitido gracias al driver "RadeonSI" para las tarjetas AMD GCN (HD 7700-7800, HD 8xxx y superiores); y para **Nvidia** con "Nouveau" para todas las arquitecturas superiores a Fermi (series 4xx en adelante). Tanto AMD como Nvidia, aunque en la práctica tenga soporte para 4.5, **oficialmente solo tienen soporte hasta OpenGL 4.3**, pues están pendientes que los drivers sean validados por los tests del grupo Khronos.


**RADV Vulkan:** Se ha añadido soporte Vulkan a las gráficas AMD gracias al driver libre "RADV". Este ha sido incluído recientemente en la rama principal de Mesa. Se trata de un desarrollo comunitario independiente al driver libre que en teoría estaría desarrollando AMD, y que comenzó este verano **David Airlie** de Red Hat. 


**Soporte del driver Nouveau para tarjetas Pascal:** se trata de un desarrollo inicial del soporte, ya que están pendientes de que Nvidia les facilite las imagenes de Firmware firmadas para poder avanzar.


**Múltiples opctimizaciones en el rendimiento de los drivers:** Gracias a Mesa 13, los usuarios experimentaran mejoras en la velocidad, especialmente con el driver "RadeonSI" para AMD.


Entre otras cosas también se ha mejorado mucho el **driver para Vulkan de Intel**, o el desarrollo de **Gallium 9**, que permite una mejor comunicación con aplicaciones DirectX 9 a través de Wine. Como veis es un lanzamiento que viene plagado de novedades y que va a alegrar mucho a los usuarios de drivers libres, especialmente a los de AMD, que hasta ahora venían sufriendo el no poder jugar en condiciones con sus tarjetas. Por supuesto que aun queda mucho trabajo por hacer, pero este lanzamiento es un comienzo muy esperanzador... quien sabe, si incluso en un futuro los usuarios, como yo, de Nvidia nos pasemos al driver libre.


 


 


 


{AimyChartsBar}
 Hatshepsut (SS1HD); FPS Vulkan; 118.7; #900
 Hatshepsut (SS1HD); FPS OpenGL; 112.1; #390
 Karnak Temple (SS1HD); FPS Vulkan; 111.2
 Karnak Temple (SS1HD); FPS OpenGL; 103.6
 Thebes-Luxor (SS1HD); FPS Vulkan; 139.6
 Thebes-Luxor (SS1HD); FPS OpenGL; 125.3
 Sierra de Chiapas (SS2HD); FPS Vulkan; 103.0
 Sierra de Chiapas (SS2HD); FPS OpenGL; 101.0
 Ziggurat (SS2HD); FPS Vulkan; 131.4
 Ziggurat (SS2HD); FPS OpenGL; 123.9
 Tower of Babel (SS2HD); FPS Vulkan; 137.5
 Tower of Babel (SS2HD); FPS OpenGL; 137.6 
{/AimyChartsBar}

