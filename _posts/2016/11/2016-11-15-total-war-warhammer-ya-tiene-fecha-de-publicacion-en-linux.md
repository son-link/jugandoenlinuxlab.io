---
author: Pato
category: Estrategia
date: 2016-11-15 21:15:47
excerpt: "<p>El 22 de Noviembre prep\xE1rate para las \xE9picas batallas de este grand\xED\
  simo juego gracias a Feral Interactive.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/063ee9aeb9f60efa02823e51450f82ce.webp
joomla_id: 107
joomla_url: total-war-warhammer-ya-tiene-fecha-de-publicacion-en-linux
layout: post
tags:
- proximamente
- feral-interactive
- estrategia
title: "'Total War: WARHAMMER' ya tiene fecha de publicaci\xF3n en Linux"
---
El 22 de Noviembre prepárate para las épicas batallas de este grandísimo juego gracias a Feral Interactive.

"El 22 de Noviembre dos fuerzas colosales se unirán en Total War: WARHAMMER en Linux". Así reza el [anuncio](http://www.feralinteractive.com/es/news/698/) que ha hecho público Feral en su web donde ya puedes visitar el "[minisite](http://www.feralinteractive.com/es/games/warhammertw/)" del juego.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" seamless="seamless" src="https://www.youtube.com/embed/3dDxQnWxDfs" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


Recordaros que mañana mismo tal y [como ya os contamos](item/87-feral-retransmitira-una-sesion-de-total-war-warhammer-en-linux-el-proximo-16-de-noviembre) en Jugando En Linux Feral retransmitirá una sesión del juego corriendo en Linux en su canal de Twitch.


¿Estás interesado en Total War: WARHAMMER? ¿Piensas ver la sesión del juego de Feral? ¿Te gustaría poder ver esta sesión (y otras) en Jugando En Linux, y poder comentarlas en español?


Cuéntamelo en los comentarios.

