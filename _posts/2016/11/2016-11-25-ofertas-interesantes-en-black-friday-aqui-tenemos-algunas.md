---
author: Pato
category: Editorial
date: 2016-11-25 17:52:17
excerpt: "<p>Algunas ofertas que pueden ser interesantes si est\xE1s pensando en \"\
  pillar\" algo este viernes. \xBFAlguien dijo 'Tomb Raider' a un precio de risa?</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/8da476f72f06a276b1f930cdb28c21f1.webp
joomla_id: 125
joomla_url: ofertas-interesantes-en-black-friday-aqui-tenemos-algunas
layout: post
tags:
- software
- hardware
title: "\xBFOfertas interesantes en 'Black Friday'? Aqu\xED tenemos algunas"
---
Algunas ofertas que pueden ser interesantes si estás pensando en "pillar" algo este viernes. ¿Alguien dijo 'Tomb Raider' a un precio de risa?

**Hardware:**


[Steam Link al 60%](http://store.steampowered.com/app/353380)


[Steam Controller al 30%](http://store.steampowered.com/app/353370/)


[NVIDIA GTX 1070 al 16%](https://www.pccomponentes.com/asus-rog-strix-geforce-gtx-1070-gaming-oc-8gb-gddr5)


 


En GameStop están promocionando las Steam Machines... lástima que no tengan tiendas en España:


<http://www.gamestop.com/browse?nav=16k-steam+machine>


Una Alienware con i7 y 1TB por 374,99$ o una con i3 y 500GB por 224,99$ no se ven todos los días.


**Juegos:**


En la [tienda de Feral Interactive](https://store.feralinteractive.com/en/offers/) están de rebajas hasta el día 29 con descuentos en sus grandes juegos, incluida la oferta de Tomb Raider ¡por solo 4,99€! Si no lo tienes es por que no quieres. Merece mucho la pena por este precio.


[Don't Starve MEGA PACK - 66%](http://store.steampowered.com/bundle/312/)


[Metro Redux Bundle - 75%](http://store.steampowered.com/sub/44169/)


[Borderlands 2 Game of the Year - 78%](http://store.steampowered.com/sub/32848/)


[Cities: Skylines - 75%](http://store.steampowered.com/app/255710/)


[Dying Light: The Following - Enhanced Edition - 50%](http://store.steampowered.com/app/239140/)


[Broforce - 75%](http://store.steampowered.com/app/274190/)


[Total War: ATTILA - 75%](http://store.steampowered.com/app/325610/)


[Insurgency - 80%](http://store.steampowered.com/app/222880/)


Estos son solo unos cuantos ejemplos pero seguro que hay más. ¿Has encontrado algún chollo o ganga que merezca la pena? ¿Has comprado ya algo?


Cuéntame tus compras o las ofertas que has visto en los comentarios.


 


Este artículo está basado en uno de [GamingonLinux](https://www.gamingonlinux.com/articles/some-nuts-black-friday-deals-for-linux-gamers-to-make-note-of.8600).

