---
author: leillo1975
category: Hardware
date: 2016-11-27 21:42:23
excerpt: <p>Nvidia recomienda volver a un driver anterior.</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/ae265ec8d02add74bcb0f72e47ec001b.webp
joomla_id: 127
joomla_url: problemas-con-el-ultimo-driver-estable-de-nvidia-375-20
layout: post
tags:
- drivers
- nvidia
- deus-ex
title: "Problemas con el \xFAltimo driver estable de Nvidia (375.20)"
---
Nvidia recomienda volver a un driver anterior.

El otro día os informaba de la salida de una [nueva versión](item/103-nuevo-driver-nvidia-375-20-estable-lanzado) estable del driver privativo de Nvidia, que incluía numerosas mejoras y novedades. Hoy os tengo que informar de que [Nvidia está recomendando](https://devtalk.nvidia.com/default/topic/977518/linux/problems-with-multiple-opengl-applications-running-simultaneously-with-375-20-on-a-gtx970/post/5024978/#5024978) que si teneis problemas en los juegos y aplicaciones, tales como paradas, pantallazos negros, salidas a escritorio o rendimientos deficientes, hagais un downgrade a una versión anterior, como la 375.10 (Beta) o directamente a la anterior estable (370.28). En mi caso opté por la segunda por la facilidad de instalación que me otorga el [PPA oficial de Ubuntu](https://launchpad.net/~graphics-drivers/+archive/ubuntu/ppa). Tomé esta decisión por problemas detectados en **Deus Ex: Mankind Divided,** los cuales me producían cuelgues o salidas a escritorio. Tengo que decir que desde que bajé la versión del controlador no he vuelto a tener más problemas con este juego (...y le he dado de lo lindo).


Esto solo es un aviso para los que tengais problemas con el driver, ya que igual que hay gente que los está reportando, también hay muchos usuarios a los que todo le va correctamente, por lo que dicho downgrade no sería necesario.


 


**ACTUALIZO:** El equipo de soporte de Feral me ha confirmado que el problema que tenía con DEUS EX: Mankind Divided es por culpa de esta versión del driver.

