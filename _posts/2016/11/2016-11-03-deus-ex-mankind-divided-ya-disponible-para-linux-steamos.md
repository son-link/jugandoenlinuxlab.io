---
author: Pato
category: "Acci\xF3n"
date: 2016-11-03 19:20:36
excerpt: "<p>Poco despu\xE9s de su publicaci\xF3n en otros sistemas ya tenemos disponible\
  \ este port por parte de Feral.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/2cebfdae7a8ea5d691033c085990a9d4.webp
joomla_id: 85
joomla_url: deus-ex-mankind-divided-ya-disponible-para-linux-steamos
layout: post
tags:
- accion
- steamos
- steam
- feral-interactive
title: Deus Ex Mankind Divided ya disponible para Linux/SteamOS
---
Poco después de su publicación en otros sistemas ya tenemos disponible este port por parte de Feral.

![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/DXMD_Mac___Linux.webp)


Ayer [publicábamos](item/66-deus-ex-mankind-divided-ya-tiene-requisitos-minimos-y-recomendados) los requisitos mínimos de Deus Ex Mankind Divided y tal y como prometieron hoy ya está disponible en Linux/SteamOS. Parecía que este año ya teníamos copado el número de juegos triple A que ibamos a recibir con Mad Max siendo el último en llegar pero este Deus Ex viene a engordar la lista.


Desde [GoL ya nos avisan](https://www.gamingonlinux.com/articles/deus-ex-mankind-divided-released-for-linux-port-report-and-review.8457) que el juego es una bestia en cuanto a consumo de CPU y GPU, por lo que más vale tener una buena máquina e incluso un buen disco duro ya que el juego ocupa la friolera de 55 Gigas. Recordar además que el juego solo tiene soporte oficial para tarjetas Nvidia y drivers privativos 367.57.


En cuanto a rendimiento, según [las pruebas](https://www.gamingonlinux.com/uploads/articles/article_images/1478102143win980average.png) hechas por Liam los resultados son inapelables. Windows gana por un holgado margen al port de Linux, sin embargo no es nada que no esperásemos ya que por norma general los drivers gráficos para windows suelen llevar mejoras en las actualizaciónes para este tipo de juegos. Aún así, el juego es vastante jugable a tenor de los comentarios publicados en diversos sitios.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" seamless="seamless" src="https://www.youtube.com/embed/UHnOGul8Xhc" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


Y tu, ¿piensas jugara Deus Ex Mankind Divided? si es así ¿que tal te va? ¿Que te parece el rendimiento y el soporte oficial solo a los drivers de Nvidia?


Cuéntamelo en los comentarios.


Puedes comprar Deus Ex Mankind Divided en la [tienda de Feral](https://store.feralinteractive.com/es/mac-linux-games/deusexmd/) y apoyar así diréctamente a los responsables del port o directamente en Steam:


<div class="steam-iframe"><iframe height="190" seamless="seamless" src="https://store.steampowered.com/widget/337000/" width="646"></iframe></div>


 

