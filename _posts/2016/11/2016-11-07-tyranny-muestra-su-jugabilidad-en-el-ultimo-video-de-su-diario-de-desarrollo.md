---
author: Pato
category: Rol
date: 2016-11-07 17:48:36
excerpt: "<p>El nuevo juego de Obsidian ser\xE1 publicado ma\xF1ana mismo con soporte\
  \ en Linux.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/245effadf41c6129f4fe7accc564ef86.webp
joomla_id: 89
joomla_url: tyranny-muestra-su-jugabilidad-en-el-ultimo-video-de-su-diario-de-desarrollo
layout: post
tags:
- proximamente
- rol
- estrategia
title: "Tyranny muestra su jugabilidad en el \xFAltimo v\xEDdeo de su diario de desarrollo"
---
El nuevo juego de Obsidian será publicado mañana mismo con soporte en Linux.

Tyranny es el nuevo juego de Rol masivo que está siendo desarrollado por la compañia detrás de juegos como [Pillars of Eternity](http://store.steampowered.com/app/291650/?snr=1_7_7_151_150_1), y que será lanzado mañana mismo. Obsidian publicó este fin de semana el último vídeo de su diario de desarrollo donde se puede ver una buena muestra de su jugabilidad.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" seamless="seamless" src="https://www.youtube.com/embed/Te_ZLdC_Wmc" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


#### Sinopsis del juego:


*En Tyranny, la gran guerra entre el bien y el mal ha llegado a su fin, y las fuerzas del mal, lideradas por el Señor Supremo Kyros, han salido victoriosas. Los ejércitos implacables del Señor Supremo dominan el mundo y sus habitantes deben encontrar su lugar en este reino asolado por la guerra... incluso mientras la discordia empieza a aflorar entre las filas de los arcontes más poderosos de Kyros.*   
*Los jugadores descubrirán este nuevo mundo bajo el poder del Señor Supremo, en un juego de rol (RPG) reactivo, interactuando con los habitantes y encarnando a un poderoso forjadestinos que trabaja al servicio del Señor Supremo; este forjadestinos irá recorriendo las tierras para inspirar lealtad y miedo mientras va controlando los últimos resquicios de resistencia en las Cotas.*


Tyranny será publicado con soporte en Linux desde su lanzamiento y con subtítulos y menús traducidos al español.


Puedes precomprar Tyranny en [GOG](https://www.gog.com/game/tyranny_commander_edition_preorder) o en Steam:


<div class="steam-iframe"><iframe height="190" seamless="seamless" src="https://store.steampowered.com/widget/362960/" width="646"></iframe></div>

