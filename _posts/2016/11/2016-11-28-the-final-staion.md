---
author: Serjor
category: Aventuras
date: 2016-11-28 22:09:53
excerpt: "<p>The Final Station llega a la estaci\xF3n Linux, dispuesto a hacer lo\
  \ que haga falta por llegar a su destino</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/7e64c4d2a4a242251ffdaa790b21fa01.webp
joomla_id: 129
joomla_url: the-final-staion
layout: post
title: The Final Station
---
The Final Station llega a la estación Linux, dispuesto a hacer lo que haga falta por llegar a su destino

El pasado 24 de noviembre llegó al sistema del pingüino de la mano de [tiniBuild](http://www.tinybuild.com) el juego The final Station, donde encarnaremos a un maquinista que hará lo que sea necesario para llegar a su destino, como recoger materiales para que el tren siga funcionando, o acabar con quién nos impida seguir con nuestro trayecto...


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/k1gmBupfIZg" width="560"></iframe></div>


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/435530/" width="646"></iframe></div>


 

