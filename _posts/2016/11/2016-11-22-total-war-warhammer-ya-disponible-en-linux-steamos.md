---
author: Pato
category: Estrategia
date: 2016-11-22 18:44:49
excerpt: "<p>Feral Interactive est\xE1 que no para. Esta vez nos traen el \xFAltimo\
  \ t\xEDtulo de la serie Total War. \xA1Que siga la fiesta!</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/73608782f50eb6af17bb69bdcd662692.webp
joomla_id: 117
joomla_url: total-war-warhammer-ya-disponible-en-linux-steamos
layout: post
tags:
- steamos
- steam
- feral-interactive
- estrategia
title: 'Total War: WARHAMMER ya disponible en Linux/SteamOS'
---
Feral Interactive está que no para. Esta vez nos traen el último título de la serie Total War. ¡Que siga la fiesta!

Si algo está claro es que los usuarios que jugamos en Linux/SteamOS le debemos mucho a Valve, pero no menos a Feral Interactive. Ellos solitos están haciendo méritos para hacerse un hueco en el minoritario mundo del juego en Linux.


Y no es para menos. Tras ports de juegos AAA como "Company of Heroes", "Mad Max", "Tomb Raider" o el último "Deus Ex Mankind Divided" entre otros los chicos de Feral nos traen un título de última hornada con este "Total War: WARHAMMER".


Todo el que lo ha probado dice lo mismo: es un gran juego, digno de la serie Total War pero los comentarios de los usuarios son variados sobre todo por la política de DLCs de Creative Assembly que hace que para tener todas las razas y características sea necesario invertir un buen dinero aparte de pagar el juego base a 60€.


De todos modos, lo que nos ocupa es el port que Feral ha hecho. Según los [tests que Liam de GoL](https://www.gamingonlinux.com/articles/total-war-warhammer-released-for-linux-port-report-and-video.8573) ha estado haciendo el rendimiento está dentro de lo esperado de un port de windows con DirecX a OpenGL, con una pérdida de entre un 20 o 25% de rendimiento. Esperemos que la cosa vaya cambiando con la estandarización de Vulkan.


Aún así, el juego es áltamente jugable y recomendable si te gustan los juegos de estrategia tipo Total War.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" seamless="seamless" src="https://www.youtube.com/embed/YJyU1dTaarE" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


#### Sinopsis del juego:


*El clamor de interminables batallas resuena en el Viejo Mundo. ¡La guerra es la única constante!*  
   
 *Total War: WARHAMMER es un juego de estrategia y fantasía de proporciones legendarias. Combina una adictiva campaña por turnos para levantar un imperio con colosales y explosivas batallas en tiempo real en el intenso e increíble mundo de Warhammer Fantasy Battle.*  
   
 *Controla cuatro razas distintas: el Imperio, los Enanos, los Condes Vampiro y los Pieles Verdes, cada una con sus propios personajes, unidades de combate y estilo de juego.*  
   
 *Lleva a tus tropas a la guerra encarnando a uno de los ocho Señores Legendarios del mundo de Warhammer Fantasy Battle, y equípalos con míticas armas, armaduras y magia de combate obtenidas en misiones encadenadas individuales.*  
   
 *Por primera vez en un juego de Total War, puedes usar poderes mágicos en combate y surcar los cielos con criaturas voladoras, desde feroces dragones y serpientes aladas hasta grifos gigantes.*


 


Puedes comprar Total War:WARHAMMER [en la tienda de Feral](https://store.feralinteractive.com/en/mac-linux-games/warhammertw/) y así apoyarás a la compañía que ha hecho posible que podamos jugar a este gran título en Linux/SteamOS o comprarlo diréctamente en su página de Steam:


<div class="steam-iframe"><iframe height="190" seamless="seamless" src="https://store.steampowered.com/widget/364360/" width="646"></iframe></div>


¿Que te parece este Total War:WARHAMMER? ¿Piensas jugarlo? ¿Que te parece la política de DLCs de Creative Assembly?


Cuéntamelo en los comentarios.


¿Tienes el juego y quieres que publiquemos tu review? ¡[Mándanosla](contacto/envia-un-articulo)!

