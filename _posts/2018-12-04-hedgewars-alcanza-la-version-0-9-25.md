---
author: leillo1975
category: "Acci\xF3n"
date: 2018-12-04 14:34:40
excerpt: "<p>Se ha lanzado oficialmente la versi\xF3n 0.9.25 de @Hedgewars</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/e5d6b2287375a08cd0c4ee7d5ab1a8fc.webp
joomla_id: 920
joomla_url: hedgewars-alcanza-la-version-0-9-25
layout: post
tags:
- open-source
- codigo-abierto
- hedgewars
title: "Nueva versi\xF3n del juego libre Hedgewars"
---
Se ha lanzado oficialmente la versión 0.9.25 de @Hedgewars

Ultimamente han aparecido montones de noticias de juegos Open Source que se actualizan, como [Speed Dreams](index.php/homepage/generos/simulacion/14-simulacion/994-speed-dreams-disponible-en-flatpak), [SuperTuxKart](index.php/homepage/generos/carreras/2-carreras/1022-los-desarrolladores-de-supertuxkart-buscan-betatesters-para-el-juego-en-red), [YORG](index.php/homepage/generos/carreras/2-carreras/1008-el-juego-libre-yorg-alcanza-la-version-0-10), [OpenRa](index.php/homepage/generos/estrategia/8-estrategia/715-la-actualizacion-de-openra-nos-trae-la-campana-harkonnen-completa),  [SuperTux](index.php/homepage/generos/plataformas/3-plataformas/1038-el-equipo-de-supertux-necesita-testers-para-su-version-0-6)...Para que luego digan que los juegos libres están muertos... El caso es que dándome un garveo por [Reddit](https://www.reddit.com/r/linux_gaming/) he visto que uno de los juegos que más he disfrutado y disfruto, especialmente con "mis vástagos" es Hedgewars, que ayer mismo se actualizaba a la versión [0.9.25](https://www.hedgewars.org/node/7024).


Para quien no conozca Hedgewars, hay que decir que se trata de un **clon libre del conocido Worms** , solo que utilizando erizos en vez de gusanos, pero manteniendo la misma jugabilidad endiablada y sus dosis de diversión y humor intactas. Este juego, de estrategia por turnos nos permitirá librar batallas entre nuestros equipos de erizos con las más diversas y disparatadas armas y herramientas, y teniendo muy en cuenta las ventajas (o desventajas) del terreno donde nos haya tocado guerrear.


Las modificaciones en esta última versión son importantes como se puede ver en [su lista de cambios](http://hg.hedgewars.org/hedgewars/file/0.9.25-release/ChangeLog.txt), pero se pueden resumir en las siguientes:



> 
> *-Revisión completa de los suministros Continentales*  
>  *-Puede ajustar las probabilidades de inicio del arma y de la caja en "Balanced Random Weapon"*  
> *-Muchos ajustes más pequeños para las armas existentes*  
>  *-Quitado el pato de goma*  
>  *-Remodelación de los rankings de los equipos*  
>  *-Los equipos empatados ahora tienen el mismo rango*  
>  *-Botón de ayuda en el menú principal*  
>  *-19 nuevas burlas de erizos*  
>  *-Muchas de las nuevas características de la API de Lua*  
>  *-Corrección de errores: Restablecimiento de la funcionalidad de los controladores*  
> *Corregir al menos 2 fallos*  
> *Corregidos algunos errores de red que causaban que los juegos se paralizaran.*  
> *Se han corregido muchos errores relacionados con la envoltura del borde del mundo (pero no todos).*  
> *La muerte súbita siempre llegaba exactamente 1 turno después de lo planeado.*
> 
> 
> 


Los desarrolladores también se enorgullecen de anunciar que esta probablemente sea la última versión antes de la 1.0, por lo que se muestran muy emocionados. También es importante decir que por ahora , si queremos disfrutar de esta ultimísima versión, solo es posible [descargar su código fuente y compilarlo](https://www.hedgewars.org/download.html), pero probablemente en unos días veamos actualizados estos paquetes en los correspondientes gestores, y en sitios como Flathub y Snapcraft. Mientras compilais podeis disfrutar de este original trailer:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/u1Y_Pw_TtFo" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 

