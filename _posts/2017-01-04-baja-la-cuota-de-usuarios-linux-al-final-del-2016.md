---
author: Serjor
category: Editorial
date: 2017-01-04 11:01:59
excerpt: "<p>Parece que los linuxeros nos hemos dedicado m\xE1s al turr\xF3n que a\
  \ los videojuegos</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/500a44935c8320008f1c713a63e32b8e.webp
joomla_id: 168
joomla_url: baja-la-cuota-de-usuarios-linux-al-final-del-2016
layout: post
tags:
- steam
- estadisticas
title: Baja la cuota de usuarios Linux al final del 2016
---
Parece que los linuxeros nos hemos dedicado más al turrón que a los videojuegos

Leemos en [phoronix](http://www.phoronix.com/scan.php?page=news_item&px=December-2016-Steam-Linux-Stats) que la cuota de usuarios de linux en Steam ha caído un 0.01% respecto al mes anterior, dejando el total de usuarios linux en un 0.87% de la cuota total.  
Esperemos que este año con la llegada de Vulkan el rendimiento y el catálogo de juegos en Linux esea mayor y atraiga a más usuarios a nuestra plataforma.


Puedes encontrar la lista completa de estadísticas [aquí](http://store.steampowered.com/hwsurvey/).


  
Y tú, ¿cómo ves el futuro de Linux como plataforma de videojuegos? Puedes compartir tu opinión en los comentarios, el [foro](index.php/foro/), el grupo de [Telegram](https://telegram.me/jugandoenlinux) o en el canal de [Discord](https://discord.gg/fgQubVY).

