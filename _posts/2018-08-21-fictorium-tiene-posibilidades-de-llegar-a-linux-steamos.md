---
author: Pato
category: Rol
date: 2018-08-21 17:39:45
excerpt: "<p>Se trata de un juego de rol y acci\xF3n en tercera persona donde podemos\
  \ destruir el entorno</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/43cb3b26768b82a0e3ec1fb837718565.webp
joomla_id: 837
joomla_url: fictorium-tiene-posibilidades-de-llegar-a-linux-steamos
layout: post
tags:
- accion
- indie
- rol
- rumor
title: '''Fictorium'' tiene posibilidades de llegar a Linux/SteamOS'
---
Se trata de un juego de rol y acción en tercera persona donde podemos destruir el entorno

Si de algo no vamos sobrados en Linux es de juegos de rol y acción en tercera persona. Salvo honrosas excepciones, es un género poco habitual en nuestro sistema favorito.


Ahora, gracias a [SteamDB](https://steamdb.info/app/503620/history/?changeid=4966408) y también a [gamingonlinux.com](https://www.gamingonlinux.com/articles/fictorum-the-action-rpg-with-destructible-environments-looks-like-its-now-on-the-way-to-linux.12383) sabemos que Fictorium tiene posibilidades de llegar a Linux/SteamOS.


Se trata de un RPG de acción que presenta entornos destructibles, un mapa del mundo procedural basado en nodos y una gestión dinámica de la magia.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/5IuPpNOF-TM" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


*Un reino de terror nunca es tan satisfactorio como cuando dejas literalmente una pila de escombros a tu paso. Dando a los brujos un cambio de imagen muy necesario, Fictorum es un juego de rol de acción que cuenta con estructuras totalmente destructibles, un mapa del mundo aleatorio basado en nodos y un sistema de magia con ajuste y personalización de hechizos sobre la marcha.*


Como puede verse en el reporte de SteamDB, el juego cuenta con repositorio para Linux donde puede verse cierta actividad reciente.


Estaremos atentos a las novedades respecto a un hipotético lanzamiento del título para Linux/SteamOS.


Si quieres saber mas, puedes visitar la [web oficial](http://www.fictorum.com/) del juego o su [página de Steam](https://store.steampowered.com/app/503620/Fictorum/).

