---
author: leillo1975
category: "Simulaci\xF3n"
date: 2018-08-29 07:28:56
excerpt: "<p class=\"ProfileHeaderCard-screenname u-inlineBlock u-dir\" dir=\"ltr\"\
  ><span class=\"username u-dir\" dir=\"ltr\">@ProModsETS2 incluye importantes cambios\
  \ y a\xF1adidos con respecto al anterior lanzamiento.</span></p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/4602f2f5153477027a754713438bf3d0.webp
joomla_id: 844
joomla_url: el-fantastico-promods-de-euro-truck-simulator-2-alcanza-la-version-2-30
layout: post
tags:
- ets2
- euro-truck-simulator-2
- mod
- promods
title: "El fant\xE1stico ProMods de Euro Truck Simulator 2 alcanza la versi\xF3n 2.30."
---
@ProModsETS2 incluye importantes cambios y añadidos con respecto al anterior lanzamiento.

Cuando está a punto de cumplirse un año desde su [lanzamiento anterior](index.php/homepage/generos/simulacion/item/575-promods-2-20-el-mejor-mod-de-ets2-ya-esta-aqui) (2.20), [MandelSoft](https://www.youtube.com/user/MandelSoft), que así se llama el grupo de modders que trabajan en este proyecto nos traen una nueva y vitaminada versión del posiblemente mejor Mod de mapas para el incombustible [Euro Truck Simulator 2](index.php/buscar?searchword=Euro%20Truck%20Simulator%202&ordering=newest&searchphrase=all). En esta ocasión los añadidos a su ya extenso trabajo son notables e incluyen multitud de nuevos paises, cuidades, carreteras, paisajes.... Incluso podemos ver **nuevas urbes en España**, que se estrenó en las dos anteriores versiones. La lista de cambios es importante y son los siguientes:



> 
> **Cambios del Mapa:**  
>  Bulgaria: Монтана (Montana), Плевен (Pleven), Видин (Vidin)  
>  Croacia: Senj, Šibenik, Split, Zadar  
>  Chipre: Λάρνακα (Larnaca), Λεμεσός (Limmasol), Λευκωσία (Nicosia), Πάφος (Paphos)  
>  Dinamarca:  Padborg [reconstruido]  
>  France:  Lyon [reconstruido]  
>  Grecia: Ηγουμενίτσα (Igoumenitsa, puerto), Ιωάννινα (Ioannina), Πτολεμαΐδα (Ptolemaida), Θεσσαλονίκη (Thessaloniki)  
>  Italia: A4 Torino-Milano, A8-A9, Como (border). Milano [reconstruido]  
>  Lituania: Šiauliai, Ukmergė  
>  Macedonia: Битола (Bitola), Скопје (Skopje), Охрид (Ohrid)  
>  Holanda: Moerdijk. Rotterdam [reconstruido]  
>  Noruega: Grumantbyen, Hammerfest, Longyearbyen [mejorado]  
>  Polonia: Ełk. Suwałki [reconstruido]  
>  Serbia: Београд (Beograd), Крагујевац (Kragujevac), Ниш (Niš)  
> **España**: Jaca, Pamplona, Soria, Teruel, Valencia, Vinaròs, Zaragoza  
>  Suecia: Kalix, Skellefteå, Ystad  
>  Suiza: Rebuilt: Bern  
>  Inglaterra: Croydon [reconstruido], London & M25  
>  Gales: Aberystwyth, Porthmadog  
>  General: Muchos bugs corregidos y mejoras de la version 2.27
> 
> 
> **Vehiculos:**  
>  General: Coches de Policía añadidos en Chipre, Grecia y Macedonia  
>   
> **Special Transport DLC:**  
>  General: Puedes activar el soporte en el generador DEF para rutas extra.
> 
> 
> 


Como veis no es moco de pavo, y pasarán muchas horas antes de que consigamos descubrirlas todas. En cuanto a los requisitos es importante decir que **es necesario tener la versión 1.31 oficial de ETS2 activada** para que el mod funcione, ya que con la beta actual ([1.32](https://youtu.be/MBGn5-Ltjl4)) que seguro que muchos de vosotros teneis instalada, no funciona aun y precisará de trabajo extra que veremos en las próximas seis semanas. También es necesario tener instalados las DLC's *[Going East!](https://www.humblebundle.com/store/euro-truck-simulator-2-going-east?partner=jugandoenlinux), [Scandinavia](https://www.humblebundle.com/store/euro-truck-simulator-2-scandinavia?partner=jugandoenlinux), [Vive la France!](https://www.humblebundle.com/store/euro-truck-simulator-2-vive-la-france-?partner=jugandoenlinux) e [Italia](https://www.humblebundle.com/store/euro-truck-simulator-2-italia?partner=jugandoenlinux).* Para poder **[descargarlo](https://promods.net/setup.php)** teneis dos posibilidades, la primera y **recomendable** es pagar un 1$ (0.85€) y bajar un único archivo a alta velocidad, o de forma gratuita en multiples partes y velocidad reducida.


En JugandoEnLinux es bastante probable que grabemos o emitamos un video en los próximos días mostrándoos algunas de las novedades de este genial mod. Mientras tanto podeis darnos vuestra opinión sobre ProMods o ETS2 en los comentarios o en nuestro canal de [Telegram](https://t.me/jugandoenlinux) y deleitaros con el Trailer (nunca mejor dicho):


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/C2NLTsCwIGU" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


Podeis comprar Euro Truck Simulator 2 en la tienda de **[Humble Bundle](https://www.humblebundle.com/store/euro-truck-simulator-2?partner=jugandoenlinux)** (patrocinado) o en [Steam](https://store.steampowered.com/app/227300/Euro_Truck_Simulator_2/).

