---
author: Pato
category: "Acci\xF3n"
date: 2017-01-25 18:33:41
excerpt: <p>Se trata de un juego de disparos en primera persona con elementos "roguelike"</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/dbe05350458c15fa6c802fb686391131.webp
joomla_id: 192
joomla_url: immortal-redneck-de-los-madrilenos-crema-games-esta-en-campana-en-greenlight
layout: post
tags:
- accion
- indie
- steam
- greenlight
- roguelike
title: "'Immortal Redneck' del estudio espa\xF1ol \"Crema Games\" est\xE1 en campa\xF1\
  a en Greenlight"
---
Se trata de un juego de disparos en primera persona con elementos "roguelike"

Seguimos con campañas de juegos. Esta vez se trata de 'Immortal Redneck' [[web oficial](http://www.immortalredneck.com/)] del estudio español [Crema Games](http://www.cremagames.com/) el que está en campaña en Greenlight. Lo he descubierto allí en mi repaso habitual y enseguida he buscado más información. En su Twitter anuncian su campaña:



> 
> We're on Greenlight! Come and see these beautifully handcrafted rooms <https://t.co/wpHhU4RDSw> [#gamedev](https://twitter.com/hashtag/gamedev?src=hash) [#indiedev](https://twitter.com/hashtag/indiedev?src=hash) [#indiegame](https://twitter.com/hashtag/indiegame?src=hash) [#madewithunity](https://twitter.com/hashtag/madewithunity?src=hash) [pic.twitter.com/XyTk0ifG9Y](https://t.co/XyTk0ifG9Y)
> 
> 
> — CremaGames (@CremaGames) [24 de enero de 2017](https://twitter.com/CremaGames/status/823972307114819586)



'Immortal Redneck' es un juego de disparos en primera persona que recuerda muchísimo a títulos como Quake Arena o Serious Sam con elementos "roguelike" y ambientado en Egipto. Según palabras del estudio en su campaña *"El juego mezcla la acción de los shooters en primera persona de la vieja escuela junto con mecánicas de rogue-lite. La jugabilidad frenética, los controles certeros y el sentimiento arcade se darán cita con las mazmorras procedurales, un completo árbol de habilidades, muerte permanente y nueve clases diferentes."*


Juzga por ti mismo:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/fUfLTsQOY9g" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


 'Immortal Redneck' promete soporte en Linux tanto en su web de Greenlight como en su web oficial. Teniendo en cuenta que lo están programando con Unity supongo que no les será dificil dar soporte en Linux.


Si te gusta lo que ves y quieres ayudarles, no dudes en pasarte por su web de Greenlight [en este enlace](http://steamcommunity.com/sharedfiles/filedetails/?id=834223670) y darle tu voto para que sea publicado en Steam.


¿Qué te parece este 'Immortal Redneck'? ¿Piensas apoyarlo en Greenlight?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux en [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

