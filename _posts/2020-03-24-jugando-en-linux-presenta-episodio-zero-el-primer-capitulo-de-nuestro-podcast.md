---
author: Jugando en Linux
category: Noticia
date: 2020-03-24 20:17:28
excerpt: "<p>Una nueva forma de llegar a vosotros</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Podcast/podcast_jel.webp
joomla_id: 1195
joomla_url: jugando-en-linux-presenta-episodio-zero-el-primer-capitulo-de-nuestro-podcast
layout: post
tags:
- podcast
- podcast-linux
- steam-play
- stadia
- episo-zero
- gforce-now
title: "Jugando En Linux presenta \"Episodio Zero\", el primer cap\xEDtulo de nuestro\
  \ podcast"
---
Una nueva forma de llegar a vosotros


Es difícil expresar en palabras la ilusión que nos hace el estreno de este episodio cero del podcast de JugandoEnLinux, pero realmente no os podéis imaginar las ganas que teníamos de poder materializar una asignatura pendiente, y que gracias a Rocket League nos animó a juntarnos y organizarnos.


Horarios, olvidos y falta de tiempo libre han retrasado algo que teníamos realmente ganas de hacer, y aunque aún falte cierto equipamiento que está en camino para mejorar la calidad del audio, hemos aprovechado y nos hemos juntados para grabar un episodio piloto y poder desempolvar así esos años de radio que alguno de los integrantes de este capítulo atesora.


Sin más os dejamos con el primer capítulo de vuestro podcast, el Episodio Zero, donde a pesar de nuestra inexperiencia, hemos intentado hacer algo que pueda resultaros interesante, y hablamos del impacto de Steam Play en el videojuego para GNU/Linux, qué supone Stadia y plataformas en la nube y Leillo nos cuenta porqué nos recomienda Shadow Of The Tomb Raider.


Podéis encontrarlo en [iVoox](https://www.ivoox.com/49270347) y en [YouTube](https://youtu.be/LvXeBLxXHqc).


Esperamos que os guste.


JugandoEnLinux

