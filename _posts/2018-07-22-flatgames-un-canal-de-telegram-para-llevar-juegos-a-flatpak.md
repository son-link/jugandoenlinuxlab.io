---
author: Serjor
category: Noticia
date: 2018-07-22 11:33:34
excerpt: "<p>El objetivo es simplificar y facilitar la instalaci\xF3n de juegos entre\
  \ distribuciones</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/23da450944f0818162562a06dc761501.webp
joomla_id: 807
joomla_url: flatgames-un-canal-de-telegram-para-llevar-juegos-a-flatpak
layout: post
tags:
- flatpak
- telegram
title: FlatGames, un canal de telegram para llevar juegos a flatpak
---
El objetivo es simplificar y facilitar la instalación de juegos entre distribuciones

El usuario SonLink de nuestro grupo de telegram ha creado un grupo para hablar, coordinar y gestionar cómo empaquetar en flatpak los diferentes juegos que tenemos disponibles para GNU/Linux.


La verdad es que es una iniciativa interesante, y una buena manera de colaborar con el ecosistema GNU/Linux, así que desde jugandoenlinux os animamos a que participéis y os paséis por el grupo, el cuál podéis encontrar en <https://t.me/joinchat/AHN6rA9n4RCxu55wAkJ4Ag>


 

