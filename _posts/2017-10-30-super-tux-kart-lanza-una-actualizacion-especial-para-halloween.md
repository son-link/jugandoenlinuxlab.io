---
author: leillo1975
category: Carreras
date: 2017-10-30 09:18:10
excerpt: <p>Se trata de la Release Candidate previa a su lanzamiento en Steam.</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/d37b40f764b14a89e2058a44f05a96b6.webp
joomla_id: 512
joomla_url: super-tux-kart-lanza-una-actualizacion-especial-para-halloween
layout: post
tags:
- open-source
- halloween
- super-tux-kart
- android
- release-candidate
title: "Super Tux Kart lanza una actualizaci\xF3n especial para Halloween (ACTUALIZADO)"
---
Se trata de la Release Candidate previa a su lanzamiento en Steam.

**ACTUALIZACIÓN 20-11-17:** Acabamos de leer en [Phoronix](https://www.phoronix.com/scan.php?page=news_item&px=SuperTuxKart-0.9.3) que el juego acaba de ser lanzado oficialmente, pues estaba en Release Candidate, y que incluye novedades como un nuevo grabador de pantalla y mejoras varias en el juego. Podeis encontrar la lista completa en la [página oficial](http://blog.supertuxkart.net/2017/11/supertuxkart-093-released.html).


 




---


El pasado día 28 de Octubre, un usuario de nuestro grupo de Telegram (**Francisco**), nos facilitó el enlace al blog donde se informaba de la salida de esta versión especial de Halloween del conocido juego libre [Super Tux Kart](https://supertuxkart.net). Desde JugandoEnLinux.com nos gustaría agradecer a este usuario el aviso, y de paso os animamos a que no os corteis en enviarnos noticias referentes a los juegos en Linux. Para ello podeis usar nuestro [correo electrónico de contacto](mailto:contacto@jugandoenlinux.com) o hacerlo como Francisco a través de [Telegram](https://t.me/jugandoenlinux).


 


Pero sigamos con lo que nos ocupa. En el artículo del blog comienzan hablando de como va el **proceso de inclusión del modo multiplayer**, donde comentan que muchos de los problemas han sido solucionados y que los parones durante el juego están corregidos; aunque aun queda mucho para hacer para que tenga todas las características que necesita. El desarrollo del juego en red está siendo elaborado en una rama separada, por lo que no encontraremos nada de esto en esta nueva versión. Sobre la versión de Halloween comentan que se trata de una **versión candidata previa al lanzamiento en Steam** del juego y contiene innumerables novedades desarrolladas durante el último año.


 


![SuperTuxKartCartelHalloween](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SuperTuxKart/SuperTuxKartCartelHalloween.webp)


 


Estas novedades incluyen **multitud de originales circuitos** entre los que encontramos el rediseño de la vieja "mansión Ravenbridge", el "parque de las calabazas", la gigantestca antena en "Alien Signal", el circuito de inspiración parisina "Candela City", para el modo batalla "Las Dunas Stadium", o un circuito en una granja llamado "Cornfield Crossing". También se han reformado los Karts Wilber, Hexley y Konqui, y añadido la mascota de Krita (Kiki). Por supuesto hay un montón de nuevas caracteristicas y correcciónes como el encendido automático de luces en los circuitos oscuros, un grabador de videos en el propio juego, cargas mucho más rápidas o varias mejoras gráficas. Si quereis tener un listado completo de todo lo se que incluye esta actualización de Halloween podeis ver [el anuncio del blog](http://blog.supertuxkart.net/2017/10/supertuxkart-halloween-update-released.html). Otra cosa interesante aunque no tenga que ver directamente con GNU/Linux es la inclusión del juego en la [PlayStore de Android](https://play.google.com/store/apps/details?id=org.supertuxkart.stk), cosa que puede ser interesante si os apetece disfrutar del juego en vuestros móbiles o tabletas. Os dejamos con el video oficial del lanzamiento:


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/7pn1chyzTFI" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


Desde JugandoEnLinux nos gustaría recomendaros encarecidamente este conocido juego, pues como todos sabreis se trata de un veterano desarrollo libre, que además es uno de los juegos más veteranos que tenemos, y que también más ha mejorado y evolucionado con el tiempo. Podeis descargarlo desde su página de sourceforge [pinchando aquí](https://sourceforge.net/projects/supertuxkart/files/SuperTuxKart/0.9.3-rc1/).


 


¿Habeis jugado alguna vez a Super Tux Kart? ¿Que os parece esta nueva versión? Déjanos tu opinión en los comentarios o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).


 

