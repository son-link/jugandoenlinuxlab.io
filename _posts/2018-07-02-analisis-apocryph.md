---
author: leillo1975
category: "An\xE1lisis"
date: 2018-07-02 14:59:31
excerpt: "<p>Le damos un repaso a lo nuevo de <span class=\"username u-dir\" dir=\"\
  ltr\">@BigzurGames</span></p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/0bbf3df5693cbd3118c040cdf0cdd245.webp
joomla_id: 783
joomla_url: analisis-apocryph
layout: post
tags:
- accion
- analisis
- retro
- apocryph
- bigzur-games
title: "An\xE1lisis: Apocryph."
---
Le damos un repaso a lo nuevo de @BigzurGames


**Apocrifo (RAE):**



> 
> *1. adj. Falso o fingido. Un conde apócrifo.*
> 
> 
> *2. adj. Dicho de una obra, especialmente literaria: De dudosa autenticidad en cuanto al contenido o a la atribución. U. t. c. s. m.*
> 
> 
> *3. adj. Dicho de un libro de la Biblia: Que no está aceptado en el canon de esta. Los evangelios apócrifos. U. t. c. s. m.*
> 
> 
> 


 Como veis, la palabra se define basicamente como algo falso, y en parte eso es lo que nos vamos a encontrar en este juego, **un paso en falso** teniendo en cuenta el objetivo y premisas que tenía este juego durante su desarrollo y posterior salida. Si recordais, por navidad los chicos de Bigzur Games, lanzaron una [Alpha](index.php/homepage/generos/accion/item/709-apocryph-un-old-school-fps-tiene-version-pre-alpha) donde nos mostraban un juego al más puro estilo Hexen o Heretic. En ella  podíamos ver un juego en desarrollo, con sus fallos lógicos de versión previa, que trataba de emular la jugabilidad y mecánicas de los clásicos antes mencionados, pero con gráficos modernos, por lo que enseguida captó nuestra atención. Durante este proceso de desarrollo incluso se llegó a proponer una versión que funcionaba con la API de Vulkan.


 ![Apocryph Demonio](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Apocryph/Analisis/Apocryph_Demonio.webp)*Tendremos una buena cantidad de enemigos diferentes que machacar*


Finalmente, tras unos pocos meses [el juego era lanzado](index.php/homepage/generos/accion/item/842-apocryph-anuncia-su-lanzamiento-para-el-proximo-27-de-abril), y en JugandoEnLinux recibíamos una copia (gracias Bigzur Games) para su evaluación que pronto os enseñaríamos en un [Stream en directo](https://youtu.be/s6JUeAd-usc) por nuestros canales de Video. En el encontramos un juego con un aspecto técnico no muy trabajado, pero que resultaba divertido jugar, aunque con una cantidad de fallos, al menos en la versión de Linux, que hacían que disfrutarlo fuese un suplicio. En un ejercicio de paciencia y bondad decidimos [reportarles todos los fallos que encontramos](https://steamcommunity.com/app/596240/discussions/0/1694919808743537652/) y aplazamos este análisis para darles la oportunidad de que estos fuesen paliados. El tiempo ha pasado, y tras algunas actualizaciones, la inmensa mayoría de los más graves no han sido solventados. El estudio ha preferido añadir niveles, enemigos, modos de armas, etc... en vez de solucionar los problemas acuciantes que tiene el juego.


Bueno, vamos a dejar un momento el tema de los bugs y vamos a intentar centrarnos en el juego en si. Como os dijimos hace un momento, Apocryph intenta traernos esa **jugabilidad rápida y directa** de los juegos de la vieja escuela, poniéndonos en la piel de un "Inquisidor" que a golpe de puños y armas mágicas tendrá que limpiar su mundo, del que vuelve tras un largo exilio, de la invasión de monstruos y demonios que ahora lo pueblan. Todo esto lo sabemos gracias a la descripción del juego, pues **no hay ningún tipo de video ni tan siquiera un texto que nos ponga en situación**, y aunque en este tipo de juegos, normalmente la historia es lo de menos, no estaría mal tener alguna introducción antes de empezar a jugar. Lo primero que vamos a encontrarnos tras la pantalla de carga es una interfaz o hud que enseguida nos llevará a otras épocas, lo cual en si es bueno pues es lo que pretende desde un principio. Por lo contrario, los escenarios, enemigos, etc... tienen un aspecto moderno, aunque no puntero, pero acusan unas deficiencias notables en la calidad de las texturas. No en vano el juego tan solo necesita de un giga y medio para instalarse, algo ridículo en juegos modernos.


![Apocryph Apostol](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Apocryph/Analisis/Apocryph_Apostol.webp)


Enseguida nos ponemos al mando de nuestro personaje con los típicos controles estandar que podemos encontrar en cualquier shooter actual, y es ahí donde empezamos a ver deficiencias. El juego, pese a que no tiene que representar en pantalla nada del otro mundo, tiene unas **tasas de frames muy bajas**, que rara vez pasan de los 50 fps y que en escenarios más abiertos, como el segundo nivel, caen en picado inexplicablemente. No está muy claro cual es el problema, pues aun sabiendo que el juego está creado con el motor Unity y que dicho motor no es una maravilla en rendimiento en nuestro sistema, hemos visto juegos con muchísima más carga gráfica que se comportan bastante mejor. Es curioso, pues juraría que [la primera Alpha que jugamos a principios de año](https://youtu.be/0jW8irg-0iA) funcionaba más fluida. Todo parece evidenciar que se trata de un problema de **falta de optimización**.


Uno de los bugs más molestos que nos encontramos en el juego tiene que ver con los gráficos, ya que a veces **tras salvar el juego la pantalla se queda como blanqueada**, como si hubiese una "niebla" que aclara todo el entorno. Ya desde el principio cuando no me di cuenta que se trataba de un bug me resultó bastante extraño, pero tras ver que al cargar de nuevo el nivel este "efecto" desaparacía entendí que se trataba de un fallo. Tras un parche han conseguido reparar a medias el problema, pues ahora tras la carga todo está normal, pero al pulsar "guardar" vuelve a aparecer y es necesario recargar el nivel.


A nivel de jugabilidad, **el juego es sencillo y emula perfectamente a sus antecesores de los 90**, otorgándonos velocidad y acción a raudales. En más de una ocasión nos sentiremos abrumados por la cantidad de enemigos y tendremos que replantearnos nuestra estrategia y armas a la hora de superarlos. Aquí volvemos encontrar otro problema, y es que **en determinados momentos los controles no parecen obedecernos y de repente vemos como dejando de movernos nuestro personaje sigue moviéndose en una dirección**. Hay también algunas zonas en las que debemos saltar barreras invisibles sin que aparentemente hay nada delante (ni tenga por que haberlo).


![Apocryph Gore](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Apocryph/Analisis/Apocryph_Gore.webp)


*El tema del gore está tratado de la misma forma que en los clásicos, siendo este aspecto muy exagerado como antaño*


**Las armas que nos acompañarán por esta aventura serán variadas** y consumirán tres tipos de maná diferentes que podremos recolectar en el escenario con items del color que cada una de estas energías mágicas tengan. Tendremos báculos que dispersarán su radio de acción, ideales para enemigos poco resistentes y en gran cantidad,  armas con gran cadencia y mayor precisión, otras que provocan exposiones... También podremos hacer uso de patadas, aunque sinceramente no logré encontrarle una utilidad muy grande teniendo todo el despliegue anterior. Durante la partida podremos recolectar diferentes items tales como granadas, pócimas de vida, potenciadores, etc. También es posible usar una especie de modo furia que nos convertirá en un auténtico Berserker que destrozará todo cuento se le ponga delante, ralentizando el tiempo para facilitarnos las cosas. Es considerable también la variedad de enemigos que nos encontraremos durante esta aventura, enfrentándonos a monstruos de todo tipo, cada cual con sus particularidades. Tendremos que hacernos cargo de esqueletos, cíclopes, plantas carnívoras.... y así hasta más de 25 diferentes.


Otro de los **guiños al pasado** de este juego está en los secretos que encontraremos durante el juego, y que tras completar un nivel descubriremos que no hemos completado ni la mitad. Teniendo en cuenta que soy de los que les gusta explorar me ha sorprendido que no haya encontrado más. En ellos nos toparemos con armas, powerups, etc. El intentar descubrirlos todos es un factor que puede darle cierta rejubilidad al título. También es muy a la vieja usanza la forma de representar el gore, con enemigos que se desmembran y dejan todo perdido cuando conseguimos liquidarlos, por lo que si sois de estómagos delicados no es para vosotros.


 Curioso es el **modo pixelado**, que nos permite ver el juego como se vería en un clásico shooter de los 90, aunque desde mi punto de vista no está todo lo bien acabado de que debería y resulta un poco confuso jugar de esta manera, ya que a mi juicio los elementos que se presentan en la pantalla deberían verse más contrastados y con colores más básicos que permitan diferenciar correctamente los objetos, paredes, enemigos, etc. Da la impresión que simplemente se ha aplicado un filtro que pixela la pantalla. Pienso que a modo de curiosidad puede llamar la atención, pero resulta poco viable a la hora de jugar.


![Apocryph Pixelizado](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Apocryph/Analisis/Apocryph_Pixelizado.webp)


*El modo pixelizado es muy llamativo pero poco útil para enfrentarnos al juego*


Los menues son parcos en opciones, lo cual se puede perdonar por tratarse de un juego que pretende ser retro, pero son poco intuitivos, además no funcionan correctamente y la traducción a nuestro idioma, que ha sido eliminada en una actualización (casi mejor) , es muy mala confundiéndonos a la hora de configurarlo. Por poner un ejemplo, se pueden activar y desactivar las sombras (shadows en Inglés) del juego, en cambio en el menú pone Oscuridad, lo cual puede no entenderse o llevar a engaños.


**Apocryph es una de cal y una de arena**. Querer rendir tributo a clásicos tan grandes como los antes mencionados puede ser un arma de doble filo. En principio te puede valer para llamar la atención de todos esos jugadores que en su día difrutaron de esos títulos, pero si no lo haces bien las críticas pueden ser feroces y el personal sentirse engañado. Aun así, como reitero durante el análisis, **aunque está lejos de ser lo que pretendía, el juego resulta entretenido** y nos hará pasar un buen rato si somos capaces de olvidar por un momento todos sus molestos bugs.


Desde JugandoEnLinux estamos dispuestos a realizar una actualización de este análisis si finalmente en algún momento sonase la campanilla y la versión para GNU-Linux corrigiese sus problemas más graves. Esperemos que sus responsables tomen nota y en un futuro cercano ojalá tengamos que tragarnos nuestras palabras y dar una segunda oportunidad a este trabajo de Bigzur Games. Os dejamos con un video que grabamos recientemente donde podeis ver muchas de las cosas que menciono en este artículo:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/svkAc6CdE_A" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


 Podeis comprar Apocryph en la tienda de [Steam](https://store.steampowered.com/app/596240/Apocryph_an_oldschool_shooter/).

