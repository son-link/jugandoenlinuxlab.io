---
author: Pato
category: Estrategia
date: 2020-05-28 18:03:00
excerpt: "<p>El estudio est\xE1 trabajando estrechamente con Aspyr Media para traernos\
  \ la expansi\xF3n</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Civilization_VI/Sidmeyerscivmaya.webp
joomla_id: 1226
joomla_url: sid-meier-s-civilization-vi-se-actualizara-con-el-pack-maya-gran-colombia
layout: post
tags:
- aspyr-media
- aspyr
title: "Sid Meier's Civilization VI se actualizar\xE1 con el pack Maya & Gran Colombia"
---
El estudio está trabajando estrechamente con Aspyr Media para traernos la expansión


Leemos en el comunicado del juego en Steam que Firaxis Games está trabajando junto a Aspyr Media en el pack Maya & Gran Colombia de cara a su lanzamiento en próximas fechas.


En esta expansión tomaremos parte en la conquista de estas zonas de Sudamérica con personajes como Simón Bolivar y otros personajes relevantes de los Mayas... 


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/5qKSQ1nvbDs" width="560"></iframe></div>


Estaremos atentos a las novedades sobre este Sid Meier's Civilization y sus expansiones. Puedes leer el comunicado [en este enlace](https://store.steampowered.com/newshub/app/289070/view/2245553986331656722), o si te interesa el juego de estratégia puedes visitar su [página web de Steam](https://store.steampowered.com/app/289070/Sid_Meiers_Civilization_VI/) donde tienes toda la información sobre el juego.


 

