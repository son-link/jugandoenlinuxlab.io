---
author: Pato
category: Estrategia
date: 2018-06-08 11:48:01
excerpt: "<p>El juego adem\xE1s tiene publicados los requisitos para nuestro sistema</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/e26e070f0b61121e8e747746e6882f29.webp
joomla_id: 761
joomla_url: victory-at-sea-pacific-aparece-en-steamdb-para-linux-y-parece-que-llegara-en-algun-momento-de-2018
layout: post
tags:
- accion
- proximamente
- rts
- estrategia
title: "'Victory at Sea Pacific' aparece en SteamDB para Linux y parece que llegar\xE1\
  \ en alg\xFAn momento de 2018"
---
El juego además tiene publicados los requisitos para nuestro sistema

Para todos los que gustan de los juegos de acción estratégica en tiempo real pero en un entorno distinto, buenas noticias ya que la siguiente entrega del ya disponible 'Victory At Sea' [[Steam](https://store.steampowered.com/app/298480/Victory_At_Sea/)]  parece que también vendrá a Linux/SteamOS ya que ha aparecido en [SteamDB](https://twitter.com/SteamDB_Linux/status/1004638282628681729) y ha publicado los requisitos para nuestro sistema. Esta nueva entrega se llamará '**Victory at Sea Pacific**' y promete nuevos desafíos en las batallas navales de la II Guerra Mundial.


*Victory at Sea Pacific es la nueva entrega del RTS de combate naval ambientado en la II Guerra Mundial. Con un enfoque épico en la jugabilidad de los juegos de guerra estratégicos, busca y destruye flotas enemigas en este juego sandbox de mundo abiertoen un desesperado intento de cambiar la historia.*


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/mdjYonQSPkU" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 En Victory at Sea Pacific podremos elegir desde comandar a toda la flota en el Pacífico o ampliar hasta coger el control de barcos individuales o vuelos de aviones dentro de una campaña **para un jugador**. Dispara torpedos desde tus submarinos o salta al puente de mando y dirige a tu flota a cambiar el curso de la guerra.


En cuanto a los requisitos que se han hecho públicos, estos son:


MÍNIMO:  



+ Requiere un procesador y un sistema operativo de 64 bits
+ SO: SteamOS o Ubuntu 14.10
+ Procesador: Intel Core i3-4170T
+ Memoria: 4 GB de RAM
+ Gráficos: NVIDIA GeForce GTX 850M 2GB
+ Almacenamiento: 15 GB de espacio disponible


RECOMENDADO:


+ Requiere un procesador y un sistema operativo de 64 bits
+ SO: SteamOS or Ubuntu 14.10
+ Procesador: Intel Core i5 3.5Ghz
+ Memoria: 8 GB de RAM
+ Almacenamiento: 15 GB de espacio disponible


Tienes más detalles sobre 'Victory at Sea Pacific' en su [página web oficial](http://www.victoryatseagame.com/) o en su [página de Steam](https://store.steampowered.com/app/806950) donde estará disponible en algún momento de este 2018.


Estaremos atentos a nuevas noticias sobre este desarrollo.


¿Te gustan los juegos RTS? ¿Desplegarás tu estrategia naval en el Pacifico?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux en [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

