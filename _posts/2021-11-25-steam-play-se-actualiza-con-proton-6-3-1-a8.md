---
author: leillo1975
category: Software
date: 2021-11-25 09:31:31
excerpt: "<p>Nueva versi\xF3n de esta popular herramienta Open Source de @ValveSoftware.</p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Proton/valve-Proton.webp
joomla_id: 1295
joomla_url: steam-play-se-actualiza-con-proton-6-3-1-a8
layout: post
tags:
- wine
- valve
- steam-play
- dxvk
- proton
- faudio
- vkd3d
- pierre-loup-griffais
- 6-3
title: ' Steam Play se actualiza con Proton 6.3-8'
---
Nueva versión de esta popular herramienta Open Source de @ValveSoftware.


**ACTUALIZADO 25-11-2021**: Recien salido del horno, ya tenemos la [versión 6.3-8 de Proton](https://github.com/ValveSoftware/Proton/wiki/Changelog#63-8) con muchos cambios y añadidos. Seguramente la llegada de la Steam Deck sea el principal causante de este empujón que le está dando Valve. Si le sumamos el [retraso en su lanzamiento]({{ "/posts/el-lanzamiento-de-steam-deck-se-retrasa-hasta-febrero-de-2022" | absolute_url }}), quizás esto sirva para mejorar y afinar mucho mas Proton de cara a su lanzamiento.


- Para empezar estos son los **muchos juegos que ahora son jugables:**


* Age of Empires 4
* Assassin's Creed
* Breath of Death VI
* Call of Duty: Black Ops II singleplayer (202970)
* DEATHLOOP
* FIA European Truck Racing Championship
* Fly'N
* Game Dev Tycoon
* Ghostbusters: The Video Game Remastered
* GreedFall
* Mafia II (Classic)
* Magicka
* Marvel's Guardians of the Galaxy (AMD GPUs only)
* Mass Effect Legendary Edition (ME1 does not have working audio, see #4823)
* Monster Boy and the Cursed Kingdom
* Monster Energy Supercross - The Official Video Game
* Monster Energy Supercross - The Official Video Game 2
* Nickelodeon All-Star Brawl
* Penny Arcade's On the Rain-Slicked Precipice of Darkness 3
* RiMS Racing
* The Riftbreaker
* Sol Survivor
* TT Isle of Man Ride on the Edge
* TT Isle of Man Ride on the Edge 2


- Como ya anunciamos se añade el soporte para el **[sistema antitrampas BattleEye]({{ "/posts/battleye-se-suma-al-carro-y-tambien-dara-soporte-a-wine-proton-a1" | absolute_url }})**


- Compatibilidad con el sistema **antitrampas de Valde CEG DRM.**


- **DLSS en juegos DX11 y DX12:** Por si aun no lo sabes Deep Learning Super Sampling es una función de rescalado similar a FSR de AMD, pero haciendo uso de IA con resultados mejores. Ahora funcionará en los juegos que lo utilizan, para ello debes de disponer de una tarjeta Nvidia de la serie RTX.  
Se activa usando el comando *PROTON_ENABLE_NVAPI=1 %command%* al lanzar un juego en Steam y *dxgi.nvapiHack = True en dxvk.conf*.


- Muchos juegos que se han **arreglado con algún problema**:


* Arreglado errores en Project Wingman, Satisfactory y otros juegos de Unreal Engine 4 usando el renderizador Vulkan.
* Arreglado un bloqueo esporádico al iniciar Baldur's Gate 3.
* Se corrigió el modo multijugador en red en RaceRoom Racing Experience.
* Se corrigió la desincronización de la escena en Assassin's Creed: Odyssey.
* Se soluciona el audio entrecortado en Gahkthun Steam Edition.
* Arreglado Atomic Shop y fallos aleatorios al inicio en Fallout 76.
* Se corrigió que el Lanzador de Paradox (utilizado por Europa Universalis IV) no mostrara nada.
* Se corrigió que el video se congelara y que el juego se bloqueara en Deep Rock Galactic.
* Arreglado Industries of Titan que no enumera resoluciones ni monitores.
* Solucione que Bloons TD6 no pueda acceder a la configuración de la cuenta`.`
* Arreglado Project CARS 3 que ignoraba las entradas después de Alt + Tab.
* Se corrigió que la tecla Alt se atascara después de salir con Alt + Tabulación en Warhammer: Chaosbane.
* Arregle los videos de Biomutant que no se reproducen.
* Admite caracteres tailandeses en Mirror y otros juegos de Unity.
* Admite caracteres coreanos y árabes en el lanzador Cyberpunk 2077.
* Solucione problemas de conexión en Satisfactory en sistemas con múltiples interfaces de red.


- Otros cambios fueron:


* Actualice dxvk a v1.9.2-13-g714ca482.
* Actualice vkd3d-proton a v2.5-50-g0251b404.
* Actualice wine-mono a 6.4.1.


 


 




---


********ACTUALIZADO 1-10-21:******** Acaba de publicarse esta misma mañana la [versión 6.3-7](https://github.com/ValveSoftware/Proton/releases/tag/proton-6.3-7) de Proton por parte de los desarrolladores de Codeweavers y Valve Software, que como vereis a continuación hace jugables unos cuantos juegos de lo más "apetitosos". La lista de cambios es la siguiente:


***-Los siguientes juegos ya son jugables:***  
 *Life is Strange: True Colors*  
 *Quake Champions (roto tras una actualización del juego)*  
 *Divinity Original Sin 2 (roto tras una actualización del juego)*  
 *eFootball PES 2021*  
 *EVERSLAUGHT VR*  
 *WRC 8, 9 y 10*


*-Arreglado el **mapeo de Logitech G920** para F1 2020.*


*-Arreglados los ajustes de visualización de **Resident Evil Village**.*


*-Mejoradas las ventanas de **Forza Horizon 4**.*


*-Actualizado **DXVK** a la [v1.9.2](https://github.com/doitsujin/dxvk/releases/tag/v1.9.2).*


*-Actualizado **vkd3d-proton** para incluir el último trabajo de desarrollo.*


Nosotros además podemos añadir que **GRID (2019)** vuelve a funcionar correctamente, incluyendo los fixes de la rama experimental; y **DIRT 5** ha mejorado el fallo de los shaders, aunque aun persiste el problema. Queremos darle las **gracias a @lordgault por el aviso y por los datos ofrecidos sobre estos dos últimos juegos**. ¡Ahora a disfrutar de vuestros juegos favoritos con Proton durante el fin de semana!


 




---


********ACTUALIZADO 21-8-21:******** La gente de Valve software acaba de actualizar Proton a la [versión 6.3-6](https://github.com/ValveSoftware/Proton/releases/tag/proton-6.3-6b), dando un paso más en pos de la compatibilidad total como han prometido para su Steam Deck. Las **novedades** en esta ocasión vuelven a ser numerosas además de destacables:  
  




* Ahora se pueden jugar los siguientes juegos: 
	+ **Tokyo Xanadu eX+**
	+ **Sonic Adventure 2**
	+ **Rez Infinite**
	+ **Elite Dangerous**
	+ **Blood of Steel**
	+ **Homeworld Remastered Collection**
	+ **Star Wars Knights of the Old Republic**
	+ **Guardians VR**
	+ **3D Aim Trainer**
* Mejora la experiencia en otros idiomas en los lanzadores **Cyberpunk 2077** y **Rockstar Games**.
* Mejora el comportamiento del lanzador en **Swords of Legends Online**.
* Mejora la reproducción de video en **Deep Rock Galactic**, **The Medium**, **Nier: Replicant** y **Contra: Rogue Corps**.
* Agregue soporte opcional para la biblioteca de **soporte de GPU NVAPI de Nvidia y DLSS**. Está deshabilitado de forma predeterminada y se puede habilitar con `PROTON_ENABLE_NVAPI=1`.
* Actualice **wine-mono** a [6.3.0](https://github.com/madewokherd/wine-mono/releases/tag/wine-mono-6.3.0) .
* Actualice **DXVK** a [v1.9.1](https://github.com/doitsujin/dxvk/releases/tag/v1.9.1) .
* Actualice **vkd3d-proton** a [v2.4](https://github.com/HansKristian-Work/vkd3d-proton/releases/tag/v2.4) .
* Actualice **FAudio** a [21.08](https://github.com/FNA-XNA/FAudio/releases/tag/21.08) .
* Se corrigió el bloqueo de **Microsoft Flight Simulator** durante la carga.
* Se corrigió la instalación de la actualización de **Unreal Engine 4**, que afectó a Everspace 2 y KARDS.
* Corrija la entrada de texto y el pegado del portapapeles en la superposición y el **iniciador de Origin**.
* Arregla parte de la música que no se reproduce en **Planet Coaster**.
* Arregla el rendimiento de **Mafia III: Definitive Edition** cuando el limitador de FPS está habilitado.
* Soluciona problemas de reproducción de audio en **Fallout: New Vegas**, **Oblivion**, **Borderlands 3** y **Deep Rock Galactic**.
* Mejore el comportamiento de captura del cursor para ventanas de pantalla completa.
* Mejore el manejo de entrada después de la pérdida de enfoque en algunos juegos, incluidos **Warhammer: Chaosbane** y **Far Cry Primal**.
* Corrija las ubicaciones de guardado del juego para mejorar el comportamiento de sincronización de la nube de Steam en: 
	+ **Guilty Gear -Strive-**
	+ **Death Stranding**
	+ **Katamari Damacy Reroll**
	+ **Scarlet Nexus**






---


 ****ACTUALIZADO 25-6-21:**** Arrancamos hace un instante Steam y comprobamos que había una nueva versión de Proton. Hemos consultado la página donde se publica el "[changelog](https://github.com/ValveSoftware/Proton/wiki/Changelog#63-5)" , y efectivamente hay actualización, y en esta ocasión con un montón de cambios, que podeis ver detallados a continuación:


***Muchas mejoras en el renderizado de vídeo.** Los juegos que se sabe que han sido mejorados son:*  
 *-Bloodstained*  
 *-Deep Rock Galactic*  
 *-Metal Gear Solid V: The Phantom Pain*  
 *-Resident Evil 2 (2019)*  
 *-Resident Evil 3 (2020)*  
 *-Team Sonic Racing*  
 ***Actualizar DXVK a la [v1.9](https://github.com/doitsujin/dxvk/releases/tag/v1.9).***  
 ***Mejorar la selección de dispositivos de la GPU.***  
 ***Actualizar vkd3d-proton para incluir el último trabajo de desarrollo.***  
 ***Los siguientes juegos son ahora jugables:***  
 *-Sid Meier's Civilization VI*  
 *-Crypt Stalker*  
 *-Dark Devotion*  
 *-Dorfromantic*  
 *-Far Cry*  
 *-Hard Reset*  
 *-Hogs of War*  
 *-Might & Magic: Clash of Heroes*  
 *-Pro Cycling Manager 2020*  
 *-Sang Froid - Tales of Werewolves*  
 *-Secret of Mana*  
 *-Trainz Railroad Simulator 2019*  
 ***Mejorar la compatibilidad con el mando en Hades y muchos títulos de Unity.***  
 ***Arreglar CyberPunk 2077, Darksburg y otros lanzadores que se cuelgan al hacer clic en enlaces externos.***  
 ***Arreglar el bloqueo de Conan Exiles y otros lanzadores de Funcom.***  
 ***Corregir la falta de audio en Project Cars 3.***  
 ***Mejor soporte para construcciones basadas en contenedores fuera de la VM de construcción.***


 ¡Hagan juego señores!




---


****ACTUALIZADO 14-5-21:**** Acabamos de recibir un correo anunciándonos que tenemos disponible una nueva versión de este desarrollo de Valve y Codeweavers (entre otros) Se trata de la [versión 6.3-4](https://github.com/ValveSoftware/Proton/releases/tag/proton-6.3-4) y algunas mejoras especialmente destacables:  
  
*-Corrige la reciente actualización del **lanzador de 2K Games** (no sabía ni que estos tenían lanzador también)*  
 *-Corrección del error de **inicio de algunos juegos Direct3D 12***  
 *-Corrige problemas de visualización del lanzador de **Divinity Original Sin 2 y Rise of Venice***  
 *-Corrige el lanzamiento incorrecto de **Star Wars Squadrons VR** en el escritorio*  
 *-Corrige los artefactos visuales de **Sacred Gold***  





---


 **ACTUALIZADO 4-5-21:** Si, es cierto, nos hemos dejado una versión, la 6.3-2 atrás, pero esta tan solo tenía un pequeño fix para el ratón, por lo que decidimos obviarla. Pero en esta ocasión la gente de Valve y Codeweavers vuelve a la carga con otra revisión más, la 6.3-3, que vuelve a traernos novedades reseñables, como las siguientes:


*-Anteriormente en Experimental: Actualizar **vkd3d-proton** a [v2.3.1](https://github.com/HansKristian-Work/vkd3d-proton/releases/tag/v2.3.1).*  
 *-Anteriormente en Experimental: La superposición de **Origin** es funcional, lo que permite jugar a **It Takes Two** con amigos.*  
 *-Anteriormente en Experimental: **Mount & Blade II: Bannerlord** es jugable.*  
 *-Anteriormente en Experimental: Se corrige el error "No comprado" de **Red Dead Redemption 2**.*  
 *-Anteriormente en Experimental: Arreglar el cuelgue de **Age of Empires II: Definitive Edition** al arrancar.*  
 *-Arreglados los lanzadores de **Evil Genius 2**, **Zombie Army 4**, **Strange Brigade**, **Sniper Elite 4**, **Beam.NG** y **Eve Online**.*  
 *-**The Bus** y **Army General** son ahora jugables.*  
 *-Soporte para ajustar el brillo/gama en juegos antiguos como Deus Ex.*  
 *-Arreglo de la detección del mando de Xbox en Far Cry Primal.*


Como veis se ha aumentado la compatiilidad en un buen puñado de juegos, por lo que si disponeis de alguno de estos ya podeis probarlos y dejar vuestras impresiones en los comentarios o como siempre en nuestros grupos de JugandoEnLinux en [Telegram](https://twitter.com/JugandoenLinux) o  [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).




---


**NOTICIA ORIGINAL 1-4-21:** Hace más o menos medio año (el 15 de Octubre) comenzaba la andadura de la anterior versión de Proton, la [5.13]({{ "/posts/steam-play-se-actualiza-con-proton-5-13-6" | absolute_url }}), y con él se ampliaba en gran medida la compatibilidad de Steam Play, algo que por supuesto todos los jugadores Linuxeros agradecimos. Hoy la historia se vuelve a repetir, pero dando un salto importante de versión, y **basándose en la versión 6.3 de Wine**, lo cual por supuesto implica un montón mejoras. El anúncio lo realizaba como siempre [Pierre-Loup Griffais](index.php/component/search/?searchword=Pierre&searchphrase=all&Itemid=828) en Twitter:  
  




> 
> Proton 6.3 now released, and Experimental rebased! [pic.twitter.com/l32CENSjpy](https://t.co/l32CENSjpy)
> 
> 
> — Pierre-Loup Griffais (@Plagman2) [April 1, 2021](https://twitter.com/Plagman2/status/1377696045400911872?ref_src=twsrc%5Etfw)


 



 


Los cambios los recibíamos de [Andrew Eikum](https://github.com/aeikum) a través de correo electrónico y son numerosos y de mucha importancia, por lo que en esta versión de [Proton 6.3-1](https://github.com/ValveSoftware/Proton/releases/tag/proton-6.3-1) encontraremos los siguientes:


Entre los **nuevos juegos jugables** se encuentran:


-Divinity: Original Sin 2  
 -Shenmue I y II  
 -Mass Effect 3 N7 Digital Deluxe Edition (2012)  
 -Tom Clancy's Rainbow Six Lockdown  
-XCOM: Chimera Squad  
 -Bioshock 2 Remasterizado  
 -Company of Heroes 2  
 -logiCally  
 -Rise of the Triad  
 -Home Behind 2  
 -Shadow Empire  
 -Arena Wars 2  
 -King Arthur: Knight's Tale  
 -Rise of Venice  
 -ARK Park  
 -Gravity Sketch  
 -Battle Arena VR


Actualizado **Wine** a la versión **6.3**.  
 Actualizado **DXVK** a la **v1.8.1**.  
 Actualizado **vkd3d-proton** a la **v2.2**.  
 Actualizado **FAudio** a la versión **21.03.05**.  
 Actualizado **wine-mono** a la **6.1.1**.  
 Anteriormente en Experimental: **Mejoras para las distribuciones de teclado** no estadounidenses.  
 Anteriormente en Experimental: **Mejoras en el soporte de video**. Puede que veas vídeos de patrones de prueba. Esto es normal.  
 Anteriormente en Experimental: Soporte para establecer las prioridades de los hilos a través de RTKit, o las prioridades de Unix si su usuario tiene permisos.  
 Anteriormente en Experimental: Mejoras en la **disposición de los mandos** y en la conexión en caliente en Slay the Spire y Hades.  
 Anteriormente en Experimental: Mejora de la compatibilidad con el **mando de PlayStation 5**.  
 Anteriormente en Experimental: **Mejora del tiempo de inicio de la Realidad Virtual y su compatibilidad**.  
 Mejoras en el inicio de sesión de Uplay.  
 **Mejora de la compatibilidad con el volante G29** en Assetto Corsa Competizione.  
 Arreglado el Simulador de Vuelo de Microsoft en el modo VR.  
 Se han corregido las escenas en Bioshock 2 Remastered.  
 Se ha revisado el sistema de construcción para reducir en gran medida los tiempos de construcción.


Pues nada, que a probarlo se ha dicho. Yo no pienso perder un minuto más, por que si quereis comentar que tal os va con esta versión podeis dejar vuestras impresiones en los comentarios y en nuestros grupos de JugandoEnLinux en [Telegram](https://twitter.com/JugandoenLinux) o  [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org). Seguiremos pendientes de las nuevas actualizaciones de esta rama.

