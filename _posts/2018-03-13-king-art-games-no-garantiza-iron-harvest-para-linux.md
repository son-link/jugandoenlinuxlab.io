---
author: leillo1975
category: Estrategia
date: 2018-03-13 14:16:14
excerpt: "<p><a href=\"https://twitter.com/KingArtGames\" target=\"_blank\" rel=\"\
  noopener user\" class=\"account-link link-complex\" data-user-name=\"KingArtGames\"\
  ><span class=\"username txt-mute\">@KingArtGames</span></a><span class=\"username\
  \ txt-mute\"> estudiar\xEDa el traer su juego m\xE1s adelante<br /></span></p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/9ec5addc67447038d196a2fc30522c2d.webp
joomla_id: 676
joomla_url: king-art-games-no-garantiza-iron-harvest-para-linux
layout: post
tags:
- kickstarter
- king-art
- iron-harvest
title: Los desarrolladores no garantizan Iron Harvest para Linux.
---
[@KingArtGames](https://twitter.com/KingArtGames) estudiaría el traer su juego más adelante  


Las palabras se las lleva el viento.... eso es lo primero que se me vino a la cabeza cuando esta mañana leía lo siguiente en su [recién estrenada campaña de financiación](https://www.kickstarter.com/projects/kingartgames/iron-harvest):



> 
> *"And what about Linux and Mac versions? – We released most of our games for Linux and Mac. Nevertheless, at this point, we do not want to promise something we might not be able to deliver. On the technology side, Iron Harvest is much more complex than our other games, for example because of the multiplayer features. Therefore, we are going to evaluate Linux/Mac versions at a later point in time."*
> 
> 
> "¿Y que pasa con las versiones de Linux y Mac?-Lanzamos la mayor parte de nuestros juegos para Linux y Mac. Sin embargo, en este momento, no podemos prometer algo que quizás no podamos llevar a cabo. Teniendo en cuenta la tecnología, Iron Harvest es mucho más complejo que nuestros otros juegos, por ejemplo por las características multijugador. Por lo tanto, **vamos a evaluar las versiones de Linux y Mac en un momento posterior**"
> 
> 
> 


Si recordáis, hace un mes y medio nos mostrábamos esperanzados al ver que [el proyecto estaba estudiando buscar financiación para su desarrollo](index.php/homepage/generos/estrategia/item/742-iron-harvest-podria-buscar-financiacion-en-kickstarter). Estábamos dispuestos a promocionar su campaña con los medios de los que disponemos ([web](https://jugandoenlinux.com), [twitter](https://twitter.com/JugandoenLinux), [facebook](https://www.facebook.com/jugandoenlinux/), [youtube](https://www.youtube.com/channel/UC4FQomVeKlE-KEd3Wh2B3Xw)...). En los últimos días habíamos recibido un correo electrónico por parte de [King Art Games](https://kingart-games.com/) animándonos a hacerlo. Incluso, yo personalmente, estaba pensando seriamente el apoyar financieramente el juego con unos 50€. Llevo más de un año siguiendo el desarrollo de este juego, y en un par de ocasiones me puse en contacto con la compañía preguntando si Linux seguía entre sus planes. Las dos veces me contestaron afirmativamente, y la segunda vez diciéndome que por supuesto, como todos sus últimos juegos.


Como veis se me nota la decepción por los cuatro costados, y aunque me gustaría ser imparcial y contar la noticia lo más objetivamente posible, me resulta tremendamente difícil dado mi interés por este juego. Lo único que digo es que en estas condiciones, **la comunidad de jugadores de Linux no puede ni debe apoyar financieramente este proyecto**, ya que en ningún momento se garantiza que el juego llegue a tener soporte, ni siquiera después de ser lanzado. Al menos han sido valientes y han tenido la decencia de comunicarlo a tiempo antes de que un incauto les adelantase dinero. Como estamos hartos de leer por muchos sitios, "No Tux, No Bucks".... Me quedan pocas ganas de dejaros con el video de la campaña, pero si después de esto aun os quedan ganas de verlo aquí lo tenéis:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/cqdR7ZhRd7c" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


¿Os habeis llevado una decepción al leer este artículo? ¿Qué opinais de este anuncio por parte de [@KingArtGames](https://twitter.com/KingArtGames)?  Déjanos tu opinión en los comentarios o discútelo en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

