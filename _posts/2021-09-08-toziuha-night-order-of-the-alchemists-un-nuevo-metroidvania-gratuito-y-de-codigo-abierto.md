---
author: Son Link
category: Plataformas
date: 2021-09-08 11:15:00
excerpt: "<p>El juego a\xFAn en desarrollo apunta maneras</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ToziuhaNight/Toziuhanightbnnr.webp
joomla_id: 1354
joomla_url: toziuha-night-order-of-the-alchemists-un-nuevo-metroidvania-gratuito-y-de-codigo-abierto
layout: post
tags:
- indie
- metroidvania
- codigo-abierto
title: "Toziuha Night Order of the Alchemists: Un nuevo metroidvania, gratuito y de\
  \ c\xF3digo abierto"
---
El juego aún en desarrollo apunta maneras


Hace poco, y otra vez a través del subreddit **linux_gaming** me encontré con el trailer de **Toziuha Night**, un nuevo metroidvania inspirado en **Castlevania: Order of Ecclesia** (juego que salio para Nintendo DS en 2008). Como en cualquier juego de este subgénero deberemos de explorar el mapa en busca de mejoras que nos permitan enfrentarnos a los distintos enemigos con los que nos iremos encontrando y acceder a nuevas zonas a las que antes no podíamos, aunque al contrario que pasaba con la entrega de Castlevania no iremos adquiriendo experiencia para poder subir de nivel.


En este juego tomaremos el papel de **Xandria**, una alquimista equipada con un látigo de hierro que luchara contra demonios y otros alquimistas para evitar que se hagan con un poder milenario. Durante su recorrido podrá obtener mejoras y diversos elemento alquímicos que la ayudaran en su búsqueda, que nos llevara a través de mazmorras infectadas de demonios, bosques tenebrosos, un pueblo en ruinas e incluso el castillo del mismísimo **Conde Dracula**.


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/Gupuz72cVv4" title="YouTube video player" width="560"></iframe></div>


### Características del juego:


* Música sinfónica original.
* Estilo retro pixelart como homenaje a las consolas de 32 bits.
* Pon a prueba tus habilidades luchando contra jefes finales y diversos enemigos.
* Explora nuevas zonas del mapa utilizando diferentes habilidades y mejorando tus estadísticas.
* Juega sin conexión a internet (juego offline).
* Personajes de estilo anime y gótico.
* Descubre más de la trágica historia del protagonista disponible en **inglés** y **español**.
* Juego de estilo metroidvania en el que no es necesario farmear (ir de un lado a otro para subir de nivel).
* Compatible con gamepads.
* Controles táctiles totalmente personalizables.
* Combina el hierro con otros elementos químicos para crear aleaciones con diferentes propiedades jugables.
* Disponible para Windows, Android, **Linux** y MacOS.
* Videojuego gratuito y de **código abierto** realizado con **Godot Engine**.


### Características planeadas para la versión final


* Un mapa con un mínimo de 7 horas de juego.
* Cualquier jugador podrá diseñar su propio mapa, otros jugadores podrán descargarlo y jugar.
* Más personajes jugables con diferentes mecánicas de juego.
* Un mapa DLC que cuenta la historia de cómo Xandria se convirtió en alquimista y su batalla en el castillo de Drácula (con una jugabilidad clásica como la de los juegos de NES).


Para ayudar a su desarrollo se ha abierto una campaña de micro mecenazgo en [Ko-fi](https://ko-fi.com/post/Toziuha-Night--Crowdfunding-campaign-E1E85UKXN), cuya primera meta es llegar a los **US$1440****, para asegurar su desarrollo durante un año (su idea es que el juego este listo para septiembre/octubre del 2022).** **Si llega a los** **US$2340** **contratara al artista** **"Montramy"****, que es el que ha hecho el material actua****l,** **ademas de publicarlo en** ****Steam******, así como obtener más sprites y música para el juego. La campaña estará** **abierta** **hasta el** ****30 de septiembre******, y como en toda campaña de este tipo, dependiendo de la cantidad donada obtendréis diferentes recompensas.**


**Desde su página en [itch.io](https://dannygaray60.itch.io/toziuha-night-order-of-the-alchemists)** podéis descargar una demo, así como enlaces a sus perfiles en varias redes y acceso al servidor de Discord del juego (ideal para reportarle bugs). Si te gusta los juegos de este tipo, te recomiendo que lo pruebes. A continuación algunas capturas del juego:


![01](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ToziuhaNight/01.webp)


![02](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ToziuhaNight/02.webp)


![03](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ToziuhaNight/03.webp)


![04](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ToziuhaNight/04.webp)


Danny, el desarrollador del juego tuvo la amabilidad de concederme una pequeña entrevista:


Son Link: Lo primero, gracias Danny por concederme esta pequeña entrevista. ¿Como surgió la idea de desarrollar este juego?


Danny: Pues después de jugarme **C****astlevania** **S****ymphony of the** **N****ight** (que estuvo bueno pero no me enamoró realmente) jugué luego el **O****rder of** **E****cclesia**, cosa que me encantó y quise hacer algo similar. Por lo general, cuando algo me gusta realmente demasiado, yo como creativo me gusta hacer cosas de las cosas que me gustan, así de simple XD


Son Link: El código del juego **es libre**, bajo la licencia **MIT** ¿Que te ha motivado a ello?


Danny: Me ha motivado a que otras personas pudieran ayudarme a mejorar el juego y claro, si hay personas que usan el mismo motor que yo, pues que tengan alguna referencia para hacer juegos de este estilo


Son Link: Para el desarrollo has optado por **Godot Engine** ¿Por que has optado por el en lugar de otros motores, como por ejemplo **Unity**?


Danny: Es gratuito, no necesita regalías, es código abierto (apoyo esta filosofía de software), es realmente fácil de usar y exporta a muchas plataformas sin tanto problema.


Son Link: ¿Que otras herramientas de código abierto usas para su desarrollo?  
 Danny: Para el pixelart uso **Libresprite**.


Son Link: He visto en tu cuenta de Itch que has desarrollado otros juegos. ¿Puedes contarnos algo sobre algunos de ellos?


Danny: Pues, ¿qué debería contar? Como he dicho, me gustar hacer cosas de las cosas que me gustan y cada juego usa uno de mis juegos favoritos como inspiración, mis juegos se inspiran en castlevania, ace attorney y varios juegos de ritmo en celulares con bangdream/lovelive.


Son Link: ¿Tienes ya en mente futuros juegos o te centraras en este?


Danny: La verdad es que tengo tantas ideas que siento la necesidad de hacer algo con ellas. Pero por ahora seguiré centrándome en Toziuha Night. He recibido apoyo en mi página de donaciones para continuar en esto, así que no quisiera decepcionar a los que creen en el potencial de este juego.


Son Link: ¿Quieres decir algo a nuestros lectores?


Danny: Pues que prueben el juego y hagan correr la voz. Lo que más me interesa es que muchas personas conozcan mis trabajos :D y que reporten cualquier bug jeje


Son Link: Muchas gracias por concederme esta entrevista y espero que logres la financiación para terminar el juego.


Danny: Muchas gracias a ti también n.n


Y hasta aquí todo. Os iremos informando de las novedades según vayan saliendo. Puedes encontrar Toziuha Night en su [página de Itch.io](https://dannygaray60.itch.io/toziuha-night-order-of-the-alchemists).

