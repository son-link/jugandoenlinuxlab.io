---
author: Jugando en Linux
category: Apt-Get Update
date: 2022-03-18 18:40:46
excerpt: ''
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/LazyGalaxy2/Lazy_Galaxy_2.webp
joomla_id: 1451
joomla_url: nuevos-juegos-con-soporte-nativo-18-03-2022
layout: post
title: Nuevos juegos con soporte nativo 18/03/2022
---

 Otra semana más, con una selección de las novedades y ofertas que pensamos que os pueden interesar con soporte nativo:


 


### Beat Invaders


Desarrolador: Raffaele Picca


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube.com/embed/qUy4281quH0" title="YouTube video player" width="720"></iframe></div>


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/1863080/" style="border: 0px;" width="646"></iframe></div>


 




---


### Lazy Galaxy 2


Desarrolador: Coldwild Games


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube.com/embed/EY0N0mMOQLI" title="YouTube video player" width="720"></iframe></div>


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/1477950/" style="border: 0px;" width="646"></iframe></div>


 




---


 


### Black Geyser: Couriers of Darkness


Desarrolador: GrapeOcean Technologies


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube.com/embed/a9gpXAgwezY" title="YouTube video player" width="720"></iframe></div>


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/1374930/" style="border: 0px;" width="646"></iframe></div>


 




---


### Fire On Fight : Online Multiplayer Shooter


Desarrolador: Amonga99


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube.com/embed/8Lq9hY3NVQg" title="YouTube video player" width="720"></iframe></div>


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/1092050/" style="border: 0px;" width="646"></iframe></div>


 




---


### EN OFERTA


### Kerbal Space Program


Desarrolador: Squad


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube.com/embed/9y9OiLHzD74" title="YouTube video player" width="720"></iframe></div>


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/220200/" style="border: 0px;" width="646"></iframe></div>


 




---


### Tails of Irons


Desarrolador: Odd Bug Studio


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube.com/embed/dURR7WW6MGA" title="YouTube video player" width="720"></iframe></div>


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/1283410/" style="border: 0px;" width="646"></iframe></div>


 




---


### Indivisible


Desarrollador: Lab Zero Games


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube.com/embed/ndzu-A4NGLs" title="YouTube video player" width="720"></iframe></div>


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/421170/" style="border: 0px;" width="646"></iframe></div>




---

