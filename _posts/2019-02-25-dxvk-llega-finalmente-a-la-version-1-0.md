---
author: leillo1975
category: Software
date: 2019-02-25 22:06:32
excerpt: "<p>En poco m\xE1s de un a\xF1o el proyecto ha progresado de forma increible</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/d29f0d8464ae634a0159c9d567754fce.webp
joomla_id: 980
joomla_url: dxvk-llega-finalmente-a-la-version-1-0
layout: post
tags:
- wine
- steam-play
- dxvk
- proton
- doitsujin
title: "DXVK llega finalmente a la versi\xF3n 1.0."
---
En poco más de un año el proyecto ha progresado de forma increible

El avance que han experimentado en este último año los juegos en nuestro sistema, se debe en gran medida a [Steam Play / Proton](index.php/homepage/generos/software/12-software/960-steam-play-de-valve-da-un-salto-de-gigante-y-amplia-enormemente-el-catalogo-para-linux-steamos-actualizado), y como todos sabeis, esta herramienta desarrollada por Valve se funda principalmente en dos proyectos. El primero es Wine, como es lógico, y el segundo el destacable trabajo de **Philip Rebohle**, alias "**doitsujin**", creador de **DXVK**, un "plugin" que aprovecha las excepcionales capacidades de **Vulkan** para mostrar el contenido de los juegos desarrollados en Windows con las API's **Direct3D 10 y 11**. Durante este último año ha hecho que tanto la compatibilidad como el rendimiento de muchos juegos se viese incrementada exponencialmente, casi semana a semana con versiones constantes que cada vez contenían más y más mejoras. El progreso fué tan expectacular que valió para que Valve se fijase en él y enseguida le fichase para su proyecto.  



Hace escasos momentos acabamos de recibir la fantástica noticia de que finalmente se ha alcanzado la icónica versión 1.0, y que aun encima [viene cargada de novedades](https://github.com/doitsujin/dxvk/releases/tag/v1.0) (recomendable consultar). Aquí os dejamos las que nos parecen **más destacables**:  



-Añadida la opción DXVK_HUD=api para mostrar el nivel de funcionalidad D3D utilizado por la aplicación. Aún no funciona correctamente con D3D10


-Mejoras menores en el rendimiento de RADV al generar mejor código de sombreado.


-Mejoras menores en la sobrecarga de la CPU en algunos escenarios.


-Si están disponibles, las extensiones VK_EXT_memory_priority y VK_EXT_memory_budget se utilizan ahora para mejorar el comportamiento bajo la presión de la memoria y para informar de la VRAM disponible a las aplicaciones con mayor precisión, respectivamente.


-Resident Evil 2: Habilitó la nueva opción d3d11.relaxedBarriers para mejorar el rendimiento hasta un 10% en escenarios con GPU limitada. Esto puede causar problemas con otros juegos.


-Arreglos en diversos juegos como en Far Cry 3 / 4 / Blood Dragon, Far Cry Primal, Final Fantasy XIV, Heroes of the Storm, Monster Hunter World y Overwatch.


Esperemos que más pronto que tarde, desde Valve, implementen esta versión en Proton, así como usar como base la versión 4.0 de Wine, y que por supuesto este desarrollo no se detenga y siga avanzando a este ritmo. Seguiremos informando....

