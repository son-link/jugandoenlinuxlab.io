---
author: Serjor
category: Aventuras
date: 2017-07-19 21:47:00
excerpt: "<p>Ad\xE9ntrate en un mundo de aventura, fantas\xEDa y crafteo multitudinario</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/8e6a8be31a229b213cafae60019138de.webp
joomla_id: 413
joomla_url: albion-online-ya-disponible-para-gnu-linux
layout: post
tags:
- albion-online
- mmorpg
title: Albion Online ya disponible para GNU/Linux
---
Adéntrate en un mundo de aventura, fantasía y crafteo multitudinario

Ya os comentábamos en varios apt-get, como [este](index.php/homepage/apt-get-update/item/531-una-semana-mas-os-traemos-un-apt-get-con-las-ultimas-novedades), que [Albion Online](https://albiononline.com) estaría disponible a partir del lunes 17 de julio, y después de un lanzamiento progresivo en función del tipo de compra, ya está disponible para todos.


Albion Online es una mezcla de MMORPG con toques de sandbox, con soporte multiplataforma, pero de la de buena, ya que a parte de GNU/Linux, Mac y Windows, está, o estará al poco del lanzamiento, para Android e iOS, puediéndote llevar la partida allí donde quieras.


Según la propia desarrolladora:



> 
> Albion Online es un MMORPG de mundo abierto ambientado en la fantasía medieval. Tiene una economía totalmente impulsada por los jugadores; Todos los elementos de equipo pueden ser hechos por el jugador. Puedes combinar libremente las piezas y las armas de la armadura en nuestro sistema sin clase único - eres lo que usas. Explora el mundo y hazle frente al desafiante contenido de PvE. Involúcrate con otros aventureros en PvP de pequeña o gran escala y conquista territorios. Reúne. Crea. Comercia. Conquista. Deja tu marca en el mundo.
> 
> 
> 


Es de agradecer que haya soporte para GNU/Linux desde el día 1, y aunque por lo visto el primer día tuvieron algún que otro problema de conexión por la alta demanda de clientes, la cosa ya funciona correctamente.


Tiene varias opciones de compra, la más económica, 29.95$ (unos 25€ al cambio en el momento de escribir el artículo), la intermedia, 49.95$ y 99.95$ para los más... jugones, con un aumento de equipamiento o artículos exclusivos según aumenta el precio.


Es de pago único, pero tiene una modalidad premium que varía desde los 9.95$ hasta los 6.95$ en función del periodo de compra, que hace que consigamos mayor experiencia y objetos en menor tiempo (pay to win).


Podéis encontrar los diferentes precios [aquí](https://albiononline.com/es/shop/)


Quizás sea demasiado pronto para poder hablar del éxito o fracaso de Albion Online, pero sí que podemos afirmar que es todo un logro que tengamos un MMORPG en GNU/Linux desde el primer día, y que otros cómo Guild Wars 2 que ya lleva años en el mercado y tiene cliente para Mac, ni se planteen el soporte nativo.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/drdk-suZp4M" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Y tú, ¿te vas a apuntar a la aventura? Cuéntanoslo en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD)

