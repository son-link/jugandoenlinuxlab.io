---
author: leillo1975
category: Editorial
date: 2019-03-21 12:10:46
excerpt: "<p>Parece mentira, pero poco a poco hemos llegado a esta cifra que sinceramente\
  \ nos sorprende. Ciertamente no es un n\xFAmero alto de suscriptores el de <a href=\"\
  https://www.youtube.com/c/jugandoenlinuxcom\" target=\"_blank\" rel=\"noopener\"\
  >nuestro canal de Youtube</a>, especialmente si lo comparamos con otros muchos que\
  \ hay en este conocido portal de videos; pero tambi\xE9n es cierto que es mucho\
  \ m\xE1s de lo que esper\xE1bamos teniendo en cuenta que nos dirigimos a un p\xFA\
  blico que es el nicho de otro nicho, y que tambi\xE9n realmente somos unos aficionados,\
  \ que nuestros medios t\xE9cnicos, conocimientos y&nbsp; sobre todo nuestras cualidades\
  \ sobre las artes de comunicar, audiovisualmente hablando, son bastante limitadas.\
  \ Pero a\xFAn as\xED hemos querido ofreceros estos contenidos por que pensamos que\
  \ ser\xEDa una buena forma de complementar la informaci\xF3n que os ofrecemos, as\xED\
  \ como tambi\xE9n de tener la posibilidad de que vieseis lo que a veces es dif\xED\
  cil de ense\xF1ar con palabras.</p>\r\n<p>A lo largo de este tiempo tambi\xE9n hemos\
  \ ido aprendiendo poco a poco, sobre todo a trav\xE9s de nuestros numerosos errores,\
  \ que a su modo forman parte tambi\xE9n de la ideosincrasia de nuestro canal. Hemos\
  \ disfrutando juntos con numerosas partidas a t\xEDtulos variados, hemos estrenado\
  \ juegos el mismo d\xEDa de su lanzamiento (gracias desarrolladores y editores por\
  \ facilitarnos el trabajo) para que pudieseis contemplar sus caracter\xEDsticas\
  \ m\xE1s all\xE1 de lo escrito; hemos probado multiples juegos sin soporte gracias\
  \ a Wine y Proton, y tambi\xE9n hemos recorrido las carreteras de media Europa y\
  \ America en nuestros viajes en cami\xF3n.</p>\r\n<p>Nos gustar\xEDa daros a todos\
  \ las gracias, por haber pulsado ese bot\xF3n de suscribirse, y por tener la paciencia\
  \ de aguantarnos en nuestros m\xFAltiples fallos y carencias comunicativas, pues\
  \ gracias a ello (y a vuestros Likes y comentarios) cada vez nos animais a crear\
  \ m\xE1s y m\xE1s contenido, e intentar hacerlo mejor. .... Y ahora a por los 400\
  \ suscriptores!!!&nbsp; Os dejamos con algunos de nuestros videos</p>\r\n<p><iframe\
  \ src=\"https://www.youtube.com/embed/qUo2UeWQ4-4\" width=\"560\" height=\"315\"\
  \ allowfullscreen=\"allowfullscreen\" allow=\"accelerometer; autoplay; encrypted-media;\
  \ gyroscope; picture-in-picture\"></iframe> <iframe src=\"https://www.youtube.com/embed/w8iMxf0q8tI\"\
  \ width=\"560\" height=\"315\" allowfullscreen=\"allowfullscreen\" allow=\"accelerometer;\
  \ autoplay; encrypted-media; gyroscope; picture-in-picture\"></iframe> <iframe src=\"\
  https://www.youtube.com/embed/VboGV7tf0Fg\" width=\"560\" height=\"315\" allowfullscreen=\"\
  allowfullscreen\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\"\
  ></iframe> <iframe src=\"https://www.youtube.com/embed/h-_JvxZHRDU\" width=\"560\"\
  \ height=\"315\" allowfullscreen=\"allowfullscreen\" allow=\"accelerometer; autoplay;\
  \ encrypted-media; gyroscope; picture-in-picture\"></iframe> <iframe src=\"https://www.youtube.com/embed/xxwds2kAD5U\"\
  \ width=\"560\" height=\"315\" allowfullscreen=\"allowfullscreen\" allow=\"accelerometer;\
  \ autoplay; encrypted-media; gyroscope; picture-in-picture\"></iframe> <iframe src=\"\
  https://www.youtube.com/embed/bkj2PuUTRGc\" width=\"560\" height=\"315\" allowfullscreen=\"\
  allowfullscreen\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\"\
  ></iframe> <iframe src=\"https://www.youtube.com/embed/ruuQCCEC094\" width=\"560\"\
  \ height=\"315\" allowfullscreen=\"allowfullscreen\" allow=\"accelerometer; autoplay;\
  \ encrypted-media; gyroscope; picture-in-picture\"></iframe></p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/d684a5f1d6d18e9647b504aaaaf15048.webp
joomla_id: 1001
joomla_url: 200-suscriptores-200-gracias
layout: post
tags:
- youtube
- videos
- suscriptores
- agradecimiento
title: "\xA1200 suscriptores en Youtube, 200 gracias!"
---
Parece mentira, pero poco a poco hemos llegado a esta cifra que sinceramente nos sorprende. Ciertamente no es un número alto de suscriptores el de [nuestro canal de Youtube](https://www.youtube.com/c/jugandoenlinuxcom), especialmente si lo comparamos con otros muchos que hay en este conocido portal de videos; pero también es cierto que es mucho más de lo que esperábamos teniendo en cuenta que nos dirigimos a un público que es el nicho de otro nicho, y que también realmente somos unos aficionados, que nuestros medios técnicos, conocimientos y  sobre todo nuestras cualidades sobre las artes de comunicar, audiovisualmente hablando, son bastante limitadas. Pero aún así hemos querido ofreceros estos contenidos por que pensamos que sería una buena forma de complementar la información que os ofrecemos, así como también de tener la posibilidad de que vieseis lo que a veces es difícil de enseñar con palabras.


A lo largo de este tiempo también hemos ido aprendiendo poco a poco, sobre todo a través de nuestros numerosos errores, que a su modo forman parte también de la ideosincrasia de nuestro canal. Hemos disfrutando juntos con numerosas partidas a títulos variados, hemos estrenado juegos el mismo día de su lanzamiento (gracias desarrolladores y editores por facilitarnos el trabajo) para que pudieseis contemplar sus características más allá de lo escrito; hemos probado multiples juegos sin soporte gracias a Wine y Proton, y también hemos recorrido las carreteras de media Europa y America en nuestros viajes en camión.


Nos gustaría daros a todos las gracias, por haber pulsado ese botón de suscribirse, y por tener la paciencia de aguantarnos en nuestros múltiples fallos y carencias comunicativas, pues gracias a ello (y a vuestros Likes y comentarios) cada vez nos animais a crear más y más contenido, e intentar hacerlo mejor. .... Y ahora a por los 400 suscriptores!!!  Os dejamos con algunos de nuestros videos


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/qUo2UeWQ4-4" width="560"></iframe></div> <div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/w8iMxf0q8tI" width="560"></iframe></div> <div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/VboGV7tf0Fg" width="560"></iframe></div> <div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/h-_JvxZHRDU" width="560"></iframe></div> <div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/xxwds2kAD5U" width="560"></iframe></div> <div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/bkj2PuUTRGc" width="560"></iframe></div> <div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/ruuQCCEC094" width="560"></iframe></div>

