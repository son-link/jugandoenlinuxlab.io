---
author: P_Vader
category: Software
date: 2022-12-01 13:31:58
excerpt: "<p>Los a\xF1adidos y mejoras son las mayores en mucho tiempo, finalizando\
  \ caracteristicas esperadas desde su nacimiento.</p>\r\n"
image: https://user-images.githubusercontent.com/26871415/204556120-04f7a507-f658-4514-93f6-121be7aa0b61.png
joomla_id: 1511
joomla_url: heroic-2-5-0-la-gran-actualizacion
layout: post
tags:
- proton
- heroic-games-launcher
- steam-deck
title: "Heroic Games Launcher 2.5.0. La gran actualizaci\xF3n."
---
Los añadidos y mejoras son las mayores en mucho tiempo, finalizando caracteristicas esperadas desde su nacimiento.


Ya tenemos la famosa "tienda" de juegos [Heroic Games Launcher](https://heroicgameslauncher.com/) en su  versión 2.5.0 'Yamato', **la que probablemente sea su lanzamiento más grande en mucho tiempo y con nuevas características que podriamos decir que cierran una fase del proyecto.** Se han centrado en añadir esas novedades que se querían implementar desde el nacimiento de la aplicación.


Vamos a mostraros lo mas destacado de esta nueva versión:


* **Administrador de descargas:**


Este era sin duda una característica muy esperada y cambia completamente cómo se usará Heroic a partir de ahora. Habrá un antes y un después al 'gestor de descargas'. No fue simple de implementar y probablemente tiene algunos errores, pero este es solo el comienzo que irá madurando con el tiempo.


En resumen, **ahora puedes añadir tantos juegos como quieras descargar o actualizar a una cola y Heroic se asegurará de ir al siguiente en cola después de terminar el actual**.


Todavía hay algunas cosas para mejorar como ser capaz de cambiar el orden de cola y verificar el espacio restante en el disco antes de agregar un elemento a la cola. Pero esto llegará muy pronto.


![Heroic](https://user-images.githubusercontent.com/26871415/204556120-04f7a507-f658-4514-93f6-121be7aa0b61.png)


* **Añadir cualquier aplicación o juego, nativo o no:**

Otra gran característica que tenemos en esta versión es la capacidad de agregar cualquier juego o aplicación en tu PC que no están en la cuenta de juegos GOG o Epic. En resumen, **ahora se puede utilizar Heroic incluso sin una cuenta en estas tiendas. En Linux puedes añadir aplicaciones nativas o binarias como Scripts, AppImages, etc. Y por supuesto las aplicaciones y juegos de Windows usándolo con Wine Proton, etc.**

Es bastante simple de usar como todo lo demás en Heroic. Simplemente hace clic en AÑADIR JUEGO en la biblioteca y rellene el formulario. Después de escribir el título, **Heroic obtiene una imagen automáticamente de [SteamGridDB](https://www.steamgriddb.com/)**, incluso puede ejecutar el instalador primero y seleccionar el ejecutable después.

![Heroic](https://user-images.githubusercontent.com/26871415/204556469-34bdb09b-ae5d-452f-b430-e8c54daced3d.png)


* **Añadidos datos de [HowLongToBeat](https://howlongtobeat.com/) en la página de juego:**

Ahora en la página de juego **puedes ver los tiempos para acabar un juego**, útil si tienes poco tiempo y poder elegir bien tu próximo juego.

![Heroic](https://user-images.githubusercontent.com/26871415/204556793-ebab77fa-e85c-4759-802e-731aebf8de43.png)


* **Ajuste para añadir un juego a Steam automáticamente (bastante útil en la SteamDeck):**

![Heroic](https://user-images.githubusercontent.com/26871415/204540491-729dd68b-636d-44fd-bef9-a445eee93111.jpg)

* **Detección si un juego está disponible o no y lista juegos no compatibles:**

Heroic ahora puede detectar si la carpeta del juego está disponible o no y le muestra esa información al usuario. Esto es muy útil cuando instalas juegos en una unidad externa o en una tarjeta SD y lo eliminas. También si borraste manualmente la carpeta del juego y no puedes desinstalar el juego. Así que **si haces clic en PLAY en un juego no disponible, le preguntará si desea eliminarlo de la lista instalada.**

![Heroic](https://user-images.githubusercontent.com/26871415/204145578-37da978f-2ec1-4b42-be41-c5acb16d63bc.png)


Heroic también **muestra cuando un juego no está soportado, normalmente por un lanzador de un tercero como Origin**. Como estos juegos no se descargan de Epic, algunas personas estaban confundidas por qué no estaban apareciendo en su biblioteca. Así quedan las cosas claras.


![Heroic](https://user-images.githubusercontent.com/26871415/204144607-bd075ac7-f831-459f-8d4b-d4a19b4185a7.png)


* **Las opciones de "herramientas" fueron trasladadas a un sub-menú dentro de la página del juego:**


![Heroic](https://user-images.githubusercontent.com/26871415/204541075-a548ed62-97c6-462c-9042-c26b6b4e9af2.jpg)


* **Reorganización de los ajustes:**

Todos los ajustes fueron reorganizados por lo que son más fáciles de encontrar y también está claro si los ajustes que estás cambiando es global o del juego.

![Heroic](https://user-images.githubusercontent.com/26871415/204541260-ec981c5b-c146-433c-ac84-d8b054be3af7.jpg)

Otros cambios y mejores fueron:

* Mejor rendimiento en la interfaz de usuario en general, por lo que algunas páginas cargan sin demora.
* Mejor sistema en línea/desconectado para que Heroic pueda manejarse mejor cuando no hay red disponible.
* La navegación GamePad fue rehechada por lo que ahora las Tarjetas de la Biblioteca no muestran ningún botón y también los comandos fueron cambiados para funcionar exactamente como en SteamOS para una mejor familiaridad.
* Changelog se muestra ahora después de que Heroic se actualiza.
* La pantalla de inicio de sesión fue rediseñada para un aspecto más moderno.
* Añadida dos nuevos temas: Nord Light y Nord Dark.
* Ahora es posible forzar una instalación incluso si Heroic detecta queda que no hay espacio en el dispositivo.
* Heroico mostrará una advertencia en caso de que no haya iniciado sesión en una tienda e intente acceder a ella desde la barra lateral.
* Actualizado Legendary y Electron a las últimas versiones.
* Varias correcciones y mejoras.

¿Qué te parece esta enorme actualziación? ¿Lo has probado? Cuéntanoslo en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix.](https://matrix.to/#/+jugandoenlinux.com:matrix.org)
