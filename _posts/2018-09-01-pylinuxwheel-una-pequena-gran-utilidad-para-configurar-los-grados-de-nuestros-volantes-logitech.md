---
author: leillo1975
category: Software
date: 2018-09-01 14:43:20
excerpt: "<p>El programa de @OdinTdh, se actualiza a la versi\xF3n 0.3</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/f6e095e44349bba4e0775cb7dacd0d59.webp
joomla_id: 849
joomla_url: pylinuxwheel-una-pequena-gran-utilidad-para-configurar-los-grados-de-nuestros-volantes-logitech
layout: post
tags:
- logitech
- utilidad
- volantes
title: "pyLinuxWheel, una peque\xF1a-gran utilidad para configurar nuestros volantes\
  \ Logitech (ACTUALIZADO 2)"
---
El programa de @OdinTdh, se actualiza a la versión 0.3

**ACTUALIZADO 1-7-19:** Parece que la cosa va hoy de volantes, porque hace un momentito acabo de actualizar el artículo de Oversteer y ahora le toca a PyLinuxWheel, que en tiempo record ha llegado a la **versión 0.3** (hace 10 días **@OdinTdh** lanzó la 0.2 como podeis ver un poco más abajo). Las [novedades](https://gitlab.com/OdinTdh/pyLinuxWheel/-/tags/v0.3) con respecto a la anterior versión son jugosas como podeis ver en la siguiente lista:


*-Se ha añadido un **instalador automático de las reglas de udev** de Feral para que los volantes Logitech usen el programa sin root. **Gracias a Feral** por darnos permiso para usar sus reglas udev.*  
*-pyLinuxWheel **recuerda el último valor guardado** entre sesiones.*  
*-**Nueva pestaña de preferencias** para ajustar el comportamiento del programa. También tiene la opción de forzar la actualización de las reglas udev.*


Aunque no salga en las notas, también **se ha cambiado la marca de 240º a 270º** (gracias) para que se adapte mejor a algunos juegos de coches antiguos. Podeis encontrar este programa en la [página del proyecto](https://gitlab.com/OdinTdh/pyLinuxWheel), y también descargar la [versión AppImage](https://bintray.com/odintdh/AppImages/download_file?file_path=pyLinuxWheel-v0.3-x86_64.AppImage) para tenerlo todo en un solo archivo y ejecutarlo más fácil.


Os recordamos que si quereis darle un buen uso a PyLinuxWheel, podeis participar en nuestra [Liga JugandoEnLinux](https://www.dirtgame.com/es/leagues/league/88825/jugandoenlinux) de [DIRT Rally](index.php/homepage/analisis/20-analisis/362-analisis-dirt-rally), donde podeis competir con otros miembros de nuestra comunidad en las mejores etapas de este magnífico juego. Si quereis más información, pasaos por nuestra [Comunidad de Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org) y entrad en la **sala DIRT Rally.**




---


**ACTUALIZADO 21-6-19**: Tras varios meses desde el primer lanzamiento oficial, el programa **PyLinuxWheel** de nuestro colaborador **OdinTdh** es noticia de nuevo después de que haya alcanzado la **versión 0.2** que trae novedades muy interesantes para poder configurar nuestros volantes de una forma sencilla. La más importante es la **posibilidad de descargarnos una versión en AppImage**, posibilitando esto que podamos ejecutarla de una forma mucho más sencilla, pero tal y como podemos leer en las [notas de la versión](https://gitlab.com/OdinTdh/pyLinuxWheel/-/tags/v0.2) se han hecho también algunos cambios más:


*- Solución en la aplicación para **corregir el desajuste entre los grados reales y los grados reportados** por el driver para el volante  Driving Force GT (DFGT) .*  
*- **Botón Actualizar** para forzar la carga del último valor guardado en el controlador.*  
*- Añadida la opción de **combinar pedales**.*


Podeis descargar el programa de la [página del proyecto](https://gitlab.com/OdinTdh/pyLinuxWheel) en GitLab, o usar la [AppImage](https://bintray.com/odintdh/AppImages/download_file?file_path=pyLinuxWheelv0.2-x86_64.AppImage) si no quereis complicaros la vida.




---


**NOTICIA ORIGINAL:**  
Si sois aficionados a los juegos de conducción y disponeis de un volante Logitech, sabreis que desde hace algún tiempo los volantes de esta conocida marca tienen un soporte bastante completo en nuestro sistema operativo. En un principio era necesario utilizar [LTWheelConf](https://github.com/TripleSpeeder/LTWheelConf) para poder aprovechar las bondades de estos perfiericos en Linux, pero desde hace unos años el propio Kernel incluye el soporte a estos dispositivos. Gracias a esto, la mayor parte de los gamers Linuxeros que tienen un volante, usan los de esta compañía.


Personalmente, y como ávido consumidor de juegos de esta categoría, en más de una ocasión me encontraba con problemas a la hora de configurar los grados habituales en los volantes, ya que el valor por defecto de mi G27 es 900º, lo que es un rango exagerado para juegos como DIRT Rally o Grid Autosport. Antes disponía de un Driving Force GT y tenía el mismo problema aunque a la inversa, ya que cada vez que quería jugar a Euro Truck Simulator 2,  necesitaba 900º y el volante por defecto se calibra con muchos menos. Para poder poner los grados adecudados había que tirar de consola y modificar el valor del rango a mano, o utilizar scripts.


Debido a esto ha nacido una aplicación en nuestra propia comunidad que facilita mucho las cosas por que dispone de una interfaz totalmente gráfica, donde usando una simple barra de desplazamiento podremos escoger los grados de giro que más nos convengan para el juego en cuestión o nuestros propios gustos. Su nombre es **pyLinuxWheel** y está escrito, como podeis averiguar por el nombre, en Python3, y sobre las librerias GTK. Esta licenciada bajo la GPLv3, y la [página del proyecto](https://gitlab.com/OdinTdh/pyLinuxWheel) se aloja en GitLab desde donde también podreis descargarlo.


![pyLinuxWheelVentana](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/pyLinuxWheelVentana.webp)


En este momento tiene **soporte para los siguientes volantes**: Driving Force GT, G25, G27 y G29. En el resto de modelos la aplicación se bloquea para evitar cambios malos en los valores del driver. En estas primeras versiones es necesario preceder la ejecución del programa del comando sudo para ejecutarlo con derechos administrativos, aunque [si habeis cambiado las reglas del volante con F1 2017](index.php/foro/tutoriales/47-activar-todas-las-funciones-del-volante-en-f1-2017) tal y como recomendaba Feral no os será necesario.También nos gustaría recordar que esta aplicación acaba de nacer hace escasos días, por lo que es probable que tenga fallos o necesite pulirse más.


 Por cierto, **@OdinTdh**, gracias por la mención a JugandoEnLinux en los créditos, y por supuesto por la aplicación.


 


¿Sois usuarios de Volantes Logitech? ¿Qué os parece esta utilidad? Podeis opiniar en los comentarios o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).


 

