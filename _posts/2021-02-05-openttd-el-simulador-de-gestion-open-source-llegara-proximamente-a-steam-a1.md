---
author: Pato
category: "Simulaci\xF3n"
date: 2021-02-05 17:38:30
excerpt: "<p>Con la nueva actualizaci\xF3n finalmente el #OpenSource #<span class=\"\
  css-901oao css-16my406 r-poiln3 r-bcqeeo r-qvutc0\">OpenTTD recala en la conocida\
  \ tienda<span style=\"text-decoration: underline;\"><strong>.</strong></span></span></p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/OpenTTD/OpenTTD.webp
joomla_id: 1276
joomla_url: openttd-el-simulador-de-gestion-open-source-llegara-proximamente-a-steam-a1
layout: post
tags:
- simulador
- open-source
- tycoon
- openttd
title: "OpenTTD, el simulador de gesti\xF3n ha llegado a Steam (ACTUALIZADO)"
---
Con la nueva actualización finalmente el #OpenSource #OpenTTD recala en la conocida tienda**.**


**ACTUALIZACIÓN 6-4-21**: Seguro que esta noticia tendría que estar hace ya casi una semana publicada, pero somos humanos, y además con poco tiempo. El caso es que finalmente ha sido publicada la [versión 1.11.0](https://www.openttd.org/news/2021/04/01/openttd-1-11-0.html), que entre otras cosas, que podeis ver un poco más abajo, se puede al fin descargar y disfrutar desde [Steam](https://store.steampowered.com/app/1536610/OpenTTD/), por lo que a partir de ahora estará disponible en esa tienda para el que todo el que lo prefiera, además de estar mucho más visible para el público en general. Los cambios de esta versión son los siguientes:


***Mejoras en el rendimiento** - El avance rápido es ahora tan rápido que hay un ajuste para limitarlo (activado por defecto).*  
 ***GUI de Generación de Mundos mejorada** - Ahora hay mapas con 100% de selva tropical si lo solicitas.*  
 ***Las ciudades ahora también pueden crecer con túneles** - ¡por fin pueden escapar de las colinas!*  
 ***Filtro en el nombre en muchas más ventanas** - para aquellos que disfrutan jugando con un NewGRF de más.*  
 ***Plantar árboles con un pincel en el Editor de Escenarios** - es como pintar, pero con árboles.*  
 *Un nuevo título de juego para esta versión por Chrnan6710, que ganó el concurso de este año.*  
*También se han corregido más de 100 errores y han añadido / cambiado / mejorado más de 100 cosas.*


Ya nos estais contando que tal os va con este mágnífico juego de simulación/gestion libre!




---


  
**NOTICIA ORIGINAL (5-2-21)**: No es muy habitual ver juegos indie u open source aparecer en las grandes plataformas de distribución, mas que nada por las propias características de muchas de ellas y el rechazo que producen entre ciertos grupos de usuarios. Sin embargo, a veces es por desconocimiento, ya que plataformas como la propia Steam, que permite la publicación de juegos open source pueden ser un gran escaparate para estos juegos, si dejamos de lado que se puede considerar el "control" que ejerce Steam como "cierto tipo de DRM".


Aparte de eso, Steam ofrece una plataforma más que solvente para **desarrollo, control y gestión de versiones,** está presente en todo el ecosistema del ordenador personal y ofrece una gran oportunidad para gestionar comunidades de jugadores, cualquiera que sea el tipo y tamaño aparte de ser "el escaparate virtual" con mayor número de usuarios. Es por eso que los desarrolladores de OpenTTD están pensando dar el salto a su distribución mediante Steam.



> 
> Históricamente, OpenTTD siempre tuvo un solo hogar desde donde distribuíamos el juego. [...]  
> Pero los tiempos están cambiando, al igual que nuestro cabello. Durante los últimos meses, hemos estado trabajando en silencio para ser un poco más visibles en el mundo. No te preocupes, no por las razones que podrías pensar: OpenTTD tiene tantos usuarios activos como tenía en 2007. Pero es más porque ya no creemos que sea el enfoque correcto distribuir solo a través de nuestro propio sitio web.
> 
> 
> Esto se volvió dolorosamente evidente cuando notamos que otras personas publicaban OpenTTD en algunas tiendas. No siempre se actualizan con nuevos lanzamientos, a veces incluso se retrasan unos años. Y quizás lo más importante para nosotros: no podemos garantizar que la versión cargada no esté modificada y sea la versión que pretendíamos. Entonces, en lugar de luchar contra esto, ¿por qué no dar la vuelta y unirse a ellos? ¿Por qué no lanzar nuestras propias versiones verificadas a partir de esas tiendas?
> 
> 
> 


*OpenTTD es un juego de simulación empresarial en el que los jugadores ganan dinero transportando pasajeros y carga por carretera, ferrocarril, agua y aire. Es una nueva versión de código abierto y una expansión del videojuego de Chris Sawyer Transport Tycoon Deluxe de 1995.*


Puedes descargar OpenTTD desde su [página web oficial,](https://www.openttd.org) y **el próximo 1 de Abril llegará al bazar de Valve**, aunque ya puedes ponerlo en tu lista de deseados desde su [página del juego en Steam](https://store.steampowered.com/app/1536610/OpenTTD/).


 

