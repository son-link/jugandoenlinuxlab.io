---
author: Pato
category: "Exploraci\xF3n"
date: 2018-08-01 18:48:42
excerpt: "<p>Adem\xE1s recibe una gran cantidad de contenido adicional</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/04ccfa5912a78134b8056676f3532d14.webp
joomla_id: 813
joomla_url: stardew-valley-lanza-su-actualizacion-1-3-y-ya-se-puede-jugar-en-modo-multijugador
layout: post
tags:
- aventura
- simulador
- exploracion
- agricultura
title: "Stardew Valley lanza su actualizaci\xF3n 1.3 y ya se puede jugar en modo multijugador"
---
Además recibe una gran cantidad de contenido adicional

Para todos aquellos que gustan de los juegos de simulación de granjas con un toque de aventura, el mas que excelente Stardew Valley es una opción mas que recomendable. Más teniendo en cuenta que en la actualización que hoy mismo ha recibido el juego ya está disponible el modo multijugador cooperativo, con lo que será posible jugar con los amigos dentro del mundo del juego.


*Stardew Valley ahora admite juego cooperativo de hasta 4 jugadores. Co-op es casi idéntico a un jugador, pero con otros 1 a 3 amigos jugando contigo para lograr un objetivo común. Cualquier juego individual se puede "convertir" a un juego cooperativo al hacer que Robin construya una o más cabañas en su granja.  
  
Un jugador sirve como anfitrión, y los otros 1-3 jugadores se conectan al host para poder jugar. Por lo tanto, el anfitrión debe estar en el juego en todo momento cuando el grupo quiera jugar.  
  
La nueva actualización agrega un botón Co-op a la pantalla de título. Al hacer clic en el botón, accederás al menú del cooperativo, desde donde podrás:  
  
     Organizar un nuevo juego cooperativo  
     Reanudar un juego cooperativo existente  
     Únirse a una nueva granja (siempre que alguno de tus amigos sea anfitrión y tenga una cabaña disponible para ti)  
     Volver a unirse a una granja (siempre que el host esté dentro del juego)  
     Unirse a un juego LAN (ingresando la dirección IP del host)  
     Ingresar un código de invitación (generado por el host) para unirse a un juego (esto permite el cruce de Steam / GOG)*


Para conmemorar el lanzamiento de esta actualización han publicado un nuevo vídeo promocional:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/-vlBE3vSfNU" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Como ya hemos dicho, aparte de esto hay muchas otras novedades. Puedes verlas en el [anuncio oficial](https://steamcommunity.com/games/413150/announcements/detail/1694928687235498030) de la actualización.


Tienes disponible Stardew Valley en [Humble Bundle](https://www.humblebundle.com/store/stardew-valley?partner=jugandoenlinux) (link afiliado), [GOG](https://www.gog.com/game/stardew_valley) o [Steam](https://store.steampowered.com/app/413150/Stardew_Valley/).

