---
author: P_Vader
category: "Acci\xF3n"
date: 2021-08-26 08:48:25
excerpt: "<p>Oficialmente el exitoso @splitgate sale de beta y lanza su primera temporada.</p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/splitgate/splitgate_season_0.webp
joomla_id: 1347
joomla_url: splitgate-lanza-la-temporada-0
layout: post
tags:
- multijugador
- gratis
- fps
- masivo
- splitgate
title: Splitgate lanza la Temporada 0
---
Oficialmente el exitoso @splitgate sale de beta y lanza su primera temporada.


Después de [algunos retrasos tras un éxito meteórico]({{ "/posts/splitgate-un-fps-gratuito-multijugador-mezcla-entre-quake-y-portal" | absolute_url }}) con la versión Beta, finalmente **ha llegado el gran día, [Splitgate es oficialmente](https://www.splitgate.com/blogs/splitgate-season-0-launch) lanzado junto a su primera temporada, la número 0.**


Desde el primer momento **hemos tenido la suerte de contar con versión nativa** y soportada para nuestro sistema operativo y es de agradecer ya que se postula para ser un titulo de éxito. Como decían en [kotaku](https://kotaku.com/splitgate-not-only-slaps-its-the-best-arena-shooter-iv-1847420433):


> 
> Splitgate no son solo tortas, es el mejor shoter arena que he jugado en años. Es brillante.
> 
> 
> [...]es un poco sorprendente que otro desarrollador no haya hecho esto antes.
> 
> 
> 


Y no puedo estar mas de acuerdo, **las posibilidades que abre este juego al mezclar un FPS tipo Quake o Halo (como les gusta mayormete compararlo) con los portales del mítico Portal son de una genialidad absoluta**.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="440" src="https://gfycat.com/ifr/PinkNearGenet" width="780"></iframe></div>

Sin mas vamos a repasar todas la novedades de este lanzamiento:


* **Estación Karman**: una reinvención del mapa original, Outpost. La estación Karman reemplaza a Outpost en la lista de mapas. Aquellos que jugaron antes de la Beta reconocerán las similitudes, pero lo más importante son ¡las mejoras!


![Splitgate UpdatedKarman](https://assets-global.website-files.com/5c72f0026e5e68e120ecd951/61269417d2f9053d6bfcb8a0_UpdatedKarman_1080.png)


* **Pase de batalla de la temporada 0:** El pase de batalla tendrá 100 niveles de elementos nuevos, exclusivos del pase. El pase incluye: nuevas armaduras, diseños de armas, etiquetas de nombre, carteles y un diseño de portal. Los desafíos de temporada están completamente desbloqueados con el pase de batalla


![Splitgate pase de batalla](https://assets-global.website-files.com/5c72f0026e5e68e120ecd951/61269426dcc4b15bad090c14_Level1BP.jpg)


* **Contaminación**: nuevo modo asimétrico con dos equipos. El equipo "Contaminado" comienza con bates solamente y busca destrozar al equipo "Humano", que tiene armas. Cuando un miembro del equipo humano muere, reaparece en el equipo contaminado. El juego termina cuando se acaba el tiempo o si todos los jugadores están contaminados.
* **Casual Team Rumble**: Una nueva lista de casuales con varios modos de juego demasiado extravagantes para el casual normal. Los modos de juego incluyen Contaminación, Big Head Snipers, Splitball y más.


![Slitgate Team Rumble](https://assets-global.website-files.com/5c72f0026e5e68e120ecd951/612693fec1944e59c0e466d1_TeamRumble.jpg)


* **Final Kill Cam**: función agregada por demanda popular, ahora puedes ver la muerte final del juego.
* **Calibración de pistola** - Cambios de equilibrio: totalmente automático, cadencia de disparo de .125, cuerpo de 15,5, cabeza de 17,5.


**Cambios que pedía la gente**:


* Control deslizante de asistencia para apuntar: algunos de vosotros solicitaron que pudieran desactivar / reducir la asistencia para apuntar de los mandos. 100%
* Prorrogas: King of the Hill, VIP y Oddball ahora tienen prorrogas.
* Hacer todos los desafíos diarios da una llave..
* Nuevo pase de recomendación: ¡9 niveles con nuevos elementos! Las nuevas referencias solo contarán para el nuevo pase de referencia.
* Navegador de juegos personalizados: búsqueda y filtrado adicionales.
* Ahora puedes disparar portales a través de los cuerpos de los aliados.
* Killcam está habilitado en todos los modos ahora (por ejemplo: SWAT)
* Rangos: se han agregado más divisiones Elo. es decir, ELO 3000, Diamante 1, ELO 3300, Diamante 4, etc.


![splitgate EloDivisions](https://assets-global.website-files.com/5c72f0026e5e68e120ecd951/612693e651715fdcb65c5401_EloDivisions.png)


* Cambio de equipo: opción de juegos personalizados para cambiar de equipo durante el partido.
* Mejoras en la visibilidad del texto de la zona de recogida.
* Se actualizaron los puntos de Hills y DOM en ciertos mapas.
* Hill Mesh actualizado.


**Y esto solo acaba de empezar. Esperemos que sea el principio de algo muy grand**e y disfrutarlo en nuestro sistema favorito **(¿Alguien dijo quiero jugarlo en una Steam Deck?)**


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/w5XKcTM_NM8" title="YouTube video player" width="780"></iframe></div>

¿Que te parece Splitgate? ¿Te ha enganchado tanto como a alguno de nosotros? Cuéntanoslo en nuestro canal de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org)

