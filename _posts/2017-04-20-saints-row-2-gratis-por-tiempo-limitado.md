---
author: leillo1975
category: "Acci\xF3n"
date: 2017-04-20 18:48:44
excerpt: "<p>El t\xEDtulo de Deep Silver se podr\xE1 adquirir a coste cero.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/36344ca48475f863c1fe44d1c1cfab3f.webp
joomla_id: 296
joomla_url: saints-row-2-gratis-por-tiempo-limitado
layout: post
tags:
- steam
- gog
- gratis
- virtual-progamming
- saints-row-2
- saints-row-the-third
- saints-row-iv
- saints-row-gat-out-of-hell
title: Saints Row 2 gratis por tiempo limitado
---
El título de Deep Silver se podrá adquirir a coste cero.

Esta mañana leía un [tweet de GOG.com](https://twitter.com/GOGcom/status/855005665076666368) donde informaba de la disponibilidad gratuita por un tiempo de Saints Row 2. Inmediatamente me lancé a por él para tenerlo también en GOG, pero cual fué mi sorpresa cuando vi que no estaba disponible para los usuarios de GNU-Linux y solamente estaba para Windows. Hace un momento, uno de nuestros lectores en nuestro canal de [Telegram](https://telegram.me/jugandoenlinux) (muchas gracias **Santos**), nos comentaba que el juego también se podía añadir a nuestra Biblioteca de Steam, y en este caso también para nuestro sistema. Concretamente se comentaba este tweet de **MuyLinux**:


 



> 
> Saints Row 2 gratis en Steam por tiempo limitado ^^. <https://t.co/ndIeqyYUql>
> 
> 
> — MuyLinux (@muylinux) [20 de abril de 2017](https://twitter.com/muylinux/status/855117459761897473)



 


Es importante resaltar que todos los juegos de la franquicia están de oferta con unos [descuentos de hasta el 75%](http://store.steampowered.com/sale/saints-row/), por lo que todos aquellos que no los tengais, teneis una oportunidad estupenda de poder disfrutarlos a un precio de risa. En cuanto a la tienda de GOG.com, también hay que decir que si disponeis de los títulos de la franquicia en Steam, podeis añadirlos también a esta plataforma gratuitamente mediante [**GOG Connect**](https://www.gog.com/connect).


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/_g7F7Pq-lh0" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


La saga Saints Row fué portada  hace algún tiempo con bastante acierto por **Virtual Programming**, pudiendo nosotros, los usuarios de Linux, jugar a las partes 2, 3, 4 y Gat Out Of Hell en nuestros equipos. En cuanto al juego que nos ocupa, su segunda parte, decir que su funcionamiento es un tanto decepcionante, aunque en este caso no es culpa de VP, sino que el original de Windows tenía muchos fallos y un pésimo rendimiento, ya que a su vez provenía de las consolas. Si quereis disponer de este juego simplemente os teneis que pasar por la página de la [tienda de Steam](http://store.steampowered.com/app/9480/):


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/9480/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


Si quieres dejar tu opinión sobre la saga Saints Row puedes dejar un comentario o usar nuestros grupos de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD).

