---
author: Jugando en Linux
category: Apt-Get Update
date: 2022-04-22 20:10:18
excerpt: ''
image: https://monolithofminds.com/assets/images/project-skyark.png
joomla_id: 1462
joomla_url: nuevos-juegos-con-soporte-nativo-22-04-2022
layout: post
tags:
- ofertas
- nativos
title: nuevos juegos con soporte nativo 22/04/2022
---

Volvemos a las buenas costumbres con un nuevo listado de juegos nativos y en oferta. Al lío que es finde y hay que jugar...


### Rocket Bot Royale

DESARROLLADOR: Winterpixel Games

<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube-nocookie.com/embed/E69G82ovrqY" title="YouTube video player" width="720"></iframe></div>

<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/1748390/" width="646"></iframe></div>

---

###  Lila’s Sky Ark

DESARROLLADOR: Monolith of Minds
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube-nocookie.com/embed/GS1PNbA2xko" title="YouTube video player" width="720"></iframe></div>

<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/1573390/" width="646"></iframe></div>

---

###  Godlike Burger

DESARROLLADOR: Liquid Pug
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube-nocookie.com/embed/aZsDOb7Wqyc" title="YouTube video player" width="720"></iframe></div>

<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/1432910/" width="646"></iframe></div>

---

### Terraformers

DESARROLLADOR: Asteroid Lab

<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube-nocookie.com/embed/NxIFctKaRsA" title="YouTube video player" width="720"></iframe></div>

<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/1244800/" width="646"></iframe></div>

---

###  Unconventional Warfare

DESARROLLADOR: Nightlife Strangers

<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube-nocookie.com/embed/CCE1iqNHpwg" title="YouTube video player" width="720"></iframe></div>

<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/831110/" width="646"></iframe></div>

---

OFERTAS
-------

### Barotrauma

DESARROLLADOR: FakeFish, Undertow Games
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube-nocookie.com/embed/RmN0y0m3YCU" title="YouTube video player" width="720"></iframe></div>

<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/602960/" width="646"></iframe></div>

---

### Iratus: Lord of the Dead

DESARROLLADOR: Unfrozen
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube-nocookie.com/embed/85OKSZ3Hd9s" title="YouTube video player" width="720"></iframe></div>

<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/807120/" width="646"></iframe></div>

---

###  The Witcher 2: Assassins of Kings Enhanced Edition

<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube-nocookie.com/embed/g6e1Rzk3OrQ" title="YouTube video player" width="720"></iframe></div>

<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/20920/8186/" width="646"></iframe></div>
