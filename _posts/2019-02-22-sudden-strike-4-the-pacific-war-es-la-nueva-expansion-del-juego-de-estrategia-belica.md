---
author: Pato
category: Estrategia
date: 2019-02-22 11:52:17
excerpt: <p>El excelente juego de @kite_games y @kalipsomedia sigue ampliando contenido</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/9063df0b1b4d9d96fd7fea653ac618e5.webp
joomla_id: 978
joomla_url: sudden-strike-4-the-pacific-war-es-la-nueva-expansion-del-juego-de-estrategia-belica
layout: post
tags:
- accion
- estrategia
title: "Sudden Strike 4: The Pacific War es la nueva expansi\xF3n del juego de estrategia\
  \ b\xE9lica"
---
El excelente juego de @kite_games y @kalipsomedia sigue ampliando contenido

Nos llega una nueva expansión para uno de los juegos de acción y estrategia bélica que mas se ha prodigado en Linux/SteamOS con permiso de la saga Company of Heroes. Hablamos de Sudden Strike 4 y su nueva expansión 'Sudden Strike 4: The Pacific War' que nos llevará al frente del pacífico con nuevos escenarios, vehículos, mecánicas y campañas.


*Disfruta como nunca de algunas de las batallas más cruentas del sudeste asiático durante la Segunda Guerra Mundial. La expansión **"The Pacific War"** para **Sudden Strike 4** te mete de lleno en el conflicto entre los Aliados y el Ejército Imperial Japonés.*


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/HYdblZvAlNs" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Características:


* **Sudden Strike 4: The Pacific War** incluye **dos nuevas campañas** con **10 nuevas misiones**ambientadas en la campaña del sudeste asiático durante la Segunda Guerra Mundial.
* Podrás elegir entre **6 nuevos comandantes**: del lado del Ejército Imperial Japonés, podrás seleccionar a **Soemu Toyoda**, almirante de la flota, o a **Hisaichi Terauchi**, comandante del Ejército Expedicionario del Sur; del lado de los Aliados, podrás elegir a **Douglas MacArthur**, el legendario general de cinco estrellas, o a **Richard J. Marshall**, jefe de Estado.
* La expansión **The Pacific War** añade **nuevas mecánicas de juego** a Sudden Strike, como lanzallamas, vehículos anfibios, grandes portaaviones manejables, combate aéreo avanzado y nuevas habilidades de unidad.
* **Sudden Strike 4: The Pacific War** incluye **51 vehículos nuevos**, como el Amtrac LTV(A)-4, el USS Enterprise CV-6, el Soko Sagyo Ki (SS-KI) o el portaaviones Shokaku.


Tienes disponible Sudden Strike 4: The Pacific War en español en su [página de Steam](https://store.steampowered.com/app/873871/Sudden_Strike_4__The_Pacific_War/) donde tiene un 15% de descuento por su lanzamiento.

