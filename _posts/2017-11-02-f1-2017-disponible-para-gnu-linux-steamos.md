---
author: leillo1975
category: Carreras
date: 2017-11-02 14:09:35
excerpt: "<p>Tal y como prometieron a principios de semana el juego ya est\xE1 disponible.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/d91d2793afc1e2281971343ae9f4138f.webp
joomla_id: 516
joomla_url: f1-2017-disponible-para-gnu-linux-steamos
layout: post
tags:
- feral-interactive
- vulkan
- codemasters
- f1-2017
title: "F1 2017 disponible para GNU-Linux/SteamOS (ACTUALIZACI\xD3N 3-11-17)"
---
Tal y como prometieron a principios de semana el juego ya está disponible.

Una vez más Feral lo ha vuelto a hacer y nos trae un título de primera categoría a nuestros equipos. Desde ya podemos disfrutar de un juego que ha recibido unas [críticas soberbias](http://www.metacritic.com/game/pc/f1-2017) por parte de todo tipo de medios especializados y que **ya es considerado como el mejor juego de F1 de la historia**, convirtiendose automáticamente en un clásico. Feral interactive ha realizado el anuncio, como suele ser habitual, a través de las redes sociales como podemos ver en este tweet:

> 
> Penguins, you’re invited to motorsport's most prestigious Championship.  
> Buy F1™ 2017 for Linux on the Feral Store: <https://t.co/UtSf7vGGof> [pic.twitter.com/sOS6OufKBv](https://t.co/sOS6OufKBv)
> 
> 
> — Feral Interactive (@feralgames) [November 2, 2017](https://twitter.com/feralgames/status/926115851161493504?ref_src=twsrc%5Etfw)


Como os [informábamos](index.php/homepage/generos/carreras/item/633-f1-2017-finalmente-llegara-a-gnu-linux-steamos-el-2-de-noviembre) el lunes pasado , la compañía de británica de ports, ha cumplido con lo prometido y ha lanzado sin retraso el juego. El título, como muchos sabreis **funcionará unicamente con la API de Vulkan**, no habiendo opción a OpenGL, lo cual es un importante paso hacia delante por parte de Feral, que "inagura" una nueva era en sus ports para nuestro sistema. También nos ha dejado los [requisitos](http://www.feralinteractive.com/es/news/823/) mínimos y recomendados que debemos cumplir si queremos ejecutar con garantías F1 2017, y que son los siguientes:


**Mínimos:**

> 
> Procesador: 3.3Ghz Intel Core i3-3225
> 
> 
> Sistema: Ubuntu 17.04
> 
> 
> RAM: 4 GB de RAM
> 
> 
> Gráfica: Nvidia 680 de 2 GB o 3º Generación de AMD Graphics Core Next (Volcanic Islands)
> 
> 
> 


**Óptimos:**



> 
> Procesador: 3.5Ghz Intel Core i5-6600K
> 
> 
> RAM: 8 GB de RAM
> 
> 
> Gráfica: Nvidia 1070 de 8 GB o superior.
> 
> 
> 


Las tarjetas gráficas Intel no son compatibles. **Las tarjetas gráficas Nvidia requieren la versión del controlador 384.90**+. **Tarjetas gráficas AMD requieren Mesa 17.2.2**.

Lo que **la compañía no ha aclarado aun es si se podrá jugar entre diferentes plataformas** sin problema, aunque es de esperar que si, como suele ser norma habitual en los juegos de carreras de Codemasters. F1 2017 incorpora múltiples novedades como es la inclusión de monoplazas clásicos de los últimos 30 años, trazados alternativos en los circuitos, y un completísimo modo de carrera profesional cargado de opciones de todo tipo, incluida la personalización. Un montón de nuevas características que nos harán olvidar la injustamente denostada versión de 2015, y que todos esperamos que sea un éxito de ventas. Como sabeis F1 2015 no vendió todo lo bien que se esperaba y [Feral canceló el port de 2016](index.php/homepage/generos/carreras/item/400-no-habra-version-para-linux-del-f1-2016) por ello.

Desde JugandoEnLinux.com os adelantamos que pronto os ofreceremos material videográfico en [nuestro canal de Youtube](https://www.youtube.com/channel/UC4FQomVeKlE-KEd3Wh2B3Xw?view_as=subscriber), así como en unas semanas un completo análisis donde desgranaremos todas las virtudes y defectos, si es que tiene, de este juego. Puedes encontrar más información de F1 2017 para Linux en el [minisitio de Feral](http://www.feralinteractive.com/es/games/f12017/). Os dejamos con el trailer de lanzamiento de F1 2017:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/BbBDcF7Zlwg" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


Si quieres comprar F1 2017 para GNU-Linux/SteamOS puedes hacerlo en la [**Tienda de Feral (recomendado)**](https://store.feralinteractive.com/es/mac-linux-games/f12017/) o en Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/515220/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>

---


**ACTUALIZACIÓN:** Hoy mismo, Jueves día 2 de Noviembre a las 22:30 hora Española (GMT+1) realizaremos un "desempaquetado" del juego a través de nuestro canal de **[Twitch](https://www.twitch.tv/jugandoenlinux)** de nuestra web o nuestro [canal de Twitch](https://www.twitch.tv/jugandoenlinux). Podreis hablar con nosotros a través del Chat en directo o mejor aun "a viva voz" a través de nuestro [canal de Discord](https://discord.gg/fgQubVY).

---


**ACTUALIZACIÓN 3-11-17:** Tras algunos problemas acabamos de colgar en nuestro canal de Youtube el video que grabamos ayer por la noche para estrenar F1 2017:

<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/ruuQCCEC094" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>

¿Qué os parece el último port de Feral? ¿Os gusta la Formula 1? Déjanos tus impresiones sobre este lanzamiento en los comentarios o en nuestro canal de [Telegram](https://t.me/jugandoenlinux).
