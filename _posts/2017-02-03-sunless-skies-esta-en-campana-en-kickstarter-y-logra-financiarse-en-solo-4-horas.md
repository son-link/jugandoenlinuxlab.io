---
author: Pato
category: Rol
date: 2017-02-03 08:41:40
excerpt: "<p>La secuela del exitoso 'Sunless Sea' tendr\xE1 soporte en Linux</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/ea91bb19891b6c623322a2cb25533741.webp
joomla_id: 205
joomla_url: sunless-skies-esta-en-campana-en-kickstarter-y-logra-financiarse-en-solo-4-horas
layout: post
tags:
- indie
- rol
- kickstarter
title: "'Sunless Skies' est\xE1 en campa\xF1a en Kickstarter y logra financiarse en\
  \ solo 4 horas"
---
La secuela del exitoso 'Sunless Sea' tendrá soporte en Linux

'Sunless Skies' [[web oficial](http://www.failbettergames.com/sunless-skies/)] será la secuela del exitoso 'Sunless Sea' [[Steam](http://store.steampowered.com/app/304650/)] , el aclamado juego de "Failbetter Games" [[web oficial](http://www.failbettergames.com/)] que ya tenemos disponible en Steam para Linux y que tiene unas críticas muy positivas. Incluso en Metacritic tiene un 81.


Así que no es de extrañar que los desarrolladores se fueran a dormir y al levantarse se encontrasen que en tan solo 4 horas habían logrado su objetivo de financiación e incluso habían superado varios de los "objetivos secundarios".



> 
> FULLY FUNDED IN FOUR HOURS! <https://t.co/yeKpc4lemt> [pic.twitter.com/ZyKrpYnc6N](https://t.co/ZyKrpYnc6N)
> 
> 
> — Failbetter Games (@failbettergames) [1 de febrero de 2017](https://twitter.com/failbettergames/status/826882911899107328)



'Sunless Skies' será un juego de rol al igual que su predecesor en vista cenital con una ambientación victoriana en un Londres en el que tendrás que enfrentarte con tu aeronave "a abominaciones en cielos lejanos, luchar por sobrevivir, hablar a las tormentas, asesinar a un sol y luchar por no perder el juicio".


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/rTB_lw06sWw" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


 Tal y como reza el lema de la campaña: "Explora un universo lleno de horror celestial y arrasado por la ambición victoriana en este RPG literario para PC, Mac y Linux" el juego al igual que su predecesor llegará a nuestro sistema favorito. Eso sí, tal y como anuncian en su apartado de [preguntas frecuentes](https://www.kickstarter.com/projects/failbetter/sunless-skies-the-sequel-to-sunless-sea/faqs) el juego no será traducido oficialmente al español, pero siempre queda la opción de que la comunidad lo traduzca extraoficialmente.


Si te gustó 'Sunless Sea' o te gustaría jugar a este 'Sunless Skies'  y quieres aportar a la campaña de financiación puedes hacerlo en:


<div class="resp-iframe"><iframe frameborder="0" height="420" scrolling="no" src="https://www.kickstarter.com/projects/failbetter/sunless-skies-the-sequel-to-sunless-sea/widget/card.html?v=2" width="220"></iframe></div>


¿Qué te parecen los juegos de "Failbetter Games"? ¿Piensas aportar en su campaña?


Cuéntamelo en los comentarios, en el foro o en los canales de Jugando en Linux en [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

