---
author: Serjor
category: Editorial
date: 2017-06-19 20:10:34
excerpt: <p>Por fin podremos ejecutar steam sin tener que preocuparnos por las dependencias
  del sistema</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/b689fad0280b286c898256c8d3b6ee9e.webp
joomla_id: 379
joomla_url: steam-ya-esta-disponible-como-flatpak
layout: post
tags:
- steam
- flatpak
title: "Steam ya est\xE1 disponible como flatpak (Actualizaci\xF3n - Video Explicativo)"
---
Por fin podremos ejecutar steam sin tener que preocuparnos por las dependencias del sistema

Gracias al aviso de un usuario del grupo de Telegram, a través de [OMG Ubuntu](http://www.omgubuntu.co.uk/2017/06/steam-now-available-flatpak) nos enteramos de que Steam está disponible vía flatpak.


Para los que no hayáis oído hablar de Flatpak o Snap, como resumen rápido y no exento de errores diremos que, son dos sistemas de distribución de aplicaciones donde al instalar cualquier programa, este y todas sus dependencias se instalan y ejecutan de manera aislada al resto de las aplicaciones del sistema, lo cuál permite por ejemplo limitar el acceso de estas aplicaciones, o también permite cambiar de versiones de una manera mucho más flexible ya que son sistemas transaccionales que permiten la coexistencia de varias versiones instaladas en el sistema o ejecutar rollbacks si ha habido algún problema durante la instalación de la aplicación sin afectar al resto del sistema.


Tanto [Flatpak](http://flatpak.org) como [Snap](https://snapcraft.io/) son dos tecnologías que vienen a resolver una problemática que vamos arrastrando en Linux desde hace tiempo, cada distro tiene que hacerse cargo de la configuración, compilación y mantenimiento de sus paquetes. En el caso que nos ocupa, Flatpak debería solucionar uno de los problemas que más quebraderos da, y es el de la instalación de dependencias sobretodo en instalaciones de 32bits. Esperemos que tarde o temprano el proyecto cuente con el respaldo de Valve y gestionen las actualizaciones a través de este sistema de distribución de aplicaciones.


Os dejamos los pasos para instalar Steam vía Flatpak:


Para quienes uséis ubuntu y derivadas:



```
`sudo add-apt-repository ppa:alexlarsson/flatpak`
```


```
`sudo apt update && sudo apt install flatpak xdg-desktop-portal`
```


```
`Y para todos los usuarios de flatpak:  
  
sudo flatpak remote-add --if-not-exists flathub <https://flathub.org/repo/flathub.flatpakrepo>`
```


```
`sudo flatpak install flathub com.valvesoftware.Steam`
```

 El usuario de nuestra web [khrysRo](https://khrysro.wordpress.com/) ha elaborado un video para Youtube donde nos enseña como instalar Flatpack y Steam en Opensuse tumbleweed. Aquí teneis el video:


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/wAVYQbsvRBs" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


Y tú, ¿eres más de Flatpak o Snap? Cuéntanoslo en los comentarios, en el foro, o en nuestros grupos de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD).

