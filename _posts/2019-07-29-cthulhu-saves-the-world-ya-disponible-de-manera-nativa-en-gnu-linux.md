---
author: Serjor
category: Estrategia
date: 2019-07-29 21:27:34
excerpt: "<p>A pesar de trabajar en mejorar Proton, Ethan Lee sigue tray\xE9ndonos\
  \ ports nativos</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/659b633c73fa75b9f907409caacd2cab.webp
joomla_id: 1082
joomla_url: cthulhu-saves-the-world-ya-disponible-de-manera-nativa-en-gnu-linux
layout: post
tags:
- ethan-lee
- cthulhu-saves-the-world
- fna
title: Cthulhu Saves The World ya disponible de manera nativa en GNU/Linux
---
A pesar de trabajar en mejorar Proton, Ethan Lee sigue trayéndonos ports nativos

Vía twitter nos enteramos de una curioso [anuncio](https://icculus.org/finger/flibitijibibo):



> 
> Cthulhu Saves Christmas was announced today! In the spirit of the holidays, I've published the Linux/macOS ports of Cthulhu Saves the World to my plan file: <https://t.co/Fj05oKVheM>
> 
> 
> — Ethan Lee (@flibitijibibo) [July 29, 2019](https://twitter.com/flibitijibibo/status/1155889563790970883?ref_src=twsrc%5Etfw)


Y es que Ethan "Flibitijibibo" Lee, el cuál es uno de los mayores responsables de las librerías FNA, el remplazo de código abierto de XNA, y de numerosos ports, ha vuelto precisamente a la senda de los ports, y en esta ocasión nos anuncia que "Cthulhu Saves The World" está disponible para GNU/Linux y macOS, no obstante, para ejecutarlo, a parte de tener el juego original, hay que seguir una serie de pasos:


* [Descargarse](http://www.flibitijibibo.com/files/CSTW_Linux.tar.bz2) la versión para GNU/Linux
* Copiar los ficheros CSTW.exe, 'CSTW.pdb', y 'Content' de la versión para Windows al directorio del ejecutable
* Obtener la suma MD5 del fichero Content/Movies/CSTW_intro2.wmv que será la password para el fichero [CSTW_Xiph.zip](http://www.flibitijibibo.com/files/CSTW_Xiph.zip) el cuál contiene los vídeos y la música recodificada y que deberemos descomprimir por encima de los ficheros la versión de Windows que ya existen
* Ejecutar la versión para GNU/Linux :-)


La verdad es que es una manera un tanto manual que podría solucionarse con un pequeño script.


Además, en el anuncio, nos hace saber que su objetivo es dedicarse en exclusiva a hacer juegos nativos para GNU/Linux, con lo que parece que su colaboración con Valve está tocando a su fin.


Si queréis haceros con el original para poder jugar a este port, podéis conseguirlo en steam:


<div class="steam-iframe"><iframe frameborder="0" height="190" src="https://store.steampowered.com/widget/107310/" width="646"></iframe></div>


Y tú, ¿ya sabes cómo se pronuncia Cthulhu? Cuéntanos qué te parece este port en los comentarios o en nuestros canales de [Telegram](https://t.me/jugandoenlinux) y [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org)

