---
author: yodefuensa
category: "An\xE1lisis"
date: 2018-10-03 16:55:16
excerpt: <p>Nuestro colaborador nos analiza el excelente port de Feral</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/98bce7d9d49222dae020697d2909b8d4.webp
joomla_id: 865
joomla_url: analisis-life-is-strange-before-the-storm
layout: post
tags:
- feral-interactive
- analisis
- life-is-strange
- before-the-storm
title: "An\xE1lisis Life is Strange: Before The Storm"
---
Nuestro colaborador nos analiza el excelente port de Feral

Con el final de life is strange nos quedamos con ganas de más. Ahora en esta precuela podemos ahondar en la historia de Chloe y Rachel.


La responsabilidad de este spin-off recae en el estudio **Deck Nine Games** que ha sabido llevar a buen puerto esta historia, llena de altibajos, y sin estar a la altura de la obra original de DONTNOD pero que en líneas generales cumple su cometido.


> "Life is Strange: Before the Storm es una aventura independiente dividida en tres partes que transcurre antes del primer juego de la serie galardonada por los premios BAFTA.


> Juegas siendo Chloe Price, una chica rebelde de 16 años que forja una amistad inverosímil con Rachel Amber, una estudiante popular destinada a un futuro lleno de éxitos.


> Rachel descubre un secreto familiar que amenaza con destruir su mundo y encuentra la fortaleza que necesita en su nueva amistad con Chloe. Juntas, Chloe y Rachel tendrán que enfrentarse a sus demonios interiores y encontrar la manera de sobreponerse a ellos."


Life is Strange: Before The Storm es un juego que al igual que en la entrega anterior, se centra en la narrativa, el intercambio de puzzles simples y la toma de decisiones para ir dirigiendo la historia. Si bien en el juego original el principal atractivo era poder retroceder en el tiempo, aquí deberemos aceptar las consecuencias de nuestros actos. La gran diferencia reside en que si en un principio con Max podíamos decidir como éramos, con Chloe seguimos siendo Chloe y nos da la impresión que las decisiones pasan entre ser descaradas o muy descaradas. No tenemos que olvidar que la grandeza de esta saga pasa por sus personajes, su carisma, y las historias que hay alrededor de ellos.


![1](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisLifeisStrangeBTS/1.webp)


Para bien o para mal, al ser un formato episódico Deck Nine cambia el ritmo de la historia. Si bien durante el primer episodio puede parecernos que se va a centrar en contarnos que es lo que sucede antes de la tormenta del primer juego, sin tener ningún objetivo fijo, siendo solo testigos de las vidas de nuestras protagonistas. Esto cambia de manera notable en el último episodio, para contarnos su propia historia con un nudo y un objetivo marcado. Este es el principal fallo del juego, lo irregular que es su historia, lo despacio que avanza o lo poco que deja entrever lo que esta pasando, para después pegar un sprint final. Bien porque Deck Nine no ha sabido crear una historia más compensada, o bien porque no conseguía dar con la tecla en los dos primeros episodios. Por una cosa u otra igualmente tenemos un juego descompensado.


Pese al ritmo irregular de la historia, trata temas bastante serios, la perdida de seres queridos y de la confianza que podamos tener en ellos. De forma menos explicita, la rebeldía juvenil o lo difícil que es volver a aceptar que la madre de Chloe pueda rehacer su vida. Temas que no son nada habituales ver todos juntos en un videojuego.


Respecto a las mecánicas del juego, si, perdemos el retroceder en el tiempo. Pero ganamos algo de frescura con la mecánica de insultos. Un duelo donde se inicia una discusión acalorada en la que deberemos escoger meticulosamente cada respuesta teniendo en cuenta las palabras clave del otro interlocutor. El gran fallo de esta mecánica es que nos indica el camino a seguir, si bien no podemos tener clara cual es la respuesta correcta, al ver la opción de iniciar un duelo de insultos, sabemos que esa es la opción a escoger y por desgracia son bastante simples de adivinar.


![2](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisLifeisStrangeBTS/2.webp)


Los puzzles del juego son bastante escasos y simples. Varios escalones por debajo de lo visto en el primer juego. No añaden un desafío del cual sentirnos orgullosos al completarlos, están ahí y suponen una perdida de tiempo. Por poner un ejemplo en un momento determinado del juego arreglar un furgoneta consiste en buscar unos objetos para decorarla y decidir con que herramienta ajustar los mecanismos.


Mención especial para lo que ha sido uno de los mejores momentos del juego, la partida de rol. No aporta nada y es totalmente opcional pero consigue sorprendernos y sacarnos una sonrisa.


![3](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisLifeisStrangeBTS/3.webp)


Visualmente se han reutilizado muchos escenarios, lo cual es obvio, estamos ante una precuela. Pero con acierto añadir más paisajes sin desentonar con lo ya establecido. Se nota que no estamos ante la última versión del motor gráfico Unity, pero el uso de cell shading y los abundantes detalles tapa estas carencias y sacan a relucir cierto mimo y cuidado, lo cual es algo de agradecer. El doblaje en inglés y la captura de movimientos son de una buena factura, al igual que la traducción de los subtítulos en español y todos los textos.


El trío británico Daughter ha sido el encargado de componer la banda sonora. Se busca desde el primer momento, que todas la emociones, las letras y la estética del juego converjan y funcionen de forma conjunta como separada.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/b-pTWUhYo1c" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


*Daughter no serán los únicos que prestarán su talento en la precuela: No*  
*Below, de Speedy Ortiz, también formará parte del juego, concretamente*   
*de los créditos iniciales.*


![4](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisLifeisStrangeBTS/4.webp)


Mención aparte para el último episodio del juego, donde volvemos a controlar a Max en el que posiblemente sea el último día que pasemos con nuestra amiga antes de mudarnos a Seatle. Un capitulo que no aporta nada que no supiéramos pero que es obligatorio jugar. Son 60 minutos cargados emociones recordando la infancia que pasaron juntas.


Comentando el rendimiento del juego hay que dejarlo claro, donde este un juego nativo que se quite proton. **Feral hace un magnífico port,** si bien es cierto que los requisitos mínimos son un poco abultados para lo que es gráficamente el juego.


- MÍNIMO:Requiere un procesador y un sistema operativo de 64 bits  
 - SO: Ubuntu 18.04  
 - Procesador: Intel Core i3-4130T @ 2.90GHz  
 - Memoria: 4 GB de RAM  
 - Gráficos: 2GB Nvidia GTX 680, 2GB AMD R9 270  
 - Almacenamiento: 28 GB de espacio disponible  
 - Notas adicionales: AMD GPU requieren el controlador MESA 18.1.6, Nvidia GPU requieren el controlador Nvidia 396.54 o superior. \* Intel GPU no son compatibles.


El equipo usado para esta review cuenta con Antergos kernel 4.17, CPU x4-860K y rx 570. El rendimiento ha sido notable manteniendo 60fps constates. Buscando forzar un poco más hemos podido ejecutar el juego con un intel i5-5200u y r7 m260, por supuesto con todo los gráficos a mínimo, una resolución algo por debajo de HD, pero con una tasa de fotogramas entre 20-35fps.


En resumen Life is Strange: Before the storm, es un buen juego que a veces falla en lo más básico, pero que si nos detenemos a tener en cuenta todos los detalles que están ahí, son los que muchas veces convierten un buen juego en una obra de arte. En general deja una buena sensación y es una compra más que recomendada para aquellos que disfrutaron del primer juego.


Tienes Life is Strange: Before the Storm en la [tienda de Feral](https://store.feralinteractive.com/es/mac-linux-games/lisbeforethestormdeluxe/) (recomendable), [Humble Bundle](https://www.humblebundle.com/store/life-is-strange-before-the-storm?jugandoenlinux) (enlace de afiliado) o en [Steam](https://store.steampowered.com/app/554620/Life_is_Strange_Before_the_Storm/).

