---
author: Pato
category: Arcade
date: 2017-11-24 09:38:46
excerpt: <p>Para celebrarlo, 2Awesome Studios lanza un nuevo trailer</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/e0b863f873638efa81a0f8773b39882d.webp
joomla_id: 552
joomla_url: dimension-drive-saldra-del-acceso-anticipado-y-llegara-oficialmente-el-7-de-diciembre
layout: post
tags:
- indie
- arcade
- shoot-em-up
title: "'Dimension Drive' saldr\xE1 del Acceso Anticipado y llegar\xE1 oficialmente\
  \ el 7 de Diciembre"
---
Para celebrarlo, 2Awesome Studios lanza un nuevo trailer

A principios de año ya os [anunciamos](index.php/homepage/generos/arcade/item/343-dimension-drive-ya-disponible-en-linux-hoy-dia-de-su-lanzamiento) la llegada de 'Dimension Drive' en Acceso Anticipado, y durante todo este tiempo los chicos de 2Awesome Studios han estado puliendo el juego y añadiendo contenidos de cara a su lanzamiento oficial. Hoy nos han anunciado que el juego saldrá de Acceso Anticipado y llegará a nuestros sistemas de forma oficial el próximo día 7 de Diciembre.



> 
> The feels after announcing the launch of [#DimensionDrive](https://twitter.com/hashtag/DimensionDrive?src=hash&ref_src=twsrc%5Etfw) on [#NintendoSwitch](https://twitter.com/hashtag/NintendoSwitch?src=hash&ref_src=twsrc%5Etfw) and [#Steam](https://twitter.com/hashtag/Steam?src=hash&ref_src=twsrc%5Etfw) on December 7 with the new amazing trailer! Go [#Nindies](https://twitter.com/hashtag/Nindies?src=hash&ref_src=twsrc%5Etfw) [#indiedev](https://twitter.com/hashtag/indiedev?src=hash&ref_src=twsrc%5Etfw) [#shmup](https://twitter.com/hashtag/shmup?src=hash&ref_src=twsrc%5Etfw) TRAILER HERE : <https://t.co/lvzldJfM5I> [pic.twitter.com/4lShKf61jK](https://t.co/4lShKf61jK)
> 
> 
> — 2Awesome Studio (@2AwesomeStudio) [November 24, 2017](https://twitter.com/2AwesomeStudio/status/933983588365819909?ref_src=twsrc%5Etfw)



 Para celebrarlo tal y como dicen en su tweet han publicado el siguiente trailer:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/B114iOGn2Qw" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Dimension Drive [[web oficial](http://www.dimensiondrive.com/)] es un galardonado shoot’em up vertical en un campo de batalla dual enfocado a un uso rápido e inteligente del teletransporte. Lánzate a una aventura espacial en esta ingeniosa mezcla de mecánicas: *Una combinación de niveles enfocados en disparos estratégicos y puzles en tiempo real que te harán cuestionarte lo que sabes sobre los shoot’em ups.*


Multiplica tu concentración y lucha en 2 shoot’em ups en paralelo en el modo para un jugador o disfruta junto a un amigo en el modo cooperativo local para 2 jugadores. Cooperativo total donde ambos jugadores comparten todo, incluso las vidas. Juega como un equipo unido o muere.


Si quieres saber más o comprar 'Dimension Drive', lo tienes ya disponible en español (aún en acceso anticipado) en su página de Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/394430/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 ¿Qué te parece la propuesta de 'Dimension Drive? ¿Te gustan los "shoot'em up's?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 

