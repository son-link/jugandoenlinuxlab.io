---
author: leillo1975
category: Software
date: 2019-04-11 08:29:10
excerpt: "<p>La utilidad de configuraci\xF3n de Volantes llega a la versi\xF3n 0.5.0\
  \ .</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/95afb94bcb9e5971a68576edb0850e71.webp
joomla_id: 1017
joomla_url: la-nueva-version-de-oversteer-nos-trae-interesantes-anadidos-a3
layout: post
tags:
- software
- open-source
- logitech
- volantes
- thrustmaster
- t300-rs
title: "La nueva versi\xF3n de Oversteer nos trae interesantes a\xF1adidos (ACTUALIZADO\
  \ 3)"
---
La utilidad de configuración de Volantes llega a la versión 0.5.0 .


**ACTUALIZADO 25-8-20:** Han pasado algunos meses desde la última vez que @Bernat actualizó esta genial **utilidad que nos ayuda a sacarle más partido a nuestros amados volantes**. En esta ocasión los cambios de la [versión 0.5.0](https://github.com/berarma/oversteer/releases/tag/0.5.0) con respecto al [anterior lanzamiento]({{ "/posts/new-lg4ff-y-oversteer-se-actualizan-a-lo-bestia" | absolute_url }}) serán los siguientes:


*- Soporte experimental para TM300RS con <https://github.com/Kimplul/hid-tmff2.>*  
*- Corregidos los ejes de pedales en el Logitech G920.*  
*- Eliminada una zona muerta en los pedales y el volante con algunos temas de GTK.*  
*- Ventana de superposición rediseñada y más fácilmente relocalizable.*  
*- La posición de la ventana de superposición se mantiene después de ocultarla y se guarda en el perfil.*  
*- Mejor gestión de la conexión/desconexión de dispositivos con la aplicación abierta.*  
*- Arreglados avisos en el proceso de compilación/instalación actualizando los requerimientos a Meson 0.50*  
*- Añadida identificación de la app para los entornos de escritorio.*  
*- Arreglada la activación del ffbmeter al cargar un perfil.*  
*- Corregidos otros pequeños errores.*


Como podeis observar, **la aplicación deja de ser exclusiva de periféricos de Logitech, y comienza a aceptar más modelos, como el Thrustmaster T300RS**, que como sabeis recientemente ha empezado a tener soporte junto con el T150 como pudisteis ver en la [noticia que publicamos hace algunos días]({{ "/posts/t150-driver-y-hid-tmff2-dos-nuevos-drivers-para-volantes-thrustmaster" | absolute_url }}). Da gusto ver como este proyecto va creciendo poco a poco y abarcando más modelos y funcionalidades.




---


**ACTUALIZADO 27-9-19:** @Bernat ha vuelto a actualizar su fantástica aplicación, llegando en esta ocasión a la [versión 0.3.0](https://github.com/berarma/oversteer/releases/tag/0.3.0), que trae dos importantísimos añadidos que nos vendrán de perlas para disfrutar aun más de nuestros juegos de conducción:


-Se ha añadido la **fuerza de Autocentrado**, lo cual hace que el volante se pueda configurar de forma que tienda a volver a la posición central, permitiendo además poder escoger la dureza con que este efecto se realiza.


-Se ha añadido la **ganancia del Force Feedback**, que permite regular la fuerza global en los juegos que no lo permitan.


Como veis, con estos dos añadidos tenemos una aplicación mucho más útil, especialmente cuando queramos disfrutar de un juego del que no podamos disfrutar de efectos de Force Feedback. Por poner ejemplos, en todos esos juegos de Steam Play que no funciona correctamente el FFB, podemos activar el autocentrado y esto hace la conducción mucho más real y más precisa, ya que al ofrecer resistencia es más improbable que nos pasemos de giro en una curva o esquivando. Yo personalmente estoy disfrutando mucho más en juegos con Proton/Wine como rFactor 2, GT Legends o Kartkraft.  Seguiremos atentos a la evolución de este tan útil software.


 




---


**ACTUALIZADO 1-7-19:** Esta aplicación ha llegado a la **versión 0.2.2** en la que se han realizado los siguientes [cambios](https://github.com/berarma/oversteer/releases/tag/0.2.2) :


*-Los perfiles pueden ser borrados.*  
 *-Pequeños cambios en el diseño y estilos de las ventanas.*  
 *-Arreglos menores.*


También es importante decir que aunque haya sido un **arreglo realizado en la versión 0.2.1**, el programa  ha arreglado una regresión importante que rompía la línea de comandos y la aplicación de cambios desde la GUI cuando los derechos de administrador tenían que ser solicitados. Como veis poquito a poco esta utilidad se va puliendo para ofrecer un mejor uso con nuestros volantes Logitech.


Os recordamos que si quereis darle un buen uso a Oversteer, podeis participar en nuestra [Liga JugandoEnLinux](https://www.dirtgame.com/es/leagues/league/88825/jugandoenlinux) de [DIRT Rally](index.php/homepage/analisis/20-analisis/362-analisis-dirt-rally), donde podeis competir con otros miembros de nuestra comunidad en las mejores etapas de este magnífico juego. Si quereis más información, pasaos por nuestra [Comunidad de Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org) y entrad en la **sala DIRT Rally.**


 




---


**NOTICIA ORIGINAL:**  
Hace algunas semanas [os hablamos de esta fantástica utilidad](index.php/homepage/generos/software/12-software/1098-configura-tus-volantes-logitech-con-oversteer) creada por uno de los miembros de nuestra comunidad, **@Bernat**. Gracias a ella podíamos configurar, además de los **grados de nuestros volantes Logitech**, **modos de emulación** y **pedales combinados**, con el añadido de poder **guardar perfiles** para usar con cada uno de nuestros juegos favoritos.


Si recordais, en un principio teníamos que anteceder a la ejecución de la aplicación con "sudo" para que esta funcionase correctamente. Ahora mismo por ejemplo, **la propia aplicación nos pedirá la contraseña cuando la necesite** para hacer cambios, con lo que se gana en comodidad. Pero la cosa no se queda única y exclusivamente ahí, si no que se han añadido otras [interesantes caracterisiticas](https://github.com/berarma/oversteer/releases/tag/0.2.0):


-Para empezar **se ha mejorado la instalación del programa**, permitiendo integrarlo comodamente en el sistema y en el escritorio, pudiendo encontrarlo ahora en nuestro lanzador de aplicaciones con su icono correspondiente.


-Ahora **el programa está traducido también al Español y al Gallego**, por lo que si usamos estos idiomas en nuestro sistema nos saldrán los textos traducidos automáticamente. También podremos cambiarlo libremente en preferencias. Con el tiempo y la colaboración de los usuarios se espera poder traducir a más idiomas.


-El programa **permite modificar permanente y automáticamente los permisos** del volante para evitar tener que poner la contraseña de administrador cada vez que realizamos un cambio, con lo que ganamos en comodidad.


-Se han añadido **nuevos textos de ayuda como "tooltips"**, con explicaciones que aclaran el funcionamiento de las diferentes opciones cuando ponemos el ratón sobre ellas.


El desarrollador también nos confirma que seguirá trabajando en la aplicación para añadir nuevas caracterísiticas y mejorar las actuales funciones. Si quereis descargar e instalar esta magnífica utilidad  encontrareis toda la información necesaria en su [página del proyecto en GitHub](https://github.com/berarma/oversteer).


¿Habeis probado ya Oversteer?¿Qué os parecen este tipo de iniciativas por parte de nuestra comunidad? Dejanos tus impresiones en los comentarios o charla sobre ello en nuestro canal de [Telegram](https://t.me/jugandoenlinux).

