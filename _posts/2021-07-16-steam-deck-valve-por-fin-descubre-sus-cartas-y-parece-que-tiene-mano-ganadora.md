---
author: Pato
category: Hardware
date: 2021-07-16 16:35:44
excerpt: "<p>Repasamos la informaci\xF3n del pr\xF3ximo PC-Portatil de Valve, im\xE1\
  genes... y condiciones de reserva</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Steamdeck/valve_steamdeck_photo_skus.webp
joomla_id: 1327
joomla_url: steam-deck-valve-por-fin-descubre-sus-cartas-y-parece-que-tiene-mano-ganadora
layout: post
tags:
- steamos
- steam
- steam-deck
title: 'Steam Deck: Valve por fin descubre sus cartas, y parece que tiene mano ganadora'
---
Repasamos la información del próximo PC-Portatil de Valve, imágenes... y condiciones de reserva


 


Es la noticia del momento, del año para nosotros, los jugadores en linux desde luego. Están corriendo ríos de tinta pues lo que ha anunciado Valve no es cualquier cosa.


**Steam Deck**, a diferencia de lo que sucedió en su momento con las Steam Machines está levantando mucha expectación pues, esta vez si parece que han dado con un producto que convence.


Es cierto que, a diferencia de las Steam Machines, **la Steam Deck viene avalada por la propia Valve**, cosa que ya le da un plus de confianza, y además con un sistema que ya está lo suficientemente maduro como para echarlo a andar para el gran público. En este aspecto estamos viendo el resultado del trabajo y la inversión realizados durante los últimos años, desde los distintos drivers, capas de compatibilidad, APIs gráficas o el propio sistema. Y todo ello redundando en beneficio de la comunidad, no hay que olvidarlo. 


Comenzando por el sistema, Valve vuelve a confiar en su propio desarrollo con **SteamOS 3.0**, lo que parece ser una capa por encima de un sistema base a cargo de KDE Plasma, sistema al que podremos acceder desde el propio menú de la consola y bajo el que podremos instalar software de terceros, como suites de productividad, navegadores y otro software (¿Alguien dijo emuladores?). **Al fin y al cabo, es un PC**, y si algo han querido dejar claro desde un principio los chicos de Valve es que **podemos hacer con el lo que queramos**, literalmente. (Hasta Tim Sweeney ha aplaudido en un tweet el que sea un sistema abierto... ¿pensará ahora en lanzar su tienda para Linux?;)


En la parte de los juegos, **confían en el gran número de juegos nativos** para Linux que ya existen en Steam, y para el resto están afinando **Proton** de cara a aumentar la compatibilidad de los juegos no nativos y que la experiencia sea lo más pulida posible. En las diversas entrevistas que han concedido tanto **Gabe Newell como Pierre Loup**, se ha dejado caer que tienen una versión de Proton en desarrollo interno que está mucho más avanzado que el que tenemos disponible en los repositorios públicos y que será el que traiga este nuevo sistema. Respecto a los juegos que utilizan software anti-trampas, en el apartado de desarrolladores, Valve ha anunciado que se encuentra trabajando con los chicos de **Easy Anti Cheat y Battleye** **para certificar estos sistemas bajo Proton**, y tenerlos listos para funcionar antes de la salida del dispositivo a final de año.


En cualquier caso, no es un secreto que en Valve confían en que el éxito de la máquina termine convenciendo a la mayoría de los desarrolladores para lanzar sus juegos nativos, o al menos ofrecer soporte oficial bajo Proton en última instancia.


Por otra parte, y como premisa el dispositivo estará abierto a poder modificar el sistema, será capaz de g**estionar los mods de Workshop**, y por supuesto tendrás la libertad de instalar otros sistemas operativos, aunque si han hecho las cosas bien la experiencia que obtendremos con SteamOS 3.0 será difícilmente igualable con sistemas no adaptados a pantallas pequeñas o táctiles, por no decir si han conseguido afinar bien el sistema para que sea lo más liviano posible de cara a extraer hasta la última gota de rendimiento.


En cuanto al hardware, hemos de decir que hemos estado echando cuentas, y **es difícil encontrar una combinación de hardware portátil mejor ajustada en relación precio/prestaciones**. Si hemos de tomar una referencia, nada mejor que los chicos de [IGN](https://es.ign.com/steam-deck/174774/preview/steam-deck-nuestras-primeras-impresiones-con-la-nueva-portatil-de-valve), que han tenido oportunidad de probar la Steam Deck en primicia, y todo lo que cuentan es que han quedado gratamente sorprendidos con el sistema, pues es capaz de mover títulos triple A actuales en su pantalla de 7 pulgadas con suficiente solvencia y fluidez, añadiendo que conectaron el aparato a una pantalla, un teclado y un ratón mediante el deck, y afirman que al rato se olvidaron que no estaban jugando en un PC de escritorio.


Volviendo al aparato en si, afirman que al principio parece que los botones y la disposición de las palancas y mandos hápticos parece algo extraña, pero una vez en las manos afirman que se siente cómodo, y con todos los mandos accesibles. En las sesiones de juego afirman que no han tenido problemas a la hora de sujetar el aparato, cosa que en un dispositivo de algo más de 600 gramos ya es un avance.


Por último, reseñar que el Steam Deck se venderá en tres versiones distintas, con la única diferencia de la capacidad y tecnología de almacenamiento, y la tope de gama traerá una capa anti-reflectante en la pantalla. El resto de características serán idénticas en las tres versiones, quedando como sigue:


* Steam Deck 64GB - eMMC con estuche de transporte por **419€**
* Steam Deck 256GB - NVMe SSD con estuche de transporte y lote de perfil exclusivo de la Comunidad por **549€**
* Steam Deck 512GB - NVMe SSD con estuche de transporte exclusivo, lote de perfil exclusivo de la comunidad, pantalla antirreflectante y teclado virtual con tema exclusivo por **679€**


En cuanto a la reserva del Steam Deck, se prodrá reservar en tan solo unos minutos desde la publicación de este artículo. Para evitar la reventa y la expeculación, Valve solo dejará reservar una unidad por usuario de Steam durante las primeras 48 horas y solo las cuentas activas que hayan comprado al menos un juego antes del mes de Junio podrán reservar durante ese plazo de tiempo. Como es lógico, no es un sistema infalible pero al menos si puede facilitar que una gran mayoría del producto llegue a las manos de los jugadores y no a las webs de reventa.


En cuanto a la reserva en si, a diferencia de otros dispositivos no tienes que pagar todo el importe ahora, si no que se te cobra una cantidad simbólica (unos 4€) a descontar de la cantidad final de venta del Steam Deck cuando tu unidad esté disponible.


Puedes reservar tu versión de Steam Deck en este enlace:


<https://store.steampowered.com/steamdeck>


A continuación, unas imágenes para que puedas admirar el dispositivo en todo su esplendor:


![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Steamdeck/SteamDeck1.webp)



![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Steamdeck/overview-shared-library-spanish.webp)


![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Steamdeck/image_2021-07-16_17-48-51.webp)


![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Steamdeck/image_2021-07-16_17-48-22.webp)


![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Steamdeck/image_2021-07-16_17-53-14.webp)


![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Steamdeck/image_2021-07-16_17-53-41.webp)

