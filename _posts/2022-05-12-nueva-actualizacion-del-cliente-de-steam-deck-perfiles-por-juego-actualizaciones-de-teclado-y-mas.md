---
author: Pato
category: Hardware
date: 2022-05-12 09:46:16
excerpt: "<p>La exitosa ultraportatil de Valve sigue recibiendo mejoras</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Steamdeck/overview-shared-library-spanish.webp
joomla_id: 1466
joomla_url: nueva-actualizacion-del-cliente-de-steam-deck-perfiles-por-juego-actualizaciones-de-teclado-y-mas
layout: post
tags:
- steamos
- steam-deck
title: "Nueva actualizaci\xF3n del cliente de Steam Deck: Perfiles por juego, actualizaciones\
  \ de teclado y m\xE1s"
---
La exitosa ultraportatil de Valve sigue recibiendo mejoras


Casi no hay semana que no hayan una o dos actualizaciones para la Steam Deck. La última actualización liberada hace apenas unas horas nos trae algunas características interesantes y muy demandadas por la comunidad de jugadores en la máquina de Valve, como los perfiles por juego o (por fin!) las encuestas de hardware de Steam para Steam Deck por lo que es de esperar que por fin aparezca el impacto que está suponiendo el lanzamiento de la máquina en las encuestas mensuales:


**General**


* Cuando se conecta a una pantalla externa, la interfaz Steam Deck ahora se escala a una resolución virtual de 1280x800 (más trabajo en esta función y la funcionalidad está en marcha)
* Añadida la funcionalidad de encuesta de hardware Steam para Steam Deck
* Corregido el problema con la aparición de múltiples notificaciones de elementos de Steam Inventory
* Solucionado el problema con los usuarios que se presentaban como favoritos cuando ya no son amigos
* Corregidos los problemas con varios cuadros de diálogo que se exponen incorrectamente cuando el teclado en pantalla es visible
* Corregidas intersticiales de teclado y lupa en pantalla que no cogen la entrada en el primer lanzamiento de algunos juegos


**Perfiles de rendimiento por juego**


* Los jugadores ahora pueden establecer configuraciones de rendimiento específicas del juego. Se accede a esta función en el menú de acceso rápido> Rendimiento> Vista avanzada
* Por defecto, los juegos utilizarán la configuración de rendimiento del sistema
* Si la configuración por juego se activa, se crea un perfil para el título actualmente en ejecución. Cualquier edición se guardará y se aplicará automáticamente cuando se inicie el juego
* Siempre puede activar esta opción para volver a la configuración predeterminada del sistema o restablecer a la configuración predeterminada del sistema en cualquier momento.


**Online / Offline**


* Rendimiento mejorado al cambiar de modo fuera de línea a modo en línea
* Mejora de la disponibilidad de secciones de la pantalla de inicio al cambiar de modo fuera de línea a modo en línea
* Los amigos fijados continúan mostrándose como en línea cuando Steam está desconectado o cambia al modo fuera de línea


**Teclados**


* Se agregaron los diseños de teclado fonético tradicional y fonético búlgaro
* Corregió la interacción entre CapsLock y Shift
* El teclado muestra las sugerencias del símbolo AltGr cuando AltGr no está activo
* El teclado solo muestra símbolos AltGr cuando AltGr está activo
* Los caracteres no ASCII funcionan en modo de escritorio
* Se corrigió la tecla 'Г' duplicada en el teclado ruso
* Se eliminó la tecla ' "" del teclado ucraniano


**Controladores**


* Eliminados los interruptores de vibración y háptico del menú Acceso rápido. Se puede acceder a ellos en: Configuración> Configuración del controlador.
* Se movió el botón Reordenar controladores a la otra sección en el menú Acceso rápido


**Juego remoto**


* Añadida una opción "Dejar de transmitir" al menú de energía al hostear una sesión de Remote Play
* Lista actualizada de juegos jugados recientemente para incluir juegos transmitidos


Puedes ver los cambios en el [post oficial en Steam](https://store.steampowered.com/news/app/1675200/view/3216144458727332893).

