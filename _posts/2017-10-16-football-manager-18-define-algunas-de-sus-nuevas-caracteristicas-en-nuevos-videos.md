---
author: Pato
category: Estrategia
date: 2017-10-16 17:44:09
excerpt: "<p>El juego de estrategia deportiva sigue desgranando novedades de cara\
  \ a su estreno el pr\xF3ximo 10 de Noviembre</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/24d767ce6c156eb07b1fc26c266211f5.webp
joomla_id: 486
joomla_url: football-manager-18-define-algunas-de-sus-nuevas-caracteristicas-en-nuevos-videos
layout: post
tags:
- proximamente
- simulador
- estrategia
title: "'Football Manager 18' define algunas de sus nuevas caracter\xEDsticas en nuevos\
  \ v\xEDdeos"
---
El juego de estrategia deportiva sigue desgranando novedades de cara a su estreno el próximo 10 de Noviembre

Si hace una semana os traíamos novedades sobre 'Football Manager 18', hoy os traemos nuevos vídeos donde Sega nos detalla algunas de las características que ya nos desvelaron entonces. Sin más preámbulos vamos con los vídeos:


**Dinámicas:**


**Dinámicas** es una nueva característica que relaciona tu gestión en el vestuario con el rendimiento de tu equipo sobre el terreno de juego. 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/zmVvc35M4N8" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


**Sistema Ojeador:**


**El Sistema Ojeador** ha sido rehecho para hacerlo el más auténtico de toda la historia de la serie al haber trabajado junto a clubs reales para reflejar el proceso de fichar a un jugador. 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/SDfaNEjK9hc" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


**Remodelado de Tácticas:**


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/BeXV0MieLmY" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


**Ciencia del Deporte rehecha:**


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/2QOchNSxakw" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 'Football Manager 18' llegará a Linux/SteamOS el próximo día 10 de Noviembre. Si quieres saber más, puedes visitar su [web oficial](http://www.footballmanager.com/) o su página de Steam donde ya puedes pre-comprarlo:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/624090/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


¿Que te parecen las novedades? ¿Te sentarás en el banquillo o en el despacho?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 

