---
author: Pato
category: "Acci\xF3n"
date: 2017-02-28 08:01:56
excerpt: <p>El juego de Conauts Creative soporta Linux desde el primer momento.&nbsp;</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/8462850dbb62c4bd159ee1ad55df6950.webp
joomla_id: 233
joomla_url: river-city-ransom-underground-ya-disponible-en-linux-steamos
layout: post
tags:
- accion
- indie
- beat-em-up
- retro
title: '''River City Ransom: Underground'' ya disponible en Linux/SteamOS'
---
El juego de Conauts Creative soporta Linux desde el primer momento. 

Ya os hablamos de River City Ransom: Underground en un [Apt-Get Update](index.php/homepage/apt-get-update/item/330-apt-get-update-mesa-overdriven-reloaded-motorsport-manager-heavy-gear-assault-river-city-ransom) del que nos hicímos eco gracias a Fernando Briano y su web [picandocodigo.net](http://picandocodigo.net/). Se trata de un juego de acción de la vieja escuela que recuerda mucho a juegos como "Double Dragon" o "Streets of Rage" (de hecho huele a Double Dragon por todas partes) con una jugabilidad endiablada y un estilo retro que viene de la época de la NES. De hecho Conauts Creative compró la licencia de River City a Arc System Works y luego finalizó una exitosa campaña en Kickstarter para traernos este juego.


En River City Ramsom: Underground tendrás que tomar el papel de Alex y Ryan y lanzarte a las calles de River City para luchar contra hordas de enemigos para limpiar sus nombres de los delitos de los que les acusan. Ganarás dinero, obtendrás nuevos golpes y aumentarás tus poderes para desencadenar el caos a tu alrededor.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/us7MnovsPKs" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


Entre las características principales tendrás:


· Cooperativo local y online con hasta 4 jugadores.


· 10 Héroes jugables en el modo historia, cada uno con su propio estilo de juego.


· 140 niveles que explorar con secretos incluidos.


· Modo de combate multijugador en línea con selección de equipos, modificadores y jugadores CPU.


'River City Ransom: Underground' no está disponible en español, pero no creo que esto sea un gran problema dado el estilo de juego que es. Si no es problema para ti, lo tienes disponible en su página de Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/422810/" width="646"></iframe></div>


¿Qué te parece este 'River City Ramsom: Underground? ¿Te gustan los juegos de estilo Retro? 


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

