---
author: leillo1975
category: "Simulaci\xF3n"
date: 2022-11-15 18:10:21
excerpt: "<p>Por fin podremos ver el trabajo de @SCSsoftware, sus desarrolladores</p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ATS/Texas/TexasDLC.webp
joomla_id: 1509
joomla_url: texas-la-nueva-y-flamante-dlc-de-american-truck-simulator-ha-sido-lanzada
layout: post
title: Texas, la nueva y flamante DLC de American Truck Simulator, ha sido lanzada.
---
Por fin podremos ver el trabajo de @SCSsoftware, sus desarrolladores


 Ya ha llovido mucho (más de un año y medio) desde que [anunciamos]({{ "/posts/american-truck-simulator-llegara-a-texas-en-su-proxima-dlc" | absolute_url }}) que el estado de la estrella solitaria sería el próximo en ver la luz como DLC de mapa en American Truck Simulator, y por el camino han pasado muchas, muchas cosas, tal vez demasiadas... Después del lanzamiento del esperadísimo [Iberia]({{ "/posts/iberia-dlc-disponible-para-euro-truck-simulator-2" | absolute_url }}) que hizo y hace las delicias de muchos de nosotros, la [suspensión de "Heart of Russia"]({{ "/posts/el-lanzamiento-de-heart-of-russia-dlc-para-euro-truck-simulator-dos-ha-sido-suspendido" | absolute_url }}), la salida de "[Montana]({{ "/posts/montana-sera-el-nuevo-estado-que-llegara-a-american-truck-simulator" | absolute_url }})", o el anuncio de "[West Balkans]({{ "/posts/2022-06-06-14-29-27" | absolute_url }})", finalmente el segundo estado más grande de EEUU ya está disponible para que todos podamos recorrerlo y admirarlo. Así era anunciado en la cuenta de twitter de SCS Software:



> 
> Texas DLC for American Truck Simulator is now available! 🇺🇸🚛  
>   
> We hope you will love exploring everything that this massive map expansion has to offer 🤩🙌  
>   
> Check it out at 👇<https://t.co/rJ6kjwuNbj> [pic.twitter.com/CqVissqzfa](https://t.co/CqVissqzfa)
> 
> 
> — SCS Software (@SCSsoftware) [November 15, 2022](https://twitter.com/SCSsoftware/status/1592578890127446016?ref_src=twsrc%5Etfw)



 Han sido duros años de preparación, investigación, diseño de mapas, modelado y trabajo, hasta llegar a este momento, y finalmente la espera ha acabado. Volveremos una vez más a las tierras del sur, a este estado con múltiples biomas, donde encontraremos **grandes ciudades, costa, montañas, desierto, praderas**.... Como veis no solo encontraremos vaqueros, ranchos y ganado. Esta es la descripción oficial de esta DLC :


* 29 ciudades explorables como Dallas, Fort Worth, Houston, Austin, San Antonio, El Paso y Galveston.
* Conducción a través de 50 ciudades pintorescas (asentamientos)
* 20.000 millas de la red de carreteras del estado
* Entregas en más de 200 depósitos industriales realistas, incluyendo industrias exclusivas del estado de Texas (astillero de alta mar, planta desmotadora de algodón, planta de vidrio e industrias espaciales)
* Famosos lugares de interés como Starbay, El Capitán, la Ruta 66, el USS Lexington, el puente de Corpus Christi y las Salinas.
* 60 paradas de camiones únicas y 30 áreas de descanso específicas


![TExasAnuncio900](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ATS/Texas/Texas_Road_map_big.webp)


También tenemos que deciros que como suele ser habitual se ha preparado un evento especial de [World of Trucks](https://www.worldoftrucks.com/en/) llamado **"Cruising Texas event"** al que podeis uniros y del que teneis información en [este enlace](https://blog.scssoft.com/2022/11/cruising-texas-event.html). Como siempre, nos encanta hablaros de los pruductos de SCS porque consideramos que tienen una grandísima calidad, y además sabemos que muchos de vosotros, miembros de la comunidad de JugandoEnLinux.com sois fans de ETS2 y ATS, como así nos lo haceis saber a traves de nuestros emisiones a través de [Youtube](https://www.youtube.com/@jugandoenlinux) y [Twitch](https://www.twitch.tv/jugandoenlinux/). Si tenemos la colaboración de SCS, esperamos poder ofreceros una serie de streams especiales recorriendo este estado para que disfruteis en nuestra compañía de esta DLC. Mientras tanto os dejamos con el **trailer oficial** de esta expansión:  
 


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/cMSEm-dbTCQ" title="YouTube video player" width="780"></iframe></div>


**Podeis comprar American Truck Simulator - Texas DLC en Steam:**  
 <div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/1465750/" width="646"></iframe></div>   
  
¿Os apetece recorrer Texas en camión? ¿Qué opinión os merecen los productos de SCS Software? Puedes contarnos tu parecer sobre esta noticia en los comentarios, o en mensajes en nuestra comunidad de [Matrix](https://matrix.to/#/#jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux). 

