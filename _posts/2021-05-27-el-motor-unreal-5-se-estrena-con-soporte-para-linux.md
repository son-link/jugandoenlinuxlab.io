---
author: P_Vader
category: Software
date: 2021-05-27 08:05:42
excerpt: "<p>Con grandes mejoras acaban de anunciar su salida en acceso anticipado,\
  \ no apto a\xFAn para producci\xF3n.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/UnrealEngine/UE5Editor.webp
joomla_id: 1310
joomla_url: el-motor-unreal-5-se-estrena-con-soporte-para-linux
layout: post
tags:
- unreal-engine
- early-access
- unreal-engine-5
title: El motor Unreal 5 se estrena con soporte para Linux
---
Con grandes mejoras acaban de anunciar su salida en acceso anticipado, no apto aún para producción.


Y tras la "polémica" de si Epic había dado a entender o no si Linux tendría cabida en su próximo motor Unreal, [finalmente se anunció](https://www.unrealengine.com/en-US/blog/unreal-engine-5-is-now-available-in-early-access) el estreno de esta versión 5 y el logotipo del pingüino aparece sin lugar a dudas entre las plataformas soportadas.


Y es que en su día el **video que anunciaba esta versión 5 se centró excesivamente en Play Station** (no era de extrañar tras su [reciente idilio con Sony)](https://hipertextual.com/2020/07/sony-inversion-epic-games) y por ningún lado apareció el logotipo de Linux. Aunque poco después aclararon que [*"linux isnt going anywhere"*](https://www.gamingonlinux.com/2020/05/unreal-engine-5-announced-epic-online-services-are-now-online/comment_id=180884) o sea que linux no se mueve de donde está.


Pero ya no hay duda, si es que en algún momento la hubo, **puedes ver el logotipo Linux junto a sus recomendaciones en su [nota de lanzamiento](https://docs.unrealengine.com/5.0/en-US/ReleaseNotes/#new:platformsdkupgrades):**


![Plataformas de unreal engine 5](https://docs.unrealengine.com/5.0/Images/ReleaseNotes/image_37.jpg)


Y entre las novedades que trae este motor cabe destacar:


* **Nanite** es capaz de crear millones de micropolígonos en tiempo real con tantos detalle como el ojo humano puede alcanzar a observar.
* **Lumen** crea una iluminación global dinámica de lo que aparece en pantalla extremadamente realista.
* **Data Layers** un editor mas rápido y potente para crear grandes mundos abiertos y dinámicos.
* **Temporal Super Resolutio**, una alternativa a DLSS de Nvidia optimizado para las tecnologías de las nuevas AMD Raedon, aunque el DLSS de Nvidia tambien estará presente.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="440" src="https://player.vimeo.com/video/554262803" title="vimeo-player" width="780"></iframe></div>

Realmente el motor viene con unas mejoras impresionantes. Si deseas saber en detalle sobre este lanzamiento puedes ver sus [notas aquí](https://docs.unrealengine.com/5.0/en-US/ReleaseNotes/) y su [anuncio en su blog](https://www.unrealengine.com/en-US/blog/unreal-engine-5-is-now-available-in-early-access).


¿Que te parece este nuevo motor? ¿Lo usarás en el desarrollo de tu próximo juego en Linux? Cuéntanos mas en nuestros canales de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org) y [Telegram](https://t.me/jugandoenlinux).

