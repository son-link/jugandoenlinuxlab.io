---
author: Serjor
category: Noticia
date: 2018-12-07 18:27:08
excerpt: "<p>Curiosamente, hoy tambi\xE9n NVIDIA anuncia tambi\xE9n una nueva versi\xF3\
  n estable de su driver</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/5cb7d9c008502deb8141091027f5f238.webp
joomla_id: 926
joomla_url: nuevo-driver-privativo-415-22-para-graficas-nvidia
layout: post
tags:
- drivers
- nvidia
title: "Nuevo driver privativo 415.22 para gr\xE1ficas NVIDIA"
---
Curiosamente, hoy también NVIDIA anuncia también una nueva versión estable de su driver

Parece que el día de hoy va de drivers. Si hace unas horas os [anunciábamos](index.php/homepage/noticias/39-noticia/1046-la-version-18-3-de-mesa-ya-disponible) que había una versión nueva de MESA, vía [phoronix](https://www.phoronix.com/scan.php?page=news_item&px=NVIDIA-415.22-Linux-Driver) nos enteramos de que NVIDIA no ha querido ser menos (y no haremos el chiste fácil con el juego de palabras), y también ha lanzado una versión estable de sus drivers propietarios, la versión 415.22.


Al igual que su homólogo libre, el driver privativo incluye como funcionalidad destacada el soporte en la rama estable de la extensión "VK_EXT_transform_feedback", la cuál como os comentábamos en el anterior artículo, tiene especial relevancia para el proyecto DXVK y que este pueda dar un mejor soporte a los juegos basados en DirectX.


Además corrige un fallo introducido en las últimas versiones del driver que impedía que algunos juegos hechos con Unity funcionaran, y ahora el driver se puede compilar contra las RC del kernel 4.20.


¿Y tu distro ya soporta esta driver? Si los estás usando cuéntanos qué tal rinde en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD)

