---
author: leillo1975
category: "Acci\xF3n"
date: 2020-11-12 15:19:04
excerpt: "<p>Sorteamos una copia del #SOR4 de @Dotemu y @lizardcube <span class=\"\
  link-complex-target\">.&nbsp; </span></p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/StreetsOfRage4/SOR4.webp
joomla_id: 1262
joomla_url: streets-of-rage-4-disponible-nativamente-para-linux-a1
layout: post
tags:
- beat-em-up
- retro
- ethan-lee
- dotemu
- lizardcube
- streets-of-rage-4
title: "\xA1Streets of Rage 4 disponible nativamente para Linux! (ACTUALIZADO) "
---
Sorteamos una copia del #SOR4 de @Dotemu y @lizardcube .  


**ACTUALIZACIÓN 26-11-20:**


Gracias a la colaboración de [Dotemu](https://www.dotemu.com/), la editora del Streets of Rage 4, tenemos la oportunidad de regalar una copia del juego entre todos vosotros. Para ello hemos grabado un video jugando a dobles Pato y yo, en donde en determinados momentos paramos y os descubrimos una sección de la clave de Steam. Pues ya sabeis, si quereis ver como nos las apañamos soltando mamporros por partida doble y de paso llevaros un buen regalo, no teneis más que ver el siguiente video:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/oHweBt7V5bc" style="display: block; margin-left: auto; margin-right: auto;" width="780"></iframe></div>


...Y si sois alguno de vosotros el afortunado, hacednoslo saber con un comentario en esta noticia, en el video de Youtube, o dejando mensaje en nuestros grupos de JugandoEnLinux en [Telegram](https://twitter.com/JugandoenLinux) o  [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).


 




---


**NOTICIA ORIGINAL:**


Mucho se ha hablado, y mucho se ha discutido desde que el Streets of Rage 4 fué lanzado para Windows.... y también mucho tiempo ha pasado. Desde el 30 de Abril, fecha en que el juego fué publicado oficialmente, hasta ahora, se ha debatido por muchos rincones de internet el porque este juego no tenía soporte oficial para nuestro sistema, siendo este trabajo algo relativamente sencillo gracias a que está hecho con XNA, y este puede ser portado a Linux con [FNA](https://github.com/FNA-XNA/FNA), la reimplementación libre de este entorno de desarrollo creada por [Ethan Lee](http://www.flibitijibibo.com/index.php?page=Portfolio/Ports), el conocido "portador" de videojuegos.


Como veis todo parecía ir a favor de que pudiesemos disfrutar el día del estreno o poco después de una versión nativa del juego, especialmente porque [Lizardcube](https://www.lizardcube.com/), sus desarrolladores, ya habían trabajado con Ethan Lee en el pasado con **Wonder Boy: The Dragon's Trap**, juego que en su día [analizamos en nuestra web](index.php/component/k2/1-blog-principal/analisis/564-analisis-wonder-boy-the-dragon-s-trap). Pero finalmente se decidió que el juego funcionaba bastante bien con Proton y se abandonó la idea, quedando el port de Ethan en suspenso. Pero, paradojas de la vida, el juego que parecía ir de maravilla en un principio, al poco tiempo, no se si por actualizaciones de este o de Proton, dejó de funcionar correctamente.


Olvidando todo este lio, **lo que verdaderamente importa es que los usuarios del pingüino, aunque tarde, finalmente podremos disfrutar de este magnífico juego** que  continua una de las sagas más exitosas de los **Beat'em-Up**, y que en su día jugamos en nuestra queridísima Megadrive. Este Streets Of Rage 4 ha obtenido **notas superlativas en montones de webs**, ha sido aclamado por la crítica y jugadores, y como sabeis, es mucho más que un lavado de cara. Según la descripción oficial que nos facilita Lizardcube, encontraremos esto:


*-El regreso de la legendaria serie Streets of Rage.*  
 *-Hermosos gráficos totalmente dibujados a mano animados por el estudio detrás de Wonder Boy: The Dragon’s Trap.*  
 *-Jugabilidad clásica mejorada con nuevas mecánicas.*  
 *-Banda sonora de una amplia gama de músicos de talla mundial.*  
 *-5 personajes jugables principales y muchos personajes retro desbloqueables.*  
 *-2 jugadores en modo cooperativo en línea y hasta 4 jugadores a nivel local.*  
 *-Modo historia, modo arcade, modo batalla.*  
 *-Pollo asado (¿o estofado?) ¡Servido en un plato prístino y más!*


Nosotros, desde JugandoEnLinux.com **intentaremos , si tenemos la colaboración de sus desarrolladores , ofreceros una serie de Streams** en nuestros canales de [Youtube](https://www.youtube.com/c/jugandoenlinuxcom) y [Twitch](https://www.twitch.tv/jugandoenlinux) para que veais de primera mano como se mueve el juego en nuestro sistema, además de un un **completo análisis del título**, por lo que os recomendamos que esteis atentos a nuestras redes sociales de [Twitter](https://twitter.com/JugandoenLinux) y [Mastodon](https://mastodon.social/@jugandoenlinux). Podeis comprar Streets of Rage 4 para Linux en la [**tienda de Humble Bundle**](https://www.humblebundle.com/store/streets-of-rage-4?partner=jugandoenlinux) **y** en [Steam](https://store.steampowered.com/app/985890/Streets_of_Rage_4/) (en GOG.com no muestra el icono de Linux). Os dejamos con el trailer oficial:  
  
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/owbKz1qcnUI" style="display: block; margin-left: auto; margin-right: auto;" width="780"></iframe></div>


¿Habeis jugado alguna vez a esta mítica saga? ¿Qué os parece el trabajo de Lizardcube/Dotemu con este clásico? Cuéntanoslo en los comentarios o en nuestros grupos de JugandoEnLinux en [Telegram](https://twitter.com/JugandoenLinux) o  [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

