---
author: Pato
category: Aventuras
date: 2018-05-09 07:54:18
excerpt: "<p>El notable t\xEDtulo de Fullbright Studios est\xE1 de oferta por su lanzamiento\
  \ en consolas</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/1f927a512a9909833c7b57ab40062fc5.webp
joomla_id: 727
joomla_url: tacoma-se-actualiza-con-un-modo-comentarios
layout: post
tags:
- indie
- aventura
title: '''Tacoma'' se actualiza con un modo "comentarios"'
---
El notable título de Fullbright Studios está de oferta por su lanzamiento en consolas

'Tacoma' es un "walking simulator" con una estética única y una historia absorbente que ahora además recibe una actualización que introduce un modo "comentarios" con más de 2 horas de audio de los creadores de este magnífico juego comentando los detalles de desarrollo, los secretos del juego y el proceso de creación de este mundo ficticio junto a las ideas de los creadores sobre los personajes que aparecen en el juego. Todo ello esta disponible clicando en los distintos iconos que aparecen ahora sobre ciertas partes de la Estación Espacial Tacoma.


*Tacoma es una aventura narrativa que tiene lugar en una estación espacial de alta tecnología en el año 2088. A lo largo de tu misión explorarás cada detalle de la vida y el trabajo de la tripulación de la estación y encontrarás las pistas que conformarán una apasionante historia de confianza, miedos y determinación frente al desastre.*


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/ySTa2GsVYAQ" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


*El sistema de vigilancia digital del centro, situado en el corazón de Tacoma, ha capturado las grabaciones 3D de los momentos decisivos de la vida de la tripulación en la estación. Conforme vayas explorando, te rodearán los ecos de estos momentos. Tendrás que emplear tus habilidades para retroceder, avanzar hacia delante y moverte por el espacio físico de estas complejas y entrecruzadas escenas para examinar los acontecimientos desde todos los ángulos, de forma que puedas reconstruir los distintos niveles de la historia a medida que explores.*


Si te gustan los juegos con excelente narrativa, tienes Tacoma en su página de Steam con un 25% de descuento por su lanzamiento en consolas, y además está traducido al español:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/343860/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 

