---
author: Pato
category: Editorial
date: 2018-10-15 11:06:12
excerpt: <p>Permitirnos teorizar sobre Steam Play, Steam Link, Proton y el juego en
  la nube...</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/38bf6e198d6986054cbf57dc3edbd491.webp
joomla_id: 873
joomla_url: y-si-valve-estuviera-pensando-en-el-streaming-opinion
layout: post
tags:
- steamos
- steam
- valve
- streaming
- steam-link
- opinion
title: "\xBFY si Valve estuviera pensando en un servicio de Streaming? - Opini\xF3\
  n -"
---
Permitirnos teorizar sobre Steam Play, Steam Link, Proton y el juego en la nube...

A estas alturas a nadie se le escapa que debemos gran parte de la buena salud del videojuego en Linux a Valve y sus múltiples frentes abiertos, dando impulso a todo lo necesario para hacer de Linux y su entorno una plataforma de videojuegos de primer nivel. Valve está involucrada directa o indirectamente en los drivers gráficos Mesa, en Vulkan, en DXVK, en Proton y Wine o en el soporte VR para Linux por citar solo algunos ejemplos.


Pero, como también hemos comentado en otras ocasiones el esfuerzo titánico que Valve está realizando en todos estos campos [no está teniendo un crecimiento pronunciado](index.php/homepage/noticias/39-noticia/989-la-cuota-de-linux-en-steam-crece-hasta-el-0-78-tras-la-correccion-de-un-error) en cuanto a número de usuarios. Sabemos, eso sí que [Valve parece tener interés en mantener un ecosistema en Linux como contrapartida a su dependencia de Windows](index.php/homepage/editorial/4-editorial/961-parece-que-valve-sabe-a-lo-que-juega-o-el-gigante-parece-haber-despertado) dado que Microsoft hace unos años comenzó una deriva hacia un sistema cada vez mas volcado en software como servicio y cada vez mas enfocado en su propia Store y ecosistema, pero siendo realistas es difícil justificar tanta inversión para tan poco retorno... ¿o si?...Si te sientes con ánimos, **vamos a teorizar un poco sobre este asunto**. Toma asiento que esto va a ser largo:


Con la aparición de [Steam Play](index.php/homepage/generos/software/12-software/960-steam-play-de-valve-da-un-salto-de-gigante-y-amplia-enormemente-el-catalogo-para-linux-steamos-actualizado), Steam está aumentando la base de juegos disponibles en Linux de forma exponencial dando lugar a un escenario en el que ya no es descabellado pensar en que Valve quiera re-lanzar SteamOS como sistema de videojuegos. Sabemos a ciencia cierta que siguen trabajando en el sistema con paso lento, pero seguro y **ya hay quien se aventura a sugerir un lanzamiento de una Steam Machine 2.0 diseñada, fabricada y distribuida por la propia Valve**, lo que supondría un nuevo intento de la compañía de entrar en el salón de casa con un PC consolizado. La ventaja respecto al relativo "fiasco" que supuso la primera hornada de Steam Machines es que **ahora Valve conoce de primera mano qué puntos fallaron y como corregirlos**, permitiéndole re-impulsar su propio ecosistema bajo la premisa de un hardware y un software realmente propios e integrados bajo su supervisión directa. Esto unido a la gran cantidad de juegos antiguos y nuevos soportados bajo Proton les daría una ventaja sustancial para convertirse por méritos propios en una "consola" muy competente, a poco que sepan jugar sus cartas.


Este movimiento además, daría sentido a [las palabras](https://www.gamasutra.com/view/news/315385/Gabe_Newell_Valve_has_always_been_a_bit_jealous_of_companies_like_Nintendo.php) que el propio Gabe Newell pronunció hace tan solo unos meses afirmando que tenían la capacidad de diseñar su propio hardware, y que "tenían envidia" de como Nintendo integra su hardware con su software para conseguir una experiencia sobresaliente.


![protonGTAV](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/steamlink/Proton/protonGTAV.webp)


*Proton ejecutando GTAV - Puede que estemos ante el paso que Valve estaba buscando*


Pero aparte de todo esto, existe otra teoría que hace pensar que Valve tiene interés en llevar cuantos más juegos a Linux mejor, incluyendo la mayor optimización de la que sean capaces de llevar a cabo. Y esta teoría no es otra que la del **videojuego como streaming**.


Desde hace ya unos meses se vienen oyendo mas y mas rumores de que los grandes de la industria, ya sea del videojuego (Microsoft con rumores de una futura versión de Xbox pensada para streaming, Sony con su Playstation Now) como grandes corporaciones con intereses en el campo multimedia (Amazon, Nvidia, Google...) empiezan a tener **proyectos de streaming de videojuegos** para lanzar diversas *plataformas como servicio*. Todas ellas están ahora mismo inmersas en la investigación y desarrollo de tecnologías de streaming o juego en la nube **que les permita monetizar un servicio de videojuegos bajo demanda** a través de internet de banda ancha en lo que parece una carrera contra-reloj para ser los primeros en ofrecer una experiencia satisfactoria a nivel mundial. Los juegos se ejecurtarían en potentes granjas de servidores ubicados estrategicamente con la suficiente capacidad como para servir la imagen, el sonido y la respuesta de los jugadores casi como si de una consola real se tratase, de modo que **cualquier dispositivo**, por poco potente que sea sería capaz de "mostrar" un videojuego de forma satisfactoria con el suficiente ancho de banda. Google, sin ir mas lejos [anunció esta semana](https://www.businessinsider.es/google-chrome-assassins-creed-odyssey-project-stream-2018-10?r=US&IR=T) un proyecto piloto en este sentido en el que los afortunados participantes podrán jugar el último Assassin's Creed en el navegador Chrome bajo streaming.


Es por esto que muchas de estas compañías ya piensan que el futuro del videojuego está en este tipo de servicios de **streaming bajo demanda**, puesto que de este modo conseguirían "matar" varios pájaros de un tiro. Por una parte, **conseguirían un suculento ingreso** a modo de "royalties" por lanzar videojuegos en sus plataformas al estilo de lo que hacen ahora compañías como Nintendo o Sony y además **ofrecerían una solución a los estudios en torno a la piratería**, ya que al no haber archivos locales no hay nada que piratear. Por otra parte, **eliminan la barrera de entrada del hardware**, ya que no haría falta un dispositivo caro o muy potente para poder jugar. Y por otra parte, se asegurarían el **aumento de potenciales clientes** a nivel exponencial ya que cualquier dispositivo con la suficiente capacidad de ancho de banda sería susceptible de convertirse en una potente consola. Por no decir que el nivel de ingresos de un servicio de este tipo sería superior a cualquier otro que podamos pensar, ya que las compañías pretenden ofrecer como ventaja al jugador el acceso tanto a un incipiente (pero creciente) catálogo de juegos como a las últimas novedades **bajo el pago de una cuota mensual de módico precio**. No está mal pensado... ¿Verdad?


Todo esto ya es "*trending topic*", y es de esperar que Valve no esté esperando a verlas venir. En este sentido, [comienzan a oírse voces](https://www.youtube.com/watch?v=M0h_PhJJ14M) que teorizan que Valve estaría forzando la máquina para conseguir tener algo con lo que competir a esta nueva forma de jugar videojuegos. Y aquí es donde toman forma todos los movimientos de Valve:


Para conseguir un servicio de Streaming competente necesitan **una gran cantidad de servidores** **con capacidad suficiente para ejecutar los juegos bajo demanda**, y es sabido que Valve cuenta con una de las mejores infraestructuras de servidores.


También necesitan **un sistema bajo el que correr esos juegos**, y es obvio el sistema en el que Valve ejecutaría su infraestructura. Para ello necesita cuantos mas videojuegos se ejecuten bajo linux de la forma mas optimizada posible mejor, y aquí es donde entra Vulkan, Proton, DXVK y demás tecnologías.


También se necesita **una buena tecnología de streaming**. Y no es descabellado pensar que ya la tienen, pues ya llevan varios años puliendo dicha tecnología con la retransmisión en casa y el Steam Link, además de su incursión en otros dispositivos móviles con la [Steam Link App](index.php/homepage/generos/software/12-software/852-steam-link-app-y-steam-video-app-llegaran-proximamente-a-nuestros-dispositivos-moviles-actualizacion).


![Steamlinkandcontroller](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/steamlink/Steamlinkandcontroller.webp)


*¿Puede Valve tener ya un sistema de Streaming con potencial para el juego en la nube con el combo Steam Link - Steam Controller?*


Y por último, se necesita **un hardware que permita reproducir el streaming multimedia**, y retornar la respuesta de los jugadores, cosa que **el propio Steam Link** o una futura Steam Machine junto al Steam Controller pueden cumplir sin mayores problemas.


Como veis, Valve tiene los recursos, tiene los juegos, tiene la tecnología y tiene el hardware. ¿No creéis que pueden estar pensando en lanzar su propio servicio de streaming de videojuegos apoyados en toda su infraestructura y en Linux?


 


Si has leído hasta aquí, ¡enhorabuena! ahora nos gustaría saber qué piensas de toda esta película (o empanada mental) que nos hemos hecho.


Puedes contarnos lo que piensas al respecto en los comentarios o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

