---
author: leillo1975
category: Software
date: 2018-11-09 08:15:02
excerpt: "<p>@EpicGames ha incluido este cambio en su \xFAltima actualizaci\xF3n de\
  \ este conocido motor gr\xE1fico.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/655cf7f08254436b1df6b279ffc38784.webp
joomla_id: 893
joomla_url: vulkan-sera-la-opcion-por-defecto-en-unreal-engine-4-en-gnu-linux
layout: post
tags:
- software
- vulkan
- unreal-engine-4
- epic-games
title: "Vulkan ser\xE1 la opci\xF3n por defecto de Unreal Engine 4 en GNU/Linux."
---
@EpicGames ha incluido este cambio en su última actualización de este conocido motor gráfico.

Ayer por la noche nuestro amigo **@lordgault** nos enviaba a través de Telegram un interesante artículo de [MuyLinux](https://www.muylinux.com/2018/11/08/unreal-engine-vulkan-defecto-linux/) en el que nos informaba que uno los motores más populares y potentes para la creación de videojuegos se actualizaba **estableciendo por defecto la opción de Vulkan como API para crear versiones para Linux**. El conocido software de Epic Games ha llegado a la [versión 4.21](https://www.unrealengine.com/en-US/blog/unreal-engine-4-21-released) donde ente otras muchas cosas ha cumplido una de sus intenciones que tenía pendiente desde el año pasado.


De todos es sabido que **la API gráfica Vulkan proporciona una potencia a la hora de desarrollar (y jugar) mucho más eficiente en tasas de rendimiento que la última versión de OpenGL**, aprovechando mejor las **capacidades multinúcleo y trabajando más directamente con la gráfica**. Además le pone las cosas mucho más fáciles a los desarrolladores a la hora de **crear videojuegos para diferentes plataformas**, ya que esa es una de sus principales virtudes. Un software desarrollado con Vulkan es mucho más fácil de portar a los diferentes sistemas que la propietaria DirectX (Windows, XBOXOne). Un mismo juego puede ser llevado de una forma mucho más sencilla a Android, Switch, Windows, Linux.... Por supuesto la opción de OpenGL seguirá estando ahí para hardware que no lo soporte, pero obviamente su uso cada vez está menos justificado.


Otro tema diferente es que los desarrolladores finalmente decidan abandonar DirectX a favor de Vulkan, pero obviamente **este es un paso que puede añadir un poquito más de peso en la balanza a favor de Vulkan**. También indirectamente ayudaría a Valve, ya que favorece la creación de versiones para Linux, y aunque no lo hagan, al escoger Vulkan para Windows, la "emulación" de Steam Play/Proton puede hacer el resto de una forma mucho más sencilla.


En cuanto a otros cambios de esta nueva versión de UE4 **también encontramos multitud de arreglos y mejoras para nuestro sistema**, y un nuevo reproductor de videos WebM que usen VP8 y VP9. En otros sistemas se han incluido los efectos Niagara para Nintendo Switch y montones de optimizaciones para Android e iOS.


Esperemos que este paso dado por Epic Games facilite la llegada de más juegos a GNU/Linux-SteamOS, y quien sabe, algún día veamos el conocido Fortnite en nuestros sistemas, ¿verdad [@**TimSweeneyEpic**](https://twitter.com/TimSweeneyEpic)? Podeis dejar vuestras opiniones sobre este tema en los comentarios o charlar sobre ello en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

