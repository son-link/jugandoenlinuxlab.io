---
author: Pato
category: Aventuras
date: 2018-01-30 17:39:20
excerpt: "<p>Incluir\xE1 los tres episodios y otros a\xF1adidos</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/abc7c98b4ddfae4816b74e0d2fd932d2.webp
joomla_id: 625
joomla_url: exordium-games-lanzara-bear-with-me-edicion-coleccionista-el-8-de-febrero
layout: post
tags:
- indie
- aventura
- aventura-grafica
title: "Exordium Games lanzar\xE1 'Bear with Me: Edici\xF3n Coleccionista' el 8 de\
  \ Febrero"
---
Incluirá los tres episodios y otros añadidos

Hace ya tiempo que no teníamos noticias de 'Bear with Me' la serie de aventuras "noir" por episodios [que llego](index.php/homepage/generos/aventuras/item/56-bear-with-me-una-aventura-grafica-con-el-estilo-de-una-novela-negra-ya-se-puede-investigar-en-linux) a nuestro sistema favorito ... hace tiempo.


Tras completar la serie de episodios que ya están disponibles en Linux/SteamOS ahora Exordium Games nos traerá una "Edición Coleccionista" que incluirá los tres episodios que cuentan la historia de Amber y Ted al completo y además diversos añadidos, como su banda sonora, un libro de arte y los documentos como el guión o el diseño de puzzles.


*Bear With Me [[web oficial](http://www.bearwithmegame.com/)] es una aventura gráfica episódica al más puro estilo de la novela negra. Amber intenta localizar a su hermano perdido con la ayuda de su osito de confianza, Ted E. Bear.*


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/HTlJtklZY0M" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 **Características**  
  



* Aventura gráfica de «terror» con un gran guion de novela negra.
* Diseños y animaciones 2D hechos a mano.
* Todos los juegos de palabras que te puedas imaginar. Sarcasmo, la más oscura ironía y diversión de principio a fin.
* Gran variedad de pistas y puzles para desvelar el misterio.
* Apoyo detectivesco interactivo y a veces controlable de Ted E. Bear.
* Sencillo sistema de consejos para evitar tocar píxeles a lo loco.
* Banda sonora original.


'Bear with Me: Edición Coleccionista' estará disponible el próximo 8 de Febrero en su página de Steam donde estará traducido al español:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/783050/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 ¿Te gustan las aventuras gráficas "noir"? ¿Te asustan los ositos de peluche?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

