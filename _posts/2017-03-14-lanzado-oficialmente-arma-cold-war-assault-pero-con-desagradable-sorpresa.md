---
author: leillo1975
category: "Simulaci\xF3n"
date: 2017-03-14 15:03:01
excerpt: "<p>Hace poco m\xE1s de un mes <span style=\"color: #000080;\"><a href=\"\
  index.php/item/287-el-clasico-arma-cold-war-assault-sera-portado-por-virtual-programming\"\
  \ target=\"_blank\" style=\"color: #000080;\">inform\xE1bamos</a></span> en esta\
  \ web de que la compa\xF1\xEDa <span style=\"color: #000080;\"><a href=\"https://www.vpltd.com/\"\
  \ target=\"_blank\" style=\"color: #000080;\">Virtual Programming</a></span> estaba\
  \ portando este cl\xE1sico de <span style=\"color: #000080;\"><a href=\"https://www.bistudio.com/\"\
  \ target=\"_blank\" style=\"color: #000080;\">Bohemia Interactive</a></span>. Hoy,\
  \ poco m\xE1s de un mes despu\xE9s, y seg\xFAn acabo de leer en <span style=\"color:\
  \ #000080;\"><a href=\"https://www.gamingonlinux.com/articles/arma-cold-war-assault-released-for-linux-mac-but-its-separated-from-the-windows-version.9308\"\
  \ target=\"_blank\" style=\"color: #000080;\">www.GamingOnLinux.com</a><span style=\"\
  color: #000000;\">, ha sido lanzado oficialmente en Steam a un atractivo precio\
  \ de 4\u20AC. </span></span></p>\r\n<p>&nbsp;</p>\r\n<p>En cuanto a la segunda parte\
  \ del t\xEDtulo, viene dada a que Virtual Programming ha lanzado este t\xEDtulo\
  \ de forma separada a la versi\xF3n que ya exist\xEDa, es decir que no podremos\
  \ hacer uso de Steamplay lo que implica que aunque tengamos comprado el juego anteriormente\
  \ en Steam para Windows, si queremos jugar con la versi\xF3n de Linux deberemos\
  \ pasar de nuevo por caja. No entiendo muy bien esta pol\xEDtica, aunque es obvio\
  \ que VP aparte de hacernos pasar por caja una segunda vez (o tercera como ser\xED\
  a mi caso) a los que ya poseemos este juego, lo cierto es que tambi\xE9n se asegura\
  \ de \"recibir su parte\" ya que public\xE1ndo el mismo juego de forma separada\
  \ no queda lugar a dudas de qu\xE9 claves del juego son de Linux y cuales de otros\
  \ sistemas. Eso aparte de quitarse de un plumazo el mercado de re-venta de claves\
  \ o \"mercado gris\".</p>\r\n<p>Quiero aclarar que personalmente no pienso hacerle\
  \ el juego a VP. No voy a pagar 3 veces por el mismo juego, y me explico. En el\
  \ a\xF1o 2001 el juego fu\xE9 lanzado con el nombre Operation Flashpoint, y aqu\xED\
  \ un servidor se dej\xF3 los cuartos en \xE9l (por cierto, muy bien invertidos).\
  \ Hace relativamente poco adquir\xED el juego en Steam, y digamos que ahora no voy\
  \ a volver a pagar una tercera vez para poder jugarlo \"nativamente\" en mi sistema.\
  \ Espero que con el tiempo rectifiquen, ya que sinceramente no creo que consigan\
  \ grandes beneficios con esta pol\xEDtica, sin\xF3 m\xE1s bien el enfado de la comunidad\
  \ y quedar marcados en nuestro inconsciente como \"peseteros\". Estoy de acuerdo\
  \ que esta compa\xF1\xEDa, como todas, est\xE1 en el negocio para ganar dinero,\
  \ pero ese dinero hay que ganarlo de una forma \xE9tica, y si casualmente una de\
  \ las caracter\xEDsticas de Steam es poder ejecutar el juego que posees en la plataforma\
  \ que elijas, dar este paso en la direcci\xF3n contraria por un juego del 2001 que\
  \ cuesta 4\u20AC se me antoja incluso rid\xEDculo.</p>\r\n<p>&nbsp;</p>\r\n<p><iframe\
  \ src=\"https://www.youtube.com/embed/a75eG8HG5Fk\" width=\"560\" height=\"315\"\
  \ style=\"display: block; margin-left: auto; margin-right: auto;\" allowfullscreen=\"\
  allowfullscreen\"></iframe></p>\r\n<p>&nbsp;</p>\r\n<p>Como informaci\xF3n para\
  \ vosotros, Arma: Cold War Assault es un juego de simulaci\xF3n militar donde podreis\
  \ poneros en la piel de un soldado raso, y a medida que avanzamos en el juego poder\
  \ tener un pelot\xF3n a nuestras ordenes, as\xED como poder conducir diferentes\
  \ vehiculos, tales como Jeeps, Camiones, Tanques o incluso Helicopteros. Un juego\
  \ muy recomendable <strong>en el caso de que no lo tengais</strong> y que podeis\
  \ adquirir en Steam:</p>\r\n<p>&nbsp;</p>\r\n<p><iframe src=\"https://store.steampowered.com/widget/594550/\"\
  \ width=\"646\" height=\"190\" style=\"display: block; margin-left: auto; margin-right:\
  \ auto;\"></iframe></p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;Puedes decirnos que te parece\
  \ este juego, la pol\xEDtica de ventas de Virtual Programming o lo que quieras en\
  \ los comentarios de esta noticia o en nuestros canales de <span style=\"color:\
  \ #000080;\"><a href=\"https://telegram.me/jugandoenlinux\" target=\"_blank\" style=\"\
  color: #000080;\">Telegram</a></span> o <span style=\"color: #000080;\"><a href=\"\
  https://discord.gg/ftcmBjD\" target=\"_blank\" style=\"color: #000080;\">Discord</a></span></p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/ec9e5d4418cddeb916f54861fa51abb5.webp
joomla_id: 247
joomla_url: lanzado-oficialmente-arma-cold-war-assault-pero-con-desagradable-sorpresa
layout: post
tags:
- virtual-progamming
- arma
- cold-war-assault
title: 'Lanzado oficialmente Arma: Cold War Assault, pero con desagradable sorpresa'
---
Hace poco más de un mes [informábamos](index.php/item/287-el-clasico-arma-cold-war-assault-sera-portado-por-virtual-programming) en esta web de que la compañía [Virtual Programming](https://www.vpltd.com/) estaba portando este clásico de [Bohemia Interactive](https://www.bistudio.com/). Hoy, poco más de un mes después, y según acabo de leer en [www.GamingOnLinux.com](https://www.gamingonlinux.com/articles/arma-cold-war-assault-released-for-linux-mac-but-its-separated-from-the-windows-version.9308), ha sido lanzado oficialmente en Steam a un atractivo precio de 4€. 


 


En cuanto a la segunda parte del título, viene dada a que Virtual Programming ha lanzado este título de forma separada a la versión que ya existía, es decir que no podremos hacer uso de Steamplay lo que implica que aunque tengamos comprado el juego anteriormente en Steam para Windows, si queremos jugar con la versión de Linux deberemos pasar de nuevo por caja. No entiendo muy bien esta política, aunque es obvio que VP aparte de hacernos pasar por caja una segunda vez (o tercera como sería mi caso) a los que ya poseemos este juego, lo cierto es que también se asegura de "recibir su parte" ya que publicándo el mismo juego de forma separada no queda lugar a dudas de qué claves del juego son de Linux y cuales de otros sistemas. Eso aparte de quitarse de un plumazo el mercado de re-venta de claves o "mercado gris".


Quiero aclarar que personalmente no pienso hacerle el juego a VP. No voy a pagar 3 veces por el mismo juego, y me explico. En el año 2001 el juego fué lanzado con el nombre Operation Flashpoint, y aquí un servidor se dejó los cuartos en él (por cierto, muy bien invertidos). Hace relativamente poco adquirí el juego en Steam, y digamos que ahora no voy a volver a pagar una tercera vez para poder jugarlo "nativamente" en mi sistema. Espero que con el tiempo rectifiquen, ya que sinceramente no creo que consigan grandes beneficios con esta política, sinó más bien el enfado de la comunidad y quedar marcados en nuestro inconsciente como "peseteros". Estoy de acuerdo que esta compañía, como todas, está en el negocio para ganar dinero, pero ese dinero hay que ganarlo de una forma ética, y si casualmente una de las características de Steam es poder ejecutar el juego que posees en la plataforma que elijas, dar este paso en la dirección contraria por un juego del 2001 que cuesta 4€ se me antoja incluso ridículo.


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/a75eG8HG5Fk" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


Como información para vosotros, Arma: Cold War Assault es un juego de simulación militar donde podreis poneros en la piel de un soldado raso, y a medida que avanzamos en el juego poder tener un pelotón a nuestras ordenes, así como poder conducir diferentes vehiculos, tales como Jeeps, Camiones, Tanques o incluso Helicopteros. Un juego muy recomendable **en el caso de que no lo tengais** y que podeis adquirir en Steam:


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/594550/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


 Puedes decirnos que te parece este juego, la política de ventas de Virtual Programming o lo que quieras en los comentarios de esta noticia o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD)

