---
author: P_Vader
category: Software
date: 2021-07-28 15:51:46
excerpt: "<p>Nuevas mejores para este lanzador multiplataforma de juegos de la tienda\
  \ de juegos de Epic.</p>\r\n"
image: https://user-images.githubusercontent.com/26871415/127303104-52cf66b8-901f-4399-8306-2285a501cdbe.png
joomla_id: 1333
joomla_url: heroic-games-launcher-llega-a-la-version-1-9
layout: post
tags:
- lanzador
- epic-game-store
- heroic-games-launcher
title: "Heroic Games Launcher llega a la versi\xF3n 1.9"
---
Nuevas mejores para este lanzador multiplataforma de juegos de la tienda de juegos de Epic.


Llega una nueva versión de este lanzador multiplataforma de juegos de la tienda de juegos de Epic, que pese a que repetidamente han demostrado que no les interesa por ahora nuestro sistema, este proyecto continua adelante con su desarrollo y con un avance envidiable ¿Será que eso de que regalen juegos llega a incentivar tanto?


En resumidas cuentas **se ha acelerado el arranque del lanzador haciendo uso de caches** y han habilitado que **se pueda usar FSR en Linux, si tienes la versión de wine** que lo permita.


Aquí todas la [novedades](https://github.com/Heroic-Games-Launcher/HeroicGamesLauncher/releases/tag/v1.9.0) que trae:


* Rendimiento de Heroic a mejorado al almacenar en caché la información de la biblioteca y realizar solicitudes en segundo plano. Haciendo que la apertura de Heroic sea casi instantánea y la navegación más receptiva.
* Se agregó un rastreador de tiempo de juego (es bueno saber cuánto tiempo llevas jugando un juego: P)
* Pantalla de inicio de sesión rehecha de acuerdo con el nuevo diseño.
* Al hacer clic en instalar desde la biblioteca, Heroic mostrará una ventana con las opciones para instalar o importar el juego.
* Se agregaron iconos de bandeja de mejor calidad.
* Se agregó la opción de iniciar Heroic minimizado.
* Se cambió el tema principal a tonos de azul.
* Se cambiaron varios elementos de la interfaz de usuario, como la tarjeta de juego, que ahora siempre muestra el título, la instalación, la configuración y los botones de reproducción.
* Se cambió el filtro para que sea una lista ya que estaba creciendo en tamaño.
* Cambió la posición de la barra de búsqueda al encabezado en lugar de la barra de navegación.
* Ya no es necesario reiniciar Heroic después de cambiar el color del icono de la bandeja.
* Varias funciones fueron refactorizadas para mejorar el rendimiento y la estabilidad de Heroic.
* Ejecutar Heroic desde la terminal debería tener aún más registros ahora, genial para depurar.
* Se eliminó la capacidad de descargar varios juegos al mismo tiempo, ya que esto hacía que algunos de ellos aparecieran como no instalados (evolucionarán a una cola en algún momento).
* Se movieron algunas configuraciones de General a Otro.
* **Linux** : Se agregó el conmutador FSR Hack y la fuerza de Nitidez a la configuración de Wine (necesita soporte en Wine).
* **Linux** : se agregó el cambio de barra de tamaño variable a la configuración de Wine (necesita soporte en Wine y NVIDIA RTX para funcionar).
* **Linux / OSX** : se agregó información de wine y su prefijo en la página del juego.


 


Recuerda que esta tienda hace uso de wine para lanzar los juegos al no existir soporte nativo en la tienda de Epic.


¿Has probado ya esta nueva versión de Heroic Games Launcher? Cuéntanoslo en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).


 


 


 

