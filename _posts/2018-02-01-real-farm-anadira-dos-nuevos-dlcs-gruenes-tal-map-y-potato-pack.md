---
author: Pato
category: "Simulaci\xF3n"
date: 2018-02-01 09:42:18
excerpt: "<p>Los dos DLCs estar\xE1n disponibles de forma gratuita para todo el que\
  \ ya tenga el juego antes de su lanzamiento</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/a7c7e20ef55b9c6f68541bcec8a4cfb6.webp
joomla_id: 630
joomla_url: real-farm-anadira-dos-nuevos-dlcs-gruenes-tal-map-y-potato-pack
layout: post
tags:
- simulador
- agricultura
title: "'Real Farm' a\xF1adir\xE1 dos nuevos DLCs: Gr\xFCnes Tal Map y Potato Pack"
---
Los dos DLCs estarán disponibles de forma gratuita para todo el que ya tenga el juego antes de su lanzamiento

Como sabéis por anteriores artículos, ahora mismo tenemos disponibles ya dos juegos de gestión de granjas en nuestro sistema favorito. Uno de ellos es este ['Real Farm'](index.php/homepage/generos/simulacion/item/612-real-farm-el-simulador-de-granjas-llegara-a-linux-steamos-este-proximo-viernes) que tras su lanzamiento no ha cosechado muy buenas críticas por parte de los usuarios que digamos.


Sin embargo Triangle Studios lejos de tirar la toalla sigue implementando mejoras casi todas las semanas para hacer de este simulador agrícola un juego destacable. Ahora nos anuncian el próximo lanzamiento de dos DLCs gratuitos para el juego que nos traerán varios vehículos y dispositivos adicionales así como un mapa inspirado en los territorios de Europa Occidental:


**Sobre Potato Pack:**


¡Es hora de expandir la experiencia de Real Farm con el Lote de Patatas! Este lote te proporciona todo lo que necesitas para cultivar patatas, con nueva maquinaria y nuevos vehículos. ¡Completa un montón de trabajos nuevos y conviértete en un verdadero agricultor de patatas!   
   
 ● Cultiva un producto nuevo: ¡patatas!  
 ● Maneja 2 cosechadoras nuevas: la Rival PH-4 y la Rival PH-4X  
 ● Utiliza 5 dispositivos para plantar, cultivar y cosechar tus nuevos campos de patatas  
 ● Conduce 3 nuevos y divertidos vehículos, el Ebon Q-Bike, el Hercules S-GT y el Ebon SUV.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/4pD-P3aaK9o" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 **Sobre Grünes Tal Map:**


*¡El mundo de Real Farm se hace más grande! Sumérgete en el nuevo mapa ""Grünes Tal"". Sumérgete en este entorno inspirado por Europa Occidental, con bosques frondosos, colinas llenas de vegetación y pueblos pintorescos.*  
   
 *Empieza en Grün Tal con un nuevo vehículo que conducir: el Ebon Sports UV, ¡y prepárate para tu nueva aventura! ¡Completa nuevos trabajos, visita lugares nuevos y únete a la nueva comunidad de granjeros! Comienza tu viaje con una granja establecida en el modo Libre o empieza de cero en el modo Trayectoria, ¡todo es posible en Grün Tal!*


Estos dos DLCs estarán disponibles de forma gratuita para todo aquel que ya tenga el juego en el momento en que se pongan a la venta, aunque aún no han hecho pública la fecha de lanzamiento. Para los que lo compren posteriormente tendrán un precio de 9,99€.


Toda la información la tienes en el [anuncio oficial](http://steamcommunity.com/games/573680/announcements/detail/1640868374274883121).


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/770060/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/770061/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 

