---
author: Pato
category: Software
date: 2022-03-08 08:06:18
excerpt: "<p>Esta versi\xF3n 2.2.2 estar\xE1 disponible en breve en Flathub aunque\
  \ a\xFAn necesitar\xE1 un tiempo de rodaje</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/HeroicGamesLauncher/HeroicGLGOG1.webp
joomla_id: 1446
joomla_url: heroic-games-launcher-se-prepara-para-lanzar-version-flatpak-y-facilitar-su-uso-en-steam-deck
layout: post
tags:
- flatpak
- heroic-games-launcher
- steam-deck
title: "Heroic Games Launcher se prepara para lanzar versi\xF3n Flatpak y facilitar\
  \ su uso en Steam Deck"
---
Esta versión 2.2.2 estará disponible en breve en Flathub aunque aún necesitará un tiempo de rodaje


Heroic Games Launcher sigue avanzando en su empeño en convertirse en un lanzador todoterreno, y si el pasado 23 de Febrero [os contábamos]({{ "/posts/heroic-games-launcher-anade-soporte-para-gog-en-su-ultima-actualizacion" | absolute_url }}) que ya se implementó en soporte para GOG, hoy nos traen como novedad su trabajo para el próximo lanzamiento en versión Flatpak oficialmente, entre otras cosas para ofrecer mejor soporte en la portatil de Valve:


*"Hoy tenemos un nuevo lanzamiento de Heroic. No es una actualización importante, pero trae varias correcciones para la implementación de GOG tanto para Linux como para Windows (aunque todavía hay algunos errores en Windows).  
Lo más importante de este lanzamiento es que trae varias configuraciones para trabajar con el nuevo Flatpak que se publicará pronto en Flathub. Esto llevará a Heroic a más distros y también a Steam Deck, que ya funciona con AppImage pero necesita varias modificaciones para captar las entradas.  
El primer flatpak no será perfecto y algunas cosas podrían no funcionar como se esperaba. Pero, por supuesto, haremos todo lo posible para solucionar los próximos problemas :)  
Un agradecimiento especial a @mirkobrombin del equipo de Bottles por toda la ayuda con la compilación de Flatpak: D"*


¿Qué ha cambiado en este lanzamiento?


* [GOG] Varias mejoras y correcciones para GOGDL por @imLinguin en # 1046
* Arreglado el manejo del protocolo cuando Heroic ya está ejecutando @CommandMC en el # 1019
* [General] Mejorada la navegación en pantalla táctil y gamepad y reviertidos algunos colores por @wiryfuture en # 964
* [Linux] Mejorado el manejo de la versión Wine por @CommandMC en # 1008
* [General] Añadido el parámetro exe alternativo faltante para GOG Linux por @redromnon en # 1024
* [General] Mejoras y adiciones de UI por @ 8Bitz0 en # 1041
* [Linux] Compatibilidad Wine-GE-Proton por @ nezd5553 en # 1040
* [Linux] Preparativos Flatpak por @flavioislima en el # 515
* Actualización de traducciones de Weblate alojado por @weblate en # 1035


 


Puedes ver todas las versiones y descargas desde su [página oficial en Github](https://github.com/Heroic-Games-Launcher/HeroicGamesLauncher/releases/tag/v2.2.2).

