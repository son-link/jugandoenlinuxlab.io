---
author: Serjor
category: Software
date: 2019-09-16 20:38:15
excerpt: <p>La lista de parches en el kernel de Linux para facilitar la vida de los
  jugadores aumenta</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/540b4c71320024543f33aec82da1495c.webp
joomla_id: 1111
joomla_url: la-futura-version-5-4-del-kernel-de-linux-permitira-la-ejecucion-de-mas-juegos-y-aplicaciones-de-64-bits-via-wine-proton
layout: post
tags:
- wine
- kernel
- proton
- umip
title: "La futura versi\xF3n 5.4 del kernel de Linux permitir\xE1 la ejecuci\xF3n\
  \ de m\xE1s juegos y aplicaciones de 64 bits v\xEDa Wine/Proton"
---
La lista de parches en el kernel de Linux para facilitar la vida de los jugadores aumenta

Vía Linux Adictos nos [enteramos](http://tinyurl.com/y3pohqlh) de que ha sido aceptado un parche de CodeWeavers para la versión 5.4 de nuestro kernel favorito.


En los procesadores más nuevos se encuentra presente una funcionalidad de seguridad conocida como UMIP (**U**ser-**M**ode **I**nstruction **P**revention por sus siglas en inglés) que impide que las aplicaciones que se ejecutan en el espacio de usuario tengan acceso a determinadas instrucciones. Si bien esto no es un problema en la mayoría de los casos, en casos como el de Wine, hay casos en los que estas instrucciones son necesarias, por lo que los juegos no pueden ejecutarse correctamente.


Actualmente el kernel de Linux ya ofrece emulación para estas instrucciones para aplicaciones de 32 bits, y ahora, gracias al parche que ha sido aceptado, dichas instrucciones también tendrán soporte cuando sean requeridas por aplicaciones de 64 bits, como por ejemplo:


* Metro Exodus
* Shadow of the Tomb Raider
* Wolfenstein: Youngblood
* Soulcalibur VI
* Grand Theft Auto
* Devil May Cry 5
* Team Sonic Racing


De todos modos, si tu microprocesador es uno de los afectados por esta funcionalidad de seguridad, siempre puedes deshabilitar UMIP pasando el parámetro *clearcpuid=514* a las opciones del kernel.


Y tu CPU ¿está "afectada" por esta funcionalidad? Cuéntanos qué otros juegos o aplicaciones no puedes ejecutar en los comentarios o en nuestros canales de [Telegram](https://t.me/jugandoenlinux) y [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org)

