---
author: P_Vader
category: Tutorial
date: 2022-02-13 09:36:15
excerpt: "<p>Magn\xEDfico software libre para el control del movimiento de la c\xE1\
  mara con la cabeza.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Opentrack/opentrack_xplane.webp
joomla_id: 1426
joomla_url: guia-de-instalacion-y-uso-de-opentrack
layout: post
tags:
- simulador
- simulacion
- simracing
- opentrack
title: "Gu\xEDa de instalaci\xF3n y uso de Opentrack"
---
Magnífico software libre para el control del movimiento de la cámara con la cabeza.


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/90DV15XifmY" title="YouTube video player" width="780"></iframe></div>


**Nivel de la guía**: Intermedio. Necesario saber ejecutar comandos en la consola.


 


**[Opentrack](https://github.com/opentrack/opentrack) es un software libre que ofrece diferentes métodos para controlar la cámara de tu video juego con la cabeza**. Por desgracia en el mundo Linux son pocos los títulos nativos que hacen uso del seguimiento de cabeza. La parte buena es que con Proton es posible jugar a muchos juegos y esta aplicación está preparada para ello.


Para la instalación, primero de todo vamos a **descargar el** **archivo comprimido con el "Source code**" desde su web en <https://github.com/opentrack/opentrack/releases> . La versión probada para esta guía fue la 2022.1.1.


También vamos a descargar y descomprimir el script que hemos adaptado desde JugandoEnLinux para facilitar la tarea de instalación: <https://gitlab.com/jugandoenlinux/opentrack-script/-/releases/v1.0>


El script **z_Build.sh lo vamos a copiar dentro de la carpeta donde descomprimimos Opentrack**, es probable ademas que necesites darle permisos de ejecutable con el comando `**chmod +x z_Build.sh**` o desde las propiedades del archivo en tu aplicación de escritorio.


A continuación vamos a instalar las dependencias necesarias para compilar Opentrack y sus módulos.


Para **Debian o derivadas** puedes ejecutar este comando en una consola:


**`sudo apt install build-essential cmake ninja-build qttools5-dev qtbase5-private-dev libprocps-dev libopencv-dev libevdev-dev gcc-multilib g++-multilib wine wine32 wine32-tools wine32-tools libqt5serialport5-dev qv4l2`**


Para distribuciones **Arch/Manjaro** necesitarás ejecutar en una consola:


`**sudo pacman -S base-devel cmake qt5-base opencv wine ninja qt5-tools qt5-serialport**`


Hay disponible un [AUR](https://aur.archlinux.org/packages/opentrack) pero no es tan completo en módulos como nuestro script.


Antes de proceder a ejecutar el script z_Build.sh dentro del directorio Opebntrack, vamos a aclarar que el script se puede ejecutar con la opción de añadirle hasta 3 parámetros para ampliar sus funcionalidades:


* **aruco**: Sistema de seguimiento con la cámara usando un código impreso en un papel.
* **onnx**: Hace uso de la IA neuronal para detectar nuestra cara. Bastante preciso con una buena cámara.
* **xplane**: Plugin para usar con X-Plane 11.


El esquema del comando a ejecutar seria:**`z_Build.sh [parametro1] [parametro2] [parametro 3]`**


Por ejemplo: `**./z_Build.sh onnx xplane aruco**`


Una vez finalizado y si todo ha ido bien aparecerá en tu menú de aplicaciones en ***Juegos > Opentrack***


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/rxk_Xp91TFk" title="YouTube video player" width="780"></iframe></div>




---


 


Ahora pasamos a **cómo usar Opentrack**. En esta parte vamos a describir por encimas las opciones y características mas comunes de la aplicación.


![opentrack principal](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Opentrack/opentrack_principal.webp)


1. **Entrada**: Los diferentes módulos que detectan el movimiento de la cabeza. Hay opciones para diferentes sistemas usando la webcam, movil, arduino, etc.
2. **Salida**: La comunicación con el video juego. En Linux tenemos 3 modos, flighgear para el propio juego, emular un joystick o wine. Para xplane nativo se usa wine.
3. **Filtros**: Ayudan a suavizar y controlar ciertos errores o movimientos bruscos.
4. **Perfiles**: Muy recomendable crear un perfil para cada juego. Las configuraciones pueden variar mucho de un juego a otro.
5. **Opciones**.
6. **Mapas de curvas**.


Entre las **entradas** que mejor hemos probado están:


* **FreePIE** se puede usar con una aplicación para android en el móvil como detector de movimiento. El movil debe ir agarrado a la cabeza de algún modo y conectado en la misma red que el ordenador. Tiene un rendmiento excelente si la aplicación del móvil detecta bien los sensores. La aplicación de andriod se encuentra en la carpeta: `~/.opentrack-install/share/doc/opentrack/contrib/freepie-udp/FreePIE_IMU_Android9.apk`
* **Hatire Arduino**: Junto con el móvil es otra opción de gran precisión. Usa arduino y sensores. Mira la [completísima guía de Bernat](index.php/foro/dispositivos-de-control/209-hatire-head-tracking-con-arduino#608) si quieres usarlo.
* **Neuralnet tracker**: La IA esta entrenada para seguir los movimiento de una cabeza. Es bastante precisa, aunque ante movimientos extraños puede descontrolarse.
* **Aruco**: Puede imprimir un codigo de marca: <https://github.com/opentrack/opentrack/blob/unstable/contrib/aruco/test3.png> y fijarlo en la cabeza. Mi experiencia no ha sido satisfactoria aunque no tengo una buena webcam.


Las **salidas** son mas limitadas en Linux disponiendo unicamente de:


* **Flightgear**: Instalas el plugin en el juego y lo activas siguiendo estas instrucciones, en mi caso no he podido hacerlo funcionar fluidamente: <https://wiki.flightgear.org/Opentrack#Configuration>
* **Joysitck**: Emulas el movimiento de un mando al mover la cabeza. Útil cuando el juego no es compatible con ningún sistema de seguimiento y permite mover la cámara con algún control.
* **WINE**: Este puede funcionar usando Proton desde steam o wine instalado en tu sistema operativo. Recuerda que tienes que indicarlo el Steam App ID de la aplicación que va dirigida en el caso de Proton. Por ejemplo en Elite Dangerous sería [359320](https://steamdb.info/app/359320/).   
Hay otra [versión modificada de Opentrack](https://github.com/JT8D-17/opentrack/) que mejora algunas cosas en el menú de WINE y se puede usar para aplicaciones fuera de Steam como Lutris.   
Mi experiencia ha sido que para steam funciona mejor opentrack original y para aplicaciones fuera de steam mejor la versión modificada de opentrack/wine. Puedes compilar en otro directorio una segunda versión y usarlas una u otra, pero eso no lo incluye el script, tendrías que cambiar el directorio final de instalación para que no se solapen.


Recuerda que para X-Plane 11 se usa la opción de WINE de tu sistema. Además tienes que copiar el archivo **`opentrack.xpl`** desde **`~/opentrack-install/libexec/opentrack`** al directorio de **`X-Plane 11/Resources/plugins`**. Cuando arranques un vuelo en X-Plane, en el menu de plugin debe aparecerte activado.


 


Las **Opciones mas destacadas** de la aplicación son la posibilidad de **(1) asignar atajos de teclado** a algunas acciones. Muy utiles el centrado de la vista y el poder pararlo o arrancarlo:


![opentrack opeciones teclado](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Opentrack/opentrack_opeciones_teclado.webp)


 Otra de las opciones importantes son **(1) la posibilidad de deshabilitar algún eje** y **(2) de invertirlo**. Según que juegos tendrás que buscar las mejores opciones para cada uno.


![opentrack opciones ejes](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Opentrack/opentrack_opciones_ejes.webp)


 


Las **curvas son una parte muy importante para la configuración de los movimientos**. En el eje **vertical (1) se ajusta la velocidad de movimiento del juego**, mientras que en el **eje horizontal (2) es el movimiento real de tu cabeza**. Normalmente se ajusta para que un movimiento de 20-40 grados de tu cabeza se corresponda con 90 grados en el juego. Puedes jugar con los puntos de la gráfica hasta que lo ajustes a tu gusto:


![opentrack curvas](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Opentrack/opentrack_curvas.webp)


 Espero que disfrutéis de esta extraordinaria aplicación. ¿Te animas a probarla? Cuéntanoslo en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).


 

