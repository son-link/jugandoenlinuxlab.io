---
author: leillo1975
category: "Acci\xF3n"
date: 2021-04-14 07:38:34
excerpt: "<p><span class=\"css-901oao css-16my406 r-1qd0xha r-ad9z0x r-bcqeeo r-qvutc0\"\
  >Finalmente <span class=\"css-901oao css-16my406 r-poiln3 r-bcqeeo r-qvutc0\">@4AGames</span>\
  \ ha publicado su port de @MetroVideoGame. <br /></span></p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/metro_exodus-4776224.webp
joomla_id: 1300
joomla_url: el-magnifico-metro-exodus-al-fin-en-linux
layout: post
title: "El magn\xEDfico Metro Exodus, al fin en Linux "
---
Finalmente @4AGames ha publicado su port de @MetroVideoGame.   



La verdad es que los jugadores que tenemos al pingüino por bandera **ya echábamos en falta algún nuevo título de calado en nuestra biblioteca de juegos**, y muy especialmente de **acción en primera persona**. El caso es que el último capítulo de una de las sagas más exitosas de los videojuegos es una pieza más que apetecible para cualquier aficionado a los juegos en general. y más especialemente a los fans del género. La verdad es que costaba entender que 4A Games después de lanzar Metro 2033 (Redux) y Metro Last Light en nuestro sistema, no portase también este "Exodus", pero viendo el cariz que está tomando la industria de los videojuegos con respecto nuestra plataforma, hemos de ser sinceros, no las teníamos todas con nosotros, especialmente después de ver como la "Épica" tienda se hacía con la exclusividad del juego en su lanzamiento, robándoselo así a Steam durante un año.


Finalmente, cumplidos los 12 meses que estipulaba el deleznable contrato de exclusividad de la Epic Store, el juego llegó a Steam, donde multitud de fans de la saga esperaron pacientemente para adquirirlo en condiciones... y por supuesto llegó la pregunta de rigor con la respuesta más esperada. ¡[Se estaba trabajando en el port!](https://twitter.com/MetroVideoGame/status/1228697765007515648?ref_src=twsrc%5Etfw%7Ctwcamp%5Etweetembed%7Ctwterm%5E1228697765007515648%7Ctwgr%5E%7Ctwcon%5Es1_&ref_url=https%3A%2F%2Fjugandoenlinux.com%2Findex.php%2Fhomepage%2Faccion%2F1171-metro-exodus-podria-llegar-a-linux-pronto-a1). Los fans Linuxeros de la saga podrían completar la colección y disfrutar de uno de los juegos más laureados de los últimos tiempos en el género de los shooters. Ciertamente era posible hacerlo gracias a Proton/Wine, pero seamos francos, no es lo mismo. Además es un **acto de respeto de la compañía hacia nosotros**.


4A Games finalmente ha publicado hace escasos minutos el tweet anunciando la llegada del juego:


> 
> [#MetroExodus](https://twitter.com/hashtag/MetroExodus?src=hash&ref_src=twsrc%5Etfw) is out now on Mac and Linux!   
>   
> Get the Mac version on the [@AppStore](https://twitter.com/AppStore?ref_src=twsrc%5Etfw), [@Steam](https://twitter.com/Steam?ref_src=twsrc%5Etfw) and [@EpicGames](https://twitter.com/EpicGames?ref_src=twsrc%5Etfw) and the Linux version on Steam. Grab your Tikhar and get out there, Ranger!  
>   
> Check out the full blog: <https://t.co/oxVswIM2y8> [pic.twitter.com/bjOfjIsvs3](https://t.co/bjOfjIsvs3)
> 
> 
> — Metro Exodus (@MetroVideoGame) [April 14, 2021](https://twitter.com/MetroVideoGame/status/1382348309914910720?ref_src=twsrc%5Etfw)


Lo más probable es que muchos ya conozcais el universo que rodea la saga Metro, en que contexto se desarrolla la historia, como son los aspectos relevantes relativos a su jugabilidad; o como son estos juegos a nivel técnico, pero por si lo teníais claro, según la descripción oficial del juego, en Metro Exodus vamos a encontrar lo siguiente:


*Es el año 2036.*  
  
*Un cuarto de siglo después de que la guerra nuclear devastara la tierra, unos pocos miles de supervivientes se aferran a la vida bajo las ruinas de Moscú, en los túneles del metro.* *Han sufrido intoxicaciones, luchado contra bestias mutantes y horrores paranormales y pasado el terror de la guerra civil.* *Ahora, en el papel de Artyom, debes escapar del metro y liderar una banda de comandos espartanos en un viaje increíble por todo el continente en la Rusia postapocalíptica para empezar una vida nueva en el este.* *Metro Exodus es un juego de disparos en primera persona centrado en la narrativa y desarrollado por 4A Games que combina el combate letal y el sigilo con la exploración, el terror y la supervivencia en uno de los mundos más inmersivos de la historia de los juegos.* *Explora las estepas rusas en niveles no lineales enormes y sigue una narrativa apasionante que abarca un año entero desde primavera, verano y otoño hasta el pleno invierno nuclear.*  
  
*Metro Exodus está **inspirado en las novelas de Dmitry Glukhovsky** y continúa la historia de Artyom en la mayor aventura de Metro hasta ahora.*


***Características***
---------------------


* *Embárcate en un viaje increíble: sube al Aurora, un tren de vapor modificado, y únete a un puñado de supervivientes para empezar una vida nueva en el este.*
	+ *Supervivencia en un mundo abierto: una historia apasionante une las mecánicas clásicas de Metro con nuevos niveles no lineales enormes.*
	+ *Un mundo precioso y hostil: descubre las estepas de la Rusia postapocalíptica, más viva que nunca con ciclos solares asombrosos y un clima dinámico.*
	+ *Combate letal y sigilo: saquea y fabrica para personalizar tu arsenal de armas artesanales y enfréntate a enemigos humanos y mutantes en batallas estratégicas emocionantes.*
	+ *Tus decisiones deciden el destino de tus camaradas: no todos sobrevivirán al viaje y cada decisión que tomes tiene consecuencias en una narrativa apasionante a la que querrás jugar una y otra vez.*
	+ *Máxima atmósfera e inmersión: una vela que titila en la oscuridad, un grito ahogado al congelarse la máscara de gas, el aullido de un mutante en la noche... Metro te sumergirá en este mundo y te aterrorizará.*


Dejándonos de cháchara y de la "lección" de historia del ya afortunadamente pasado, **el juego es presente en nuestro sistema**, y se puede descargar en la tienda de Valve. A nivel técnico el juego podrá aprovechar las bondades el [Ray Tracing](https://twitter.com/MetroVideoGame/status/1375081207814361088) en Linux, **siempre que nuestra tarjeta gráfica lo permita**, por lo que este es uno de los primeros juegos en nuestro sistema en permitir esta tecnología. En cuanto a los requisitos mínimos estos fueron publicados hace algúnos días:


  
![MetroRequisitos](https://cdn.cloudflare.steamstatic.com/steamcommunity/public/images/clans/29145483/aaf87a1cafa271d2a29d72068558cac9842247c6.png)


Si quereis comprar Metro Exodus podeis hacerlo en la [Humble Store](https://www.humblebundle.com/store/metro-exodus?partner=jugandoenlinux) (**Patrocinado**), o en [Steam](https://store.steampowered.com/app/412020/Metro_Exodus/). Os dejamos con el trailer oficial del juego:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/l3dfIglDzAs" style="display: block; margin-left: auto; margin-right: auto;" title="YouTube video player" width="780"></iframe></div>


¿Has jugado ya a algún juego de la saga Metro? ¿Qué te parece la llegada a nuestro sistema de este bombazo? Cuéntanoslo en los comentarios o en nuestros grupos de JugandoEnLinux en [Telegram](https://twitter.com/JugandoenLinux) o  [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

