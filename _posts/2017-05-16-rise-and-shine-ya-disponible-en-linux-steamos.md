---
author: Pato
category: "Acci\xF3n"
date: 2017-05-16 16:07:21
excerpt: "<p>El juego de Super Mega Team nos llega por f\xEDn con su genial propuesta\
  \ de acci\xF3n y aventuras</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/9c2fe6cb8c357cf6d57c8926869c1003.webp
joomla_id: 321
joomla_url: rise-and-shine-ya-disponible-en-linux-steamos
layout: post
tags:
- accion
- indie
- plataformas
- aventura
title: '''Rise and Shine'' ya disponible en Linux/SteamOS'
---
El juego de Super Mega Team nos llega por fín con su genial propuesta de acción y aventuras

![RiseandShineheader](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/RiseandShine/RiseandShineheader.webp)


Desde hace mucho que lo [veníamos anunciando](https://www.jugandoenlinux.com/index.php/homepage/generos/arcade/item/409-rise-shine-para-linux-a-punto-de-ser-lanzado), ¡y hoy por fin lo tenemos ya disponible en nuestro sistema favorito!


Ya hemos hablado largo y tendido de 'Rise and Shine' [[web oficial](http://supermegateam.com/our-games/rise-shine/)] en Jugando en Linux, y es que es de esos juegos que no dejan indiferente. Su planteamiento tipo "run and gun", su historia y su mezcla de puzzles con una dificultad desafiante hace que este título sea por méritos propios muy apetecible. Además es obra de un estudio español, por lo que la traducción está garantizada. Echa un vistazo:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/hXYQ4iunOSo" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


####  **Sinopsis de Steam:**



> 
> Dispara rápido, piensa más rápido  
> Rise & Shine es un auténtico juego de disparos "pero con cabeza" que combina elementos de los shooters de las recreativas, los infiernos de balas y los plataformas con rompecabezas, creando así una combinación de estrategia y vísceras alucinante. Elige entre los diversos complementos de Shine para resolver rompecabezas que redefinen los posibles usos que se le pueden dar a una bala. Guía los proyectiles por laberintos intrincados, electrifica equipamiento dañado para descubrir rutas nuevas y haz malabares con frutas de 8 bits porque... ¿por qué no?
> 
> 
> Prepárate para reaparecer  
> No permitas que los diseños de los adorables personajes te engañen; salvar a Gamearth no va a ser fácil. Tendrás que lanzar ráfagas de misiles y esquivar trampas mientras luchas para evitar la destrucción del planeta. Vas a tener que poner toda la carne en el asador para sobrevivir mientras te enfrentas a flotas de robots letales, jefes de gran tamaño, ruedas mortales gigantescas y (por supuesto) zombis con ganas de mascar cerebro. Chaval, no vas a salir de esta de una sola pieza.
> 
> 
> Detalles de primera clase  
> Participa en un mundo donde todos los niveles son auténticas obras de arte. Cada uno de ellos se ha creado con ilustraciones diseñadas a mano con múltiples capas de desplazamiento lateral, por lo que cada paso que des será completamente distinto. ¡Nada de imágenes repetidas! Todos los detalles se han cuidado al máximo para que cada plano fluya de forma impecable; con esta calidad desearás que el juego no se acabe nunca.
> 
> 
> 


En cuanto a los requisitos, son estos:


SO: Ubuntu 16.04 LTS (64bit)  
Procesador: 2.4 GHz Dual core  
Memoria: 2 GB de RAM  
Gráficos:Tarjeta con al menos 512MB VRAM  
Almacenamiento: 4 GB de espacio disponible  
Tarjeta de sonido: Integrated audio interface  
Notas adicionales: Gamepad compatible con XBOX360 


Si aún no tienes 'Rise and Shine'... ¿a qué esperas?... Puedes encontrarlo en su página de Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/347290/" width="646"></iframe></div>


 


¿Qué te parece 'Rise and Shine'? ¿Piensas jugarlo?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 

