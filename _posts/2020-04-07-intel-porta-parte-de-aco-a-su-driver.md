---
author: Serjor
category: Software
date: 2020-04-07 11:21:13
excerpt: "<p>Y consigue unas mejoras interesantes</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Drivers/mesa.webp
joomla_id: 1202
joomla_url: intel-porta-parte-de-aco-a-su-driver
layout: post
tags:
- amd
- intel
- open-source
- codigo-abierto
- aco
title: Intel porta parte de ACO a su driver
---
Y consigue unas mejoras interesantes


Hay quién aún no lo entiende, y sigue apostando por el software privativo pensando que da mayor ventaja competitiva, pero las bondades del software libre son siempre sorprendentes. O sino que se lo digan a Intel, que han importado parte de ACO, el compilador de shaders que está desarrollando para AMD.


Como leemos en [PC GAMER](https://www.pcgamer.com/intel-ports-amd-compiler-code-for-a-10-performance-boost-in-linux-gaming/), en Intel han aprovechado parte del desarrollo de este compilador de shaders para llevarlo al hardware de Intel, para obtener mejoras de hasta el 10%, un valor nada despreciable.


Si todo va bien, este código entrará dentro del driver de Intel en próximas versiones, y gracias a la filosofía de código abierto, todos (bueno, casi todos, verdad NVIDIA...) podremos beneficiarnos de las diferentes mejoras que se van consiguiendo en uno y otro lado.


Y tú, ¿qué juegos piensas que podrás disfrutar a partir de ahora con estas mejoras? Cuéntanoslo en los comentarios o en nuestros canales de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org) y [Telegram](https://t.me/jugandoenlinux)

