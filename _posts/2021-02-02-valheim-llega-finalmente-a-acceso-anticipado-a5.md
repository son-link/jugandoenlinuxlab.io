---
author: leillo1975
category: "Exploraci\xF3n"
date: 2021-02-02 09:05:38
excerpt: "<p><span class=\"css-901oao css-16my406 r-poiln3 r-bcqeeo r-qvutc0\"><span\
  \ class=\"css-901oao css-16my406 r-poiln3 r-bcqeeo r-qvutc0\">@-Valheimgame recibe\
  \ una nueva actualizaci\xF3n. </span><br /></span></p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/valheim/Valheim900.webp
joomla_id: 1272
joomla_url: valheim-llega-finalmente-a-acceso-anticipado-a5
layout: post
tags:
- acceso-anticipado
- exploracion
- supervivencia
- unity3d
- valheim
- iron-gate-studio
title: Valheim llega finalmente a Acceso Anticipado (ACTUALIZADO 6)
---
@Valheimgame recibe una nueva actualización.   



**ACTUALIZACIÓN 9-6-21**: En esta ocasión Iron Gate nos trae la nueva versión [0.154.1](https://store.steampowered.com/news/app/892970/view/3059613869755598898). Además de arreglar algunos fallos y problemas menores, encontraron que algunos de los eventos aleatorios nunca se activaban como se esperaba. Así que dad la bienvenida a dos nuevos eventos: "Estás siendo cazado..." y "La horda está atacando". Seguro que os alegrarán el día. (¡JA!)


* Se ha corregido un problema por el que las capas a veces formaban extrañas protuberancias en la espalda después de saltar.  
* Se ha corregido el problema de cerrar la guía de construcción con la selección ESC.  
* Arreglado el evento del ejército de Fuling  
* Se ha corregido el evento de caza de lobos.  
* Ahora se puede bloquear el golpe de tierra del trol.  
* Arreglo del sonido de reparación y construcción de barcos largos.  
* Arreglo de la durabilidad de Wolfcape.  
* Arreglo del temporizador de reaparición de recursos (las bayas y el pedernal ahora deberían reaparecer correctamente).  
* Las serpientes ya no huyen cuando tienen poca salud.  
* Se ha corregido el problema de que los contenedores de las naves no se cierran correctamente en algunas situaciones.  
* El terreno elevado es más suave y menos puntiagudo.  
* Se han corregido los problemas de recorte de las partículas de agua y de la superficie del agua dentro de los barcos.  
* Mejora de la respuesta del fermentador cuando está expuesto.  
* Se han ajustado algunos recursos de sonido para que utilicen menos RAM.  
* Las rocas cubiertas ya no parecen mojadas cuando llueve.


Por último, **no habrá caballos en Valheim,** [aunque en otra noticia así lo pareciese.](https://store.steampowered.com/news/app/892970/view/3035968700877261597)


---


****ACTUALIZACIÓN 12-5-21:**** Llegamos a vosotros de nuevo con otra actualización de este fantástico y popular título que tantos buenos ratos nos está haciendo pasar en nuestra comunidad de JugandoEnLinux.com. El número de la versión es la [0.153.2](https://steamcommunity.com/ogg/892970/announcements/detail/3071996229880181250) entre otras cosas tiene importantes mejoras en el aspecto de algunas criaturas como los Trolls o algunos jefes finales. También tiene los siguientes cambios:


* Arreglado el problema de la fundición cuando ha pasado mucho tiempo desde la visita (10000 días)  
* Se ha añadido un aviso de guardado previo al mundo de 30 segundos.  
* Arreglo de la física de las escaleras de piedra  
* Arreglo del enfoque del gamepad en la pantalla de inventario  
* Revisión visual de las criaturas grandes (troll, jefe 2 y jefe 3)  
* Arreglo del material de los ragdolls de los trolls  
* Revisión de la mecánica del arpón  
* Motor actualizado (algunas mejoras de estabilidad)  
* Arreglos visuales del arquero draugr (los draugrs de 1 y 2 estrellas a veces tenían el material equivocado)  
* Arreglo de problemas cuando se destruye la estación de artesanía mientras está en uso  
* Se ha ajustado la creación de grupos de criaturas para evitar que se superen los límites totales (en algunos casos).  
* Actualización de los créditos  
* Actualización de la localización


![Troll](https://cdn.cloudflare.steamstatic.com/steamcommunity/public/images/clans/33695701/989bb19baa045cf0abe9f8910b3b025762597ce1.png)


Ya nos contareis si ahora los Trolls dan incluso más miedo que antes. Saludos!

---


****ACTUALIZACIÓN 19-4-21:**** Casi un mes después de nuestra última actualización de este artículo, la gente de [Iron Gate Studio](https://irongatestudio.se/) vuelve a la carga con un nuevo parche, el 0.150.3, que tiene como elemento más distintivo el **nuevo sistema de modificación del terreno**, así como diversos arreglos relacionados. El [listado completo](https://discord.com/channels/391142601740517377/805712079704424498/833685850008715275) de estos cambios lo hemos podido encontrar en sus salas de discord, y por supuesto ya estamos deseando probarlo en [nuestro servidor dedicado]({{ "/posts/servidor-dedicado-de-valheim-oficial-de-jugandoenlinux-com" | absolute_url }}) del juego. La actualización tiene las siguientes características:


[0.150.3](https://steamcommunity.com/games/892970/announcements/detail/5411613798265437193)


¡Esto ha sido muy largo! Este parche ha tardado mucho en desarrollarse debido al nuevo sistema de modificación del terreno. El nuevo sistema de modificación del terreno está hecho para reducir el número de instancias de red y hacer que la carga sea más rápida y suave. Técnicamente es un cambio bastante grande, pero esperamos que no notes mucha diferencia, excepto algunos cambios menores en el comportamiento de la azada y el pico y, por supuesto, una carga mucho más suave de las áreas con muchas modificaciones del terreno. Todas las modificaciones del terreno después de este parche (utilizando la azada, el pico o el cultivador) utilizarán automáticamente el nuevo sistema de modificación. Para las áreas existentes en las que se han realizado grandes modificaciones del terreno antes de este parche, hemos añadido un comando de consola especial "optterrain" que básicamente convierte todas las antiguas modificaciones del terreno en el área cercana al nuevo sistema. Para utilizar el comando de consola "optterrain", primero hay que activar la consola del juego añadiendo "-console" como argumento de lanzamiento del juego.


* Arreglo de la ubicación del generador de draugr del pantano para evitar que los draugrs aparezcan dentro de las piedras
* Arreglo de los efectos de las mascotas Lox
* Las antorchas en las localizaciones ya no deberían soportar las construcciones
* Arreglo del tamaño de la piedra de la ubicación del dolmen
* Nuevo sistema de modificación del terreno
* Cambio en la prioridad de las modificaciones del terreno (las modificaciones del terreno en una zona deben cargarse antes que los edificios, sólo se aplica al nuevo sistema de modificación del terreno)
* Ajustes en la carga del mundo (para arreglar los problemas con los barcos y los edificios que se dañan durante la carga)
* Detener la descarga de la lista de servidores al salir del menú de inicio (para disminuir el uso del ancho de banda de la red)
* Se ha reducido la cantidad de piedra necesaria para levantar el terreno con la azada


Los numerosos usuarios de este juego de la comunidad de JugandoEnLinux.com podeis decirnos que os parecen estos cambios en los comentarios o en nuestros grupos de JugandoEnLinux en [Telegram](https://twitter.com/JugandoenLinux) o  [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).


---


**ACTUALIZACIÓN 23-3-21:** Cuando ya no sabemos cuantos millones de jugadores pueden haber comprado este Early Access (lo último que sabemos es que son [6 millones](https://store.steampowered.com/news/app/892970/view/3047221359533847024)), nos llega la noticia de la publicación del [parche 0.148.6](https://store.steampowered.com/news/app/892970/view/3047221359547604552), que incluye tanto cambios de calado, especialmente en el balance, así como cambios menores, y un buen número de correcciones de errores. La lista es enorme y podeis verla justo aquí debajo:  



* Las hogueras reciben daño cuando infligen daño
* El cofre reforzado aumenta su espacio 6×4
* Los drops de los jefes ahora flotan en el agua
* Se modificó la entrada de la cripta hundida (para evitar que las lápidas se atasquen)
* Se corrigió la rotación del Escudo de torre de madera en los soportes de artículos
* Aumento de la tasa de aparición de trofeos de Deathsquito y Drake
* Corregidos los puntos de vida de las criaturas de 1 y 2 estrellas
* Los lobos que aparecen de noche deberían ser más fáciles de domesticar ahora (deberían dejar de intentar huir y desaparecer después de comenzar a domesticarlos)
* El arpón ya no funciona con los jefes
* La consola del juego está deshabilitada de forma predeterminada (agregue el argumento de inicio «-console» para habilitar)*
* El comando de la consola para habilitar los comandos de desarrollador/depuración se ha cambiado de «imacheater» a «devcommands» de y se ha agregado un mensaje de advertencia.
* Sistema mejorado de reacción de proyectiles enemigos.
* Ajustes del hacha de batalla (golpea a múltiples enemigos más fácilmente)
* La fuerza de retroceso del jugador se ve afectada por los modificadores de velocidad del equipo (es decir, el equipo pesado reducirá el retroceso de los enemigos)
* Ajustes de la torre de piedra de Blackforest
* Correcciones del sistema de guardia (ya no se puede colocar una nueva sala donde se superpone una sala enemiga)
* Corregido cálculo de comodidad
* Se corrigió el mensaje de error «No se pudo conectar»
* Corrección de la acumulación de trofeos de serpiente
* Se corrigió la ubicación de generación del jefe Moder que faltaba en algunos mundos (NOTA: para mundos existentes, el comando «genloc» debe ejecutarse manualmente en un juego local con comandos de desarrollo habilitados para generar nuevas ubicaciones, esto solo es necesario si tu mundo específico tiene este problema, esto no es muy común)
* Corregida la colisión de objetos con el Megingjord
* Se agregó un ligero retraso en el uso de martillo, azada y cultivador
* Eliminación de la repetición automática añadida al martillo
* Mejor manejo del ancho de banda de la red (debería funcionar mejor en conexiones de ancho de banda bajo y también usar una velocidad de datos más alta si es posible)
* Correcciones de ubicación de dolmen (evitar que la piedra superior se caiga sin motivo)
* Se corrigió que la eliminación de objetos del soporte no siempre sincronizara las estadísticas del objeto
* El botón de actualización de la lista de servidores se puede presionar antes de que se haya descargado toda la lista
* Mejor detección de mala conexión
* Se solucionó el problema que causaba que el servidor enviara más datos cuanto más tiempo estaba conectado un cliente
* Actualizaciones de localización


Seguiremos con la misma atención de siempre las noticias que genere este fenómeno que es Valheim.

---


**ACTUALIZACIÓN 24-2-21:** "Imparable" es la palabra que mejor puede definir el éxito sin precedentes de este juego, que **recordemos sigue en Early Access**. Como veis debajo, hace 2 semanas el juego conseguía llegar al millón de unidades, pero lejos de estancarse, tan solo unos días, llegaba a los [dos millones](https://steamcommunity.com/games/892970/announcements/detail/4627981752752748716), y poco después a los [3 millones](https://steamcommunity.com/games/892970/announcements/detail/3059603718053006419). Supongo que ya adivinais cual es la noticia que reseñamos hoy, si amigos, son [cuatro millones de jugadores](https://steamcommunity.com/games/892970/announcements/detail/3058478454611894476) .


No solo hablamos de copias vendidas, sino que este fin de semana han alcanzado los **500.000 jugadores simultaneos**, colocándose **entre los 5 títulos más jugados de la historia en Steam**. Además las **críticas "Extremadamente Positivas" son ya más de 81.000**. Como veis el juego no para de batir records, cuando solo lleva **20 días en Acceso Anticipado**.... y hay que decirlo, ha sido también un éxito en nuestra comunidad, donde bastantes de nuestros integrantes se han sumado a las hordas vikingas (recordad nuestro [servidor dedicado]({{ "/posts/servidor-dedicado-de-valheim-oficial-de-jugandoenlinux-com" | absolute_url }})).


La verdad es que en Iron Gate Studio pueden darle las gracias a Odín por semejante éxito. Os dejamos con nuestro último Stream de Valheim:


  
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/mH9g9o_8ofo" style="display: block; margin-left: auto; margin-right: auto;" width="780"></iframe></div>


---


**ACTUALIZACIÓN 10-2-21**: Una semana después de su lanzamiento el juego de Iron Gate Studio se ha convertido en el **juego nº1 de ventas en Steam**, superando el **millón de copias vendidas**, obteniendo así un éxito sin precedentes teniendo en cuenta que se trata de un juego en Acceso Anticipado creado por un equipo de tan solo 5 personas. El título de supervivencia Vikinga además acumula en este momento **más de 11000 reseñas Extremadamente Positivas**, además de multitud de alagos entre la prensa especializada.


Ayer mismo, martes 9 de febrero, se convirtió en el **cuarto juego más jugado en Steam, con un pico de más de 160.000 usuarios concurrentes**, superando a Grand Theft Auto V, Apex Legends y Rocket League, según la plataforma. También ha demostrado ser popular entre algunos de los creadores de contenido más importantes del mundo, con **más de 127.000 espectadores máximos en Twitch** la noche del 8 de febrero.


![valheim](https://mcusercontent.com/5db105e138032e564b4758e4b/images/116a8641-397b-4eed-90a4-a4a204293e05.jpg)


El director del pequeño estudio sueco, **Richard Svensson**, comentaba a proposito de este rotundo éxito: *"Valheim es un juego cooperativo sobre la exploración de un mundo enorme y bastante colorido con muy pocas fronteras o límites, que se lanzó durante una pandemia global, en un momento en el que muchas personas están encerrados dentro de sus casas y sin poder ver a sus amigos y familiares. Con suerte, ser aporreados por un troll los hará sentir mejor".* 


***Sebastian Badylak,** Productor Ejecutivo de* [*Coffee Stain Publishing*](https://www.coffeestainstudios.com/), la editora del juego también comenta esto:*"Cuando firmamos con Valheim y Iron Gate, que **en ese momento era solo Richard,** sabíamos que el juego era increíble, pero ha seguido superando nuestras expectativas en todo momento. Cuando buscamos juegos, los desarrolladores en solitario y los equipos pequeños se destacan porque tienen una visión enfocada y un talento obvio, pero con mucho espacio para que los apoyemos. Valheim es un ejemplo perfecto de lo que un equipo ágil y con mucho talento puede lograr cuando se les da el espacio y las herramientas para hacer lo que quieren. Este es sólo el comienzo."*


En JugandoEnLinux.com estamos felices de poder daros estas noticias de este magnífico juego, y por supuesto **os recomendamos este título nativo sin ningún tipo de reserva:** 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/892970/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


---


**NOTICIA ORIGINAL (2-2-21):**


Bastante tiempo ha pasado desde [la última vez que os hablamos](index.php/component/k2/1-blog-principal/exploracion/944-valheim-un-survival-horror-ambientado-en-la-mitologia-nordica) de este juego de [Iron Gate Studio](https://irongatestudio.se/), y tenemos que reconocer que le habíamos perdido la pista injustamente. Con la cantidad de juegos en desarrollo existentes y el ajetreo diario de nuestras vidas, es imposible para nosotros, que no vivimos de esto, darle su merecida cobertura. Menos mal que gracias a un correo de [Jesús Fabre](https://twitter.com/jesusfabre?lang=es) hemos podido retomar el seguimiento del desarrollo de este juego.


Para quien no lo recuerde, es un **juego de supervivencia y exploración** creado con el **motor Unity3D**, en el que nos pondremos en la piel de un Vikingo al que "sueltan" en [Valheim](https://www.valheimgame.com/), el décimo mundo nórdico, donde seremos el guardían del purgatorio primordial y tendremos que enfrentarnos a las criaturas del caos y los antiguos enemigos de los dioses, matando a los rivales de Odin para poder entrar finalmente en el Valhala.


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/BSrJRrlVQII" width="780"></iframe></div>


Con esta premisa tan apetecible llegaremos a un **inmenso mapa, generado de forma procedural**, en el que podremos jugar tanto solos, como buscar compañeros de viaje que nos ayuden a través del **multijugador cooperativo, que permite hasta 10 personas conectadas**. Llegarás a esta tierra en las garras de un cuervo (hugin) gigante, que te dejará en el centro de una especie circulo de megalitos rituales. A partir de ese momento tendrás que apañártelas desarmado en un mundo en un principio no muy hostil. Poco a poco irás recolectando recursos de la naturaleza y transformándolos con tus propias manos para construir (o reparar) un lugar donde pasar la primera noche.  A medida que pasen los días, irás descubriendo más y más terreno, y de esa forma también se irán ampliando tus posibilidades, pudiendo acceder a nuevos materiales que te permitirán construir y elaborar armas, ropa, herramientas, muebles e incluso barcos que te permitirán explorar zonas remotas del mapa. Pero esto también tendrá su contrapartida, pues también habrá enemigos más duros que vencer.


A lo largo de nuestro viaje **moriremos, muchas veces, y volveremos a renacer perdiendo todo nuestro equipo y parte de nuestras habilidades**. Tendremos, pues, que ir a buscarlos si no queremos empezar de nuevo, pero poniéndonos en peligro, especialmente si el enemigo que nos derrotó, que seguirá ahí, es por ejemplo un gigantesco Troll o uno de los **jefes del juego**. En cuanto a estos últimos, al derrotarlos **adquiriremos parte de sus poderes**, que podremos activar especialemente en situaciones complicadas.


En cuanto al apartado técnico, el juego no despliega un derroche gráfico, pues está desarrolado con **voxeles, escasos polígonos y texturas de baja resolución** de forma intencionada, inspirado en los viejos juegos 3D de los años 90; pero **combinándolo con iluminación y efectos actuales**, dándole un encanto especial, y siendo notables las puestas y salidas de sol, o los imponentes paisajes que pueden observar desde lugares elevados. Según la descripción oficial que podemos encontrar en Steam estas son algunas de sus caracteríticas:


* Enorme mundo generado proceduralmente: explora y habita tierras místicas, desde bosques misteriosos hasta imponentes cadenas montañosas cubiertas de nieve y prados impresionantes, llenos de criaturas legendarias para luchar y vida silvestre para cazar.
* Multijugador cooperativo - por si deseas enfrentarte a estas tierras sólo o aventurarte con aliados de confianza, Valheim soporta servidores independientes, alojados por jugadores y creación de mundos infinitos.
* Sistema de combate punitivo basado en esquivar y bloquear en el que dispondrás de una amplia gama de armas
* Construye y navega en barcos, desde frágiles navíos hasta majestuosos barcos de guerra, construye barcos legendarios para conquistar los mares y descubrir nuevas tierras.
* Invoca y derrota a los vengativos jefes legendarios de los mitos y leyendas, y recoge trofeos para progresar y crear nuevos y poderosos objetos.
* Sistema flexible para construir casas y bases - crea salas de hidromiel, granjas, asentamientos, puestos de avanzada, castillos y más.
* Creación intuitiva de objetos - forja las mejores armas y armaduras, y elabora comida y aguamiel artesanalmente.
* Servidores dedicados - para jugadores que desean ejecutar un servidor persistente.


En cuanto a la hoja de ruta del desarrollo del juego, la gente Iron Gate Studio acaba de publicar la siguiente infografía donde podemos ver los hitos a los que tienen pensado llegar a lo largo de este año:  
  
![roadmap](https://mcusercontent.com/5db105e138032e564b4758e4b/images/a0f45faf-6133-41ea-b6a7-123edb820920.jpg)


He de decir que habiendo jugado más de 12 horas, el juego ha conseguido engancharme, y ciertamente vale mucho la pena, especialmente si os gustan los juegos de supervivencia tipo Minecraft o Rust. Desde JugandoEnLinux.com seguiremos informando sobre las noticias que genere su desarrollo y por supuesto cuando finalice el periodo de Acceso Anticipado y teingamos la versión final. Os dejamos con un video que hemos grabado mostrandoos los primeros compases del juego, y atentos, que **en él desvelaremos una clave del juego para el más rápido en registrarla**:


  
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/6W9vN61wWOE" style="display: block; margin-left: auto; margin-right: auto;" width="780"></iframe></div>  
**Podeis comprar Valheim en Acceso Anticipado en Steam por 16,79€:**  
  
<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/892970/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 ¿Os gustan los juegos de supervivencia? ¿Qué os parece la vikinga propuesta de Valheim? Cuéntanoslo en los comentarios o en nuestros grupos de JugandoEnLinux en [Telegram](https://twitter.com/JugandoenLinux) o  [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).
