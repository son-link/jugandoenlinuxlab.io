---
author: Pato
category: "Simulaci\xF3n"
date: 2017-12-14 09:15:57
excerpt: "<p>\xBFTe dedicas a sembrar el caos o a ayudar a detenerlo?</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/876bf0d06d19e26d37c3ab00d016af7b.webp
joomla_id: 576
joomla_url: te-gusta-ser-hacker-grey-hack-llega-manana-a-linux-steamos-en-acceso-anticipado
layout: post
tags:
- proximamente
- acceso-anticipado
- simulador
- simulacion
- mmo
title: "\xBFTe gusta ser Hacker? 'Grey Hack' llega ma\xF1ana a Linux/SteamOS en acceso\
  \ anticipado"
---
¿Te dedicas a sembrar el caos o a ayudar a detenerlo?

Si te has visto todos los episodios de la serie de Mr Robot y te quedaste con ganas de coger la terminal de tu ordenador y hacerle un roto al mundo, o salvarlo de la catástrofe que sufre toda la humanidad en la serie  puede que este juego te interese. 'Grey Hack' es un juego algo atípico. Tras pasar por una campaña en Steam Greenlight mañana llega a nuestro sistema favorito lo que puede ser el sueño de cualquiera que le guste 'trastear' con equipos ajenos pero sin incurrir en delito alguno.



> 
> *Grey Hack es un simulador de hacking multijugador masivo online.*
> 
> 
> *Eres un pirata informático con total libertad para actuar como desees en una vasta red de ordenadores creada aleatoriamente.*
> 
> 
> *La interfaz del juego es similar a un sistema operativo de escritorio actual. El explorador de archivos, la terminal de comandos o el editor de texto son algunos de los programas que estarán disponibles desde el inicio del juego. A medida que mejores tus habilidades, encontrarás nuevas utilidades que podrás instalar y usar.*
> 
> 
> *En Grey Hack, la terminal tiene un papel fundamental, se basa en comandos Unix reales y será una herramienta indispensable para realizar acciones de hacking con éxito.*
> 
> 
> 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/A512mhb_rrU" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 Como indica el autor, el juego llega en fase de acceso anticipado pues aún no ha implementado todos los elementos que aún están pendientes de llegar, y a su vez servirá para recibir "feedback" de los jugadores para mejorar e implementar nuevas ideas.


Si estás interesado en crear un "caos total" cual Elliot Alderson puedes encontrarlo mañana mismo en su página de Steam traducido al español:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/605230/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 

