---
author: Pato
category: Noticia
date: 2018-06-19 10:17:52
excerpt: "<p>Llegan a Steam las p\xE1ginas personalizadas para poder seguir mejor\
  \ a tus favoritos</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/3ff2a64fd14a6a6d971315fb7b8085ec.webp
joomla_id: 775
joomla_url: ya-puedes-seguir-a-tus-estudios-y-editoras-favoritos-en-steam
layout: post
tags:
- steam
title: Ya puedes seguir a tus estudios y editoras favoritos en Steam
---
Llegan a Steam las páginas personalizadas para poder seguir mejor a tus favoritos

Steam sigue introduciendo cambios y ahora le toca el turno a las páginas personalizadas de estudios y editoras, de modo que podremos seguir con un mayor detalle todos los juegos y novedades que se vayan introduciendo en la plataforma de nuestros favoritos.


"Cualquier desarrollador o editor ahora puede configurar una página personalizada para mostrar todo su catálogo, títulos y contenido. Una vez configuradas, estas páginas pueden ser encontradas clicando en el nombre de la editora o el desarrollador desde la página de la tienda de tus juegos favoritos.


![edotira](https://steamcdn-a.akamaihd.net/store/about_devs/announcement_page_example_1.jpg)


Hay que tener en cuenta que esta característica está en fase de beta abierta, por lo que es posible que muestre fallos de formato o alguna característica errónea.


Si quieres saber más, puedes visitar la página del anuncio [en este enlace](https://store.steampowered.com/publisher/).

