---
author: leillo1975
category: Software
date: 2019-12-16 10:55:08
excerpt: "<p>El proyecto acaba de actualizarse a la versi\xF3n 1.5 </p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/DXVK/DXVK-D9VK.webp
joomla_id: 1139
joomla_url: d9vk-finalmente-se-integra-en-dxvk
layout: post
tags:
- wine
- steam-play
- dxvk
- proton
- d9vk
title: D9VK finalmente se integra en DXVK (ACTUALIZADO).
---
El proyecto acaba de actualizarse a la versión 1.5 


**ACTUALIZADO:** Hace menos de 20 minutos que nos ha llegado un mail anunciándonos una [nueva versión, la 1.5](https://github.com/doitsujin/dxvk/releases/tag/v1.5), de DXVK con D9VK integrado. Contiene las siguientes novedades:


*Corrección de errores de D9VK 0.40.1*


*-Corregidas algunas fugas de memoria y recursos en los bloques de estado, borrado y restablecimiento de dispositivos.*  
 ***-Dark Souls: Prepare to Die Edition:** Se ha corregido un problema en la pantalla negra que se producía en algunos controladores.*


*Mejoras en el HUD*


***-Se mejoró la apariencia general del HUD** y se corrigió un problema con algunos "glyphs" que se colocaron incorrectamente.*  
 *-Las estadísticas de asignación de memoria muestran ahora la cantidad de memoria asignada por pila de memoria Vulkan, lo que permite distinguir entre la memoria de vídeo y las asignaciones de memoria del sistema.*  
 *-Se ha corregido un problema por el que las estadísticas de envío de llamadas de sorteo y colas se actualizaban antes de que el marco actual haya terminado de procesarse, lo que daba lugar a números inexactos.*  
 *-Las estadísticas de envío de llamadas y colas de sumisión se actualizan cada 0,5 segundos para hacerlas más legibles.*


*Corrección de errores y mejoras*


*-**Atelier Ryza:** Habilitó la solución para un problema de pantalla negra causado por el uso de D3D9 del juego cuando se intentaba reproducir vídeos introductorios. Requiere tanto D3D9 como D3D11.*  
 *-**Crysis 3:** Todas las GPUs son ahora reportadas como GPUs Nvidia por defecto. Esto permite al juego utilizar una ruta rápida con una sobrecarga de CPU considerablemente menor, pero puede causar un pequeño impacto en el rendimiento de algunas GPUs Nvidia en escenarios vinculados a la GPU. Tenga en cuenta que puede ser necesario desactivar la carga de nvapi.dll en el prefijo de su Wine cuando utilice wine-stapping.*  
 *-**Halo MCC:** Redujo el número de mensajes de registro generados debido al uso no válido de la API D3D11.*  
 *-**Star Citizen:** Arreglado una regresión que causaba problemas con la iluminación.*


 




---


**NOTICIA ORIGINAL:**


Tal y como acabamos de leer en [reddit](https://www.reddit.com/r/linux_gaming/comments/eb8y4z/d9vk_is_soon_to_be_merged_into_dxvk/), este "hermano pequeño" de DXVK acaba de ser fusionado en este, por lo que **a partir de ahora solo habrá un proyecto** y **D9VK será un componente de DXVK**. Esto facilitará mucho las cosas para su uso, y **las actualizaciones futuras serán conjuntas**, por lo que serguir el desarrollo del proyecto será mucho más sencillo. El "[Pull Request](https://github.com/doitsujin/dxvk/pull/1275)" que solicitaba esta integración ha sido aceptado hace unas horas, por lo que ya es oficial.


Como sabeis tanto **DXVK como D9VK son implementaciones de DirectX10 y 11, y DirectX9 respectivamente usando** para ello la **API Vulkan**, lo que les confiere una **mayor calidad y velocidad** cuando lo usamos conjuntamente con Wine o Steam Play/Proton. Como sabeis ambos proyectos son ahora una parte sustancial de la herramienta de Valve, y su avance la ha permitido dotar de una gran compatibilidad.


[DXVK](https://github.com/doitsujin/dxvk) **ha sido noticia hace tan solo unos días** porque su creador, Philip Rebohle ([@doitsujin](https://github.com/doitsujin)), lo ha tenido que poner en [modo mantenimiento]({{ "/posts/dxvk-entra-en-mantenimiento-a-partir-de-ahora-solo-obtendra-parches" | absolute_url }}) para poner un poco de orden en el código debido a su cada vez más complicado desarrollo. En cuanto a [D9VK](https://github.com/Joshua-Ashton/d9vk), ha sido un proyecto creado por Joshua Ashton ([@Joshie](https://github.com/Joshua-Ashton)) basándose en las mismas premisas que DXVK. Esperemos que esta colaboración entre ambos lleve al DXVK a unas cotas de éxito aun si cabe mayores que las que han cosechado hasta ahora.


¿Qué os parece esta fusion de proyectos? ¿Sois usuarios de DXVK/D9VK? Cuéntanoslo en los comentarios, o en mensajes en nuestra comunidad de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org) o nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

