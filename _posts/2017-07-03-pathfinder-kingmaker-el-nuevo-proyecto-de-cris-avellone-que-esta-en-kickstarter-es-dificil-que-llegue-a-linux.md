---
author: Pato
category: Rol
date: 2017-07-03 16:24:44
excerpt: "<p>(Actualizaci\xF3n) \xA1El proyecto ha superado el objetivo y tendremos\
  \ el juego para Linux!</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/9aa91352e792f145830c61b99571c730.webp
joomla_id: 393
joomla_url: pathfinder-kingmaker-el-nuevo-proyecto-de-cris-avellone-que-esta-en-kickstarter-es-dificil-que-llegue-a-linux
layout: post
tags:
- indie
- aventura
- rol
- kickstarter
title: "'Pathfinder: Kingmaker' el nuevo proyecto de Cris Avellone que est\xE1 en\
  \ Kickstarter es dif\xEDcil que llegue a Linux - Actualizaci\xF3n (\xA1Pero no imposible!)"
---
(Actualización) ¡El proyecto ha superado el objetivo y tendremos el juego para Linux!

**ACTUALIZACIÓN:**


¡Quién lo diría hace menos de una semana! Hace tan solo 7 días el Kickstarter de Pathfinder: Kingmaker estaba estancado en unos 600.000$, y de hecho hace tan solo 3 días aún faltaban cerca de 30.000$ para llegar al objetivo que desbloqueaba la versión para Linux, pero en tan solo 48 horas el proyecto ha cogido carrerilla y ahora mismo, y a falta de 44 horas para terminar la campaña ¡el objetivo está cumplido!.


El equipo del proyecto en su [ultima actualización](https://www.kickstarter.com/projects/owlcatgames/pathfinder-kingmaker/posts/1932923) lo celebra:


*"Vosotros los que rezáis a Mac & Linux no necesitaréis cambiar a vuestras deidades - Pathfinder: Kingmaker estará disponible en vuestras sagradas tierras"*


Ahora mismo ya han superado los 720.000$, y van a por el siguiente objetivo que será el diseñar 3 arquetipos por clase para hacer el juego aún mas profundo en cuanto al desarrollo y configuración de personajes. Además, han añadido la posibilidad de aportar vía Paypal para aquellos que quieran o no puedan hacerlo vía Kickstarter.


Esperemos que lleguen a buen puerto y nos traigan una versión Linux decente. No es la primera vez que un proyecto nos deja en la estacada por diversos motivos tras prometer una versión para Linux en Kickstarter, pero personalmente espero que esta no sea una de esas veces. Por cierto, van a hacer un [AMA (Ask me Anything) en Reddit](https://www.reddit.com/r/Pathfinder_RPG/comments/6maqvu/ama_with_the_devs_of_pathfinder_kingmaker_on_july/) esta misma tarde a las 8:59h PM (CET).


¿Quieres aportar al proyecto ahora que ya han "asegurado" el objetivo? Puedes hacerlo en su campaña:


<div class="resp-iframe"><iframe height="420" src="https://www.kickstarter.com/projects/owlcatgames/pathfinder-kingmaker/widget/card.html?v=2" width="220"></iframe></div>


¿Qué te parece Pathfinder: Kingmaker? ¿Te gustan los juegos de Rol de la vieja escuela?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


**Artículo original:**


Para el que no lo conozca, Cris Avellone está detrás de la narrativa de multitud de juegos conocidos, algunos de ellos de gran renombre como "Planetscape Torment", "Star Wars: Knights of the Old Republic II, The Sith Lords" o "Fallout 2" o "New Vegas". Después de reunir a un buen grupo de miembros, han fundado un nuevo estudio llamado "[Owlcat Games](https://owlcatgames.com/)" para intentar traer un nuevo juego de rol "de la vieja escuela" para un solo jugador del conocido juego de rol "Pathfinder".


De este modo, han lanzado una campaña en Kickstarter para recabar fondos y poder financiar más y mejores características para el juego, consiguiendo llegar a la meta de los 500.000$ necesarios para garantizar la financiación pero tras conseguir llegar a más de 610.000$ y conseguir algunos objetivos adicionales la campaña parece haberse estancado un poco. El siguiente objetivo está en los 700.000$, y este precísamente es el que más nos interesa a nosotros ya que según la campaña daría pié a que el juego llegase a Linux y Mac. 


La pena es que a falta de tan solo 8 días aún quedan unos 80.000$ para llegar a esta meta, pero no parece probable que la campaña llegue a recaudar semejante cantidad a no ser que haya un buen sprint en los últimos días, aunque también es cierto que suelen darse sorpresas y sprints de última hora en Kickstarter que pueden darnos una alegría.


Si estás interesado o quieres colaborar a que se cumpla el objetivo, puedes echar un vistazo a la campaña:


<div class="resp-iframe"><iframe height="360" src="https://www.kickstarter.com/projects/owlcatgames/pathfinder-kingmaker/widget/video.html" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>

