---
author: Pato
category: Software
date: 2019-04-18 10:34:15
excerpt: <p>El cliente de juegos de Epic llega a Linux de forma "no oficial"</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/1d199d462017bbd5dcb9d16235a6c9e3.webp
joomla_id: 1023
joomla_url: lutris-hace-posible-ejecutar-la-epic-games-store-gracias-wine
layout: post
tags:
- wine
- lutris
- epic-games
title: Lutris hace posible ejecutar la Epic Games Store gracias a Wine
---
El cliente de juegos de Epic llega a Linux de forma "no oficial"

Esta mañana ha saltado la sorpresa al enterarnos de que Lutris ahora tiene un script para instalar la Epic Games Store mediante Wine:



> 
> Good news! [@EpicGames](https://twitter.com/EpicGames?ref_src=twsrc%5Etfw) Store is now fully functional under Linux if you use Lutris to install it! No issues observed whatsoever. <https://t.co/cYmd7PcYdG>[@TimSweeneyEpic](https://twitter.com/TimSweeneyEpic?ref_src=twsrc%5Etfw) will probably like this ? [pic.twitter.com/7mt9fXt7TH](https://t.co/7mt9fXt7TH)
> 
> 
> — Lutris Gaming (@LutrisGaming) [April 17, 2019](https://twitter.com/LutrisGaming/status/1118552969816018948?ref_src=twsrc%5Etfw)


  





Según publica la cuenta oficial de Lutris *"la Epic Games Store ahora es plenamente funcional bajo Linux si usas Lutris para instalarla. No se han observado problemas. A Tim Sweeney probablemente le gusta esto."*


Si nos dirigimos al e[nlace de la Epic Games Store en Lutris](https://lutris.net/games/epic-games-store/) podemos ver que le dan un soporte "platinum" y desde ahí podéis ejecutar el script para instalarlo.


Polémicas aparte, en Jugando en Linux hemos estado un buen rato debatiendo en si esto era merecedor de una reseña en la web, y tras meditarlo hemos decidido publicarlo y además explicar un poco nuestra postura.


Creo que no soprenderemos a nadie si decimos que Epic no es una compañía muy *amiga* de Linux. Sí es cierto que ofrece soporte para nuestro sistema con su motor Unreal Engine, pero todo se limita a eso, un soporte de cara a desarrollo pero que no va mas allá. Por otro lado, su CEO Tim Sweeney hace años que comenzó a quejarse de la deriva que parecía tomar Microsoft con Windows y su ecosistema cada vez mas cerrado, pero siempre hizo oídos sordos a las sugerencias de apoyo al sistema del pingüino.


Además, ahora tenemos a Epic con su nueva tienda sustentada en el éxito fulminante que supuso Fortnite, y que según nuestro punto de vista tiene unas políticas cuanto menos discutibles en cuanto a apoyo a desarrolladores, venta de juegos y contratos de exclusividad que están derivando en no pocas acaloradas discusiones entre los usuarios que ven por un lado un posible intento de "cerrar" el mercado de juegos en PC sobre una tienda con políticas muy restrictivas y prácticas cuanto menos tóxicas, y por otro una mas que saludable competencia frente a una Valve acomodada en su posición dominante del mercado.


Por último, no hay que olvidar que Epic no tiene planes conocidos de traer ninguno de sus presentes desarrollos a Linux. Ni Fortnite, ni la Epic Games Store ni por supuesto ninguno de sus títulos exclusivos. Sin embargo, al igual que hemos hecho con otras tiendas y juegos que gracias a Wine nos llegan a Linux, si existe la posibilidad de "jugar" a los títulos de la Epic Games Store a través de Lutris es algo que como jugadores al fin y al cabo pensamos que debemos publicar.


Al fin y al cabo, esto se trata de jugar en Linux, y si podemos hacerlo a través de Lutris y la EGS... ¿por que no?


Otra cosa es que nosotros estemos de acuerdo o no con sus políticas, o pensemos que mientras mantengan sus políticas actuales no jugaremos a ninguno de sus juegos. Pero como para gustos los colores, puede que vosotros tengáis una opinión  distinta y como tal, nuestra posición es la de no juzgar por vosotros. Nos comprometimos a informar sobre los juegos en Linux, y pensamos cumplir nuestro compromiso.


¿Que pensáis sobre la posibilidad de ejecutar la Epic Games Store y sus juegos en Linux mediante Wine y Lutris? ¿Has probado a ejecutar algún juego con la EGS?


Cuéntamelo en los comentarios o en el canal de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux).

