---
author: leillo1975
category: Carreras
date: 2018-04-26 07:16:00
excerpt: <p>El juego, de @NeognosisGames, sale en Acceso Anticipado</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/edb573a17581af3290eff07627fcedd0.webp
joomla_id: 722
joomla_url: disponible-ballisticng-para-gnu-linux-steamos
layout: post
tags:
- retro
- wipeout
- ballisticng
- psygnosis
- neognosis
title: Disponible "BallisticNG" para GNU-Linux/SteamOS
---
El juego, de @NeognosisGames, sale en Acceso Anticipado

A través de diversos comentarios en nuestro canal de [Telegram](https://t.me/jugandoenlinux), nos ha llegado información de la existencia de este juego desarrollado por la [Neognosis Games](http://neognosisgames.com), una compañía británica indie formada por tan solo dos personas que han conseguido crear este juego de velocidad inspirado en el clásico [Wipeout](https://es.wikipedia.org/wiki/Wipeout_(videojuego)) de los 90' (que tiempos con mi Sega Saturn...), que a su vez fué diseñado por la mítica [Psygnosis](https://es.wikipedia.org/wiki/Psygnosis) (como veis, hasta el nombre de NeoGnosis hace clara alusión a este mítico estudio).


El juego en este momento se encuentra en desarrollo y ha sido lanzado al mercado en **Acceso Anticipado**. En el tendremos que competir a lo largo de 17 pistas con 13 vehículos diferentes, con diversos modos de juego donde incluso podremos jugar a pantalla partida. El aspecto gráfico, como podeis ver en el trailer del juego, tiene una clara inspiración de su antecesor, emulando los primeros títulos en 3D de mediados de los años 90:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/Futz_uuszFc" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


El juego, que en este momento no está traducido al español, permite disfrutar también de un modo campaña y tiene soporte para Realidad Virtual, de ahí el porqué de la vista desde el interior del vehículo, que también está incluída. En cuanto a los requisitos técnicos del juego, son muy accesibles y funcionará sin problemas en cualquier equipo, como podeis ver aquí:


**Mínimo:**  

+ **SO:** 64-bit distro
+ **Memoria:** 2 GB de RAM
+ **Gráficos:** Intel HD Graphics 4000
+ **Almacenamiento:** 2 GB de espacio disponible
+ **Notas adicionales:** Requires OpenGL 3.2+
+ 



BallisticNG **se puede comprar en Steam a un precio de risa**, por lo que adquirirlo no supondrá un problema:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/473770/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


¿Comprarás BallisticNG? ¿Has jugado a WipeOut en el pasado? Deja tus impresiones en los comentarios o en nuestro canales de [Telegram](https://t.me/jugandoenlinux) o [Discord](https://discord.gg/fgQubVY)

