---
author: Son Link
category: Estrategia
date: 2021-10-27 14:33:36
excerpt: "<p>Tras un par de meses de trabajo tenemos nueva versi\xF3n de @Warzone_2100</p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Warzone2100/420/portada.webp
joomla_id: 1383
joomla_url: warzone-2100-2-4-0
layout: post
tags:
- open-source
- warzone-2100
title: Lanzado oficialmente Warzone 2100 2.4.0
---
Tras un par de meses de trabajo tenemos nueva versión de @Warzone_2100


Si hace algunos días os hable de la nueva versión del veterano [Battle for Wesnoth](index.php?option=com_content&view=article&id=1382:la-batalla-por-wesnoth-1-16&catid=17:estrategia&Itemid=1420), hoy toca de hablar de la nueva versión de otro veterano: **Warzone 2100 4.2.0**


La comunidad detrás del juego sigue trabajando en ir mejorandolo, y prueba de ello es que se han realizado un total de 340 commits desde la publicación de la versión anterior hace un par de meses.


![en partida](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Warzone2100/420/en_partida.webp)


**Entre los cambios están:**


* Se han añadido a los **Ultimate Scavengers****.**
* Se han añadido [slots para espectadores](https://wz2100.net/news/new-feature-spectators/) en el multijugador, los cuales podrán ver la partida en vivo (siempre que su conexión lo permita), ver el mapa completo y hablar con otros espectadores, pero no con los jugadores, entre más cosas.
* Ahora es posible [grabar repeticiones](https://wz2100.net/news/new-feature-replays/) en partidas de escaramuzas / multijugador.
* Devolución de las unidades a las torretas móviles y mejor distribución en los múltiples puntos de reparación.
* Actualización de camBalance para Gamma (el reequilibrio de la campaña está básicamente completo)
* Añadir nuevos mapas aleatorios "DustyMaze": uno para 2 jugadores y dos para 4 jugadores
* Utilizar un directorio de configuración estable para las versiones de lanzamiento (antes cada versión tenia el suyo propio).
* Cambios en el equilibrio del multijugador.
* Se ha aumentado la potencia del barril de petroleo en las campaña Beta (150) y Gamma (200).
* Implementado un atajo para eliminar del grupo.
* Ahora las unidades y estructuras seleccionas parpadean en el radar.
* Se añadió *"potencia por segundo"* al tooltip de la barra de potencia.


Y entre otras correcciones:


* Órdenes de los Comandantes después de cargar las partidas guardadas.
* No revelar la artillería a menos que la torre CB esté completamente construida.
* Que al adjuntar sensores al Comandante su lógica no sea inútil.
* Ahora sólo se reproduce el sonido de *"asignado al comandante"* si el grupo no está lleno.
* No se bloquea el botón de estado listo cuando el jugador hace clic en el demasiado rápido.
* Las unidades primarias de torretas AA no atacan si las secundarias pueden hacerlo, entre otras correcciones.
* No se bloquea la combinación de teclas **Saltar al extractor de recursos** después de recargar el nivel.
* Evitar un problema del transportador antiguo en el final de la campaña Beta que puede llevar al fracaso del nivel.


Tenéis la lista de cambios completo [en este enlace](https://github.com/Warzone2100/warzone2100/raw/4.2.0/ChangeLog)


Y si queréis jugar contra otros miembros de la comunidad, pásate por la sala **A Jugar** en Matrix, que varios estaremos encantados de ello.


![configurando espectadores](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Warzone2100/420/configurando_espectadores.webp)![reproduciendo escaramuza](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Warzone2100/420/reproduciendo_escaramuza.webp)

