---
author: leillo1975
category: Carreras
date: 2020-07-13 11:12:42
excerpt: "<p>Sus desarrolladores <span class=\"css-901oao css-16my406 r-1qd0xha r-ad9z0x\
  \ r-bcqeeo r-qvutc0\">@KarmaPlay2</span> as\xED lo han confirmado recientemente.</p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/OldSkoolRacer/OldSkoolRacer001.webp
joomla_id: 1239
joomla_url: el-arcade-de-carreras-old-skool-racer-llegara-a-linux
layout: post
tags:
- carreras
- arcade
- unity3d
- old-skool-racer
- karma-play
title: "El arcade de carreras \"Old Skool Racer\" llegar\xE1 a Linux"
---
Sus desarrolladores @KarmaPlay2 así lo han confirmado recientemente.


 Gracias a un enlace que nos pasó nuestro colega @ser_jor de la fantástica web que es [LinuxGameConsortium.com](https://linuxgameconsortium.com/old-skool-racer-will-get-native-release/), nos hemos enterado de la existencia de este juego, que como podeis deducir por su nombre, "Old Skool Racer", **pretende ser un homenaje a los juegos de coches de antaño**. [Karma Play](https://karmaplay.net/), que así se llama la pequeña compañía que está detrás, confirmaba a LGC la versión de Linux:


*"Once the Windows version is stable and more fully featured. There will be a Linux port." ("Una vez que la versión de Windows sea estable y más completa. Habrá un port de Linux.")*  



Entrando en su página de [Steam](https://store.steampowered.com/app/1285310/OLD_SKOOL_RACER/), podreis comprobar que **el juego pretende emular la jugabilidad y aspecto de juegos como los primeros Need For Speed, Screamer o Star Wars Racer**. El juego, **desarrollado con el motor Unity3D**, está en **Acceso Anticipado** y **se encuentra en sus primeras fases de su creación**, teniendo estimado lanzar la versión definitiva en 2023 **La intención es actualizarse regularmente mes a mes, recogiendo el feedback de los usuarios para mejorarlo**. Según la información oficial, en "Old Skool Racer" encontraremos:


***Características clave del juego planificado***  
*Recrea la pura emoción de los clásicos juegos de carreras arcade como Outrun, Screamer, Star Wars Pod Racer y Wave Race 64. Los jugadores pueden elegir entre cinco tipos diferentes de vehículos, incluidos automóviles, motocicletas, lanchas rápidas, aerodeslizadores y carros.*  
  
*Más de 30 ubicaciones diurnas y nocturnas como Hurricane City, Borneo, Aspen, Perú, San Francisco, Londres, París, Yosemite, Egipto, Nevada, China, Noruega, Alemania, Amsterdam, Suiza, India, Tanzania, Islandia y Venecia.*  
  
*Los modos de juego para un jugador incluyen Quick Race, Time Trial, Stunt Mode, Combat Mode y Championship*  
  
*Las características multijugador incluirán Quick Race, Championship, servidores dedicados, lista de amigos, emparejamiento, logros, trofeos y una tabla de clasificación mundial.*  
  
*Opciones de modificación del vehículo*  
*Personalice la apariencia de su vehículo utilizando una combinación de piezas especiales, colores de pintura, calcomanías y efectos. Elija el color exacto de literalmente millones de colores usando una rueda de colores estilo Photoshop.*  
  
*Cambie completamente la apariencia del vehículo utilizando múltiples tipos de materiales como fibra de carbono, metal brillante y toon. Agregue calcomanías de cientos ya incluidas o elija de su propia biblioteca de imágenes. Pinte directamente sobre la superficie de los vehículos con una variedad de pinceles personalizados. Agregue piezas nuevas como spoilers y cree su propio escape personalizado, efectos nitrosos para que sus vehículos se vean únicos y distintos de otros jugadores.*  
  
***La isla del ganador***  
*Adquiera su propia hermosa isla privada de Winner's y una mansión de lujo que se puede personalizar con cientos de elementos virtuales, luces y efectos visuales. La mansión también albergará la Sala de trofeos 3D del jugador, la tabla de clasificación digital y el garaje.*  
  
*Camina entre cientos de islas similares creadas por otros ganadores como tú. Participe en minijuegos multijugador como tenis, ping-pong, billar y waterpolo en cualquiera de estas islas. Envíe invitaciones a otros jugadores y a sus amigos de las redes sociales para que se unan a usted para un mini juego o una proyección de video privada en el cine en casa virtual de su mansión. Transmita videos desde su canal de YouTube o directamente desde su PC.*  
  
***Editor de niveles***  
*El juego contará con un editor de niveles completo que permitirá a los jugadores personalizar todos los niveles existentes o crear niveles completamente nuevos utilizando las herramientas y los activos proporcionados. El editor de niveles también proporcionará la capacidad de cambiar la iluminación y agregar efectos visuales como lluvia, fuego, humo, nieve y explosiones.*


Como veis sus desarrolladores son bastante ambiciosos, pero por el momento **el juego aun necesita un montón de trabajo** (recordad que está en Acceso Anticipado). Esperemos que en un futuro próximo podamos ver de forma nativa todas estas características y echar unas desenfadadas carreras al viejo estilo. Os dejamos con un "gameplay" del juego:


  
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/V1TyBHwfZ6g" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


¿Os gustan los juegos de carreras antiguos? ¿Qué os parece la propuesta de Karma Play con este "Old Skool Racer"? Podeis darnos vurstra opinión sobre este juego en los comentarios de este artículo, en nuestra comunidad de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

