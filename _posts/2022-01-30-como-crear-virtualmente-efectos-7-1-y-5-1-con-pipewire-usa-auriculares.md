---
author: Serjor
category: "V\xEDdeos JEL"
date: 2022-01-30 15:59:01
excerpt: "<p>En esta ocasi\xF3n os traemos un v\xEDdeo de c\xF3mo crear un dispositivo\
  \ virtual obtener un efecto 7.1 en auriculares est\xE9reo.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Videotutoriales/pipewire_7.1.webp
joomla_id: 1417
joomla_url: como-crear-virtualmente-efectos-7-1-y-5-1-con-pipewire-usa-auriculares
layout: post
tags:
- pipewire
- tutorial
- videotutorial
- sonido-envolvente
title: "C\xF3mo crear virtualmente efectos 7.1 y 5.1 con pipewire (usa auriculares\
  \ \U0001F3A7)"
---
En esta ocasión os traemos un vídeo de cómo crear un dispositivo virtual obtener un efecto 7.1 en auriculares estéreo.


 


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/hfuLPCb_Lyo" title="YouTube video player" width="560"></iframe></div>

