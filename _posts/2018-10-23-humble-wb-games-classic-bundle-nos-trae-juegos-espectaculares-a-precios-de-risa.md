---
author: leillo1975
category: Ofertas
date: 2018-10-23 19:19:44
excerpt: <p>@humble lo ha vuelto a hacer....</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/52738552667e33eb1fba35383a2da217.webp
joomla_id: 879
joomla_url: humble-wb-games-classic-bundle-nos-trae-juegos-espectaculares-a-precios-de-risa
layout: post
tags:
- humble-bundle
- ofertas
- humble-store
- warner-bros
title: Humble WB Games Classic Bundle nos trae juegos espectaculares a precios de
  risa.
---
@humble lo ha vuelto a hacer....

EXTRA!!!, EXTRA!!!... Super-mega-chachi bundle que te pasas!!! La conocida página se acaba de sacar de la manga una estupenda colección de títulos de **Warner Bros** entre los cuales encontraremos grandísimos juegos por un precio ridículo. hacía tiempo que no teníamos un bundle tan interesante como este, y por muy poco dinero podremos conseguir estos enormes títulos que cualquier colección linuxera que se precie debe incluir. Este es el tweet que lo anunciaba:



> 
> WB Games is here with a bundle of some of the most beloved characters, franchises, and heroes in the world!<https://t.co/HZ503Bj54G> [pic.twitter.com/7DvqEOkuap](https://t.co/7DvqEOkuap)
> 
> 
> — Humble Bundle (@humble) [23 de octubre de 2018](https://twitter.com/humble/status/1054794852968423424?ref_src=twsrc%5Etfw)



**Si pagamos 1$ (0.87€)** nos llevaremos el espectacular "**Shadow of Mordor**" (basado en "El señor de los anillos"), a parte de "**Scribblenauts Unlimited"** y "**Batman Arkham Origins**" (para Windows)


**Si ponemos un poco más (en este momento 3.82$ - 3.32€)** nos llevaremos también el original "**Bastion"** y "**Mad Max"**, [juego que analizamos hace algún tiempo](index.php/homepage/analisis/item/102-analisis-mad-max) en la web. También van incluidos en el saco "**Injustice: Gods Among Us Ultimate Edition**" y un **descuento del 10% el el juego de LEGO de los Increibles**, ambos para Windows.


**Si decidimos pagar 12$ (10.44€)** nos haremos también con "**Batman ™: Arkham Knight**" y su **Season Pass** (Windows)


Podeis encontrar estos juegos en:


### **[Humble WB Games Classic Bundle](https://www.humblebundle.com/games/wb-games-classics-bundle?partner=jugandoenlinux)** (patrocinado).


Recordad que teneis dos semanas para haceros con ellos.... aunque realmente ya estais tardando

