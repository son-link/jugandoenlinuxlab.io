---
author: leillo1975
category: "Simulaci\xF3n"
date: 2017-08-12 15:50:00
excerpt: "<p>SCS Software acaba de anunciar su nueva expansi\xF3n</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/661821a9442a8dbd824e89bd18c0fd2e.webp
joomla_id: 430
joomla_url: bella-italia-sera-la-nueva-dlc-para-euro-truck-simulator-2
layout: post
tags:
- ets2
- dlc
- scs-software
- euro-truck-simulator-2
- bella-italia
title: "\"Bella Italia\" ser\xE1 la nueva DLC para Euro Truck Simulator 2 (ACTUALIZACI\xD3\
  N 2)"
---
SCS Software acaba de anunciar su nueva expansión

**ACTUALIZACIÓN 24-11-17:** SCS acaba de publicar el trailer oficial de esta expansión, y si en las fotos de la última actualización el nivel de hype era ya altísimo, ahora ... buff. Mejor juzgad por vosotros mismos (*gracias Pato por el aviso*):


 <div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/OtYNJ077oWA" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 




---


**ACTUALIZACIÓN 20-11-17:** Mientras esperamos la llegada de esta expansión, en las [noticias de Steam](http://steamcommunity.com/games/227300/announcements/detail/2818553827436409971) han publicado fotos de la isla italiana de Sicilia (gracias al "jefe" [Pato](index.php/homepage/generos/estrategia/itemlist/user/192-pato) por el aviso). Las fotos tienen una pinta estupenda y muchos ya estamos "babeando" por darle a la rosca en esta región. En este isla encontraremos multitud de  ciudades costeras, paraisos naturales, el volcán Etna, todo ello rodeado de un ambiente puramente mediterraneo que pronto estaremos disfrutando. Os dejamos con unas cuantas instantaneas para que os vayais deleitando:


![ETS2BISic1](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisETS2BellaItalia/ETS2BISic1.webp)


![ETS2BISic2](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisETS2BellaItalia/ETS2BISic2.webp)


![ETS2BISic3](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisETS2BellaItalia/ETS2BISic3.webp)


 


 




---


**NOTICIA ORIGINAL:** Poco antes de la últimas navidades dábamos cuenta de la nueva expansión de Euro Truck Simulator 2 ([Vive la France!](index.php/homepage/analisis/item/243-analisis-ets2-vive-la-france-dlc)), que nos permitía trabajar por tierras galas.  Hoy la compañía checa, [SCS Software](index.php/component/search/?searchword=SCS&searchphrase=all&Itemid=456), ha anunciado por sorpresa que su próxima expansión para ETS2 discurrirá por mi idolatrada Italia. Podemos verlo en la siguiente tweet y un [mensaje de su Blog](http://blog.scssoft.com/2017/08/bella-italia.html?m=1):


 



> 
> Bella Italia! ??  
>   
> That's our next destination in Euro Truck Simulator 2!  
>   
> Check out the link below for more info ?<https://t.co/UhcWjDZywM> [pic.twitter.com/rUN15oGEtf](https://t.co/rUN15oGEtf)
> 
> 
> — SCS Software (@SCSsoftware) [August 11, 2017](https://twitter.com/SCSsoftware/status/896053038263050240)



 


Cuando aún estamos esperando para poder disfrutar de la [expansión de Nuevo México](index.php/homepage/generos/simulacion/item/416-scs-software-lanzara-nuevo-contenido-para-american-truck-simulator) para el hermano americano de ETS2 (American Truck Simulator), nos volvemos a emocionar con conducir nuestros camiones favoritos por los fantásticos paisajes de la "bota". Según palabras de SCS Soft, el juego contará con multitud de ciudades, carreteras, zonas verdes, de montaña, áridas, costeras... Las ciudades que actualmente dispone el juego base del norte de Italia serán remodeladas y ampliadas al igual que se hizo en su día con las del este de Francia con la DLC antes comentada.


 


![ETS2 BItalia01](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/ETS2-BItalia01.webp)


 


ETS2 y ATS son dos de los juegos más queridos por la comunidad linuxera, y SCS Software siempre a cuidado a su tremenda base de jugadores con el continuo desarrollo de jugosas actualizaciones gratuitas, DLC's, caracterisiticas nuevas y complementos en sus juegos, proporcionando una completa y amena experiencia. La expansión saldrá a final de este año, sin existir en este momento una fecha concreta. Desde JugandoEnLinux.com os informaremos como siempre de todo cuanto se cueza con respecto a esta expansión, así como de cualquier contenido que nos ofrezca SCS Software. Para que se os haga la boca agua os dejamos con el trailer previo de "Bella Italia":


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/LdXkngfzANA" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


¿Sois jugadores de los juegos de SCS Software? ¿Qué tal se os dan los camiones virtuales? Cuentanos lo que quieras en los comentarios o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).


 

