---
author: Pato
category: Editorial
date: 2017-05-09 15:35:32
excerpt: "<p>Los Chicos de Feral est\xE1n que no paran tras un par de meses en calma</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/f68bc1606a499c66a1eabd66e99d6817.webp
joomla_id: 311
joomla_url: el-radar-de-feral-interactive-vuelve-a-detectar-otro-lanzamiento-y-esta-vez-esta-muy-cerca
layout: post
tags:
- proximamente
- feral-interactive
title: "El radar de Feral Interactive vuelve a detectar otro lanzamiento y esta vez\
  \ est\xE1 muy cerca"
---
Los Chicos de Feral están que no paran tras un par de meses en calma

De nuevo tenemos noticias del radar de lanzamientos de Feral. 


Tras el [teaser de la semana pasada](index.php/homepage/editorial/item/427-feral-interactive-tiene-algo-entre-manos-publica-un-breve-teaser-en-twitter) y el [avistamiento del último "OFNI"](index.php/homepage/editorial/item/405-el-radar-de-feral-vuelve-a-detectar-un-nuevo-juego-no-identificado) (Objeto Feral No Identificado) esta vez la compañía que tantas alegrías nos está dando nos vuelve a poner un lanzamiento, y esta vez en el "anillo" de lanzamientos inminentes. En concreto, la imagen que puede verse en el radar es esta:


![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Feral/Feralteasermay17.webp)


Como podeis ver, parece ser una cara con una máscara y la boca pacere un pico abierto. La leyenda pone "Spice Clove". Puedes ver la imagen en la [web de Feral](https://www.feralinteractive.com/es/upcoming/).


¿Alguien tiene idea de qué puede ser? ¿Os atrevéis a adivinarlo? En Jugando en Linux estamos deseando saber vuestras teorías.


Podéis contarnoslas en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

