---
author: Serjor
category: Estrategia
date: 2017-05-19 15:41:23
excerpt: "<p>Feral Interactive nos trae la adaptaci\xF3n del \xFAltimo juego de la\
  \ franquicia</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/0243cbf1978673fe443d7558de6ab4f0.webp
joomla_id: 331
joomla_url: warhammer-4000-dawn-of-war-iii-tendra-version-para-linux
layout: post
tags:
- feral-interactive
- warhammer
- dawn-of-war
title: "Warhammer 40.000: Dawn of War III tendr\xE1 versi\xF3n para Linux (ACTUALIZACI\xD3\
  N Requisitos)"
---
Feral Interactive nos trae la adaptación del último juego de la franquicia

Ya están todas las cartas sobre la mesa y Feral Interactive ha mostrado por fin el último As que tenía en su radar, y es que gracias a [gamingonlinux.com](https://www.gamingonlinux.com/articles/warhammer-40000-dawn-of-war-iii-releasing-for-linux-on-june-8th.9691), nos enteramos de que hoy han anunciado, a través de este [minisite](https://www.feralinteractive.com/es/linux-games/dawnofwar3/about/), que el próximo 8 de junio estará disponible la tercera parte de la saga Dawn Of War.


Os recordamos que Feral ya se encargó de traernos el anterior juego de la saga, por lo que no es de extrañar que se hagan cargo de esta tercera parte, aunque la gran diferencia en esta ocasión es que el juego llega apenas unos meses después de su lanzamiento en Windows, y aunque a pesar de que lo ideal hubiera sido haber podido disfrutar del juego desde el primer día, nos alegra saber que SEGA sigue apostando por nuestra plataforma, y que lo hace en plazos mucho más que razonables.


Os dejamos con el vídeo presentación para comenzar con el hype.







 


Y tú, ¿estás dipuesto a declarar la guerra? Cuéntanoslo en los comentarios, o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 


**ACTUALIZACIÓN:** Tras confirmar en dias anteriores que el juego tendrá [soporte multijugador entre Linux y Mac](index.php/homepage/generos/estrategia/item/460-warhammer-40-000-dawn-of-war-iii-tendra-multijugador-entre-linux-y-macos), y que tendrá la [opción de ejecutarse con Vulkan](index.php/homepage/generos/estrategia/item/465-dawn-of-war-iii-ofrecera-soporte-para-vulkan-en-linux), nos llega ahora una noticia por parte de Feral Interactive de que harán una demostración previa al lanzamiento en [Twitch](https://www.twitch.tv/feralinteractive) el día 7 de Junio (el miércoles de la semana que viene) a las 18:00 (5 de la tarde Inglesa) donde nos enseñarán en directo las virtudes de su último proyecto justo un día antes del lanzamiento. Podeis ver el anuncio en este tweet:


 



> 
> Get ‘em, boys! [#FeralPlays](https://twitter.com/hashtag/FeralPlays?src=hash) Warhammer 40,000: Dawn of War III live on Linux.  
> 7th June, 6PM BST / 10AM PDT on Twitch: <https://t.co/cuzZ9AMVuq> [pic.twitter.com/ISrVgE9lBB](https://t.co/ISrVgE9lBB)
> 
> 
> — Feral Interactive (@feralgames) [June 2, 2017](https://twitter.com/feralgames/status/870573443917500417)



 


**ACTUALIZACIÓN 2:** Feral acaba de comunicar mediante twitter los requisitos para jugar :


 



> 
> Is your war machine combat-ready? Mac and Linux system requirements revealed for Warhammer 40,000: Dawn of War III – <https://t.co/zUkBGsxDLq> [pic.twitter.com/Mg52cRS252](https://t.co/Mg52cRS252)
> 
> 
> — Feral Interactive (@feralgames) [7 de junio de 2017](https://twitter.com/feralgames/status/872428206112034816)



 



> 
> **Requisitos mínimos del sistema Linux**   
>  Procesador: 3.4 GHz Intel i3-4130   
>  RAM: 8 GB   
>  Gráficas: 1 GB Nvidia 650Ti   
>  Sistema operativo Ubuntu 16.04   
>  Actualmente, las tarjetas gráficas AMD e Intel no son compatibles OFICIALMENTE con *Dawn of War III*.
> 
> 
> **Requisitos recomendados del sistema Linux**   
>  Procesador: 3.4GHz Intel i7-4770   
>  RAM: 8 GB   
>  Gráficas: 4 GB Nvidia 980Ti   
>  Sistema operativo Ubuntu 16.10   
>  Actualmente, las tarjetas gráficas AMD e Intel no son compatibles OFICIALMENTE con Dawn of War III.
> 
> 
> Vulkan (experimental): las GPUs de NVIDIA requieren la versión del controlador 381.22. **Las GPUs de AMD requieren la versión del controlador Mesa 17.1 or AMDGPU-PRO 17.10.**
> 
> 
> **SORPRESA!!!!:** Las GPUs de Intel requieren la versión del controlador Mesa 17.2-devel.
> 
> 
> 

