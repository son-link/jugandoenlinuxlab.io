---
author: Serjor
category: Editorial
date: 2017-02-10 20:05:32
excerpt: "<p>La lanzadera de juegos para nuevas desarrolladoras ser\xE1 sustituido\
  \ por un nuevo sistema llamado Steam Direct</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/d81dde7813bd99a2f6a5498a2ec932ec.webp
joomla_id: 216
joomla_url: valve-cerrara-greenlight
layout: post
tags:
- steam
title: "Valve cerrar\xE1 GreenLight"
---
La lanzadera de juegos para nuevas desarrolladoras será sustituido por un nuevo sistema llamado Steam Direct

Leemos en [gamingonlinux.com](https://www.gamingonlinux.com/articles/valve-set-to-replace-greenlight-with-steam-direct.9093) que Steam va a cerrar su servicio [GreenLight](http://steamcommunity.com/greenlight/) para convertirse en una distribuidora de videojuegos.


Según comentan en su [anuncio](http://steamcommunity.com/games/593110/announcements/detail/558846854614253751), a pesar de que en un comienzo GreenLight funcionó muy bien y se convirtió en una plataforma donde nuevos desarrolladores podían dar a conocer sus obras, creen que con este nuevo cambio pueden conseguir un sistema mejor. Este nuevo sistema, llamado "Steam Direct" y que pretenden que esté lista en algún momento de este 2017, funcionará de una manera un tanto diferente.


Para empezar todo aquél que quiera publicar un videojuego,  ya sea un particular o una empresa deberá darse de alta y rellenar una serie de documentación y para cada videojuego que quieran lanzar en Steam deberán pagar una entrada que más adelante recuperarán. El objetivo es que de esta manera, siendo más formales y poniendo una tasa recuperable, se cree un filtro natural entre aquellas personas que tienen una propuesta firme y quienes no.


Esperemos que este cambio consiga el que se supone que es el objetivo final para Steam, reducir el ruido que producen los juegos de baja calidad y conseguir una plataforma de distribución de mayor calidad.


 


¿Qué te parece este cambio? Cuéntanoslo en los comentarios, en el [foro](index.php/foro/) o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

