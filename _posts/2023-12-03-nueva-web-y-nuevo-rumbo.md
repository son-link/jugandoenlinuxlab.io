---
author: Odin
category: "Noticia"
date: 03-12-2023 18:45:00
excerpt: "Tenemos una nueva web y algo que queremos compartir contigo."
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/NuevaWeb/jel.webp
layout: post
tags:
  - noticias
title: "Nueva web y nuevo rumbo."
---

Parece mentira, pero el tiempo pasa, y lo hace realmente muy rápido. Ya han pasado más de siete años desde que [anunciamos]({% post_url 2016-08-25-creacion-jugandoenlinux-fundamentos %}) la llegada Jugando En Linux, de una nueva comunidad de habla hispana centrada en compartir contigo las novedades del mundo de los videojuegos en Linux. Desde los primeros
juegos que iban saliendo nativos para Linux en Steam, pasando por la revolución que fue Proton, hasta la increible
Steam Deck.

Estamos orgullosos de haber puesto nuestro granito de arena en el campo de los videojuegos de Linux. Lo que empezó como una pequeña comunidad de aficionados, en algo tan nicho como era los juegos de ordenador para Linux, ha ido creciendo en una comunidad de la que gracias a sus miembros han salido programas como [**ExtremeCooling4Linux**](https://odintdh.itch.io/extremecooling4linux), [**OverSteer**](https://github.com/berarma/oversteer), [**PyLinuxWheel**](https://odintdh.itch.io/pylinuxwheel) y hasta drivers como el desarrollado por **Bernat** [**new-lg4ff**](https://github.com/berarma/new-lg4ff).

Logicamente, para colaborar en **Jugando En Linux**, no hace falta ser programador, durante estos años hemos tenido
iniciativas como las **partidas multijugador de los viernes**, en el que cada semana probabamos un videojuego diferente. También llegamos a tener un **clan de 0Ad**, una liga de rallies con el inmejorable **Dirt Rally**, un mundial de **F1**, y como muchos, caímos en algunos juegos de moda como lo fue el **Rocket League**.

Durante este tiempo hemos hecho infinidad de artículos, entrevistas a estudios, hemos colaborado con desarrolladores indies, y seguramente habrás visto alguno de nuestros [vídeos](https://www.youtube.com/@jugandoenlinux) e incluso escuchado episodios de nuestro podcast.

Pero como deciamos al principio del artículo, el tiempo pasa, y lo hace para todos. Es por eso que hemos pensado que era necesario cambiar nuestra web a una más moderna y minimalista. Gracias a nuestro compañero **[Son Link](https://gitlab.com/son-link)**, estáis viendo la nueva versión que esperamos que os guste.

Además de la web, tenemos que deciros que vamos a dar un cambio de rumbo. Este cambio surge de varios motivos, que van, desafortunadamente, desde la falta de tiempo de nuestros colaboradores para poder actualizar con artículos diarios la web, hasta la forma de consumir contenidos online. Cada vez tienen menos peso los blogs siendo un mayor consumo a través de las redes sociales. De hecho, si os habéis fijado, cada vez hemos publicado más en nuestra cuenta de [Mastodon](https://mastodon.social/@jugandoenlinux) y menos en nuestra web.

Lo que antes era una web y una comunidad surgida entorno a ella, ahora ha cambiado para ser una **comunidad** de la que surgen iniciativas de todo tipo. Es por eso que esto no es un adiós, ni un hasta luego, esto es un _bienvenid@ de nuevo a vuestra comunidad, bienvenid@ a la nueva JugandoEnLinux_.
