---
author: leillo1975
category: Ofertas
date: 2019-05-09 18:18:00
excerpt: "<p>Podeis tambi\xE9n haceros con Age Of Wonders III completamente gratis</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/ec6196337b424054df1d55951c9bb0a7.webp
joomla_id: 1042
joomla_url: montones-de-ofertas-en-la-spring-sale-de-la-humble-store
layout: post
tags:
- humble-bundle
- gratis
- ofertas
- humble-store
- descuentos
title: Montones de ofertas en la Spring Sale de la Humble Store.
---
Podeis también haceros con Age Of Wonders III completamente gratis

Cuando nuestra cartera no se recuperó aun de las ofertas de Navidad e Invierno, ya están aquí de nuevo las de Primavera en la conocida tienda de [Humble Bundle](https://www.humblebundle.com/store). Para celebrarlo también **están regalando el Age of Wonders III**, un juegazo de estrategia por turnos, que podeis encontrar pinchando [en este enlace](https://www.humblebundle.com/store/age-of-wonders-iii?partner=jugandoenlinux). decir que Como solemos hacer cada vez que hay este tipo de descuentos multitudinarios, **os detallamos las que nos parecen más destacables**. Empezamos:


-[GRID Autosport](https://www.humblebundle.com/store/grid-autosport?partner=jugandoenlinux): 7.99€ (-80%)


-[XCOM 2](https://www.humblebundle.com/store/xcom-2?partner=jugandoenlinux): 12.49€ (-75%)


-[Aragami](https://www.humblebundle.com/store/aragami?partner=jugandoenlinux): 7.99€ (-60%)


-[Mount & Blade: Warband](https://www.humblebundle.com/store/mount-blade-warband?partner=jugandoenlinux): 7.99€ (-60%)


-[Civilization VI](https://www.humblebundle.com/store/sid-meiers-civilization-6?partner=jugandoenlinux): 17.99€ (-70%)


-[Life is Strange: Before the Storm Deluxe Edition](https://www.humblebundle.com/store/life-is-strange-before-the-storm-deluxe-edition?partner=jugandoenlinux): 7.49€ (-70%)


-[DIRT Rally](https://www.humblebundle.com/store/dirt-rally?partner=jugandoenlinux): 6.99€ (-80%)


-[Rise of the Tomb Raider: 20 Year Celebration](https://www.humblebundle.com/store/rise-of-the-tomb-raider-20-year-celebration?partner=jugandoenlinux): 9.99€ (-80%)


-[Victor Vran](https://www.humblebundle.com/store/victor-vran?partner=jugandoenlinux): 4.99€ (-75%)


-[Battletech](https://www.humblebundle.com/store/battletech?partner=jugandoenlinux): 23.99€ (-40%)


-[Deus Ex: Mankind Divided Digital Deluxe Edition](https://www.humblebundle.com/store/deus-ex-mankind-divided-digital-deluxe?partner=jugandoenlinux): 6.74€ (-85%)


-[Surviving Mars](https://www.humblebundle.com/store/surviving-mars?partner=jugandoenlinux): 17.99€ (-40%)


-[Borderlands 2 GOTY](https://www.humblebundle.com/store/borderlands-2-game-of-the-year?partner=jugandoenlinux): 9.89€ (-78%)


-[Europa Universalis IV](https://www.humblebundle.com/store/europa-universalis-iv?partner=jugandoenlinux): 9.99€ (-75%)


-[Everspace](https://www.humblebundle.com/store/everspace?partner=jugandoenlinux): 6.99€ (-75%)


Estos son solo algunos ejemplos, pero por supuesto que podeis dejarnos un comentario si encontrais otras tan destacables como estas. También os decimos como siempre cuando escribimos sobre las ofertas de Humble Bundle,  que los enlaces tienen el identificador de afiliado, por lo que **si comprais siguiéndolos nos estais ayudando a costear los gastos** de Hosting y dominio de JugandoEnLinux.com.

