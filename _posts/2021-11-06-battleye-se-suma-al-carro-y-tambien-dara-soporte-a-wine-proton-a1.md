---
author: Serjor
category: Software
date: 2021-11-06 09:22:00
excerpt: "<p>De esta manera m\xE1s del 50% del cat\xE1logo de Steam con anti-trampas\
  \ ser\xEDa jugable, sin necesidad de modificaciones por parte del desarrollador.</p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/BattlEye/batleye-wine.webp
joomla_id: 1367
joomla_url: battleye-se-suma-al-carro-y-tambien-dara-soporte-a-wine-proton-a1
layout: post
tags:
- steamos
- wine
- proton
- steam-deck
- battlye
- anti-cheat
- anti-trampas
title: "BattlEye se suma al carro y tambi\xE9n dar\xE1 soporte a Wine/Proton (ACTUALIZADO)"
---
De esta manera más del 50% del catálogo de Steam con anti-trampas sería jugable, sin necesidad de modificaciones por parte del desarrollador.


**ACTUALIZACIÓN 6/11/2021:** Valve acaba de actualizar más información detallada sobre la integración del sistema anti-trampas BattlEye junto con Proton/Wine y las noticias son mejores de lo que pensábamos en un principio:



> 
> El lanzamiento de Proton Experimental de esta noche, junto con Steam Client Beta, permite la integración en la que hemos estado trabajando con @TheBattlEye durante mucho tiempo. **Se puede activar sin necesidad de ningún trabajo o actualizaciones por parte del desarrollador del juego.**
> 
> 
> - [Pierre-Loup Griffais](https://twitter.com/Plagman2/status/1456792857960857604) 6 nov. 2021
> 
> 
> 


Es un paso gigante a la hora de facilitar la integración del famoso anti-trampas con Proton.


Según detalla en el [comunicado](https://store.steampowered.com/news/group/4145017/view/3104663180636096966) para desarrolladores, estos no tendrían que hacer ningún cambio en el código del juego, **simplemente han de comunicar a BattlEye que desean activar esta compatibilidad** y ellos se encargan de todo.


Ya hablan de dos títulos que **podremos disfrutar en Steam Deck: *Mount & Blade II: Bannerlord* y *ARK: Survival Evolved*** y que ya debería de estar funcionando para nuestro sistema haciendo uso de Steam Beta y Proton Experimental.


Sin lugar a dudas este paso es mucho mas ambicioso a la hora de traer los famosos juegos con anti-trampas al catalogo de la Steam Deck y por ende a Linux.


 




---


 


**ORIGINAL 25/09/2021:** Si este pasado 23 de diciembre los cimientos del juego en linux [tambalearon]({{ "/posts/easy-anti-cheat-lanza-su-soporte-completo-para-linux-incluyendo-wine-y-proton" | absolute_url }}), ayer llegó la esperada réplica, y es que el segundo gran jugador de los anti trampas, [BattlEye](https://www.battleye.com/), siguió la estela de Epic, anunciando que BattleEye también soportaría Wine y Proton:



> 
> BattlEye has provided native Linux and Mac support for a long time and we can announce that we will also support the upcoming Steam Deck (Proton). This will be done on an opt-in basis with game developers choosing whether they want to allow it or not.
> 
> 
> — BattlEye (@TheBattlEye) [September 24, 2021](https://twitter.com/TheBattlEye/status/1441477816311291906?ref_src=twsrc%5Etfw)



Al igual que en el caso de EAC, los desarrolladores serán quienes decidan en última instancia si dan soporte o no a sus juegos habilitándolo en la consola del desarrollador.


Esta noticia viene a confirmar lo que Valve anunció junto con el Steam Deck, que su promesa de que todos los juegos de la biblioteca de Steam serían jugables usando GNU/Linux venía acompañada de trabajo y acuerdos con los fabricantes de los anti trampas. No obstante, que sea una opción en manos de los desarrolladores no garantiza que se vaya a activar la casilla de "compatible con Wine/Protón", con lo que todavía existe el riesgo de que "todos los juegos se puedan ejecutar" se quede en "potencialmente todos los juegos serán jugables", pero sin garantías al depender de la voluntad de los desarrolladores (entre nosotros, salvo juegos sin soporte, todos van a hacer "click").


Y tú, ¿cómo ves todas estas noticias alrededor de los anti trampas? Cuéntanoslo en los comentarios o en los canales de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org) y [Telegram](https://t.me/jugandoenlinux).

