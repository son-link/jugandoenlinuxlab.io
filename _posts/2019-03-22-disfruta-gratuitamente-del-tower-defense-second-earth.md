---
author: leillo1975
category: Estrategia
date: 2019-03-22 15:19:42
excerpt: <p>El juego de&nbsp;<span class="st">@squidcor (</span> <span class="username
  u-dir" dir="ltr">@Free_Lives ) se vuelve a actualizar<br /></span></p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/4a5dadae8236179289a29d4a9cb30754.webp
joomla_id: 1004
joomla_url: disfruta-gratuitamente-del-tower-defense-second-earth
layout: post
tags:
- tower-defense
- estrategia
- unity3d
- desarrollo
- second-earth
- free-lives
title: Disfruta gratuitamente del tower-defense "Second Earth" (ACTUALIZADO 2)
---
El juego de @squidcor ( @Free_Lives ) se vuelve a actualizar  


**ACTUALIZACIÓN 2-10-19:** Llevábamos ya varios meses sin noticias sobre el desarrollo de este fantástico y **gratuito** tower defense, del que si haceis un poco de memoria [dedicamos un gameplay](https://youtu.be/SCCHP7z__AE) hace algún tiempo. En esta ocasión la larga espera ha merecido la pena y la nueva versión del juego, la Alpha 9,  [viene cargada de novedades de lo más interesantes](https://squidcor.itch.io/second-earth/devlog/102367/alpha-9-ui-overhaul-world-map-and-new-bugs). Según su desarrollador, [Robbie Frasier](https://twitter.com/squidcor), lo principal era arreglar muchas cosas pequeñas que hiciesen que el juego fuese una experiencia más completa y dejase de parecer un prototipo. Entre todos estos cambios podemos destacar:  
 


*-**Nuevos bichos** - Dos nuevos bichos han sido añadidos y todo el arte enemigo ha sido rehecho desde cero.*   
 *-**Revisión de la interfaz de usuario** - La interfaz de usuario ha sido completamente rediseñada, pulida y ampliada.*  
 *-**Tutoriales** - Las dos primeras misiones del juego ahora incluyen sistemas de tutoriales más detallados.*  
 *-**Mapa del mundo** - Se ha añadido un mapa del mundo con un meta-juego. Ahora mismo está bastante desnudo, ¡pero tenemos grandes planes!*  
 *-**Sistema de población obrera** - Los edificios ahora requieren que los trabajadores operen. Los trabajadores se mudarán a sus casas si usted satisface sus necesidades.*  
 *-**Pausa** - El juego ya no se detiene después de cada ola. En cambio, puedes hacer una pausa cuando quieras, siempre y cuando no haya una ola atacando en ese momento.*  
 *-**Nuevo sistema de auto-construcción** - La forma en que se construyen granjas, casas y minas ha sido racionalizada (y esperamos que sea más interesante)*


Todo esto y mucho más ha sido incluido en el juego. Desde JugandoEnLinux os recomendamos que os lo descargueis y lo disfruteis, y si veis algo que mejorar, que lo comenteis en su [página de itch.io](https://squidcor.itch.io/second-earth/devlog/102367/alpha-9-ui-overhaul-world-map-and-new-bugs).


![SecondEarthA9](/https://img.itch.zone/aW1hZ2UvMzY3NTA5LzI1MzgxMDEuanBn/original/7igY9i.webp)




---


**ACTUALIZACIÓN 27-5-19:** Este interesante título adquiere nuevas e importantes características tal y como podeis ver en el tweet de Robbie Frasier ([@**Squidcor**](https://twitter.com/Squidcor)):



> 
> New update to our base builder is up for download on itch:<https://t.co/RACybhUGUI>  
>   
> Now including laser drills and giant bugs![#screenshotsaturday](https://twitter.com/hashtag/screenshotsaturday?src=hash&ref_src=twsrc%5Etfw) [#gamedev](https://twitter.com/hashtag/gamedev?src=hash&ref_src=twsrc%5Etfw) [#indiedev](https://twitter.com/hashtag/indiedev?src=hash&ref_src=twsrc%5Etfw) [#madewithunity](https://twitter.com/hashtag/madewithunity?src=hash&ref_src=twsrc%5Etfw) [#indiegame](https://twitter.com/hashtag/indiegame?src=hash&ref_src=twsrc%5Etfw) [#games](https://twitter.com/hashtag/games?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/8mixwAMyUx](https://t.co/8mixwAMyUx)
> 
> 
> — Robbie Fraser (@Squidcor) [May 25, 2019](https://twitter.com/Squidcor/status/1132339409083359232?ref_src=twsrc%5Etfw)


  





Segun podemos ver en la [página del juego en Itch.io](https://squidcor.itch.io/second-earth/devlog/82651/new-build-lasers-and-a-new-enemy), las novedades que presenta son numerosas y destaca la inclusión de un nuevo alienígena enorme y acorazado, laseres, cañones de mágma y 6 niveles más. Por supuesto hay montones de mejoras en el terreno de la jugabilidad y la corrección de bugs.


![ScondEarth01](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SecondEarth/ScondEarth01.webp)


 




---


**NOTICIA ORIGINAL:** 


Gracias a la información facilitada en la magnífica web [DesdeLinux](https://blog.desdelinux.net/second-earth-juego-de-construccion-con-build-para-linux/), nos hemos enterado de la existencia de este juego de tipo **Tower-Defense con muchos toques de estrategia**. El juego está desarrollado por la misma compañía que creó el magnífico [BroForce](https://www.humblebundle.com/store/broforce?partner=jugandoenlinux), los sudafricanos [Free Lives](https://freelives.net), autores también de [Genital Jousting](https://www.humblebundle.com/store/genital-jousting?partner=jugandoenlinux) y [Gorn](https://www.humblebundle.com/store/gorn?partner=jugandoenlinux).


El juego se encuentra en una **fase muy temprana de su desarrollo**, pero ya presenta buenas maneras y se hace muy ameno de jugar. En el debemos defender nuestras colonias en Proxima Centauri B de las **hordas de Aliens que en repetidas oleadas intentarán llevar al traste todos nuestros esfuerzos**. Tendremos que recolectar y gestionar bien nuestros recursos a través de 11 fases, para crear defensas que eviten el desastre.


El juego está desarrollado con el conocido y versátil **motor Unity 3D**, podemos encontrarlo en [Itch.io](https://itch.io/), donde en este momento está [disponible para descargar gratuitamente](https://squidcor.itch.io/second-earth) en nuestro sistema operativo, así como también en Windows y MacOS. Nosotros desde JugandoEnLinux seguiremos con Lupa este interesante desarrollo y os ofreceremos cumplida información de sus actualizaciones. Os dejamos con un gameplay del juego:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/hRLfGXqW1lE" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


 ¿Os gustan los Tower- Defense? ¿Qué opinión os merece este "Second Earth"? ¿Le veis futuro? Contéstanos en los comentarios o debate con nosotros en nuestro canal de [Telegram](https://t.me/jugandoenlinux).

