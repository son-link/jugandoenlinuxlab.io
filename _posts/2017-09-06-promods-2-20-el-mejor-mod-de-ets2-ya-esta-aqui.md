---
author: leillo1975
category: "Simulaci\xF3n"
date: 2017-09-06 08:00:40
excerpt: "<p>El equipo de Mandelsoft lo ha vuelto a hacer: m\xE1s paises, m\xE1s carreteras,\
  \ m\xE1s paisajes.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/c53a9eb7f98bd9efe52407712806bfee.webp
joomla_id: 453
joomla_url: promods-2-20-el-mejor-mod-de-ets2-ya-esta-aqui
layout: post
tags:
- ets2
- scs-software
- euro-truck-simulator-2
- mod
- mandelsoft
- promods
title: "ProMods 2.20, el mejor MOD de ETS2 ya est\xE1 aqu\xED."
---
El equipo de Mandelsoft lo ha vuelto a hacer: más paises, más carreteras, más paisajes.

No es ningún secreto que siento adoración por los juegos de [SCSSoft](index.php/component/search/?searchword=scs&searchphrase=all&Itemid=311), y más concretamente por [Euro Truck Simulator 2](index.php/top-linux/item/94-euro-truck-simulator-2). Si hay un juego del que nunca me canso es este, ya que son años jugándolo. En mi caso no puedo poner límite al número de horas que puedo dedicar a recorrer carreteras, paisajes, ciudades... Cuando conduzco estas moles virtualmente, me invade una sensación de tranquilidad que creo que no tiene punto de comparación con otros juegos. Más de una vez he hablado sobre estos juegos y siempre coincido en los mismos aspectos, pero sentarte en tu sillón con el volante y pasar una "aburrida" tarde de domingo de paseo por Europa no tiene precio. Supongo que todo esto tiene que ver con ciertas cosas arraigadas en mi familia y mi niñez.


 


Si la experiencia que ofrece el juego base con sus expansiones es algo genial, añadir a esto **ProMods** es algo sin parangón. Y es que no solo añade más paises al ya de por si extenso mapa de ETS2, si no que lo hace con una calidad y buen hacer solo comparables al equipo de modeladores de la propia SCSSoft, algo que me anima a decir sin ningún tipo de duda que este siempre fué y es el mejor mod que vamos a encontrar para el juego.


 


![Promods220 01](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Promods220-01.webp)


 


Para quien no conozca ProMods, como decía se trata de un añadido desarrollado por el grupo de modders **[MandelSoft](https://www.youtube.com/user/MandelSoft)** que añade la posibilidad de circular por una **red de carreteras mucho más extensa incluyendo más paises** como Islandia, Irlanda, Andorra, Letonia, Estonia, Rumanía, entre otros muchos. Por supuesto, en este momento, también tenemos la posibilidad de visitar varias ciudades del norte del estado español como Bilbao, Donosti, Irún, Huesca, Barcelona..., aunque gracias al trabajo de *IvanGF* y *Realdeal* cada vez serán más hasta completar la peninsula ibérica.


 


Pero no solo se trata de añadir más mapa, sinó que el mod también **tiene muchisimas áreas rediseñadas** y con muchas más ciudades y carreteras, como es el caso de Alemania, Polonia o Bélgica. Encontraremos también a nuestro paso **paisajes y monumentos muy reconocibles** que enriquecerán nuestra experiencia. Una cualidad muy importante de este mapa es que también permite enlazar carreteras con otro gran mod como es [RusMap](http://www.forosimuladores.com/viewtopic.php?f=93&t=14261), donde podremos unir además paises como Bielorusia y Rusia al ya de por si extenso conjunto de paises incluidos.


 


Para poder disfrutar de ProMods 2.20 vais a necesitar tener instalado ETS2 a la última versión (1.28) y disponer de las muy recomendables expansiones [**Going East**](http://store.steampowered.com/app/227310/Euro_Truck_Simulator_2__Going_East/), [**Scandinavia**](http://store.steampowered.com/app/304212/Euro_Truck_Simulator_2__Scandinavia/) y **[Vive la France!](http://store.steampowered.com/app/531130/Euro_Truck_Simulator_2__Vive_la_France/)** (del que en su día hicimos un [completo análisis](index.php/homepage/analisis/item/243-analisis-ets2-vive-la-france-dlc)).


 


Os dejo con el trailer (nunca mejor dicho) de ProMods 2.20:


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/UTFpc_ISSCw" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


Si quereis más información sobre las novedades de este mod podeis ver la lista de cambios en [este enlace](https://blog.promods.net/2017/09/promods-2-20-is-available/). Para descargarlo podeis hacerlo **[desde aquí](https://blog.promods.net/2017/09/promods-2-20-is-available/)**. En esta última dirección vais a encontrar un asístente donde configurareis el MOD y podreis descargar sus archivos. Teneis dos opciones, descargar el mod en multiples ficheros de forma gratuita pero con velocidad limitada; o en un solo archivo y en servidores rápidos pagando 1$, lo cual os recomiendo, no solo por comodidad, sino porque de esa forma ayudais a los modders.


 


ACTUALIZACIÓN: Acabamos de grabar un video donde hacemos un pequeño viaje de Marsella a Lleida donde podemos ver algunas de las novedades de Promods 2.20:


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/ZtheSRiJOLI" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


¿Habeis probado ProMods? ¿Que otros mods teneis añadidos a ETS2? Deja tus respuestas en los comentarios o en nuestro canal de [Telegram](https://t.me/jugandoenlinux).

