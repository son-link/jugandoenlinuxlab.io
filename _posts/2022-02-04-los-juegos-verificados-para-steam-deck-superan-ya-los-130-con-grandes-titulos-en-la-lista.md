---
author: Pato
category: Hardware
date: 2022-02-04 21:57:29
excerpt: "<p>En los pr\xF3ximos d\xEDas y semanas se espera que la lista sea mucho\
  \ mayor</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Steamdeck/SteamDeckVerified.webp
joomla_id: 1423
joomla_url: los-juegos-verificados-para-steam-deck-superan-ya-los-130-con-grandes-titulos-en-la-lista
layout: post
tags:
- steamos
- steamdb
- proton
- steam-deck
title: "Los juegos verificados para Steam Deck superan ya los 130, con grandes t\xED\
  tulos en la lista"
---
En los próximos días y semanas se espera que la lista sea mucho mayor


A menos de un mes para la llegada de Steam Deck, Valve va a marchas forzadas puliendo todo lo que respecta al lanzamiento de la portátil. Uno de los puntos clave es la verificación de los juegos para asegurarse de que cuando la máquina llegue a los usuarios estos tengan toda la información posible sobre la compatibilidad de cada juego con esta.


Es por esto que Valve **se encuentra verificando internamente todo su catálogo** para ofrecer un [sistema de verificación]({{ "/posts/valve-lanza-un-programa-de-verificacion-de-juegos-para-steam-deck" | absolute_url }}) mediante un proceso automatizado que ya ha empezado a [desvelar los primeros juegos]({{ "/posts/novedades-steam-deck-la-ultraportatil-de-valve-suma-titulos-y-facilita-el-soporte-anti-trampas" | absolute_url }}) "Deck Verified". Con el paso de los días, este listado ha comenzado a crecer, ofreciendo ya más de 130 títulos (hablamos de los verificados, si sumamos los que tienen verificado cierto nivel de jugabilidad la lista sube hasta los 248 títulos ahora mismo) incluyendo algunas sorpresas y títulos triple A. A modo de selección, aquí tienes unos cuantos de los que más nos han llamado la atención:


**Horizon Zero Dawn™ Complete Edition**:


BOOM! empezamos fuerte!. Uno de los últimos HITS de Guerrilla y Playstation verificado para Steam Deck!.Vive la misión legendaria de Aloy para desvelar los secretos de una futura Tierra dominada por máquinas. ¡Usa devastadores ataques contra tus depredadores y explora un majestuoso mundo abierto en este galardonado RPG de acción!


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/76O5KaJHEA0" title="YouTube video player" width="560"></iframe></div>


[Enlace a su página de Steam](https://store.steampowered.com/app/1151640/Horizon_Zero_Dawn_Complete_Edition/)


**Hades**:


El juego de Supergiant Games es un muy recomendable "Hack and Slash" de los creadores de [Pyre](https://store.steampowered.com/app/462770/Pyre/), [Transistor](https://store.steampowered.com/app/237930/Transistor/) y [Bastion](https://store.steampowered.com/app/107100/Bastion/) que nunca ha llegado a salir para Linux. Ahora está verificado para Steam Deck. Desafía al dios de los muertos y protagoniza una salvaje fuga del Inframundo en este juego de exploración de mazmorras:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/DU_FmvgwPAU" title="YouTube video player" width="560"></iframe></div>


[Enlace a su página en Steam](https://store.steampowered.com/app/1145360/Hades/). 


 


**Dishonored**:


El juego de Arkane Studios junto a Bethesda es un juego de acción en primera persona al que te entregarás por completo encarnando a un asesino sobrenatural movido por la sed de venganza. Un sistema de combate flexible te permitirá combinar una gran variedad de poderes sobrenaturales, armas y enrevesados artilugios para eliminar a tus enemigos desplegando toda tu creatividad:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/-XbQgdSlsd0" title="YouTube video player" width="560"></iframe></div>


 


[Enlace a su página de Steam](https://store.steampowered.com/app/205100/Dishonored/)


 


**Risk of Rain 2**:


En este juego editado por Gearbox tendrás que luchar contra hordas de monstruos enloquecidos junto a tus amigos o en solitario para lograr escapar de un planeta alienígena sumido en el caos. Combina el botín de maneras asombrosas y domina cada personaje hasta encarnar la destrucción que tanto te aterraba tras tu primer aterrizaje forzoso.


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/Qwgq_9EOCTg" title="YouTube video player" width="560"></iframe></div>


[Enlace a su página en Steam](https://store.steampowered.com/app/632360/Risk_of_Rain_2/)


 


**God of War**:


Si, muchos lo esperaban, y ya está confirmado para Steam Deck el último hit de Sony Santa Monica. En esta aventura de acción Kratos ha dejado atrás su venganza contra los dioses del Olimpo y vive ahora como un hombre en los dominios de los dioses y monstruos nórdicos. En este mundo cruel e implacable debe luchar para sobrevivir… y enseñar a su hijo a hacerlo también.


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/RQK_40a0XUY" title="YouTube video player" width="560"></iframe></div>


[Enlace a su página en Steam]("https://store.steampowered.com/app/1593500/God_of_War/)


 


**Hellblade: Senua's Sacrifice**:


De los creadores de Heavenly Sword, Enslaved: Odyssey to the West y DmC: Devil May Cry llega uno de los grandes títulos de la generación. Embárcate en el viaje brutal de una guerrera hacia el mito y la locura. Ambientado en la era vikinga, una guerrera celta destrozada se embarca en una misión onírica obsesiva para luchar por el alma de su difunto amante.


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/H2o9KRJiOwE" title="YouTube video player" width="560"></iframe></div>


[Enlace a su página en Steam](https://store.steampowered.com/app/414340/Hellblade_Senuas_Sacrifice/) 


 


**LEGO® Star Wars™ - The Complete Saga**:


Uno de los más icónicos juegos de Lego junto a LucasArts (y que portó Feral Interactive a Mac, pero no a Linux) llega para darle caña a los Bloques desde el I hasta el VI! ¡Juega a lo largo de las seis películas de Star Wars en un solo videojuego! Con nuevos personajes, nuevos niveles, nuevas características y, por primera vez, ¡la oportunidad de construir y luchar a tu manera en una divertida galaxia de Star Wars en tu PC!


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/B9LqxgKGp6A" title="YouTube video player" width="560"></iframe></div>


[Enlace a su página en Steam](https://store.steampowered.com/app/32440/LEGO_Star_Wars__The_Complete_Saga/)


 


**Sekiro™: Shadows Die Twice - GOTY Edition**:


La última y galardonada joya de From Software, creadores de la saga Dark Souls y Bloodborne llega para introducirnos en la era Sengoku de finales del siglo XVI, un brutal periodo de constante conflicto, mientras te enfrentas a inconmensurables enemigos en un mundo oscuro y tortuoso. Despliega un arsenal de instrumentos protésicos letales y poderosas habilidades ninja, al mismo tiempo que combinas el sigilo, la verticalidad y transversalidad, y los viscerales combates cara a cara en una sangrienta confrontación.


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/a2IYJRXjCHY" title="YouTube video player" width="560"></iframe></div>


[Enlace a su página en Steam](https://store.steampowered.com/app/814380/Sekiro_Shadows_Die_Twice__GOTY_Edition/)


 


**METAL GEAR RISING: REVENGEANCE**:


El spin off  de la saga Metal Gear de la mano de Kojima y Konami nos mete en una aventura repleta de acción con todo el estilo Kojima productions:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/YeK9ZG7mc1w" title="YouTube video player" width="560"></iframe></div>


[Enlace a su página en Steam](https://store.steampowered.com/app/235460/METAL_GEAR_RISING_REVENGEANCE) 


 


**DARK SOULS™ III**:


Otro título de From Software dentro del mundo que redefinió un género. Una saga de juegos con una dificultad endiablada con un atractivo mundo oscuro de trasfondo: (Por cierto, hay más Dark Souls en la lista de verificados)


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/2HIxuGi_2TQ" title="YouTube video player" width="560"></iframe></div>


 [Enlace a su página en Steam](https://store.steampowered.com/app/374320/DARK_SOULS_III/)


 


**Tetris® Effect: Connected**:


Tetris, con efectos, multijugador. VR opcional. Y en formato Steam Deck se ajusta como un guante. Tetris. ¿Necesita más presentaciones?


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/glZpE3Gib1w" title="YouTube video player" width="560"></iframe></div>


[Enlace a su página de Steam](https://store.steampowered.com/app/1003590/Tetris_Effect_Connected/)


 


**FINAL FANTASY IV**:


Aunque no es la última entrega de la saga en PC, esta es la última que hay verificada pero hay más en la lista. Disfruta de una historia clásica narrada a través de unos gráficos retro fascinantes. Toda la magia del original con una jugabilidad mejorada.


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/9kNHVVz_ThE" title="YouTube video player" width="560"></iframe></div>


[Enlace a su página de Steam](https://store.steampowered.com/app/1173800/FINAL_FANTASY_IV/)


 


**DEATHLOOP:**


Bethesda nos trae un shooter en primera persona de nueva generación desarrollado por Arkane Lyon, el galardonado estudio creador de Dishonored. Dos asesinos rivales se ven atrapados en un bucle temporal en la misteriosa isla de Blackreef, condenados a repetir una y otra vez el mismo día.


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/QKVqwEsw4Uo" title="YouTube video player" width="560"></iframe></div>


[Enlace a su página de Steam](https://store.steampowered.com/app/1252330/DEATHLOOP/)


 


**Rocket League®**:


Sorpresa!. Rocket League está en la lista de verificados!. Después de dejar tirada la versión Linux hace unos años al pasarse a la EGS, ahora nos vuelve desde Steam, imaginamos que con la versión Windows bajo Proton. ¡El fútbol se une a los coches una vez más en la esperada continuación basada en la física del querido juego clásico en arenas Supersonic Acrobatic Rocket-Powered Battle-Cars!


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/OmMF9EDbmQQ" title="YouTube video player" width="560"></iframe></div>


[Enlace a su página de Steam](https://store.steampowered.com/app/252950/Rocket_League/)


 


**Bayonetta**:


El (¿ya clásico?) juego de Platinum Games nos trae el icónico juego de la bruja más sexy. Un Hack and Slash en toda regla. Experimenta su insuperable acción en 60 fps en resoluciones desbloqueadas en HD. El estilo de juego definitivo: ser malos nunca estuvo tan bien.


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/0Hyw2h7VX8Y" title="YouTube video player" width="560"></iframe></div>


[Enlace a su página de Steam](https://store.steampowered.com/app/460790/Bayonetta/)


 


**DEATH STRANDING**:


Otro juego Kojima. Death Stranding ya sabíamos que funcionaba bien bajo Proton, y su verificación nos confirma que el juego funcionará en la Steam Deck sin mayores problemas. Sam Bridges debe enfrentarse a un mundo transformado por el Death Stranding. Con los restos desconectados de nuestro futuro en sus manos, se embarca en un viaje para volver a conectar paso a paso el mundo destrozado.


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/dftGPBPXTXA" title="YouTube video player" width="560"></iframe></div>


[Enlace a su página de Steam](https://store.steampowered.com/app/1190460/DEATH_STRANDING/)


 


Esta es nuestra (¿pequeña?) selección, pero hay muchos más juegos en la lista de verificados. Puedes verla en [SteamDB en este enlace](https://steamdb.info/instantsearch/?page=3&refinementList%5Boslist%5D%5B0%5D=Steam%20Deck%20Verified).


¿Qué te parecen los verificados de Steam Deck? ¿Tienes interés en alguno en particular?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de Telegram o Matrix.

