---
author: Pato
category: "Acci\xF3n"
date: 2018-01-12 11:50:44
excerpt: "<p>El juego llegar\xE1 a Linux/SteamOS aunque no han confirmado oficialmente\
  \ si tambi\xE9n tendremos acceso anticipado para nuestro sistema</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/1371abab3c4aabc3692f4afcfda574f7.webp
joomla_id: 601
joomla_url: battalion-1944-estrenara-acceso-anticipado-el-proximo-1-de-febrero
layout: post
tags:
- accion
- proximamente
- acceso-anticipado
- fps
- mmo
- ii-guerra-mundial
title: "'Battalion 1944' un juego multijugador ambientado en la IIGM estrenar\xE1\
  \ acceso anticipado el pr\xF3ximo 1 de Febrero"
---
El juego llegará a Linux/SteamOS aunque no han confirmado oficialmente si también tendremos acceso anticipado para nuestro sistema

Si eres de los que echan de menos luchar en la II Guerra Mundial al estilo "Call of Duty" ahora puedes comenzar a preparar el petate, por que parece que 'Battalion 1944' viene al rescate en un género que en Linux al menos ha estado estancado durante mucho tiempo.


'Battalion 1944' [[web oficial](http://battaliongame.com/)] es un FPS ambientado precísamente en la II Guerra Mundial y donde tendrán lugar enfrentamientos de infantería competitivos 5vs5, y con la premisa de unos gráficos y fluidez de juego a la altura de los tiempos que corren.


*'Battalion 1944' recupera el espíritu de los shooters competitivos clásicos y refina la experiencia para la nueva generación. Enfocado en el combate de infantería 5vs5 donde los disparos certeros cuentan con tu Kar98, tu Thompson y un movimiento fluido son la clave de cada ronda de tu equipo. Lucha junto a tu equipo en una experiencia FPS premium competitiva en la WWII."*


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/7wQ9dkqxF9Y" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 El juego tendrá servidor dedicado para poder "hostear" y **soportará partidas LAN**, mods de la comunidad, tendrá rankings por equipos y mucho más, y los desarrolladores [han anunciado](http://steamcommunity.com/gid/[g:1:26062409]/announcements/detail/1614971408319748272) que el juego llegará oficialmente a acceso anticipado el próximo día 1 de Febrero y aunque no han especificado oficialmente si también estará disponible para Linux/SteamOS, lo que si parece cierto es que el juego llegará a nuestro sistema próximamente.


Desde Jugando en Linux estaremos atentos y os informaremos tanto si el juego llega oficialmente en acceso anticipado como cuando llegue de manera oficial a nuestro sistema favorito.


Si quieres saber más sobre 'Battalion 1944', puedes verlo en su página de Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/489940/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 

