---
author: leillo1975
category: Carreras
date: 2020-03-20 10:36:18
excerpt: "<p><span class=\"css-901oao css-16my406 r-1qd0xha r-ad9z0x r-bcqeeo r-qvutc0\"\
  >@Gen90Software nos hace un resumen de las novedades en las que est\xE1n trabajando.&nbsp;\
  \ </span></p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/PocketCars/PocketCars01_copy.webp
joomla_id: 1193
joomla_url: pocketcars-llega-a-linux-en-acceso-anticipado-a4
layout: post
tags:
- carreras
- acceso-anticipado
- unity3d
- gen90-software
- pocketcars
title: PocketCars llega a Linux en Acceso Anticipado (ACTUALIZADO 4)
---
@Gen90Software nos hace un resumen de las novedades en las que están trabajando.  


**ACTUALIZADO 14-1-21:** Ya han pasado unos cuantos meses sin noticias de los chicos de [Gen90 Software](https://gen90software.com/) , pero finalmente han publicado la versión 0.74 de su juego, que continua por supuesto en acceso anticipado. Las novedades son de mucha importancia, y seguro que todos los que poseais este juego agradecereis. El principal cambio con respecto a la anterior versión es la **inclusión del apartado Multijugador,** pues el equipo de desarrollo a empezado a dar forma a este importante modo de juego, y ahora es posible crear y unirse a los servidores, usar matchmaking, seleccionar tu región preferida, jugar a un evento de carrera, usar todas las armas / coches / neumáticos, votar por el reinicio, seleccionar el siguiente evento, y algo muy importante para nostros, la interoperabilidad entre los jugadores de Windows y nuestro querido sistema GNU/Linux. Queda pendiente para un futuro próximo el crear una sala privada e invitar a un amigo de Steam, modo de juego de Arena, objetos físicos sincronizados, lista de reproducción de eventos y votar para el próximo evento.


También se han hecho **cambios en la apariencia del juego**, con un nuevo aspecto en el juego y de la tienda.Se ha ampliado el número de pistas de música del juego a 10. También y gracias a la comunidad, el juego tiene un nuevo logo. Finalmente [el juego ya tiene sus correspondientes salas en Discord](https://discord.gg/czkZA59dCd), por lo que si quereis charlar sobre el, estar al tanto de su desarrollo o reportar problemas podeis hacerlo por este medio.


![PocketKArs](/https://steamcdn-a.akamaihd.net/steamcommunity/publichttps://gitlab.com/jugandoenlinux/imagenes/-/raw/main/clans/36217081/48044ff8e20ae3f062f02ceb6db02995d1a19afc.webp)


Otros pequeños cambios introducidos en el juego son los siguientes:


-La hierba 3D puede ser desactivada en los escenariosArreglada que la entrada del controlador repita problemas cuando se usa el menú con el joystick  
-Arregladas las sombras en el menú del puerto  
-Arreglado que el coche se escape después de seleccionar (Menú principal / Garaje)  
-Arreglado que el creador de eventos personalizados recuerde tus últimas selecciones  
-Arreglada la optimización de luces en el salón occidental


Por último, también nos comunican cuales serán los próximos pasos a dar en el proceso de desarollo del juego durante el periodo de Early Access. Basándose en el feedback que han recogido de los usuarios mejorarán el multijugador y prepararán el juego para las traducciones de idiomas. La hierba 3D se renovará pronto. Están probando una solución mejor y optimizada que funciona también en Linux.


 




---


**ACTUALIZADO 18-8-20**: Debido a que llevan un tiempo sin hacer lanzar ninguna noticia ni actualización, desde [Gen90 Software](https://gen90software.com/) han querido aclarar que **el desarrollo no se ha detenido ni mucho menos**, sino que continua a buen ritmo, tal y como podemos ver en la [noticia](https://store.steampowered.com/newshub/app/1170720/view/2728571481164320148) publicada en Steam. **Sus trabajos en el juego se estan centrando en diversas areas:**


**Estabilidad:** En la ultima actualización, la [0.67](https://store.steampowered.com/newshub/app/1170720/view/2577698447812106223) han hecho numerosas correcciones que han arrojado unos resultados muy positivos, habiendo muchos menos reportes de errrores y cierres, por lo que consideran que **el estado actual del juego es un buen punto de partida para mejoras de más calado**.


**Personalización:** Dentro de muy poco podremos disfrutar del añadido de una de sus características más ambiciosas, la **personalización de los vehículos**, pudiendo modificar los materiales y colores de la carrocería, llantas, antena y muelles. También nos preguntan que nos parecería incluir en el juego una **herramienta para la creación de "skins" que permita su edición y compartición con otros usuarios** a través del **Workshop** (Menuda pregunta, ¡por supuesto que si!)


![editor](/https://cdn.cloudflare.steamstatic.com/steamcommunity/publichttps://gitlab.com/jugandoenlinux/imagenes/-/raw/main/clans/36217081/748b6663872de07e2317dbaa6bd1c4da3fafff94.webp)


**Nuevos Mapas:** Pronto podremos disfrutar de **dos nuevas localizaciones**. La primera, **el museo** nos permitirá disfrutar de un marco diferente al los actuales, al ser completamente "indoor", con varios niveles de altura, y superficies lisas. El segundo mapa serán **las ruinas**, y contrariamente será un escenario "offroad" en un entorno rocoso a la orilla del mar.


![museo](/https://cdn.cloudflare.steamstatic.com/steamcommunity/publichttps://gitlab.com/jugandoenlinux/imagenes/-/raw/main/clans/36217081/540bfeb54e3b5f0c710ef3fc46996761909244c5.webp)


**Multijugador:** Por supuesto, esta es la opción más demandada, pues obviamente, en un juego tan divertido como este, sería un completo desproposito no disponer de un modo online que nos permita disfrutar de sus freneticas carreras. Los desarrollares comentan que **están haciendo los primeros tests a este modo**. Nos informarán de esta caracteristica y el resto en un futuro proximo en **discord**.


En JugandoEnLinux os iremos informando de la llegada de estas mejoras tan pronto como tengamos conocimiento de ellas, y por supuesto intentaremos acompañarlas de algún video para que podais verlas de primera mano. Permaneced atentos a nuestras redes sociales de [Twitter](https://twitter.com/jugandoenlinux) y [Mastodon](https://mastodon.social/@jugandoenlinux) para estar al tanto de noticias sobres este y otros juegos.




---


**ACTUALIZADO 19-6-20**: Hacía tiempo que no os hablábamos de este fantástico juego de coches de Radio Control, y es que **desde la última vez se han sucedido varias actualizaciones que han añadido notables características al juego**, como por ejemplo el nuevo modo "Stunt Arena" o la posiblidad de convertirlo en un "Simulador" pudiendo retocar las asistencias ([0.64](https://store.steampowered.com/newshub/app/1170720/view/2211777006209151436)). Pero en esta ocasión vamos a hablar de la ultimísima que ha recibido el juego, la [versión 0.66](https://store.steampowered.com/newshub/app/1170720/view/2453847373787459845), que **añade nuevos coches tipo "truck"**, además de otros cambios:


-Añadido: Muestra aviso de bloqueo si no tiene el coche / neumático / arma adecuada para el evento seleccionado  
-Agregado: Muestra desafíos con el sistema de unidad seleccionado (m/s, km/h, mph)  
-Añadido: Nivel de dificultad más fácil para los eventos de la temporada  
-Añadido: Hojas caídas en 3D en el mapa del vecindario  
-Actualizado: Texturas AO de coches  
-Actualizado: Amortiguadores para automóviles (visual y optimización)  
-Corregido: Error de inicialización al cargar (sólo en Windows)---> este nos da igual...JEJEJEJE


Os dejamos con un video que creamos para mostraros las novedades de las que os hablamos en este artículo. Seguimos atentos....


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/RrjLyPuT41o" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>




---


**ACTUALIZADO 11-4-20**: Ayer recibíamos el aviso desde Steam de que este fantástico juego de carreras de coches de Radio Control recibía una nueva actualización. En esta ocasión [los cambios son importantes](https://store.steampowered.com/newshub/app/1170720/view/2197135853632355957) tal y como podemos ver en la siguiente lista:


*- Se añade la **pantalla dividida**, pudiendo competir contra otro jugador en el mismo PC. Más adelante se permitirá jugar hasta 4 personas en local.*


*- Habilitación de los **eventos personalizados**, de forma que podremos crear nuestra carrera a medida seleccionando las opciones que queramos.*


*- Se permite **cambiar el tipo de música** del juego por el que nosotros queramos, pudiendo poner Rock, Pop de los 90' , o quizás tecno.*


*- Mejoras en la protección de la pared de la cámara.*


*- Mejoras de calidad de textura (puerto)*


*- Reparar colisionadores de ruedas de vehículos (puerto)*


*- Correcciones de ruta de AI (puerto)*


Nosotros vamos a echarnos una partida para disfrutar de esta actualización. Seguiremos pendientes para informaros.




---


**NOTICIA ORIGINAL:**


 La verdad es que no nos esperábamos que un juego con estas características arribase en nuestro sistema, pero gracias una vez más a la gran web que es [GamingOnLinux,](https://www.gamingonlinux.com/articles/16252) nos llega la información de que el juego está disponible en nuestro sistema en Acceso Anticipado, por lo que desde ya [podemos adquirirlo en Steam](https://store.steampowered.com/app/1170720/PocketCars/) por un precio más que recomendable, **11.59€**, con lo que ayudaremos a financiar el desarrollo de este título.


PocketCars es como su nombre indica un juego de **frenéticas carreras de coches teledirigidos** (RC, Radio Control), que indudablemente nos recuerda a los más viejos al clásico  [Re-Volt](https://es.wikipedia.org/wiki/Re-Volt); y donde podremos **competir y combatir** a nuestros contrincantes usando **armas y trampas** en pistas increibles. Los coches tendrán **características Offroad**, teniendo una gran **maniobrabilidad** y **capacidad de impulso** que hará de las partidas una experiencia más que recomendable. El juego está desarrolado con **Unity3D** y a nivel gráfico, tal y como podemos ver en el trailer de abajo, **destaca por un aspecto realista**.


En cuanto a la jugabilidad, **cada coche podrá estar equipado con dos armas diferentes**, donde elegirás tu estrategia antes de la carrera para ir un paso por delante de tus oponentes. Para ello debes **evitar las trampas**, **mejorar tus habilidades** de conducción y **utilizar tus armas** sabiamente para completar los desafíos.


**Características principales:**


Eventos de temporada para un solo jugador con 93 desafíos  
 2 ambientes diferentes incluyen 10 pistas para jugar  
 6 coches simulados con motor físico  
 7 neumáticos diferentes  
 8 armas únicas, como Rocket, Mine, EMP, y muchas más...  
 soporte de controladores


**Próximas características (aún no implementadas):**


Multijugador con pantalla dividida para 2 jugadores  
 Multijugador en línea de 8 jugadores  
 personalización de coches


Como veis, el juego, que se espera en su versión final a principios del año que viene, promete diversión a raudales, y para que ilustreis un poco lo que acabais de leer os dejamos con el trailer del juego:


<div class="resp-iframe"><iframe allow="accelerometer; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://steamcdn-a.akamaihd.net/steam/apps/256768381/movie_max.webm?t=1575977015" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


La verdad es que el juego pinta muy bien, y desde JugandoEnLinux.com prometemos seguirle la pista a su desarrollo. ¿Qué os parece a vosotros? ¿Echábais en falta un juego de estas características? Podeis darnos veustra opinión sobre esto en los comentarios de este artículo, en nuestra comunidad de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).


 

