---
author: Pato
category: Noticia
date: 2019-08-02 11:38:39
excerpt: <p>Repasamos algunos juegos que puedes jugar de forma gratuita mientras te
  vas de vacaciones con tu PC a cuestas</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/83a2ea62e0f4cfa71f77de7659289685.webp
joomla_id: 1091
joomla_url: juegos-free-to-play-comerciales-que-puedes-jugar-en-tu-linux-steamos-este-verano
layout: post
tags:
- gratis
title: Juegos "free to play" (comerciales) que puedes jugar en tu Linux/SteamOS este
  verano
---
Repasamos algunos juegos que puedes jugar de forma gratuita mientras te vas de vacaciones con tu PC a cuestas

Comenzamos el mes estival por excelencia, y mientras unos seguimos al pie del cañón otros están de vacaciones, o descansando. Pero sea como sea, aquí seguimos todos disfrutando de lo que más nos gusta que son los videojuegos, y sobre todo si son nativos para Linux.


Con la oferta actual, todos tenemos una lista de juegos que podemos disfrutar de forma gratuita, pues en Linux tenemos un buen número de "juegos libres" como Super Tux Car, Xonotic u otros muchos, pero esta vez queremos centrarnos en juegos "comerciales" que están disponibles para jugar gratis al modo "free to play" y que también están disponibles para Linux/SteamOS.


Nota: La siguiente lista no está confeccionada con un patron concreto en mente, simplemente es un listado de juegos sin un orden ni género concretos.


 


#### Albion Online


Si hablamos de juegos gratuitos, uno de los mejores exponentes de rol masivo online (MMORPG) es este Albion Online en el que podremos sumergir las horas muertas de este verano.


*"Albion Online es un MMORPG de mundo abierto ubicado en un mundo de fantasía medieval. El juego tiene una economía basada en los jugadores en la que casi todos los elementos son fabricados por jugadores. Combina piezas de armadura y armas con su estilo de juego en un sistema único y sin clases, "Eres lo que vistes". Explorar el mundo, aventurarse en batallas emocionantes, conquistar territorios y construir casas."*


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/eBU_Fxd0Jf4" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Lo tienes disponible para descargar [desde Steam](https://store.steampowered.com/app/761890/) o desde su [página web oficial](https://albiononline.com/en/download).


 


#### Allspace


Para todo el que gusta de irse al espacio en estos meses de calor a pasar un tiempo en la oscuridad del universo está disponible este "Allspace", un juego aún en acceso anticipado que ya es disfrutable, aunque mucha calma no vais a tener. Imaginar: hasta 100 pilotos online luchando en batallas masivas, naves disparando láseres de plasma por doquier y acción a raudales en este título de acción en "primera persona"... bueno, en la cabina de tu nave, ¡y además compatible con los visores de RV!


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/0g32NtGQupM" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Lo tienes [disponible en Steam](https://store.steampowered.com/app/1000860/).


 


**The Pirate: Plague of the Dead**


Y del espacio bajamos hasta el mar con este juego de barcos piratas en el que deberemos surcar los mares luchando contra otros bucaneros, monstruos y barcos de guerra que querrán hacerse con nuestro botín. El juego es del estilo de juegos de barcos que se popularizó hace algún tiempo en dispositivos móviles pero que aún así es disfrutable pidiendo pocos requisitos.


*"Comanda los más temibles piratas en una emocionante aventura en el Caribe! Plague of the Dead es un nuevo capítulo de la serie The Pirate. Con un mundo abierto sin pantallas de carga, un ciclo de día/noche dinámico, y un sistema de clima el juego le ofrece a cada jugador una experiencia única"*


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/yXZ31p0A5Co" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


Lo tienes [disponible en Steam](https://store.steampowered.com/app/723790/).


 


####  Rise Of Titans


Vamos ahora con la estrategia, las cartas y la mitología fantástica de Rise Of Titans, aún en acceso anticipado pero también disfrutable.


*"Rise of Titans en un juego de cartas y estrategia multijugador online gratuito ambientado en el misterioso mundo de Zorocu. Encarna tu dios favorito, construye mazos únicos, crea combos poderosos y controla el campo de batalla para defender a tu civilización."*


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/I3GbMZOwSUY" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Tienes Rise Of Titans disponible en acceso anticipado [en Steam](https://store.steampowered.com/app/692130/).


 


#### Tale of Toast


Vamos ahora con algo mas desenfadado. Nos podemos embarcar en el mundo abierto de Tale of Toast, con un apartado artístico de lo mas peculiar tenemos esta aventura fácil de jugar en modos jugador contra jugador con altas apuestas para aquellos que se atreven a participar, y un enfoque en combate simple pero táctico, aspectos sociales, habilidades comerciales, mazmorras generadas proceduralmente y misiones en las que prima la calidad sobre la cantidad.


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/cztgyjhn8D4" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Tienes disponible Tale of Toast [en Steam](https://store.steampowered.com/app/640150).


 


#### Fishing Planet


Vamos ahora con algo mas acorde al tiempo de ocio estival. Muchos aprovechan las vacaciones para practicar sus aficiones favoritas, entre las que seguro está la pesca. Pues en Linux tenemos también esa faceta cubierta con este Fishing Planet:


"*Fishing Planet – es un único, realístico y GRATUITO simulador en línea en primera persona, creado por los pescadores empedernidos para los pescadores. Elija varios aparejos, disfrúte de pesca, descubra el mundo de la pesca mejorando sus capacidades en cualquier momento y lugar ¡junto con sus amigos!*


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/wo0_EG1nogQ" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Tienes Fishing Planet disponible en su [página de Steam](https://store.steampowered.com/app/380600/Fishing_Planet/).


 


#### The Exiled


Volvemos de nuevo con un MMORPG esta vez estilo "sandbox" donde la estrategia y la supervivencia son vitales para progresar. The Exiled está aún en acceso anticipado, este juego nos ofrece el aspecto social y de skills de los MMORPGs pero intentando evitar las tediosas raids para poder farmear y progresar.


"*The Exiled es un MMORPG social del tipo caja de arena donde la estrategia de supervivencia se encuentra con el combate PvP basado en las habilidades. Coopera o conquista, crea o combate; cada decisión es crítica mientras buscas florecer en un valle olvidado."*


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/STmKFoFV_Do" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Tienes The Exiled disponibleb [en su página de Steam](https://store.steampowered.com/app/332650/The_Exiled/).


 


#### Blade Symphony


Cambiamos ahora de tercio para introducirnos en la lucha a espada con este juego que hace poco que pasó a ser "free to play" y donde podremos enfrentarnos a otros jugadores en el "noble" arte de la espada, o a espadazos.


"*Demuestra que eres el mejor espadachín del mundo en Blade Symphony: un slash-em-up con un sistema de lucha con espada muy detallado y profundo. ¡Enfréntate a otros jugadores en juegos de espadas tácticos, duelos de equipo 2 contra 2, sandbox FFA o el modo de juego Control Points!"*


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/4YMSe71sn5Y" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Tienes disponible Blade Symphony [en su página de Steam](https://store.steampowered.com/app/225600/Blade_Symphony/).


 


#### WAKFU


Vamos ahora a por otro MMORPG con un estilo peculiar, donde la aventura y el humor tienen relevancia junto al combate táctico dentro de la fantasía del universo de los doce.


"*Vive una aventura única Embárcate hacia el Mundo de los Doce y sumérgete en la gran aventura de WAKFU, un juego de rol multijugador masivo en línea con un universo original, donde los combates tácticos riman con el humor (sí, sí, verás como sí rima)."*


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/fwIoV86G-N8" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Tienes disponible WAKFU en su [página de Steam](https://store.steampowered.com/app/215080/WAKFU/).


#### Dota 2 / Dota Underlords


Vamos ya con los pesos pesados de Valve. Dota 2 (Defense Of The Ancients) y Dota Underlords tienen como carta de presentación ser los mayores exponentes de su género, ya sea MOBA (Multiplayer Online Battle Arena) o el ahora en pleno auge "Battle Chess". Además este último también está disponible en smartphones.


Hablamos de enfrentarnos a otros jugadores en batallas estratégicas de héroes con características propias ya sea en la arena de juego o desplegando nuestra estrategia en el tablero de lucha.


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/-cSFPIwMEq4" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/Epuqn_gxNhc" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


En Steam, como es lógico al ser juegos de Valve tienes tanto [Dota 2](https://store.steampowered.com/app/570/Dota_2/) como [Dota Underlords](https://store.steampowered.com/app/1046930/Dota_Underlords/).


 


#### Counter-Strike: Global Offensive (y Red Zone)


Terminamos ya esta lista (se nos ha ido un poco de las manos) con Counter Strike: Global Offensive. A falta de un Battle Royale, Valve nos trae este juego en el que podremos enfrentarnos a otros jugadores de forma competitiva en su último modo "Red Zone".


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/edYCtaNueQY" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Lo tienes disponible en su [página de Steam](https://store.steampowered.com/app/730/CounterStrike_Global_Offensive/).


 


Hasta aquí la lista de juegos "free to play" de este verano. ¿Qué te ha parecido? seguro que nos dejamos alguno que conoces o que juegas a menudo, o incluso tienes tu propia lista de juegos "free to play". ¿Que tal si nos lo cuentas en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org) y así los disfrutamos nosotros también?


¡Buen verano a todos!

