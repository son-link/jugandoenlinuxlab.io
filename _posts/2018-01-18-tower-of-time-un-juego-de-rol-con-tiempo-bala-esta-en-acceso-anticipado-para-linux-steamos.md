---
author: Pato
category: Rol
date: 2018-01-18 18:42:32
excerpt: <p>El juego acaba de recibir un "gran parche"</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/52847945d34c24d045f4f6bfeae5a17d.webp
joomla_id: 612
joomla_url: tower-of-time-un-juego-de-rol-con-tiempo-bala-esta-en-acceso-anticipado-para-linux-steamos
layout: post
tags:
- accion
- rol
- acceso-anticipado
title: "'Tower of Time' un juego de rol con \"tiempo bala\" est\xE1 en acceso anticipado\
  \ para Linux/SteamOS"
---
El juego acaba de recibir un "gran parche"

Un nuevo juego de Rol ha llamado mi atención. Se trata de 'Tower of Time', aún en acceso anticipado y que ayer mismo [recibió un "gran" parche](http://steamcommunity.com/games/617480/announcements/detail/1653252639167845503). Los desarrolladores anuncian que el juego ha recibido una nueva versión en Linux que ha sido testada en Ubuntu 17.04 aparte de algunos "bugfixes" y otras mejoras.


"Tower of Time [[web oficial](http://www.evehor.com/)] es una nueva vuelta de tuerca respecto a los clásicos RPGs. Tiene niveles diseñados a mano llenos de enemigos, desafiantes puzzles e historias absorbentes. Su sistema de combate dinámico en tiempo real con modo "cámara lenta" requiere pericia táctica y planificación cuidadosa durante toda la partida. "


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/YrnIbH-En6I" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Características destacadas:


* 7 Clases de personajes, cada uno con diferentes puntos fuertes y débiles
* Sistema complejo de progresión con dos árboles exclusivos de progreso y múltiples opciones.
* Generación de Loot aleatoria con unas 100.000 combinaciones posibles
* Unos 100 enemigos, cada uno con habilidades únicas
* Cinco niveles de dificultad, incluyendo dificultad épica diseñada para jugadores veteranos de los RPG/RTS y MOBA.
* Múltiples modos de combate aseguran que hay multitud y variedad de desafíos.
* Resuelve puzzles y encuentra áreas escondidas con recompensas incleibles.


En cuanto a los requisitos, los mínimos son:


* **SO:** Ubuntu 17.04, Mint 18.2 o Debian 9
* **Procesador:** Intel Core i3 o equivalente
* **Memoria:** 8 GB de RAM
* **Gráficos:** NVIDIA GeForce GT 700M series o equivalente
* **Almacenamiento:** 12 GB de espacio disponible
* **Notas adicionales:** VLC puede ser necesario para reproducir las cinemáticas.


Los requisitos recomendados son:


* **SO:** Ubuntu 17.04, Mint 18.2 o Debian 9
* **Procesador:** Intel Core i5 2400 o superior
* **Memoria:** 8 GB de RAM
* **Gráficos:** NVIDIA GeForce GT 700M series o equivalente
* **Almacenamiento:** 12 GB de espacio disponible
* **Notas adicionales:** VLC puede ser necesario para reproducir las cinemáticas


'Tower of Time' es un juego para un solo jugador, aún se encuentra en acceso anticipado y no está traducido al español, pero si la idea de un RPG con "tiempo bala" te atrae y quieres probarlo, lo tienes disponible en su página de Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/617480/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 ¿Que te parece 'Tower of Time'? ¿Piensas jugarlo?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

