---
author: P_Vader
category: "Acci\xF3n"
date: 2021-12-12 10:57:34
excerpt: "<p>The Game Kitchen cierra el final de Blasphemous con este DLC para centrarse\
  \ en el desarrollo de la segunda parte.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Blasphemous/Blasphemous_Wounds_of_Eventide.webp
joomla_id: 1393
joomla_url: blasphemous-lanza-su-ultimo-dlc-gratuito-llamado-wounds-of-eventide
layout: post
tags:
- gratis
- dlc
- metroidvania
- gotico
- blasphemous
title: "Blasphemous lanza su \xFAltimo DLC gratuito llamado Wounds of Eventide"
---
The Game Kitchen cierra el final de Blasphemous con este DLC para centrarse en el desarrollo de la segunda parte.


Con este será el **tercer contenido descargable gratuito** para el afamado juego, y cierra la historia de este extraordinario producto, como [ya os dimos a conocer](https://www.youtube.com/watch?v=bZ55SE_fDto).


O no, por que comentaron hace unos meses que Blasphemous tendría una continuación y con este último lanzamiento, **se confirma de manera oficial. Tendremos Blasphemous 2, probablemente para 2023.**


En relación al nuevo DLC Wounds of Eventide revela el capítulo final de la primera historia de The Penitent One y ofrece nuevos niveles, jefes y elementos, con secretos que desbloquearán otro destino para Cvstodia, lo que conducirá a los acontecimientos que darán lugar a la continuación de Blasphemous. El final queda abierto para la su secuela a modo de segunda parte.


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/1rBiW_OiJpE" title="YouTube video player" width="780"></iframe></div>


 


Además se han hecho una [gran cantidad de arreglos](https://store.steampowered.com/news/app/774361/view/3102414008267792849) para solucionar algunos problemas que se podían dar en el juego.


**Recordaros que podéis adquirir el juego en [Humble (patrocinado](https://www.humblebundle.com/store/blasphemous?partner=jugandoenlinux)) y [Steam](https://store.steampowered.com/app/774361/Blasphemous/). Actualmente con el lanzamiento de este último DLC está muy rebajado**, es una gran oportunidad para haceros con este juegazo.


¿Qué te parece el cierre de Blasphemous? **¿Serán capaces de estar a la altura con su segunda parte?** Cuéntanos tu opinión en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org)


 


 

