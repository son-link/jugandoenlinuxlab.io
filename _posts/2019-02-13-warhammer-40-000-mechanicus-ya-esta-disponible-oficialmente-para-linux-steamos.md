---
author: Pato
category: Estrategia
date: 2019-02-13 15:49:05
excerpt: <p>El juego de @bulwarkStudios tiene valoraciones muy positivas</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/f2cd3ea2fa74205369dfcf9169ee1dae.webp
joomla_id: 969
joomla_url: warhammer-40-000-mechanicus-ya-esta-disponible-oficialmente-para-linux-steamos
layout: post
tags:
- estrategia
- estrategia-por-turnos
- warhammer-40k
title: "Warhammer 40,000: Mechanicus ya est\xE1 disponible oficialmente para Linux/SteamOS"
---
El juego de @bulwarkStudios tiene valoraciones muy positivas

Tras pasar por la fase de acceso temprano de la que ya os informamos, Bulwark Studios [anunció ayer](https://steamcommunity.com/gid/103582791461657294/announcements/detail/1730974328876975772) mismo que su juego '**Warhammer 40,000: Mechanicus**' ya está oficialmente disponible para nuestro sistema favorito.


Como ya sabéis, se trata de un juego de estrategia por turnos ambientado en el universo Warhammer en el que tomaremos el mando de las tropas humanas más tecnológicamente avanzadas del Imperium: los **Adeptus Mechanicus**. En este juego táctico por turnos, toda decisión que tomes será clave de cara al resultado de la misión.


*Ponte al mando de las tropas humanas más tecnológicamente avanzadas del Imperum: el Adeptus Mechanicus. En la piel del Magos Dominus Faustinius, liderarás la expedición al nuevo planeta Silva Tenebris, que ha sido recientemente descubierto. Gestiona recursos, descubre tecnologías olvidadas, personaliza tu equipo y gestiona todos los movimientos de tus Tech-Priests.*


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/XfqF81VPoM0" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


En cuanto a los requisitos mínimos y recomendados para Linux, son:


**Mínimo:**  

+ Requiere un procesador y un sistema operativo de 64 bits
+ **SO:** SteamOS, Ubuntu 16.04 (64bit)
+ **Procesador:** Intel Core i7 3.0 GHz+
+ **Memoria:** 8 GB de RAM
+ **Gráficos:** 2GB ATI Radeon HD 7970, 2GB NVIDIA GeForce GTX 770 o superior
+ **Almacenamiento:** 8 GB de espacio disponible


**Recomendado:**  

+ Requiere un procesador y un sistema operativo de 64 bits
+ **SO:** SteamOS, Ubuntu 16.04 (64bit)
+ **Procesador:** 3GHz Quad Core
+ **Memoria:** 8 GB de RAM
+ **Gráficos:** GeForce GTX 960 4GB/AMD Radeon R7 200 series/4GB
+ **Almacenamiento:** 8 GB de espacio disponible


Warhammer 40,000: Mechanicus está traducido al español. Si quieres saber mas, puedes visitar su [web oficial](https://www.mechanicus40k.com/), o puedes comprarlo en la [Humble Store](https://www.humblebundle.com/store/warhammer-40000-mechanicus?partner=jugandoenlinux) (enlace patrocinado) o en su [página de Steam](https://store.steampowered.com/app/673880/Warhammer_40000_Mechanicus/) donde está rebajado un 25% en estos momentos.



