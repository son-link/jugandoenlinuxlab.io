---
author: Serjor
category: Noticia
date: 2018-12-27 18:10:44
excerpt: "<p>Junto a la esperada integraci\xF3n, esta beta trae correcciones de errores\
  \ y cambios en la interfaz de usuario</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/5d9106cd3b26b3ecac196958778c9f70.webp
joomla_id: 941
joomla_url: lutris-se-prepara-para-la-conquista-de-la-galaxia-con-una-beta-que-anade-integracion-con-gog
layout: post
tags:
- gog
- lutris
- gogcom
title: "Lutris se prepara para la conquista de la galaxia con una beta que a\xF1ade\
  \ integraci\xF3n con GOG"
---
Junto a la esperada integración, esta beta trae correcciones de errores y cambios en la interfaz de usuario

Leemos en [GOL](https://www.gamingonlinux.com/articles/the-first-beta-for-lutris-05-is-out-with-a-refreshed-ui-and-gog-support.13240) que la gente de [Lutris](lutris.net) se está preparando para su versión 0.5, y han sacado a la luz la primera beta de esta versión (o mini versión, que Lutris aún no ha llegado a la versión 1 a pesar de todo lo que ya ofrece).


Como gran novedad tenemos la integración con GOG, una de las funcionalidades más demandadas por sus usuarios ya que GOG no ofrece una versión para GNU/Linux de su cliente Galaxy.


Si quieres probar esta beta puedes descargarla desde su página de [GitHub](https://github.com/lutris/lutris/releases/tag/v0.5.0-beta), una vez descargada, ir al directorio bin y ejecutar el fichero lutris.py (es un script de python, así que depende de lo bien que se lleve tu DE será tan fácil como hacer doble click o no). Una vez lo ejecutemos tendremos que registrar nuestra cuenta de GOG, en mi caso ha sido al ir a "Import Games", pero si no se me ha olvidado leer, pasará la primera vez que queramos hacer algo con GOG, y cómo podéis ver en la siguiente imagen, luego es tan sencillo cómo querer instalar cualquier otro juego, y él se encargará de la descarga e instalación automáticamente:


![lutris gog download](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Lutris/lutris-gog-download.webp)


Lo dicho, es una beta de la próxima versión mínima, así que es de esperar que haya algún fallo, así que si lo pruebas y encuentras algo raro, no olvides reportarlo.


Y tú, ¿usas Lutris como tu gestor de videojuegos? Cuéntanos qué te parece esta nueva funcionalidad en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD)

