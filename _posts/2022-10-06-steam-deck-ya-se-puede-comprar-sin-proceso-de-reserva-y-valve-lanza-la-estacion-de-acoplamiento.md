---
author: Pato
category: Hardware
date: 2022-10-06 19:53:03
excerpt: "<p>Buenas noticias para todos</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Steamdeck/SteamdeckPile.webp
joomla_id: 1492
joomla_url: steam-deck-ya-se-puede-comprar-sin-proceso-de-reserva-y-valve-lanza-la-estacion-de-acoplamiento
layout: post
tags:
- steam-deck
- estacion-de-acoplamiento
- docking-station
title: "Steam Deck ya se puede comprar sin proceso de reserva, y Valve lanza la estaci\xF3\
  n de acoplamiento"
---
Buenas noticias para todos


El día ha llegado. Tras sucesivas mejoras en la cadena de producción de la Steam Deck por fin Valve anuncia que han logrado acabar con la lista de espera, y por lo tanto todo aquel que quiera comprar la máquina de Valve podrá hacerlo sin el proceso de reserva. 


Debido a esto, la página oficial del dispositivo ha sido remodelada para la ocasión, y ya puedes visitarla en: <https://www.steamdeck.com/es/>


Por otra parte, si quieres comprarla diréctamente, ya puedes acceder a <http://store.steampowered.com/steamdeck> y adquirir la tuya.


Además de esto, anuncian que ya han comenzado las reservas en varios países asiáticos, por lo que la expansión de la máquina sigue su curso y ya anuncian que están trabajando para llegar a más países próximamente.


#### Steam Docking Station ya disponible


En otro orden de cosas, y al tiempo que nos anuncian esta nueva, Valve ya ha puesto a la venta la estación de acoplamiento (Docking station) con las siguientes características:





Periféricos: 3 puertos USB-A 3.1 Gen1




Redes: Gigabit Ethernet




Pantallas externas: DisplayPort 1.4, HDMI 2.0
 



Alimentación: Entrada de transferencia de suministro de energía mediante USB-C (fuente de alimentación incluida)



Conexión del Deck: Cable cautivo USB-C de 6 pulgadas con conector de perfil bajo de 90°




Cargador: Fuente de alimentación incluida con un cable de 1,5 m de largo (el mismo que viene con Steam Deck)
 



Tamaño y peso:



Tamaño 117 mm x 29 mm x 50,5 mm
Peso Aprox. 120 gramos




 
![Steamdocking](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Steamdeck/Steamdocking.webp)
 
Si estás interesado en adquirir uno para tu Deck, [aquí puedes comprarlo](https://store.steampowered.com/dockingstation?snr=2_groupannouncements_detail_).
 
Por último, han publicado un vídeo con todas estas novedades:
 
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/bcwkMfoUATc" title="YouTube video player" width="560"></iframe></div>
 
Puedes ver el anuncio en el post del blog oficial [en este enlace](https://steamcommunity.com/games/1675200/announcements/detail/3276955672625890260).
 



¿Qué te parece el fin de la cola de espera? ¿Comprarías una estación de acoplamiento para tu Deck?


Cuéntamelo en los comentarios o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

