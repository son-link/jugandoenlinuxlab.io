---
author: Pato
category: Noticia
date: 2018-10-29 18:29:42
excerpt: "<p>Rebajas terror\xEDficas hasta el d\xEDa 1 de Noviembre</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/0cd16382a44dd5666a6cdb7bae9c7fbd.webp
joomla_id: 885
joomla_url: las-rebajas-de-halloween-de-steam-ya-estan-aqui
layout: post
tags:
- steam
- rebajas
- ofertas
- halloween
title: "Las rebajas de Halloween de Steam ya est\xE1n aqu\xED"
---
Rebajas terroríficas hasta el día 1 de Noviembre

Ya se rumoreaba desde hace unos días, incluyendo algunas filtraciones y hoy por fin tenemos en Steam las tradicionales rebajas de Halloween de este año, preludio de las que tendremos dentro de apenas mes y medio en las que serán las rebajas de Invierno.


Como siempre, tenemos a nuestra disposición títulos con temática terrorífica rebajados para la ocasión, como pueden ser:


[Dying Light al 60% de descuento](https://store.steampowered.com/app/239140/Dying_Light/)


[Layers of Fear al 75% de descuento](https://store.steampowered.com/app/391720/Layers_of_Fear/)


[Salt and Sanctuary al 50% de descuento](https://store.steampowered.com/app/283640/Salt_and_Sanctuary/)


[Sunless Sea al 66% de descuento](https://store.steampowered.com/app/304650/SUNLESS_SEA/)


[Alien Isolation al 75% de descuento](https://store.steampowered.com/app/214490/Alien_Isolation/)


[Amnesia: The dark descend al 80% de descuento](https://store.steampowered.com/app/57300/Amnesia_The_Dark_Descent/)


[SOMA al 80% de descuento](https://store.steampowered.com/app/282140/SOMA/)


Solo por citar unos cuantos...


Todas las rebajas las puedes ver en:


<https://store.steampowered.com/>


Y tu... ¿Piensas comprar algún título en estas rebajas? ¿Cual?


Cuentamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 

