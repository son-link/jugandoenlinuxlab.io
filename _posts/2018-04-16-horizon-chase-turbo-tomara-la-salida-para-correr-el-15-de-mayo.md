---
author: Pato
category: Carreras
date: 2018-04-16 18:03:46
excerpt: <p>Ya se pude adquirir el juego de <span class="username u-dir" dir="ltr">@AquirisGS</span></p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/e53c2c9f4d6761de9122d72eac64a0cc.webp
joomla_id: 711
joomla_url: horizon-chase-turbo-tomara-la-salida-para-correr-el-15-de-mayo
layout: post
tags:
- carreras
- indie
- proximamente
- arcade
title: "Horizon Chase Turbo tomar\xE1 la salida para correr el 15 de Mayo (ACTUALIZADO)"
---
Ya se pude adquirir el juego de @AquirisGS

**ACTUALIZACIÓN 15-5-18**: Tal y como anunciábamos en este mismo artículo hace un mes, finalmente ya está a la venta, y con soporte para GNU-Linux/SteamOS este juego arcade de conducción retro. Podeis comprar   Horizon Chase Turbo por menos de 17€ en Steam:


 <div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/389140/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>





---


**NOTICIA ORIGINAL**: Si disfrutaste con los juegos de carreras de los 80 y los 90, seguro que este 'Horizon Chase Turbo' te sonará. Se trata de un juego de carreras inspirado en clásicos como Outrun, Top Gear o Lotus en los que el objetivo era la diversión pura y sin complicaciones. No nos vamos a engañar, el título no nos trae gráficos de última generación, ni físicas realistas, pero a cambio, a poco que los chicos de Aquiris Game Studio consigan transmitir la esencia jugable de aquellos míticos títulos estaremos ante un juego digno de meterle horas.


*"Horizon Chase Turbo es un juego de carreras inspirado en los grandes éxitos de los años 80's y 90's: Out Run, Lotus Turbo Challenge, Top Gear (SNES) y la serie Rush entre otros. Cada curva y cada vuelta en Horizon Chase Turbo recrea el gameplay de los arcades clásicos y ofrece velocidad sin límites. ¡Acelera al máximo y disfruta!"*


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/CxvVZ261dAQ" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


#### Características Principales


* Excitante juego arcade de carreras, inspirado por Outrun, Top Gear, Lotus Turbo Challenge y otros clásicos del género.
* Gráficos e interfaz modernos de alta calidad.
* Increíble modo multiplayer a pantalla dividida con hasta 4 jugadores.
* Música a cargo de Barry Leitch, Sound Designer en los clásicos de los 90s como Top Gear y Lotus Turbo Challenge, entre otros.
* Multitud de contenido: 12 Copas, 48 Ciudades, 110 Circuitos, 26 Coches Desbloqueables y 10 Actualizaciones.
* Desafía a tus amigos en el “Modo Fantasma Competitivo” online.
* Haz historia incluyendo tu nombre en las tablas de puntuaciones de tus amigos.


En cuanto a los requisitos mínimos y recomendados, son:


**Mínimo:**  

+ **SO:** Linux Ubuntu 12.04 o superior
+ **Procesador:** Intel Core 2 Duo 2.0 GHZ o superior
+ **Memoria:** 2 GB de RAM
+ **Gráficos:** Intel HD Graphics 4000 o superior
+ **Almacenamiento:** 400 MB de espacio disponible


**Recomendado:**  

+ **SO:** Linux Ubuntu 12.04 o superior
+ **Procesador:** Intel Core i5 2.5 GHz o superior
+ **Memoria:** 4 GB de RAM
+ **Gráficos:** GeForce 8000 Series o superior
+ **Red:** Conexión de banda ancha a Internet
+ **Almacenamiento:** 800 MB de espacio disponible


No me negaréis que si cumple lo que promete, será un título para disfrutar solo o en el sofá con los amigos al estilo "multijugador local". Si queréis saber más, podéis ver más detalles en su página de Steam donde llegará el próximo 15 de Mayo traducido al español:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/389140/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>
 



