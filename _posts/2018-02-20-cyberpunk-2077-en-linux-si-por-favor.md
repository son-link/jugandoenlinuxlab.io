---
author: leillo1975
category: Rol
date: 2018-02-20 09:09:31
excerpt: <p>Ha surgido un movimiento comunitario para solicitar que el juego tenga
  soporte.</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/8d20793c9524220702360040c923d064.webp
joomla_id: 652
joomla_url: cyberpunk-2077-en-linux-si-por-favor
layout: post
tags:
- virtual-progamming
- cd-projekt-red
- witcher-3
- ciberpunk-2077
- gogcom
title: Cyberpunk 2077 en Linux? Si por favor!
---
Ha surgido un movimiento comunitario para solicitar que el juego tenga soporte.

Esta mañana, haciendo la ronda por las webs que suelo visitar habitualmente, me encontré con un [post en Reddit](https://www.reddit.com/r/linux_gaming/comments/7yrco4/show_demand_for_cyberpunk_2077_for_linux/) en el que el título del juego al que se refería me llamó la atención. Este juego es [Cyberpunk 2077](http://cyberpunk.net/), y como muchos de vosotros seguro que sabeis, se trata de el nuevo proyecto de [CD Projekt RED](http://en.cdprojektred.com/), los desarrolladores de otro de los juegos por los que llevamos años suspirando los Linuxeros, [The Witcher 3: Wild Hunt](http://thewitcher.com/en/witcher3/).


El caso es que en dicho post de Reddit se pide a **GOG.com** en su sección de **Community Wishlist** (Lista de Deseos Comunitaria) que por favor traigan el juego a Linux. Seguro que más de uno de vosotros sabe que esta tienda digital pertenece a CD Project RED, y aunque como comentó **LinuxVanGOG** en una fantástica [entrevista en Boiling Steam](https://boilingsteam.com/linuxvangog-talks-about-gogs-stance-on-linux/), sean dos empresas que funcionan de forma independiente, seguro que si la demanda es grande no pasará desapercibido para la empresa matriz.


En este momento la petición lleva 1900 apoyos, y desde JugandoEnLinux.com nos gustaría animaros a todos los que querais ver Cyberpunk 2077 en Linux a que paseis y dejeis un mensaje agregandoos a la petición. La petición podeis encontrarla en:


<https://www.gog.com/wishlist/games/cyberpunk_2077_on_linux>


Es importante decir, que también hay una petición para traer **The Witcher 3** que lleva unas 11500 peticiones y que no estaría de más que mostraseis tambien vuestro apoyo. Como sabeis en su día [os hablamos](index.php/homepage/editorial/item/521-the-witcher-3-posiblemente-no-llegara-nunca-a-linux-por-las-criticas-a-the-witcher-2) que el juego no llegó nunca a Linux por la mala recepción que tuvo por parte de la comunidad  su segunda parte por culpa de su inicial mal funcionamiento. El port había sido realizado por Virtual Programming y en su salida no funcionaba todo lo bien que se podía esperar, lo cual provocó la ira y el cabreo de algunos usuarios descontentos. Poco después y gracias al trabajo de los desarrolladores y la comunidad se consiguió mejorar bastante el port, pero la nefasta imagen dada por algunos hizo huir a CD Projekt Red, que decidió cancelar el port de su tercera parte, a pesar de los anuncios previos. La petición de The Witcher 3 es esta:


<https://www.gog.com/wishlist/games/witcher_3_linux_version_1>


Para quien no conozca Ciberpunk 2077, se trata de un juego de rol al estilo The Witcher, pero obviamente con una temática completamente diferente. Según CD Projeckt RED se trata de su proyecto más ambicioso hasta la fecha, y será incluso más extenso que The Witcher 3. Del juego poco más se sabe pues sus desarrolladores mantienen un **mutismo casi absoluto** sobre él, pero conociendo al estudio bien seguro que será una obra maestra. De lo poco que conocemos está su trailer de avance que puedes ver justo aquí debajo:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/P99qJGrPNLs" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


¿A qué esperas para mostrar tu apoyo a estas iniciativas? Si quieres compartir algo más con respecto a Ciberpunk 2077 puedes dejar un comentario o usar nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

