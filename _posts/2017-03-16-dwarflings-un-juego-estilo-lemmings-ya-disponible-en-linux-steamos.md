---
author: Pato
category: Estrategia
date: 2017-03-16 18:54:34
excerpt: <p>"Los Lemmings conocen a Lost Vikings"</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/9ad74ebcc3d83e86bcc0098026ed5e9f.webp
joomla_id: 252
joomla_url: dwarflings-un-juego-estilo-lemmings-ya-disponible-en-linux-steamos
layout: post
tags:
- indie
- aventura
- steam
- estrategia
title: '''Dwarflings'' un juego estilo "Lemmings" ya disponible en Linux/SteamOS'
---
"Los Lemmings conocen a Lost Vikings"

"Un reto para los dedos y la mente", así es como define Starwind Games este 'Dwarflings' donde tendrás que dirigir hasta a 5 personajes, cada cual con habilidades distintas y ayudarlos a escapar hacia la libertad.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/bTmNqDMyv_E" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


 El control sobre los personajes será indirecto, pudiendo controlar el tiempo y las acciones que luego tendrán que realizar los personajes por su cuenta.


Si te van los juegos tipo Lemmings con una vuelta de tuerca, puedes encontrar este 'Dwarflings' para Linux en su página de Steam con un buen descuento por su lanzamiento, eso sí, no está traducido al español:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/562330/" width="646"></iframe></div>


 

