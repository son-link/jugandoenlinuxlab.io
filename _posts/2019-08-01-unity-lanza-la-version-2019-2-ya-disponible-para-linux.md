---
author: yodefuensa
category: Software
date: 2019-08-01 16:23:19
excerpt: <p>El popular motor multiplataforma sigue evolucionando a buen ritmo</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/4cafa21f58b00d9806ade60732bdd038.webp
joomla_id: 1086
joomla_url: unity-lanza-la-version-2019-2-ya-disponible-para-linux
layout: post
tags:
- software
- unity3d
- unity
title: "Unity lanza la versi\xF3n 2019.2, ya disponible para Linux"
---
El popular motor multiplataforma sigue evolucionando a buen ritmo

El pasado 30 de julio fue publicada la versión 2019.2 de unity, el motor de videojuegos multiplataforma. En esta versión, obtiene más de 170 nuevas funciones y mejoras para artistas, diseñadores y programadores. Así como las actualizaciones para ProBuilder, Shader Graph, Animación 2D, Burst Compiler, UI Elements y muchos más. Podéis ver las novedades más destacadas en siguiente el video resumen: 


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/qoERAtLGq-8" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Si quereis instalaros Unity recordad pasaros por el foro, donde hay un tutorial para instalar y configurar el motor gráfico y empezar a hacer nuestros pinitos con él. 


Los más impacientes también puede obtener acceso a la versión 2019.3 alfa, que será la siguiente gran actualización que está programada para el otoño de 2019. 


Y ya que estamos de paso aprovechar la noticia para comentar el fantastico bundle de recursos 2D para nuestros proyectos que ha sacado Humble Bundle


[https://www.humblebundle.com/software/2d-game-dev-bundle?](https://www.humblebundle.com/software/2d-game-dev-bundle?partner=jugandoenlinux) (enlace patrocinado)


Fuente:<https://blogs.unity3d.com/es/2019/07/30/heres-whats-in-the-brand-new-unity-2019-2>

