---
author: leillo1975
category: "Acci\xF3n"
date: 2018-04-19 07:12:19
excerpt: <p>@feralgames ha sido la encargada del port</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/139702ff92e93f29d84ebf976a5dfd8f.webp
joomla_id: 713
joomla_url: rise-of-tomb-raider-20-aniversario-disponible-en-gnu-linux-steamos
layout: post
tags:
- vulkan
- feral
- gamemode
- rise-of-tomb-raider
title: "\"Rise of the Tomb Raider: 20\xBA Aniversario\" disponible en GNU-Linux/SteamOS\
  \ (ACTUALIZACI\xD3N)"
---
@feralgames ha sido la encargada del port

**ACTUALIZACIÓN 24-4-18**: Según informa [Phoronix](https://www.phoronix.com/scan.php?page=news_item&px=AMDVLK-Tomb-Raider-Fixed),  se ha arreglado el driver AMDVLK para que no se vuelva a bloquear cuando se ejecuta el juego. Como sabeis,  con el driver RADV no existe este problema, pero los usuarios de AMDVLK habían reportado que el driver no funciona correctamente. Es una noticia corta pero más que interesante para los que usais soluciones gráficas de AMD.

---


**NOTICIA ORIGINAL:** Dos meses ha desde que teníamos la [confirmación oficial](index.php/homepage/generos/accion/item/763-rise-of-the-tomb-raider-20-anniversario-llegara-a-gnu-linux-proximamente-de-la-mano-de-feral-interactive) por parte de la compañía británica **Feral Interactive** de la existencia del port... dos meses de tensa espera. Aunque era casi un secreto a voces, y el juego era demandado constantemente por la comunidad, la noticia provocó una profunda alegría en los jugones linuxeros, que veían como las [pistas](http://www.feralinteractive.com/es/upcoming/) que se daban previamente eran correctas.


Tras un anuncio por sorpresa en el día de ayer, el juego finalmente está entre nosotros y seguramente hará las delicias de todos los que disfrutamos hace dos años del reboot de la Saga, el cual fué un auténtico exitazo de ventas que puso en primer plano de nuevo a Lara Croft, con un aspecto mucho más real y unas nuevas mecánicas de juego que claramente marcaron un antes y un después en el género. El anuncio de la salida oficial del juego se acaba de producir en **Twitter**:



> 
> Lara Croft returns to Linux in Rise of the Tomb Raider: 20 Year Celebration.  
>   
> The 20 Year Celebration bundles the base game with all DLC ever released for the game.  
>   
> To set out on a new adventure, buy from the Feral Store now – <https://t.co/ArIT0ijkut> [pic.twitter.com/AguK6Lcw6Y](https://t.co/AguK6Lcw6Y)
> 
> 
> — Feral Interactive (@feralgames) [April 19, 2018](https://twitter.com/feralgames/status/986910873376755712?ref_src=twsrc%5Etfw)


El título **hace uso de Vulkan** , lo que permitirá explotar las bondades que esta API gráfica ofrece, y será el primer juego que incluirá [GameMode](index.php/homepage/generos/software/item/830-feral-lanza-gamemode-una-utilidad-open-source-para-mejorar-el-rendimiento-en-los-juegos) sin necesidad de activarlo mediante comandos. La lista de **requerimientos mínimos** en esta ocasión es un tanto elevada, aunque en un juego de estas características es normal, pero aun así no será un inconveniente para la mayoría de los PC's actuales que solemos usar los que nos gusta darle al vicio:



+ **SO:** Ubuntu 17.10
+ **Procesador:** Intel Core i3-4130T o AMD equivalente
+ **Memoria:** 8 GB de RAM
+ **Gráficos:** 2GB AMD R9 285 (GCN 3ª Gen o superior), 2GB Nvidia GTX 680 o mejor
+ **Almacenamiento:** 28 GB de espacio disponible
+ **Notas adicionales:**
	- \* Requiere Vulkan
	- \* Driver Nvidia 396.18 o posterior.
	- \* AMD requiere Mesa 17.3.5 o posterior.  Se recomienda el PPA estable de Padoka
	- \* Vega requiere Mesa 18.0 o posterior.
	- \* AMD GCN 1ª y 2ª generacion no son soportadas
	- \* Gráficos Intel no soportados
	- \* Requiere un procesador con instrucciones SSE2.



La descripción oficial del juego ofrecida por Feral es la siguiente:

> 
> *""Descubre la leyenda oculta.*
> 
> 
> *En la impresionante secuela de Tomb Raider, Lara Croft se convierte en mucho más que una superviviente al embarcarse en su primera gran expedición para explorar las regiones más antiguas y remotas de la tierra y descubrir el secreto de la inmortalidad.*
> 
> 
> *Explota los talentos emergentes de Lara en una dramática aventura de acción que va desde las ruinas quemadas por el sol de Siria hasta las montañas heladas de Siberia. Juega como la joven arqueóloga mientras conquista tumbas, supera a rivales y forja su leyenda.*
> 
> 
> *VIAJE A LA CIUDAD PERDIDA DE KITEZH Un año después de su aventura en Yamatai, Lara se propone encontrar una ciudad olvidada por mucho tiempo que alberga la Fuente Divina, un artefacto bíblico con el poder de conceder la inmortalidad. Cuando la búsqueda la pone en el punto de mira de la Trinidad, una organización religioso-militar, Lara debe hacer uso de todo su ingenio, habilidad y audacia para alcanzar la Fuente Divina antes que nadie.*
> 
> 
> *MUJER VS ENTORNOS SALVAJES Salta, trepa y sube en tirolina a través de hermosos pero letales entornos. Acecha a animales salvajes, busca suministros y explora el paisaje mientras perfeccionas tus habilidades en la lucha, la caza y la supervivencia.*
> 
> 
> *COMBATE DE GUERRILA ÉPICO Configura las armas y municiones de Lara, desde arcos y escopetas hasta flechas venenosas y explosivos improvisados, para perfeccionar tu equilibrio entre el sigilo y la acción. Aprovecha el entorno escalando árboles o buceando para emboscar a tus enemigos.*
> 
> 
> *UN NIVEL SUPERIOR PARA TOMB RAIDER Recorre las precarias profundidades de las inmensas tumbas desafiantes mientras resuelves grandes rompecabezas multinivel y evades trampas mortíferas para encontrar tesoros perdidos.*
> 
> 
> *LA AVENTURA COMPLETA Rise of the Tomb Raider: Paquete 20º Aniversario equipa al juego básico con su colección completa de DLC. Descubre los secretos de la Mansión Croft en Lazos de Sangre, encuentra la verdad tras el mítico terror en Baba Yaga: El Templo de la Bruja, evita un brote letal en la Fría Oscuridad y enfréntate a enemigos sobrenaturales en La Pesadilla de Lara. En el Modo Aguante, sobrevive al mundo salvaje de Siberia en soledad o con otro jugador en el modo cooperativo.*
> 
> 
> *DISEÑADO PARA macOS Y LINUX La serie de Tomb Raider regresa a macOS y Linux en una forma espectacular al usar la última tecnología gráfica de Metal y Vulkan.""*
> 
> 
> 

En JugandoEnLinux.com nos gustaría adelantaros que en unas semanas podreis leer un **completo análisis** de este título, además, esta misma noche a partir de las 22:30 podreis ver en nuestro canal de [Twitch](https://www.twitch.tv/jugandoenlinux)" un **desempaquetado en directo del juego** que realizará nuestro colega [Pato](index.php/homepage/generos/accion/itemlist/user/192-pato). ¡No falteis a la cita!


Podeis comprar "Rise Of The Tomb Raider: 20º Aniversario" en la [tienda de Feral](https://store.feralinteractive.com/es/mac-linux-games/riseofthetombraider/) **(recomendado)** o en Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/391220/90033/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


Os dejamos con un video del juego:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/sr2jcR4ovPw" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>
 
 
¿Os gusta la saga Tomb Raider? ¿Comprareis esta última entrega? Dejanos tus impresiones y respuestas sobre el juego en los comentarios o en nuestros canales de [Telegram](https://t.me/jugandoenlinux) y [Discord](https://discord.gg/fgQubVY).