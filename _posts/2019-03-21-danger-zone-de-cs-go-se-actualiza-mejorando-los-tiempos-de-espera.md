---
author: leillo1975
category: "Acci\xF3n"
date: 2019-03-21 14:40:06
excerpt: "<p>El Matchmaking ahora ser\xE1 m\xE1s llevadero</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/7d13cc638b031f8ce1bae1b0204ae77c.webp
joomla_id: 1002
joomla_url: danger-zone-de-cs-go-se-actualiza-mejorando-los-tiempos-de-espera
layout: post
tags:
- actualizacion
- danger-zone
- cs-go
- counter-strike-global-offensive
- matchmaking
title: Danger Zone, de CS:GO, se actualiza mejorando los tiempos de espera.
---
El Matchmaking ahora será más llevadero

Danger Zone es un Battle Royale divertidisimo, y además está muy bien hecho y funciona de maravilla en nuestro querido Linux; pero reconozcámoslo, sus **largos periodos de espera mientras nos empareja** en una partida han provocado que muchos de nosotros hayamos pasado casi completamente de él incluso a pesar de ser gratuito. Era desesperante estar cinco minutos esperando para luego jugar dos o tres minutos (un servidor que es muy malo) Si no me creeis podeis ver el siguiente video donde comprobareis lo que aquí os comentamos:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/dBY6jZhLDek" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


Pues parece que esta situación finalmente ha terminado, ya que parece que desde Valve le han puesto fin a esta situación y ahora los tiempos son mucho más razonables y se han reducido sustancialmente. Gracias a la información ofrecida por [GoL](https://www.gamingonlinux.com/articles/counter-strike-global-offensive-now-has-much-better-queue-times-for-danger-zone.13804), sabemos que en la [última actualización](https://blog.counter-strike.net/index.php/2019/03/23620/) de este conocido juego se ha solventado este, que a nuestro modesto juicio era su principal problema. Además  de esta mejora se han añadido las siguientes (traducción online):


**GAMEPLAY**  
- Se agregó un elemento de marcador para indicar la bonificación por pérdida de cada equipo en los modos de juego Competitivo y Wingman.  
- Zeus-x27 ya no podrá matar gente a través de las paredes.


**MISC**  
- Se ha solucionado un problema que provocaba la aparición de una pantalla negra en los usuarios con GPU Intel integrada que se ejecutaban con la opción de lanzamiento -d3d9ex.  
- Mejora de los tiempos de emparejamiento de la Zona de Peligro.  
- La pestaña de compañeros de equipo recientes ahora mostrará a tu compañero de equipo de la Zona Peligrosa incluso antes de que termine el partido en curso de la Zona Peligrosa, así que si tuviste un buen partido, puedes invitarlo a tu fiesta para que juegue de nuevo.  
- Añadido faltando "El" a "El Emperador"


**MAPAS**  
- Vértigo ha sido añadido a Competitive matchmaking.  
- Cobblestone ha sido eliminado de Competitive matchmaking.  
- El paso elevado y el Nuke han sido movidos al Delta del Grupo Defusal.  
- Cobblestone y Vertigo han sido movidos a Defusal Group Sigma.


**Vértigo:**  
-Cambió la entrada del TAC a la mitad, de modo que los TAC tengan una rotación más segura entre los sitios.  
-Mover la pila de madera contrachapada hacia adelante y la caja de madera cerca del lado T de la mitad para hacer que los impulsos de T sean un poco más seguros.  
-Suavizó la colisión del jugador en las escaleras de metal en el desove de T y en el sitio B.  
-Paisajes sonoros actualizados  
-Generador desplazado en T desova hasta el segundo piso  
-Hizo un poco más fácil lanzar granadas desde el techo en el pasillo T hasta la mitad de la calle.  
-Ajustes y correcciones menores


 


Pues nada amigo, sabiendo esto tan solo nos queda probarlo juntos de nuevo y volver a pasar un buen rato con el incombustible CS:GO ¿Para cuando la próxima partida? Si quieres proponer una o hablar sobre este juego puedes dejar un comentario o encontrarnos en nuestro canal de [Telegram](https://t.me/jugandoenlinux).

