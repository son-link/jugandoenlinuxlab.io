---
author: Pato
category: Software
date: 2017-02-14 21:31:05
excerpt: <p>La tienda ofrece un paquete con muchos juegos disponibles en Linux</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/49d07d2f2b048709fab28e0845347114.webp
joomla_id: 220
joomla_url: humble-freedom-bundle-nos-trae-un-buen-punado-de-juegos
layout: post
title: "'Humble Freedom Bundle' nos trae un buen pu\xF1ado de juegos"
---
La tienda ofrece un paquete con muchos juegos disponibles en Linux

De vez en cuando Humble Bundle nos trae ofertas de lo mas interesantes. En concreto ahora mismo tiene en marcha el 'Humble Freedom Bundle' donde nos ofrece una buena colección de juegos por un precio más que atractivo y lo mejor de todo es que muchos de ellos están disponibles para Linux, y además puedes conseguirlos sin DRM. ¿Alguien da más?


Como ejemplo, en el momento de escribir este artículo están disponibles 'Stardew Valley', 'Octodad', 'Super Meatboy', 'World of Goo' y muchos más, por tan solo 30€. Eso sí, si tienes intención de pillar el paquete no tardes, pués las existencias son limitadas y ya hay unos cuantos juegos que ya se han agotado en el paquete.


Además, todo lo recaudado con la venta de este Bundle irá destinado a la caridad, en concreto a ACLU (American Civil Liberties Union), Médicos sin fronteras e International Rescue Comite.


Puedes ver todo el Bundle en el siguiente enlace:


<https://www.humblebundle.com/freedom>


Gracias a Jaime por avisarnos en [Telegram](https://telegram.me/jugandoenlinux).

