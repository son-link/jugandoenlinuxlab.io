---
author: Pato
category: Estrategia
date: 2019-08-01 16:03:29
excerpt: "<p>El juego de ChaosForge nos trae una propuesta de acci\xF3n estrat\xE9\
  gica por turnos</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/0d77511af28edff9982f6b0b2eb7aca3.webp
joomla_id: 1085
joomla_url: jupiter-hell-llega-a-linux-steamos-en-acceso-anticipado
layout: post
tags:
- accion
- indie
- estrategia-por-turnos
title: Jupiter Hell llega a Linux/SteamOS en acceso anticipado
---
El juego de ChaosForge nos trae una propuesta de acción estratégica por turnos

Por fin, después de varios años de desarrollo, por fin **Jupiter Hell** [ha salido en acceso anticipado](https://steamcommunity.com/games/811320/announcements/detail/1610519172266976739) para Linux/SteamOS para todo aquel que le guste la propuesta de la **acción estratégica por turnos**.


Con un trasfondo espacial y base roguelike, Jupiter Hell nos pone en la piel de un marine espacial en una de las lunas de Júpiter que se las tendrá que ver con multitud de monstruos y demonios de toda clase y condición.


Aquí tenéis el trailer de presentación:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/YY8hfy0GopQ" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Los requisitos necesarios para el juego publicados son:


**RECOMENDADO:**


+ Requiere un procesador y un sistema operativo de 64 bits
+ **SO:** Ubuntu 16.04 64-bit
+ **Memoria:** 8 GB de RAM
+ **Gráficos:** NVIDIA GeForce GTX 960 o superior
+ **Almacenamiento:** 2 GB de espacio disponible
+ **Tarjeta de sonido:** DirectX compatible


Si te interesa la propuesta de Jupiter Hell, ya lo tienes disponible [en su página de Steam](https://store.steampowered.com/app/811320/Jupiter_Hell/) en acceso anticipado con un 10% de descuento por estrenar el acceso anticipado.

