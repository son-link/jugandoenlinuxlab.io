---
author: Pato
category: Arcade
date: 2017-02-16 16:49:36
excerpt: <p>El juego de 2Awesome Studio celebra el lanzamiento en acceso anticipado
  con un 10% de descuento</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/f50c152c7848dac24210d3ad3ad22154.webp
joomla_id: 221
joomla_url: dimension-drive-ya-disponible-en-linux-hoy-dia-de-su-lanzamiento
layout: post
tags:
- indie
- steam
- acceso-anticipado
- arcade
- shoot-em-up
title: "'Dimension Drive' ya disponible en Linux hoy d\xEDa de su lanzamiento"
---
El juego de 2Awesome Studio celebra el lanzamiento en acceso anticipado con un 10% de descuento

Tal y como [ya anunciamos](index.php/item/284-dimension-drive-un-interesante-shoot-em-up-llegara-el-proximo-16-de-febrero-en-acceso-anticipado), hoy se ha puesto a la venta el ambicioso juego de 2Awesome Studios 'Dimension Drive' [[web oficial](http://www.dimensiondrive.com/)]. Se trata de un "Shoot 'Em up" vertical ambientado en un "multiverso" con dos campos de batalla donde tendrás que manejar tu nave con sumo cuidado, cambiando de "universo" cuando sea necesario para progresar.


El juego recuerda a otros con el característico "bullet hell" solo que este juego promete llevar el concepto al límite al tener que estar pendiente de dos pantallas a la vez. También tendrás que resolver puzzles en tiempo real y combatir jefes finales en 4 modos de dificultad, incluyendo un "SPM" con muerte permanente.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/2tQw9K_beXQ" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


También podrás jugar en cooperativo junto a otro compañero en un modo en el que compartirás todo incluyendo las vidas y los recursos.


El juego se lanza en acceso anticipado, y en exclusiva durante este periodo 2Awesome Studios lanzará un nuevo nivel cada 2 semanas, así si te pasas el modo historia seguirás teniendo diversión.


¿Te atreves a probarlo?


Dimension Drive está disponible en español en su página de Steam, con un 10% de descuento por su lanzamiento:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/394430/" width="646"></iframe></div>

