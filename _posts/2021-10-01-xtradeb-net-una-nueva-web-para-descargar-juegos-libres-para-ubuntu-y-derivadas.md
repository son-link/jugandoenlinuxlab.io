---
author: leillo1975
category: Software
date: 2021-10-01 08:43:41
excerpt: "<p>El portal de juegos #OpenSource acaba de ponerse en marcha.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/xtradebnet.webp
joomla_id: 1371
joomla_url: xtradeb-net-una-nueva-web-para-descargar-juegos-libres-para-ubuntu-y-derivadas
layout: post
tags:
- open-source
- ubuntu
- xtradeb
- portal
title: XtraDeb.net, una nueva web para descargar juegos libres para Ubuntu y derivadas.
---
El portal de juegos #OpenSource acaba de ponerse en marcha.


 Hace casi un año os dábamos la noticia en exclusiva del nacimiento de un [Xtradeb PPA,]({{ "/posts/xtradeb-un-ppa-de-juegos-libres-para-ubuntu-y-derivadas" | absolute_url }}) un proyecto impulsado por **Jhonny Oliveira** para facilitar la instalación de juegos (y otros softwares) libres en **distribuciones derivadas de Ubuntu**. A lo largo de este tiempo, el catálogo de juegos a los que podemos instalar gracias a él ha ido creciendo, y su creador ha decidido dar el siguiente paso, y es poner en marcha un portal donde ver información sobre cada uno de esos juegos, y con un simple click instalarlos.


La idea probablemente  a muchos les sonará, y es que si recordais hace años existía **PlayDeb** (dentro de GetDeb), otro portal que realizaba la misma función, pero que con el tiempo se abandonó y se convirtió en "otra cosa". Tomando como referente esta web, y esta forma de instalar juegos (y software) libres, nace [Xtradeb.net](https://xtradeb.net/), que pretende que tengamos de una forma sencilla las **últimas versiones de nuestros juegos favoritos**.


Para poder descargar los juegos desde la web, deberemos previamente [añadir el repositorio](https://xtradeb.net/wiki/how-to-install-applications-from-this-web-site/) del que antes hablamos, y luego simplemente pulsar en "Install" en la sección correspondiente de cada juego que queramos descargar. Gracias a **aptURL**, esta comenzará inmediatamente. También [podemos solicitar que se incluyan más juegos](https://xtradeb.net/wiki/how-to-request-a-new-package/) (o software) libres al repositorio/portal si veis que no está el que buscais. 


Esto es solo el comienzo, ya que **el proyecto aun está en construcción**, pero pronto incluirá más y más cosas. Si quereis empezar por alguna, podeis hacerlo por descargar la última versión de [SuperTuxKart]({{ "/posts/supertuxkart-1-3-ya-esta-aqui" | absolute_url }}) que acaba de ser lanzada, y de la que os hablamos ayer.


 


¿Qué os parece la iniciativa de XtraDeb.net? ¿Érais usuarios de PlayDeb hace años? Si quereis contestarnos o dicirnos tu opinión sobre Xtradeb, hazlo en los comentarios, en nuestro grupo de JugandoEnLinux en [Telegram](https://t.me/jugandoenlinux), o nuestras salas de [Matrix](https://matrix.to/#/#jugandoenlinux.com:matrix.org).


 

