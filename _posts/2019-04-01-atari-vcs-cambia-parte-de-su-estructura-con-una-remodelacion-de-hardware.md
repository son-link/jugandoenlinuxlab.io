---
author: Pato
category: Hardware
date: 2019-04-01 07:03:36
excerpt: "<p>La nueva consola de la m\xEDtica compa\xF1\xEDa @TheAtari_VCS obtiene\
  \ nuevos detalles para mejorar su producci\xF3n</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/f59e5e3530c2c45e61d5a0c945f954cb.webp
joomla_id: 1011
joomla_url: atari-vcs-cambia-parte-de-su-estructura-con-una-remodelacion-de-hardware
layout: post
tags:
- hardware
- atari
- atarivcs
title: "Atari VCS cambia parte de su estructura con una remodelaci\xF3n de hardware"
---
La nueva consola de la mítica compañía @TheAtari_VCS obtiene nuevos detalles para mejorar su producción

Atari sigue en su tarea de lanzar la que será su primera consola en décadas y para ello está poniendo todo el empeño para que sea un producto a la altura de las expectativas. Si hace solo unas semanas [nos anunciaron](index.php/homepage/hardware/15-hardware/1120-atari-vcs-montara-un-procesador-con-nucleos-zen-y-una-grafica-vega) el cambio del procesador de la Atari VCS ahora nos muestran la remodelación que han llevado a cabo para "adecuar" los componentes en una nueva carcasa.


Lo que nos muestran es una remodelación del diseño industrial con el objetivo de mejorar tanto la refrigeración de los componentes como el ensamblaje de la consola ahorrando tiempo y materiales plásticos en su construcción:


![AtariVCSnr5](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Ataribox/AtariVCSnr5.webp)![AtariVCSnr6](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Ataribox/AtariVCSnr6.webp)![AtariVCSnr10](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Ataribox/AtariVCSnr10.webp)![AtariVCSnr3](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Ataribox/AtariVCSnr3.webp)![AtariVCSnr8](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Ataribox/AtariVCSnr8.webp)![AtariVCSnr9](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Ataribox/AtariVCSnr9.webp)![AtariVCSnr4](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Ataribox/AtariVCSnr4.webp)![AtariVCSnr7](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Ataribox/AtariVCSnr7.webp)![AtariVCSnr12](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Ataribox/AtariVCSnr12.webp)


Si os habéis fijado, llama la atención un par de detalles en relación con el anterior diseño. En primer lugar, la aparición de **dos conexiones USB 3.0 en la parte inferior del frontal de la consola** lo que supone que ya no tendremos que conectar todos los periféricos en la parte trasera (el anterior diseño tenía cuatro USB's en la parte trasera) y además **ha desaparecido la ranura de conexión de tarjetas SD**. Según el equipo de diseño se ha eliminado el soporte para dichas tarjetas dado que sería "redundante" con el uso de las conexiones USB 3.0 en los que se pueden conectar dispositivos de almacenamiento de alto rendimiento y ancho de banda sin perder prestaciones. Veremos si esta decisión es acertada.


![AtariVCSnr11](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Ataribox/AtariVCSnr11.webp)


Por otra parte, también se ha modificado la carcasa, pasando del anterior diseño "anillado" donde se debían ensamblar hasta 16 partes para dar forma a la carcasa, a **un nuevo diseño que solo se compone de cuatro partes**, simplificando mucho el ensamblaje, reduciendo el tiempo de fabricación y costes y mejorando la refrigeración interna de los componentes. Además, se han reducido las dimensiones de la consola, pasando a ser de 12,3" de anchura, 5,9" de profundidad y 2.0" de altura.


Por último, Nos dicen que **el logo frontal** que en un principio iba a ser solo un elemento estético con una iluminación pasiva ahora **va a ser un elemento más activo de la consola pasando a ser un indicador** que nos dará información o "mensajes" combinando diversos parpadeos o juegos de luces con información en pantalla.


![AtariVCSnr1](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Ataribox/AtariVCSnr1.webp)


Como viene siendo habitual, tienes toda la información de la nueva consola Atari VCS en el [post oficial de Atari en Medium](https://medium.com/@atarivcs/atari-vcs-structural-improvements-and-feature-adjustments-ba4ee3af5317?r=slt-eml-bck-a2e0).


¿Qué te parece el nuevo diseño de la Atari VCS? ¿Te gustan las novedades de esta nueva consola que vendrá con Linux bajo el capó?


Cuéntamelo en los comentarios o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


Estaremos atentos a las novedades que vayan surgiendo respecto a la Atari VCS, de la que esperamos saber detalles como su precio de venta al público general, sus características de sistema definitivas y su fecha de lanzamiento en los próximos meses, dado que **se espera su lanzamiento para algún momento de este mismo año**.

