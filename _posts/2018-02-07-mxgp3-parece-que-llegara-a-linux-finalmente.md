---
author: leillo1975
category: Carreras
date: 2018-02-07 14:27:10
excerpt: "<p>El port del juego de Motocross correr\xEDa a cargo de Virtual Programming.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/274f1bf2bd18e7f0d33d6244e658426e.webp
joomla_id: 637
joomla_url: mxgp3-parece-que-llegara-a-linux-finalmente
layout: post
tags:
- virtual-progamming
- motocross
- mxgp3
- milestone
title: "MXGP3 parece que llegar\xE1 a Linux finalmente."
---
El port del juego de Motocross correría a cargo de Virtual Programming.

Hacía bastante tiempo que había visitado la página de [Virtual Programming](https://www.vpltd.com/) y sabía de la existencia de este port, pero ciertamente lo tenía un tanto olvidado. Quizás las pocas publicaciones de este compañía de porting en [Twitter](https://twitter.com/virtualprog) tengan parte de culpa, y quizás también sea culpa mia por no vigilarlos más de cerca, pero lo cierto es que si no es por nuestro colega [Pato](index.php/component/k2/itemlist/user/192-pato) y por la información que ha publicado hoy [GamingOnLinux](https://www.gamingonlinux.com/articles/mxgp3-motocross-is-showing-new-signs-of-linux-support.11182/page=2#comments); pues no me habría enterado.


¿Y por qué si se sabe que se a portar el juego a nuestro sistema os venimos ahora con esta noticia? La respuesta es que hace bien poquito han aparecido una cambios en [SteamDB](https://steamdb.info/app/561600/history/?changeid=4025092) donde aparecen nuevos sistemas soportados, **Mac y Linux**, lo cual significa que el juego parece estar más próximo a su llegada que hace unos días. Si tomamos como buena la poca información que ofrece la página de sus "porters" el juego está desde hace meses en estado "alpha".


Para quien no lo conozca, [MXGP3](http://mxgpvideogame.com/) se estrenó en Windows en el Mayo pasado, y está desarrollado por [Milestone](http://milestone.it/?lang=en), una compañía italiana especialista en juegos de relaccionados con el motor, tanto de coches como motos. Esta es la descripción del juego en la tienda de Steam:



> 
> *¡Vive toda la adrenalina del motocross con el único videojuego oficial del campeonato! MXGP3 - The Official Motocross Videogame ofrece la experiencia de juego más cautivadora hasta el momento, con una jugabilidad y gráficos completamente nuevos gracias a Unreal® Engine 4. ¡Compite en 18 circuitos oficiales y el emocionante MXoN con todos los pilotos y todas las motocicletas de la temporada 2016 de MXGP y MX2 y sé el primero en experimentar la emoción de pilotar una de las 10 motocicletas de dos tiempos disponibles! ¡Haz que tu piloto y tu motocicleta sean únicos con más de 300 componentes oficiales con los que crear una personalización completa!*  
>   
> ***Inmersión total***  
> *Diseñado desde cero con el motor **Unreal® Engine 4**, dotado de gráficos y escenarios ultrarrealistas, increíbles detalles y espectaculares efectos visuales.*  
>   
> ***Solo contenido oficial***  
> *Licencias, circuitos y pilotos oficiales de la temporada **2016 del FIM Motocross World Championship**.*  
> *Disfruta de gran cantidad de opciones de personalización para tu moto y piloto utilizando **más de 300 elementos**.*  
>   
> ***Más diversión con los motores de 2 tiempos***  
> *Disfruta, por primera vez en la licencia, de la conducción con **motor de 2 tiempos**.*   
> *Elige entre **10 motos reales**, con sonido y movimientos increiblemente realistas.*
> 
> 
> 


Como veis se trata de un videojuego de lo más interesante y su propuesta sería única en nuestro sistema. Esperemos que finalmente llegue a buen puerto y más pronto que tarde lo tengamos en nuestras manos. También **estaría bien poder contar con más títulos de esta desarrolladora** como [Gravel](http://gravelvideogame.com/), [MotoGP](http://motogpvideogame.com/) o WRC en GNU-Linux/SteamOS. Parece que ultimamente los fanáticos del motor estamos de enhorabuena y los proyectos de juegos van llegando poco a poco ([Dakar 18](index.php/homepage/generos/carreras/item/729-los-desarrolladores-de-dakar-18-estan-valorando-la-posibilidad-de-traer-su-juego-a-gnu-linux-steamos), [DRAG](index.php/homepage/generos/carreras/item/735-pronto-veremos-drag-un-juego-de-carreras-en-acceso-anticipado)..) Os dejamos con el trailer del juego:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/uUxmQqamN1k" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


¿Como veis la posibilidad de poder pilotar una moto de Motocross en Linux? ¿Os parece que finalmente podremos verlo en nuestras pantallas? Responde en los comentarios o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

