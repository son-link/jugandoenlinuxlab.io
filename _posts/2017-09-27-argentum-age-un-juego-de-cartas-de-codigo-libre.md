---
author: Serjor
category: Estrategia
date: 2017-09-27 22:19:57
excerpt: '<p>Magic: The Gathering hecho open-source</p>'
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/daf2bbcc481f0cf083f5d1524716cf65.webp
joomla_id: 467
joomla_url: argentum-age-un-juego-de-cartas-de-codigo-libre
layout: post
tags:
- cartas
- open-source
- argentum-age
title: "Argentum Age, un juego de cartas de c\xF3digo libre"
---
Magic: The Gathering hecho open-source

Nos enteramos gracias a [reddit](https://www.reddit.com/r/opensourcegames/comments/72q3o4/introducing_argentum_age_an_open_source/?st=j83k0ku7&sh=1a2f471d) de la existencia de [Argentum Age](http://argentumage.com/), un juego de cartas coleccionables open-source.


Dentro de los objetivos de los desarrolladores está el tener un juego de cartas dónde no haga falta arruinarse para poder avanzar y ganar. Entre sus características están:


* Modo single player con historia, dónde se podrán conseguir bonificaciones para usar en el modo competitivo
* Modo online para jugar contra otros jugadores o modo espectador para ver partidas, ya sean single o multiplayer.
* El escenario está diseñado para que la estrategia del juego tenga que tener en cuenta los "combates" entre cartas ya que se pueden condicionar a través de "hechizos".
* Unas 200 cartas para poder usar, aunque van añadiendo más
* Las cartas están dibujadas a mano, lo cuál las hace estéticamente muy chulas.
* El motor permite ser modificado para crear a partir de él más juegos de cartas.
* El multi soporta juego cruzado entre GNU/Linux, Mac OS y Windows.


Sinceramente no conocía este juego, y ha sido una grata sorpresa haberlo descubierto. Si bien no soy fan de este tipo de juegos, es de agradecer iniciativas de este tipo, que además, viendo el vídeo que acompaña este artículo da la sensación de estar pulido.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/hXju4I_T33I" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Y tú, ¿te animas a echar unas partidas? Organiza un evento con nosotros a través de los comentarios, el foro o nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

