---
author: Son Link
category: "Acci\xF3n"
date: 2021-08-19 18:34:28
excerpt: "<p>@SelacoGame de Altered Orbit Studios llama la atenci\xF3n por lo que\
  \ consigue con este motor&nbsp;</p>\r\n"
image: https://cdn.cloudflare.steamstatic.com/steam/apps/1592280/ss_712798f86b89c8d2ce43bbcdbc69d0c7949acb3a.jpg
joomla_id: 1336
joomla_url: selaco-el-nuevo-fps-que-no-creeras-que-usa-gzdoom
layout: post
tags:
- accion
- fps
title: "Selaco, el nuevo FPS que no creer\xE1s que usa GZDoom"
---
@SelacoGame de Altered Orbit Studios llama la atención por lo que consigue con este motor 


A través del subredit **linux_gaming** me ha llegado esta noticia de [Gaming On Linux](https://www.gamingonlinux.com/2021/08/its-hard-to-believe-selaco-is-running-on-gzdoom-in-the-latest-3-minute-trailer) sobre Selaco, un nuevo FPS que aunque por al trailer parezca mentira, usa el motor libre para Doom, Doom II, etc, GZDoom. Antes yo ya había probado varios mods y conversiones totales, pero este juego va más allá.

Este nuevo juego **esta inspirado en F.E.A.R. y cuenta ademas con elementos clásicos de Doom y Quake y añadiendo características modernas.** La historia tiene lugar durante una violenta invasión de Selaco, la instalación subterránea que alberga a los refugiados supervivientes tras la caída de la Tierra. Juegas como Dawn, una capitana de seguridad de ACE que, con su reciente ascenso y sus autorizaciones de seguridad de alto nivel, está indagando en la verdad de la turbia historia de Selaco. Antes de que pueda terminar su investigación, Selaco se ve sacudida por explosiones e invasores armados. Aunque Selaco se desarrolla en un conflicto a gran escala, gran parte de él será la historia personal de Dawn, la pieza central del juego.

Para el desarrollo del juego han contado con la participación de expertos artistas, tanto para el desarrollo de los gráficos, la historia y la banda sonora para que Selaco sea un mundo completamente desarrollado, lleno de personajes, acción y misterio.

Nuevas características:

Este juego trae novedades nunca antes vistas en mods u otros juegos para GZDoom:

* **Cada bala es capaz de reaccionar a cada material y objeto**, los artistas han creado varias reacciones para objeto (voxel), por lo que una simple oficina puede acabar con papeles por los suelos, cuadros y monitores rotos, cristales por los suelos, montones de escombros, etc.
* **Inteligencia artificial personalizada**: Aquí cada enemigo es capaz de reaccionar e colaborar con el resto, pudiendo esconderse de tus disparos, acorralarte si te estas quedando sin munición, lanzarte granadas si estas escondido en el mismo sitio mucho tiempo, etc.e han escrito y representado cientos de líneas personalizadas que proporcionan pistas al jugador sobre sus planes y tácticas actuales.
* Una completa banda sonora compuesta por un músico que trabajo anteriormente en otros juegos
* **Doblaje profesional**: Han contado con la colaboración de profesionales del doblaje para sumergir al jugador en el juego. **Melissa Medina** es la voz de Dawn y poco a poco se ira revelando el resto del elenco.
* **Modos de juego adicionales**: Al completar los niveles, podrás volver a jugarlos en el modo Incursión, una versión ampliada de ese mapa en la que tus habilidades se pondrán a prueba contra una avalancha casi infinita de enemigos. Gana mapas de incursión para obtener bonificaciones que te ayudarán a lo largo de la campaña. También se desbloquearán nuevos niveles de incursión exclusivos y nuevos mutadores a medida que avance la campaña.

El juego aun no tiene fecha de lanzamiento, pero yo al menos ya tengo ganas jugarlo. El juego **estará disponible a través de [Steam](https://store.steampowered.com/app/1592280/Selaco/?curator_clanid=4218320) y de manera nativa para nuestro sistema**.

Os dejo con un trailer de 3 minutos que fue publicado durante el **Realms Deep 2021**, un evento anual de **3D Realms** (si, los de **Duke Nukem**), junto a **1C Entertainment**, **Devolver Digital** o **Nightdive Studio**s y donde presentan sus juegos y todos aquellos otros que nos llevan de vuelta a los FPS de los 90.

En cuanto sepa nuevos detalles, así como fecha de lanzamiento y precio, iré informando.

<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube-nocookie.com/embed/ozxnmQyzll4" title="YouTube video player" width="780"></iframe></div>

Cuentame que te parece este nuevo juego en los canales habituales de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).
