---
author: Serjor
category: Noticia
date: 2019-08-07 18:54:19
excerpt: <p>Aunque falta una de las piezas claves para un buen rendimiento a la hora
  de jugar</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/eb28519930c54357ccc74f171853378d.webp
joomla_id: 1096
joomla_url: nvidia-comienza-otra-vez-a-liberar-informacion-sobre-el-hardware-de-sus-gpus
layout: post
tags:
- drivers
- nvidia
title: "NVIDIA comienza (otra vez) a liberar informaci\xF3n sobre el hardware de sus\
  \ GPUs"
---
Aunque falta una de las piezas claves para un buen rendimiento a la hora de jugar

Como viene siendo habitual, y cosa que agradecemos enormemente, vía nuestro canal de [Telegram](https://t.me/jugandoenlinux) nos enteramos de que en Phoronix [anuncian](https://www.phoronix.com/scan.php?page=news_item&px=NVIDIA-Open-GPU-Docs) que NVIDIA ha retomado su promesa del pasado, y ha empezado a liberar documentación sobre el hardware de sus tarjetas gráficas.


Por ahora, y según anuncian, irán liberando la documentación poco a poco, por lo que por el momento solamente de las familias Maxwell, Pascal, Volta, y Kepler, a las gráficas de la familia Turing les toca esperar. De igual manera toca esperar para tener información sobre uno de los aspectos que más nos interesa de cara a poder jugar usando drivers libres, y es que la información liberada no contiene los datos necesarios para poder ajustar la frecuencia de trabajo de la gráfica, por lo que seguimos sin el aumento de velocidad tan necesario para que los drivers nouveau sean una alternativa viable.


No obstante, es de agradecer que hayan dado este paso y esperemos que el flujo de información sobre su hardware no pare de tal manera que podamos tener un escenario similar al de los poseedores de gráficas AMD e Intel, y podamos disfrutar de nuestros juegos favoritos haciendo uso de drivers libres.


Si quieres echar un vistazo a la documentación, puedes hacerlo visitando su repositorio en [GitHub](https://github.com/nvidia/open-gpu-doc)


Y tú, ¿qué piensas de este movimiento por parte de NVIDIA? Cuéntanoslo en los comentarios o en nuestros canales de [Telegram](https://t.me/jugandoenlinux) y [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org)

