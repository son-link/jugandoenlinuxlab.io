---
author: Serjor
category: "Acci\xF3n"
date: 2017-05-16 18:24:30
excerpt: <p>Nuevas funcionalidades, nuevas armas, nuevas opciones...</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/ce742d950ca63a98d59ecec5eba0da2e.webp
joomla_id: 324
joomla_url: ballistic-overkill-se-actualiza-para-dar-soporte-a-vulkan
layout: post
tags:
- fps
- ballistic-overkill
title: Ballistic Overkill se actualiza para dar soporte a Vulkan
---
Nuevas funcionalidades, nuevas armas, nuevas opciones...

Si hace poco más de un mes os [anunciábamos](index.php/homepage/generos/accion/item/394-ballistic-overkill-abandona-la-fase-de-acceso-anticipado) que Ballistic Overkill salía de su Early Access para pasar a la versión estable del juego, hoy os tenemos que anunciar que tras una actualización de casi 1Gb, Ballistic Overkill se ha actualizado para dar soporte a Vulkan.


Y no tan solo para Vulkan, en Mac dan soporte para Metal, y todo esto es posible gracias a que están haciendo uso de la última versión de Unity3D, la cuál como ya os avanzábamos por [aquí](index.php/homepage/editorial/item/395-liberada-la-version-5-6-de-unity3d-con-un-mejor-soporte-para-vulkan), mejoraba su soporte para Vulkan.


Además de soportar dos nuevas APIs gráficas, la lista de cambios es enorme y puedes encontrarla [aquí](http://store.steampowered.com/news/externalpost/steam_community_announcements/2423345440423938298).


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" frameborder="0" height="315" src="https://www.youtube.com/embed/uq79CxIZmYs" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


No podemos dejar de recomendaros este juego, el cuál ya funcionaba a las mil maravillas con OpenGL, y sinceramente, en cuanto termine de escribir este artículo, voy a probar a ver qué tal rinde con esta nueva API ;-).


 


<div class="steam-iframe"><iframe frameborder="0" height="190" src="https://store.steampowered.com/widget/296300/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


¿Te animas a una partida a Ballistic Overkill? Déjanos tu reto o en los comentarios o en nuestros canal de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD).

