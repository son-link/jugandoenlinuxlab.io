---
author: Pato
category: "An\xE1lisis"
date: 2017-03-10 16:25:07
excerpt: <p>Analizamos el galardonado juego de Salmi Games donde puzzles y habilidad
  se dan la mano</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/ee760d1c1474fde7f8f71a96b2452e29.webp
joomla_id: 214
joomla_url: analisis-ellipsis
layout: post
tags:
- indie
- steam
- puzzles
- analisis
- ellipsis
title: "An\xE1lisis: Ellipsis"
---
Analizamos el galardonado juego de Salmi Games donde puzzles y habilidad se dan la mano

*Agradecemos a Salmi Games la clave del juego para este análisis.*


Ellipsis [[web oficial](http://playellipsis.com/)] es de esos juegos que son fáciles de jugar y que cuando te vas a dar cuenta le has metido unas cuantas horas. El concepto del juego es simple. Se trata de un juego de puzzles de habilidad donde tienes que liberar primero y recoger después unos "puntos" que están encerrados dentro de unas cápsulas, evitando tocar cualquier elemento "sensible". Así a priori parece sencillo pero cuando van pasando los niveles la cosa se va complicando. No solo eso, si no que la dificultad que ofrecen los mismos puzzles es "dinámica", como veremos más adelante.


Lo primero que llama la atención es lo minimalista de la interfaz y los gráficos. No hay tutorial, pero sinceramente no le hace falta, ya que los primeros niveles sirven perféctamente para introducirte en las mecánicas del juego de manera que comienzas haciendo puzzles realmente simples que te sirven para saber cómo desenvolverte, y poco a poco vas aprendiendo nuevas mecánicas y estratégias a medida que vas haciendo puzzles cada vez más complejos. Por que, eso sí, algunos niveles llegan a ser complejos de verdad.


![Ellipsis2](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisEllipsis/Ellipsis2.webp)


*La interfaz minimalista solo muestra iconos. Ni una línea de texto, pero realmente no le hace falta para ser funcional*


Prepárate para repetir algunas fases una y otra vez, y estudiar el entorno. A veces los puzzles se parecen más a "esquivar disparos" que a moverte de aquí para allá, pero precísamente eso es lo que hace a Ellipsis diferente, y es lo que ha hecho a algunos a pensar que el juego es de tipo "bullet hell", sin embargo yo lo considero más un juego de **puzzles "dinámicos"**. Ellipsis es capaz de mezclar con acierto fases mas estáticas y que aparentemente son mas simples con otras que repetirás y repetirás hasta que consigas hacerte con la mecánica adecuada. Además de esto, los puzzles también tienen su "nivel de dificultad". Me explico:


Cuando comienzas cada una de las pantallas o puzzles, debes "tocar" los círculos que contienen los "puntos" para ser liberados y posteriormente poder recogerlos. Estos puntos luego serán la referencia para determinar el nivel de "completado" de cada puzzle, cosa que se reflejará mediante un número de estrellas que aparecerán sobre la fase una vez finalizado el puzzle con éxito. Para finalizar los puzzles habrá que atravesar un "portal" que se activará una vez conseguido un número concreto de círculos.


![Ellipsis4](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisEllipsis/Ellipsis4.webp)


Cada puzzle puede resolverse "rompiendo" un número determinado de círculos contenedores y recogiendo los puntos correspondientes, pero en culaquier caso siempre hay "círculos" más difíciles de alcanzar de modo que sea un reto superior el conseguir todos los puntos para luego obtener todas las "estrellas" de la fase. Se entenderá mejor con un vídeo gameplay:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/JD_6BbkogpE" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


 


Ellipsis puede jugarse tanto con mando como con el ratón. Yo lo he probado con ambos, y si bien con el mando se deja jugar, con el ratón se adquiere una mayor agilidad a la hora de moverte por la pantalla de modo que ciertos puzzles que con mando pueden ser de un nivel considerable, con el ratón se solventan con mayor facilidad.


En cuanto a su apartado técnico, el juego destaca por sus gráficos tipo "neon". Todos los elementos móviles o que no se pueden tocar destacan sobre las líneas blancas y el fondo negro. También hay fases en las que los gráficos te ofrecen "pistas" sobre como afrontar un determinado reto, como por ejemplo fases donde un "viento" te empuja en direcciones opuestas a las que en principio debes seguir, y que puedes adivinar gracias al movimiento de partículas o de otros elementos móviles. Lo cierto es que el resultado es positivo, ofreciendo algunas pantallas realmente atractivas y brillantes.


Por otra parte, el sonido no destaca demasiado pero cumple complementando al entorno, con sonidos futuristas que reflejan tus movimientos. La música es sencilla, y sirve como acompañamiento general.


![Ellipsis1](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisEllipsis/Ellipsis1.webp)


*Cada vez que ejecutamos un movimiento se puede "oir"*


En cuanto a su funcionamiento en Linux, debo decir que salvo un CTD que no pude volver a reproducir posteriormente el juego no ha deparado ni un solo problema. Además, los requisitos mínimos son realmente bajos, con lo que he podido ejecutarlo tanto en mi PC gaming como en un portatil con un procesador intel core i3 2350M y los drivers Mesa que vienen por defecto en Ubuntu 16.04 LTS sin mayores problemas y de forma fluida.


Por último destacar que el juego realmente es entretenido. La mezcla de habilidad con puzzles está resuelta de forma muy acertada ofreciendo momentos brillantes, y donde un puzzle que se te resiste se convierte en pura satisfacción cuando lo consigues resolver. No en vano Ellipsis ha sido galardonado en diversas ferias y eventos, por que realmente cumple con su cometido.


Si quieres jugar a Ellipsis puedes encontrarlo en su página de Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/514620/" width="646"></iframe></div>

