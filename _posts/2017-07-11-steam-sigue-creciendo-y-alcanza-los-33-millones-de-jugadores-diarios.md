---
author: Pato
category: Editorial
date: 2017-07-11 08:50:33
excerpt: "<p>El n\xFAmero de jugadores en Linux estar\xEDa creciendo en consecuencia</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/6f7af10029bcf59a35635b8213d55753.webp
joomla_id: 404
joomla_url: steam-sigue-creciendo-y-alcanza-los-33-millones-de-jugadores-diarios
layout: post
tags:
- steamos
- steam
- linux
title: Steam sigue creciendo y alcanza los 33 millones de jugadores diarios
---
El número de jugadores en Linux estaría creciendo en consecuencia

Cada vez que Steam libera sus estadísticas mensuales ([aquí las de Junio](http://store.steampowered.com/hwsurvey/)) vemos como Linux sigue más o menos estancado entre un 0,7% y un 0,8% de cuota de usuarios. Sin embargo esto no significa que el número de usuarios esté estancado, ni mucho menos. Mas bien al contrario, la tendencia que se adivina es que el número de jugadores en Linux está creciendo, aunque lentamente. ¿Como llegamos a esta conclusión?


Durante este fin de semana pasado y comienzo de semana se han estado viendo informaciones sobre una serie de conferencias que Valve ha estado realizando en distintos lugares enfocadas a desarrolladores, y donde se han podido ver datos bastante interesantes. En concreto [se han publicado imágenes](http://www.valvetime.net/threads/steam-news-from-indigo-2017.257832/) con varios datos, como el crecimiento de ventas por usuario que ha experimentado una gran subida este pasado año o el porcentaje de cuotas de mercado según las regiones, pero lo interesante de verdad está en la información de la media de jugadores diarios y mensuales:


![valvegrow](http://www.valvetime.net/attachments/large15-jpg.25977)


33 millones de jugadores activos diarios, y 67 millones de jugadores mensuales. ¡Ahí es nada!.


Dejando de lado la fiabilidad o casualidad de la encuesta de hardware de Steam, o si tiene en cuenta a todas las distros Linux o si aparece más o menos que en otros sistemas, lo cierto es que el crecimiento de Steam es significativo, y sin saber realmente el número de cuentas de Steam, el 0,72% de cuota de Linux de este último mes da para algunas interpretaciones.


Si tenemos en cuenta que [los datos conocidos](http://expandedramblings.com/index.php/steam-statistics/) de números de usuarios activos eran unos 125 millones en 2015, y estimando un crecimiento anual de 2 millones por año (según los datos deberían ser mas) deberíamos estar en torno a unos 130 millones de usuarios en estos momentos. El hecho de que la cuota de Linux siga estando en los porcentajes que estaba después de este crecimiento supone que el número de usuarios de Linux ha debido crecer en un porcentaje similar (algo menos, dado que en este último año el mercado asiático ha entrado con fuerza en Steam haciendo bajar un poco las estadísticas y desviando el resultado), y eso significa que haciendo unas sencillas cuentas nos sale que ¡somos algo más de 900.000 usuarios jugando en Linux!.


Aparte de esto, En dichas conferencias se ha estado hablando de las próximas novedades que traerá Steam próximamente, como la nueva interfaz, un renovado apartado para los "curators", nuevas funcionalidades en cuanto a publicaciones multimedia y más.


Si quieres ver una de estas conferencias, Unity ha publicado un vídeo de la Unite Europe 2017:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/HMxHxP1hVjU" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


 ¿Qué te parece el crecimiento de Steam? ¿Piensas que Linux está creciendo en cuanto a jugadores, o crees que es al contrario? ¿Te gustan las novedades que anuncia Steam para un futuro próximo?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 Fuente: <http://www.valvetime.net>

