---
author: Pato
category: Editorial
date: 2021-05-25 22:21:00
excerpt: "<p>Los indicios apuntan a alg\xFAn tipo de consola impulsada por un sistema\
  \ Linux</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/headers/Valve.webp
joomla_id: 1308
joomla_url: valve-comienza-a-dar-senales-de-que-tiene-un-nuevo-proyecto-de-hardware-entre-manos
layout: post
tags:
- steamos
- steam
- valve
- hardware
- rumor
title: "Valve comienza a dar se\xF1ales de que tiene un nuevo proyecto de hardware\
  \ entre manos"
---
Los indicios apuntan a algún tipo de consola impulsada por un sistema Linux


Si eres de los que estás al día de lo que se cuece en el mundillo del videojuego sabrás que hoy ha explotado una bomba en forma de rumor. Pero lo primero que hemos de decir es que a falta de información oficial **todo lo que se publique sobre este asunto, incluidas estas líneas debes tomarlas con cautela** ya que no hay nada confirmado y todo lo que sabemos es en buena parte gracias a filtraciones o rumores que puede que estén equivocados en todo o en parte. 


Dicho lo cual, trataremos de arrojar algo de luz sobre lo que se sabe, y por último daré mi propia opinión personal. Si quieres saber de qué se trata, coge asiento y sígueme a través de las siguientes líneas.


Todo comienza el pasado 11 de Mayo cuando preguntaron en un foro a Gabe Newell si Valve tenía planeado algún tipo de lanzamiento en consolas, a lo que el mandamás de Valve [contestó](https://twitter.com/SteamDB/status/1392177546318553091) que debíamos esperar a final de este año, pero que **cuando se produjese probablemente no sería lo que nos esperábamos**. Tras aquellas declaraciones los rumores acerca de lo que podrían significar sus palabras se dispararon especulando si sería Steam o algún juego de los anunciados o por anunciar lo que terminaría llegando a alguna de las consolas que hay en el mercado. Muchos incluso plantearon la posibilidad de que Half Life Alix, el sobresaliente juego de realidad virtual de Valve terminase llegado a Playstation VR. 


Sin embargo, todo parece haber dado una vuelta de tuerca esta mañana tras un [tweet de Steam Database](https://twitter.com/SteamDB/status/1397098304765366272) en el que su responsable **Pavel Djundik** afirmaba haber encontrado en el código de la última beta del cliente de Steam referencias a un proyecto con nombre en código "**Neptune**" y **Neptune Games Collection**, con multitud de referencias a mapeado de mandos y cadenas relacionadas a una interfaz de usuario como "menú de acceso rápido, **configuración de sistema (modo avión, wifi, bluetooth)** y menú de encendido. También se introdujo una referencia relacionada como "**juegos optimizados para el dispositivo**".


En referencia a Neptune (apareció como referencia en la pasada [actualización de Septiembre](https://github.com/SteamDatabase/SteamTracking/commit/db9118c45bd2d603ea611fe996c44495d5f52a20)), el nombre asociado parece ser **SteamPal**, nombre que aparece en el código y que **Pavel asocia a un dispositivo "handheld" o consola portatil.**Además, también se encuentran referencias a una especie de categoría o filtro asociado a "**SteamPal Games**" (GameList_View_NeptuneGames) y un programa para partners llamado **Callisto partner program**. Demasiadas referencias para ser algo sin relevancia. ¿Quieres verlo por ti mismo? [Aquí lo tienes](https://github.com/SteamDatabase/SteamTracking/blob/3d8e7591e4464298b89c98d72a6c1b4a697db9af/ClientExtracted/steamui/6.js).


Tras todo el revuelo que se ha montado en distintos medios nacionales e internacionales, el portal [Ars Technica](https://arstechnica.com/gaming/2021/05/exclusive-valve-is-making-a-switch-like-portable-gaming-pc/) afirma haber contactado con "multiples fuentes relacionadas con el proyecto" que afirman que llevan desarrollando en secreto desde hace un tiempo y que **se trataría de una "consola portatil" al estilo Switch con sistema Linux** y con capacidad de ejecutar un gran número de títulos. Hasta aquí, lo que sabemos.


Ahora entramos en el terreno de la especulación. Desde mi punto de vista **está claro que Valve tiene algún tipo de dispositivo entre manos**. No es casualidad todo el esfuerzo que lleva haciendo en el ecosistema Linux para no sacarle provecho. Ya lo hemos dicho hasta la saciedad.


Por otra parte, muchos dicen que sea lo que sea fracasará como fracasaron las Steam Machines. Sin embargo, hay que decir que aquellos "PCs consolizados" fueron un quiero y no puedo por diversos motivos, entre los que se encontraban que eran máquinas diseñadas por diversas empresas sin estar bajo el paraguas de Valve, y con un SteamOS demasiado verde para la época que no pudo cumplir con el cometido de ejecutar un número decente de videojuegos y atraer nuevos e importantes desarrollos. Hoy día ni que decir tiene que la situación ha cambiado mucho. **Entre juegos nativos, ports y lo que aporta Proton** podemos decir que los juegos que se mueven en nuestro sistema se cuentan por miles si no por decenas de miles.


Por otra parte, una "consola" PC portatil no es un proyecto descabellado, ya que de hecho ya existen dispositivos de este estilo como las GPD Win, que ya están en el mercado y funcionan. Si además está desarrollada por la propia Valve, en colaboración con Intel o AMD (Nvidia juega a su propio juego con las Shield) y son capaces de meter hardware decente y características interesantes puede que sea el empujón que necesita Valve para hacer "despegar" nuevo hardware y con ello el juego en Linux. Además, experiencia no les falta; ahí están las [Valve Index](https://store.steampowered.com/sub/354231) como muestra del éxito a la hora de lanzar hardware por parte de los de Bellevue.


También es verdad que puede que el proyecto no llegue a nada. No sería la primera vez que Valve cancela un proyecto, aunque tras las señales que estamos viendo, todo parece indicar que Gabe sabía a lo que se refería en referencia a las consolas. En cualquier caso, y si es cierto el plazo que dio en dicha conferencia, **a finales de año saldremos de dudas**. 


Como siempre, en jugandoenlinux.com seguiremos las noticias que vayan surgiendo respecto a SteamPal o como quiera que llamen a la criatura.


¿Y tu que piensas? ¿Crees que tendría éxito una "consola PC portatil" con sistema Linux y con el logo oficial de Valve -Steam?


Cuéntamelo en los comentarios o en los canales de jugandoenlinux de [Telegram](https://t.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).


 

