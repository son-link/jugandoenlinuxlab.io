---
author: Pato
category: Plataformas
date: 2018-05-23 16:36:27
excerpt: "<p>Lo nuevo de @JanduSoft es un plataformas de acci\xF3n. \xBFTe gusta el\
  \ caf\xE9?</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/cc7d5309aa94aa0d0b6c12a9b8c6f0e5.webp
joomla_id: 745
joomla_url: super-interactive-ninja-ya-esta-disponible-en-linux-steamos
layout: post
tags:
- accion
- indie
- plataformas
title: "'Super Interactive Ninja' ya est\xE1 disponible en Linux/SteamOS"
---
Lo nuevo de @JanduSoft es un plataformas de acción. ¿Te gusta el café?

Siempre es una buena noticia el anunciar el lanzamiento de un juego de un estudio español. En este caso hablamos de 'Super Interactive Ninja', un juego de acción y plataformas a raudales con una dificultad a la altura de los clásicos.


*Super Hyperactive Ninja es un juego de acción/plataformas en 2D de ritmo frenético e hipercafeinado, diseñado para aquellos que buscan un desafío de verdad. ¡Recupera el café robado antes de caer dormido! ¡Usa el poder de la cafeína para abrirte camino!*


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/5Xb-AvIzg0E" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 Prepárate para poner a punto tus habilidades de coffee-ninja, ¡las necesitarás para este plataformas hardcore!  
  
*Super Hyperactive Ninja* es un juego de **acción/plataformas en 2D de ritmo frenético e hipercafeinado**, diseñado para aquellos que buscan un **desafío de verdad** y los **speedrunners**.


El malvado Shogun ha robado todo el café de la villa secreta de Kohinomura, ¡recupéralo antes de caer dormido!  
  
Toma el control de **Kohimaru**, el último Coffee-Nin, para derrotar al malvado ejército de ninjas y samurais del Shogun y a sus grandes generales Yokai.


Con un argumento así, ¿quién se resiste?


Tienes ya disponible 'Super Hyperactive Ninja' en español en su página de Steam con un 15% de descuento por su lanzamiento:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/743330/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 

