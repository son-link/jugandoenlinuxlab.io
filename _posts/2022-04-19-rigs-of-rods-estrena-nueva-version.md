---
author: leillo1975
category: "Simulaci\xF3n"
date: 2022-04-19 14:15:41
excerpt: "<p>El simulador de f\xEDsicas en veh\xEDculos #OpenSource&nbsp;<span class=\"\
  css-901oao css-16my406 r-poiln3 r-bcqeeo r-qvutc0\">@rigsofrods</span> se actualiza\
  \ a la 2022.04.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/RigsOfRods/RigsOFRods.webp
joomla_id: 1461
joomla_url: rigs-of-rods-estrena-nueva-version
layout: post
tags:
- open-source
- rigs-of-rods
- ogre
title: "Rigs of Rods estrena nueva versi\xF3n"
---
El simulador de físicas en vehículos #OpenSource @rigsofrods se actualiza a la 2022.04.


 Hace más o menos un año que [os hablamos]({{ "/posts/nueva-version-de-rigs-of-rods" | absolute_url }}) de este **particular juego de simulación**, y para ser justos hay que destacar que sus desarrolladores, a parte de seguir trabajando han estado lanzando versiones a lo largo de estos meses, pero en esta ocasión, dada la relevancia de esta última, hemos decidido dedicarle de nuevo otro merecidisimo espacio en nuestra web.


Para quien no conozca [Rigs of Rods](https://www.rigsofrods.org/), hay que decir que no se trata de un juego convencional, sinó que se trata de una plataforma que **nos permitirá experimentar las físicas todo tipo de vehículos** (coches, camiones, barcos, trenes, aviones....) **en diferentes entornos**. Además de conducirlos y ver como se comportan, Rigs of Rods posee un **complejo sistema de colisiones**, donde podremos observar como se deforman y rebotan de forma realista los objetos.


![RigsOfRods repo](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/RigsOfRods/RigsOfRods_repo.webp)


Este proyecto, que usa la **licencia GPL-3.0**, está programado mayoritariamente en **C++** y desarrollado usando el [motor de renderizado Ogre.]({{ "/posts/nueva-actualizacion-del-motor-de-renderizado-libre-ogre" | absolute_url }}) En esta ocasión, entre los muchos cambios destaca la **posibilidad de navegar en su repositorio, e instalar y gestionar los diferentes mods desde dentro del propio juego**, lo que facilitará mucho su uso. Esta sería la l[ista de cambios](https://forum.rigsofrods.org/threads/rigs-of-rods-2022-04-released.3521/) con respecto a la anterior versión:


* *Explora, descargua y actualiza mods desde el repositorio sin necesidad de interactuar con Zips*
* *Se agregó soporte básico para scripts de complementos con AngelScript.*
* *Se solucionó un problema por el cual el personaje del jugador parpadeaba en FPSs altos.*
* *Al buscar en el Selector en una determinada categoría, la barra de búsqueda ahora etiquetará correctamente en qué categoría está buscando.*
* *Se solucionó un problema de larga data de "parpadeo" con PSSM en Intel, AMD y Nvidia que afectaría al personaje del jugador, ciertos terrenos y vehículos.*
* *Ahora se tolerará un corchete de cierre faltante en el formato de archivo .skin, con una advertencia*
* *Se ha corregido otro problema de larga duración por el que el cielo negro se representaba en los espejos cuando se utilizaba el cielo de Caelum.*
* *Se ha corregido un problema por el que los vehículos con pieles eran de color rosa tras la recarga.*
* *Se ha corregido un problema por el que se producía un fallo al eliminar un actor mientras se mantenía la tecla de retroceso.*
* *Ahora se registran los errores de análisis de los formatos de archivo TERRN2/OTC.*
* *El cursor del ratón ya no aparece al cargar un terreno*
* *Se han reintroducido los iconos antiguos como parte de la limpieza de `putMessage()`.*
* *Varios cambios de quality-of-life en diversos aspectos de la interfaz de usuario.*
* *Se ha añadido el ID del jugador a la lista de jugadores.*
* *Se ha corregido la opción de CMake para especificar una versión personalizada.*
* *Varias mejoras en quality-of-life de CMake.*
* *Se han añadido accesos directos a las funciones de AngelScript en la consola.*
* *Se ha actualizado el Doxygen para reflejar varios cambios internos y actualizaciones de AngelScript.*
* *Se ha corregido el indicador de calidad de la red para que no se muestre sólo parcialmente y no se reinicie tras la desconexión.*
* *Reemplazado los spinners con un spinner singular más moderno.*
* *Se ha corregido un problema con el ciclo NewFrame()/EndFrame() de Dear ImGui.*
* *Se ha añadido más relleno a los botones de acceso directo para una mejor visibilidad en los controles.*
* *Corregido el uso de las bibliotecas del sistema.*
* *Actualizado AngelScript a 2.35.1.*
* *Se ha corregido un problema por el que un tiempo de espera provocaba una salida*


Si quereis conseguir el juego lo podeis descargar desde [itch.io](https://rigs-of-rods.itch.io/rigs-of-rods), o compilar su código fuente desde su [web del proyecto](https://github.com/RigsOfRods/rigs-of-rods).  Os dejamos con el trailer oficial, que tiene ya unos añitos pero que es muy descriptivo de lo que podemos encontrar en el juego:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/bRbQ4OaljWs" style="display: block; margin-left: auto; margin-right: auto;" width="780"></iframe></div>


También hace algún tiempo grabamos este video probando el juego y que podeis ver a continuación:  
  
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/FgFiMW2OE2k" style="display: block; margin-left: auto; margin-right: auto;" width="780"></iframe></div>

