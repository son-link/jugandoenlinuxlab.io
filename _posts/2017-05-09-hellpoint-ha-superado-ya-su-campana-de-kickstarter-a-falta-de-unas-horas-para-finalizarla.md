---
author: Pato
category: Rol
date: 2017-05-09 08:18:08
excerpt: "<p>El estudio \"Cradle Games\" ha migrado la gesti\xF3n de su demo a Steam</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/4653e069ed7369840191e8bf38ab8dc9.webp
joomla_id: 309
joomla_url: hellpoint-ha-superado-ya-su-campana-de-kickstarter-a-falta-de-unas-horas-para-finalizarla
layout: post
tags:
- accion
- indie
- aventura
- rol
- demo
- terror
- kickstarter
title: "'Hellpoint' ha superado ya su campa\xF1a de Kickstarter a falta de unas horas\
  \ para finalizarla"
---
El estudio "Cradle Games" ha migrado la gestión de su demo a Steam

El prometedor juego de rol y acción 'Hellpoint' del que [ya os hablamos en otro artículo](index.php/homepage/generos/rol/item/402-hellpoint-un-llamativo-rpg-espacial-con-tematica-oscura-esta-en-campana-en-kickstarter) ha conseguido financiarse con éxito en su campaña de Kickstarter a falta de unas horas para terminar su campaña. Además, como ya dijimos anteriormente también superó con éxito una campaña en Greenlight, con lo que las espectativas de que llegue a buen puerto son vastante altas. 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/10Cm-iZdSRM" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


 En cuanto a Linux, el estudio ya ofrecía una versión demo del juego para poder probarlo, y ahora han movido la gestión de las demos a su página de Steam con el fin de mejorar la gestión de las versiones y el ingente tráfico que las descargas les genera. Si quieres probar la demo para ver qué tal te va, puedes hacerlo desde su página de Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/628680/" width="646"></iframe></div>


Y si quieres aportar en su campaña, aún tienes unas horas para poder hacerlo en el siguiente enlace:


<div class="resp-iframe"><iframe height="420" src="https://www.kickstarter.com/projects/hellpoint/hellpoint-a-dark-sci-fi-rpg/widget/card.html?v=2" width="220"></iframe></div>


 

