---
author: leillo1975
category: "Simulaci\xF3n"
date: 2018-10-04 10:22:52
excerpt: "<p>@SCSsoftware acaba de publicar esta expansi\xF3n</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/b0776ea0ff3c7ec4ba82e898c021ccc5.webp
joomla_id: 868
joomla_url: oregon-la-nueva-dlc-de-american-truck-simulator-ya-esta-disponible
layout: post
tags:
- dlc
- american-truck-simulator
- scs-software
- oregon
title: "\"Oregon\", la nueva DLC de American Truck Simulator, ya est\xE1 disponible."
---
@SCSsoftware acaba de publicar esta expansión

Si echamos la vista un poco atrás, la última vez que American Truck Simulator "creció" fué hace hace casi un año, concretamente el [2 de Noviembre del año pasado](index.php/homepage/generos/simulacion/item/639-la-dlc-de-nuevo-mexico-para-american-truck-simulator-disponible-en-una-semana). En esa ocasión se trataba del estado de Nuevo México, el cual [analizamos](index.php/homepage/analisis/item/656-analisis-american-truck-simulator-dlc-new-mexico) poco después. Tras este no muy dilatado "parón", y tras varias actualizaciones importantes ([1.31](index.php/homepage/generos/simulacion/item/855-ets2-y-ats-se-actualizan-a-la-version-1-31) y [1.32](http://blog.scssoft.com/2018/09/ats-and-ets2-update-132.html)), los camioneros virtuales ya echábamos de menos el poder incarle el diente a nuevos territorios en el hermano americano de ETS2. Hoy mismo el estudio checo SCS Software, anunciaba en las redes sociales al esperada salida de esta expansión:



> 
> ? American Truck Simulator: Oregon DLC is now released and available for everyone on Steam! ?<https://t.co/jkjmTEQ6Xj> [pic.twitter.com/7WxIqM29FG](https://t.co/7WxIqM29FG)
> 
> 
> — SCS Software (@SCSsoftware) [October 4, 2018](https://twitter.com/SCSsoftware/status/1047853549525327872?ref_src=twsrc%5Etfw)



Con el crecimiento hacia el sur del mapa de ATS, podremos por fin disfrutar de paisajes más verdes, profundos bosques y nuevas industrias, principalmente madereras que enriquecerán aun más la experiencia de conducir nuestros preciados camiones por la costa oeste americana. La descripción que podemos encontrar en Steam de esta expansión es la siguiente (traducción online):  
  




> 
> *Siga el camino para experimentar la belleza salvaje de "Beaver State" Oregon. El paisaje natural siempre verde irá de la mano con el paisaje desértico del campo, pero también con zonas urbanas densas como Portland o Salem. Los vastos bosques cubren aproximadamente el 60 por ciento del estado y junto con muchos ríos forman la naturaleza típica del oeste de Oregón, pero también son una parte importante de la economía del estado.*
> 
> 
> *Es uno de los estados más diversos geográficamente en la región noroeste de los EE.UU. Es el noveno más grande y, con una población de 4 millones, el 27º estado más poblado. Su belleza natural incluye bosques profundos, abundantes áreas de agua y ríos, pero también volcanes, desiertos altos y matorrales áridos.*  
> *Características principales*
> 
> 
> *Más de 5.000 millas de nuevas carreteras en el juego*  
>  *14 ciudades principales, incluyendo Portland, Salem y Eugene*  
>  *13 paradas grandes para estacionamiento y reabastecimiento de combustible.*  
>  *Muchas áreas de descanso más pequeñas y moteles alrededor de las carreteras*  
>  *Más de 700 activos 3D completamente nuevos*  
>  *25 uniones únicas, complejas y realistas construidas a medida e intercambiadores interestatales*  
>  *17 nuevos muelles de empresas e industrias locales*  
>  *Monumentos conocidos tanto naturales como artificiales - Mt. Hood, Thor's Well, Crater Lake, Crooked River Valley, Yaquina Head Lighthouse, Youngs Bay Bridge...*  
>  *Logros de Oregon para desbloquear*
> 
> 
> 


En JugandoEnLinux os ofreceremos esta misma noche un  "gameplay" de esta expansión y un análisis lo más pronto posible, por lo que os conviene estar atentos a nuestra web y nuestros canales de [Youtube](https://www.youtube.com/c/jugandoenlinuxcom) y [Twitch](https://www.twitch.tv/jugandoenlinux). Podeis comprar American Truck Simulator - Oregon en la [tienda de Humble Bundle (patrocinado)](https://www.humblebundle.com/store/american-truck-simulator-oregon?partner=jugandoenlinux) o en [Steam](https://store.steampowered.com/app/800370/American_Truck_Simulator__Oregon/). Os dejamos con el trailer de celebración del lanzamiento de esta expansión:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/vWKjRuRGkVw" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>

