---
author: Serjor
category: Carreras
date: 2019-04-21 10:33:30
excerpt: "<p>Uno de los juegos m\xE1s queridos por la comunidad gamer de GNU/Linux\
  \ llega a la versi\xF3n 1.0</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/38bea6f232aa990273b367ca55af8e69.webp
joomla_id: 1027
joomla_url: supertuxkart-llega-a-la-version-1-0
layout: post
tags:
- supertuxkart
title: "SuperTuxKart llega a la versi\xF3n 1.0"
---
Uno de los juegos más queridos por la comunidad gamer de GNU/Linux llega a la versión 1.0

¡Y por fin llegó el día! Después de muchos años, SupertTuxKart [llega a la versión 1.0](http://blog.supertuxkart.net/2019/04/supertuxkart-10-release.html?m=1), y con el ansiado juego en red completamente implementado.


Poco podemos decir de SupertTuxKart que no se haya dicho ya, es un viejo conocido entre los jugadores linuxeros, y muchos podemos decir que lo hemos conocido casi desde el principio viendo y lo hemos visto evolucionar a lo largo del tiempo.


Para quienes no lo conozcan, el resumen rápido pero injusto, sería decir que es un Super Mario Kart, pero protagonizado por varias mascotas de proyectos de software libre. Trae un modo "historia" por definirlo de alguna manera, por si queremos jugar en modo single player, y el modo online que ya os [avanzábamos](index.php/homepage/generos/carreras/2-carreras/1022-los-desarrolladores-de-supertuxkart-buscan-betatesters-para-el-juego-en-red) en diciembre el pasado año.


Podéis descargarlo desde su página de [descargas](https://supertuxkart.net/Download) o desde la tienda de itch.io


<div class="resp-iframe"><iframe height="167" src="https://itch.io/embed/83502" width="552"></iframe></div>


Os dejamos con el trailer de esta versión 1.0


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/Lm1TFDBiIIg" width="560"></iframe></div>


Y digo yo, habrá que probarlo, ¿no? Pásate por nuestro canal de [Telegram](https://t.me/jugandoenlinux), que seguramente ahí organicemos algo ;-)

