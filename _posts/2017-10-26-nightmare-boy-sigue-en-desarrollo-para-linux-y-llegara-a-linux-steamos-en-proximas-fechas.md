---
author: Pato
category: "Acci\xF3n"
date: 2017-10-26 16:22:09
excerpt: "<p>El juego sali\xF3 a la venta ayer en otras plataformas</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/26fdc8bcfe874d68b945cfc55e1c7548.webp
joomla_id: 506
joomla_url: nightmare-boy-sigue-en-desarrollo-para-linux-y-llegara-a-linux-steamos-en-proximas-fechas
layout: post
tags:
- accion
- indie
- plataformas
- aventura
- proximamente
- metroidvania
title: "'Nightmare Boy' sigue en desarrollo para Linux y llegar\xE1 a Linux/SteamOS\
  \ en pr\xF3ximas fechas"
---
El juego salió a la venta ayer en otras plataformas

Ayer mismo se puso a la venta 'Nightmare Boy' el juego de plataformas y acción "metroidvania" del estudio español "The Vanir Project" [[web oficial](http://www.thevanirproject.com/nightmare-boy/)]. Tal y como nos hicimos eco cuando lanzaron su campaña de Greenlight, el juego estaba previsto que lleguase a Linux/SteamOS tras su lanzamiento en otras plataformas, pero ¿sigue siendo así?


Para saberlo nos hemos puesto en contacto con el estudio y nos han confirmado que están trabajando en el desarrollo, aunque aún no tienen una fecha estimada de lanzamiento:



> 
> *Aun no tenemos fecha pero si estamos trabajando en ello. Esperamos no demorarnos mucho!*
> 
> 
> 


 Por otra parte, el juego [ya ha aparecido](https://twitter.com/SteamDB_Linux/status/923221972653187072) en SteamDB para Linux/SteamOS, por lo que estaremos atentos al lanzamiento para nuestro sistema favorito.


*Nightmare Boy Se desarrolla en un mundo interconectado donde se mezclan escenarios oníricos con enemigos sacados de las peores pesadillas.*  
*Además, conocerás a extraños personajes con los que podrás interactuar, y aprenderás magias o habilidades que te conducirán a nuevas zonas por descubrir.*


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/OOwYn2IG4-8" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


¿Que te parece este 'Nightmare Boy'? ¿te gustan los juegos de tipo metroidvania?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

