---
author: Pato
category: Estrategia
date: 2019-03-07 13:11:20
excerpt: <p>El estudio <span class="username u-dir" dir="ltr">@Stardock tiene noticias
  agridulces sobre el juego.</span> </p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/97a935595bd2614586903fc5e76b1e31.webp
joomla_id: 989
joomla_url: ashes-of-the-singularity-escalation-puede-estar-ya-muy-cerca-de-llegar-a-linux-steamos
layout: post
tags:
- rts
- ciencia-ficcion
- estrategia
title: '''Ashes of the Singularity: Escalation'' puede estar ya muy cerca de llegar
  a Linux/SteamOS (ACTUALIZADO)'
---
El estudio @Stardock tiene noticias agridulces sobre el juego. 

**ACTUALIZADO 2-10-19:** Ayer nos enterábamos (perdón por el restraso)  a través de los [foros de la comunidad de Steam](https://steamcommunity.com/app/507490/discussions/0/1290691308574348748/?ctp=100), que **el port de Ashes of the Singularity se cancelaba**, debido a que , tal y como comentan sus desarrolladores, el actual estado, aunque es funcional presenta problemas de rendimiento que no son aceptables, especialmente entre diferentes tarjetas gráficas, por lo que no lanzarán un producto con una calidad baja.


Pero también anunciaban, y es muy importante decirlo, que **gracias a todo lo aprendido en estos meses realizando el port a Linux/Vulkan** de esta primera parte, **la segunda ya será desarrollada teniendo en cuenta que será multiplataforma**, algo que no se planteo en los inicios de la creación del primer juego. Esto significa que **Ashes of the Singularity II tendrá soporte para nuestro sistema en un futuro**, pues ya será diseñado con esa idea.


 




---


**NOTICIA ORIGINAL:**


**Ashes of the Singularity** es uno de esos juegos que llevamos esperando desde hace tiempo y que parece que nunca termina de llegar. Pero ahora sabemos que la versión para nuestro sistema favorito está ya muy cerca a tenor de las palabras del estudio Stardock, creadores del también notable *Sins of a Solar Empire* en u[no de sus últimos comunicados](https://www.stardock.net/article/493837/ashes-of-the-singularity-march-2019-update), donde además anuncian **la llegada de un nuevo DLC** que llegará próximamente:


*Muchos de los ingenieros de Stardock han estado trabajando para trasladar el motor a Linux. He mencionado esto en otra parte pero lo tenemos funcionando. El desafío ahora es que necesitamos hacer más optimización con **Vulkan** antes de poder ponerlo a disposición del público.*


*Mientras el trabajo de **Vulkan** avanza, estamos en el proceso de crear un nuevo DLC para Ashes que incluye nuevas unidades y edificios que deberían resultar en nuevas estrategias y mayor profundidad estratégica. Tendremos más sobre eso también esta primavera.*


Ashes of the Singularity es un juego de *estrategia en tiempo real* (RTS) en el que l*a raza humana se ha expandido por la galaxia gracias a las maravillas de la singularidad tecnológica. Te has vuelto tan poderoso que puedes manejar grandes ejércitos a través de un mundo que te proporciona un imperio galáctico cada vez más grande.*  
  
*Ahora, la humanidad se encuentra bajo el asedio de un nuevo enemigo. Se denominan Sustrato y quieren aniquilar la raza humana. Como prometedor miembro de la Coalición Poshumana tendrás que lidiar con esta nueva amenaza y con los humanos renegados que intentan reclamar sus propios mundos.*


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/qaosR3MQbsA" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Por otra parte, Stardock anunciará en breve un "*nuevo juego"* entre comillas ya que comenzó como una expansión para Ashes pero ha evolucionado para ser independiente:


*Estamos a punto de anunciar **un nuevo juego** en el universo de Ashes of the Singularity. Originalmente, se iba a lanzar como un DLC para Ashes, pero los cambios de juego requeridos hacían imposible mantenerlo como parte del mismo ejecutable, por lo que se lanzará como un juego independiente. Los Fundadores de Ashes vitalicios obtendrán este juego agregado automáticamente.*


*Tendremos más detalles sobre esto más adelante esta semana. La buena noticia para los jugadores de Ashes es que se ha experimentado e implementado mucha tecnología nueva con este juego que repercutirá en Ashes of the Singularity esta primavera.*


Si quieres saber mas sobre Ashes of the Singularity puedes visitar su [página web oficial](https://www.ashesofthesingularity.com/). Estaremos atentos a las novedades y al lanzamiento del juego para nuestro sistema favorito.

