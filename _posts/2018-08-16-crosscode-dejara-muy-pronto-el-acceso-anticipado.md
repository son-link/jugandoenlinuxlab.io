---
author: leillo1975
category: Rol
date: 2018-08-16 15:10:35
excerpt: "<p class=\"ProfileHeaderCard-screenname u-inlineBlock u-dir\" dir=\"ltr\"\
  ><span class=\"username u-dir\" dir=\"ltr\"><span class=\"username u-dir\" dir=\"\
  ltr\">El juego de </span></span><span class=\"username u-dir\" dir=\"ltr\">@RadicalFishGame</span>\
  \ finalmente alcanzar\xE1 versi\xF3n final.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/b6004aaf43dbcfb743aa1b54a758f57d.webp
joomla_id: 836
joomla_url: crosscode-dejara-muy-pronto-el-acceso-anticipado
layout: post
tags:
- rol
- rpg
- retro
- 2d
- radical-fish-games
title: "Crosscode dejar\xE1 muy pronto el Acceso Anticipado."
---
El juego de @RadicalFishGame finalmente alcanzará versión final.

Según acabamos de leer en [Steam](https://steamcommunity.com/games/368340/announcements/detail/1699432921261651351), tras 7 años de desarrollo y 3 en Acceso Anticipado, finalmente el juego de la compañía alemana [Radical Fish Games](https://www.radicalfishgames.com/) lanzará oficialmente la versión 1.0 de su juego en poco más de un mes, concretamente el próximo 20 de Septiembre.


El juego, para quien no lo conozca, es un RPG de acción en dos dimensiones con aspecto retro que combina unos gráficos 16 bit al estilo Megadrive-SNES con unas físicas suaves, un combate rápido y puzzles, todo ello envuelto en una interesante historia de ciencia ficción. Está desarrollado basándose en HTML5 y  el motor [impact.js](https://impactjs.com/), y podeis probarlo actualmente descargando su demo desde [Steam](https://store.steampowered.com/app/368340/CrossCode/).


Según sus desarrolladores en esta próxima actualización a la versión 1.0 podemos esperar (traducción online):



> 
> *El lanzamiento completo será enorme y por mucho la actualización más grande que el juego ha tenido hasta ahora.*
> 
> 
> *He aquí una lista de cosas que puede esperar de CrossCode 1.0:*
> 
> 
> *La historia principal completa con al menos el doble de escenas de animación*  
> *Momentos trama impresionantes y dramáticos*  
>  *Un área completamente nueva + mazmorra + varias extensiones en áreas existentes*  
>  *Plaza Rombo - la ciudad más grande del juego*  
>  *¡Nuevos miembros de la aventura!*  
>  *Más de 9 nuevos tipos de enemigos + más variaciones*  
>  *Más de 13 nuevas peleas de jefes y otras batallas importantes*  
>  *Alrededor de 12 nuevas misiones*  
>  *Varias mejoras en el menú y adiciones de QoL*  
>  *Una traducción completa al alemán*  
>  *Otraas Innumerables pequeñas mejoras/cambios*
> 
> 
> *También esperamos que tenga menos bugs que las versiones anteriores, ya que hacemos algunas pruebas internas esta vez!*
> 
> 
> *Otras características que todavía esperamos terminar pero que aún no están 100% seguras:*
> 
> 
> *Logros de Steam*  
>  *Tarjetas de Steam*
> 
> 
> *Haremos todo lo posible para implementarlas a tiempo para la versión 1.0. Sin embargo, si no lo logramos, puede esperar que estas características se añadan muy pronto en una actualización de seguimiento. No te preocupes por los logros, el juego ya hace un seguimiento de todos los logros internamente, por lo que todavía obtendrá todos los logros de Steam que ya recogió antes de la actualización.*
> 
> 
> 


También se lanzará la banda sonora del juego con más de 20 pistas nuevas, por lo que el total serán 64 temas. Está compuesta por [Deniz ‘Intero’ Akbulut](https://soundcloud.com/interovgm) y podrá ser adquirida también en Steam, así como en [Bandcamp](https://interovgm.bandcamp.com/album/crosscode-original-soundtrack). Pero la cosa no acaba aquí por que tras el lanzamiento de esta versión final tienen pensado añadir más características nuevas en actualizaciones posteriores entre las que destacan:



> 
> *Un nuevo modo que te permite reiniciar el juego de una manera especial (los detalles siguen abiertos).*  
>  *Varios NPCs, misiones y enemigos adicionales que prometimos a nuestros patrocinadores de Indiegogo.*  
>  *El Gremio de Héroes de Último Minuto que (irónicamente) decidimos no implementar en el último minuto.*  
>  *Más mapas para Rhombus Square y algunas aventuras más que no han llegado a la versión 1.0.*  
>  *¡La Arena en la Plaza Rombo! Lo menos que puedes esperar es un modo rápido de jefe entre otras cosas!*
> 
> 
> 


En JugandoEnLinux os informaremos lo más puntualmente posible de su salida oficial y todas las noticias que genere tal evento. Os dejamos con un video del juego donde podeis ver con más claridad las virtudes del juego:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/6CuG6jId3MI" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 

