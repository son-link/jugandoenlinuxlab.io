---
author: Pato
category: "Acci\xF3n"
date: 2017-02-01 18:49:45
excerpt: "<p>Se trata de un juego de acci\xF3n competitiva en 2.5D que a\xFAn est\xE1\
  \ en acceso anticipado</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/7fb770f34c796f7501d3cf0f0dc39075.webp
joomla_id: 203
joomla_url: battlecrew-space-pirates-tiene-posibilidades-de-llegar-a-linux
layout: post
tags:
- accion
- indie
- multijugador
- 2-5d
- acceso-anticipado
- competitivo
title: '''BATTLECREW Space Pirates'' tiene posibilidades de llegar a Linux'
---
Se trata de un juego de acción competitiva en 2.5D que aún está en acceso anticipado

Hace unos días nos llegaban noticias gracias a [linuxgameconsortium.com](https://linuxgameconsortium.com/linux-gaming-news/battlecrew-space-pirates-multiplayer-shooter-linux-port-46886/) de que habían posibilidades de que 'BATTLECREW Space Pirates' [[web oficial](http://battlecrew-game.com/)] llegara a Linux. 


El juego en sí recuerda horrores a otro del que [estuvimos hablando justo esta semana pasada](index.php/item/288-hive-altenum-wars-un-shooter-competitivo-2-5d-llegara-el-24-de-marzo) y que está siendo desarrollado por el estudio español Catness Game Studios titulado 'Hive: Altenum Wars', y que este si es seguro que llegará a Linux próximamente. Se trata de juegos de acción multijugador competitivo mediante héroes pero a diferencia de -un ejemplo- Overwatch este 'BATTLECREW' o el caso de 'Hive' es en 2.5D. A continuación el trailer:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" frameborder="0" height="360" src="https://www.youtube.com/embed/LprVNqqJb5g" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


En el caso de 'BATTLECREW Space Pirates' de momento no hay prevista versión para Linux, pero esto puede cambiar ya que los chicos de linuxgameconsortium.com se pusieron en contacto con el estudio "DONTNOD ELEVEN" para preguntar por la posibilidad de un port, a lo que el estudio les respondió que estaba en consideración un port a Linux:



> 
> “The game is developed on Unreal 4.  
> For now, we’re focusing on the PC version, a LINUX one is under consideration but it’s too early to say.”
> 
> 
> 


"*El juego está desarrollado en Unreal 4.*


*Por ahora, nos estamos enfocando en la versión PC, una versión Linux está en consideración ,pero es demasiado pronto para decir algo."*


La respuesta deja la puerta abierta, y los chicos de [linuxgameconsortium.com](https://linuxgameconsortium.com) han abierto un hilo en el foro de Steam del juego para que todo el que esté interesado en que el juego llegue a Linux pueda expresar su apoyo. Puedes verlo y participar [en este enlace](http://steamcommunity.com/app/411480/discussions/0/135507403158631172/).


Está claro que algo se está moviendo en los juegos de acción competitivos, con dos juegos muy similares en cuanto a temática y con un desarrollo y factura excelente. 


Seguiremos de cerca lo que suceda, y si este no termina llegando al menos si sabemos que el otro (además de factura española) sí nos llegará. Esperemos.


¿Que te parece este 'BATTLECREW Space Pirates'? ¿Te gustaría que llegase a Linux?


Cuéntamelo en los comentarios, en el foro o en los canales de Jugando en Linux en [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

