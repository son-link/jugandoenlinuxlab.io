---
author: yodefuensa
category: Noticia
date: 2020-08-05 18:10:58
excerpt: "<p>Un instalador de @LutrisGaming permite la ejecuci\xF3n de esta herramienta.</p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Lutris/lutrisge.webp
joomla_id: 1245
joomla_url: geforce-now-ya-funciona-gracias-a-lutris
layout: post
tags:
- wine
- nvidia
- lutris
- geforce-now
title: GeForce Now ya funciona gracias a Lutris
---
Un instalador de @LutrisGaming permite la ejecución de esta herramienta.


 Nos hacemos eco de esta noticia gracias al **mensaje publicado en el Twitter de Lutris**:



> 
> Thanks to amazing work from Alexandre Abgrall (aabgrall on Github), GeForce Now can now be used under Linux!  
> With it, you can even play anticheat-protected games, which normally don't work on Linux at all.  
> Install it using Lutris: <https://t.co/6h0knpjPQO> [pic.twitter.com/2vAFHNWheU](https://t.co/2vAFHNWheU)
> 
> 
> — Lutris Gaming (@LutrisGaming) [August 5, 2020](https://twitter.com/LutrisGaming/status/1290824921334128645?ref_src=twsrc%5Etfw)



**Traducción:** "*Gracias al increíble trabajo de Alexandre Abgrall (aabgrall en Github), GeForce Now puede ser usado ahora en Linux!*  
*Con ella, puedes incluso jugar a juegos protegidos contra las trampas, que normalmente no funcionan en Linux."*


Ahora podremos usar GeForce Now en nuestro sistema gracias al "[installer](https://lutris.net/games/geforce-now/)" de Wine que podemos encontrar en la web de este software. Recordemos que **GeForce Now es un sistema de juego en nube** como Stadia o PlayStation Now, por lo que podremos ejecutar juegos que antes no podíamos si estaban protegidos con anticheat, y de diversas plataformas como Steam, Epic o Uplay. Todo esto gracias al trabajo de Alexandre Abgrall, que ha sido quien lo ha conseguido ejecutarlo en Lutris y subir la configuración del instalador.


Por supuesto, en JugandoEnLinux.com preferiríamos que esta herramienta de Nvidia tuviese soporte nativo (recemos por ello), pero mientras tanto podremos usarla gracias a este instalador. ¿Qué te parece GeForce Now? ¿Y Lutris y sus instaladores? Puedes dejarnos tus impresiones sobre esta noticia en los comentarios, o en mensajes en nuestra comunidad de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).  
 

