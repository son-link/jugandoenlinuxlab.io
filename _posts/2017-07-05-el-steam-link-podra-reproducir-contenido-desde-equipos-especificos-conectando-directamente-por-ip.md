---
author: Pato
category: Hardware
date: 2017-07-05 17:47:12
excerpt: "<p>Abre la puerta a jugar desde otras ubicaciones fuera de la propia red\
  \ local, aunque el streaming a trav\xE9s de internet no est\xE1 \"oficialmente soportado\"\
  </p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/072519f74a95ea36f571d1e83f1c23bd.webp
joomla_id: 394
joomla_url: el-steam-link-podra-reproducir-contenido-desde-equipos-especificos-conectando-directamente-por-ip
layout: post
title: "El Steam Link podr\xE1 reproducir contenido desde equipos espec\xEDficos conectando\
  \ directamente por IP"
---
Abre la puerta a jugar desde otras ubicaciones fuera de la propia red local, aunque el streaming a través de internet no está "oficialmente soportado"

Se nota que estamos en el periodo vacacional, y tras finalizar las ofertas de Steam hoy mismo se nota que todo el mundillo anda ya de vacaciones. Esto no impide que de vez en cuando nos salgan informaciones de calado como esta que nos concierne. Y es que la última actualización beta del [Steam Link](http://store.steampowered.com/app/353380/Steam_Link/) ha traído una sorpresa que puede deparar nuevos usos para este pequeño aparato.


Para el que no lo sepa, el Steam Link es una especie de "replicador" que transmite a la pantalla de un televisor la imagen y el sonido de nuestro ordenador desde el cliente de Steam a través de la red local (doméstica) a la vez que transmite los parámetros de los periféricos conectados a el de forma que es como si estuviésemos jugando delante de nuestra pantalla. Digamos que es una forma de poder jugar con nuestros juegos de Steam en el salón haciendo streaming sin tener que mover nuestro equipo de lugar.


Dejando de lado el rendimiento del aparato, la novedad de esta actualización será que ahora podremos conectar desde nuestro Steam Link a un equipo concreto apuntando diréctamente a su dirección IP. Según explican [en el anuncio oficial de la beta](http://steamcommunity.com/app/353380/discussions/0/1353742967806351457/) esto se ha hecho para permitir que el Steam Link pueda reproducir el contenido de ordenadores en redes domésticas complejas donde los equipos no estén en el mismo rango o que estén conectados a través de diferentes routers. 


Para conseguirlo, en el menú para añadir equipos pulsando la Y del mando podremos configurar los parámetros como la IP del equipo al que queremos conectar. Hay que decir que esto abre la puerta también a que equipos en diferentes ubicaciones puedan conectar al Steam Link a través de internet, eso si siempre que tengan buena conexión entre sí (ping bajo y suficiente ancho de banda) y que puedan hacer ping al Steam Link. ¿Alguien dijo VPN?


El caso es que los mismos desarrolladores saben que cabe la posibilidad de utilizarlo "de ese modo", ya que en el mismo anuncio ya dicen que "**el streaming a través de internet no está oficialmente soportado**".


Además de esto, añadirá una mejora adicional para que si el aparato detecta una conexión cableada la utilice por defecto sobre la conexión por Wifi dado que suelen ser mucho más estables para el streaming de los juegos.


¿Que te parecen las mejoras del Steam Link? ¿Tienes uno? ¿Piensas probarlo a través de una VPN?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 

