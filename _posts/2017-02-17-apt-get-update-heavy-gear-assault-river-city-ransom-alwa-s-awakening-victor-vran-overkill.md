---
author: Pato
category: Apt-Get Update
date: 2017-02-17 19:22:03
excerpt: "<p>Cerramos otra semana de lo m\xE1s interesante</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/f41bf091a4e18f2312495cc0e975d9f7.webp
joomla_id: 224
joomla_url: apt-get-update-heavy-gear-assault-river-city-ransom-alwa-s-awakening-victor-vran-overkill
layout: post
title: Apt-Get Update Heavy Gear Assault & River City Ransom & Alwa's Awakening &
  Victor Vran Overkill...
---
Cerramos otra semana de lo más interesante

Otra semana se nos va, y como siempre tenemos que ponernos al día. ¿Comenzamos?


Desde [www.gamingonlinux.com](http://www.gamingonlinux.com):


- 'River City Ransom Underground' llegará a Linux el día de su lanzamiento. Puedes verlo [en este enlace](https://www.gamingonlinux.com/articles/turns-out-that-river-city-ransom-underground-will-see-day-1-linux-support.9106).


- Liam nos descubre 'Alwa's Awakening', un juego inspirado en los antiguos juegos de NES. Puedes verlo [en este enlace](https://www.gamingonlinux.com/articles/alwas-awakening-is-a-pretty-decent-nes-inspired-adventure-game-with-a-very-retro-feel.9111).


- También nos descubrió que los chicos de Feral andan trasteando con los drivers Mesa para introducir mejoras. Puedes verlo [en este enlace](https://www.gamingonlinux.com/articles/looks-like-feral-interactive-are-doing-more-work-on-mesa.9114).


- 'Diluvion' un juego de exploración submarina con elementos RPG puede llegar a Linux en un futuro, aunque no hay fecha ni confirmación de a qué distribuciones. Puedes verlo [en este enlace](https://www.gamingonlinux.com/articles/diluvion-a-deep-sea-exploration-game-with-rpg-elements-may-be-coming-to-linux.9117).


- 'Holobunnies Pause Café' es un juego de tipo "brawler" cooperativo para dos personas que tendrá soporte en Linux. Puedes verlo [en este enlace](https://www.gamingonlinux.com/articles/holobunnies-pause-caf-a-two-player-co-op-brawler-that-looks-pretty-fun-will-support-linux.9123).


- 'Victor Vran Overkill Edition' nos traerá dos nuevos DLCs a este juego de acción RPG. Puedes verlo [en este enlace](https://www.gamingonlinux.com/articles/victor-vran-overkill-edition-announced-two-new-dlc.9127).


 


Desde [www.linuxgameconsortium.com:](http://www.linuxgameconsortium.com)


- 'Heavy Gear Assault' el juego de "mechas" disponible en Linux recibe una buena actualización para mejorar características y rendimiento. Puedes verlo [en este enlace](https://linuxgameconsortium.com/linux-gaming-news/heavy-gear-assault-major-updates-coming-bug-fixes-performance-linux-pc-games-47773/).


- 'Fumiko' un juego creado con el editor Linux de Unity llega a Steam y ya está disponible. Puedes verlo [en este enlace](https://linuxgameconsortium.com/linux-gaming-news/fumiko-created-unity3d-linux-editor-launches-steam-47868/).


- 'Bear with me: Episode 2' la continuación de este juego de aventuras "point and Click" ya está disponible para Linux. Puedes verlo [en este enlace](https://linuxgameconsortium.com/linux-gaming-news/bear-with-me-episode-2-noire-adventure-releases-linux-mac-pc-47928/).


 


Vamos con los Vídeos de la semana:


River City Ransom Underground


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/us7MnovsPKs" width="640"></iframe></div>


 Alwa's Awakening


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/ijP4WJjSdLg" width="640"></iframe></div>


 Diluvion


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/52GOtB2sxnE" width="640"></iframe></div>


 Holobunnies


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/MlMq-JyMiX0" width="640"></iframe></div>


 Victor Vran


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/mRYXYQIL8Ew" width="640"></iframe></div>


 


Esto es todo por esta semana. ¿Qué te ha parecido este Apt-Get Update? Seguro que me he dejado muchas cosas en el tintero.


¿Que tal si me lo cuentas en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD)?

