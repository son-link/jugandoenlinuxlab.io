---
author: leillo1975
category: Estrategia
date: 2018-11-27 19:59:53
excerpt: "<p class=\"ProfileHeaderCard-screenname u-inlineBlock u-dir\" dir=\"ltr\"\
  ><span class=\"username u-dir\" dir=\"ltr\">@WeBeHarebrained lanza nuestra versi\xF3\
  n conjuntamente con la expansi\xF3n \"Flashpoint\"</span></p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/69046a6b25db0c21ed636e7df739fd1b.webp
joomla_id: 912
joomla_url: battletech-ya-esta-oficialmente-en-linux-steamos
layout: post
tags:
- beta
- dlc
- paradox-interactive
- battletech
- harebrained-schemes
- flashpoint
title: "\"Battletech\" ya est\xE1 en Linux/SteamOS, aunque aun en Beta."
---
@WeBeHarebrained lanza nuestra versión conjuntamente con la expansión "Flashpoint"

Finalmente Battletech ya está aquí. A pesar de continuar en [fase beta](index.php/homepage/generos/estrategia/8-estrategia/984-battletech-ya-esta-disponible-para-gnu-linux-en-version-beta), finalmente el juego de [Harebrained Schemes](http://harebrained-schemes.com/) puede ser instalado y jugado con soporte oficial en GNU-Linux/SteamOS tal y como habían prometido, pues esta **ha sido incluida en la versión principal del juego** ([FAQ](https://forum.paradoxplaza.com/forum/index.php?threads/battletech-flashpoint-release-faq.1131658/)) y se puede descargar tanto en [Steam](https://store.steampowered.com/app/637090/BATTLETECH/), como en [GOG](https://www.gog.com/game/battletech_game) y [Humble](https://www.humblebundle.com/store/battletech?partner=jugandoenlinux). Si recordais, hasta ahora era necesario acceder a las propiedades del juego y el la pestaña BETAS había que seleccionar "public_beta_linux". Dicho soporte (beta) viene acompañado con la **nueva expansión Flashpoint**, que incorpora al juego, entre otras muchas cosas, 30 horas de nuevos contenidos. Aqui teneis el tweet anunciando la salida de esta DLC:



> 
> Start your Fusion Engines! BATTLETECH: Flashpoint is here! New 'Mechs, new missions, new tropical biome and MORE in 30 hours of new content! Jump in RIGHT HERE: <https://t.co/FxVGYMfIMr> [pic.twitter.com/Vu7Tztx6Af](https://t.co/Vu7Tztx6Af)
> 
> 
> — BATTLETECH Game (@BATTLETECH_Game) [November 27, 2018](https://twitter.com/BATTLETECH_Game/status/1067479448306556929?ref_src=twsrc%5Etfw)



El juego fué lanzado en el mes de Abril sin versión para Linux a pesar de [estar este confirmado](index.php/homepage/generos/estrategia/8-estrategia/439-paradox-interactive-mueve-ficha-anuncia-surviving-mars-y-lanzaran-battletech) por su editora [**Paradox Interactive**](https://www.paradoxplaza.com/) durante el desarrollo. Finalmente este soporte ha llegado con retraso, pero ha llegado, que es lo importante. Para quien no conozca Battletech, se trata de un **juego de estrategia táctica por turnos** donde haremos uso de "Mechs" durante los combates. El juego está desarrollado por **el equipo de Shadowrun Returns** y nos permite jugar tanto en modo campaña como en Multijugador y escaramuza contra la CPU. El juego ha conseguido unas **críticas muy buenas** y ha sido **nominado recientemente al mejor juego de estrategia** en "[TheGameAdwards.com](https://thegameawards.com/awards/#best-strategy-game)".


Los **requisitos técnicos mínimos** que deben cumplir vuestros PC's para poder disfrutar con garantías son los siguientes:


* Requiere un procesador y un sistema operativo de **64 bits**
* **SO:** 64-bit Ubuntu 18.04 LTS o SteamOS
* **Procesador:** Intel® Core™ i3-3240 CPU
* **Memoria:** 8 GB de RAM
* **Gráficos:** Nvidia® GeForce™ GTX 560
* **Red:** Conexión de banda ancha a Internet
* **Almacenamiento:** 35 GB de espacio disponible
* **Notas adicionales:** Multiplayer es compatible entre Windows, Mac y Linux.


 Podeis comprar en [Humble Store](https://www.humblebundle.com/store?partner=jugandoenlinux) (patrocinado) tanto [Battletech](https://www.humblebundle.com/store/battletech?partner=jugandoenlinux), como su expansión [Flashpoint](https://www.humblebundle.com/store/battletech-flashpoint?partner=jugandoenlinux), **ambos con importantes descuentos en este momento**. Os dejamos con el trailer del juego y también de su expansión Flashpoint:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/tsIMfOo_VO0" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/Cb4WLNxFk68" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


La verdad es que el juego y su expansión tienen una pinta asombrosa, ¿cierto? . ¿A vosotros que impresión os produce el juego? ¿Ya lo habeis probado? Dejad vuestras impresiones en los comentarios o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

