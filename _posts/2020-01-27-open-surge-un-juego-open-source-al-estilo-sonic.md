---
author: leillo1975
category: Plataformas
date: 2020-01-27 10:06:37
excerpt: "<p>El juego adem\xE1s permite la creaci\xF3n de nuevo contenido.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/OpenSurge/OpenSurge.webp
joomla_id: 1154
joomla_url: open-surge-un-juego-open-source-al-estilo-sonic
layout: post
tags:
- open-source
- open-surge
- gpl
- sonic
title: Open Surge, un juego Open Source al estilo Sonic.
---
El juego además permite la creación de nuevo contenido.


 Gracias a esta noticia publicada por **Noel** (@BreoganGal) en la fantástica web que es [Lignux](https://lignux.com/crea-y-juega-a-open-surge-un-entorno-de-plataformas-retro-2d/), nos hemos enterado de la existencia de este juego de **estilo retro**. Como podeis ver está claramente basado en el archiconocido **Sonic** de la Megadrive/Genesis, y tiene muy buena factura. El juego ademas permite ser descargado en **muchas plataformas diferentes** como lo son Windows, Mac y Linux, aunque pronto también estará en Android y Raspberry Pi.


Una de las características principales de Open Surge es que además de ser un juego, **también es un entorno de desarrollo** que permite crear nuestros propios niveles e incluso juegos independientes, facilitándonos la creación de nuevos niveles, items, jefes, mecánicas, habilidades o personajes jugables. Para ello simplemente tendremos que **pulsar el boton F12** durante la partida, y seguir la documentación que podeis encontrar en su extensa [Wiki](http://opensnc.sourceforge.net/wiki/index.php/Main_Page), o en [videotutoriales](https://www.youtube.com/user/alemart88).


Open Surge está **desarrollado desde cero usando C** y la biblioteca de programación de juegos [Allegro](https://liballeg.org/), siendo su Licencia la **GPL 3.0**. Su desarrollador principal es el brasileño **Alexandre Martíns** ([@alemart](https://github.com/alemart)), entre muchos [otros](http://opensnc.sourceforge.net/wiki/index.php/Contact_the_developers). Podeis encontrar más información sobre el juego en [su página web](https://opensurge2d.org/) o en la web del [proyecto en Github](https://github.com/alemart/opensurge). Si quereis probar la última versión del juego, la [0.5.1.1](https://github.com/alemart/opensurge/releases/tag/v0.5.1.1),  podeis descargar su código y compilarlo, o instalarlo mediante un cómodo [paquete Snap](https://snapcraft.io/opensurge):


`sudo snap install opensurge`


¿Qué os parece este desarrollo de código libre? ¿Os van los juegos de temática retro? Puedes contarnos tu parecer sobre esta noticia en los comentarios, o en mensajes en nuestra comunidad de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux). Os dejamos con un video del juego:


 


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/nCH7xeZp2hQ" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>

