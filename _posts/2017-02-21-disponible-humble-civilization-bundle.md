---
author: leillo1975
category: Estrategia
date: 2017-02-21 20:33:49
excerpt: <p>Humble Bundle nos ofrece un jugoso pack de juegos y DLC's de la franquicia
  de Firaxis Games</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/3bd6583af5a14653b7b54db2c9fe7f3e.webp
joomla_id: 227
joomla_url: disponible-humble-civilization-bundle
layout: post
tags:
- humble-bundle
- aspyr-media
- civilization
- dlcs
- firaxis
title: Disponible Humble Civilization Bundle
---
Humble Bundle nos ofrece un jugoso pack de juegos y DLC's de la franquicia de Firaxis Games

Está disponible en la Web de Humble Bundle una extraordinaria colección de la serie Civilization a un precio más que apetecible. Hace unos pocos días, concretamente el 9 de este mes, [Aspyr Media nos traía la última entrega de este clásico](index.php/homepage/generos/estrategia/item/334-sid-meier-s-civilization-vi-ya-esta-disponible-en-linux-steamos) de la Estrategia por turnos a nuestro querido Linux. Hoy, la conocida página nos da la oportunidad de adquirir las entregas anteriores con sus expansiones por muy poco dinero. Existen como siempre tres modalidades de pago, que a continuación pasaré a describiros:


**Pagando un 1$ o más:**


-Civilization III Complete (Solo Windows)


-Civilization IV The Complete Edition (Solo Windows y Mac)


**Pagando 6€ o más (en este momento)_**


-Civilization V


-Civilization V: Brave New World DLC


-Civilization V: Gods and Kings DLC


-Civilization V: DLC Pack


-20% de descuento en Civilization VI


-25% de descuento en Civilization VI Digital Deluxe


**Pagando 15$ o más:**


-Civilization Beyond Earth


-Civilization Beyond Earth: Exoplanets Map Pack DLC


-Civilization Beyond Earth: Rising Tide DLC


 


Si no disponeis de estos fantásticos juegos, estais ante una fabulosa ocasión de haceros con todas estas joyas. Para ello solo tenies que pichar en el siguiente enlace:


**[Humble Civilization Bundle](https://www.humblebundle.com/civilization-bundle)**


 


Puedes dejar tu opinión acerca de esta noticia en los comentarios o usando nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD).

