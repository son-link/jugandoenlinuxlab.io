---
author: leillo1975
category: "Simulaci\xF3n"
date: 2021-04-02 17:32:47
excerpt: "<p>@SCSsoftware acaba de anunciarlo, y ser\xE1 muy pronto.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ETS2/Iberia/iberiaRelease.webp
joomla_id: 1296
joomla_url: ya-hay-fecha-para-iberia-la-nueva-dlc-de-euro-truck-simulator-2
layout: post
tags:
- ets2
- dlc
- scs-software
- iberia
- eurotruck-simulator-2
title: Ya hay fecha para "Iberia", la nueva DLC de Euro Truck Simulator 2
---
@SCSsoftware acaba de anunciarlo, y será muy pronto.


 Hace unos días, cuando os hablábamos de la [nueva versión]({{ "/posts/euro-truck-simulator-2-se-actualiza-a-la-version-1-40" | absolute_url }}) de Euro Truck Simulator 2, la 1.40, os pronosticábamos que el juego llegaría en unos 15 dias, y nos complace deciros que no nos hemos equivocado, pues en un recentísimo tweet, la compañía checa acaba de comunicar la fecha definitiva:



> 
> Ready to head to Spain & Portugal? 🇪🇸🇵🇹  
>   
> The Iberia DLC for Euro Truck Simulator 2 will be released on April 8th, 2021 at 7pm CEST 🚛💨  
>   
> Watch the official trailer here ⬇️ <https://t.co/KWWqHFOyfm>  
>   
> Read more info at our official announcement post 📰<https://t.co/W3kOA5JGmY> [pic.twitter.com/E4QLIWPnuy](https://t.co/E4QLIWPnuy)
> 
> 
> — SCS Software (@SCSsoftware) [April 2, 2021](https://twitter.com/SCSsoftware/status/1377998667584520199?ref_src=twsrc%5Etfw)


  





Pues si, señores, en menos de una semana estaremos disfrutando de la más esperada de todas las DLCs de este juego, al menos por todos nosotros,  y al fin podremos disfrutar de las carreteras y paisajes de nuestra  querida península ibérica, pudiendo visitar la vecina Portugal, y por supuesto España. Os dejamos con el trailer final, que por cierto, es una delicia:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/MVEBdGZ7UcM" style="display: block; margin-left: auto; margin-right: auto;" title="YouTube video player" width="780"></iframe></div>


SCS también ha anunciado en su blog que el mismo día del lanzamiento, a partir de las 16:30 realizarán también un Stream especial de presentación en sus canales de [Twitch](https://www.twitch.tv/scssoftware) y [YouTube](https://www.youtube.com/channel/UCb9XDB0X13EEqUqCSsOhonQ) con algunas sorpresas. Ahora solo quedan unos días para el día 8 de Abril a las 19:00 CEST, pero seguro que se van a hacer largos... dios, que ganas!

