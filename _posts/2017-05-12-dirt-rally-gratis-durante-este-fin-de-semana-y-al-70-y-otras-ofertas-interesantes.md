---
author: leillo1975
category: Carreras
date: 2017-05-12 07:22:35
excerpt: <p>Podremos probar el simulador de Rallys hasta el domingo por la noche</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/2ec788b5d2483f73e1f9efd1de8baaf4.webp
joomla_id: 316
joomla_url: dirt-rally-gratis-durante-este-fin-de-semana-y-al-70-y-otras-ofertas-interesantes
layout: post
tags:
- steam
- gratis
- feral-interactive
- dirt-rally
- scs-software
- fin-de-semana
title: Dirt Rally gratis durante este fin de semana y al 70%, y otras ofertas interesantes
---
Podremos probar el simulador de Rallys hasta el domingo por la noche

Hace algún tiempo los usuarios de Linux/SteamOS estábamos de enhorabuena gracias a que Feral Interactive había portado el famoso simulador de Rallys a nuestros sistemas. Aquí en JugandoEnLinux [os informábamos](index.php/homepage/generos/carreras/item/358-lanzado-dirt-rally-para-gnu-linux-steamos) de la noticia. Poco después [publicabamos un análisis](index.php/homepage/analisis/item/362-analisis-dirt-rally) sobre este escelente título. Desde ayer por la noche, todos aquellos que no hayais probado Dirt Rally teneis la gran oportunidad de probar en vuestras carnes la más aproximada sensación de conducir un coche de esta disciplina automobilistica por los tramos de los rallyes más míticos, como Gracia, Finlandia o Mónaco.


 


También es importante decir que si después de probarlo quereis haceros con él, podeis adquirirlo por muy poco dinero ya que estará disponible con un **importante descuento del 70%**, por lo que su precio pasará de 49.99€ a **15€**, una rebaja importante, como podeis observar. Sobre este port hay que decir que los chicos de Feral han hecho un trabajo de conversión magnífico, dotando al juego de un rendimiento asombroso que nos permitirá disfrutar a una **buena tasa de FPS** y **soporte Force Feedback** en los volantes.


 


![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisDIRTRally/DiRT_Rally_Wales_Banner_E.webp)


 


En cuanto a otras ofertas interesantes, el juego de gestión **Motorsport Manager** está a mitad de precio en estos momentos. También sabed que [**podreis comprar los juegos de SCS Soft**](http://store.steampowered.com/sale/scs/) (Euro Truck Simulator 2 y American Truck Simulator) junto con sus expansiones a descuentos de hasta el 80%. También teneis disponible el **Steam Controller y el Steam Link** con una importante rebaja. Si no disponeis ninguno os [saldrá más barato comprarlos juntos](http://store.steampowered.com/bundle/789/Steam_Controller_and_Link/) (**10% adicional**). Creo que no hace falta decir que la calidad y posibilidades de estos periféricos están por encima de toda duda.


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/feFOnF0GoMA" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


También es de destacar que en la tienda de Humble Bundle [**teneis de forma gratuita el juego Dungueons II**](https://www.humblebundle.com/store/dungeons-2) para descargar durante poco más de un día, por lo que si no lo teneis aun, ya estais tardando en conseguirlo a coste cero.Dungueons II es un juego que se inspira en los míticos Dungueon Keeper, por lo que la diversión está asegurada.


 


 [![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/BannersOfertas/Dungueons2Gratis.webp)](https://www.humblebundle.com/store/dungeons-2)


Sin más os dejo los enlaces a la tienda de Steam por si quereis probar o haceros con cualquiera de los mencionados:


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/310560/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/415200/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/227300/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/270880/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/353370/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/353380/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


 ¿Pinesas hacer unos tramos este fin de semana? ¿Te harás con alguna de estas ofertas? Cuentanoslo en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD)

