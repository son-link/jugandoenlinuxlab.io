---
author: leillo1975
category: Plataformas
date: 2022-09-02 08:16:53
excerpt: "<p>El clon del cl\xE1sico Sonic alcanza la v.0.6.0 y pasa a llamarse Surge\
  \ the Rabbit.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/OpenSurge/OpenSurge.webp
joomla_id: 1487
joomla_url: open-surge-estrena-nueva-version-y-cambia-de-nombre
layout: post
tags:
- retro
- open-source
- open-surge
- sonic
- surge-the-rabbit
- allegro
title: "Open Surge estrena nueva versi\xF3n y cambia de nombre"
---
El clon del clásico Sonic alcanza la v.0.6.0 y pasa a llamarse Surge the Rabbit.


Ya han pasado más de dos años desde la última vez que [os hablamos]({{ "/posts/open-surge-un-juego-open-source-al-estilo-sonic" | absolute_url }}) de este juego libre, pero eso no quiere decir que el desarrollo de Open Surge haya estado parado, pues ciertamente ha habido lanzamientos en este periodo de los que no os hemos hablado.


Para aquellos que no conozcáis el proyecto se trata de un **juego de estilo Retro** creado por desarrollador brasileño **Alexandre Martins** ([@alemart](https://github.com/alemart)), **inspirado en archiconocido Sonic de Hegdehog**, el clásico de la Megadrive (o Genesis según en que mercados). El juego es completamente **multiplataforma** y está **desarrollado en C**, usando la biblioteca de programación de juegos [Allegro](https://liballeg.org/). El juego, además es un **entorno de desarrollo** que permite crear nuestros propios niveles e incluso juegos independientes, facilitándonos la creación de nuevos niveles, items, jefes, mecánicas, habilidades o personajes jugables. Para ello basta con pulsar la tecla F12 y seguir la documentación que podeis encontrar en su extensa [Wiki](http://opensnc.sourceforge.net/wiki/index.php/Main_Page), o en [videotutoriales](https://www.youtube.com/user/alemart88).


El juego acaba de lanzar hace unas horas la **versión 0.6.0**, donde además ha cambiado de nombre, pasando a llamarse ahora **"Surge the Rabbit"**, e incorporando numerosas novedades, como las que podeis ver a continuación:


*-Cambio de nombre del juego a Surge the Rabbit  
 -Se ha mejorado el estilo de pixel art del juego  
 -Se ha creado una nueva zona acuática.  
 -Nuevos trucos: Cintas transportadoras, caminar sobre el agua, enchufe de energía  
 -Nueva pantalla de título y nueva animación de la tarjeta de título  
 -Nuevas traducciones: Italiano, Esperanto  
 -Introducción de extensiones de idiomas (carpeta languages/extends/)  
 -Mejora de la compatibilidad con macOS  
 -Se ha introducido la compatibilidad con las transiciones entre animaciones.  
 -Introducción de los conceptos de punto de acción y anclaje de sprites  
 -Mejoras en la API de SurgeScript  
 -Mejora de varios scripts del núcleo  
 -Se ha añadido compatibilidad con las ${EXPRESSIONS} compuestas al evaluar el texto  
 -Cambiado la física  
 -Se ha añadido soporte para la entrada del D-pad en los mandos de Xbox  
 -Se ha eliminado el código heredado de Allegro 4  
 -Corrección de errores y mejoras generales*


Podeis encontrar más información sobre este juego en su [página web oficial](https://opensurge2d.org/), así como en la [página del proyecto](https://github.com/alemart/opensurge). Por el momento solo es posible descargar el [código fuente](https://github.com/alemart/opensurge/releases/tag/v0.6.0) de su nueva versión, pero se espera que pronto haya versiones en [Snap](https://snapcraft.io/opensurge) y [Flatpak](https://flathub.org/apps/details/org.opensurge2d.OpenSurge), por lo que cuando leais esto , es posible que ya existan. Os dejamos con el último trailer que ha publicado de esta nueva versión:  
  



<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/gJgCVMUwEt4" title="YouTube video player" width="780"></iframe></div>


¿Qué os parece este desarrollo de código libre? ¿Os van los juegos de temática retro? Puedes contarnos tu parecer sobre esta noticia en los comentarios, o en mensajes en nuestra comunidad de [Matrix](https://matrix.to/#/#jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux). Os dejamos con un video del juego:

