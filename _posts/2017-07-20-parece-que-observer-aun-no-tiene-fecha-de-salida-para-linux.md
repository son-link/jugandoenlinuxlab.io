---
author: leillo1975
category: Terror
date: 2017-07-20 15:58:00
excerpt: "<p>Aspyr Media y Bloober Team no aclaran si saldr\xE1 simultaneamente junto\
  \ con la de Windows y consolas</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/e071acc32c7e4befc3022bf1715b6773.webp
joomla_id: 415
joomla_url: parece-que-observer-aun-no-tiene-fecha-de-salida-para-linux
layout: post
tags:
- terror
- aspyr-media
- observer
- bloober-team
title: Parece que OBSERVER aun no tiene fecha de salida para Linux (ACTUALIZADO)
---
Aspyr Media y Bloober Team no aclaran si saldrá simultaneamente junto con la de Windows y consolas

**ACTUALIZACIÓN a 19-10-17:** De nuevo a través de [GamingOnLinux](https://www.gamingonlinux.com/articles/crazy-cyberpunk-horror-game-observer-is-coming-to-linux-on-october-24th.10574) nos llega la noticia de que el juego finalmente saldrá el 24 de este mes en nuestro sistema. La confirmación viene por parte de Aspyr, editora del juego.


 




---


Nos llega la noticia por parte de [GamingOnLinux](https://www.gamingonlinux.com/articles/cyberpunk-horror-game-observer-from-bloober-team-aspyr-media-will-likely-see-a-delayed-linux-release.10020) de que ha sido revelada la fecha de salida oficial, pero esta no incluye en principio fecha para nuestro sistema. La fecha elegida para Windows y consolas es el 15 de Agosto, por lo que no sabemos si lo tendremos disponible el primer día como el resto.


 


[Aspyr Media](http://www.aspyr.com/), conocida por portar bastantes juegos de calidad a nuestro sistema (Borderlands 2, Civilization VI o Star Wars: Knights of the Old Republic II), en este caso es la editora del juego, que desarrolla la compañía [Bloober Team](https://www.blooberteam.com/projects.html), que en el pasado desarrolló **Layers Of Fear**.


 


Observer es un juego de terror ambientado en el futuro, con estética ciberpunk y dirigido a un público adulto. Como podeis ver en el trailer (abajo) todo parece indicar que no tendrá desperdicio para los amantes del género. Esperemos que el juego esté disponible el día del lanzamiento o al menos cerca de la fecha. Siendo Aspyr la editora, un retraso dilatado parece poco probable.


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/2CwD6o5OGSE" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


¿Qué os parece Observer? ¿Os gustan los juegos de Terror y/o de temática Ciberpunk? Deja tus impresiones en los comentarios, o en nuestro canal de [Telegram](https://t.me/jugandoenlinux) .

