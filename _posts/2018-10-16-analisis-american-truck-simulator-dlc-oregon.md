---
author: leillo1975
category: "An\xE1lisis"
date: 2018-10-16 08:48:46
excerpt: <p>Repasamos todas las virtudes de lo nuevo de @SCSsoftware.</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/3a9fdfdb8b1a460a423baa85a1a46274.webp
joomla_id: 876
joomla_url: analisis-american-truck-simulator-dlc-oregon
layout: post
tags:
- dlc
- analisis
- american-truck-simulator
- scs-software
- oregon
title: "An\xE1lisis: American Truck Simulator - DLC Oregon."
---
Repasamos todas las virtudes de lo nuevo de @SCSsoftware.

No ha pasado ni un año desde que los muchos fans que tiene American Truck Simulator vimos llegar la expansión de [Nuevo México](index.php/homepage/analisis/20-analisis/656-analisis-american-truck-simulator-dlc-new-mexico), cuando [hace unas semanas](index.php/homepage/generos/simulacion/14-simulacion/990-oregon-la-nueva-dlc-de-american-truck-simulator-ya-esta-disponible) teníamos la oportunidad de volver a agrandar nuestro horizonte en el juego. En esta ocasión [SCS Software](index.php/buscar?searchword=scs&ordering=newest&searchphrase=all) nos lleva al noroeste de los Estados Unidos al estado de Oregón, también conocido como "Beaver State" (estado del Castor). Para preparar el lanzamiento de esta expansión los desarrolladores liberaron primero la [actualización 1.32](http://blog.scssoft.com/2018/09/ats-and-ets2-update-132.html), que tenía como principal característica la posibilidad de adquirir remolques en propiedad y realizar trabajos con ellos, pero que también añadía nuevas carreteras y rediseñaba zonas del mapa.


![ATSoregon mapa](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ATS/Oregon/ATSoregon_mapa.webp)


Con esta DLC, SCS pone fin a una de las críticas más importantes que se han hecho a este juego, críticas que a nuestro juicio son un tanto injustas pero no por ello dejan de ser ciertas, y es que aunque no sean exactamente los mismos paisajes, hasta el momento **los jugadores de ATS achacaban de monotonas las localizaciones del juego y sus expansiones**. Si sois seguidores del "hermano americano" de ETS2, sabreis que el juego comenzó su andadura a principios de 2016 con el estado de California. Al poco tiempo en una actualización se añadió Nevada, y poco después de forma también gratuita Arizona. En la [actualización 1.5](index.php/homepage/generos/simulacion/14-simulacion/248-american-truck-simulator-crece-con-la-actualizacion-1-5) llegaba un cambio de escala en el mapa, y uniendo todo esto se acabó corrigiendo la falta de millas recorribles de un primer momento. Después llego la [primera expansión de pago](https://www.humblebundle.com/store/american-truck-simulator-new-mexico?partner=jugandoenlinux) dedicándosela a la "Tierra del Encanto", Nuevo México. Si nos ponemos a analizar todo esto veremos que **nuestros camiones hasta la fecha circulaban por la América más árida y desértica** , y que las diferencias entre los paisajes, aunque existían eran muy sutíles. Esta situación podía llegar a cansar a los jugadores acostumbrados a la gran variedad que nos encontramos en Euro Truck Simulator 2.


![ATSMaderera](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ATS/Oregon/ATSMaderera.webp)


*Los grandes bosques de Oregon también condicionaran el tipo de trabajos a realizar.*


**En Oregón esta situación cambia radicamente**, encontrándonos con una mayor diversidad visual que hasta el momento. Si bien la zona sureste sigue albergando grandes llanuras, el estado es predominantemente verde, con frondosos bosques de coníferas, praderas, rios, lagos, montañas, la costa del océano Pacífico... Es claramente una contraposición con los desolados paisajes de Nuevo México, **que por supuesto también tiene su encanto**, pero que probablemente resulte mucho menos agradable y ameno que las carreteras del que nos ocupa. Resulta tremendamente reconfortante para la vista recorrer las carreteras de este estado, especiamente las primeras veces, pues la cantidad de detalles que el equipo de SCS ha puesto en esta expansión es abrumadora. 


Pero cuando hablamos de "Oregon", no solo estamos destacando su naturaleza, ya que nos encontramos con multitud de características interesantes, especialmente el **diseño tanto de pueblos y ciudades**, que resultan facilmente reconocibles. Podemos encontrar por el camino pequeñas y solitarias granjas rurales en medio de extensas praderas; pequeños pueblos y ciudades con disposición horizontal con sus típicas tiendas que rondan en nuestro imaginario (Astoria); o enormes Urbes surcadas por mutitud de viaductos, como es el caso de Portland. Lo que está claro es que en Oregón **se aprecia mucha más vida tanto en los paisajes como en los pueblos y ciudades**.


![PuenteAstoria Megler Columbia](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ATS/Oregon/PuenteAstoria-Megler_Columbia.webp)


*Puente Astoria-Megler sobre el Rio Columbia*


Por supuesto, y como suele ser norma de la casa, existe un buen número de **puntos de interés** por los que vamos a pasar, y que seguramente a todos nosotros nos encantaría visitar en la vida real si tuviésemos ocasión. Mientras recorremos las carreteras de Oregon podremos divisar desde nuestra cabina el impresionante y nevado **monte Hood**, cerca de Portland; el extraño **pozo de Thor** en Cooks Chasm (costa del océano Pacífico); el espectacular **lago del Cráter,** al sur del estado; o el cañon formado por el **Crooked River** en la zona central.


![ThorsWell](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ATS/Oregon/ThorsWell.webp)


*El pozo de Thor parece tragarse el mar como si un desagüe se tratase*


**Mención especial merecen los puentes** que podemos transitar y divisar en el juego, pues tanto su cantidad, como diseños y variedad hacen que sean un elemento destacado de esta DLC. Oregón, al ser un estado repleto de rios, lagos, bahías, y montañas está surcado por multitud de puentes que **destacan por su arquitectura**. También es curioso ver como podemos encontrarnos con un **puente levadizo** en mitad de **Youngs Bay**, que nos obliga a detener el paso para que transiten los barcos por debajo; o un **puente giratorio** que, aunque no circulemos por él, podemos divisar en la costa entre New Port y Coos Bay.  Otro elemento diferenciador de esta expansión son también las **industrias y compañías** que visitaremos cuando realicemos trabajos, que son propias y definitorias del estado. En total podemos encontrar 17 diferentes y principalmente son **madereras y papeleras**, teniendo , por ejemplo, que circular en ciertos momentos por **pistas de tierra** a través del bosque hasta llegar al lugar de carga.


![NewYoungsBayBridge](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ATS/Oregon/NewYoungsBayBridge.webp)


*En el puente levadizo de de Youngs Bay toca esperar....*


El trabajo realizado por SCS Software en Oregon no es mejor que el que han hecho en Nuevo México, simplemente resulta mucho más reconfortante que este último. Aun así podemos decir que Oregon es justamente lo que todos estábamos esperando hace tiempo en American Truck Simulator, y seguramente "perderemos" más horas en su territorio que en otros. Por lo tanto concluyo, que Oregon es claramente **LA EXPANSIÓN** para ATS, y no exageramos nada cuando afirmamos que esta si es **completamente obligatoria si disfrutamos de este juego.** SCS Software se han vuelto a superar una vez más y han dado un plus muy grande a este juego que eleva aun más su categoría y posibilidades. Además le acompaña un precio más que justo incluso sin estar de oferta, ya que podreis adquirirlo por menos de 12€..... y sin más os dejo, que me voy a recorrer Oregón.


**Podeis comprar ["American Truck Simulator Oregon DLC" en la Humble Store](https://www.humblebundle.com/store/american-truck-simulator-oregon?partner=jugandoenlinux)** (patrocinado). Os dejamos con el último video que grabamos conduciendo por las carreteras de esta expansión:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/GKjitDAHtms" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


¿Sois jugadores de American Truck Simulator? ¿Habeis recorrido ya las carreteras de este estado? Déjanos tus respuestas en los comentarios o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

