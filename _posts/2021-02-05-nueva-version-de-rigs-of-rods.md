---
author: leillo1975
category: "Simulaci\xF3n"
date: 2021-02-05 15:00:55
excerpt: "<p>El simulador de f\xEDsicas en veh\xEDculos #OpenSource&nbsp;<span class=\"\
  css-901oao css-16my406 r-poiln3 r-bcqeeo r-qvutc0\">@rigsofrods</span> alcanza la\
  \ versi\xF3n 2021.02.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/RigsOfRods/RigsOFRods.webp
joomla_id: 1275
joomla_url: nueva-version-de-rigs-of-rods
layout: post
tags:
- open-source
- rigs-of-rods
- ogre
title: "Nueva versi\xF3n de Rigs of Rods. "
---
El simulador de físicas en vehículos #OpenSource @rigsofrods alcanza la versión 2021.02.


Aun recordamos con cariño un artículo que escribimos hace ya dos años (como pasa el tiempo) sobre algunos de [los mejores juegos de conducción libres](index.php/component/k2/1-blog-principal/carreras/1099-los-mejores-juegos-de-coches-libres). En él os enumerábamos hasta 10 ejemplos, desde los más arcade hasta los más realistas, y entre estos últimos encontramos [Rigs of Rods](https://www.rigsofrods.org/). Para quien no lo recuerde y no conozca este proyecto, en su momento lo describíamos como "**simulador de físicas**, con capacidad para emular las reacciones de un vehículo ante determinados terrenos, superficies o incluso las colisiones contra objetos".


Realmente no estamos ante un juego "al uso", y probablemente de buenas a primeras pueda espantar un poco a los más neófitos, pues en principio simplemente nos vamos a econtrar a nuestro avatar en un mapa concreto, y a él tendremos que ir añadiendo vehículos tales como **coches, camiones, aviones, barcos, trenes**.... Podremos probar las físicas de los diferentes vehículos y ver como se comportan en los diferentes terrenos o como se deforman al estrellarlos. La gracia está en ir probando **mods**, que hay a montones, incluso en **modo multijugador,** habiendo algunos más que conseguidos. Si quereis más detalles de todas las cosas que podeis llegar a hacer y como , teneis un [completo PDF](https://github.com/RigsOfRods/rigs-of-rods/blob/master/doc/Things%20you%20can%20do%20in%20Rigs%20of%20Rods.pdf) en la web del proyecto.


![tren](https://img.itch.zone/aW1hZ2UvMzUxMzk3LzE3NDg5ODIuanBn/original/KVdU0q.jpg)


Rigs of Rods es un **proyecto GPL-3.0 programado mayoritariamente en C++ y desarrollado usando el** [**motor de renderizado Ogre**.]({{ "/posts/nueva-actualizacion-del-motor-de-renderizado-libre-ogre" | absolute_url }}) El juego comenzó su andadura en el año 2005 por los mismos desarrolladores del conocido juego comercial [BeamNG](https://store.steampowered.com/app/284160/BeamNGdrive/). Aunque este último ha sido completamente desarrollado desde cero, conserva muchas similitudes y conceptos de Rigs of Rods, y muchos de los fans de BeamNG lo son a su vez  de este proyecto Open Source, lo cual demuestra la importancia del proyecto libre.


Gracias a nuestro amigo **@CansecoGPC** nos hemos enterado que en esta nueva [versión 2021.02](https://github.com/RigsOfRods/rigs-of-rods/releases) hay **muchas novedades interesantes** que podeis ver detalladas a continuación:


* Revisión final de la interfaz de usuario con DearIMGUI
* Limpieza importante del código base
* Las traducciones son ahora manejadas por weblate
* Introducción de variables de consola al estilo de Quake llamadas cVars
* Aumentado el límite de velocidad máxima de 21 a ilimitada
* El velocímetro se basa ahora en la velocidad de las ruedas en lugar de la velocidad del vehículo
* Se ha añadido la posibilidad de configurar el rango de bloqueo de los intermitentes
* Se ha añadido la posibilidad de activar el acoplamiento de la velocidad hidráulica
* Se ha añadido soporte para el mapeo de entrada específico del sistema operativo
* Se ha añadido un panel de información de los controles del juego
* Se ha añadido la posibilidad de invertir el empuje de los aviones.
* Se ha añadido la posibilidad de eliminar vehículos individualmente
* Se han actualizado los mapas de entrada del mando de Xbox 360/One y del volante G29, y se ha añadido el mapa de entrada del mando inalámbrico de PS4*  
* Se ha corregido un problema por el que la recepción de una solicitud de aparición en la red no válida provocaba un fallo.
* Se ha corregido un problema por el que algunos objetos del lado del servidor no aparecían.
* Se ha corregido un problema con las luces de los aviones
* Se ha corregido un problema por el que aparecían artefactos en los aviones con cámara estática*  
* Se ha corregido un problema por el que las carreras no terminaban correctamente
* Se ha solucionado un problema por el que el enganche provocaba un fallo
* Se ha solucionado un problema con la carga de los localizadores ILS para las aeronaves


Como podeis ver los cambios son numerosos y de importancia, y podeis verlos al detalle en los enlaces del [este tema de su foro](https://forum.rigsofrods.org/threads/rigs-of-rods-2021-02-released.2999/). Si quereis conseguir el juego lo podeis descargar desde [itch.io](https://rigs-of-rods.itch.io/rigs-of-rods), o compilar su código fuente desde su [web del proyecto](https://github.com/RigsOfRods/rigs-of-rods). También **teneis un [repositorio](https://forum.rigsofrods.org/resources/) donde encontrar los diferentes vehículos y terrenos** donde poder "jugar". Os dejamos con el trailer oficial, que tiene ya unos añitos pero que es muy descriptivo de lo que podemos encontrar en el juego:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/bRbQ4OaljWs" style="display: block; margin-left: auto; margin-right: auto;" width="780"></iframe></div>


También acabamos de grabar un video probando este juego que podeis ver a continuación:  
  
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/FgFiMW2OE2k" style="display: block; margin-left: auto; margin-right: auto;" width="780"></iframe></div>

