---
author: Pato
category: Estrategia
date: 2018-02-22 18:46:14
excerpt: <p>El juego base se expande con nuevos contenidos y modos de juego</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/539ae9434274fcaedab8deb6007ffb6b.webp
joomla_id: 656
joomla_url: la-expansion-stellaris-apocalypse-ya-esta-disponible-en-gnu-linux-steamos
layout: post
tags:
- espacio
- estrategia
- dlc
- simulacion
title: "La expansi\xF3n 'Stellaris Apocalypse' ya est\xE1 disponible en GNU-Linux/SteamOS"
---
El juego base se expande con nuevos contenidos y modos de juego

Buenas noticias para los amantes y adictos de 'Stellaris' [[Steam](http://store.steampowered.com/app/281990/)]. Ya está disponible la expansión "Apocalypse" con nuevos contenidos, modos de juego y todo el mimo y el saber hacer de Paradox:


*Stellaris: Apocalypse es una expansión completa que redefine la guerra espacial para todos los jugadores con un nuevo elenco de opciones ofensivas y defensivas. Destruye mundos enteros con nuevas y aterradoras armas destructoras de planetas, combate contra (o junto a) implacables piratas espaciales y descubre, quizá, otras funciones no violentas del juego.*


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/AssQqRk3qQM" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 Novedades:


#### Eso no es una Luna, eso tampoco, eso podría ser una Luna, espera, no:


Mantén a raya los sistemas locales a través del miedo a la nueva arma destructora de planetas "Colossus" – un aterrador avance tecnológico que elimina mundos enteros del universo.


#### Tu base entera te pertenece:


Las nuevas y gigantescas embarcaciones "Titan" pueden liderar a tus flotas en la conquista, ofreciendo tremendas bonificaciones a las embarcaciones que estén bajo su mando. Mientras tanto, fortifica sistemas clave con gigantescas instalaciones orbitales y protege tu mundo como un bastión impenetrable entre las estrellas.


#### Piratas de la constelación:


Cuidado con los saqueadores – nómadas espaciales que asaltan imperios ya establecidos y viven al límite de la civilización. Contrátalos como mercenarios para tus propios conflictos, ¡pero ten cuidado, no sea que se alíen y desencadenen una nueva crisis en el juego!


#### Algunas funciones no violentas:


La expansión incorpora nuevos beneficios de ascensión y elementos cívicos, además de las nuevas ambiciones de unidad, que proporcionan nuevas formas de invertir la unidad y personalizar tu desarrollo.


#### Sonidos de destrucción:


Como acompañamiento a tu expedición destructora de planetas, tres nuevos temas musicales obra de Andreas Waldetoft serán una delicia para tus oídos.


Si te gusta el universo Stellaris, échale un vistazo a 'Stellaris Apocalypse' en Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/716670/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 

