---
author: Pato
category: Software
date: 2022-08-29 10:38:03
excerpt: "<p>Una nueva opci\xF3n para los que gustan de emular en Linux</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/CEMU/Cemu-20.webp
joomla_id: 1483
joomla_url: el-popular-emulador-cemu-lanza-su-version-experimental-2-0-con-version-para-linux-y-adopta-licencia-open-source
layout: post
tags:
- emulador
- cemu
title: "El popular emulador CEMU lanza su versi\xF3n experimental 2.0 con versi\xF3\
  n para Linux y adopta licencia Open Source"
---
Una nueva opción para los que gustan de emular en Linux


Dejando de lado el tema legal de si es lícito o ilícito utilizar ROMs, lo cierto es que los emuladores siempre han sido una opción para ejecutar videojuegos de otros sistemas en PC. Desde hace ya tiempo en Linux tenemos un buen número de emuladores para multitud de sistemas como MAME o Dolphin, pero ahora se suma el popular emulador de Wii U **CEMU** a la lista.


Según podemos leer en su [post en Reddit](https://www.reddit.com/r/cemu/comments/wwa22c/cemu_20_announcement_linux_builds_opensource_and/) el principal desarrollador del proyecto ha comunicado que **CEMU pasa a ser open source** bajo licencia Mozilla Public License 2.0. 


Según explica, los motivos para dar este paso son variados, incluyendo el hecho de que el otro desarrollador principal del proyecto lo dejó hace unos meses y es necesario que se puedan incorporar nuevos desarrolladores para seguir, y que tras 8 años al frente siente que es momento de dar paso a nuevas incorporaciones quedándose el "en la trastienda". 


En cualquier caso, CEMU 2.0 ya está disponible en su [página del proyecto en Github](https://github.com/cemu-project/Cemu), pero aún no hay una versión compilada para Linux por lo que si queremos probarlo deberemos compilarlo por nuestra cuenta, aunque el desarrollador ya ha anunciado que está estudiando **publicar las versiones para Linux como appimage o como flatpak**. Además, debido a la gran cantidad de cambios introducidos, nuevas características y la compilación para Linux debe considerarse esta nueva versión como *experimental*, y pueden encontrarse algunos problemas o inconsistencias sobre todo en la interfaz de usuario. 


Si quieres saber más sobre CEMU puedes visitar su [página web oficial](https://cemu.info/).

