---
author: Serjor
category: Apt-Get Update
date: 2017-07-15 04:00:00
excerpt: <p>El Early Access se abre paso poco a poco</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/3d9c691fd3a1e80f63a4a70c2a2e3155.webp
joomla_id: 409
joomla_url: una-semana-mas-os-traemos-un-apt-get-con-las-ultimas-novedades
layout: post
tags:
- aptget
title: "Una semana m\xE1s os traemos un apt-get con las \xFAltimas novedades"
---
El Early Access se abre paso poco a poco

Cómo viene siendo habitual os traemos una recopilación de noticias que por un motivo u otro no os hemos reportado como artículos, pero que consideramos interesantes:


Desde <http://linuxgamenews.com> tenemos:


* [Albion Online anuncia fechas y horas concretas para su lanzamiento oficial](http://linuxgamenews.com/post/162874734567/albion-online-release-times-and-starter-packs#_=_)
* [The Escapist 2 llegará el 22 de agosto](http://linuxgamenews.com/post/162912131706/the-escapists-2-will-break-out-august-22nd)
* [Comienza la Beta/Early Access para Ashworld](http://linuxgamenews.com/post/162912129538/ashworld-now-in-early-access-for-linux-mac-windows)
* [Es posible jugar a Beyond Enemy Lines en estado beta para Linux](http://linuxgamenews.com/post/162912129543/beyond-enemy-lines-now-playable-on-linux)
* [Tangledeep, un RPG por turnos inspirado en los clásicos de 16Bits, llegará a Linux](http://linuxgamenews.com/post/162990416898/tangledeep-dungeon-crawler-rpg-coming-soon)
* [La actualización de Sublevel Zero es tan grande, que le han añadido la coletilla Redux y lo han puesto de oferta](http://linuxgamenews.com/post/162990417043/sublevel-zero-redux-is-a-huge-update)
* [La temporada 1 de The Lion's Song ya esá disponible al completo en Linux](http://linuxgamenews.com/post/162990413955/the-lions-song-releases-full-season)


En [http://boilingsteam.com](http://boilingsteam.com/) nos traen [una interesante comparativa](http://boilingsteam.com/distros-used-for-gaming-2017-vs-2016/) de las distros Linux más usadas para jugar en el 2016 y el 2017.


También leemos en <https://www.gamingonlinux.com/> que:


* [El RTS Tooth and Tail tendrá versión para Linux](https://www.gamingonlinux.com/articles/tooth-and-tail-a-new-rts-game-from-pocketwatch-games-looks-awesome-confirmed-for-linux.9996)
* [GOG nos trae la serie de videojuegos The Humans a través de DosBox](https://www.gamingonlinux.com/articles/gog-have-released-dosbox-wrapped-copies-of-the-humans-series-with-linux-versions.9986)
* [El RPG táctico Robothorium tendrá versión para Linux](https://www.gamingonlinux.com/articles/robothorium-a-futuristic-tactical-rpg-with-turn-based-combat-is-heading-to-linux.9981)
* [Starship Theory tiene una versión para Linux disponible, aunque el juego todavía está en Early Access](https://www.gamingonlinux.com/articles/starship-theory-just-launched-a-linux-version-developer-looking-for-feedback.9976)
* [Terroir 3D también ha liberado una versión para Linux, y también está en Early Access](https://www.gamingonlinux.com/articles/terroir-a-3d-tile-based-vineyard-tycoon-game-adds-linux-support.9974)
* [Geocore también en Early Access, y también con una versión para Linux recién estrenada, nos llega con soporte para Vulkan](https://www.gamingonlinux.com/articles/geocore-another-six-degrees-of-freedom-shooter-has-linux-vulkan-support.9973)


Y esta vez oímos en vez de leemos en [https://linuxgamecast.com/](https://linuxgamecast.com/2017/07/linuxgamecast-weekly-255-grain-of-sandy-salt/) que los chicos de feral están trasteando con [Vulkan en Hitman](https://www.youtube.com/watch?v=yqalQSAdxb8)


Y comenzamos con los vídeos:


Albion Online


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/HKb8NPs5YDY" width="560"></iframe></div>


The Escapists 2


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/Uml0vz48krk" width="560"></iframe></div>


Ashworld


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/rY2L0LiR8-Q" width="560"></iframe></div>


Beyond Enemy Lines


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/IgpRGVGE5XQ" width="560"></iframe></div>


Tangledeep


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/YkJJTGmNyhE" width="560"></iframe></div>


Sublevel Zero Redux


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/RU-NlZzAPTE" width="560"></iframe></div>


The Lion's Song


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/iNtavcpPADI" width="560"></iframe></div>


Tooth And Tail


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/nC99U9cJGR4" width="560"></iframe></div>


Robtothorium


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/V0kXKMAA374" width="560"></iframe></div>


Starship Theory


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/qYDXP89xNwQ" width="560"></iframe></div>


Terroir


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/exPwH3OfJD0" width="560"></iframe></div>


Geocore (gameplay de youtube)


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/FeAJVkfxtHo" width="560"></iframe></div>


Hitman - Vulkan


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/yqalQSAdxb8" width="560"></iframe></div>


 


Y tú, ¿echas de menos alguna noticia para este apt-get? Déjanos el enlace en los comentarios.

