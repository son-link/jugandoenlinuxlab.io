---
author: leillo1975
category: "Simulaci\xF3n"
date: 2019-12-05 10:14:08
excerpt: "<p>@SCSsoftware nos trae su nueva expansi\xF3n de mapa</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ETS2/RoadToTheBlackSea/ETS2_RttBS_Custom_900A.webp
joomla_id: 1136
joomla_url: road-to-the-black-sea-la-nueva-dlc-de-euro-truck-simulator-2-disponible
layout: post
tags:
- ets2
- dlc
- scs-software
- euro-truck-simulator-2
- road-to-the-black-sea
title: '"Road to the Black Sea", la nueva DLC de Euro Truck Simulator 2, disponible. '
---
@SCSsoftware nos trae su nueva expansión de mapa


 La semana pasada [os adelantábamos la fecha de salida](index.php/homepage/generos/simulacion/1044-road-to-the-black-sea-sera-la-nueva-expansion-de-euro-truck-simulator-2) de lo nuevo de la compañía checa, y hoy fieles a la cita volvemos a la carga con este artículo que celebra su puesta a la venta hace unos minutos en Steam. Con esta [nueva entrada en su Blog](https://blog.scssoft.com/2019/12/road-to-black-sea-release.html) y este tweet, SCS anunciaba la llegada de "Road to the Black Sea":



> 
> Road to the Black Sea for Euro Truck Simulator 2 is now available! ?? ?? ??   
>   
> Journey through 3 brand new countries featuring many major cities, towns, villages, landmarks, industries & more! ?  
>   
> Check it out on the Steam Store Page: <https://t.co/NInhG4Xk3b> [pic.twitter.com/HxSMM9Hcxa](https://t.co/HxSMM9Hcxa)
> 
> 
> — SCS Software (@SCSsoftware) [December 5, 2019](https://twitter.com/SCSsoftware/status/1202634990778892288?ref_src=twsrc%5Etfw)



 Los que somos seguidores de ATS y ETS2 sabemos que ultimamente los desarrolladores de estos juegos están con la marcha más larga puesta y nos están trayendo sus ampliaciones de mapa cada vez más a menudo. Si hacemos memoria, en el último año hemos estado sembrados de productos de SCS, empezando por "[Beyond the Baltic Sea](index.php/component/k2/20-analisis/1067-analisis-euro-truck-simulator-2-beyond-the-baltic-sea-dlc)" (ETS2) a finales de 2018, y continuando con [Washington](index.php/component/k2/20-analisis/1196-analisis-american-truck-simulator-washington-dlc) y posteriormente [Utah](index.php/component/k2/14-simulacion/1251-lanzada-la-dlc-utah-para-american-truck-simulator), del que os debemos un análisis, para el "hermano americano". En esta ocasión nos vamos al [Mar Negro](https://es.wikipedia.org/wiki/Mar_Negro), continuando por tierras del este, pero en esta ocasión más hacía el sur, por **carreteras de Rumanía, Bulgaria y la Tracia turca** (la parte europea de Turquía).


![ETS2 RttBSbalkan e blog map big](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ETS2/RoadToTheBlackSea/ETS2_RttBSbalkan_e_blog_map_big.webp)


La descripción oficial de esta DLC es la siguiente:


*El camino hacia el Mar Negro trae tres nuevas regiones europeas. Los conductores podrán expandir su empresa y entregar a los países de Rumania, conocida por la región boscosa de Transilvania y la cordillera de los Cárpatos que la rodea, Bulgaria con su diverso terreno y la costa del Mar Negro, y la región de Trakya de Turquía, que es la puerta de entrada a la ciudad más grande de Europa, Estambul.*


*Desde las grandes ciudades hasta las pequeñas y humildes aldeas en el campo, los camioneros también podrán conducir por carreteras que conducen a pintorescas costas a lo largo del Mar Negro.*


*Estas regiones también ofrecen una gran variedad de industrias para que los agentes las entreguen desde y hacia las granjas, las empresas de logística, la minería y las industrias siderúrgicas.*


  
***Características principales***


*Más de 10.000 kilómetros de carreteras por las que circular.*  
 *Rumania, Bulgaria y la parte europea de Turquía para explorar*  
 *La ciudad más grande de Europa, Estambul.*  
 *11 nuevos muelles e industrias de la compañía local*  
 *20 nuevas ciudades principales y muchos pueblos y asentamientos más pequeños*  
 *Hitos famosos y lugares reconocibles*  
 *Arquitectura balcánica característica*  
 *Nuevos y únicos activos 3D*  
 *Exuberante vegetación regional*  
 *Trenes locales de inteligencia artificial, tranvías y coches de tráfico*  
 *Carros de caballos - para el campo búlgaro y rumano*  
 *Cruces fronterizos - incluyendo controles fronterizos ricos en características*  
 *Ferry fluvial sobre el Danubio (Dunaj)*  
 *Logros de la región del Mar Negro para desbloquear*


Como siempre, en JugandoEnLinux.com os ofreceremos una serie de Streams especiales mostrandoos el contenido de esta DLC en nuestros canales de [Youtube](https://www.youtube.com/c/jugandoenlinuxcom) y [Twitch](https://www.twitch.tv/jugandoenlinux) para que nos acompañeis mientras recorremos las carreteras de esta DLC, por lo que os recomendamos que permanezcais atentos a nuestras redes sociales ([Twitter](https://twitter.com/JugandoenLinux) y [Mastodon](https://mastodon.social/@jugandoenlinux)). También podreis disponer de un completo análisis unas semanas más tarde aquí mismo, en JugandoEnLinux.com. Nos gustaría agradecer a SCS Software su colaboración facilitándonos una copia de "Road to the Black Sea" para poder crear estos contenidos. Os dejamos con el trailer de esta nueva DLC:


 <div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/DVfCDX64xiM" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


Podeis adquirir Euro Truck Simulator 2 - Road to the Black Sea DLC en [Steam](https://store.steampowered.com/app/1056760/Euro_Truck_Simulator_2__Road_to_the_Black_Sea/) por **17.99 €**. ¿Qué opinión te merece este nuevo territorio? ¿Te han gustado las expansiones anteriores? Cuéntanoslo en los comentarios, o en mensajes en nuestra comunidad de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

