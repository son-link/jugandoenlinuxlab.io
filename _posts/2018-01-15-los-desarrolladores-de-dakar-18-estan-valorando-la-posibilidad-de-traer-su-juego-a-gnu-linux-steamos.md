---
author: leillo1975
category: Carreras
date: 2018-01-15 16:08:35
excerpt: "<p style=\"margin-bottom: 0cm; line-height: 100%;\">El juego de <span class=\"\
  username u-dir\" dir=\"ltr\">@BigmoonEnt</span> podr\xEDa llegar despu\xE9s de ser\
  \ lanzado en el resto de plataformas.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/549dfa53895c3908b16352e73edc2d26.webp
joomla_id: 607
joomla_url: los-desarrolladores-de-dakar-18-estan-valorando-la-posibilidad-de-traer-su-juego-a-gnu-linux-steamos
layout: post
tags:
- dakar-18
- bigmoon-entertaiment
- deep-silver
title: "Los desarrolladores de Dakar 18 est\xE1n valorando seriamente traer su juego\
  \ a GNU-linux / SteamOS (ACTUALIZADO 2)"
---
El juego de @BigmoonEnt podría llegar después de ser lanzado en el resto de plataformas.

**ACTUALIZACIÓN 25-9-18:** Finalmente el juego ha sido lanzado para Windows y consolas, pero la versión para nuestro sistema desgraciadamente no está incluida. En respuesta a un [mensaje en los foros de Steam](https://steamcommunity.com/app/767390/discussions/1/1735465524722005969/?tscn=1537893777#c1735465524722010481) sobre la versión para Linux del juego, uno de los desarrolladores ha comentado lo siguiente: 



> 
> *"We had previously stated that, while Linux support is on the todo list, it's not a priority issue.*  
> *We'll probably look more into it soon"*
> 
> 
> **Traducido**: "Ya habíamos dicho que, aunque el soporte de Linux está en la lista de tareas pendientes, no es un tema prioritario.   
> Probablemente lo investigaremos más **pronto**."
> 
> 
> 


Como veis seguimos igual que antes, pero al menos no niegan la posibilidad de portarlo. Crucemos los dedos para que esto no caiga en el olvido y más **pronto** que tarde.... o nunca.....




---


**ACTUALIZACIÓN (18-1-18):** Acabamos de leer en un comentario de la [página de Facebook del juego](https://www.facebook.com/dakarthegame/) (ver abajo), que el juego irá encaminado a ofrecer una **experiencia realista** y no arcade. Será un **simulador** donde tengamos que hacer uso de la navegación, y donde cada etapa, de las 14 en total, durará entre una y dos horas para completar. Los desarrolladores no ven una prioridad la Realidad Virtual, aunque no la descartan como parche o en nuevas entregas. También intentarán dar el mejor soporte posible a Volantes y Pedales. Podeis ver las palabras exactas aquí:


<div class="resp-iframe"><iframe height="161" src="https://www.facebook.com/plugins/comment_embed.php?href=https%3A%2F%2Fwww.facebook.com%2Fdakarthegame%2Fposts%2F1570307129719387%3Fcomment_id%3D1570335069716593%26reply_comment_id%3D1570371383046295&amp;include_parent=false" style="overflow: hidden; display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Os recordamos una vez más que **el soporte para GNU/Linux-SteamOS aun no está confirmado**, por lo que hay que ser muy cautos y no "emocionarse" demasiado antes de tiempo.


 




---


**NOTICIA ORIGINAL:** Para el que no lo conozca, Dakar 18 es un juego que está siendo desarrollado por [Bigmoon Entertaiment](http://www.bigmoonstudios.com) (Neighbours from Hell, MotoGP13). Como el nombre indica, se trata de un título que nos permitirá realizar la peligrosa carrera comodamente desde nuestro escritorio o sillón favorito, a través de extensos mapas,  y conduciendo coches, motos y camiones, quads, etc. Según las notas de prensa el juego se está desarrollando como **uno de los mundos abiertos más grandes que se han llegado a crear**, y dispondrá de **licencia oficial**, por lo que veremos los equipos y pilotos reales de la prueba. Podremos disfrutar tanto del modo de un solo jugador como de partidas online. El juego editado por **Deep Silver** (Koch Media) , que será lanzado en las plataformas principales en la época estival, podría llegar a nuestro sistema. Como sabeis, no es la primera vez que Deep Silver lanza juegos para GNU-Linux, pues títulos tan conocidos como las sagas Dead Island, Metro o Saints Row ya aterrizaron antes.


En primer lugar nos gustaría decir que todo este tema debe ser cogido con pinzas. **Estamos hablando de una posibilidad**, ya que **en ningún momento los desarrolladores han confirmado nada**, y por lo tanto debemos ser cautos y no hacer las cuentas de la lechera. Pero cierto es que, el que contemplen esa posibilidad, al menos nos da la esperanza de poder disponer de él en un futuro. Las palabras exactas que nos han sido comunicadas por parte del departamento de prensa de la desarrolladora son las siguientes (traducido del Inglés):



> 
> “Por ahora, nuestro principal objetivo es cumplir con los plazos y lanzar el juego este verano para las principales plataformas. **Eventualmente, si nuestros horarios nos lo permiten, también portaremos y lanzaremos el juego para Linux/SteamOS**. No voy a mentir y decir que esta es una prioridad para nosotros en este momento, pero **puedo asegurarte que está en la lista de deseos**."
> 
> 
> *"For now our main goal is to meet the deadlines and release the game this Summer for the main platforms. Eventually, if our schedules allow us, we'll port and release the game for Linux/SteamOS as well. I won't lie and say this is a priority for us at the moment, but i can assure you it's on the wishlist."*
> 
> 
> 


En una segunda comunicación, el tema de una versión para Linux se afianzó más:



> 
> "Acabo de tener una reunión de prensa rápida y surgió el tema de Linux. **Definitivamente está en una lista de tareas pendientes, más que una lista de deseos** ... veamos qué nos depara el tiempo"
> 
> 
> *"Just had a quick press meeting and the Linux subject came up. it's definitely on a todo list, more than a wishlist... let's see what the time allows us"*
> 
> 
> 


El juego está siendo desarrollado con **Unreal Engine 4**, lo que da cierta garantía de calidad. Esperemos que nos hagan un huequito en si apretada agenda y ponto podamos surcar las dunas del desierto usando nuestras distribuciones favoritas. Para ir abriendo nuestro apetito han editado un espectacular trailer que puedes ver a continuación:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/B3cGetYXaAc" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 En JugandoEnLinux.com **estaremos pendientes de este juego** y te traeremos cualquier noticia relevante que se produzca en cuanto a su **posible desarrollo en GNU-Linux/SteamOS**.


¿Te parece atractivo lo que propone este juego? ¿Crees que llegará a Linux? Dejanos tus impresiones en los comentarios o en nuestro canal de Telegram.

