---
author: chema
category: Estrategia
date: 2021-10-11 08:51:37
excerpt: "<p>Un juego con unos a\xF1os a la espalda que pas\xF3 desapercibido y que\
  \ merece la pena visitar de cara a pr\xF3ximas fechas&nbsp;@ZN_Terror</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Zombienightterror/zombienightterror1.webp
joomla_id: 1373
joomla_url: descubre-zombie-night-terror-apocaliptico-puzlezombie
layout: post
tags:
- indie
- puzzles
- zombies
- estrategia
title: "Descubre Zombie Night Terror: apocal\xEDptico puzlezombie"
---
Un juego con unos años a la espalda que pasó desapercibido y que merece la pena visitar de cara a próximas fechas @ZN_Terror


Hola a todos.


Como gran fan de las películas y videojuegos de zombis que soy, y como se acercan fechas propicias para este tipo de juegos, voy a hablarte de **Zombie Night Terror** un juego de estrategia y puzzle que me encontré por casualidad navegando en busca de juegos una tarde.


**El juego:**


Zombie Night Terror es un juego lanzado en el año 2016 en el que básicamente controlas un Apocalipsis zombie, tiene un estilo clásico que recuerda mucho a las películas mas conocidas como la noche de los muertos vivientes, El regreso de los muertos vivientes, etc y referencias a George A.Romero pero todo como una mezcla de 8 bits bastante interesante y buen humor.


-Tiene un montón de detalles como una tv que te informa como en una especie de noticiario como funcionan algunas cosas, y el selector de niveles que es como si entrases al cine a ver una peli. Además, recibió una actualización llamada "Moonwalkers" en la que lleva la acción fuera del planeta Tierra. También cuenta con un editor de niveles por si te sientes creativo y decides montarte la fiesta por tu cuenta.


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/RJNcyhXxmL0" title="YouTube video player" width="560"></iframe></div>


 


**Jugabilidad:**


La jugabilidad es simple: los zombies se mueven mientras el usuario tiene que usar sus habilidades, o incluso pudiendo infectar a otros humanos para hacer que los propios zombies infecten, a otros congeneres, y así ir consiguiendo infectar a todos en el nivel o los suficientes. Por decirlo de alguna manera, es como un Lemmings pero con zombies.


**Gráficos:**


Los gráficos son geniales , la estética retro-moderna mola mucho y tiene ese ambiente a terror que tanto gusta en las pelis del género.


Te dejo el vídeo de lanzamiento del juego:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/vvwpekr93vY" title="YouTube video player" width="560"></iframe></div>


 


Conclusión:


Buen juego, con buenas mecánicas y divertido por momentos. Si quieres más información, puedes visitar su página web oficial [www.zombienightterror.com](https://www.zombienightterror.com), visitar [su página en Humble Bundle](https://www.humblebundle.com/store/zombie-night-terror?partner=jugandoenlinux) (enlace patrocinado) o su página en Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/416680/84029/" style="border: 0px;" width="646"></iframe></div>


 

