---
author: Serjor
category: Software
date: 2021-01-10 11:02:59
excerpt: "<p>Ya tenemos lanzador nativo para Epic Games Store, no oficial, claro</p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/HeroicGamesLauncher/heroicgameslauncher_1_0.webp
joomla_id: 1267
joomla_url: heroic-games-launcher-ha-sido-lanzado-en-su-version-1-0-0
layout: post
tags:
- epic-game-store
- heroic-games-launcher
title: "Heroic Games Launcher ha sido lanzado en su versi\xF3n 1.0.0"
---
Ya tenemos lanzador nativo para Epic Games Store, no oficial, claro


La comunidad de GNU/Linux es de lo que no hay, para lo bueno y para lo malo. Somos capaces de lo mejor, de lo peor, de apoyar económicamente a GNOME contra un lobby aunque luego lo critiquemos con todo nuestro odio, o de "pon aquí lo que quieras, que la comunidad lo ha hecho o lo va a hacer fijo".


Y cómo comunidad incoherente que somos, damos soporte a Epic Games Store aunque nos parezca el padre de Satán. Y es que a parte de Lutris, que nos permite desde hace tiempo usar EGS en GNU/Linux, tenemos también Legendary, un cliente CLI para instalar y ejecutar juegos de EGS. Y aunque a un linuxero de pura cepa eso de usar la línea de comandos no le supone ningún problema, ¿por qué usar la línea de comandos si podemos hacer una interfaz gráfica? Así que la comunidad se ha puesto manos a la obra, y tenemos ya la versión 1.0 de Heroic Games Launcher , que en realidad es un frontend para Legendary, ya que es este quién hace todo el trabajo sucio, y HGL es una interfaz, bastante chula tengo que decir, que nos facilita el instalar y ejecutar los videojuegos de EGS.


La verdad que el caso de HGL ha sido un tanto curioso. No hace ni quince días que el desarrollador preguntaba en [Reddit](https://www.reddit.com/r/wine_gaming/comments/klxhfz/im_working_on_a_native_epic_games_launcher_for/) un nombre para la aplicación, y aunque se entiende que para entonces ya tenía parte del código avanzado, ayer mismo lanzó la [versión 1.0](https://github.com/flavioislima/HeroicGamesLauncher/releases/tag/v1.0.0), que permite:


* Loguearse/desloguearse en EGS
* Ver la biblioteca de juegos que tengamos en EGS
* Instalar y desinstalar juegos
* Gestionar la configuración de wine con la que se ejecutará el juego (de manera muy limitada)
* Abrir desde el lanzador la página de la tienda del juego
* Abrir desde el lanzador los reportes en protondb del juego


La verdad es que es sorprendente que en tan poco tiempo hayan conseguido una aplicación con un resultado muy aparente y que "funciona" (tienen algún fallo, pero bueno, nada serio).


Personalmente he probado a lanzar Elite Dangerous (tengo debilidad por este juego, y eso que apenas he jugado media en mi vida, qué le voy a hacer), y afortunadamente para mí no ha funcionado (sino no estaría ahora mismo escribiendo esta noticia, no os voy a mentir), y es que en el caso particular de este juego hay que hacer ajustes en la configuración de wine para que vaya, pero esto lo dejamos para otro momento...


Y así estamos, en esta dicotomía en la que no queremos ni oír de hablar de Epic por su desprecio a GNU/Linux, pero luego como comunidad hacemos lo que haga falta para poder jugar a los juegos que nos regala la tienda (porque Epic en realidad es la tienda que la gente usa para conseguir juegos gratis y Borderlands 3), e incluso damos publicidad a un software que si bien es de código abierto y nativo para nuestro sistema preferido (cosa que aplaudimos y apoyamos incondicionalmente), en el fondo sirve para apoyar una causa que no queremos apoyar.


Tendremos que querernos como somos, qué le vamos a hacer.


Dinos qué te parece este lanzador en los comentarios o en nuestros canales de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org) y [Telegram](https://t.me/jugandoenlinux)

