---
author: Pato
category: "Acci\xF3n"
date: 2018-12-07 10:07:52
excerpt: <p>El primer Battle Royale 'serio' llega a Linux/SeamOS</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/a39a6f25dc4540ba260d65470894f1a3.webp
joomla_id: 922
joomla_url: valve-lanza-el-modo-de-cs-go-danger-zone-en-modalidad-free-to-play
layout: post
tags:
- accion
- free-to-play
- fps
- battle-royale
title: Valve lanza el modo de CS:GO - Danger Zone en modalidad "free to play"
---
El primer Battle Royale 'serio' llega a Linux/SeamOS

Tras los últimos fiascos e intentos de traer un "battle royale" a Linux/SteamOS, al final la propia Valve ha decidido tomar cartas en el asunto y aprovechando su base del juego **Counter Strike: Global Offensive** ha desarrollado un modo "Battle Royale" para tratar de competir dentro de esta modalidad de juego.


Además, la llegada de **'CS:GO Danger Zone'**, que así se llama la modalidad ha supuesto el cambio de Counter Strike: Global Offensive **"free to play"**, de modo que puedes jugarlo sin gastar ni un euro.


Danger Zone es una modalidad battle royale basada en la mecánica táctica de CS:GO donde los jugadores usan su ingenio, habilidad y recursos para luchar hasta el final. Juega solo o trabaja en equipo con dos o tres miembros y enfréntate con hasta 16 jugadores simultáneos (18 si entras junto a otros amigos en modo escuadrón) dentro de un mapa que irá haciéndose mas y mas pequeño a medida que avanza la partida. La duración aproximada de cada enfrentamiento es de unos 10 minutos.  
  
Todos los jugadores que hayan jugado a CS:GO antes de la actualización recibirán una insignia de lealtad conmemorativa que podrán mostrar en su perfil. Además, las cuentas de todos los jugadores existentes de CS:GO han sido actualizadas a estatus Prime.  
  
Como antes, el estatus Prime te empareja con otros jugadores que tengan el mismo estatus. Además, te dará acceso a la nueva apariencia conmemorativa Ratas de laboratorio para el MP5-SD (que puedes conseguir obteniendo EXP en partidas de Danger Zone) y a la nueva caja Danger Zone.


Si quieres saber mas, puedes visitar su [página web](http://www.counter-strike.net/dangerzone/), o la página del juego en Steam:


<https://store.steampowered.com/app/730/CounterStrike_Global_Offensive/>


Está claro que el movimiento de Epic con Fortnite y su próxima tienda está haciéndole mover ficha a Valve. ¿Tu que piensas?


Cuéntamelo en los comentarios o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

