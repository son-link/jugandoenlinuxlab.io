---
author: Odin
category: Rol
date: 2022-12-13 11:22:03
excerpt: "<p>Espadas, Magia, Mechas y mucho m\xE1s en este prometedor RPG.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ChainedEchoes/chainedEchoes.webp
joomla_id: 1513
joomla_url: publicado-el-juego-de-rol-chained-echoes-para-linux
layout: post
tags:
- rol
- jrpg
- pixel-art
- nativos
- steam-deck
title: 'Publicado el juego de rol Chained Echoes para Linux '
---
Espadas, Magia, Mechas y mucho más en este prometedor RPG.


Recientemente publicado para Linux, [Chained Echoes](http://www.chainedechoes.com//) es un videojuego que pretende ser un homenaje a los **clásicos JRPG de 16 bits** que se publicaron en la **Super Nintendo**. Y si hacemos caso de las valoraciones que están haciendo los usuarios en **Steam**, que son **muy positivas**, parece que lo ha conseguido con creces.


El argumento del juego trata de un grupo de jóvenes que viajan a través de Valandis, un continente que se encuentra envuelto en una feroz guerra entre varios reinos. A lo largo de la partida, que según sus desarrolladores dura entre **30 y 40 horas**, podremos visitar una gran variedad de paisajes, en las que nos encontraremos desde grandes ciudades hasta peligrosas mazmorras. Además, para dar un punto de variedad jugable, hay múltiples minijuegos y misiones secundarias que nos harán más variada la partida.


![lindenstrasse2](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ChainedEchoes/lindenstrasse2.webp)


Los gráficos, como podéis ver en las imágenes del artículo, están hechos en un **pixel art muy cuidado** y que nos recuerdan a los grandes clásicos de la época de las 16 bits. Un punto que nos parece muy interesante, es que al contrario que la gran mayoría de los videojuegos de esta época, aunque las batallas son a turnos, no hay encuentros aleatorios, sino que siempre podemos ver a los enemigos que vamos a combatir.


Los desarrolladores prometen un sistema completo de habilidades, equipamiento y creación de objetos entre otras características. Tampoco queremos dejar de destacar las batallas con **Mechas** y la posibilidad de movernos por el mundo a través de un **dirigible** que podremos personalizar a nuestro gusto.


![airboat](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ChainedEchoes/airboat.webp)


Desde Jugando en Linux estamos encantados de que salgan juegos nativos para nuestro sistema, y si además tenemos en cuenta que el juego también está verificado para la **Steam Deck**, parece que es el juego perfecto para echar una partida durante estas próximas vacaciones de Navidad. Chained Echoes lo podéis adquirir, con una **rebaja del quince por ciento**, tanto en [GOG](https://www.gog.com/en/game/chained_echoes) como en [Steam](https://store.steampowered.com/app/1229240/Chained_Echoes/).


¿Qué te parece [Chained Echoes](http://www.chainedechoes.com/)? Cuéntanos acerca de este espectacular JRPG en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).


 

