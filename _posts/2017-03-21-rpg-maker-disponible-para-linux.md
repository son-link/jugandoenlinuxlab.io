---
author: Serjor
category: Software
date: 2017-03-21 19:53:44
excerpt: "<p>A parte de exportar para Linux, tambi\xE9n proporcionan un editor nativo</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/e561084e9220383c7204eb40f9bc13f1.webp
joomla_id: 260
joomla_url: rpg-maker-disponible-para-linux
layout: post
tags:
- rpg-maker
title: RPG Maker disponible para Linux
---
A parte de exportar para Linux, también proporcionan un editor nativo

No solemos comentar mucho por aquí actualizaciones o lanzamientos de software, pero esta ocasión lo merece. Enterbrain, la compañía que desarrolla RPG Maker, ha sacado una nueva versión de su editor, el cuál tiene la capacidad de exportar para Linux, y además incluye también un editor para poder trabajar y editar de manera nativa bajo nuestra plataforma.


![anuncio rpgmaker linux](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/rpgmaker/anuncio_rpgmaker_linux.webp)


Son grandes noticias ya que mucha gente utiliza este software para llevar a cabo sus creaciones, y que tengan la opción de exportar a Linux abre las puertas a que nuevos proyectos lleguen a Linux.


Además, como podéis observar, está disponible desde Steam, facilitándonos el tener debidamente actualizado un software que normalmente se encuentra fuera de los repositorios.


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" frameborder="0" height="315" src="https://www.youtube.com/embed/AdLt8ZcyCqA" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


<div class="steam-iframe"><iframe frameborder="0" height="190" src="https://store.steampowered.com/widget/363890/80322/" width="646"></iframe></div>


 


Y tú, ¿ya estás pensando en como será tu primer RPG? Coméntanos qué te parece la noticia en un comentario o a través de nuestros canales en [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 

