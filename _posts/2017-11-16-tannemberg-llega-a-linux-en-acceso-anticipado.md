---
author: Pato
category: "Acci\xF3n"
date: 2017-11-16 12:47:39
excerpt: "<p>El juego de acci\xF3n ambientado en la Primera Guerra Mundial es obra\
  \ del estudio responsable del notable 'Verdun'</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/afb2f0b609b92600310905cf1a1820fe.webp
joomla_id: 539
joomla_url: tannemberg-llega-a-linux-en-acceso-anticipado
layout: post
tags:
- accion
- primera-persona
- multijugador
- acceso-anticipado
- masivo
- mmofps
title: '''Tannemberg'' llega a Linux en acceso anticipado'
---
El juego de acción ambientado en la Primera Guerra Mundial es obra del estudio responsable del notable 'Verdun'

Hoy por fin ha salido de forma oficial en acceso anticipado la "secuela" del notable '[Verdun](http://store.steampowered.com/app/242860/Verdun/)', también disponible en Linux/SteamOS y del que ya os hemos hablado en Jugando en Linux en diversas ocasiones.


Tannemberg [[web oficial](https://www.1914-1918series.com/tannenberg/)] es obra del estudio Blackmill Games y esta vez nos trasladan al frente del este en este MMOFPS ambientado en la Primera Guerra Mundial.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/IR6k2MagQbs" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


"*Participa en la lucha entre el Imperio Ruso y las Fuerzas Centrales. Tannemberg ofrece una nueva experiencia para los jugadores veteranos con cuatro nuevos escuadrones, mas de 20 armas, cuatro mapas que dan a los jugadores libertad táctica, y un nuevo modo de juego con hasta 64 jugadores con soporte para IA para una experiencia épica de batalla"*


Los requisitos mínimos para disfrutar del juego son:


**Mínimo:**  

+ **SO:** Ubuntu 12.04 o superior
+ **Procesador:** Intel Core2 Duo 2.4Ghz o Superior / AMD 3Ghz or Superior
+ **Memoria:** 4 GB de RAM
+ **Gráficos:** Geforce GTX 960M / Radeon HD 7750 o superior, 1GB
+ **Red:** Conexión de banda ancha a Internet
+ **Almacenamiento:** 6 GB de espacio disponible
+ **Notas adicionales:** Solo multijugador, asegúrate de tener una conexión a internet rápida y estable.


'Tannemberg' no está disponible en español, pero si estás interesado en participar y tratar de sobrevivir en las carnicerías batallas de la Primera Guerra Mundial y el idioma no es problema para ti, lo tienes disponible en su página de Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/633460/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>
 


