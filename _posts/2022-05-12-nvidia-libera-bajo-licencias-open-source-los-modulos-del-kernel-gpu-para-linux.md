---
author: Pato
category: Noticia
date: 2022-05-12 09:02:19
excerpt: "<p>Que alguien mande abrigos al infierno, por caridad</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/NVIDIA/nvidia-logosm.webp
joomla_id: 1465
joomla_url: nvidia-libera-bajo-licencias-open-source-los-modulos-del-kernel-gpu-para-linux
layout: post
tags:
- drivers
- nvidia
title: "Nvidia libera bajo licencias open source los m\xF3dulos del kernel GPU para\
  \ Linux"
---
Que alguien mande abrigos al infierno, por caridad


Como suele decir el clásico, ¡qué momento para estar vivos!.


Menudo año 2022 llevamos, con el lanzamiento de multitud de tecnologías, hardware... pero algo gordo se intuía a últimas horas de la pasada noche cuando empezaron a llegarnos alarmas y avisos por doquier. Y no era para menos, pues en un movimiento que ha cogido por sorpresa a más de uno [Nvidia ha publicado sus módulos del kernel GPU bajo licencias GPL/MIT.](https://developer.nvidia.com/blog/nvidia-releases-open-source-gpu-kernel-modules/)


Este movimiento supone un cambio radical en el enfoque que hasta ahora había mantenido la empresa de tecnología gráfica más potente del planeta respecto a sus drivers y controladores, y sobre todo supone un impulso sin precedentes en el soporte y adopción de tecnologías opensource.


Tal y como ellos mismos exponen, este movimiento es *un paso significativo hacia la mejora de la experiencia de usar GPU NVIDIA en Linux, para una integración más estrecha con el sistema operativo y para que los desarrolladores depuren, integren y contribuyan. Para los proveedores de distribución de Linux, los módulos de código abierto aumentan la facilidad de uso. También mejoran la experiencia de usuario lista para firmar y distribuir el controlador de GPU NVIDIA. [...] Esto ayudará a mejorar aún más la calidad y seguridad del controlador GPU NVIDIA con aportes y revisiones de la comunidad de usuarios finales de Linux.*


Hay que tener en cuenta que de momento los drivers liberados **solo se enfocan a las familias de chips gráficos Turin y Ampere** enfocados sobre todo en centros de datos y computación en la nube, **quedando el soporte para estaciones de trabajo y GPU's Geforce en estado alfa.**


También anuncian que han estado trabajando con las grandes empresas del sector como Canonical, SUSE o Red Hat para poder integrar el kernel GPU en las distribuciones más importantes y tratar de adoptar el mejor enfoque de cara a una futura integración en el kernel Linux. De hecho, estas distribuciones ya son capaces de empaquetar y distribuir el nuevo driver, y se espera que pronto lleguen a las distribuciones principales.


En cuanto al mundo del videojuego en Linux, este movimiento no supone un cambio sustancial en estos momentos, dado que aún pasará un tiempo hasta que los nuevos drivers sean estables y puedan integrarse para poder jugar con un rendimiento a la par del driver privativo actual, pero es de esperar que con el impulso que se está dando con el videojuego en Linux ya sea en la nube o de forma nativa/Wine/Proton es cuestión de tiempo el que podamos utilizar al fin drivers gráficos libres con las tarjetas Nvidia para poder ejecutar los juegos. Por otra parte, este movimiento no deja de ser un cambio sustancial que supondrá un gran impulso para drivers como Noveau que ahora podrá acceder a la tecnología gráfica de Nvidia cosa que antes solo podían realizar mediante ingeniería inversa.


Los nuevos drivers son lanzados en base a una nueva serie de numeración comenzando por los actuales R515, que ya están disponibles en su [repositorio de github](https://github.com/NVIDIA/open-gpu-kernel-modules).


Puedes ver el anuncio oficial [en este enlace](https://developer.nvidia.com/blog/nvidia-releases-open-source-gpu-kernel-modules/), y si quieres profundizar en lo que supone este movimiento por parte de Nvidia te dejamos unos enlaces (en inglés) para que veas el terremoto que acaba de suceder:


[www.phoronix.com](https://www.phoronix.com/scan.php?page=article&item=nvidia-open-kernel&num=1)


[blogs.gnome.org](https://blogs.gnome.org/uraeus/2022/05/11/why-is-the-open-source-driver-release-from-nvidia-so-important-for-linux/)


[www.gamingonlinux.com](https://www.gamingonlinux.com)


En otro orden de cosas, y continuando con Nvidia y sus tecnologías gráficas, nos enteramos vía [gamingonlinux.com](https://www.gamingonlinux.com/2022/05/nvidia-releases-open-source-linux-gpu-kernel-modules-beta-driver-5154304-out/) que en el propio lanzamiento de sus drivers **515.43.04** ya se han registrado cambios que implicarían el comienzo para soportar [Gamescope](https://github.com/Plagman/gamescope), el micro-compositor que utiliza Valve para Linux y que además también están realizando [solicitudes](https://github.com/Plagman/gamescope/pull/488) en los repositorios para llevar **NVIDIA image scaling a Proton**.


¿Qué te parece la apertura del kernel GPU de Nvidia? ¿cambia esto tu percepción de la compañía respecto a Linux? ¿Supondrá esto un cambio de la comunidad Linux respecto a comprar y utilizar las gráficas de Nvidia? ¿Con la que está cayendo, alguien ha pensado en enviar mantas y ropa de abrigo al infierno?


Cuéntamelo en los comentarios, o en los canales de jugandoenlinux de [Telegram](https://twitter.com/JugandoenLinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

