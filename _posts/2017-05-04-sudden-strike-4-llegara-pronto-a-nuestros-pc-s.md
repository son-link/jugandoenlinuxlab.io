---
author: leillo1975
category: Estrategia
date: 2017-05-04 14:21:00
excerpt: <p>Kalypso Media anuncia la fecha de salida del juego</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/1519e954ae6bd629544356cae3e51766.webp
joomla_id: 306
joomla_url: sudden-strike-4-llegara-pronto-a-nuestros-pc-s
layout: post
tags:
- rts
- sudden-strike-4
- kalypso-media
- segunda-guerra-mundial
- kite-games
title: "Sudden Strike 4 llegar\xE1 pronto a nuestros PC's"
---
Kalypso Media anuncia la fecha de salida del juego

Hace 3 meses os anunciabamos en nuestra sección "[APT-GET UPDATE](index.php/homepage/apt-get-update/item/337-apt-get-update-bleed-2-quetoo-killing-floor-2-cossacks-3-sudden-strike-4)", que la cuarta entrega de esta franquicia de juegos sería desarrollada también para nuestro sistema. A través de un [anuncio en Steam](http://steamcommunity.com/games/373930/announcements/detail/1305326168700833636), la distribuidora [**Kalypso Medía**](http://www.kalypsomedia.com/en/games/suddenstrike4/index.shtml), acaba de anunciar la fecha oficial de salida de este título para concretamente el 11 de Agosto del presente año. En esta fecha verá la luz, si todo discurre como está estipulado, este trabajo desarrollado por el estudio húngaro **[Kite Games](http://kite-games.com/)**.


 


Para quien no conozca esta veterana saga de juegos, hay que decir que pertenecen al género de la estrategia en tiempo real, y que están ambientados en la segunda guerra mundial. El primer juego vió la luz hace la friolera de más de 20 años. En esta ocasión podremos comanadar durante 3 campañas diferenciadas (con unas 20 misiones) a los ejercitos Inglés y Americano, Soviético, así como también al Alemán; pudiendo usar sobre 100 tipos de unidades diferentes, así como escoger personajes tales como el General Patton, entre otros, que nos ofrecerán habilidades diferenciadas. Durante la partida también tendremos la oportunidad de parar el tiempo para establecer órdenes. Por supuesto también podremos medirnos con otros jugadores online, y hacer uso de modificaciones gracias al uso de Steam Workshop.


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/l2AW5W_a63w" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


En este momento puedes [pre-comprar en Steam Sudden Strike 4](http://store.steampowered.com/app/373930/Sudden_Strike_4/) con un 15% de descuento y pudiendote llevar un mapa exclusivo, la banda sonora y una copia digital de el libro "El arte de la guerra" donde podremos disfrutar de las ilustraciones y bocetos del juego. Desde JugandoEnLinux.com os informaremos cumplidamente de la salida de este juego, así de todas las noticias reseñables sobre este juego. Si quereis dejar vuestras impresiones sobre este lanzamiento o la saga Sudden Strike, podeis dejar un comentario en este artículo o usar nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Discord.](https://discord.gg/ftcmBjD)


 

