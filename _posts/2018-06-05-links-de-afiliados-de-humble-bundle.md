---
author: Serjor
category: Editorial
date: 2018-06-05 21:22:42
excerpt: "<p>Un peque\xF1o gesto, una gran ayuda</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/e914558b2d1ba8901152392da365c367.webp
joomla_id: 756
joomla_url: links-de-afiliados-de-humble-bundle
layout: post
tags:
- humble-bundle
- humble-store
- partners
title: Links de afiliados de Humble Bundle
---
Un pequeño gesto, una gran ayuda

Cómo hace poco os [anunciamos](index.php/homepage/editorial/item/865-jugando-en-linux-cambia-para-poder-seguir-adelante), hemos retirado los anuncios de la web. Entre que no nos gusta que anden jugando con la privacidad de nuestros lectores y no nos aportaban realmente nada a nivel económico, hasta ahora solamente teníamos [PayPal](https://www.paypal.com/donate/?token=TIBTLiNC707B9WkV_r7XSCqoZht0ZvwfTuI1Ut9hod1B56F3-lJ4jKH_YjgQ0TqrpTWcIG&country.x=ES&locale.x=ES) como opción de financiación externa. Y decimos hasta ahora porque desde hoy tenemos disponibles **los enlaces de afiliados para Humble Bundle**. Como ya sabréis si hacéis uso de estos enlaces a la hora de compraros un juego en Humble Bundle a vosotros no os costará ni un céntimo más, pero estaréis ayudando de manera indirecta a mantener los costes que genera la web.


Así que si estabas dudando en hacerte con el [Bundle de Daedalic](https://www.humblebundle.com/games/daedalic-2018-bundle?partner=jugandoenlinux), la [suscripción mensual](https://www.humblebundle.com/monthly?partner=jugandoenlinux) o [algún juego suelto](https://www.humblebundle.com/store?partner=jugandoenlinux), hazlo a través de los enlaces que os hemos puesto si piensas que este es un proyecto que merece la pena.


Esta no será la única novedad que tenemos preparada respecto a nuestros planes de ayuda. Pronto daremos más novedades en otras plataformas e iremos añadiendo enlaces de afiliación en los artículos que vayamos publicando.


¡Muchas gracias por apoyarnos!

