---
author: Pato
category: Estrategia
date: 2018-02-02 18:45:11
excerpt: "<p>El juego est\xE1 recibiendo cr\xEDticas positivas seg\xFAn reconoce el\
  \ estudio responsable del juego</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/72ca15047a78e7b4a322422f38badf4b.webp
joomla_id: 635
joomla_url: railway-empire-anuncia-mejoras-y-un-mapa-completo-de-norte-america
layout: post
tags:
- estrategia
- simulacion
- gestion
title: '''Railway Empire'' anuncia mejoras y un mapa completo de Norte America'
---
El juego está recibiendo críticas positivas según reconoce el estudio responsable del juego

Buenas noticias para los poseedores de 'Railway Empire' [[steam](http://store.steampowered.com/app/503940/Railway_Empire/)], del cuyo lanzamiento nos [hicimos eco](index.php/homepage/generos/estrategia/item/749-railway-empire-ya-esta-disponible-en-linux-steamos) tan solo hace 3 días. Hace unas horas han anunciado que ya están trabajando en la mejora del juego para un futuro próximo en el que quieren seguir solucionando bugs, e introducir nuevo contenido.


También han anunciado que expandirán el mapa del juego para cubrir todo el territorio de Norte América de costa a costa introduciendo de paso las mejoras tecnológicas que surgieron durante los 100 años de la revolución de los trenes de vapor.


Por último, han anunciado que próximamente habrá una hoja de ruta con todas las próximas novedades que llegarán gratuitamente al juego, incluyendo nuevo contenido, mejoras en la IA, y otras mejoras sugeridas directamente por los jugadores.


Tienes todos los detalles en el [anuncio](http://steamcommunity.com/games/503940/announcements/detail/1657757502225302305) publicado en Steam.


¿Qué te parece la propuesta de 'Railway Empire'? ¿Crearás tu imperio del ferrocarril?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 

