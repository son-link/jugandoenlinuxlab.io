---
author: leillo1975
category: Plataformas
date: 2022-01-12 19:52:57
excerpt: "<p>&nbsp;Este plataformas cl\xE1sico #OpenSource adem\xE1s se ha actualizado\
  \ recientemente.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SuperTux/SuperTux01.webp
joomla_id: 1409
joomla_url: supertux-ya-esta-en-steam-en-acceso-anticipado
layout: post
tags:
- plataformas
- acceso-anticipado
- open-source
- supertux
title: "SuperTux ya est\xE1 en Steam en Acceso Anticipado"
---
 Este plataformas clásico #OpenSource además se ha actualizado recientemente.


Como sabeis, SuperTux es uno de los juegos con más solera (y éxito) del panorama de los juegos en GNU/Linux . Este veterano del software libre que **lleva dando guerra desde 2003**, acabanos acaba de regalar esta navidades la **versión 0.6.3**, tal y como podeis ver en este tweet:



> 
> SuperTux v0.6.3 is out now! Download it from <https://t.co/NII9wRLNgc> and find out what's new or (NEW!!!) play it online here: <https://t.co/Ws4SSkUstj>
> 
> 
> — SuperTux Team (@supertux_team) [December 22, 2021](https://twitter.com/supertux_team/status/1473804983686553602?ref_src=twsrc%5Etfw)



En ella, como podeis ver en su [blog](https://www.supertux.org/index.html), hay montones de novedades importantes, entre las que destacan las siguientes:


* ¡Recopilación de wasm! Los nightlies de SuperTux ahora **[se pueden jugar directamente en el navegador](https://supertux.semphris.com/play/)** (gracias a Semphris).
* Natación añadida (gracias a Daniel y Zwatotem)
* Añadido walljumping (gracias a Daniel)
* Autotiles (gracias a Semphris)
* Animaciones actualizadas (gracias a Alzter, Daniel y RustyBox)
* Se actualizaron muchos mapas de mundos de contribuciones (gracias a Servalot)
* Muchas actualizaciones a las rutas, con suavizado, curvas Bezier, uso compartido de rutas entre objetos y más (gracias a Semphris)
* Se rehizo el mosaico de cristal (gracias a Alzter y RustyBox)
* Muchos mosaicos de nieve nuevos (gracias a Daniel)
* Muchos objetos nuevos, como el parachoques lateral y los bloques que caen (gracias a Daniel)
* Partículas personalizadas (gracias a Semphris)
* Un nuevo objeto rublight (gracias a HybridDog)
* Binarios oficiales para FreeBSD, Linux de 32 bits y Ubuntu Touch (gracias a Semphris)
* Se agregaron estadísticas de progreso en el juego (gracias a Semphris)
* Nuevo selector de color basado en OKLab (gracias a HybridDog)
* Creador de complementos, para crear fácilmente paquetes de complementos con tu mundo (gracias a Semphris)
* Reelaboración del mapa mundial Revenge in Redmond (gracias a RustyBox)
* Se agregó un ambiente de cambio de tiempo en el mapa mundial (gracias a Semphris)
* Escenas saltables (gracias a Semphris)
* El editor se guarda automáticamente a intervalos regulares (gracias a Semphris)
* Integración opcional con Discord (gracias a Semphris)
* Traducciones actualizadas, por supuesto (gracias a los traductores)


Como veis, en un año y medio después de la anterior versión, los cambios son más que notables. Incluso se podría esperar una ver una versión 0.7, pero el equipo de desarrollo quiere para este futuro lanzamiento unos cambios más notables en el modo historia y finalizar algunos mundos.


Pero esto ya es pasado, y os pedimos perdón por no haber informando en su día. Hoy **la noticia que os traemos es la disponibilidad del juego en Steam**, tal y como el equipo de desarrollo había anunciado recientemente. A pesar de estar en **Acceso Anticipado**, por lo poco que hemos podido probar no hay aparentes problemas y funciona correctamente tanto con teclado como con Steam Controller. Con este movimiento, que estamos viendo ultimamente en muchos juegos libres, se busca llegar a más gente, además de dar una nueva forma de instalación a los usuarios a parte de las [habituales](https://www.supertux.org/download.html).


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/1572920/" width="646"></iframe></div>


 Pues nada, ya no teneis excusa para disfrutar de este magnífico plataformas. Si quereis contarnos que os parece este juego podeis hacerlo en nuestro grupo de JugandoEnLinux en [Telegram](https://t.me/jugandoenlinux), o nuestras salas de [Matrix](https://matrix.to/#/#jugandoenlinux.com:matrix.org). Os dejamos con un gameplay que hemos encontrado en Youtube de esta versión:  
  



<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/eIciR9y6-Jc" title="YouTube video player" width="780"></iframe></div>

