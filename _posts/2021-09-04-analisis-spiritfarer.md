---
author: P_Vader
category: "An\xE1lisis"
date: 2021-09-04 18:39:23
excerpt: "<p>Analizamos #Spiritfarer de @ThunderLotus aprovechando que se actualiza\
  \ tras revelarse como una joya de videojuego.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Spiritfarer/spiiritfarer_portada.webp
joomla_id: 1352
joomla_url: analisis-spiritfarer
layout: post
tags:
- indie
- plataformas
- exploracion
- analisis
- construccion
- dibujo-a-mano
title: "An\xE1lisis: Spiritfarer"
---
Analizamos #Spiritfarer de @ThunderLotus aprovechando que se actualiza tras revelarse como una joya de videojuego.


No voy a mentir que hace mas de un año probé la demo de este juego atraído unicamente por sus gráficos a mano. **Inspirados claramente en el [Studio Ghibli](https://www.ghibli.jp/), sobre todo en el famoso ["El viaje de Chihiro"](https://www.imdb.com/title/tt0245429/)** como han [reconocido](https://www.youtube.com/watch?v=qxx8sEGjntI) sus desarrolladores, ademas la historia de su protagonista tiene cierto paralelismo con el reconocido anime, con esos mundos simbólicos de espíritus y almas. Aunque ha pasado un año desde su estreno y que la curiosidad me topase con este juego, aprovechando su última gran actualización vamos a hablar un poco mas de este juego, que bien se lo merece.


![Spiritfarer SCREENSHOTS 07](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Spiritfarer/Spiritfarer-SCREENSHOTS-07.webp)


No nos puede sorprender la calidad de este titulo ya que **[Thunderlotus](https://thunderlotusgames.com) son los también creadores de [Jotun](https://store.steampowered.com/app/323580/Jotun_Valhalla_Edition) o [Sundered](https://store.steampowered.com/app/535480/Sundered_Eldritch_Edition/) nada menos.** Juegos indies de gran calidad. Ya en **los primeros 6 meses Spiritfarer llegó a las 500.000 copias, signo del éxito para un titulo tan particular**.


 


**La historia trata sobre el fallecimiento, aunque parezca un tanto dura, es tratada de manera entrañable y con mucho cariño**. Stella la protagonista junto a su adorable gato Daffodil, deben de hacer lo posible para que los espíritus se vayan en paz. **Con esta premisa Stella se encuentra que un día se convierte en la guiá de almas, encargada del tránsito de las almas en su barco**. Para ello debe manejar y gestionar las necesidades particulares de cada personaje que la irá acompañando en su travesía hasta que cada uno finalice ese último viaje.


En esta ocasión los desarrolladores han escogido el mito de [Caronte](https://es.wikipedia.org/wiki/Caronte_(mitolog%C3%ADa)) de la mitología griega para reinterpretarlo de esta manera tan particular. Repito, quizás el argumento **te pueda resultar un poco tétrico, pero la sutil manera de tratar todo con tanto respeto hace la historia sea una auténtica maravilla**.


 


**El juego en si es una mezcla de gestión, exploración y construcción, con plataformas, y con unas mecánicas muy adictivas**. Por compararlo podría ser el Stardew Valley en vista lateral. Nuestros protagonistas no dejan de correr, saltar, abrazar, cultivar, tocar la guitarra, pescar y un largo etc. Y es que una vez que te adentras en el universo de **Spiritfarer te dejas llevar y las horas se pasan relajadamente volando**. Tiene el añadido que **puedes jugar en modo cooperativo local metiéndote en la piel de su gato**.


![Spiritfarer SCREENSHOTS 03](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Spiritfarer/Spiritfarer-SCREENSHOTS-03.webp)


**La calidad gráfica y de sus animaciones, tan sumamente detalladas, son de tal nivel que a veces cuesta creer que estamos ante un juego indie** y no ante una gran producción anime. Han llegado a un punto muy alto.


**El apartado sonoro y ambiental rozan la perfección, que ya quisieran algunos grandes títulos**, en paralelo a esta última actualización han lanzado una **versión física del juego para coleccionista con su correspondiente vinilo de la banda sonora**. Un lujo que hará las delicias de los mas fans.


La versión para Linux, desde que probé su demo antes de su lanzamiento hasta que lo compré completo, siempre me ha funcionado bien. **Como es costumbre con  Thunderlotus tenemos una versión nativa de calidad** y eso es de agradecer en los tiempos de Proton.


 


![Spiritfarer Beverly Update key art 1920x1080 EN](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Spiritfarer/Spiritfarer_Beverly_Update_key_art_1920x1080_EN.webp)


**Respecto a la actualización que nos ocupa llega con el titulo de Beverly** nombre de la pequeña búho que han añadido como nuevo personaje con su respectiva historia y que fue lanzada hace escasos días. Otros añadidos y cambios han sido:


* **Nuevo sistema de guardado**: ahora puede tener tres ranuras de guardado separados.
* **Nuevas r****evistas literarias de saqueadores**: Revistas especiales que contienen una serie de misiones con mapas del tesoro.
* **Habitación de la radio**: Pasa el rato junto al bar y escucha cualquiera de las canciones de la banda sonora  has escuchado antes.
* **Nuevas comidas picantes**.
* **Cola de destino**: seleccione hasta tres puntos para explorar en un solo viaje.
* **Nuevo modo "Viaje seguro"** para viajar, que omite eventos automáticamente durante su próximo viaje.
* Ahora puede deshabilitar (o volver a habilitar) la música de la estación de autobuses.
* Sentarse en cualquier silla por la noche ahora habilita el **modo de observación de estrellas**, que oculta los edificios y muestra el cielo nocturno estrellado. (Modo relax total)


 


Para mi gusto y creo que la tónica general de la crítica ha sido esa, **estamos ante lo mejor que se dejó ver en 2020 sin duda.**


 Aquí podéis ver la partida que jugamos en directo en modo cooperativo para enseñaros el juego:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/cKtonim3AjE" title="YouTube video player" width="780"></iframe></div>


 


Puedes hacerte con él en [Humble Bundle](https://www.humblebundle.com/store/spiritfarer?partner=jugandoenlinux) (enlace de afiliado) o en Steam:


 


<div class="steam-iframe"><iframe height="200" src="https://store.steampowered.com/widget/972660/" style="border: 0px;" width="650"></iframe></div>


 


¿Qué te parece Spiritfarer? ¿Lo has probado? Cuéntanos como siempre tu opinión sobre este título tan particular en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org) .


 


 

