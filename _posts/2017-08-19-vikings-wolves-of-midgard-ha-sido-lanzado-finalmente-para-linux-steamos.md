---
author: leillo1975
category: Rol
date: 2017-08-19 18:47:00
excerpt: <p>Kalypso Media cumple con lo prometido y nos trae este juego de Rol</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/4feb08b32fc2bca59f9db42b69cd4022.webp
joomla_id: 439
joomla_url: vikings-wolves-of-midgard-ha-sido-lanzado-finalmente-para-linux-steamos
layout: post
tags:
- rpg
- kalypso-media
- vikings-wolves-of-midgard
- games-farm
title: Vikings - Wolves of Midgard ha sido lanzado finalmente para Linux/SteamOS.
---
Kalypso Media cumple con lo prometido y nos trae este juego de Rol

Con un ligero retraso con respecto a la versión de Windows del juego nos llega este nuevo título de Rol enfocado hacia la acción de la mano de la editora **[Kalypso Media](http://www.kalypsomedia.com/en/games/vikings/index.shtml)** (Tropico 5, [Sudden Strike 4](index.php/homepage/generos/estrategia/item/550-sudden-strike-4-disponible-en-gnu-linux-steamos), Jagged Alliance) y el estudio **[Games Farm](http://www.games-farm.com/)**. Al más puro estilo Diablo, en **Vikings - Wolves of Midgard**, nos enfrentaremos a golpe de ratón a innumerables hordas de enemigos, donde podremos usar nuestras armas, así como habilidades pasivas y activas, y por supuesto la magia, Podremos utilizar diferentes tipos de personajes separados por clases donde encontraremos guerreros, magos, arqueros...


 


Esta es la descripción oficial del juego según lo que encontramos en Steam:



> 
> *Teme a los Lobos. Tienen frío, tienen hambre y harán cualquier cosa por sobrevivir...*
> 
> 
> *Ragnarök. Cuenta la leyenda que cuando soplen los más gélidos vientos de invierno, los jotun regresarán para vengarse de los dioses de Asgard. El mundo se dirige a la perdición y el destino de Midgard pende de un hilo. Pero cuando los gigantes de fuego y hielo empiecen a unir sus ejércitos, se encontrarán con el clan Ulfung, los Lobos de Midgard. Esta sanguinaria banda de vikingos condenados al ostracismo no se doblegó tras la destrucción de su aldea, y están decididos a cumplir su destino y salvar el mundo. Como jefe del clan, tu deber es evitar la destrucción absoluta de Midgard y liderar la lucha contra las diabólicas criaturas del Fimbulvetr.*
> 
> 
> *La fantasía se une a la mitología nórdica*
> -------------------------------------------
> 
> 
> *Viaja por los reinos telúricos de Midgard, por el glacial mundo de Niflheim y por el abrasador Balheim, ya sea como un fiero guerrero vikingo o como una despiadada doncella escudera.*  
>   
> 
> 
> 
> *El mortal Fimbulvetr*
> ----------------------
> 
> 
> *Resiste el frío glacial y protégete de la ira de la naturaleza para sobrevivir.*  
>   
> 
> 
> 
> *Rinde tributo a los dioses*
> ----------------------------
> 
> 
> *Recoge sangre de tus enemigos caídos y sacrifícala en el altar para recibir obsequios de los dioses y mejorar tus poderes.*   
>   
> 
> 
> 
> *Furia berserker*
> -----------------
> 
> 
> *Déjate llevar por la ira activando el modo Furia para aplastar a tus enemigos en un sangriento combate.*  
>   
> 
> 
> 
> *El arte de la batalla*
> -----------------------
> 
> 
> *Perfecciona tus habilidades con cada arma para desatar su auténtico poder, incluidos arcos, espadas y báculos encantados.*  
>   
> 
> 
> 
> *El poder de los dioses*
> ------------------------
> 
> 
> *Aprópiate de los poderes divinos equipándote con amuletos de poder, como los talismanes de Thor o Loki. Pon a prueba tus nuevas habilidades compitiendo en las Pruebas de los dioses para conseguir más recompensas.*   
>   
> 
> 
> 
> *Trabajo en equipo*
> -------------------
> 
> 
> *Forma equipo con un amigo y aventuraos juntos en las Costas de Midgard en el épico modo cooperativo en línea para dos jugadores. Elegid entre cuatro niveles de dificultad el que más se ajuste a vuestras habilidades y talento. ¿Os atreveréis con el implacable modo Experto?*
> 
> 
> 


 


Desde JugandoEnLinux.com nos gustaría poder ofrecerte un completo análisis de este título, así como videos del juego en acción, pero aun no disponemos de una copia para su evaluación . En caso que podamos hacernos con una podremos ofreceros estos contenidos lo antes posible.


 


Os dejamos con el fantástico trailer del juego:


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/X7dMUXbpuzs" width="800"></iframe></div>


 


 


Puedes comprar Vikings - Wolves of Midgard en Steam:


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/404590/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


¿Qué os parece este título de Kalypso Media? ¿Os van este tipo de juegos? Dinos lo que piensas en los comentarios o en nuestro canal de [Telegram](https://t.me/jugandoenlinux).

