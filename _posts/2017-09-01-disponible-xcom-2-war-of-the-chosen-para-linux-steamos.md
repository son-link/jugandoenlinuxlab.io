---
author: leillo1975
category: Estrategia
date: 2017-09-01 07:20:24
excerpt: "<p>Feral nos trae esta expansi\xF3n del t\xEDtulo de Firaxis.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/6cd7000b5fdc149d15c15e2b49b0ac68.webp
joomla_id: 449
joomla_url: disponible-xcom-2-war-of-the-chosen-para-linux-steamos
layout: post
tags:
- feral-interactive
- dlc
- firaxis
- xcom-2
- war-of-the-chosen
- expansion
- 2k
title: 'Disponible XCOM 2: War of the Chosen para Linux/SteamOS.'
---
Feral nos trae esta expansión del título de Firaxis.

A mediados del mes de junio, [Feral Interactive](http://www.feralinteractive.com/es/) nos anunciaba que se estaba encargando de portar esta nueva expansión para XCOM 2, y que esta se produciría poco después de la salida para Windows (29 de Agosto), pero sin aclarar una fecha concreta. Hace 3 días, coincidiendo con el lanzamiento para este último [nos encomendaron para el mes de Septiembre](index.php/homepage/generos/estrategia/item/493-feral-nos-traera-xcom-2-war-of-the-chosen) sin aclarar ninguna fecha. En el día de ayer, a última hora de la tarde, y de manera sorpresiva, Feral nos informaba por mail y mediante Twitter que la  ya estaba disponible la expansión, llegando justo un día antes de que empezase el mes señalado.


 



> 
> [#XCOM2](https://twitter.com/hashtag/XCOM2?src=hash): War of the Chosen deploys onto macOS and Linux.  
> Buy this epic-scale expansion from the Feral Store – <https://t.co/vv097ofpND> [pic.twitter.com/YR9pGXoXOm](https://t.co/YR9pGXoXOm)
> 
> 
> — Feral Interactive (@feralgames) [August 31, 2017](https://twitter.com/feralgames/status/903303083110879233)



 


 Por si queda alguien que aun no conozca la saga XCOM, se trata de una serie de juegos de estrategia por turnos en los que deberemos comandar a la resistencia contra una invasión estraterrestre. Para ello tendremos que gestionar recursos, investigar, desarrollar armas y tecnologías, mejorar soldados... y por supuesto dirigirlos en el campo de batalla. Unos juegos altamente recomendables si sois aficionados de los juegos de estrategia por turnos. En esta ocasión, tal y como ocurrió con la **primera parte del juego** y  **Enemy Within**, [Firaxis](http://www.firaxis.com/) y [2K Games](https://www.2k.com/) nos proporciona con esta **expansión independiente** multitud de cambios y añadidos que amplian enormemente la experiencia del juego. Esta es la **descripción del juego** que nos proporciona Feral Interactive:


 






> 
> ***XCOM® 2: War of the Chosen** es la expansión del título galardonado como juego de estrategia del año en 2016.*
> 
> 
> *War of the Chosen incorpora extensos contenidos nuevos a la lucha contra ADVENT cuando se forman más facciones de la resistencia para acabar con la amenaza alienígena sobre la Tierra. Como respuesta, surge un nuevo enemigo, los "Elegidos", cuyo único objetivo es volver a capturar al comandante. La expansión incluye nuevos enemigos, misiones, entornos, clases de héroes para enfrentarse a los "Elegidos" y una estrategia de juego más completa.*
> 
> 
> ***NUEVAS FACCIONES Y CLASES DE HÉROES***
> 
> 
> *Surgieron tres nuevas facciones para reforzar la resistencia de la Tierra: Segadores, Guerrilleros y Templarios, cada una con habilidades diferentes y filosofías opuestas. Estas facciones proporcionan soldados heroicos poderosos para ayudar en las misiones y ofrecen más oportunidades en el plano de la estrategia.*
> 
> 
> ***LOS ELEGIDOS***
> 
> 
> *Los Elegidos son los enemigos más astutos a los que jamás se haya enfrentado XCOM y cada uno tiene sus propios puntos fuertes y débiles, que se verán en cada nueva campaña. Los Elegidos van a la caza del comandante y secuestrarán, interrogarán y matarán a los soldados de XCOM para conseguir su objetivo. Los Elegidos también pueden irrumpir en el plano estratégico y desbaratar las operaciones globales de XCOM. Encuentra y asalta las fortalezas de los Elegidos para derrotar al enemigo de una vez por todas.*
> 
> 
> ***NUEVAS AMENAZAS ALIENÍGENAS Y DE ADVENT***
> 
> 
> *Un alienígena nuevo y mortal –conocido como Espectro y que es capaz de crear dobles oscuros de los soldados de XCOM– se ha infiltrado en el campo de batalla. Adopta nuevas tácticas para contrarrestarlo, además de los ataques explosivos del Purificador de ADVENT y del Sacerdote de ADVENT, que rebosa poder psíquico.*
> 
> 
> ***NUEVOS ENTORNOS Y OBJETIVOS DE MISIONES***
> 
> 
> *Lleva a cabo misiones tácticas en nuevos entornos; desde ciudades abandonadas y asoladas por armas biológicas alienígenas durante la invasión original hasta túneles subterráneos y apartadas regiones xenoformas.*
> 
> 
> ***MEJORAS EN EL PLANO ESTRATÉGICO***
> 
> 
> *Gestiona las relaciones de XCOM con las facciones y contrarresta las operaciones de los Elegidos desde el Avenger. Utiliza las nuevas órdenes de la resistencia para dar prioridad a tu estrategia personal. Ahora puedes desplegar soldados, científicos e ingenieros en operaciones encubiertas que conceden suministros y aumentan la simpatía de las facciones al completarlas.*
> 
> 
> ***MAYOR PERSONALIZACIÓN Y REJUGABILIDAD***
> 
> 
> *Los soldados pueden desarrollar vínculos con los compañeros compatibles y lograr así nuevas habilidades y mejoras. El sistema de los informes de situación añade nuevos modificadores al plano táctico para cerciorarse de que cada misión ofrezca un reto único. Las opciones avanzadas de campaña permiten ajustar con mayor precisión la duración y la dificultad del juego.*
> 
> 
> ***MODO RETO***
> 
> 
> *Sigue la estrategia perfecta en los nuevos retos de la comunidad para conseguir el primer puesto en el marcador global. ¡Solo tendrás una oportunidad!*
> 
> 
> ***COMPARTE LA RESISTENCIA***
> 
> 
> *Personaliza a tus soldados y hazles posar y luego añade filtros, textos y fondos para crear tus propios carteles de la resistencia para que aparezcan en el juego y compartirlos con tus amigos.*
> 
> 
> 





 


Desde JugandoEnLinux.com nos gustaría informarte que estamos preparando algún gameplay para [**nuestro canal de Youtube**](https://www.youtube.com/channel/UC4FQomVeKlE-KEd3Wh2B3Xw), y después un **completo análisis** de tan suculenta expansión. Sin más os dejo con el trailer de Feral anunciando la salida del juego:


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/M6tNYBFOtTA" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


Puedes comprar XCOM 2: War of the Chosen en la **[tienda de Feral (recomendado)](https://store.feralinteractive.com/en/mac-linux-games/xcom2warofthechosen/)** o en Steam:


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/593380/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


¿Te gusta la saga XCOM? ¿Tienes XCOM 2 y adquirirás esta expansión? Deja tus impresiones en los comentarios o en nuestro canal de [Telegram](https://t.me/jugandoenlinux).

