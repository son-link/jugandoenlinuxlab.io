---
author: Serjor
category: "Acci\xF3n"
date: 2017-03-31 14:40:07
excerpt: <p>Esto supone el inicio de la primera temporada del juego, First Blood</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/34570ba96cc3a4daee50221a47a4e2ec.webp
joomla_id: 272
joomla_url: ballistic-overkill-abandona-la-fase-de-acceso-anticipado
layout: post
tags:
- fps
- ballistic-overkill
title: Ballistic Overkill abandona la fase de acceso anticipado
---
Esto supone el inicio de la primera temporada del juego, First Blood

Gracias a [Boilling Steam](http://boilingsteam.com/ballistic-overkill-a-very-polished-fps/) nos enteramos de que la gente de [Aquiris Game Studio](http://www.aquiris.com.br/?lang=es_ES) anuncian que el [juego abandona la fase de acceso anticipado](http://steamcommunity.com/games/ballistic/announcements/detail/468778664304068015) con la llegada de la versión 1.3.5


Para aquellos que no hayáis tenido la oportunidad de acercaros a este juego, diremos que Ballistic Overkill es un FPS competitivo, donde dos equipos se enfrentan entre sí en diferentes modalidades de juego. Quizás las referencias más cercanas y conocidas sean los populares Team Fortress 2 y Overwatch, ya que en BA contamos con diferentes clases con las que jugar, las cuales pueden ser modificadas a través de perks, y con diversas modalidades de juego.


 


![Podemos modificar ligeramente ciertos aspectos de la cada clase](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/BallisticOverkill/ba4.webp)


 


Por suerte he tenido la oportunidad de jugar a Ballistic Overkill durante su fase de acceso anticipado, y la verdad es que es un juego muy sólido. Está desarrollado con Unity3D, y aunque en el pasado Unity tenía graves problemas con el control en Linux, al menos yo no puedo quejarme, y las mecánicas de juego permiten unas partidas donde el juego es muy rápido, pero con margen como para que no de esa sensación de no "sé qué está pasando".


 


![A la vista está que no soy muy bueno...](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/BallisticOverkill/ba2.webp)


Por otro lado, esta actualización trae consigo la inclusión de un modelo femenino para la clase Shadow, y el comienzo de las temporadas, donde en cada temporada podremos hacernos con objetos exclusivos de cada temporada.


![La primera temporada, primera sangre, da el pistoletazo de salida a la obtención de diversos objetos](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/BallisticOverkill/ba5.webp)


 Si estáis interesados en este juego, como sugerencia, al buscar un servidor, podéis usar los servidores de [GamingOnLinux](http://www.gamingonlinux.com), y así jugar con otros miembros de la comunidad Linuxera.


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" frameborder="0" height="315" src="https://www.youtube.com/embed/uq79CxIZmYs" width="560"></iframe></div>


 


<div class="steam-iframe"><iframe frameborder="0" height="190" src="https://store.steampowered.com/widget/296300/" width="646"></iframe></div>


 


¿Habéis probado ya Ballistic Overkill? ¿Qué os parece? Déjanos tu impresión en los comentarios, o pásate por los grupos de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD) para que sepamos qué es lo que te parece.

