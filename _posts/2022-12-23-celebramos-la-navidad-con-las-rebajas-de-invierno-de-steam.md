---
author: Odin
category: Noticia
date: 2022-12-23 11:02:23
excerpt: "<p>Ayer se publicaron las populares rebajas de invierno de Steam y vienen\
  \ con ofertas muy interesantes.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/rebajas_invierno_2022/rebajasInviernoSteam.webp
joomla_id: 1517
joomla_url: celebramos-la-navidad-con-las-rebajas-de-invierno-de-steam
layout: post
tags:
- steam
- rebajas
title: Celebramos la Navidad con las Rebajas de Invierno de Steam
---
Ayer se publicaron las populares rebajas de invierno de Steam y vienen con ofertas muy interesantes.


Año tras año, las rebajas de invierno de **Steam** nos ofrecen descuentos sobre una gran cantidad de su catalogo de juegos. Ya sea porque quieras adquirir ese videojuego que te falta en tu colección o porque hay pocas cosas mejores que pasar el frio del invierno con tu juego favorito, te proponemos un listado de ofertas que te pueden resultar de interés:


**Nativos**:


* [Children of Morta](https://store.steampowered.com/app/330020/Children_of_Morta/) **70%** descuento
* [Skullgirls 2nd Encore](https://store.steampowered.com/app/245170/Skullgirls_2nd_Encore/) **90%** descuento
* [Mindustry](https://store.steampowered.com/app/1127400/Mindustry/) **45%** descuento
* [Spiritfarer Edición Farewell](https://store.steampowered.com/app/972660/Spiritfarer_Edicin_Farewell/) **75%** descuento
* [Metro Exodus](https://store.steampowered.com/app/412020/Metro_Exodus/) **75%** descuento
* [Wonder Boy The Dragons Trap](https://store.steampowered.com/app/543260/Wonder_Boy_The_Dragons_Trap/) **60%** descuento


**Verificados** para **Steam Deck** con **Proton**:


* [God of War](https://store.steampowered.com/app/1593500/God_of_War/) **40%** descuento
* [Horizon Zero Dawn Complete Edition](https://store.steampowered.com/app/1151640/Horizon_Zero_Dawn_Complete_Edition/) **67%** descuento
* [Uncharted](https://store.steampowered.com/app/1659420/UNCHARTED_Coleccin_Legado_de_los_Ladrones/) **30%** descuento
* [Hades](https://store.steampowered.com/app/1145360/Hades/) **50%** descuento
* [Ace Combat 7 Skies Unknown](https://store.steampowered.com/app/502500/ACE_COMBAT_7_SKIES_UNKNOWN/) **84%** descuento
* [Ni no Kuni Wrath of the White Witch Remastered](https://store.steampowered.com/app/798460/Ni_no_Kuni_Wrath_of_the_White_Witch_Remastered/) **80%** descuento


 ¿Qué te parecen [las rebajas de invierno de Steam](https://store.steampowered.com/)? Cuéntanos que juegos te vas a comprar durante estas rebajas en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

