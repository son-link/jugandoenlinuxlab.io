---
author: Pato
category: "Acci\xF3n"
date: 2018-06-26 21:57:50
excerpt: "<p>@TheGameKitchen conmemora tambi\xE9n el aniversario de su exitoso kickstarter\
  \ con un v\xEDdeo</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/3bd383cdf9446912a35458166e99234d.webp
joomla_id: 786
joomla_url: ya-disponible-el-tercer-prototipo-narrativo-de-blasphemous-para-los-backers
layout: post
tags:
- accion
- indie
- plataformas
- proximamente
title: Ya disponible el tercer prototipo "narrativo" de 'Blasphemous' para los backers
---
@TheGameKitchen conmemora también el aniversario de su exitoso kickstarter con un vídeo

Aún no ha salido y el nuevo desarrollo del estudio sevillano "The Game Kitchen" ya comienza a recibir elogios por parte de sus seguidores ya que acaba de publicar una tercera versión del "prototipo narrativo" de 'Blasphemous'. De hecho, hace ya un año que llevaron a cabo su exitosa campaña en [Kickstarter](https://www.kickstarter.com/projects/828401966/blasphemous-dark-and-brutal-2d-non-linear-platform?ref=nav_search&result=project&term=blasphemous) donde consiguieron recaudar mas de 330.000$ para llevar a cabo este juego que apunta maneras.


Ahora, para conmemorar este aniversario han publicado el prototipo (¡**que también está disponible para Linux!**) y un vídeo de esta versión comentada por los desarrolladores, eso sí en inglés, pero para haceros una idea de como pinta el juego es suficiente:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/elISQOq4hhI" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


Blasphemous es un juego de acción y plataformas 2D con un brillante apartado artístico "pixel art" y un transfondo pseudo-religioso que nos pone en la piel del "Penitente", un personaje atormentado que luchará contra criaturas diabólicas y seres grotescos atormentados por una sociedad ultra-religiosa. En la tierra de Ortodoxia se ha desatado la corrupción, y "el Penitente" tedrá que luchar para contener las hordas de seres que han corrompido su alma.


Si eres "backer" del juego, busca tu acceso al "backerkit" donde podrás encontrar la versión para Linux. ¿A que esperas?


Seguiremos atentos a las novedades de 'Blasphemous por que desde luego tiene una pinta genial. ¿No crees?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux en [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 

