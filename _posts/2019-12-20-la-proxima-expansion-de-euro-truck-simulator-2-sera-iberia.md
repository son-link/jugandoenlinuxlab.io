---
author: leillo1975
category: "Simulaci\xF3n"
date: 2019-12-20 15:26:54
excerpt: "<p>Por fin podremos recorrer la pen\xEDnsula ib\xE9rica gracias a @SCSSoftware.</p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ETS2/Iberia/ETS2_Iberia_copy.webp
joomla_id: 1142
joomla_url: la-proxima-expansion-de-euro-truck-simulator-2-sera-iberia
layout: post
tags:
- dlc
- scs-software
- iberia
- eurotruck-simulator-2
title: "La pr\xF3xima expansi\xF3n de \"Euro Truck Simulator 2\" ser\xE1..... IBERIA!!!!.\
  \  "
---
Por fin podremos recorrer la península ibérica gracias a @SCSSoftware.


 Un tweet de la [Revista Morcego](https://twitter.com/MorcegoRevista/status/1208019788804632576) nos ponía en guardia, Al principio he de reconocer que era un Fake,más teniendo en cuenta que es un [cacho de autopista](https://www.google.es/maps/@42.2593481,-8.6768083,3a,75y,3.47h,82.27t/data=!3m6!1e1!3m4!1sNWHeM0mv1pkm09aAAyMCFQ!2e0!7i16384!8i8192?hl=es) que recorro muy a menudo, pero después de comprobar que aun queda una semana para el día de los santos inocentes, y [encontrar en Steam su página correspondiente](https://store.steampowered.com/app/1209460/Euro_Truck_Simulator_2__Iberia/), me tomé esto mucho más en serio..... y es que tras una tensa busqueda, para encontrar una confirmación oficial, finalmente el anuncio se ha hecho en su tweet correspondiente:



> 
> Another hot news has just been revealed during our special Xmas live stream! ?  
>   
> Iberia is coming as the next map expansion for the [#ETS2](https://twitter.com/hashtag/ETS2?src=hash&ref_src=twsrc%5Etfw)! ?   
>   
> All aboard! The hype train is about to leave the station! Add it to your Steam wishlist now! ??<https://t.co/Nwg6euz7gL> [pic.twitter.com/yfGw4vm9r1](https://t.co/yfGw4vm9r1)
> 
> 
> — SCS Software (@SCSsoftware) [December 19, 2019](https://twitter.com/SCSsoftware/status/1207716503321284609?ref_src=twsrc%5Etfw)



Cuando parecía que nunca nos iba a tocar el poder recorrer nuestros lares, tras las sucesivas "decepciones" (recalco las comillas) de [Italia](index.php/component/k2/20-analisis/688-analisis-euro-truck-simulator-2-italia-dlc), [Beyond the Baltic Sea](index.php/component/k2/20-analisis/1067-analisis-euro-truck-simulator-2-beyond-the-baltic-sea-dlc), y [Road to the Black Sea](index.php/homepage/generos/simulacion/1136-road-to-the-black-sea-la-nueva-dlc-de-euro-truck-simulator-2-disponible), nos llega este bombazo informativo que bien seguro que aumentará el hype hasta que podamos disfrutar de su lanzamiento.


![ETS2 Iberia 2 copy](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ETS2/Iberia/ETS2_Iberia_2_copy.webp)


 Por ahora aun no tenemos mucho más que ofreceros, pero tan pronto tengamos más noticias os las traeremos raudos y veloces. BRAVO SCS, BRAVO.


 


¿Tu también estabas cansado de esperar para recorrer las carreteras de nuestra península? ¿Qué te parece el trabajo de SCS Software?  Cuéntanoslo en los comentarios, o en mensajes en nuestra comunidad de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

