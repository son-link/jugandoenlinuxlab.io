---
author: leillo1975
category: "Simulaci\xF3n"
date: 2023-02-08 19:35:08
excerpt: "<p>Los chicos de @speeddreams_oms nos traen montones de novedades y correcciones</p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SpeedDreams/2.3.0-RC.webp
joomla_id: 1524
joomla_url: speed-dreams-2-3-0-release-candidate
layout: post
tags:
- open-source
- speed-dreams
- simracing
title: "Publicada la versi\xF3n 2.3.0 Release Candidate de Speed Dreams"
---
Los chicos de @speeddreams_oms nos traen montones de novedades y correcciones


El equipo de desarrollo ha dado un paso más para alcanzar la **versión 2.3.0** lanzando en el día de hoy esta versión candidata. Como sabéis hace algo más de 3 meses se publicó la [versión Beta]({{ "/posts/ya-esta-aqui-la-primera-beta-de-la-nueva-version-de-speed-dreams" | absolute_url }}) con montones y montones de novedades con respecto a la última versión oficial, [la 2.2.3]({{ "/posts/speed-dreams-alcanza-oficialmente-la-version-2-2-3" | absolute_url }}), y tras unos meses de testeo **se han corregido y optimizado multitud de cosas en el código** de este veterano simulador de carreras de coches. También **se han incluido alguna que otra novedad**, como la inclusión de un nuevo coche, y algunas nuevas libreas. El anuncio se realizaba hace tan solo unos momentos en sus cuentas de redes sociales ([twitter](https://twitter.com/speeddreams_oms) y [mastodon](https://mastodon.social/@speed_dreams_official)):



> 
> We are one step closer to our long-awaited 2.3.0 release, as we just launched its [#ReleaseCandidate](https://twitter.com/hashtag/ReleaseCandidate?src=hash&ref_src=twsrc%5Etfw). We ask you again to test it and report bugs before the final version. More information and downloads at this link:<https://t.co/4brnY974D7>[#SpeedDreams](https://twitter.com/hashtag/SpeedDreams?src=hash&ref_src=twsrc%5Etfw) [#OpenSource](https://twitter.com/hashtag/OpenSource?src=hash&ref_src=twsrc%5Etfw) [#SimRacing](https://twitter.com/hashtag/SimRacing?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/ob1d5Tau7u](https://t.co/ob1d5Tau7u)
> 
> 
> — Speed Dreams Official - Open Motorsport Simulator (@speeddreams_oms) [February 8, 2023](https://twitter.com/speeddreams_oms/status/1623403948269883392?ref_src=twsrc%5Etfw)



 Tomando como punto de partida la anterior versión beta, encontraremos los siguientes cambios:


* Correcciones en la versión de BSD (Beaglejoe)
* Corrección de la compilación en ARM de 64 bits (Beaglejoe)
* Actualizaciones en CMake, Expat, Curl, Libpng y SDL2 (Beaglejoe)
* Nuevas ruedas para LS-GT1s. Nuevas libreas + fuentes: Tsubashi (Archer), TAIGA (Zentek), Aitronics-AEP (Cavallo), Sprint Group (Toro). (June Ravenmoon)  
![ls1 archer r9 preview](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SpeedDreams/ls1-archer-r9-preview.webp)
* Nuevas libreas para los pilotos de LS-GT1, para los siguientes coches: Cavallo, Marisatech, Newcastle, Taipan, Vulture V6-R (June Ravenmoon)
* Trackeditor: mostrar los objetos del mapa de objetos en las propiedades y en la vista de pista (IOBYTE)
* Arreglo del borde de la puerta del pit para los actuales equipos de robots LS-GT1 (June Ravenmoon)
* Corrección del widget de fuerzas de aceleración/frenado de OSG (Madbad)
* Plantilla de pintura para Vulture V5-R. También librea para Shadow LS-GT1 (June Ravenmoon)
* Muchos errores corregidos en trackeditor y trackgen (IOBYTE)
* Actualización del pitting en robots usr, shadow y dandroid (Xavier)
* Añadido sc-deckard-conejo a los robots dandroid (Xavier)
* Arreglado setup para los robots USR de sc-deckard-conejo (Xavier)
* Arreglos en neumáticos y degradación en categoría supercars (Xavier)
* Actualización del modelado 3D en el ls1-newcastle-fury (por WEC)  
![ls1 newcastle fury shocker2 preview](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SpeedDreams/ls1-newcastle-fury-shocker2-preview.webp)
* Arreglar bug con distancia de niebla en escena (Xavier)
* Arreglar neumaticos y degradacion para 67GP y añadir neumaticos y degradacion a 67gp-mango-ms7 (Xavier)
* Actualizar los sonidos de motores para 67gp (Xavier)
* Reemplazar algunos jpegs de previsualización (Beaglejoe)
* Stock Cars 70′, karts, y LS2-Cavallo-360 son ahora visibles usando OSG (IOBYTE)
* Añadir 67gp-murasama-37a (por WEC), añadir usr y shadow drivers (Xavier)  
  
![67gp murasama 37a preview](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SpeedDreams/67gp-murasama-37a-preview.webp)
* Actualizar la pista de Dijon (por Leillo)
* Actualizar robots usr, shadow y dandroid 67gp' (Xavier)
* Actualizar LS2-bavaria-nb (por WEC)
* Corregir interior faltante de 36gp-silber-w25b (IOBYTE)
* Ejecutar mogrify en archivos png que tienen: " libpng warning: iCCP: known incorrect sRGB profile" (IOBYTE)
* Añadir la temperatura del aire en la interfaz SSG (Xavier)
* Arreglar temperatura de neumáticos para 67gp-cavallo (Xavier)
* Actualizar para tener ajuste de rotación del volante visible (Xavier)
* Intentar arreglar colisión al inicio cuando la curva está cerca en robots shadow (Xavier)
* Mejor frenado para robots dandroid (Xavier)
* Añadida nueva variable para mejorar el frenado en robots shadow (Xavier)
* Arreglado bug con rueda->Tire (Xavier)


 Como veis vale mucho la pena actualizar desde la beta de hace unos meses a esta nueva Release Candidate, además de que si encontráis "cositas" que no deben estar ahí, y lo reportáis, le hacéis un gran servicio al equipo de desarrollo de Speed Dreams. Tenéis las diferentes formas de poneros en contacto con ellos para estas lides en [este artículo de su página web oficial](https://www.speed-dreams.net/en/new-2-3-0-release-candidate/), donde **también encontrareis todos los enlaces para descargaros esta versión RC** del juego, entre los que se encuentra **la AppImage desarrollada por nuestro compañero @son_link**. Para despedirnos os dejamos con último video de su [canal de Youtube oficial](https://www.youtube.com/@speeddreams_oms) donde podreis ver una carrera con uno de los nuevos coches:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/yDqjnrhO7ag" title="YouTube video player" width="780"></iframe></div>


 


 Si queréis comentar algo o tenéis cualquier duda sobre Speed Dreams, podéis poneros en contacto con nosotros en nuestra [sala de Matrix "Zona Racing"](https://matrix.to/#/#zona_racing-jugandoenlinux:matrix.org) o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

