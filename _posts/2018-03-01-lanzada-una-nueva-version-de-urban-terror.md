---
author: leillo1975
category: "Acci\xF3n"
date: 2018-03-01 10:48:42
excerpt: <p>Veamos cuales son las novedades de este divertido shooter libre.</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/90c57ca8306bf148ef75c82407e738af.webp
joomla_id: 665
joomla_url: lanzada-una-nueva-version-de-urban-terror
layout: post
tags:
- open-source
- ioquake
- quake-3
- shooter
title: "Lanzada una nueva versi\xF3n de Urban Terror (ACTUALIZADO)"
---
Veamos cuales son las novedades de este divertido shooter libre.

**ACTUALIZACIÓN 25-6-18**: Hace unos meses nos hacíamos eco de la salida de la versión 4.3.3 del juego. Al parecer esta tenía problemas con cierres inesperados y parones en ciertos mapas, en especial "Eagle". Para ello hace unos pocos días acaba de ser lanzada una actualización, la 4.3.4 (gracias Alfonso por el chivatazo), donde principalmente solucionan estos problemas, además de actualizar a la última versión de [ioquake](https://ioquake3.org/), lo que permite soporte para VoIP, Mumble, un nuevo renderizador OpenGL2 y muchas correcciones. Podeis ver el listado completo de novedades en este [enlace](https://www.urbanterror.info/news/520-urban-terror-4-3-4-release/). En JugandoEnLinux.com estamos deseando montar una partida y probar todas sus "bondades".




---


**NOTICIA ORIGINAL**: Hoy vamos con uno de los más veteranos. Como bien sabeis, el código fuente de Quake 3 ([ioquake3](https://ioquake3.org/)) fué liberado hace ya bastantes años por ID Software, concretamente el 20 de Agosto de 2005. Desde entonces numerosos proyectos de shooters libres han sido desarrollados con mayor o menor suerte. Ejemplos de ello son Open Arena, [Xonotic](index.php/homepage/generos/accion/item/398-nueva-version-de-xonotic-con-importantes-mejoras), Tremulous, Smokin' Guns.... y por supuesto este [Urban Terror](http://www.urbanterror.info/home/).


El juego, desarrollado por [FrozenSand](https://frozensand.com/) , es un **FPS multiplayer libre y gratuito** enfocado a la diversión pura. El juego está ambientado en escenarios basados en el mundo real, al igual que sus armas, equipamientos y modelos. Puede recordar en ciertos momentos al mítico Counter Strike y posee **numerosos modos de juego** entre los que encontramos "Capture The Flag", "Team Survivor", "Capture and Hold" o "Bomb Mode" , entre otros. Disponeis de un montón de información sobre el juego en [Wikipedia](https://es.wikipedia.org/wiki/Urban_Terror).


Pero no nos distraigamos, la noticia es que hoy mismo ha sido liberada una nueva versión, concretamente la **4.3.3**, que basicamente se trata de una versión de mantenimiento con numerosos bugs corregidos relativos a la seguridad, el motor, o el gameplay, entre otros. Podeis consultar la [Lista de Cambios](http://www.urbanterror.info/news/516-urban-terror-4-3-3-release/) para disponer de una información más detallada sobre esta actualización.


También es importante comentar que se está trabajando en **Urban Terror : Resurgence** (también conocida con Urban Terror HD), una conversión total del juego hecha con el **Unreal Engine 4**, siendo su estado y fecha de lanzamiento desconocidos. En JugandoEnLinux.com os informaremos de cualquier novedad que llegue a nuestras manos tanto sobre este juego como el [desarrollo de UT: Resurgence](http://www.urbanterror.info/forums/forum/68-urban-terror-resurgence-and-ue4/). También nos gustaría animaros a que si os apetece jugar a este shooter lo propongais en nuestro canal de [Telegram](https://t.me/jugandoenlinux) para jugarlo todos juntos. Os dejamos con un video del juego para que si no lo conoceis os hagais una idea:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/2qgQXzTQ4AU" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


¿Habíais jugado ya a Urban Terror? Contéstanos en los comentarios o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

