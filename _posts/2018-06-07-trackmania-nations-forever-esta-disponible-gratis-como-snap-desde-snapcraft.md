---
author: Pato
category: Carreras
date: 2018-06-07 18:28:18
excerpt: <p>Se trata de un "experimento de fin de semana" desarrollado entre otros
  por Martin Wimpress</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/893c4fdc824f051f07606f44d5d60e5a.webp
joomla_id: 759
joomla_url: trackmania-nations-forever-esta-disponible-gratis-como-snap-desde-snapcraft
layout: post
tags:
- carreras
- arcade
title: "TrackMania Nations Forever est\xE1 disponible gratis como Snap desde Snapcraft"
---
Se trata de un "experimento de fin de semana" desarrollado entre otros por Martin Wimpress

Nos hacemos eco del post de [linuxandubuntu.com](http://www.linuxandubuntu.com/home/trackmania-nations-forever-available-as-a-snap-application) (gracias a **Txema** por ponernos bajo la [pista](https://plus.google.com/+coreydrewbruce/posts/L3NasMGJPxg)) en el que nos anuncian que un tal Martin Wimpress (¿alguien dijo Ubuntu Mate?) ha hecho un llamamiento a testear el "invento" que dice que han hecho como "ejercicio de fin de semana". Y es que de lo que se trata es de que han "embotellado" con Wine, el "traductor" de programas de Windows a Linux el juego gratuito 'TrackMania Nations Forever'. ¿Como han hecho esto sin permisos de distribución?


Sencillo. Gracias a la capacidad de la paquetería Snap, han adaptado el Wine que necesitan y por otro lado descargan el juego directamente desde la página oficial de descarga gratuita, lo que es perfectamente legal y lo ponen a funcionar todo junto para una experiencia lo más óptima posible.


¿Quieres saber como hacerlo. Es bien sencillo:


Lo primero es tener un sistema con capacidad para instalar paquetes Snapcraft. Lo segundo ejecutar estas simples líneas en una terminal:



> 
> 
> ```
> snap install tmnationsforever --edge  
> 
> ```
> 
> `snap connect tmnationsforever:joystick`
> 
> 
> 


Listo, como está en el repositorio de Snapcraft se instalará y descargará lo necesario para ejecutar el juego mediante Wine. ¡A disfrutar!


**TrackMania Nations Forever**



* Juego completamente gratuito
* Cautivador modo solitario con 65 nuevas pistas
* Únete a millones de jugadores online y completa cientos de pistas en los servidores de TracMania
* Un entorno completo de TrackMania: "Stadium Forever"
* Editor del juego para crear tus propias pistas, estudio de vídeo para crear tus propias películas y una tienda de pintura para personalizar tus vehículos
* Listas oficiales para solo o multijugador


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/_HOJxiQ34C0" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>
En cuanto a los requisitos del juego, poco podemos decir al no estar disponible para Linux. Tan solo tenemos como referencia lo que pide el juego en [Windows](https://store.steampowered.com/app/11020/TrackMania_Nations_Forever/):


* **Procesador:**  Pentium IV 1.6GHz / AthlonXP 1600+
* **Graficos:**  3D accelerator 16 MB DirectX 9.0c compatible graphics card
* **DirectX Version:**  DirectX 9.0c or better
* **Memoria:**  256 MB (512 MB con Vista)
* **Sonido:**  16 bit DirectX compatible
* **HD:**  750 MB


Tienes la información del desarrollo del Snap en el post del propio Martin Wimpress [en el foro de Snapcraft](https://forum.snapcraft.io/t/call-for-testing-track-mania-nations-forever/5708).


