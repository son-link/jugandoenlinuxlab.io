---
author: P_Vader
category: Puzzles
date: 2021-10-12 13:01:16
excerpt: "<p><a href=\"https://twitter.com/draknek\" target=\"_blank\" rel=\"noopener\"\
  >@draknek</a> nos trae otro entretenido juego de rompecabezas dise\xF1ado por <a\
  \ href=\"https://twitter.com/_coreymartin\" target=\"_blank\" rel=\"noopener\">@_coreymartin</a>.</p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/BonfirePeaks/bonfirepeaks-portada.webp
joomla_id: 1375
joomla_url: bonfire-peaks-puzles-relajantes-pero-nada-faciles
layout: post
tags:
- puzzles
- pixel
- voxel
title: "Bonfire Peaks, puzles relajantes pero nada f\xE1ciles"
---
[@draknek](https://twitter.com/draknek) nos trae otro entretenido juego de rompecabezas diseñado por [@_coreymartin](https://twitter.com/_coreymartin).


No soy un especialista de juegos de puzles, pero siempre que empiezo uno y es bueno me engancho. Y en esta ocasión, [tras ver anunciado](https://store.steampowered.com/news/app/1147890/view/2863717429846959784) este juego con su demo, no me pude resistir.


El juego ha sido creado por [Corey Martin](http://toboggan.work/) y lanzazdo por [Draknek](https://www.draknek.org/). Ambos tienen a sus espaldas unos cuantos juegos de este genero y se nota la experiencia.


La historia parte de un genuino personaje que se encuentra en una misteriosa isla, donde debe ir avanzando a lo largo de casi **200 niveles de rompecabezas,** en los cuales siempre aparece una hoguera y una caja con pertenencias que deben ser quemadas, claro que llevar la caja hasta el fuego no es tarea fácil. **El diseño es un clásico estilo [Sokoban](https://es.wikipedia.org/wiki/Sokoban) con el añadido de la tridimensionalidad.**


![Bonfire Peaks captura](https://www.bonfirepeaks.com/press/img/screenshots/bonfirepeaks-screens202109-02.jpg)


La **vista isométrica  permite cierta libertad de cámara**, pudiendo rotarla, alejarte o acercarte, de manera muy suave para observar diferentes puntos de vista de la escena.


La **ambientación y música es como es de esperar bastante relajante y te mantiene concentrado en el juego,** además consigue que no nos arda el cerebro en ciertos puntos.


Personalmente me han agradado bastante los **gráficos con el estilo voxelado y pixelado, muy buen diseño y sobresalientes** dentro de la limitación de por si del estilo.


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/yGd1cP_dKQE" title="YouTube video player" width="780"></iframe></div>


Si te gustan los puzles creo que **deberías probar su demo en [Steam](https://store.steampowered.com/app/1147890/Bonfire_Peaks/)** o comprarlo:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/1147890/" style="border: 0px;" width="646"></iframe></div>


También los puedes adquirir en Itch:


<div class="resp-iframe"><iframe height="167" src="https://itch.io/embed/396012" style="border: 0px;" width="552"><a href="https://coreymartin.itch.io/bonfire-peaks">Bonfire Peaks by Corey Martin, Draknek &amp; Friends</a></iframe></div>


¿Vas a probar este endiablado rompecabezas? Si es así cuéntanos que te ha parecido en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).
