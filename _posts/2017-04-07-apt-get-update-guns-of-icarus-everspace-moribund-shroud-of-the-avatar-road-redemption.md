---
author: Pato
category: Apt-Get Update
date: 2017-04-07 18:22:57
excerpt: "<p>Nuevamente estamos a viernes, y como la loter\xEDa, ya toca</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/64f7b8990be2d94add5152c155ac4915.webp
joomla_id: 286
joomla_url: apt-get-update-guns-of-icarus-everspace-moribund-shroud-of-the-avatar-road-redemption
layout: post
title: Apt-Get Update Guns of Icarus & Everspace & Moribund & Shroud of the Avatar
  & Road Redemption...
---
Nuevamente estamos a viernes, y como la lotería, ya toca

De nuevo es Viernes, día en que como viene siendo habitual hacemos el repaso semanal de lo que ha pasado en el mundo del juego en Linux y no hemos podido contar. Comenzamos:


Desde [www.gamingonlinux.com](http://www.gamingonlinux.com):


- 'Guns of Icarus Alliance' un juego de gestion de naves y disparos multijugador online ya está disponible como juego individual. Puedes verlo [en este enlace](https://www.gamingonlinux.com/articles/airship-combat-game-guns-of-icarus-alliance-released-as-a-standalone-game.9458).


- 'EVERSPACE' el juego de acción espacial que está siendo desarrollado en Unreal Engine parece que ha encontrado un modo de avanzar sobre los bugs en su camino hacia Linux. Nos lo cuenta Liam [en este enlace](https://www.gamingonlinux.com/articles/everspace-still-dealing-with-unreal-engine-bugs-possible-workaround-found-for-linux.9464).


- 'Moribund' un juego "brawler" multijugador local con una jugabilidad endiablada ya está disponible en Linux. Puedes verlo [en este enlace](https://www.gamingonlinux.com/articles/moribund-a-brutal-local-multiplayer-brawler-released-with-linux-support.9466).


- 'Shroud of the Avatar' el grandioso juego de rol de Lord British ha recibido una gran actualización y se puede jugar gratis por unos días. Puedes verlo en este enlace.


Desde [www.linuxgameconsortium.com](http://www.linuxgameconsortium.com):


- 'Feral Fury' es un juego de acción brutal vastante gore en vista cenital. Puedes verlo [en este enlace](https://linuxgameconsortium.com/linux-gaming-news/feral-fury-gory-top-down-shooter-releases-today-steam-51177/).


- 'King under the Mountain' es un juego de construcción y gestión estratégica que está en campaña en Kickstarter y tiene demo gratis en Linux. Puedes verlo [en este enlace](https://linuxgameconsortium.com/linux-gaming-news/king-under-the-mountain-kickstarter-free-linux-demo-51260/).


- La expansión 'Stellaris Utopia' ya está disponible con un buen montón de contenido y mejoras. Puedes verlo [en este enlace](https://linuxgameconsortium.com/linux-gaming-news/stellaris-utopia-launches-steam-51462/).


- 'Road Redemption' el juego de acción sobre motos al estilo "Road Rush" ha recibido una buena actualización con nuevas mejoras y contenido. Puedes verlo [en este enlace](https://linuxgameconsortium.com/linux-gaming-news/road-redemption-latest-update-includes-linux-via-steam-51142/).


 


Vamos ya con los vídeos de la semana:


Feral Fury


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/q_HDpoNElS4" width="640"></iframe></div>


Guns of Icarus Alliance


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/utgPtBHDAUA" width="640"></iframe></div>


Moribund


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/wWS-OvOntPo" width="640"></iframe></div>


 Shroud of the Avatar


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/g3zXNn6YP-8" width="640"></iframe></div>


 Stellaris Utopia


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/Bl5IjvPvSPs" width="640"></iframe></div>


 Road Redemption


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/Uiwp8uv4dZQ" width="640"></iframe></div>


 


Esto es todo por esta semana. ¿Qué te ha parecido? 


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux en [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

