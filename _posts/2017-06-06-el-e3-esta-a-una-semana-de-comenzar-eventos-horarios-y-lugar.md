---
author: Pato
category: Editorial
date: 2017-06-06 06:22:39
excerpt: "<p>Devolver Digital celebrar\xE1 su conferencia de prensa durante el \"\
  Pre Pre Show\"</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/dd45d054dfce696b68bc0b43a11d1bfe.webp
joomla_id: 358
joomla_url: el-e3-esta-a-una-semana-de-comenzar-eventos-horarios-y-lugar
layout: post
tags:
- proximamente
- e3-2017
title: "El E3 est\xE1 a una semana de comenzar: eventos, horarios y lugar"
---
Devolver Digital celebrará su conferencia de prensa durante el "Pre Pre Show"

Hace una semana ya [os informamos](index.php/homepage/editorial/item/449-el-e3-2017-se-acerca-y-en-jugando-en-linux-vamos-a-seguirlo-de-cerca) que "asistiríamos" (al menos) a dos de las conferencias del E3 2017 desde la comodidad de nuestras casas. Pues bien, estamos ya a tan solo una semana de que todo empiece y es momento de explicar donde y cuando vamos a verlas.


En primer lugar, hay que señalar que Devolver Digital ha anunciado que su conferencia de prensa se llevará a cabo durante el evento "Pre Pre Show" el próximo Lunes:



> 
> The Devolver E3 “press conference” will air during [@Twitch](https://twitter.com/Twitch)’s Pre Pre Show June 11 at 10PM PT.  
>   
> It’s ridiculous. <https://t.co/5bMgPKxnrk> [pic.twitter.com/aM9dJKt0JR](https://t.co/aM9dJKt0JR)
> 
> 
> — Devolver Digital (@devolverdigital) [June 6, 2017](https://twitter.com/devolverdigital/status/871901847577141248)



Este evento es realmente un "maratón de espacios" que estará online durante 9 horas seguidas. Pero a lo que vamos, según el anuncio de Devovler Digital tenemos que estar atentos el próximo Lunes 12 a partir de las 7 de la mañana (horario español)... siendo así tendremos que estar "online" siempre que podamos para intentar no perdernos nada.


Después de esto, el siguiente evento al que vamos a "asistir" es el "PC Gaming Show", que se llevará a cabo también el Lunes 12 de Junio a las 19:00 h (GTM+2) o 21:00h (horario español).


Está claro que el Lunes va a ser un día muy largo. ¿Y donde vamos a ver estos eventos?


Pues **aquí mismo**. Gracias a las posibilidades de streaming de Twitch y su capacidad, vamos a "embeber" el streaming oficial de los eventos en nuestro propio [Canal de Twitch](https://www.twitch.tv/jugandoenlinux), de modo que **podremos verlos directamente en nuestra propia web**. **Habilitaremos un apartado especial "En Vivo"** en la página jugandoenlinux.com en donde podremos ver los eventos y utilizar nuestro propio chat donde estaremos para comentar con vosotros todas las novedades que se vayan desvelando.


¿Te apuntas? ¿Te interesaría ver otros eventos con nosotros?


Cuéntanoslo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 


 

