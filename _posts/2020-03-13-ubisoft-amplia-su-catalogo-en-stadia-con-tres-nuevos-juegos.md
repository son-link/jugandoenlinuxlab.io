---
author: Pato
category: Noticia
date: 2020-03-13 12:16:54
excerpt: "<p>The Division 2, The Crew 2 y Monopoly son las propuestas que llegar\xE1\
  n a nuestras pantallas linuxeras</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Stadia/Ubistadia13022020.webp
joomla_id: 1190
joomla_url: ubisoft-amplia-su-catalogo-en-stadia-con-tres-nuevos-juegos
layout: post
tags:
- proximamente
- stadia
- ubisoft
title: "Ubisoft ampl\xEDa su cat\xE1logo en Stadia con tres nuevos juegos"
---
The Division 2, The Crew 2 y Monopoly son las propuestas que llegarán a nuestras pantallas linuxeras


Poco a poco Stadia empieza a ponerse las pilas, esta vez con la apuesta por parte de Ubisoft de incluir en la plataforma de streaming tres nuevos juegos como son **The Division 2, The Crew 2 y Monopoly**. 


Según leemos en su comunicado, en próximas fechas tendremos disponible Tom Clancy’s The Division 2 con la nueva expansión Warlords of New York y The Crew 2 el próximo día **25 de Marzo**, y además Monopoly llegará el próximo **28 de Abril**.


Además, **Tom Clancy's The Division 2** será el primer juego de Stadia que tendrá juego cruzado y progreso cruzado con otras plataformas de PC si vinculas tu cuenta con la de Ubisoft.


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/T_FJwyytVRs" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Tom Clancy’s The Division 2 incluirá la característica "Stream Connect", una característica que anuncian exclusiva de Stadia que te permite transmitir los puntos de vista de tus compañeros de equipo en una sola pantalla. *Con Stream Connect, los agentes de la división pueden recorrer los mapas de mundo abierto de Washington D.C. y el Bajo Manhattan y ver las áreas del mapa desde los ojos de sus compañeros de escuadrón. Además de tu propio punto de vista, podrás ver puntos de vista de hasta tres compañeros de escuadrón.*


Por otra parte, para los amantes de la velocidad tendremos la posibilidad de conducir diversos vehículos en **The Crew 2**:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/X9-bWlWilps" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


*Enfréntate a la escena del automovilismo estadounidense mientras exploras y dominas la tierra, el aire y el mar de los Estados Unidos en uno de los mundos abiertos más emocionantes jamás creados en The Crew 2. Con una amplia variedad de coches exóticos, bicicletas , barcos y aviones para elegir, experimente la emoción desenfrenada y la emoción de la adrenalina de competir en todo Estados Unidos mientras pruebas tus habilidades en una amplia gama de disciplinas de conducción.*


Por último, para los que gustan de los juegos de tablero, tendremos disponible el clásico e incombustible **Monopoly** para jugar solo o en familia:


*¡Juega al juego de mesa más famoso del mundo con Stadia el 28 de abril! Ahora puedes disfrutar del juego que conoces y amas en 3 tableros vivos únicos en 3D. Tu ciudad vive y evoluciona; ¡invierte en barrios encantadores con personalidades únicas y fuertes y mira cómo prosperan frente a tus ojos a medida que progresas! Juega como quieras seleccionando una de las seis reglas oficiales de la casa elegidas por los fanáticos de MONOPOLY de todo el mundo. ¿No tienes tiempo suficiente para un juego completo? Elije un nuevo objetivo de nuestros cinco objetivos especiales. Esos objetivos más rápidos de lograr asegurarán sesiones de juego más cortas y te harán cambiar tu estrategia. ¡Prepárate para desafiarte a ti mismo!*


![Monopoly Stadia](/https://community.stadia.com/t5/image/serverpage/image-id/1689i1B504EBD78084E89/image-dimensions/829x467?v=1.webp)


Estos títulos estarán disponibles para comprar desde la tienda de Stadia como hemos dicho a partir del próximo 25 de Marzo y 28 de Abril respectivamente. Tienes toda la información en el [post del comunicado](https://community.stadia.com/t5/Stadia-Community-Blog/Exciting-games-from-Ubisoft-coming-soon-to-Stadia/ba-p/16918).


 

