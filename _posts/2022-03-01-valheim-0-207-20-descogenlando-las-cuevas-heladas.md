---
author: Son Link
category: Rol
date: 2022-03-01 16:52:53
excerpt: "<div>Irongate Studios acaba de lanzar oficialmente esta actualizaci\xF3\
  n de <span class=\"css-901oao css-16my406 r-poiln3 r-bcqeeo r-qvutc0\">@Valheimgame</span></div>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/valheim/valheim-cuevas-heladas.webp
joomla_id: 1441
joomla_url: valheim-0-207-20-descogenlando-las-cuevas-heladas
layout: post
tags:
- steam
- supervivencia
- valheim
- steam-deck
title: 'Valheim 0.207.20: Descogenlando las Cuevas Heladas'
---
Irongate Studios acaba de lanzar oficialmente esta actualización de @Valheimgame
  
Hace un par semanas os [hable de la salida de la beta abierta]({{ "/posts/valheim-0-207-15-beta-publica" | absolute_url }}) de la próxima gran actualización de **Valheim**, pues bien, ya esta disponible para todo el mundo dicha gran actualización.


Una de las grandes novedades son las nuevas localizaciones en las **Montañas**, donde nos podremos encontrar con las **Cuevas Heladas**, hogar de los **fenrings** (esos seres parecidos a los hombres lobos), así como nuevos enemigos, algunos de ellos encima osaran atacar nuestra base (como si ya no tuviéramos bastante ;) ).


Para aventurarnos en ellas, si juegas en una partida ya empezada, solo se podrán encontrar en zonas inexploradas. También decir que esta segunda gran actualización reemplazada a **El Culto del Lobo**, de la hace tiempo cancelada primera hoja de ruta (en mayo del año pasado)


Otro de los cambios que mencionados en el articulo anterior es el soporte para mandos y **Steam Deck**. Pues gracias a la gente de **Gaming On Linux** podemos verlo en acción:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube.com/embed/GcAFtLceVy0" title="YouTube video player" width="720"></iframe></div>


 


Y para seguirnos abriendo la boca con la próxima gran actualización han compartido un par de artes conceptuales de otro de los enemigos que nos podremos encontrar en las **Mistlands** y de como quieren que nos sintamos al explorar. Si quieres verlos puedes encontrarlos en [este articulo](https://store.steampowered.com/news/app/892970?emclan=103582791463217109&emgid=3131694924763930710) dentro de las noticias del juego en Steam


Por ultimo, y ya como algo aparte, han publicado la construcción del mes, otra muestra de los que puedes llegar a construir con mucho tiempo, material e imaginación:


![valheim cuevas heladas constrccion mes](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/valheim/valheim-cuevas-heladas-constrccion-mes.webp)


 Y esto es todo de momento. Hasta la proxima Hijos/as de Odin

