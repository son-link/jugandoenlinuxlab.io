---
author: Serjor
category: Hardware
date: 2019-03-30 10:53:42
excerpt: <p>Para Valve la realidad virtual sigue siendo un mercado interesante por
  el que apostar</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/68fb3e3b9d4ee9517ac1022989fdfc49.webp
joomla_id: 1009
joomla_url: valve-index-las-gafas-de-realidad-virtual-de-valve-por-fin-se-confirman
layout: post
tags:
- realidad-virtual
- valve
- vr
- valve-index
title: Valve Index, las gafas de realidad virtual de Valve por fin se confirman
---
Para Valve la realidad virtual sigue siendo un mercado interesante por el que apostar

Ya lo adelantábamos el año pasado, y es que había [rumores muy fundados](https://jugandoenlinux.com/index.php/homepage/realidad-virtual/38-realidad-virtual/1019-rumor-las-gafas-de-realidad-virtual-de-valve-cada-vez-mas-cerca) de que Valve iba a desarrollar sus propias gafas de realidad virtual.


Y es que nos enteramos a través [uploadvr](https://uploadvr.com/valve-teases-index-vr-headset-for-may/) de que por fin la propia Valve ha confirmado el rumor, y aunque sin mucho bombo y platillo (no les hace falta, la verdad), han creado en su tienda una sección para [Valve Index](https://store.steampowered.com/sale/valve_index/), que es como se llamaran estas gafas de realidad virtual.


En sí no es más que una imagen (la que podéis ver en al cabecera del artículo), con la que entendemos será la fecha de salida de dicho dispositivo, o al menos su presentación oficial, mayo de este mismo año.


Toca esperar y ver cómo será esta solución de realidad virtual, no solamente el diseño final de las gafas, también el diseño final de los mandos, necesidades de accesorios extra o no, precio, catálogo y requerimientos de máquina para ese catálogo, y sobre todo, ¿habrán conseguido evitar el mareo que algunos usuarios sufrimos?


Y tú, ¿piensas que la realidad virtual llegará alguna vez para quedarse o todavía no es su momento? Cuéntanoslo en los comentarios y en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD)

