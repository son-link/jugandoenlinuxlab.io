---
author: leillo1975
category: Ofertas
date: 2020-05-07 18:05:42
excerpt: "<p>Adem\xE1s la @humble Store regala \"Ashes of the Singularity: Escalation\"\
  . </p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/HumbleBundle/SpringSale2020.webp
joomla_id: 1216
joomla_url: ha-llegado-la-spring-sale-a-la-tienda-de-humble-bundle
layout: post
tags:
- humble-bundle
- ofertas
- humble-store
- descuentos
title: Ha llegado la "Spring Sale" a la tienda de Humble Bundle
---
Además la @humble Store regala "Ashes of the Singularity: Escalation". 


 Como siempre, nos hacemos eco de las rebajas en las principales tiendas que venden juegos para nuestro sistema, y como no, a la que más cariño le tenemos es a la [Humble Store](https://www.humblebundle.com/store/search?sort=bestselling&filter=onsale&partner=jugandoenlinux), pues como sabeis, con cada juego que compreis en ella a través de nuestros enlaces, estareis colaborando con el sostenimiento de esta web, pues hosting y dominios, sin publicidad no salen a cuenta... Por supuesto hemos preparado una lista con algunas de estas ofertas de juegos que van "de rechupete" en nuestro sistema. Estas ofertas estarán disponibles a lo largo de las siguientes dos semanas:


#### [Deus Ex: Mankind Divided™ - Digital Deluxe Edition](https://www.humblebundle.com/store/deus-ex-mankind-divided-digital-deluxe?partner=jugandoenlinux): 6.74€ (-85%)


#### [Slime Rancher](https://www.humblebundle.com/store/slime-rancher?partner=jugandoenlinux): 7.99€ (-60%)


#### [For The King](https://www.humblebundle.com/store/for-the-king?partner=jugandoenlinux): 7.99@ (-60%)


#### [Move or Die](https://www.humblebundle.com/store/move-or-die?partner=jugandoenlinux): 3.74€ (-75€)


#### [Shadow of the Tomb Raider: Definitive Edition](https://www.humblebundle.com/store/shadow-of-the-tomb-raider-definitive-edition?partner=jugandoenlinux): 19.79€ (-67%)


#### [Shadow Tactics: Blades of the Shogun](https://www.humblebundle.com/store/shadow-tactics-blades-of-the-shogun?partner=jugandoenlinux): 9.99€ (-75%)


#### [State of Mind](https://www.humblebundle.com/store/state-of-mind?partner=jugandoenlinux): 4.49€ (-85%)


#### [Rise of the Tomb Raider: 20 Year Celebration Edition](https://www.humblebundle.com/store/rise-of-the-tomb-raider-20-year-celebration?partner=jugandoenlinux): 8.99€ (-70%)


#### [Sid Meier’s Civilization® VI](https://www.humblebundle.com/store/sid-meiers-civilization-6?partner=jugandoenlinux): 17.99€ (-70%)


#### [Life Is Strange™ - Complete Season](https://www.humblebundle.com/store/life-is-strange-complete-season?partner=jugandoenlinux): 3.99€ (-80%)


#### [Tomb Raider (2013)](https://www.humblebundle.com/store/tomb-raider?partner=jugandoenlinux): 2.99€ (-85%)


#### [XCOM: Enemy Unknown Complete Pack](https://www.humblebundle.com/store/xcom-enemy-unknown-complete-pack?partner=jugandoenlinux): 5.99€ (-80%)


#### [F1 2017](https://www.humblebundle.com/store/f1-2017?partner=jugandoenlinux): 5.99€ (-70%)


#### [Victor Vran](https://www.humblebundle.com/store/victor-vran?partner=jugandoenlinux): 5.99€ (-70%)


Estos solo son algunos ejemplos, pero podeis buscar ese juego al que le teneis tantas ganas en [este enlace](https://www.humblebundle.com/store/search?sort=bestselling&filter=onsale&partner=jugandoenlinux). También no debeis olvidar que también [está regalando el juego "Ashes of the Singularity: Escalation"Ashes of the Singularity: Escalation"](https://www.humblebundle.com/store/ashes-of-the-singularity-escalation-free-game?partner=jugandoenlinux), juego que no consiguió finalmente el soporte, pero que con [unos pocos retoques](https://www.protondb.com/app/507490) funciona con Proton. Como siempre, podeis mencionar otros juegos con descuento de la Humble Store en los comentarios de este artículo, en nuestra comunidad de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

