---
author: leillo1975
category: Editorial
date: 2021-08-02 10:17:47
excerpt: "<p>La encuesta mensual del mes de Julio as\xED lo atestigua.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/steam-linux.webp
joomla_id: 1335
joomla_url: steam-vuelve-a-tener-un-1-de-usuarios-con-gnu-linux
layout: post
tags:
- steam
- linux
- steam-deck
- encuesta
title: Steam vuelve a tener un 1% de usuarios con GNU/Linux
---
La encuesta mensual del mes de Julio así lo atestigua.


 Aun recuerdo años ha, cuando Steam anunció el lanzamiento de su cliente para Linux (si, ya está el abuelo cebolleta otra vez con el tema...), y de repente se desató el furor por los juegos entre los usuarios de nuestro sistema. En aquel momento, **se demostró que a muchos Linuxeros les gustaba jugar**, y que ya estaban hartos de los pocos juegos nativos que existían, de usar un Wine que aun dejaba mucho que desear, y de tener que reiniciar el sistema para usar Windows. En aquella época **el interés fué tal que se alcanzó un 2% de cuota**, algo que hizo que muchas compañías pusiesen el ojo en esto y al menos se planteasen el portar sus juegos a GNU/Linux.


Pero poco a poco **este porcentaje fué menguando por varios factores** que si me permitís voy a recordaros por si aun no los conoceis:


- **El desarrollo drivers gráficos y de periféricos aún estaba "en pelotas".** Quizás los usuarios de Nvidia no lo notaban tanto, pero si tenías una gráfica de AMD estabas perdido si pretendías usar tu equipo para jugar en Linux, pues el driver propietario era vergonzoso, y el libre aun estaba lejos de ser usable. En el caso de los periféricos, había muchos que directamente no funcionaban, o no tenían un soporte adecuado, algo que por desgracia aun ocurre en demasiadas ocasiones.


- **La mala calidad de algunos ports muy esperados.** Es horroroso recordar el caso del port de [Virtual Programing]({{ "/tags/virtual-progamming" | absolute_url }}) de "The Witcher 2", que incluso tuvo "el honor" de conseguir que debido a la actitud de muchos "Linuxeros" maleducados [se cancelase el port de la tercera parte]({{ "/posts/the-witcher-3-posiblemente-no-llegara-nunca-a-linux-por-las-criticas-a-the-witcher-2" | absolute_url }}); pero no toda la culpa la tiene esta empresa, pues **muchos otros o no tenían un rendimiento adecuado o venían recortados en funciones**, como bastantes títulos cuyo apartado online no permitía la multiplataforma.


- **La cancelación de ports de juegos AAA.** No solo fué el caso de "The Witcher 3" como acabamos de comentar, [muchos otros juegos que Valve y las compañías habían anunciado a bombo y platillo se quedaron así, en solo un anuncio]({{ "/posts/lo-que-pudo-haber-sido-y-no-fue" | absolute_url }}). Street Fighter V, Rome 2 Total War, la saga Darksiders, Project Cars... Es cierto que tenemos juegos y juegos nativos en nuestro sistema para esta y 3 vidas más, pero aunque los juegos indie están muy bien, **la gran mayoría de  los jugadores quieren disfrutar de las grandes franquicias**, y eso es algo que por desgracia no ocurrió.


- **El fracaso estrepitoso de las Steam Machines.** Todo lo anterior sumado a unas **máquinas excesivamente caras, sin una máquina de referencia por parte de Valve, y un SteamOS muy inmaduro**, acabó provocando que se vendiesen muy poco, y que los "afortunados" poseedores de estas acabasen instalandole Windows.


A mi juicio estas son las razones que propiciaron que ese 2% de aquellos tiempos fuese solo un espejismo y mucha gente volviese a Windows para poder disfrutar de sus juegos, o que siga usando el Dual Booting; algo a lo que muchos no hemos sucumbido pese a la tentación, pero que es perfectamente entendible.


Ahora que **parece que Valve ha aprendido de estas experiencias** y ha puesto toda la carne en el asador con [Steam Deck]({{ "/tags/steam-deck" | absolute_url }}) y Proton, el interés en GNU/Linux como SO para juegos vuelve  a estar en el candelero, lo que se refleja en la [última encuesta de Julio sobre hardware y software](https://store-steampowered-com.translate.goog/hwsurvey/Steam-Hardware-Software-Survey-Welcome-to-Steam?platform=combined&_x_tr_sl=en&_x_tr_tl=es&_x_tr_hl=gl&_x_tr_pto=ajax,se,elem). Aunque la subida no es tan espectacular como antaño , el caso es que ha alcanzado el 1% , una cifra que llevaba años rondando pero que nunca parecía que se lograse alcanzar.


![SteamSurveyJuly21](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Steam/SteamSurveyJuly21.webp)


Se puede observar un **crecimiento con respecto al mes anterior de 0.11%**, que si bien puede parecer no mucho, en nuestro SO es muy significativo. Según esta cifra, y teniendo en cuenta que en Steam a principio de año había sobre unos [120 millones de usuarios actvos](https://www.vidaextra.com/pc/numeros-steam-2020-10-000-juegos-nuevos-120-millones-jugadores-20-ventas), en este momento los seguidores de Tux dentro de la plataforma **seríamos 1,2 millones de jugadores**, una cifra que como sabeis, a pesar de que el porcentaje se estanque, se incrementa, ya que así lo hace la base de jugadores total.


Desde mi punto de vista, es muy probable que, aunque no inmediatamente , el agua vuelva a su cauce y vuelva  bajar un poco. Hay que tener en cuenta que estamos en vacaciones, y la gente tiene más tiempo libre (recordad lo que pasó con el crecimiento de usuarios de GNU/Linux durante el confinamento), y debido al tirón de la Steam Deck, muchos se hayan puesto a probar como funcionan sus juegos favoritos en Linux para ver si la nueva "consola" dará la talla (que la dará), y luego vuelvan a Windows... o quizás me equivoque (ojalá) y vean las muchas bondades de nuestro sistema y vean que pueden disfrutar perfectamente de sus títulos sin problemas y decidan quedarse. Habrá que esperar al final de las vacaciones estivales para saberlo...Como veis Steam Deck trae cola, y sin estar en el mercado no para de crear titulares y dar que hablar, aunque mucho sea por ahora simplemente especulación.

