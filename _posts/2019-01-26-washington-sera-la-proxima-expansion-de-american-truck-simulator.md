---
author: leillo1975
category: "Simulaci\xF3n"
date: 2019-01-26 15:22:44
excerpt: "<p>@SCSsoftware lanzar\xE1 esta DLC en unos d\xEDas</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/31a4faa1393284941a0a77ff3faf2c17.webp
joomla_id: 959
joomla_url: washington-sera-la-proxima-expansion-de-american-truck-simulator
layout: post
tags:
- dlc
- american-truck-simulator
- ats
- scs-software
- expansion
title: "Washington ser\xE1 la pr\xF3xima expansi\xF3n de American Truck Simulator\
  \ (ACTUALIZADO)"
---
@SCSsoftware lanzará esta DLC en unos días

**ACTUALIZADO 5-6-19**: Muchisimo antes de lo esperado nos llegará esta nueva expansión de territorio para American Truck Simulator, y es que según acabamos de saber, será lanzada el próximo 11 de Junio, tal y como podemos ver en [su Blog](https://blog.scssoft.com/2019/06/washington-announcement.html). Como podeis ver en la noticia original (abajo) se han adelantado bastantes meses, pues normalmente hasta ahora estas llegaban en el último cuarto del año. Bienvenida sea pues... Entre las novedades que encontraremos encontraremos las siguientes:


*-3800 millas de densa red de carreteras*  
 *-16 nuevas ciudades (Vancouver, Longview, Aberdeen, Olympia, Tacoma, Seattle, Everett, Port Angeles, Bellingham, Omak, Wenatchee, Yakima, Kennewick, Grand Coulee, Spokane, Colville)*  
 *-Más de 20 puentes emblemáticos*  
 *-10 paradas de camiones originales*  
 *-Puerto de Keystone-Port Townsend Ferry*  
 *-Puente giratorio Spokane*  
 *-Presa de Grand Coulee*  
 *-Las emblemáticas montañas de Washington (Mount St. Helens y Mt. Rainier)*  
 *-Desafiante camino de acantilado*  
 *-Límites de velocidad dinámicos y señales LED*  
 *-Nuevas cargas únicas: motores de avión, camiones, barcos....*  
 *-Puertos grandes y realistas (Seattle, Tacoma, Everett)*  
 *-Muchas intersecciones únicas*  
 *-Diversos muelles de empresas: constructores de yates, grandes granjas, almacenes de materiales....*  
 *-Logros específicos de Washington para desbloquear*


Por supuesto, como siempre, en JugandoEnLinux os informaremos puntualmente de su salida, y si tenemos la oportunidad realizaremos un Streaming para mostrarosla en cuanto dispongamos de una copia para su revisión. Mientras esperamos impacientes por esta nueva DLC os dejamos con el trailer que acaban de publicar para mostrarnos los montones de novedades que vendrán con ella:


  
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/rfMXLvxqgoI" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 




---


**NOTICIA ORIGINAL**: Como muchos de los aficionados a este siempre mágnífico y espectacular simulador predijimos, [el estado de Washington será la próxima adición](https://blog.scssoft.com/2019/01/american-truck-simulator-washington.html) al ya "crecidito" mapa de American Truck Simulator. Hace poco más de una semana, la desarrolladora checa [publicaba como pistas unas cuantas capturas de pantalla](https://blog.scssoft.com/2019/01/ats-teasing.html) a modo de adivinanza para su multitud de seguidores, y en seguida muchos de ellos reconocieron los paisajes y lugares allí representados. Confirmado en el día de ayer que se trataba de el "**Evergreen State**", parece claro que SCS continua por ahora con la mira puesta en el noroeste de los Estados Unidos, tal y como lo hizo hace bien poco con su exitosa [DCL Oregon](index.php/homepage/analisis/20-analisis/998-analisis-american-truck-simulator-dlc-oregon).


Esta nueva expansión no tiene todavía una fecha definitiva de salida y tan solo han dicho que **llegará a finales de año**, por lo que se supone que lo veremos entre Octubre y Noviembre de este año, tal y como está siendo habitual si analizamos las fechas de salida de [Nuevo México](index.php/homepage/analisis/item/656-analisis-american-truck-simulator-dlc-new-mexico) y la antes comentada Oregon. Aun no han proporcionado información de lo que encontraremos en este estado, pero es de suponer que recreará a la perfección las principales características de sus lugares, por lo que encontraremos tal y como reza su lema, el verde por doquier, grandes bosques de coniferas, lagos, rios...


A espera de más detalles os dejamos con el video del anuncio donde podemos ver **la ciudad más grande del estado, Seattle**, y de fondo los conocidos acordes de "Smells Like Teen Spirit" de [Nirvana](https://www.nirvana.com/), probablemente el más conocido grupo musical de esta ciudad, (con permiso de [Queensrÿche](https://es.wikipedia.org/wiki/Queensr%C3%BFche)):


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/MyPXE4TWvug" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


¿Qué os parece la elección de Washington como su siguiente expansión? ¿Disfrutais de American Truck Simulator? Contesta en los comentarios o charla con nosotros en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

