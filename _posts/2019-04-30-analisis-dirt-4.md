---
author: leillo1975
category: "An\xE1lisis"
date: 2019-04-30 15:09:17
excerpt: "<p>Acomp\xE1\xF1anos repasando todas las caracter\xEDsticas del nuevo juego\
  \ de @feralgames</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/efa01eaff4368b269c4a14f272a05fbe.webp
joomla_id: 1021
joomla_url: analisis-dirt-4
layout: post
tags:
- vulkan
- analisis
- feral
- codemasters
- dirt-4
- rallycross
- landrush
- joyride
title: "An\xE1lisis: DIRT 4."
---
Acompáñanos repasando todas las características del nuevo juego de @feralgames

Admitámoslo, no es el juego que todos estábamos esperando. Después del **exito arrollador que tuvo en nuestra comunidad el genial DIRT Rally**, cuando [Feral Interactive](https://www.feralinteractive.com/es/) anunció que portaría **DIRT 4**, muchos de nosotros nos sentimos un tanto decepcionados, en primer lugar por tratarse por un juego que tiene un bagaje de 2 años a sus espaldas y llega un tanto tarde; y en segundo lugar por que muchos de los que nos gustan los juegos de conducción queríamos una experiencia más realista y que siguiera la estela de DIRT Rally, es decir, su segunda parte.


Y es que como os decimos DIRT Rally marca mucho, mucho , y ha puesto las cosas difíciles a [**Codemasters**](http://www.codemasters.com/) para poder superarse, pero tenemos que ser claros con algo, y puede que muchos de vosotros no lo sepais. **DIRT no es exactamente una saga de juegos de conducción**. **Más bien son 3**, ya que si haceis un poco de memoria todo comenzó hace más de 10 años con "Colin McRae: DIRT", continuando con su segunda parte y luego llamándose DIRT 3 a secas. En el año 2012 vivimos la llegada de un "spin off" llamado "**DIRT Showdown**", juego que tomaba una vertiente mucho más arcade y alocada, y que los Linuxeros podemos disfrutar gracias al trabajo excelente que hizo en su día Virtual Programming portándolo. Por último llegaría [DIRT Rally](https://www.humblebundle.com/store/dirt-rally?partner=jugandoenlinux), que cambiaría totalmente de tercio ofreciéndonos un simulador realista y exigente que haría las delicias de muchos ([Analizado en nuestra web](index.php/homepage/analisis/20-analisis/362-analisis-dirt-rally)). DIRT 4 continua lo visto en los primeros juegos, siendo continuación de DIRT 3, y por lo tanto estando en un punto intermedio entre DIRT Showdown y DIRT Rally. El juego fué lanzado al mercado en Windows y consolas a principios del verano de 2017, y  el [anuncio oficial](index.php/homepage/generos/carreras/2-carreras/1047-feral-desvela-el-misterio-de-su-ultimo-port-dirt-4-correra-en-linux-steamos-actualizado) de su llegada a Linux se realizó el pasado Diciembre, siendo su [lanzamiento](index.php/homepage/generos/carreras/2-carreras/1128-disponible-dirt-4-en-linux-steamos) hace menos de un mes, concretamente el 28 de Marzo.


![DIRT4 Michiganb](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/DIRT4/analisis/DIRT4_Michiganb.webp)


*Michigan, en los EEUU, será una de las nuevas localizaciones*


Aclarado esto empezamos con el análisis propiamente dicho de este juego.  Lo primero que nos llama la atención, y siendo a nuestro parecer uno de los puntos fuertes del juego, es que nada más empezar, tras crear nuestro perfil, **tendremos que escoger como queremos conducir nuestros coches**, teniendo **dos opciones diferenciadas**, la "**Gamer**", enfocada a los que prefieren **diversión inmediata** y pensada para jugar con gamepad; y "**Simulación**", que como todos imaginais está pensado para quien **busca experiencias más realistas** y su uso con Volante. Escoger uno u otro modo condicionará el desarrollo del juego, pero podremos combiarlo en cualquier momento. Es importante decir que elegir uno u otro modo de control no afecta a la dificultad del juego, ya que el reto de la competición lo podremos ajustar independientemente de ello. Si tuviésemos que hacer una analogía podríamos decir que **el modo "Simulación" se parece a lo que encontramos en DIRT Rally, y el modo "Gamer" a lo visto en DIRT 3**. Esta dualidad lo hace especialmente perfecto para abarcar un espectro mucho más amplio de jugadores , o de permitirnos disfrutar de dos formas diferentes del mismo juego, lo cual es un punto muy importante a su favor.


Inmediatamente después de haber escogido el modo de control nos pondrá inmediatamente a los mandos de un Ford Fiesta para expliacarnos lo más básico del juego en una sencilla etapa situada en Australia. Nada más terminar, **según nuestra habilidad conduciendo nos popondrá un nivel de dificultad**, que podremos cambiar entre 3 disponibles y uno por desbloquear. También **podemos editar las opciones de dificultad manualmente** si no encontramos un nivel que se ajuste a nuestras preferencias. Nada más escoger la dificultad nos enviará a la "**Academia DIRT**" donde podremos practicar múltiples técnicas de conducción mediante sencillos tutoriales. Una vez terminamos el primero, podremos continuar aprendiendo o comenzar nuestra carrera saliendo al sencillo pero completo Menú Principal.


![DIRT4 TarragonaPueblob](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/DIRT4/analisis/DIRT4_TarragonaPueblob.webp "img-responsive")


*Uno de los momentos más vistosos del juego será cuando atravesemos los pueblos en Tarragona*


El primer apartado del menú es "**Pruebas**", y en el podremos acceder a todos los modos de competición disponibles en el juego. Comenzamos pues con el **modo "Carrera"** que conjuntamente con el "Multijugador" conforman el núcleo duro del juego. En encontraremos las 4 diferentes disciplinas que encontraremos en el juego, comenzando por el **Rally** , que es tal y como lo imaginamos y muy parecido a lo que vimos en DIRT Rally; y en el que se nos irán proponiendo diferentes pruebas enmarcadas en **Australia, Tarragona, Gales, Míchigan  y Suecia,** de forma que podremos disfrutar de todas las superficies posibles como **el barro, el asfalto, la nieve, la tierra y la gravilla**, por lo que **tendremos que aprender a adaptarnos tanto nuestra conducción como nuestro coche a cada una de ellas**. También es importante resaltar que **los fenómenos meteorológicos influirán en gran medida** en nuestra forma de conducir, siendo especialmente reseñable la inclusión de la niebla, que nos hará perder la visibilidad del tramo y complicará mucho las cosas.


Como es lógico comenzaremos en categorias inferiores con un nivel de adversarios bastante bajo que irá incrementándese al igual que la dificultad de las etapas que disputaremos. Antes de cada etapa **podremos modificar muchos elementos de nuestro coche como suspensiones, frenos, caja de cambios...** y reparar las posibles averías y desperfectos que tengamos. También **será posible mejorar las piezas para obterner mejor rendimiento** en nuestro vehículo. Una de las novedades que encontraremos en este DIRT 4 y que no hemos visto en anteriores entregas ni en DIRT Rally, es que cuando lleguemos a meta tendremos que conducir hasta la posición del comisario para que termine la etapa, lo cual incrementa un poco más la inmersión y el realismo en el juego.


![DIRT4 setupb](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/DIRT4/analisis/DIRT4_setupb.webp)


*Por supuesto podremos configurar multitud de aspectos mecánicos de nuestros coches para sacarles más partido en las diferentes etapas*


A medida que vayamos ganando carreras **iremos ganando tanto reputación como creditos** que nos servirán para pagar las reparaciones, como los gastos generados por el equipo en que militemos. Una vez que hayamos reunido el suficiente dinero, **podremos crear nuestro propio equipo**, comenzando por la compra de nuestro primer coche en el **menú "Vehículos"**, y pudiendo optar tanto por coches nuevos como usados. La variedad de estos es enorme, más de 50, encontrándolos tanto para todas los modos del juego, como las difierentes categorias, encontrando desde sencillos compactos a potentes 4x4, pasando por icónicos vehículos históricos y buggies.


 En el menú "**Mi equipo**" podremos configurar desde el **diseño y colores de nuestro equipo**, a los **patrocinadores**, así como todas las **mejoras disponibles** que podrán afectar al funcionamiento de nuestro equipo en el modo carrera. Adquirir estas mejoras es uno de los aspectos que hace mucho más atractivo a DIRT 4 y que nos invita a seguir avanzando para conseguir más patrocinadores, poder adquirir mejores piezas para nuestros coches, aumentar el personal o mejorar las condiciones de estos para que hagan mejor su trabajo.


![DIRT4 Equipob](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/DIRT4/analisis/DIRT4_Equipob.webp)


*En la pantalla de equipo podremos editar la apariencia del equipo y elegir nuestros patrocinadores*


Al igual que en DIRT Rally, también **podremos competir de tu a tu con otros competidores** en divertidas y disputadas carreras de "**RallyCross**"  contando con cinco escenarios diferentes **Lohéac Bretagne, Montealegre, Lydden Hill, Holjes y Hell**. Gracias a que esta modalidad de juego cuenta con la **licencia oficial de la FIA World RallyCross**, podremos competir contra pilotos reales como el conocido Sebastien Loeb. En estas competiciones será muy habitual el chocar con el resto de coches y realizar arriesgados adelantamientos que pondrán a prueba nuestros nervios, reflejos y destreza para evitar comenter errores que nos hagan perder posiciones. Si habeis jugado a este modo en DIRT Rally sabreis que tendreis que hacer una **vuelta comodín** en cada ronda por un desvio en el trazado, pudiendo pasar por esta zona en cualquier vuelta.  Al igual que en el resto de modos habrá distintas categorias de coches para competir, desde **vehículos de los 80**, a los más modernos y potentes vehículos actuales, pasando por los ágiles **crosskarts**.


 ![DIRT4 Rallycrossb](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/DIRT4/analisis/DIRT4_Rallycrossb.webp)


*Con el Rallycross disputaremos de divertidas y frenéticas carreras contra otros coches, siendo en el apartado Multijugador donde más disfrutaremos*


Otro modo que no debemos olvidad es el **LandRush**, el el que disputaremos las más salvajes carreras en **Buggies, Crosskarts** y **camiones** en circuitos cerrados de tierra donde los baches, saltos, choques y derrapes nos harán morder el polvo en tres escenarios diferentes, **Baja** en México, y **California** y **Nevada** en los Estados Unidos. Este modo **está quizás mucho más pensado para el modo de juego "Gamer"**, ya que el control en modo "Simulación" es un tanto exigente y cuesta hacerse con él sin pasarse derrapando en las curvas. Este modo quizás se queda un poco corto en cuanto a los circuitos y realmente se echa de menos que tan solo haya tres. Aún así, es claramente un **aliciente extra para la adquisición** de este juego, ya que cambia las foma de conducción completamente, además de ser un elemento diferenciador con respecto a DIRT Rally, que no lo incluía en favor de las etapas de montalla de Pikes Peak. Por supuesto, no debemos olvidarnos de **modo de Rally Clásíco**, que basicamente es igual que el de Rally normal pero con **icónicos coches antiguos** como el Fiat 131 Abarth o el incombustible Mini, una razón más especialmente para los que disfrutamos de la condución virtual de estas joyas del automobilismo de épocas pasadas.


![DIRT4 LandRushb](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/DIRT4/analisis/DIRT4_LandRushb.webp)


*Al contrario que en DIRT Rally no tenemos Hillclimb, pero si el frenético y polvoriento Landrush*


Otro de los aspectos más destacables de DIRT 4 es la utilidad "**Your Stage**" que te permitirá **crear tus propias etapas** tanto de Rally normal como Clásico. En ellas se generarán tramos aleatoriamente pudiendo nosotros escoger la localización, longitud y complejidad del trazado, además de las condiciones climáticas y la hora del día. Una vez generadas **podemos agruparlas en un campeonato, y si nos sentimos orgullosos de nuestra creación, compartirlas en linea** para desafiar a otros jugadores. Hay que decir que en ningún momento hemos notado "artificialidad" en los escenarios autogenerados, concatenandose las secciones del tramo de una forma completamente creible sin que se note para nada que no están prediseñadas con anterioridad por una mano humana. Gracias a esta función del juego es posible tener etapas infinitas que representen un desafío constante para nuestras ganas de conducir.


Como colofón a todos los diferentes modos de los que disponemos en DIRT 4, no debemos dejar atrás a "**Joyride**", un modo que nos recuerda ligeramente lo que vimos en su día en **DIRT Showdown** aunque con bastantes diferencias. En él viajaremos a la academa DIrtfish en Estados Unidos, el mismo lugar del que hablamos al principio de este análisis. En este modo tendremos dos tipos de pruebas diferentes. En la primera, **Smash Attack**, tendremos que destrozar cajas en un tiempo determinado. En la segunda, **Time Attack**, tendremos que recoger todos los iconos verdes del escenario en el menor tiempo posible, y con cuidado de no tocar los iconos rojos que nos añadirán más tiempo a nuestro contador. Habrá un total de 60 pruebas diferentes donde tendremos que usar diferentes coches en variados recorridos. Este modo ofrece un una experiencia desenfadada y muy festiva que nos servirá para cambiar de tercio cuando queramos "desengancharnos" del resto de modos.


![DIRT4 Joyrideb](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/DIRT4/analisis/DIRT4_Joyrideb.webp)


*En el modo Joyride disfrutaremos de la vertiente más arcade del juego con las más divertidas pruebas*


Algo que no debemos dejar de lado es el **modo Online** del juego, que es **uno de los platos fuertes del juego**. Este se subdivide en dos partes bien diferencidas. La primera, **Multijugador**, nos permitirá jugar con otras personas a través de internet en todos los modos posibles del juego, es decir, en Rally, LandRush, Rallycross y Rally Clásico. Podremos tanto unirnos a las diferentes partidas, como crear nuestras propias pruebas personalizando todos los aspectos, desde la localización a las condiciones meteorológicas, pasando por el tipo de conducción y las ayudas. En este modo **es destacable el modo Rallycross** que a nuestro juicio resulta el más disputado y divertido. En segundo lugar nos encontramos con el modo **Competitivo ,** que se divide a su vez en  "**Eventos de Comunidad**", que son pruebas diarias, semanales o mensuales donde según nuestros resultados mejoraremos la experiencia y obtendremos créditos gratis;  y "**Pro Tour**", una especie de Liga donde tendremos que ganar puntos para subir de categoría y así poco a poco enfrentarnos a competidores más expertos. Como suele ser habitual en este tipo de videojuegos, este apartado proporciona una fuente inagotable de horas de juego que **aumenta el valor añadido del juego**. Durante las partidas que jugamos no encontramos ningún problema de conexión y todo funcionó de forma correcta.


 En cuanto al apartado técnico el juego presenta un **sistema de daños realista**, que cuando chocamos, volcamos o nos salimos de la carretera, **afecta al funcionamiento del coche** y nos obliga en ciertos momentos a parar para repararlo, perdiendo en esa situación unos valiosos segundos, como por ejemplo cambiando una rueda. Estos daños **nos harán perder también tiempo y dinero entre etapa y etapa**, por lo que debemos minimizarlos si no queremos salir perjudicados. También es especialmente reseñable la posibilidad de que tengamos **averias aleatorias** tales como pinchazos, averías en la caja de cambios o pérdida de potencia en el motor, algo que nos afectará sensiblemente en el rendimiento del coche.


![DIRT4 Tarragonab](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/DIRT4/analisis/DIRT4_Tarragonab.webp)


*El SEAT Ibiza Kitcar, uno de los coches más divertidos del juego*


El juego, como es de esperar presenta **multitud de vistas** para que podamos elegir según nuestras preferencias o momento del juego. Existirán **dos vistas exteriores** (más o menos cerca) especialmente indicadas para una conducción más "Gamer" (arcade) , y también **cuatro vistas subjetivas** (morro, parabrisas, cuadro y conductor), siendo estas más pensadas para el modo "Simulación". Podremos recrearnos visualizando nuestros mejores tramos con las repeticiones que ofrecen multitud de opciones y puntos de vista. **Graficamente el juego supera con claridad a DIRT Rally.** El motor EGO, que en esta ocasión es el 4.0, se comporta de forma excelente encontrando unas texturas mucho más definidas, un público más detallado, y unos efectos de iluminación y meteorológicos superiores, tales como la lluvia (donde quizás hemos encontrado más diferencia), la nieve y la niebla que combinados con el día y la noche otorgan más realismo al título sin sacrificar en demasía el fantástico rendimiento que tiene el juego. Como sabreis, **Feral ha usado una vez más la API Vulkan para la renderización**, lo que facilita mucho que **el juego tenga unas buenas tasas de frames** en gráficas corrientes y se comporte de una forma bastante estable. Claramente se nota que la compañía británica tiene más que controlado su uso y por supuesto lo que es trabajar sobre juegos creados con el motor de Codemasters, lo que le da una solvencia en su trabajo que garantiza la calidad del port.


En el apartado sonoro, como es habitual en los productos de Codemasters, se cuida hasta el detalle todos sus aspectos. Los efectos sonoros como el público, los golpes, los derrapes o la lluvia son excelentes. Por supuesto **el sonido de los motores es sobresaliente como en anteriores entregas**, y varía según las vistas que utilicemos. El juego, además de los textos. se muestra **completamente doblado al castellano**, con unas voces de calidad. Un aspecto muy importante son los **comentarios del copiloto**, que están mucho mejor realizados que en DIRT Rally. donde eran comunes los errores; aunque en algunos momentos , cuando hay varias indicaciones seguidas estos se superponen o no llegan a tiempo. La selección musical le queda como anillo al dedo y es de primera linea, pudiendo escuchar temas **Bastille**, **Queens of the Stone Age** o **The Chemical Brothers.**


![DIRT4 Australia b](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/DIRT4/analisis/DIRT4_Australia_b.webp)


*El paisaje de Australia con sus eucaliptos y su tierra rojiza es inconfundible*


Si hablamos de los **dispositivos de control**, obviamente podremos usar el teclado y los gamepads sin ningún problema, pero como es habitual en este tipo de juegos lo suyo es disfrutarlo con un Volante. En nuestro caso hemos usando un Logitech G29, volante que ofrece una magnífica compatibilidad en nuestro sistema y que ofrece una calidad y sensaciones inmejorables. Hay que decir que el juego **permite desde el lanzador modificar los grados de giro**, siendo lo más adecuado un valor entre 300 y 400 grados. Por supuesto podremos disfrutar del **Force Feedback**, algo indispensable para una buena experiencia. En este aspecto da la impresión que **quizás Dirt Rally tiene unas mejores sensaciones**, con una respuesta más realista, dando la impresión que en esta ocasión es más suave y no tan ruda.


En cuanto a los puntos negativos del juego lo único que se le puede achacar, a parte de lo antes comentado sobre el copiloto,  es como comentamos antes que no tenga más escenarios en todas las modalidades, especialmente en LandRush y Rallycross, donde podría haber más localizaciones.


A modo de conclusión, Feral Interactive ha apostado por un valor seguro, y en este caso, tras jugarlo bastante y viéndolo con perspectiva no se ha equivocado ni un pelo. DIRT 4 **reune todo lo necesario para contentar tanto a los que buscan pasar un buen rato** **como a los más exigentes**, y todo ello aderezado con una variedad y **más progresividad que DIRT Rally**. Si te gustan los juegos de coches no lo dudes, porque DIRT 4 no te va a decepcionar, tanto por su multitud de modos de juego como por las sensaciones que produce. Si al principio dudábamos, ahora en JugandoEnLinux.com, os recomendamos sin objeciones este título, pues os va a comer horas y horas de juego y os hará disfrutar al máximo.


Podeis compar DIRT 4 para Linux en la [tienda de Feral](https://store.feralinteractive.com/es/mac-linux-games/dirt4/) (recomendado), en la **[Humble Store (patrocinado)](https://www.humblebundle.com/store/dirt-4?partner=jugandoenlinux)**, o e [Steam](https://store.steampowered.com/app/421020/DiRT_4/). Os dejamos en compañía del video que grabamos estrenando el juego el día de su salida en nuestro sistema:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/Z9aqpaxMl3M" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


 ¿Habeis jugado ya a DIRT 4? ¿Estais de acuerdo con nuestro análisis? Déjanos tus opiniones sobre él en los comentarios, o charla sobre DIRT 4 en nuestro canal de [Telegram](https://t.me/jugandoenlinux).

