---
author: Pato
category: Estrategia
date: 2021-07-15 10:01:31
excerpt: "<p>La expansi\xF3n nos llega, como no, de la mano de @feralgames que tambi\xE9\
  n nos traer\xE1 Total War: WARHAMMER III</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/TotalWarWarHammerII/warhammer-2-silence-and-fury-1.webp
joomla_id: 1325
joomla_url: ya-disponible-el-dlc-total-war-warhammer-ii-the-silence-the-fury
layout: post
tags:
- feral-interactive
- estrategia
- sega
- total-war
title: 'Ya disponible el DLC Total War: WARHAMMER II - The Silence & The Fury'
---
La expansión nos llega, como no, de la mano de @feralgames que también nos traerá Total War: WARHAMMER III


Ayer mismo fue lanzada la última expansión de la saga **Total War: WARHAMMER II** con el título "**The Silence & The Fury**" en la que obtendremos a dos nuevos señores para los "Hombres Lagarto" y los "Hombres Bestia". Cada uno lidera su propia facción e introduce unidades y personajes nuevos, así como mecánicas de juego y objetivos narrativos únicos.



> 
> *Taurox, el Minotauro de la Condenación lleno de rabia y hecho de bronce vivo, es prácticamente invencible, salvo por una zona de su gigantesco cuello. Los Dioses del Caos le susurran que hay un ritual que puede eliminar esta debilidad, pero las promesas de los Poderes Ruinosos rara vez son lo que parecen...  
> Por su parte, Oxyotl, el venerado Eslizón Camaleón y maestro del sigilo, detecta las maquinaciones del Caos y reúne a sus cohortes. Hay que detener a Taurox a toda costa, no sea que una nueva marea de Caos arrase el mundo. Cuando el silencio y la furia se encuentren en una batalla final, ¿cuál de ellos prevalecerá?*
> 
> 
> 


 <div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/sUWOU7o6v3Q" title="YouTube video player" width="560"></iframe></div>


En concreto, para los Hombres Bestia llega **Taurox**, un señor que obtiene Impulso con cada batalla ganada, lo que permite a su ejército reponer sus puntos de acción para seguir luchando sin parar. Además, las nuevas unidades:


* **Señor Minotauro de la Condenación**
* **Héroe Beligor**
* **Carro de Tuskgors**
* **Gorgona**
* **Escuerzo Alado**


Por parte de los Hombres Lagarto tendremos a Oxyotl, un Eslizón Camaleón que gracias a su clarividencia le permite detectar donde atacarán las fuerzas del Caos, puede elegir a qué amenazas enfrentarse y obtener las recompensas de la victoria. Además, Oxyotl puede viajar instantáneamente entre su capital, sus zonas de misión exclusivas y cualquiera de los Santuarios Silenciosos que haya vuelto a descubrir.


En cuanto a las nuevas unidades de los Hombres Lagarto tendremos:


* **Héroe Oráculo Eslizón**
* **Acechadores Camaleón**
* **Coatl**
* **Troglodón Salvaje**


Todo esto, aderezado con regimientos de renombre y mucho más. Tienes toda la información en la página del DLC en Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/1556110/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


Por último, mencionar que **Feral Interactive** nos traerá Total War: WARHAMMER III como ya anunciamos en su momento.  
Puedes verlo en la web de Feral [en este enlace](https://www.feralinteractive.com/es/upcoming/).


¿Juegas al universo Total War: WARHAMMER II? ¿Que te parecen las novedades y la próxima llegada de la tercera parte?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

