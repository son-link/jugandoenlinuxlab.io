---
author: leillo1975
category: Estrategia
date: 2019-07-22 14:35:15
excerpt: <p>@feralgames actualiza este veterano juego de Estrategia en tiempo real</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/2940b97ab5721a9e279f736f864f4ce6.webp
joomla_id: 1078
joomla_url: lanzada-la-commanders-update-de-company-of-heroes-2
layout: post
tags:
- rts
- feral-interactive
- actualizacion
- sega
- company-of-heroes-2
- relic
- coh2
title: Lanzada la "Commanders Update" de "Company Of Heroes 2".
---
@feralgames actualiza este veterano juego de Estrategia en tiempo real

Algo más de un mes más tarde después de ser lanzada en Windows, nos llega esta actualización para uno de los mejores juegos de **Estrategia en tiempo real** en el mercado, Company of Heroes. Como sabeis este título fué uno de los primeros (2015) y más exitosos ports de la bendita Feral Interactive, y desde entonces ha sido fuente de numerosas DLC's y actualizaciones.  Hace escasos minutos recibíamos el anuncio en nuestro correo a través del departamento de prensa y también en las redes sociales como podeis ver en este tweet:



> 
> Company of Heroes 2 for macOS and Linux has been reinforced with five new community-driven Commanders. Update your game now!  
>   
> Advance to the game’s official blog for more intel: <https://t.co/wKnaPH8lY3> [pic.twitter.com/N3VwVmsTsK](https://t.co/N3VwVmsTsK)
> 
> 
> — Feral Interactive (@feralgames) [22 de julio de 2019](https://twitter.com/feralgames/status/1153309986686152704?ref_src=twsrc%5Etfw)


  





El juego que fué desarrollado originalmente por Relic Entertaiment y publicado por SEGA, es renovado una vez más con [importantes cambios](http://www.companyofheroes.com/blog/2019/06/12/new-commander-update-live-now), entre las que destaca principalmente la inclusión de **5 nuevos comandantes**, cada uno conectado con su propia Doctrina, Regimiento, Compañía o Ejército y por supuesto con sus propias características, como puedes ver aquí abajo:


*-**Tácticas Aerotransportadas Soviéticas**: Las tropas tienen potencia de fuego adicional y apoyo aéreo flexible.*  
 *-**Doctrina de la Reserva Estratégica de la Wehrmacht**: Infantería agresiva respaldada con armadura de élite.*   
 *-**Gran Doctrina Ofensiva del *Oberkommando Oeste***: Una composición de infantería poderosa y flexible.*   
 *-**Compañía de Asalto Urbano de las Fuerzas Estadounidenses**: Los jugadores pueden desarraigar las defensas enemigas y equipar a la armadura especializada.*   
 *-**Regimiento de Asalto ***Británico*** de Préstamo y Arrendamiento** : Las tropas pueden usar una variedad de equipo americano.*


Pero la cosa no se queda unicamente ahí, ya que **se han invertido montones de horas en testeo, balanceamiento y arreglos varios**, como podeis ver en el su extensa [Lista de Cambios](https://community.companyofheroes.com/discussion/67/coh-2-changelog/p6). Si teneis el juego instalado en vuestros equipos vereis como se actualizará automáticamente, sinó teneis una buena oportunidad de "desempolvarlo" y probar las nuevas caracteristicas. Por supuesto, si aún no lo teneis podeis haceros con él en la [tienda de Feral](https://store.feralinteractive.com/en/mac-linux-games/companyofheroes2/) o en la [Humble Store](https://www.humblebundle.com/store/company-of-heroes-2?partner=jugandoenlinux) (patrocinado). Os dejamos con el trailer de lanzamiento del juego:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/Ur5ETQ9A5Hc" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


¿Has jugado ya a Company Of Heroes 2? ¿Qué te parece este ya "clásico" de la RTS? Cuéntanoslo en los comentarios o deja tus mensajes en nuestra comunidad de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

