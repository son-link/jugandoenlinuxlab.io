---
author: leillo1975
category: "Acci\xF3n"
date: 2017-07-21 07:35:00
excerpt: <p>Corred insensatos!!!!</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/25da67824c9f1869e8ef3eacb5d2ced0.webp
joomla_id: 417
joomla_url: shadow-warrior-special-edition-gratis-en-humble-store-por-tiempo-limitado
layout: post
tags:
- gratis
- shadow-warrior
- humble-store
- tiempo-limitado
title: 'Shadow Warrior: Special Edition gratis en Humble Store por tiempo limitado'
---
Corred insensatos!!!!

Bueno, **por si queda alguien que todavía no se haya enterado**, y que no tenga esta juego, [**la conocida tienda Humble Store está regalando el juego Shadow Warrior: Special Edition**](https://www.humblebundle.com/store/shadow-warrior-special-edition), un juegazo de acción que no te debes perder. En este momento quedan 1 día y 9 horas para poder adquirirlo gratis. por lo que si no lo tienes debes pasar a recogerlo antes de mañana Sabado a las 19:00 (hora Española).


 


Para quien no lo conozca, Shadow Warrior: Special Edition es un remake del conocido juego clásico de 1997, desarrollado por los polacos [Flying Wild Hog](http://flyingwildhog.com/) y editado por [Devolver Digital](https://www.devolverdigital.com/), fué portado a nuestro sistema por [Knockout Games](http://www.knockoutgames.co/) con muy buenos resultados. En el juego deberemos derrotar a hordas de demonios haciendo uso de nuestra Katana, todo tipo de armas y ataques mágicos. El juego tiene unos gráficos y arte impresionantes, y se caracteriza por la acción frenética y unas buenas dosis de humor. Podeis ver el trailer de lanzamiento aqui:


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/sUfTNLIZlyo" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 ¿Has jugado ya a Shadow Warrior? ¿Que te parece este juego? Deja tu opinión en los comentarios o en nuestro canal de [Telegram](https://t.me/jugandoenlinux).


 


 

