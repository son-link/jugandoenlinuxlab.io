---
author: Serjor
category: Web
date: 2017-11-20 22:31:58
excerpt: "<p>Continuamos con las partidas tem\xE1ticas</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/cb814f6646368a9340b7ed30aa1a9ed7.webp
joomla_id: 546
joomla_url: este-viernes-en-la-partida-con-la-comunidad-especial-juegos-2d
layout: post
tags:
- comunidad
- 2d
- eventos
title: 'Este viernes en la partida con la comunidad: Especial juegos 2D'
---
Continuamos con las partidas temáticas

Si la [semana pasada](index.php/homepage/web/item/655-este-viernes-en-la-partida-de-la-comunidad-especial-juegos-cooperativos) las partidas de la comunidad se las dedicábamos a los FPS cooperativos, esta semana os proponemos uniros a nosotros a través de juegos que buscan la profundidad en sus mecánicas o historias, porque en esta ocasión todas las opciones propuestas en la encuesta del grupo de [Telegram](https://telegram.me/jugandoenlinux) son juegos 2D.


Y los contendientes son:


Battle for Wesnoth, que podréis encontrar en los repositorios de vuestra distro, en [Lutris](https://lutris.net/games/the-battle-for-wesnoth/) o en la [página oficial](https://www.wesnoth.org/)


CS2D


<div class="steam-iframe"><iframe height="190 " src="https://store.steampowered.com/widget/666220/" style="border-style: none;" width="646"></iframe></div>


Teeworlds


<div class="steam-iframe"><iframe height="190 " src="https://store.steampowered.com/widget/380840/" style="border-style: none;" width="646"></iframe></div>


Hedgewars, también lo podéis encontrar en los repositorios o su [página oficial](https://www.hedgewars.org/)


Skullgirls


<div class="steam-iframe"><iframe height="190 " src="https://store.steampowered.com/widget/245170/31087/" style="border-style: none;" width="646"></iframe></div>


Terraria


<div class="steam-iframe"><iframe height="190 " src="https://store.steampowered.com/widget/105600/8183/" style="border-style: none;" width="646"></iframe></div>


Como veis juegos de gustos variados, con alternativas open source y que además esperamos que no consuman grandes recursos y así hasta los ordenadores con menos prestaciones también puedan tomar parte.


Os recordamos que si queréis participar en la votación tendréis que pasaros por el canal de [Telegram](https://telegram.me/jugandoenlinux), e ir al mensaje que está anclado, y que tenéis hasta este miércoles para participar.


Y sino os emplazamos al próximo viernes a las 22:30 hora peninsular, donde nos encontraremos en el chat de voz del canal de [Discord](https://discord.gg/ftcmBjD).


Además, de cara a que todo vaya lo más ágil posible, os recomendamos que tengáis el juego ganador instalado y probado para el evento por si surgiera algún problema técnico, poder haberlo solucionado con anterioridad y que no os quedéis fuera.


Y si no podéis asistir, intentaremos que haya una retransmisión, ya sea por el canal de [Twitch](https://www.twitch.tv/jugandoenlinux) o por el de [YouTube](https://www.youtube.com/channel/UC4FQomVeKlE-KEd3Wh2B3Xw).

