---
author: Pato
category: "Acci\xF3n"
date: 2018-02-20 17:24:25
excerpt: "<p>El juego de rol y acci\xF3n \"pixel-art\" est\xE1 excelentemente valorado\
  \ en Steam</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/4b40c73d585255a6299314146674b253.webp
joomla_id: 654
joomla_url: ya-esta-disponible-momodora-reverie-under-the-moonlight-en-linux-steamos
layout: post
tags:
- accion
- rol
- metroidvania
- pixel-art
title: "Ya est\xE1 disponible 'Momodora: Reverie Under The Moonlight' en Linux/SteamOS"
---
El juego de rol y acción "pixel-art" está excelentemente valorado en Steam

Hablamos de un juego que se nos ha pasado por alto hasta ahora. Este 'Momodora: Reverie Under The Moonlight' es la cuarta entrega (precuela de los anteriores títulos, mas bien) de un juego de rol y acción de tipo "metroidvania" con un estilo inconfundible. El pasado día 16 aprovechando las rebajas del nuevo año chino los chicos de Bombservice [anunciaron que habían lanzado](http://steamcommunity.com/games/428550/announcements/detail/2914263157609891005) una versión del juego para nuestro sistema favorito que nos había pasado inadvertida.


*En "Momodora: Reverie Under the Moonlight" explorarás una tierra maldita en plena decadencia. El mal se propaga, los muertos se alzan y la corrupción reina. La esperanza es un recuerdo lejano para todos, excepto una sacerdotisa llamada Kaho, venida de la aldea de Lun. Una audiencia con su majestad, la reina, posiblemente salvaría las tierras--pero el tiempo escasea y cada noche es mas oscura que la anterior.*


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/9wJI_VjFqlg" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 'Momodora: Reverie Under The Moonlight' es un juego de plataformas 2D enfocado a la acción, con combos cuerpo a cuerpo, mecánicas de esquive y abundantes hechizos y objetos para usar contra tus enemigos. Entre sus características principales encontramos:


* Gráficos bellamente animados
* Gameplay excitante lleno de acción - combea, esquiva y ensarta a los enemigos con flechas
* Intensas batallas contra jefes con retantes patrones de movimiento
* Modos de dificultad apropiados para todos, desde los que quieren disfrutar de la historia hasta los entusiastas de la acción
* Una amplitud de objetos que pueden ser combinados para crear nuevas estrategias
* Variedad de mundos por explorar, llenos de secretos y tesoros
* Excéntricos aliados y enemigos con personalidades irresistibles
* Misterioso y rico trasfondo de la historia en eventos, diálogos y descripciones de los objetos


La editora del juego, Playism es ya conocida pues nos ha traído otros títulos como '[La Mulana](http://store.steampowered.com/app/230700/LaMulana/)' o '[Detention](http://store.steampowered.com/app/555220/Detention/)' (del que hablaremos en otra ocasión). Esperemos que este 'Momodora: Reverie Under The Moonlight' sea lo suficientemente exitoso en nuestro sistema para que tanto Momodora III como los próximos desarrollos del estudio acaben llegando a GNU-Linux/SteamOS.


Si te gustan los juegos de rol y acción con un buen apartado "pixel-art" y una dificultad ajustada, puedes encontrar este Momodora: Reverie Under The Moonlight en español en su página de Steam con un 40% de descuento en estos momentos:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/428550/88600/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 ¿Qué te parece este Momodora: Reverie Under The Moonlight? ¿Te gustaría que nos llegaran mas juegos de este tipo?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 

