---
author: Odin
category: Software
date: 2022-10-13 10:56:19
excerpt: "<p>El equipo de&nbsp;<span class=\"css-901oao css-16my406 r-poiln3 r-bcqeeo\
  \ r-qvutc0\">@batocera_linux</span> ha publicado una versi\xF3n Beta de su excelente\
  \ distribuci\xF3n de Linux.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Batocera/batocera.webp
joomla_id: 1494
joomla_url: batocera-ya-disponible-para-tu-steam-deck
layout: post
tags:
- emulador
- retro
- steam-deck
- batocera
title: Batocera ya disponible para tu Steam Deck
---
El equipo de @batocera_linux ha publicado una versión Beta de su excelente distribución de Linux.


Para quien no la conozca, es una distribución al estilo de [RetroPie](https://retropie.org.uk/), lo que nos permitirá emular una ingente cantidad de sistemas de forma sencilla gracias al uso de la interfaz [EmulationStation](https://emulationstation.org). El rango de máquinas que soporta es increíble, desde el legendario **Spectrum** hasta videoconsolas como **PS3** y **WiiU**. El listado completo lo podéis comprobar en su [Wiki](https://wiki.batocera.org/systems).


Con respecto a su instalación necesitaréis bajar la imagen desde su página de [descargas](https://batocera.org/download). Una vez completada podéis hacer uso de la aplicación [Etcher](https://www.balena.io/etcher/) para grabarla en la **microSD**. Y es que, a diferencia de otros sistemas como [EmuDeck](https://www.emudeck.com), Batocera es una distribución Linux completa y necesitará de una microSD para ejecutarse.


![EmulationStation](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Batocera/emulationStation.webp)


Para iniciar Batocera hay que activar en la **Bios** de Steam Deck (pulsando a la vez los **botones de volumen+ y encendido**) el arranque desde la unidad microSD. En su primera ejecución tendremos que configurar la **Wifi** para tener acceso a **Internet** y también para exportar como una unidad de red el sistema de directorios de Batocera. Luego, con cualquier **cliente samba**, podremos añadir las roms y bios que usaremos para emular nuestras máquinas favoritas.


Desde [JEL](https://jugandoenlinux.com) os recomendamos la colección de clásicos publicada por Sega en [Steam](https://store.steampowered.com/app/34270/SEGA_Mega_Drive_and_Genesis_Classics/) para obtener una selección de las mejores roms de **Mega Drive** y también probar los juegos **homebrew** que año tras año siguen manteniendo vivos estos sistemas.


¿Qué te parece [Batocera](https://batocera.org/)? ¿La has probado? Cuéntanos como siempre tu opinión sobre esta distribución en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).


 

