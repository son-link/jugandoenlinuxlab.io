---
author: Pato
category: "Acci\xF3n"
date: 2017-11-07 19:38:37
excerpt: "<p>El juego mezcla de acci\xF3n y cartas ya est\xE1 disponible con soporte\
  \ en Linux de salida</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/cb9c495b17bc28a44ffb50c55572ed63.webp
joomla_id: 478
joomla_url: hand-of-fate-2-llegara-a-linux-el-proximo-dia-7-de-noviembre
layout: post
tags:
- accion
- indie
- rol
- cartas
title: "'Hand of Fate 2' llegar\xE1 a Linux el pr\xF3ximo d\xEDa 7 de Noviembre (Actualizado)"
---
El juego mezcla de acción y cartas ya está disponible con soporte en Linux de salida

(Actualización 07-11-2017)


'Hand of Fate 2' Ya está disponible para su compra.




---


Otra confirmación de lanzamiento para próximas fechas. Y van...


Gracias a [gamingonlinux.com](https://www.gamingonlinux.com/articles/hand-of-fate-2-to-release-on-november-7th-with-linux-support.10477) nos enteramos hace algunos días de que 'Hand of Fate 2' [[web oficial](http://www.defiantdev.com/hof2.html)] la secuela del aclamado '[Hand of Fate'](http://store.steampowered.com/app/266510/Hand_of_Fate/) que en su día nos llegó también a Linux estará disponible el próximo 7 de Noviembre con versión linux de lanzamiento.


'Hand of Fate 2' seguirá los pasos de su predecesor, mezclando de forma única el juego de cartas con el de acción para desarrollar partidas en donde el azar y la destreza cobran importancia a partes iguales.



> 
> *Hand Of Fate 2 es un juego de exploración de mazmorras ambientado en un mundo de oscura fantasía. ¡Domina un juego de mesa rebosante de vida en el que cada fase de la aventura se saca de una baraja de encuentros legendarios que tú te encargas de elegir! Elige de forma inteligente: tu adversario, el enigmático repartidor, no se andará con rodeos mientras te convierte en el instrumento de su venganza. La mesa ha cambiado, pero las apuestas son las mismas: ¡a vida o muerte!*
> 
> 
> 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/6iaU155JFAI" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 'Hand of Fate 2' llegará traducido al español y lo podrás comprar en su página de Steam. Si quieres saber más detalles, puedes visitarla:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/456670/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 

