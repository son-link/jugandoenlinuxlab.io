---
author: leillo1975
category: Estrategia
date: 2018-09-28 14:29:18
excerpt: <p><span class="username u-dir" dir="ltr">@WeBeHarebrained anuncia la fecha
  de salida oficial<br /></span></p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/b4d7de5ff725512a637402454dd08203.webp
joomla_id: 862
joomla_url: battletech-ya-esta-disponible-para-gnu-linux-en-version-beta
layout: post
tags:
- beta
- paradox
- estrategia-por-turnos
- battletech
- harebrained-schemes
- mechwarrior
title: "Battletech ya est\xE1 disponible para GNU-Linux en versi\xF3n Beta."
---
@WeBeHarebrained anuncia la fecha de salida oficial


**ACTUALIZACIÓN 24-10-18**: Según pudimos ver en twitter, BattleTech para Linux será lanzado oficialmente **el próximo 27 de Noviembre**, conjuntamente con su primera expansión "Flashpoint". Recordad que tendreis acceso a esta expansión si adquirís el [Season Pass](https://www.humblebundle.com/store/battletech-season-pass?partner=jugandoenlinux) (donde podreis acceder a todos los contenidos que se vayan publicando) , o cuando salga como expansión independiente. Este es el tweet que anunciaba el soporte para nuestros sistemas:



>
> -The Linux version will leave Beta on November 27th.
> - Well it won't be in Beta at that point, but yes, it should indeed work. :)
>
>
> — BATTLETECH Game (@BATTLETECH_Game) [22 de octubre de 2018](https://twitter.com/BATTLETECH_Game/status/1054333892830863361?ref_src=twsrc%5Etfw)



 Os dejamos sobre esta expansión:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/bSc5GWIRHBU" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>




---


 **NOTICIA ORIGINAL:** Nos acabamos de enterar que los chicos de [Harebrained Schemes](http://harebrained-schemes.com/) acaban de lanzar en Steam una versión Beta de su juego [Battletech](http://battletechgame.com/) (editado por Paradox). El estudio realizaba el anuncio en twitter hace unas pocas horas:



>
> Want to hear what's in BATTLETECH 1.2.1? (Spoiler - there might be a Linux version available as a Beta!) Read more here: <https://t.co/voSuka5bDs>
>
>
> — Harebrained Schemes (@WeBeHarebrained) [27 de septiembre de 2018](https://twitter.com/WeBeHarebrained/status/1045433190804905984?ref_src=twsrc%5Etfw)



Para poder activarla, si disponeis del juego debis seleccionar en las propiedades del juego, en la pestaña BETAS, la opción "public_beta_linux" en el desplegable. Después os dejará instalar el juego sin problemas. Os recalcamos que este juego está en **BETA**, y que por lo tanto es susceptible de tener fallos. Tambien avisan que hay determinados [problemas relativos a la versión de linux](https://forum.paradoxplaza.com/forum/index.php?threads/live-battletech-update-1-2-1-and-linux-beta-release-notes.1121329/) que están localizados, y por supuesto si encontrais alguno más podeis notificarlo en los [foros de Paradox](https://forum.paradoxplaza.com/forum/index.php?forums/battletech-bug-reports.998/).


Para quien no conozca este juego, se trata de un juego de Estrategia por turnos con Mechwarriors. Aquí teneis la descripción oficial del juego en Steam:


>
> ***Acerca de este juego***
>
>
> *![](https://steamcdn-a.akamaihd.net/steam/apps/637090/extras/Kamea.png)*


> *De la mano de Jordan Weisman, creador original de BATTLETECH/MechWarrior y los desarrolladores de la galardonada serie Shadowrun Returns, llega la nueva generación de combate táctico 'Mech por turnos.*



> *Estamos en el año 3025 y la galaxia está atrapada en un ciclo de guerra perpetua, protagonizada por casas nobles armadas con unos enormes vehículos de combate mecanizados llamados BattleMechs. Toma el mando de tu propia unidad de mercenarios 'Mechs con sus pilotos MechWarriors, tratando de mantenerte a flote mientras te ves involucrado en una brutal guerra civil interestelar. Mejora tu base de operaciones para el transporte galáctico, negocia contratos mercenarios con señores feudales, repara y mantén tu elenco de BattleMechs envejecidos y ejecuta devastadoras tácticas de combate para derrotar a tus enemigos en el campo de batalla.*



> *![](https://steamcdn-a.akamaihd.net/steam/apps/637090/extras/dropship.png)*
> ***LIDERA A UN ESCUADRÓN DE 'MECHS EN COMBATES POR TURNOS***
>
>
> *Despliega más de 30 BattleMechs con una amplia variedad de combinaciones. Aprovecha el terreno, la ubicación, la selección de armas y las habilidades especiales para superar a tus rivales con tus maniobras y tu mecánica de juego.*
> ***DIRIGE TU PROPIA COMPAÑÍA DE MERCENARIOS***
>
>
> *Recluta, adapta y desarrolla exclusivos MechWarriors. Mejora y personaliza tu nave de descenso. Como mercenario, podrás recorrer el espacio, aceptar misiones y gestionar tu reputación con distintas casas nobles y facciones locales.*
> ***PARTICIPA EN UNA DESESPERADA GUERRA CIVIL***
>
>
> *Sumérgete en la historia de una soberana derrocada con violencia que libra una encarnizada batalla para recuperar el trono con el apoyo de tu improvisada compañía de mercenarios.*
> ***PERSONALIZA TUS 'MECHS***
>
>
> *En tu MechLab podrás mantener y mejorar tus unidades, sustituyendo los sistemas de armas dañados por objetos recuperados del campo de batalla, de los enemigos caídos.*
> ***MULTIJUGADOR JCJ Y MODO ESCARAMUZA***
>
>
> *Personaliza tu escuadrón de 'Mechs y MechWarriors para que peleen frente a frente con tus amigos, compitan en línea contra tus adversarios o se lancen a una escaramuza para un jugador para poner a prueba tus estrategias contra la máquina.*


> *![](https://steamcdn-a.akamaihd.net/steam/apps/637090/extras/mech.png)*

 
Para que tengais una idea más aproximada del juego os dejamos con el trailer oficial de lanzamiento:
 
<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/tsIMfOo_VO0" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>
 

Podeis adquirir **BattleTech** en la [Humble Store](https://www.humblebundle.com/store/battletech?partner=jugandoenlinux) (enlace patrocinado)