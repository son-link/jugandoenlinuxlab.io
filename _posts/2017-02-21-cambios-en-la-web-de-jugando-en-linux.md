---
author: Pato
category: Web
date: 2017-02-21 21:43:17
excerpt: "<p>Hoy hemos implementado algunos cambios en la web de Jugando en Linux,\
  \ que aunque no son muchos si afectan al funcionamiento de la web. Estos cambios\
  \ se realizan con el objetivo de mejorar la forma en que se muestra nuestro contenido\
  \ a la vez que nos permite dejar la apariencia visual m\xE1s clara y que esto nos\
  \ permita implementar las nuevas mejoras y caracter\xEDsticas que tenemos en mente\
  \ para un futuro pr\xF3ximo.</p>\r\n<p>En primer lugar, y el cambio m\xE1s obvio\
  \ es la desaparici\xF3n del men\xFA de g\xE9neros en el lateral que pasa a formar\
  \ parte del men\xFA principal, dentro de la secci\xF3n art\xEDculos:</p>\r\n<p><img\
  \ src=\"images/Articulos/Web/nuevomenu.jpg\" alt=\"nuevo menu\" width=\"703\" height=\"\
  374\" id=\"Nuevo Men\xFA\" style=\"display: block; margin-left: auto; margin-right:\
  \ auto;\" /></p>\r\n<p>Esto nos permite economizar espacio en el lateral para la\
  \ llegada de pr\xF3ximas novedades, y de paso implementar el men\xFA de forma estructurada\
  \ dejando m\xE1s claro qu\xE9 categor\xEDas pertenecen a los g\xE9neros y cuales\
  \ no.</p>\r\n<p>Por otra parte, ahora el \"slider\" de los destacados ya no aparece\
  \ una vez que abrimos los art\xEDculos, quedando estos con una apariencia m\xE1\
  s clara y directa, sin tener que hacer scroll para poder leer el contenido:</p>\r\
  \n<p><img src=\"images/Articulos/Web/articlenew.jpg\" alt=\"Art\xEDculos\" width=\"\
  704\" height=\"481\" id=\"Art\xEDculos\" style=\"display: block; margin-left: auto;\
  \ margin-right: auto;\" title=\"Art\xEDculos\" />&nbsp;</p>\r\n<p>&nbsp;Esperamos\
  \ que estos cambios nos permitan implementar nuevas mejoras y os sirvan a vosotros\
  \ para visualizar mejor y con m\xE1s claridad nuestro contenido. Al fin y al cabo,\
  \ esto lo hacemos por vosotros, que sois los que de verdad est\xE1is ah\xED detr\xE1\
  s de la pantalla.</p>\r\n<p>\xBFQue os parecen estos cambios?</p>\r\n<p>Cont\xE1\
  dmelo en los comentarios, o en los canales de Jugando en Linux de <a href=\"https://telegram.me/jugandoenlinux\"\
  \ target=\"_blank\">Telegram</a> o <a href=\"https://discord.gg/ftcmBjD\" target=\"\
  _blank\">Discord</a>.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/de2df791682f079f8397226a3ff38bc7.webp
joomla_id: 226
joomla_url: cambios-en-la-web-de-jugando-en-linux
layout: post
title: Cambios en la web de Jugando en Linux
---
Hoy hemos implementado algunos cambios en la web de Jugando en Linux, que aunque no son muchos si afectan al funcionamiento de la web. Estos cambios se realizan con el objetivo de mejorar la forma en que se muestra nuestro contenido a la vez que nos permite dejar la apariencia visual más clara y que esto nos permita implementar las nuevas mejoras y características que tenemos en mente para un futuro próximo.


En primer lugar, y el cambio más obvio es la desaparición del menú de géneros en el lateral que pasa a formar parte del menú principal, dentro de la sección artículos:


![nuevo menu](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Web/nuevomenu.webp)


Esto nos permite economizar espacio en el lateral para la llegada de próximas novedades, y de paso implementar el menú de forma estructurada dejando más claro qué categorías pertenecen a los géneros y cuales no.


Por otra parte, ahora el "slider" de los destacados ya no aparece una vez que abrimos los artículos, quedando estos con una apariencia más clara y directa, sin tener que hacer scroll para poder leer el contenido:


![Artículos](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Web/articlenew.webp "Artículos") 


 Esperamos que estos cambios nos permitan implementar nuevas mejoras y os sirvan a vosotros para visualizar mejor y con más claridad nuestro contenido. Al fin y al cabo, esto lo hacemos por vosotros, que sois los que de verdad estáis ahí detrás de la pantalla.


¿Que os parecen estos cambios?


Contádmelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

