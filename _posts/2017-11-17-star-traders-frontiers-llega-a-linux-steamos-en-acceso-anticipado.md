---
author: Pato
category: Rol
date: 2017-11-17 10:45:44
excerpt: "<p>Consigue una tripulaci\xF3n y comercia, piratea o caza por toda la galaxia\
  \ con tu nave</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/6e100b693083fe7b237779ffd809d8b7.webp
joomla_id: 541
joomla_url: star-traders-frontiers-llega-a-linux-steamos-en-acceso-anticipado
layout: post
tags:
- indie
- rol
- acceso-anticipado
- estrategia
- turnos
title: '''Star Traders: Frontiers'' llega a Linux/SteamOS en acceso anticipado'
---
Consigue una tripulación y comercia, piratea o caza por toda la galaxia con tu nave

De nuevo nos encontramos con otro juego de rol y toques de estrategia donde tendremos que comandar nuestra nave y nuestra tripulación, encarnando diferentes roles ya sea como pirata espacial, mercante o caza-recompensas en este 'Star Traders: Frontiers'.


Tendremos que elegir nuestro camino creando nuestra propia nave y eligiendo a nuestra tripulación a través de una galaxia donde se producirán cambios continuamente. Tendrás que decidir a que facciones tratar, hacer amigos o enemigos y correr con las consecuencias en un entorno donde las políticas, los intereses económicos o los tratados y otras circunstancias harán que cada partida sea totalmente diferente.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/QyneeQ4jZpQ" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


 'Star traders: Frontiers' no está disponible en español, pero si te van los títulos de este tipo y no es problema para ti, lo tienes disponible en su página de Steam con un 20% de descuento por su lanzamiento en acceso anticipado:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/335620/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 ¿Que te parece este 'Star Traders: Frontiers'? ¿Serás pirata, comerciante, caza-recompensas...?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

