---
author: Pato
category: Terror
date: 2017-11-28 19:02:01
excerpt: "<p>KING Art y THQ Nordic nos traen esta aventura point &amp; Clic con un\
  \ aspecto fant\xE1stico</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/9cb6e71fd379c0a6d58ef216f9910ae9.webp
joomla_id: 554
joomla_url: black-mirror-la-aventura-de-terror-gotico-ya-esta-disponible-en-linux-steamos
layout: post
tags:
- aventura
- terror
- point-and-clic
title: "'Black Mirror' la aventura de terror g\xF3tico ya est\xE1 disponible en Linux/SteamOS"
---
KING Art y THQ Nordic nos traen esta aventura point & Clic con un aspecto fantástico

¿Fan de las aventuras de terror? ¿te van las historias de Poe o HP Lovecraft? pues estás de enhorabuena. Black Mirror es un juego de aventura point&clic basado en la primera trilogía de "Black Mirror" y que bebe de las historias de terror gótico con una jugabilidad renovada centrada en la investigación y la interacción con los elementos y personajes.



> 
> *Escocia, 1926. Tras el suicidio de su padre, David Gordon visita el hogar de sus ancestros por primera vez. Pero su vida pronto se verá amenazada por los secretos que ya les arrebataron la cordura a muchos de sus antepasados.*
> 
> 
> *Atormentado por pesadillas y alucinaciones, David teme estar destinado a seguir los pasos de su padre, a recorrer un sendero que conduce a la demencia y a la muerte.*
> 
> 
> *¿Existe realmente una maldición que persigue a su familia, tal y como creía su padre?*   
> *David deberá desentrañar espantosas verdades enterradas bajo generaciones de silencio y brazas de piedra. El castillo de Black Mirror exige una ofrenda.*
> 
> 
> 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/a4wqLie53IY" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 Con una ambientación sobrecogedora, David tendrá que descubrir la verdad que se esconde entre los muros de Black Mirror.


En cuanto a los requisitos mínimos y recomendados, son:


**Mínimo:**  

+ **SO:** SteamOS o Ubuntu (64bit)
+ **Procesador:** Intel Q9650 / AMD Phenom II X4 940
+ **Memoria:** 8 GB de RAM
+ **Gráficos:** GeForce GTX660 / Radeon 7870 - 2GB VRAM
+ **Almacenamiento:** 11 GB de espacio disponible


**Recomendado:**  

+ **SO:** SteamOS o Ubuntu (64bit)
+ **Procesador:** Intel Core i7 3770 3,9 Ghz / AMD FX-8350 4 GHz
+ **Memoria:** 8 GB de RAM
+ **Gráficos:** GeForce GTX960 / Radeon R9 290 - 4GB VRAM
+ **Almacenamiento:** 11 GB de espacio disponible




Si eres de los valientes que no dudan en entrar en la mansión, tienes 'Black Mirror' disponible traducido al español (solo textos) en su página de Steam con un 10% de descuento por su lanzamiento:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/581300/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 

