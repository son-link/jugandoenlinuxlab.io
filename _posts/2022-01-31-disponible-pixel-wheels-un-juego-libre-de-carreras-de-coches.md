---
author: leillo1975
category: Carreras
date: 2022-01-31 18:25:34
excerpt: "<p>Su creador, <span class=\"link_group for_nil\">@aureliengateau, acaba\
  \ de lanzar la versi\xF3n 0.22. </span></p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/PixelWheels/PixelWheels.png
joomla_id: 1419
joomla_url: disponible-pixel-wheels-un-juego-libre-de-carreras-de-coches
layout: post
tags:
- open-source
- vista-cenital
- java
title: Disponible Pixel Wheels, un juego libre de carreras de coches
---
Su creador, @aureliengateau, acaba de lanzar la versión 0.22. 


 De paseo por Reddit me encontré un [post](https://www.reddit.com/r/opensourcegames/comments/sgehsh/new_version_of_pixel_wheels_pixelart_topdown/) que me pareció interesante, cuando justo en ese momento **nuestro colega P_Vader** me envió un mensaje, y vaya, que coincidencia, hablaba del mismo juego que el post. Se trata obviamente de **Pixel Wheels,** un juego de **carreras de coches en 2D de estilo retro y con perspectiva cenita**l , creado por el desarrollador francés [Aurélien Gâteau](https://twitter.com/aureliengateau), y lo mejor de todo, **completamente Open Source** (La lógica del juego está licenciada bajo GPL 3.0 o posterior, siendo el resto del código  licenciado en Apache 2.0, y los activos bajo Creative Commons BY-SA 4.0.). El juego está programado usando principalmente **Java**, y su código se puede encontrar en [Github](https://github.com/agateau/pixelwheels). Este título **multiplataforma**, además de en Linux, como es lógico,  se puede jugar tanto en Windows, como en MacOS , como en Android. Recientemente se ha actualizado a la [versión 0.22](https://agateau.com/2022/pixelwheels-0-22-0/), tal y como anuncia este tweet:



> 
> Pixel Wheels 0.22.0 is out, comes with more translations, configurable keyboard shortcuts and less bugs!<https://t.co/VMJrWIxUvR>[#gamedev](https://twitter.com/hashtag/gamedev?src=hash&ref_src=twsrc%5Etfw) [#pixelart](https://twitter.com/hashtag/pixelart?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/bII9oKnyyr](https://t.co/bII9oKnyyr)
> 
> 
> — Aurélien Gâteau (@aureliengateau) [January 30, 2022](https://twitter.com/aureliengateau/status/1487839955162443780?ref_src=twsrc%5Etfw)



 Si nos centramos en su jugabilidad, veremos que podremos realizar **carreras con diversos tipos de vehículos**, que al igual que las pistas, iremos desbloqueándolos a medida que ganemos carreras. Podremos también **ir recogiendo items a lo largo de la carrera** que nos permitirán tener ciertas ventajas sobre el resto de competidores, como boosts, misiles, minas etc. También habrá elementos fijos en el escenario que actuarán de la misma manera. Existen **dos modos de juego, en solitario, y el multijugador a pantalla partida** donde podremos jugar con otra persona en el mismo ordenador. En cada uno de estos modos podremos jugar tanto una **carrera única**, como un **campeonato**.


El juego se encuentra aun en desarrollo, pero eso no quita que se pueda disfrutar perfectamente de él en su actual estado. Podeis encontrar más información en su [página web](https://agateau.com/projects/pixelwheels/), así como su descarga en [itch.io](https://agateau.itch.io/pixelwheels). Os dejamos con un video de hace algún tiempo donde podeis ver algunas de sus virtudes:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/xriYt8K9lxw" title="YouTube video player" width="780"></iframe></div>

