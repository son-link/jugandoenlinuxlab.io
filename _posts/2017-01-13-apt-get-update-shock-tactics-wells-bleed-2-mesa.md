---
author: Pato
category: Apt-Get Update
date: 2017-01-13 18:28:22
excerpt: "<p>A\xF1o nuevo, vida... bueno... volvemos a las andadas</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/ab98897d60eaa0d05c1862c118538407.webp
joomla_id: 178
joomla_url: apt-get-update-shock-tactics-wells-bleed-2-mesa
layout: post
title: Apt-Get Update Shock Tactics & Wells & Bleed 2 & Mesa...
---
Año nuevo, vida... bueno... volvemos a las andadas

- Comenzamos este primer Apt-Get Update con una cancelación. No es plato de gusto el anunciar cancelaciones pero los chicos de [linuxgameconsortium.com](https://linuxgameconsortium.com) nos anunciaron que Shock Tactics, un juego de acción y estrategia por turnos ha sido cancelado. Lo puedes leer [en este enlace](https://linuxgameconsortium.com/linux-gaming-news/shock-tactics-turn-based-strategy-cancelled-linux-mac-pc-2017-45881/).


- Por otra parte, desde itch.io nos llegan noticias de 'Wells' un juego de disparos en perspectiva lateral y gráficos 3D al mas puro estilo "run & gun".


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/UVe5EV3FE5o" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


Puedes verlo [en este enlace](https://towerupstudios.itch.io/wells).


- Volvemos con linuxgameconsortium.com donde nos avisaron de que 'Bleed 2' ya tiene fecha de salida para el 8 de febrero.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/FxZcEnMTrOQ" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


Puedes ver la noticia [en este enlace](https://linuxgameconsortium.com/linux-gaming-news/bleed-2-shoot-em-up-launch-date-announced-linux-mac-pc-45889/).


- Vamos ahora a por una de drivers. Desde [gamingonlinux.com](https://www.gamingonlinux.com/) Liam nos anuncia que los drivers Mesa siguen progresando a un buen ritmo y ya ofrecen soporte para OpenGL 4.2 en la famila Haswell. Puedes verlo [en este enlace](https://www.gamingonlinux.com/articles/haswell-should-now-see-opengl-42-thanks-to-recent-work-in-mesa.8899).


- También desde [gamingonlinux.com](https://www.gamingonlinux.com/) nos cuentan que Mesa ofrece también soporte para OpenGL 4.3 en los drivers Nouveau (Nvidia) para la familia Maxwell y siguientes. Puedes verlo [en este enlace](https://www.gamingonlinux.com/articles/opengl-43-now-available-in-mesa-for-nouveau-nvidia-for-maxwell-and-above.8903).


- Ah! y Valve está buscando crear una lista de juegos que no funcionen con el driver Radeonsi. Puedes verlo [en este enlace](https://www.gamingonlinux.com/articles/valve-are-looking-to-gather-a-list-of-games-that-dont-work-with-radeonsi.8874).


- Por último, Liam nos informa también que un desarrollador de Valve ha desarrollado una herramienta para testeo de tarjetas AMD bajo Linux. Puedes verlo [en este enlace](https://www.gamingonlinux.com/articles/a-valve-developer-has-released-a-tool-to-debug-amd-graphics-cards-on-linux.8894).


Hasta aquí el Apt-Get Update de esta semana, pero seguro que me he dejado muchas cosas en el tintero. ¿Qué tal si me lo cuentas en los comentarios, o en los canales de Jugando en Linux en [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD)?.

