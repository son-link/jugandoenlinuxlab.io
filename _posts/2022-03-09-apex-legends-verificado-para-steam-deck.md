---
author: P_Vader
category: "Acci\xF3n"
date: 2022-03-09 12:34:07
excerpt: "<p>Era un secreto a voces que iba a llegar de la mano con la consola de\
  \ Valve.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ApexLegends/apex_legeneds_2022_900px.webp
joomla_id: 1448
joomla_url: apex-legends-verificado-para-steam-deck
layout: post
tags:
- proton
- steam-deck
- verificado
- apex-legends
title: Apex Legends verificado para Steam deck
---
Era un secreto a voces que iba a llegar de la mano con la consola de Valve.


Lo de Apex Legend era un secreto a voces. Desde hace casi un mes teníamos conocimiento de movimientos en las actualizaciones del juego con el anti trampas (EAC) y ademas en el repositorio de Proton de Valve incluían mejoras de rendimiento exclusivo para este juego.


Una grandísima noticia para el videojuego en Linux, **estamos hablando de uno de los juegos AAA multijugador que más se juega actualmente.** Además de que puede ejercer de efecto dominó sobre otros títulos que vean con buenos ojos apostar por darles soporte a Steam Deck.


![apex gameplay 2022](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/ApexLegends/apex_gameplay_2022_900px.webp)


Hace precisamente una semana vimos que en Apex volvían a actualizar las librerías de EAC e incluía la de Linux, **nos lanzarnos a probarlo con un resultado espectacular.**


Y aunque en el video podrás notar algo tirón, era simplemente una cuestión de que los mapas no tenían cacheados las sombreadores aun. Conforme vas jugando ese problema desaparece una vez que obtengas las famosas shaders o sombreadores. Aquí puedes ver el video que grabamos ese día aunque es posible que la experiencia haya mejorado incluso como decía:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="405" src="https://www.youtube.com/embed/QXOx_oUEP88" title="YouTube video player" width="720"></iframe></div>


En el momento que **se grabó el video era necesario usar Proton experimental**, aunque suponemos que pronto no será necesario una vez que se publique la próxima versión estable.


Por si aún no conoces el juego **Apex Legend se trata de un Battle Royal por equipos de dúos o tríos**. Las características con las que se publicita el juego son:


* Un elenco de personajes legendarios - Con habilidades, puntos fuertes y personalidades únicas. Empezar a jugar es fácil, pero dominarlas será todo un desafío.
* Forma un equipo - Elige tu leyenda y combina sus habilidades únicas junto con otros jugadores para formar el equipo definitivo.
* Juego estratégico basado en pelotones - Tanto si luchas en una enorme ciudad flotante en el Battle Royale como si te bates en duelo cuerpo a cuerpo en las Arenas, deberás pensar rápido. Domina las habilidades únicas de tu leyenda y coordínate con tus compañeros de equipo para descubrir nuevas tácticas y potentes combinaciones.
* Combate innovador - Domina una gran variedad de potentes armas y equipamiento. Deberás moverte rápido y aprender los ritmos de cada arma para sacar el máximo partido al arsenal. Además, prueba algo nuevo con los modos por tiempo limitado y prepárate para un montón de contenidos nuevos cada temporada.
* Universo en continua expansión - Apex Legends™ se desarrolla en un universo envolvente en el que la historia evoluciona continuamente, los mapas cambian en cada temporada y nuevas leyendas se unen a la lucha. ¡Deja tu huella en los Juegos Apex con multitud de atuendos distintivos y únete a la aventura!


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/1172470/" style="border: 0px none;" width="646"></iframe></div>


¿Aun no lo has probado? ¡Estás tardando! Cuéntanos que te parece en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

