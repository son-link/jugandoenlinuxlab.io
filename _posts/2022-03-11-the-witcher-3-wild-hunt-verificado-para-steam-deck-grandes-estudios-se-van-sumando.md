---
author: Pato
category: Rol
date: 2022-03-11 21:34:54
excerpt: "<p>XBox Game Studios tambi\xE9n se felicita de sus juegos verificados para\
  \ la m\xE1quina de Valve</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Thewitcher/TW3Wildhunt.webp
joomla_id: 1450
joomla_url: the-witcher-3-wild-hunt-verificado-para-steam-deck-grandes-estudios-se-van-sumando
layout: post
tags:
- accion
- steamos
- rol
- the-witcher-3
- steam-deck
title: 'The Witcher 3: Wild Hunt verificado para Steam Deck. Grandes estudios se van
  sumando'
---
XBox Game Studios también se felicita de sus juegos verificados para la máquina de Valve


Poco a poco se van sumando más y más estudios al carro de Steam Deck. Hoy nos encontramos con un anuncio oficial por parte de **CDProjekt RED** en el que anuncian que **The Witcher III: Wild Hunt** ya está verificado para Steam Deck, y además también están trabajando para que **Cyberpunk 2077** también ofrezca una buena experiencia jugable:



> 
> *"Estamos trabajando en estrecha colaboración con Valve en los aspectos de compatibilidad y rendimiento de nuestros juegos en Steam Deck. El objetivo es proporcionar la mejor experiencia posible de The Witcher 3 y Cyberpunk 2077 en esta plataforma, teniendo en cuenta sus características de hardware únicas."*
> 
> 
> 


Si bien avisan que esto es solo para los juegos mencionados, dejando aparte los anteriores títulos del estudio, al menos ya es una buena señal, y un paso adelante en cuanto a soporte de sus grandes franquicias.


Puedes ver el anuncio en su [post oficial de Steam](https://store.steampowered.com/news/app/292030/view/4712459761457742330), y encontrar The Witcher III [en su página de la tienda](https://store.steampowered.com/app/292030/The_Witcher_3_Wild_Hunt/).


Otro de los estudios que se suma al carro es ni mas ni menos que **XBox Game Studios,** que también se congratula de que varios de sus juegos ya han sido verificados para Steam Deck (otros títulos de sus grandes franquicias también están en la categoría de "jugables") como **Psychonauts 2, Hellblade: Senua's Sacrifice, Battletoads o Max: The Curse of Brotherhood,**aunque avisan que depende de cada estudio el como encajar el soporte para Steam Deck, por lo que algunos pueden llevar algo más de tiempo en llegar. También añaden en el post los títulos que no tienen soporte debido a los sistemas anti-cheat como Gears 5, Halo MCC, Halo Infinite y Microsoft Flight Simulator X.


Puedes ver los enlaces a estos juegos y a sus otros títulos jugables en su [post oficial en Steam](https://store.steampowered.com/news/group/3090835/view/3112553992726030833).


Por último, reseñar que otro de los nombres importantes de Xbox Game Studios, Aaron greenberg manager general en XBox felicitó a Valve por el lanzamiento de la Steam Deck.



> 
> Congratulations [@valvesoftware](https://twitter.com/valvesoftware?ref_src=twsrc%5Etfw) on the launch of the Steam Deck!  
>    
> More and more of our 1st Party titles are getting marked as Verified or Playable – you can check out which ones here:<https://t.co/KYgytWMos7> [pic.twitter.com/mAT048H7O1](https://t.co/mAT048H7O1)
> 
> 
> — Aaron Greenberg 🙅🏼‍♂️💚U (@aarongreenberg) [March 10, 2022](https://twitter.com/aarongreenberg/status/1501973514684813320?ref_src=twsrc%5Etfw)



¿Qué te parece la llegada de grandes estudios y grandes franquicias a Steam Deck? ¿crees que en última instancia esto ayudará al juego en Linux?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org). 

