---
author: Pato
category: "Acci\xF3n"
date: 2017-01-26 17:25:12
excerpt: <p>El juego de The Vanir Project y BadLand Games busca apoyos para su lanzamiento
  en Steam</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/ea82697ed9755e975f3c7d735db2070c.webp
joomla_id: 194
joomla_url: nightmare-boy-un-juego-de-accion-de-tipo-metroidvania-esta-en-campana-en-greenlight
layout: post
tags:
- accion
- indie
- plataformas
- aventura
- greenlight
- metroidvania
title: "'Nightmare Boy' un juego de acci\xF3n de tipo \"metroidvania\" est\xE1 en\
  \ campa\xF1a en Greenlight"
---
El juego de The Vanir Project y BadLand Games busca apoyos para su lanzamiento en Steam

Seguimos con la "explosión" de títulos indie. Esta vez se trata de 'Nightmare Boy' [[web oficial](http://www.thevanirproject.com/nightmare-boy/)], un juego de tipo metroidvania de acción y aventura en 2D del estudio The Vanir Project compuesto por tan solo dos personas, y que está en campaña en Greenlight. Ya sabéis de mi debilidad por este tipo de juegos.


El juego en si tiene un apartado gráfico muy llamativo de estilo "cartoon" y la temática del juego recuerda a otros títulos de "monstruos y zombies". Según las propias palabras del estudio: 



> 
> Nightmare Boy Se desarrolla en un mundo interconectado donde se mezclan escenarios oníricos con enemigos sacados de las peores pesadillas.  
> Además, conocerás a extraños personajes con los que podrás interactuar, y aprenderás magias o habilidades que te conducirán a nuevas zonas por descubrir.
> 
> 
> 


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/_NJibAPvpqo" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


Ahora bien. Lo que nos interesa a nosotros: ¿Llegará a Linux?


Como en su web oficial o en su página de Greenlight solo hacen referencia a "PC" como plataforma nos hemos puesto en contacto con el estudio para preguntar por la posibilidad de publicarlo en Linux:



> 
> [@JugandoenLinux](https://twitter.com/JugandoenLinux) Hola! Sí, también estará disponible para Linux ;) muchas gracias!
> 
> 
> — The Vanir Project (@vanirproject) [January 26, 2017](https://twitter.com/vanirproject/status/824601141002596353)



 ¡Queda confirmado!


Si te gusta la temática y quieres apoyar al estudio para que lancen el juego en Steam, o quieres saber más detalles sobre el juego puedes pasarte y votar en su página de Greenlight [en este enlace](http://steamcommunity.com/sharedfiles/filedetails/?id=848597986&searchtext=nightmare+boy).


¿Qé te parece este 'Nightmare Boy'? ¿Piensas apoyarlo en Greenlight?


Cuéntamelo en los comentarios, en el foro o en los canales de Jugando en Linux en [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

