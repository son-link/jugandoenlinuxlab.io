---
author: leillo1975
category: Carreras
date: 2022-06-27 09:53:02
excerpt: "<p>El desarrollo de <span class=\"css-901oao css-16my406 r-poiln3 r-bcqeeo\
  \ r-qvutc0\">@raceleaguegame</span> acaba de entrar en esta nueva fase, aunque por\
  \ ahora sin soporte nativo.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/RaceLeague/RaceLeuagueEA.webp
joomla_id: 1479
joomla_url: raceleague-se-estrenara-muy-pronto-en-acceso-anticipado-a2
layout: post
tags:
- acceso-anticipado
- unity3d
- raceleague
- oversteer-studios
title: 'RaceLeague se ha estrenado ya en Acceso Anticipado (Actualizado). '
---
El desarrollo de @raceleaguegame acaba de entrar en esta nueva fase, aunque por ahora sin soporte nativo.


**ACTUALIZACIÓN 27-7-22:**


Tal y como os comentamos hace exactamente un mes, finalmente este juego ha sido lanzado en "Early Access", aunque con una pequeña decepción, pues **por ahora no cuenta con una versión pública descargable para Linux**. Nos hemos puesto en contacto con su creador, **Jali Hautala** , y nos ha comentado que se ha encontrado con algunos **problemas en la build para nuestro sistema**, por lo que ha decidido posponerla un poco más y centrarse en temas relativos al lanzamiento de esta versión anticipada. Este es el tweet que anuncia el nuevo estado de desarrollo del juego:



> 
> The game I have been developing for four years is now out on early access. I'm excited, I'm scared!<https://t.co/ZYiJ8BfSIU>[#madewithunity](https://twitter.com/hashtag/madewithunity?src=hash&ref_src=twsrc%5Etfw) [#gamedev](https://twitter.com/hashtag/gamedev?src=hash&ref_src=twsrc%5Etfw) [#racing](https://twitter.com/hashtag/racing?src=hash&ref_src=twsrc%5Etfw) [#gaming](https://twitter.com/hashtag/gaming?src=hash&ref_src=twsrc%5Etfw)
> 
> 
> — RaceLeague (@raceleaguegame) [July 26, 2022](https://twitter.com/raceleaguegame/status/1551985666430570496?ref_src=twsrc%5Etfw)


 



  
**Desconocemos si el juego funciona correctamente con Proton,** lo cual haría la espera más llevadera, pero confiamos en que pronto podamos contar con una versión con soporte nativo para disfrutar en nuestros PCs con Linux, y **Steam Decks**, dispositivo para la que parece que se adaptaría como anillo al dedo. Esperemos poder ofreceros pronto una nueva actualización donde tengamos la noticia que todos vosotros, Linuxeros, estáis esperando, y también poderos mostraros sus virtudes en nuestros canales de video ([Youtube](https://www.youtube.com/c/jugandoenlinuxcom) y [Twitch](https://www.twitch.tv/jugandoenlinux/)). Mientras tanto os dejamos con un tutorial que nos muestra como funciona el editor de pistas:  
  



 


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/yGyu8Q6s7l4" title="YouTube video player" width="780"></iframe></div>


 




---


 **NOTICIA ORIGINAL 27-6-22:**


Con motivo del NextFest del otoño pasado, [descubríamos]({{ "/posts/raceleague-llegara-de-forma-nativa-a-linux" | absolute_url }}) una demo para Windows de un juego de carreras de coches de lo más llamativo. Inmediatamente nos pusimos a buscar más información sobre ella, y pronto su creador [nos confirmaba en los foros de Steam](https://steamcommunity.com/app/1565890/discussions/0/3056240878550324318/#c2957167122127030848) que también daría soporte a Linux. Desde entonces hemos podido seguir a través de las redes sociales el impresionante avance que ha tenido a lo largo de estos meses, donde hemos visto trabajos en físicas, colisiones,categorías de coches y el editor, entre otras muchísimas cosas. Esos trabajos finalmente culminarán en una versión en Acceso Anticipado que estará disponible exactamente dentro de un mes, concretamente el día **26 de Julio**, tal y como podemos ver en este tweet:



> 
> RaceLeague finally has a confirmed release date!  
> Mark your calendar! 26th of July! Check out the new trailer! Spread the word![#madewithunity](https://twitter.com/hashtag/madewithunity?src=hash&ref_src=twsrc%5Etfw) [#gamedev](https://twitter.com/hashtag/gamedev?src=hash&ref_src=twsrc%5Etfw) [#racing](https://twitter.com/hashtag/racing?src=hash&ref_src=twsrc%5Etfw) [#gaming](https://twitter.com/hashtag/gaming?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/V1xMzUVme5](https://t.co/V1xMzUVme5)
> 
> 
> — RaceLeague (@raceleaguegame) [June 26, 2022](https://twitter.com/raceleaguegame/status/1541043930858487814?ref_src=twsrc%5Etfw)


 



Para quien no conozca este título, tenemos que deciros que se trata, como podéis ver, de un juego de **carreras de coches creado con el motor Unity3D**, centrado en el apartado **multijugador**, donde podremos competir con otros jugadores tanto online, como a través de tiempos que se almacenan en una **base de datos de pistas**. Con respecto a estas, **podremos crear nuestros propios circuitos mediante un potente editor** incluido en el juego, y compartirlo con todos los jugadores. El juego además hace gala de unas **físicas muy trabajadas** y vistosas. Si queréis [más info]({{ "/posts/raceleague-llegara-de-forma-nativa-a-linux" | absolute_url }}), podéis consultar nuestro anterior artículo sobre [RaceLeague](for%20you%20to%20make?).


Es increíble que todo esto que podemos ver del juego haya sido creado por una sola persona, **Jali Hautala**, un **desarrollador indie Finlandés**. Lo que si sabemos es que recientemente ha dado nombre a su compañía como **Oversteer Studios**, y podemos ponernos en contacto con él para seguir su desarrollo a través de [discord](https://discord.com/invite/deRKaXCPxA). Nosotros por nuestra parte seguiremos atentos al desarrollo de este videojuego, e **intentaremos publicar algún video o emitir en directo algún gameplay** si es posible. Podréis encontrar el juego en su página de [Steam](https://store.steampowered.com/app/1565890/RaceLeague/). Mientras tanto os dejamos con el trailer recién publicado que anuncia la próxima llegada del Acceso Anticipado:


 <div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/LsruATZxihE" title="YouTube video player" width="780"></iframe></div>   



  
¿Os hareis con RaceLeague en Acceso Anticipado? ¿Que os parece el trabajo de su creador? Si queréis contestarnos o decirnos vuestra opinión sobre RaceLeague, hacedlo en los comentarios, en nuestro grupo de [Telegram](https://t.me/jugandoenlinux), o nuestras salas de [Matrix](https://matrix.to/#/#jugandoenlinux.com:matrix.org).

