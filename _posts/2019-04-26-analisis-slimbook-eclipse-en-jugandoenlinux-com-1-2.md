---
author: Pato
category: "An\xE1lisis"
date: 2019-04-26 09:53:03
excerpt: "<p>Analizamos el port\xE1til gamer de @SlimbookEs</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/d8ecaf477e032972e780acc36e808256.webp
joomla_id: 1033
joomla_url: analisis-slimbook-eclipse-en-jugandoenlinux-com-1-2
layout: post
tags:
- hardware
- analisis
- slimbook
title: "An\xE1lisis: Slimbook Eclipse en Jugandoenlinux.com -1/2-"
---
Analizamos el portátil gamer de @SlimbookEs

Dentro del ecosistema Linuxero siempre se ha dicho que lo que faltaba era más apoyo de empresas de hardware para que nuestro sistema favorito tuviera mas presencia de cara al usuario final. Por suerte, en España tenemos un par de empresas dedicadas al hardware y que ofrecen soporte para sistemas Linux tanto a nivel nacional como internacional, y una de ellas es la valenciana **Slimbook** a la que agradecemos desde aquí que nos prestara este **portátil Slimbook Eclipse** para poder realizar esta review.


**El Slimbook Eclipse es un portatil pensado para las aplicaciones que requieren potencia**. No es un portátil pensado para la movilidad extrema ni para un uso con aplicaciones que requieran pocos recursos o mucha autonomía si no para poder ejecutar aplicaciones "pesadas", y ante el auge del mundo del videojuego en Linux este tipo de portátiles toma todo el sentido.


Una vez que abrimos la caja nos encontramos el portátil, el impreso de garantía y la fuente de alimentación de 19 voltios y **150 Watios**, suficiente para alimentar la potencia del hardware que veremos a continuación.


![IMG 20190411 153110](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/slimbook/IMG_20190411_153110.webp)


*Justo al salir de la caja. No hace falta mucho mas.*


Estamos ante un portátil de dimensiones generosas de **390 x 269.5 x 27.9mm**, con **pantalla de 15.6 pulgadas LED mate Anti-Glare con resolución FullHD 1080p,** **teclado** **Retroiluminado LED RGB monolítico con varios modos de iluminación** (configurable mediante teclas) expandido con teclado numérico distribución ANSI, y **2,6 Kg de peso** con las baterías incluidas. Respecto a estas, son baterías de 4 celdas 60Wh que si bien ofrecen una autonomía (según sistema) de cerca de 2 horas en cuanto lo hemos puesto a sudar con algunos juegos la autonomía ha bajado drásticamente y con rapidez, siendo imprescindible conectar la fuente de alimentación si no queremos quedarnos a mitad de la partida.  



![IMG 20190411 153735](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/slimbook/IMG_20190411_153735.webp)


En cuanto a conectividad, el Slimbook Eclipse cuenta con **Wifi AC, Bluetooth 4.0, 2 puertos USB 2.0 y 2 USB 3.0, como salida de vídeo un puerto HDMI, un puerto lector de tarjetas** **SD, SDHC, SDXC, MMC y un puerto de entrada/salida de audio minijack** por donde podemos conectar los cascos con micrófono tan necesarios hoy día para las partidas online. Ojo con este último punto, pues el minijack de los cascos tiene que ser de tres contactos, no valen cascos que tengan jacks separados para los auriculares y el micrófono.


![lateralizq](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/slimbook/lateralizq.webp)


*Detalle de las conexiones del lateral izquierdo*


![lateraldere](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/slimbook/lateraldere.webp)


*Detalle de las conexiones del lateral derecho*


Para terminar con la parte externa del Slimbook Eclipse, la carcasa es de plástico ABS negro con una apariencia mate que se nota robusto al tacto. Además cuenta con una cámara web-cam HD con doble micrófono integrado en el frontal.


Siguiendo con las características internas, este equipo cuenta con un procesador **Intel Kaby Lake i7-7700HQ 4 núcleos y 8 hilos, 8 GB de RAM DDR4** (no se especifica la velocidad), **gráfica Nvidia GTX 1060 con 6 GB GDDR5 dedicados y un disco duro SSD M.2 de 120 Gb**. Respecto a esto útlimo, dejar claro que **la velocidad de respuesta** que ofrece el disco duro M.2 **es sobresaliente** dejando constancia del excelente ancho de banda que ofrecen estos discos duros y haciendo que el sistema se mueva con gran fluidez a la hora de cargar aplicaciones o en el arranque del propio sistema, pero a todas luces es claramente insuficiente en cuanto a capacidad, ya que si bien los juegos indie no suelen ocupar mucho en cuanto a espacio en disco duro, en cuanto queramos cargar juegos triple A como los Tomb Raider, algún Total War o F1 tendremos que ir con ojo por que a poco que nos descuidemos nos podemos quedar sin espacio para nada mas. Es por esto que si pensáis adquirir uno de estos equipos **es más que recomendable que optéis por un disco duro de mayor capacidad**,  o una configuración de doble disco duro (que también está disponible para montar en el equipo), uno para el sistema y otro para los juegos.


Va siendo hora de arrancar el equipo. **El Slimbook Eclipse puede adquirirse con un buen número de distribuciones pre-instaladas**, incluido Windows pero para el caso **hemos recibido el equipo pre-cargado con Ubuntu 18.04.02** con su Gnome Shell "de fabrica". Podemos enzarzarnos en si a cada uno nos gusta más tal o cual distro para jugar o hacer nuestras tareas pero lo que no se puede discutir es que Ubuntu a pelo es la distro recomendada para jugar, ya que prácticamente la totalidad de los juegos que recibimos hoy día en Linux se programan y testean para que funcionen en Ubuntu o Debian/SteamOS.


![Frontalencen](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/slimbook/Frontalencen.webp)


*Detalle del teclado una vez encendido*


![screenfetch1](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/slimbook/screenfetch1.webp)


*El ya clásico screenfetch*


Aparte de Ubuntu y sus paquetes habituales que vienen por defecto, el Slimbook Eclipse **viene pre-cargado con algunos añadidos marca de la casa**, como los applets para señalizar los típicos "caps-lock" y "num-lock", la configuración de la batería y sus ciclos de carga y la configuración del touchpad el cual podemos configurar a nuestro antojo.


 También nos encontramos con una aplicación de configuración llamada Slimbook Essentials que nos permitirá configurar diversos aspectos. Cuenta con varios apartados como ajustes, utilidades y aplicaciones. Algunas capturas para que os hagáis una idea, por que no vamos a entrar en detalle de cada opción. Nos daría casi para otro artículo:


![slimbookessentials1](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/slimbook/slimbookessentials1.webp)


![Slimbookessentials2](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/slimbook/Slimbookessentials2.webp)


![Essentialsutils](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/slimbook/Essentialsutils.webp)


![Essentialsapps](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/slimbook/Essentialsapps.webp)


Desde luego Slimbook no ha escatimado en esfuerzos en ofrecer opciones y hacer amigable el sistema para cualquier tipo de usuarios. Volviendo al primer arranque, sorprende lo silencioso que es dado el procesador y la gráfica que lleva bajo el capó, hasta que se le pide un poco de potencia. En ese momento se activan los ventiladores que lleva en la parte trasera y ya os podemos decir que el equipo en ese momento se hará de notar. Según nuestro registro, en reposo el equipo apenas arroja **43 dBAs**, pero una vez que ejecutamos un juego que exija potencia **la sonoridad se nos va fácilmente a 64 dBAs de media llegando a registrar picos de 67 dBAs**.


Vamos a hablar ahora de la configuración del sistema. En si, Slimbook ha hecho un buen esfuerzo en seleccionar los componentes adecuados para que no tengamos problemas en cuanto a drivers, por lo que con la configuración que trae de fábrica no tendremos problemas de compatibilidad por los componentes como la tarjeta WIFI, el Bluetooth o la pantalla y los gráficos. Eso sí, hablando de gráficos, el equipo viene configurado por defecto con los drivers Nvidia que Ubuntu trae por defecto, siendo necesaria la instalación de los drivers privativos más actualizados mediante el correspondiente PPA, tal y como siempre os hemos recomendado para gráficas Nvidia.


![Driversgpudefecto](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/slimbook/Driversgpudefecto.webp)


*L*os drivers gráficos, tal y como nos los encontramos por defecto


Tenéis esta información en el [post de nuestro foro](index.php/foro/drivers-graficas-nvidia/19-instalar-el-ultimo-driver-propietario-de-nvidia-en-ubuntu-y-derivadas) donde se explican los pasos para instalar estos drivers, explicado por nuestro compañero Leillo.


Hasta aquí hemos descrito lo que este Slimbook Eclipse es capaz de ofrecernos en cuanto a su hardware y sistema. Llegados a este punto, hemos preferido "cortar" esta review para no hacerla demasiado extensa, ya que nuestro objetivo para la segunda parte es mostraros dentro de lo posible lo que este portátil es capaz de ofrecernos mediante benchmarks de distintos juegos, pero eso será en la segunda parte que publicaremos en los próximos días.


Mientras tanto, si queréis saber mas sobre el **Slimbook Eclipse** podéis visitar su [página web](https://slimbook.es/eclipse-linux-profesional-workstation-and-gaming-laptop), o la [tienda de Slimbook](https://slimbook.es/pedidos/slimbook-eclipse-15/eclipse-intel-i7hq-comprar) donde podéis ver todas las opciones de configuración y precios.


¿Qué os parece este Slimbook Eclipse? ¿Lo veis interesante como equipo para jugar?


Podéis darme vuestras opiniones o hacerme las preguntas que consideréis al respecto en los comentarios, o en el canal de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux).

