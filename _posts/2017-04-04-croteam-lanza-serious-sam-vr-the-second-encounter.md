---
author: leillo1975
category: "Acci\xF3n"
date: 2017-04-04 11:08:06
excerpt: "<p>El estudio croata est\xE1 en racha</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/bd9435f235cb4005045f2e6c43e9346a.webp
joomla_id: 279
joomla_url: croteam-lanza-serious-sam-vr-the-second-encounter
layout: post
tags:
- vulkan
- croteam
- vr
- htc-vive
- serious-sam-fusion-2017
title: 'Croteam lanza Serious Sam VR: The Second Encounter (ACTUALIZADO)'
---
El estudio croata está en racha

 Unos dias despues del [lanzamiento de la primera parte](index.php/homepage/generos/accion/item/397-hazte-con-toda-la-coleccion-de-serious-sam-por-muy-poco), **[Croteam](http://www.croteam.com/)** anuncia la disponibilidad de su continuación para su uso con el equipo de realidad virtual HTC Vive:


 



> 
> Now Available on Steam - Serious Sam VR: The Second Encounter, 25% off! [#SteamNewRelease](https://twitter.com/hashtag/SteamNewRelease?src=hash) <https://t.co/F4e6frFGWk> [pic.twitter.com/TUuI2jr4BN](https://t.co/TUuI2jr4BN)
> 
> 
> — Steam (@steam_games) [April 4, 2017](https://twitter.com/steam_games/status/849203223403655168)



 


 Según informan en [Phoronix](http://www.phoronix.com/scan.php?page=news_item&px=SSVR-Second-Encounter-Launch), el título está disponible en este momento con un **25% de descuento** como oferta de lanzamiento (27.74€), y por supuesto viene con integración en **su [plataforma Fusion](http://store.steampowered.com/app/564310/)**, desde donde podremos también lanzar y gestionar el juego. El juego funciona en 64 bits y gracias a la integración de la **API Vulkan**, que tan de moda está ultimamente, para alegría de todos. Informar que aunque el juego no dispone aun del icono, si tiene disponibilidad para Linux/SteamOS.


 


No debemos confundir este juego con "[Serious Sam: Second Encounter HD](http://store.steampowered.com/app/41014/)" , por lo que aunque tengamos este último, por ahora aun no está integrado en la plataforma fusion, aunque se espera que en un futuro si lo estee tal y como aparece la primera parte. Si dispones de unas HTC Vive y quieres hacerte con "Serious Sam VR: The second Encounter" pasate por la tienda de Steam:


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/552460/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


**ACTUALIZADO:** Confirmo que **Serious Sam: The Second Encounter HD ya está disponible en la plataforma Fusion**. Hace un momento he arrancado Steam y se ha actualizado la aplicación con un parche de 2.1 GB. De todas formas debe haber un bug por que el juego no arranca, por lo que agradecería que por favor si alguno de vosotros tiene el mismo problema o al contrario deje un mensaje.


 


Que te parece este lanzamiento. Déjanos un comentario al respecto o pasa por alguno de nuestros grupos de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 

