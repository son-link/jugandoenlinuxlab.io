---
author: Pato
category: "Acci\xF3n"
date: 2019-10-22 16:00:07
excerpt: <p>El juego de Fireblade Software <span class="TweetAuthor-screenName Identity-screenName"
  dir="ltr" title="@GameAbandonShip" data-scribe="element:screen_name">@GameAbandonShip</span>&nbsp;
  nos propone capitanear nuestro barco de vela</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/30033458e7237f5676a81459cd1e537b.webp
joomla_id: 1124
joomla_url: abandon-ship-sale-de-acceso-anticipado-y-ya-esta-disponoble-en-linux-steamos
layout: post
tags:
- accion
- indie
- estrategia
- tactico
title: "Abandon Ship sale de acceso anticipado y ya est\xE1 disponoble en Linux/SteamOS"
---
El juego de Fireblade Software @GameAbandonShip  nos propone capitanear nuestro barco de vela

Nos llegan noticias de que Abandon Ship, el juego en el que tendremos que gestionar y luchr en un navío de vela del que [ya os hemos hablado en otras ocasiones](index.php/homepage/generos/accion/5-accion/1214-abandon-ship-planea-la-llegara-a-linux-steamos-a-finales-del-mes-de-agosto-y-ya-tiene-disponible-una-beta-actualizado) ya ha salido de acceso anticipado y ya está disponible para nuestro sistema favorito.



> 
> Unleash the Kraken!   
>   
> Abandon Ship is out now on Steam - please help us spread the word! ?<https://t.co/xUeRcPIVpw>[#gamedev](https://twitter.com/hashtag/gamedev?src=hash&ref_src=twsrc%5Etfw) [#indiedev](https://twitter.com/hashtag/indiedev?src=hash&ref_src=twsrc%5Etfw) [#indiegame](https://twitter.com/hashtag/indiegame?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/cFjiCI26Sv](https://t.co/cFjiCI26Sv)
> 
> 
> — Abandon Ship (@GameAbandonShip) [October 22, 2019](https://twitter.com/GameAbandonShip/status/1186668445531885568?ref_src=twsrc%5Etfw)



 *Asume el mando de un barco clásico de la «Era de la Vela» y su tripulación, y explora un amplio mundo lleno de historias donde tus decisiones tienen consecuencias. Lucha contra barcos, fortalezas y monstruos marinos en combates tácticos con un estilo artístico inspirado por la pintura clásica naval al óleo.*


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/RIw8fvqGjLg" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Si te gustan los juegos de gestión táctica, tienes Abandon Ship [en Steam](https://store.steampowered.com/app/551860/Abandon_Ship/) con un 10% de descuento por su lanzamiento.

