---
author: Pato
category: Plataformas
date: 2019-09-06 10:44:53
excerpt: "<p>El juego de <span class=\"css-901oao css-16my406 r-1qd0xha r-ad9z0x r-bcqeeo\
  \ r-qvutc0\">@moi_rai_</span> publicado por <span class=\"css-901oao css-16my406\
  \ r-1qd0xha r-ad9z0x r-bcqeeo r-qvutc0\">@Team17Ltd</span> mezcla g\xE9neros de\
  \ forma acertada y tiene incluso demo</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/ba7071c0fd7786ec7342034042359ffe.webp
joomla_id: 1109
joomla_url: caza-y-entrena-tus-monstruos-favoritos-en-monster-sanctuary-ya-en-acceso-anticipado
layout: post
tags:
- indie
- plataformas
- metroidvania
- entrenamiento-de-monstruos
title: Caza y entrena tus monstruos favoritos en Monster Sanctuary, ya en acceso anticipado
---
El juego de @moi_rai_ publicado por @Team17Ltd mezcla géneros de forma acertada y tiene incluso demo

Con la renovada popularidad de los juegos de caza y entrenamiento de monstruos que (todos) conocemos para otras plataformas, como móviles o consolas se nos pasó inadvertido el lanzamiento en acceso anticipado de **Monster Sanctuary**, un juego que puede ser del agrado de los que gustan de cazar y entrenar monstruos para luego enfrentarlos en batallas estratégicas para conseguir objetivos.


En este caso no tendremos que andar de aquí para allá físicamente, aunque si virtualmente por que en Monster Sanctuary se entremezcla el género de caza y entrenamiento de Monstruos con las plataformas de tipo "metroidvania" dentro de un mundo de estética "pixel art" que deberemos recorrer en busca de aventuras. Las influencias reconocidas por el propio autor son juegos como Pokemon, Metroid, Castlevania, Hollow Knight, Dead Cells, Terraria, Starbound, Digimon o Tem Tem.


*"Embárcate en una aventura épica donde puedes usar los poderes de los monstruos reunidos y el equipo entrenado para progresar en un mundo gigantesco. Durante tu búsqueda para convertirte en el mejor guardián de monstruos, descubrirás el origen de un misterio que amenaza la paz entre humanos y monstruos."*


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/LlNbLNhlOQM" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Monster Sanctuary llegó [**en acceso anticipado**](https://store.steampowered.com/app/814370/Monster_Sanctuary/) el pasado día 28 de Agosto con soporte para Linux/SteamOS, y según el creador **el juego se encuentra actualmente terminado al 50%** con multitud de Monstruos y mundos aún por llegar, pero que ya es jugable (varios usuarios reportan haber necesitado mas de 20 horas para completar la campaña) y ya se ha publicado un parche que corrige varios problemas en nuestro sistema favorito. De hecho el juego tiene disponible incluso una demo, que a diferencia del juego que **sí** está traducido al español, solo está disponible en inglés.


Si quieres saber mas sobre Monster Sanctuary puedes visitar su [página web oficial](http://www.monster-sanctuary.com/) o si quieres probar la demo o comprarlo lo tienes disponible en su [página de Steam](https://store.steampowered.com/app/814370/Monster_Sanctuary/), eso sí recordar que el juego sigue en desarrollo y puede presentar algún bug.


¿Qué te parece Monster Sanctuary? ¿Te gustan los juegos de entrenar mónstruos?


Cuentamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

