---
author: leillo1975
category: "Acci\xF3n"
date: 2017-03-31 18:23:29
excerpt: "<p>Croteam pone a la venta todo su cat\xE1logo a precio de risa</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/7884581148e30ccb73a24dfa45b62e09.webp
joomla_id: 275
joomla_url: hazte-con-toda-la-coleccion-de-serious-sam-por-muy-poco
layout: post
tags:
- vulkan
- serious-sam
- croteam
- fusion
- vr
title: "Hazte con toda la colecci\xF3n de Serious Sam por muy poco"
---
Croteam pone a la venta todo su catálogo a precio de risa

Hace una escasa quincena anunciabamos la [disponibilidad de Serious Fusion 2017](index.php/homepage/generos/accion/item/380-serious-fusion-2017-se-aproxima). Hace un momento acabo de leer vía twiter que **Croteam** está tirando la casa por la ventana y todos los que no dispongais de sus juegos teneis la oportunidad de haceros con ellos. El anuncio en cuestión es el siguiente:


 



> 
> SSVR: The First Encounter is now available on Steam!  
> Don't own a VR headset? NP, our catalog is up to 90% off! <https://t.co/nis8lqADqA> [pic.twitter.com/EzhVB5mHqC](https://t.co/EzhVB5mHqC)
> 
> 
> — Croteam (@Croteam) [March 31, 2017](https://twitter.com/Croteam/status/847857264044257281)



 


Como veis también avisan de la disponibilidad de Serious Sam the first Encounter VR para todos los afortunados poseedores de unas gafas de realidad virtual. Como podeis ver en **[la siguiente lista](http://store.steampowered.com/search/?term=croteam)**, las ofertas son tremendamente jugosas, pudiendo haceros también con su original **[Talos Principle](http://store.steampowered.com/app/257510/)** por 10€.


 


Una opción muy apetecible es comprar el lanzador Serious Sam Fusion 2017 y **[luego el pack completo de Serious Sam](http://store.steampowered.com/bundle/2634/)**, lo que os va a permitir poder disfrutar del primer episodio (The first Encounter HD) a través del lanzador usando Vulkan, y según se vayan portando los sucesivos juegos del resto de la coleción. También en el pack está disponible Serious Sam 3 que dispone de soporte para Linux desde hace tiempo. Aquí os dejo el enlace del lanzador:


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/564310/2535/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


 Este es un enlace al pack completo de Serious Sam:


 


[![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SSCpack.webp)](http://store.steampowered.com/bundle/2634/)


 


¿Piensas aprovechar esta oportunidad y adquirir esta colecion? Déjanos un comentario al respecto o pasa por alguno de nuestros grupos de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

