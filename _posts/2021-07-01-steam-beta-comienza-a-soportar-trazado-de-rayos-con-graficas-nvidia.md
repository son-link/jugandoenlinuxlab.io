---
author: Pato
category: Software
date: 2021-07-01 18:40:09
excerpt: "<p>El cliente nos trae novedades para el cliente Linux en su \xFAltima actualizaci\xF3\
  n</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/headers/steam-wallpaper.webp
joomla_id: 1322
joomla_url: steam-beta-comienza-a-soportar-trazado-de-rayos-con-graficas-nvidia
layout: post
tags:
- steam
- beta
title: "Steam Beta comienza a soportar trazado de rayos con gr\xE1ficas Nvidia"
---
El cliente nos trae novedades para el cliente Linux en su última actualización


Hace unos minutos nos ha llegado notificación de la última actualización del cliente beta de Steam en el que según lo planeado Valve comienza a dar soporte al trazado de rayos sobre la API Vulkan siempre que contemos con una gráfica Nvidia que lo soporte.


Además de eso, el *runtime Soldier* y su contenedor reciben multitud de mejoras tanto para soportar Proton como la última versión de SDL. También se solucionan multitud de problemas potenciales de seguridad.


El runtime *Scout*también se actualiza y se añade soporte para el mando de PS5 Dualsense.


Hay que recordar que estos *runtimes* son utilizados por el cliente de Steam tanto en su versión normal de escritorio como su versión "Big Picture". Esperemos que todas las novedades que empiezan a llegar por parte de Steam nos vayan acercando a lo que quiera que sea que Valve tenga entre manos para final de año.


Como siempre, os tendremos informados.


Tienes toda la información de la actualización del cliente Beta de Steam en su [post de actualización](https://steamcommunity.com/groups/SteamClientBeta/announcements/detail/2972920842754444923).


¿Que crees que Valve tiene entre manos? ¿Piensas que estas actualizaciones tienen algo que ver?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de Telegram o Matrix.


 

