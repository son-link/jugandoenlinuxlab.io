---
author: Pato
category: Plataformas
date: 2017-04-06 21:06:55
excerpt: "<p>El caballero de la pala... ahm... err... \xBFg\xFCada\xF1a? vuelve a\
  \ las andadas</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/71601b6fd7fc74a9f4eea8e6c1b43d35.webp
joomla_id: 281
joomla_url: shovel-knight-specter-of-torment-ya-disponible-para-linux-steamos
layout: post
tags:
- accion
- indie
- plataformas
- aventura
title: '''Shovel Knight: Specter of Torment'' ya disponible para Linux/SteamOS'
---
El caballero de la pala... ahm... err... ¿güadaña? vuelve a las andadas

Si no conoces al Caballero de la Pala... güadaña... lo que sea, ¿A qué esperas?


'Shovel Knight: Specter of Torment' es la nueva aventura que nos trae "Yatch Club Games" de la serie por todos aclamada "Shovel Knight":


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/BRwP0P5j05g" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


 


 Como era de esperar, estética retro, música de 8 bits, jugabilidad endiablada y mucha acción y plataformas.


Sinopsis de Steam:



> 
> ¡Shovel Knight: Specter of Torment es la precuela de Shovel Knight y viene repleta de acción! Encarna al no muerto Specter Knight, siervo de la Hechicera, en su misión para reclutar un pelotón de caballeros y crear la Orden Sin Cuartel. Equipado con su guadaña y su agilidad sobrenatural, puede derrapar por el suelo, escalar muros y dominar un arsenal de curiosas armas.
> 
> 
> 


Por otra parte, Shovel Knight: Specter of Torment es gratuito si ya posees el anterior título de la serie: "Shovel Knight: Treasure Trove".


Si quieres un buen plataformas de acción, puedes encontrar este 'Shovel Knight: Specter of Torment' en español en su página de Steam:


<div class="steam-iframe"><iframe frameborder="0" height="190" src="https://store.steampowered.com/widget/589510/" width="646"></iframe></div>


 

