---
author: Serjor
category: Web
date: 2017-11-29 23:19:03
excerpt: "<p>Toca poner tropas y recursos al m\xE1ximo de su capacidad</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/007f7dcc1de776ae0fbd86ceae8656ad.webp
joomla_id: 562
joomla_url: este-viernes-en-la-partida-con-la-comunidad-especial-juegos-de-estragegia
layout: post
tags:
- estrategia
- comunidad
- eventos
title: 'Este viernes en la partida con la comunidad: Especial juegos de estragegia'
---
Toca poner tropas y recursos al máximo de su capacidad

Seguimos una semana más con las partidas de la comunidad por temáticas, y en esta ocasión si participáis tendréis que pensar mucho cada movimiento, porque para este viernes os traemos un especial juegos de estrategia.


La lista de juegos que os proponemos es la siguiente:


0ad, un veterano en nuestras partidas de la comunidad que podéis encontrar [aquí](https://play0ad.com/)


Zero-K, que encontraréis [aquí](http://zero-k.info/)


XCOM


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/200510/17222/" style="border: none;" width="646"></iframe></div>


Warzone 2100, que podréis encontrar [aquí](http://wz2100.net/)


Freeciv, que podréis encontrar [aquí](https://play.freeciv.org/) para jugar incluso desde el navegador


Warhammer 40K: Dawn of War II


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/15620/1308/" style="border: none;" width="646"></iframe></div>


En esta ocasión, y si las cuentas no fallan, tenemos hasta cuatro juegos open source en la lista, nada mal.


Ya sabéis que la votación se hace a través de nuestro canal de [Telegram](https://telegram.me/jugandoenlinux) y finaliza el miércoles, que para organizarnos y comunicarnos durante las partidas solemos usar el canal de [Discord](https://discord.gg/ftcmBjD), que pondremos en nuestro grupo de [Steam](http://steamcommunity.com/groups/jugandoenlinux) un evento (si el ganador es un juego de Steam, lógicamente) y que en la medida de lo posible intentaremos retransmitir la partida en directo por [Twitch](https://www.twitch.tv/jugandoenlinux) y por [YouTube](https://www.youtube.com/channel/UC4FQomVeKlE-KEd3Wh2B3Xw).

