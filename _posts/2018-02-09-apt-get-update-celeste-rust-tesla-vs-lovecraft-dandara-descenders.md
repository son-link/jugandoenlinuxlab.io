---
author: Pato
category: Apt-Get Update
date: 2018-02-09 18:58:31
excerpt: "<p>Repasamos los \xFAltimos y m\xE1s interesantes lanzamientos que no nos\
  \ ha dado tiempo a comentar</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/bfa9b7e5542153d134a3eed2bd0df6f6.webp
joomla_id: 639
joomla_url: apt-get-update-celeste-rust-tesla-vs-lovecraft-dandara-descenders
layout: post
title: Apt-Get Update Celeste & Rust & Tesla vs Lovecraft & Dandara & Descenders...
---
Repasamos los últimos y más interesantes lanzamientos que no nos ha dado tiempo a comentar

Repasando la lista de últimos lanzamientos de Steam, hay unos cuantos títulos de los que no hemos hablado y que merece la pena comentar. Así que vamos con nuestra selección de últimos lanzamientos "olvidados":


#### Celeste


Comenzamos con un gran olvidado. El juego "plataformas" de Matt Makes Games ha sido todo un bombazo, tanto por la calidad del juego como por su endiablada jugabilidad y dificultad.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/FqBj2IGg6Uw" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 [Celeste](http://www.celestegame.com/) se puede conseguir desde su página de Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/504230/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


#### Dandara


 Dandara es otro juego de plataformas, pero esta vez con un componente estratégico mezclado con puzzles y habilidad.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/GGDn0Ofa00M" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 [Dandara](http://www.longhathouse.com/games/dandara/) está disponible en español en su página de Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/612390/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


####  Remnants of Naezith


Otro juego donde la rapidez y reflejos a la hora de actuar son determinantes.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/fc--XUEZDg4" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 [Remnants of Naezith](https://naezith.com/) está disponible en español en su página de Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/590590/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


####  Attack of the Earthlings


Este es un juego que mezcla el estilo de juego de Xcom con parte de estrategia por turnos y buena jugabilidad.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/OaOVQovB9bk" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 [Attack of the Earthlings](http://teamjunkfish.com/game/attack-of-the-earthlings/) está disponible en español en su página de Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/621930/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


####  Chinbu's Adventure


Un juego de rol y acción donde tendrás que tomar el control de un bonito dragón con el poder de modificar los elementos.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/A92kuKDSb8Y" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 Chinbu's Adventure está disponible en su página de Steam pero no en español:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/755570/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


####  Tesla vs Lovecraft


De la mano de los creadores de Crimsonland y Neon Chrome nos llega este notable juego de acción en vista cenital con gráficos muy llamativos y jugabilidad endiablada donde tendremos que derrotar a hordas de monstruos dignos de haber salido de la mente de H.P. Lovecraft con máquinas con poderes eléctricos dignas de la peor pesadilla de Tesla:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/ken-Kz4aOZA" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 [Tesla vs Lovecraft](http://teslavslovecraft.com/) está disponible en español en su página de Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/636100/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


####  Descenders


Un juego de bicis en el que nos jugaremos el pellejo bajando por las pendientes más locas y descabelladas.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/UVNSH-HFG-c" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 [Descenders](http://www.descendersgame.com/) está disponible en español en su página de Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/681280/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


####  Rust


Por último, hoy ha salido de fase de acceso anticipado el archiconocido Rust, que también está disponible para nuestros sistemas. Construye, combate y sobrevive en este popular juego de supervivencia particular:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/HNavTaDpXXU" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 [Rust](https://rust.facepunch.com/) está disponible completamente en español en su página de Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/252490/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 ¿Que piensas de nuestra lista de lanzamientos? ¿Nos hemos dejado algún título interesante en el tintero?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

