---
author: Serjor
category: "Acci\xF3n"
date: 2018-04-02 13:56:03
excerpt: <p>Id preparando vuestras espadas</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/1397be472e45feda66c2cb00ba5f4ce2.webp
joomla_id: 700
joomla_url: blade-symphony-llegara-a-mediados-de-abril-a-gnu-linux
layout: post
tags:
- beat-em-up
- source
- blade-symphony
- espadas
title: "Blade Symphony llegar\xE1 a mediados de abril a GNU/Linux"
---
Id preparando vuestras espadas

Leemos en [gaming on linux](https://www.gamingonlinux.com/articles/blade-symphony-harmonious-prelude-update-will-add-linux-support-to-the-third-person-tactical-swordfighter.11499) que Blade Symphony, el juego de lucha con espadas a lo Soul Calibur, llegará a GNU/Linux el próximo 12 de abril.


Y es que en un largo [anuncio](http://steamcommunity.com/games/bladesymphony/announcements/detail/1655508871988503047), nos informan del avance del juego, y desvelan detalles bastante interesantes, aunque os hacemos un resumen:


* Por un lado el juego será F2P, aunque por el momento seguirán dando la opción a compra para generar más entrada de dinero y poder seguir con el desarrollo.
* Quieren traducir el juego a nuevos idiomas (el castellano entre ellos), y para ello están pidiendo ayuda a la comunidad para que les ayude en la traducción. Si te interesa puedes ir [aquí](http://pootle.punyhumangames.com/)
* Han actualizado la versión del motor Source a la misma que usa actualmente Valve en CS:GO y han conseguido así varias mejoras.
* En breve comenzarán a buscar beta-testers. Sus mecenas en patreon tienen ya acceso al juego, pero les interesa aumentar el número de usuarios de cara a pulir el juego antes del lanzamiento oficial.


La verdad es que el juego tiene una pinta interesante, así que esperaremos al lanzamiento oficial y quién sabe, puede que se convierta en una opción en nuestras partidas con la comunidad que celebramos todos los viernes a la noche.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/VSALhfq5uJc" width="560"></iframe></div>


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/225600/27252/" style="border: 0px;" width="646"></iframe></div>


Y tú, ¿ya has afilado tu katana? Cuéntanos que opinas de este Blade Symphony en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

