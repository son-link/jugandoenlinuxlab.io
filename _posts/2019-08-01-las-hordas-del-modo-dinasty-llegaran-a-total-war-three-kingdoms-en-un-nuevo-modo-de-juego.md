---
author: Pato
category: Estrategia
date: 2019-08-01 16:40:15
excerpt: "<p>El nuevo modo ser\xE1 gratuito y nos lo traer\xE1 @feralgames poco despu\xE9\
  s de la versi\xF3n Windows</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/ed0b0c31c412f7d67bd39119503bb394.webp
joomla_id: 1087
joomla_url: las-hordas-del-modo-dinasty-llegaran-a-total-war-three-kingdoms-en-un-nuevo-modo-de-juego
layout: post
tags:
- accion
- estrategia
- total-war
- three-kingdoms
title: "Las Hordas del modo Dinasty llegar\xE1n a Total War: THREE KINGDOMS en un\
  \ nuevo modo de juego"
---
El nuevo modo será gratuito y nos lo traerá @feralgames poco después de la versión Windows

Buenas noticias para los amantes de la estrategia con mayúsculas, y es que **Feral Interactive** nos anuncia la llegada de un nuevo modo de juego al último **Total War: THREE KINGDOMS**.


 



> 
> The hordes of Dynasty Mode surge onto macOS and Linux shortly after Windows. ?? <https://t.co/MF2eoHUgPP>
> 
> 
> — Feral Interactive (@feralgames) [August 1, 2019](https://twitter.com/feralgames/status/1156946431179202561?ref_src=twsrc%5Etfw)



En concreto se trata de un "**Dinasty Mode**" desarrollado por **Creative Assembly junto a Intel** para exprimir todo el poder multinucleo de las CPUs actuales, y en el que nos tendremos que enfrentar en solitario o junto a tres amigos online a sucesivas hordas de enemigos tan solo con tres de nuestros héroes, con una dificultad incremental en cada oleada. Sega ha lanzado un vídeo cinemático para el evento:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/lI8VELAk1xs" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Este "Modo Dinastía" estará disponible de forma gratuita para todos los poseedores del juego Total War: THREE KINGDOMS en próximas fechas, tras su publicación para sistemas Windows de la mano como siempre de Feral Interactive.


Si quieres saber mas sobre el Dinasty mode puedes visitar el post del blog de Total War donde explican de forma pormenorizada todos los detalles. Si por el contrario quieres comprar Total War: THREE KINGDOMS para poder jugarlo, lo tienes disponible en [Humble Bundle](https://www.humblebundle.com/store/total-war-three-kingdoms?partner=jugandoenlinux) (enlace patrocinado), en la [tienda de Feral](https://store.feralinteractive.com/en/mac-linux-games/threekingdomstw/) (donde ellos se llevan el mayor beneficio) o en su [página de Steam](https://store.steampowered.com/app/779340/Total_War_THREE_KINGDOMS/).

