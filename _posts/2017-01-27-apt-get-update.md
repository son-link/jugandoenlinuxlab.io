---
author: Pato
category: Apt-Get Update
date: 2017-01-27 19:07:49
excerpt: <p>Llegamos al fin de semana, y hay que hacer el repaso habitual</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/e4c07973dbc8eb2f7380bdedc4201087.webp
joomla_id: 200
joomla_url: apt-get-update
layout: post
title: Apt-Get Update drivers Mesa & Homefront & Rocket League & Shadow of Mordor
  & Ellion...
---
Llegamos al fin de semana, y hay que hacer el repaso habitual

Bien. Vamos con otro Apt-Get Update para ver que cosas se nos quedaron en el tintero. Comenzamos:


Desde [www.gamingonlinux.com:](http://www.gamingonlinux.com)


- La última actualización de los drivers Mesa 12 fue lanzada arreglando bugs y avisando a los usuarios de actualizar a Mesa 13 lo antes posible. Puedes verlo [en este enlace](https://www.gamingonlinux.com/articles/mesa-1206-released-with-bug-fixes-for-the-older-stable-version-users-encouraged-to-update-to-mesa-13.8979).


- 'Homefront: The Revolution' no parece que vaya a llegar a Linux en próximas fechas. Puedes verlo [en este enlace](https://www.gamingonlinux.com/articles/it-doesnt-look-like-homefront-the-revolution-is-going-to-come-to-linux-any-time-soon.8977).


- Se está trabajando en arreglar los cuelgues de 'Rocket League' con los drivers Mesa. Puedes verlo [en este enlace](https://www.gamingonlinux.com/articles/timothee-besset-is-working-on-fixing-the-long-hangs-mesa-users-get-in-rocket-league.8987).


- Portal 2 ha recibido un parche para arreglar las texturas. Puedes verlo [en este enlace](https://www.gamingonlinux.com/articles/portal-2-has-been-patched-to-fix-broken-textures.8988).


- 'Shadows of Mordor' recibe un nuevo parche para arreglar problemas con las gráficas Nvidia y mas cosas. Puedes verlo [en este enlace](https://www.gamingonlinux.com/articles/shadow-of-mordor-patch-released-for-linux-fixes-issue-with-nvidia-cards-and-moree.8993).


- Civilization VI ha entrado en fase de testeo final y llegará pronto. Puedes verlo [en este enlace](https://www.gamingonlinux.com/articles/civilization-vi-has-entered-final-testing-for-linux-could-release-soon-should-be-on-sale-too.9001).


 


Desde [www.linuxgameconsortium.com:](http://www.linuxgameconsortium.com)


- 'Hellion' un juego FPS espacial de supervivencia llegará en acceso anticipado el 24 de Febrero. Puedes verlo [en este enlace](https://linuxgameconsortium.com/linux-gaming-news/hellion-first-person-space-survival-early-access-release-date-46774/).


- Recounter' un juego de rol por turnos reciber soporte para Linux y tiene descuento. Puedes verlo [en este enlace](https://linuxgameconsortium.com/linux-gaming-news/rencounter-turn-based-rpg-gets-linux-support-big-discount-games-46610/).


- 'Geneshift' un juego de disparos y zombies en vista isométrica llegará a Linux. Puedes verlo [en este enlace](https://linuxgameconsortium.com/linux-gaming-news/geneshift-topdown-zombie-action-shooter-coming-linux-46565/).


 


Desde [www.phoronix.com:](http://www.phoronix.com)


- AMD GPUPRO 16.60 ha sido lanzado. Puedes verlo [en este enlace](http://www.phoronix.com/scan.php?page=news_item&px=AMDGPU-PRO-16.60).


- El driver Vulkan de Mesa para Intel recibe nuevas características. Puedes verlo [en este enlace](http://www.phoronix.com/scan.php?page=news_item&px=Intel-ANV-maintenance1).


 


Hasta aquí el Apt-Get Update de esta semana. ¿Qué te ha parecido?


Seguro que me he dejado muchas cosas en el tintero. ¿Que tal si me lo cuentas en los comentarios, o en los canales de Jugando en Linux en [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD)?

