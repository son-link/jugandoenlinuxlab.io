---
author: Pato
category: Aventuras
date: 2018-01-09 19:02:51
excerpt: "<p>Desde hace un par de d\xEDas ya tenemos disponible el juego</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/6eb4b1a8ed68214ae7a4c86692674e4d.webp
joomla_id: 599
joomla_url: el-juego-de-aventura-espacial-the-station-llegara-a-linux-steamos-el-proximo-20-de-febrero
layout: post
tags:
- indie
- aventura
- terror
title: "El juego de aventura espacial 'The Station' llegar\xE1 a Linux/SteamOS el\
  \ pr\xF3ximo 20 de Febrero (Actualizaci\xF3n)"
---
Desde hace un par de días ya tenemos disponible el juego

**(Actualización)**


Ya tenemos disponible este 'The Station', tal y como prometieron los desarrolladores. Una aventura espacial de terror con unos gráficos bastante cuidados donde tendremos que desvelar los misterios de una estación espacial en órbita en un planeta donde existe una civilización alienígena. A continuación el trailer de lanzamiento:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/Ce0odGcIqRk" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


En un [artículo de actualización](http://steamcommunity.com/id/jugandoenlinux/commentnotifications/), los desarrolladores anuncian que ya están trabajando en corregir bugs y que una vez que tengan el juego estabilizado empezarán a traducir el juego a diversos idiomas,  añadir un modo libre con el que poder explorar la nave y sus secretos una vez hayamos terminado la historia y también están pensando en la posibilidad de dar soporte VR.


Si te intrigan las historias de terror espaciales, tienes el enlace de 'The Station' en Steam al final de este artículo, pero recordar que aún no está traducido al español.




---


**(Artículo original)**


Buenas noticias para los que gustan de aventuras espaciales. El prometedor 'The Station' llegará a Linux/SteamOS el próximo día 20 de Febrero tal y como ha anunciado su desarrollador en [su página de Steam.](http://steamcommunity.com/gid/[g:1:27154691]/announcements/detail/1581192507838557182)


Además de esto, ha aprovechado para publicar un nuevo trailer del juego:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/RxE9tk6iN5E" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 'The Station' es un juego de misterio y ciencia ficción en primera persona que tiene lugar en una estación espacial enviada para estudiar una civilización alienígena. Tendremos que asumir el rol de un especialista en reconocimiento para descubrir el misterio que envuelve el destino de dos civilizaciones.


En cuanto a los requisitos del sistema, son:


**Mínimo:**  

+ **SO:** Ubuntu 12.04+
+ **Procesador:** Dual core 2.4 Ghz
+ **Memoria:** 2 GB de RAM
+ **Gráficos:** Gráfica con 512MB de VRAM
+ **Almacenamiento:** 4 GB de espacio disponible
+ **Tarjeta de sonido:** Soporta 7.1 surround sound


**Recomendado:**  

+ **SO:** Ubuntu 12.04+
+ **Procesador:** i5 Dual core 2.5 Ghz
+ **Memoria:** 4 GB de RAM
+ **Gráficos:** Gráfica con 1024MB de VRAM
+ **Almacenamiento:** 4 GB de espacio disponible
+ **Tarjeta de sonido:** Soporta 7.1 surround sound


'The Station' no estará traducido al español, pero si eso no es problema para ti y te van los misterios espaciales, lo podrás obtener en su página de Steam el próximo 20 de febrero:


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/565120/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>
¿Piensas embarcarte en 'The Station?
 Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).
