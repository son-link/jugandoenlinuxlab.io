---
author: Pato
category: Estrategia
date: 2017-11-29 19:11:08
excerpt: "<p>El juego introducir\xE1 nuevos elementos y mapas tras su reciente expansi\xF3\
  n \"Road to Dunkirk\"</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/d503b79bddae5a3eeed71620c726e5d6.webp
joomla_id: 559
joomla_url: sudden-strike-4-muestra-el-desarrollo-de-sus-proximas-caracteristicas-en-un-video
layout: post
tags:
- rts
- estrategia
- sudden-strike-4
title: "'Sudden Strike 4' muestra el desarrollo de sus pr\xF3ximas caracter\xEDsticas\
  \ en un v\xEDdeo"
---
El juego introducirá nuevos elementos y mapas tras su reciente expansión "Road to Dunkirk"

Volvemos con Kalypso Media y su 'Sudden Strike 4'. Tras el lanzamiento de su expansión "[Road to Dunkirk](index.php/homepage/generos/estrategia/item/617-sudden-strike-4-se-amplia-con-road-to-dunkirk)" Kalipso Media ha anunciado en un [extenso artículo](http://blog.kalypsomedia.com/en/sudden-strike-4-paving-the-road/) en su blog de desarrollo titulado "Paving the Road" donde nos desvelan algunas de las próximas caracteristicas que aparecerán en el juego.


Una de las características principales del juego es su capacidad de crear mods y las herramientas para ello. Inicialmente el editor ha estado basado en Unity, pero ahora están trabajando para mejorarlo y hacerlo mas intuitivo y accesible a la comunidad. Junto a esta, los desarrolladores están trabajando desde los foros de la versión beta para introducir novedades y características en los próximos meses. Novedades de las que nos hacen un pequeño adelanto en un vídeo:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/W95cxlHINIU" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 Entre otras cosas, las novedades que podemos esperar son:


* modo clásico skirmish
* zoom y rotación de cámara
* Balanceo de comandancia
* IA mejorada
* Nuevos comandantes
* Nuevas campañas
* Nuevos continentes


Esperemos que sigan llegando noticias sobre el desarrollo de este más que interesante juego de estrategia en tiempo real. Si estás interesado en echarle un vistazo, lo tienes disponible en su página de Steam completamente en español:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/373930/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


¿Qué te parecen las novedades de Sudden Strike 4 que están por venir?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

