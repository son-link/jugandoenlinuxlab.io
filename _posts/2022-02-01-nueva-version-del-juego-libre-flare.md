---
author: leillo1975
category: Rol
date: 2022-02-01 18:24:23
excerpt: "<p>El equipo de desarrollo de este proyecto #OpenSource acaba de lanzar\
  \ la versi\xF3n 1.13.</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/flare/Flare_empyrean_4.webp
joomla_id: 1420
joomla_url: nueva-version-del-juego-libre-flare
layout: post
tags:
- rol
- open-source
- flare
- gpl3
title: "Nueva versi\xF3n del juego libre Flare"
---
El equipo de desarrollo de este proyecto #OpenSource acaba de lanzar la versión 1.13.


 Hacía ya bastante tiempo que no os hablábamos de este juego en JugandoEnLinux.com. Buscando en el historial de nuestra web, vimos que la última vez que hablamos fué con motivo de la salida de la [versión 1.0]({{ "/posts/publicada-la-version-1-0-de-flare-un-a-rpg-open-source" | absolute_url }}) hace casi 4 años...como pasa el tiempo. El caso es que **ayer mismo llegaba a la versión 1.13**, y creimos que era hora de recordaros que este gran juego libre aun sigue vivito y coleando.


Para quien no lo conozca, [Flare](https://flarerpg.org/) es un juego **escrito en C++ con SDL2**, y licenciado bajo la **GPL3**, que bebe de la esencia de títulos como **Diablo o Torchlight**, es decir, ese Rol de acción en tercera persona en perspectiva isométrica donde tendremos que ir explorando y limpiando escenarios, recogiendo objetos y subiendo niveles. También tiene un **interesante soporte de mods**, que le permite crear juegos enteros usando su motor.


**En esta versión 1.13 hay numerosas novedades**, como podeis ver en su [lista de cambios](https://flarerpg.org/2022/01/31/flare-1-13/), pero los más destacables son los siguientes:


***-Mejor comportamiento del gamepad:** Se ha dejado de usar la API SDL_Joystick para usar la API SDL_GameController. Esto implica un funcionamiento mucho más adecuado en los gamepads, usando las teclas estandar que podemos encontrar en los mandos de Xbox o PS. También se ha modificado el valor por defecto de la zona muerta y se han creado unas asignaciones predeterminadas para empezar a jugar sin configurar.*


***-Conjunto de equipos secundarios:** Un colaborador del proyecto, **r-a-cristian-93**, ha agregado soporte para multiples equipamientos, pudiendo alternar entre estos según necesitemos durante la partida.*


***-Niebla de Guerra:** El mismo colaborador de antes ha agregado esta función, que se puede usar para dibujar niebla oscura sobre mosaicos inexplorados y niebla ligera sobre mosaicos fuera del rango de visión. También es posible usar niebla de guerra para el minimapa. Ahora, solo las áreas que el jugador ha explorado se revelarán en el minimapa.*


***-Representación de fuentes mejorada:** En ocasiones el texto era difícil de leer, especialmente cuando se trataba de tamaños de fuente pequeños o grafías complejas. Al combinar texto con una copia de sí mismo, pude obtener un resultado mucho más claro.*


***-Mejoras en el Juego:** Las pociones de salud y maná ahora tienen variantes Super y Ultra que se pueden fabricar a través del sistema de alquimia. El Duende ladrón y la falda de alquimista también se han actualizado para que funcionen con todos los tipos de pociones disponibles. El poder Escudo de la clase Adepto se ha modificado para que sea más útil para los jugadores de todas las clases. En lugar de solo daño mental, el poder del escudo está determinado por una combinación del nivel del jugador, la estadística mental y la estadística de defensa. Por supuesto, los Adeptos seguirán siendo los mejores cuando se trata de usar el poder de manera efectiva.*


***-Cambios específicos en la plataforma Android:** Se han solucionado varios problemas para los jugadores en Android. Lo más importante es que Flare ahora debería funcionar correctamente en Android 10 o superior.*


Podeis haceros con este magnífico juego en **formato AppImage** en su sección de [descargas](https://flarerpg.org/download/). También, si estais interesados, podeis encontrar su código fuente en su [página de Github](https://github.com/clintbellanger/flare-game). Os dejamos con un gameplay del juego:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/c9SdhACeEzQ" title="YouTube video player" width="780"></iframe></div>


 


¿Conocíais este proyecto Open Source? ¿Qué os parecen los juegos tipo Diablo? Cuentanoslo en nuestro grupo de JugandoEnLinux en [Telegram](https://t.me/jugandoenlinux), o nuestras salas de [Matrix](https://matrix.to/#/#jugandoenlinux.com:matrix.org).


 

