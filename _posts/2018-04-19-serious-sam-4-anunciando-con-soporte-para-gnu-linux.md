---
author: Serjor
category: "Acci\xF3n"
date: 2018-04-19 21:40:39
excerpt: "<p>Acci\xF3n y humor se vuelven a juntar en una nueva iteraci\xF3n de la\
  \ saga Serious Sam</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/59cf578b8924155f92d236307362633c.webp
joomla_id: 714
joomla_url: serious-sam-4-anunciando-con-soporte-para-gnu-linux
layout: post
tags:
- serious-sam
- croteam
- e3
- serious-sam-4
title: Serious Sam 4 anunciado, con soporte para GNU/Linux
---
Acción y humor se vuelven a juntar en una nueva iteración de la saga Serious Sam

Con un trailer propio del humor de la saga de Serious Sam, Croteam, uno de los estudios que mejor cuida a sus jugadores, y a los linuxeros en especial, nos llega el anuncio de Serious Sam 4, Planet Badass.


Y poco más podemos comentar del juego, ya que no hay más datos, y como ellos mismos [dicen](http://www.croteam.com/serious-sam-4-planet-badass-official-reveal-coming-e3-2018/), hasta la conferencia de Devolver Digital en el próximo E3 no habrá más detalles.


Os dejamos con el trailer:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/v8eEXPhv-bg" width="560"></iframe></div>


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/257420/" style="border: 0px;" width="646"></iframe></div>


Y tú, ¿tienes ganas de que llegue este Serious Sam 4? Cuéntanoslo en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD)

