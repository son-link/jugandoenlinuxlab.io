---
author: Pato
category: Plataformas
date: 2017-12-12 17:37:14
excerpt: "<p>El t\xEDtulo ha sido portado por Ryan C. Gordon</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/edc0638bab3e8797f6e26ab82f87fc49.webp
joomla_id: 570
joomla_url: the-end-is-nigh-sale-de-fase-beta-y-ya-esta-disponible-en-linux-steamos-oficialmente
layout: post
tags:
- indie
- plataformas
- aventura
title: "'The End is Nigh' sale de fase beta y ya est\xE1 disponible en Linux/SteamOS\
  \ oficialmente"
---
El título ha sido portado por Ryan C. Gordon

La noticia nos llegaba hace unas horas en un tweet del propio Ryan:



> 
> The Linux port of The End is Nigh is no longer in a beta branch. If you own it on Steam, you can just install it directly now!
> 
> 
> — Ryan C. Gordon (@icculus) [December 12, 2017](https://twitter.com/icculus/status/940445379878891520?ref_src=twsrc%5Etfw)



 Como ya os adelantamos [en el mes de Octubre](index.php/homepage/generos/plataformas/item/632-lanzado-the-end-is-night-en-beta-abierta-para-linux-steamos), 'The End is Nigh' llegaba a Linux en fase beta y por fin ya lo tenemos disponible en nuestro sistema favorito.


'The End is Nigh' ha sido desarrollado por los creadores de juegos como [Super Meat Boy](http://store.steampowered.com/app/40800/Super_Meat_Boy/) o [The binding of Isaac](http://store.steampowered.com/app/113200/The_Binding_of_Isaac/) y donde tomaremos el control de Ash, uno de las pocos seres que han quedado vivos después del fin del mundo. En el debemos recoger durante más de 600 niveles,  pedazos de seres vivos para poder juntarlos y crearnos un amigo. También será importante coleccionar "tumores" para desbloquear nuevas zonas y minijuegos. Todo ello aderezado como siempre de un control y jugabilidad exqusitos marca de la casa.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/KJ71Wz90zt4" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 'The End is Nigh' no está disponible en español, pero si te va la temática y los juegos difíciles, y el idioma no es problema lo tienes disponible en su página de Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/583470/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 

