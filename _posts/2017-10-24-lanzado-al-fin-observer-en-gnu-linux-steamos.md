---
author: leillo1975
category: Aventuras
date: 2017-10-24 15:01:18
excerpt: "<p>Nos llega tras un par de meses de retraso con respecto a la versi\xF3\
  n de Windows.</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/ca0cf11bf5690b02c0c2b544bd40355c.webp
joomla_id: 503
joomla_url: lanzado-al-fin-observer-en-gnu-linux-steamos
layout: post
tags:
- cyberpunk
- terror
- aspyr-media
- observer
title: Lanzado finalmente Observer en GNU-Linux/SteamOS.
---
Nos llega tras un par de meses de retraso con respecto a la versión de Windows.

Todos los que habeis ido siguiendo las [noticias sobre este juego](index.php/component/search/?searchword=observer&searchphrase=all&Itemid=101) en nuestra web sabreis que se trata de una aventura ciberpunk de terror desarrollada por [Bloober Team](https://www.blooberteam.com/), conocidos también por el destacable [Layers of Fear](http://store.steampowered.com/app/391720/Layers_of_Fear/). Como en este último, la conocida compañía por portar juegos a nuestro sistema,  [Aspyr Media,](https://www.aspyr.com/) ejerce de editora.


 


También han sido publicados los requisitos técnicos para poder jugar a este título. Pese a no ser muy exagerados, conviene decir que se echa de menos el soporte para gráfias INTEL y AMD, aunque esto hay que tomárselo con muchas reservas como muchos sabreis, existen muchos juegos que oficialmente no las soportan y luego funcionan perfectamente. En caso de que nos enteremos si funciona o no con dichas gráficas os mantendremos informados. Sin más os dejo con estos requisitos:


 


* *Sistema Operativo: 16.04, 17.04, SteamOS 2.0*
* *Procesador: Intel Core i3 / AMD A8-6700*
* *Velocidad de Procesador: Intel (3.4 GHz), AMD (3.1 GHz)*
* *Memoria: 8 GB*
* *Espacio en Disco Duro: 10 GB*
* *Gráfica (NVIDIA): GeForce GT 680*
* *VRAM: 2 GB*


***Periféricos:**Microsoft® Xbox® 360 Controller para Windows® (cableado), Steam Controller*


 


El juego está ambientado en un futuro postapocalíptico (2084) donde eres el detective "neural" Lazarskiun (el conocido actor [Rutger Hauer](https://es.wikipedia.org/wiki/Rutger_Hauer) de **Blade Runner**), capaz de hackear e invadir las mentes de los sospechosos. Cualquier recuerdo, sentimiento o pensamiento puede ser usado contra tí en un juicio.  Como podeis ver en el trailer de lanzamiento se trata de un título muy adecuado para estás fechas, ya que la temática encaja perfectamente con Halloween (o [Samaín](https://es.wikipedia.org/wiki/Samhain), como lo llaman por mi tierra), por lo que aquellos que seais especialmente sensibles, tomaoslo con calma:


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/M-10n_U6FeQ" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


Si te gusta **Observer** puedes hacerte con el en Steam:


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/514900/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


¿Qué te parece el nuevo juego de Aspyr y Bloober Team? ¿Te atraen este tipo de aventuras? ¿Te gusta la temática Ciberpunk? Déjanos tu opinión en los comentarios o en nuestro canal de [Telegram](https://t.me/jugandoenlinux).

