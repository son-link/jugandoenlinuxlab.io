---
author: Serjor
category: Ofertas
date: 2018-07-31 19:10:30
excerpt: <p>Ofertas del mundo del motor muy interesantes</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/ccdc9739c83f032bb1c664f00a4afaf7.webp
joomla_id: 811
joomla_url: humble-sports-bundle
layout: post
tags:
- humble-bundle
- dirt-rally
- motorsport-manager
- football-manager
- ofertas
- humble-store
- f1-2017
title: Humble Sports Bundle
---
Ofertas del mundo del motor muy interesantes

Si os fijáis, habitualmente no solemos hacer ningún artículo sobre bundles, pero en esta ocasión, el pack que ha sacado humble bundle nos ha tocado la fibra, ya que en esta ocasión han sacado un bundle centrado en el deporte, y con una gran carga de juegos de conducción, así que no nos hemos podido resistir, y os animamos a que os hagáis con él, donde podréis conseguir el F1 2017 por unos 10€, un precio más que razonable para un juegazo como él.


Y no solamente os podéis hacer con el F1 2017, sino con el Dirt Rally y el MotorSport Manager, y un 75% de descuento para el Football Manager, disponibles con tan solo superar la media de compra (así que daros prisa para que la media os sea lo más baja posible).


Si os interesa, aquí tenéis el link de afiliado <https://www.humblebundle.com/games/sports-bundle?partner=jugandoenlinux> con el que si lo usáis para hacer la compra del bundle nos estaréis echando una mano.

