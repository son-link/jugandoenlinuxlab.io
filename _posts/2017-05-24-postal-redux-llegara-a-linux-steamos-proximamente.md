---
author: Pato
category: "Acci\xF3n"
date: 2017-05-24 08:49:01
excerpt: "<p>As\xED lo ha publicado el estudio responsable del juego \"Running with\
  \ Scissors\"</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/93ba6c299f04f710c35672b0f157402a.webp
joomla_id: 337
joomla_url: postal-redux-llegara-a-linux-steamos-proximamente
layout: post
tags:
- accion
- indie
- proximamente
- gore
- violento
title: "'POSTAL Redux' llegar\xE1 a Linux/SteamOS pr\xF3ximamente"
---
Así lo ha publicado el estudio responsable del juego "Running with Scissors"

Si alguna vez has oído hablar sobre la saga POSTAL, ya sabes de que va esto. Acción a raudales sin contemplaciones en una ciudad devastada por la violencia en la que tendrás que acabar con todo lo que se mueva. 


Si bien es cierto que ya teníamos varios de los títulos que "Running With Scissors", creadores de esta saga [POSTAL o Butcher](http://store.steampowered.com/search/?sort_by=Released_DESC&term=POSTAL&os=linux) ya nos habían traído a Linux, nos faltaba este 'POSTAL redux' que viene con gráficos renovados, multijugador cooperativo en línea y toda la acción y gore del original. Sin embargo, el estudio nos ha anunciado su llegada a nuestro sistema favorito para próximas fechas:



> 
> Coming soon! [#steam](https://twitter.com/hashtag/steam?src=hash) [#SteamOS](https://twitter.com/hashtag/SteamOS?src=hash) [#Linux](https://twitter.com/hashtag/Linux?src=hash) [@gamingonlinux](https://twitter.com/gamingonlinux) [pic.twitter.com/fDHQt8eGzq](https://t.co/fDHQt8eGzq)
> 
> 
> — RunningWithScissors (@RWSbleeter) [24 de mayo de 2017](https://twitter.com/RWSbleeter/status/867180505199820800)



 Estaremos atentos a los anuncios que el estudio haga para concretar más la fecha de lanzamiento. Mientras tanto, puedes deleitarte con el trailer de lanzamiento del juego:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/scHKZCCxHEI" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


¿Qué te parece 'POSTAL Redux'? ¿Te gustan los juegos de acción de este tipo?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 

