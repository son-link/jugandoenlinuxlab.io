---
author: Pato
category: Noticia
date: 2020-03-10 16:58:29
excerpt: "<p>Adem\xE1s llega la temporada Season of the<strong>&nbsp;</strong>Worthy\
  \ a Destiny 2 y captura de pantallas en PC</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Stadia/Stadiasteamworld.webp
joomla_id: 1188
joomla_url: dos-nuevos-juegos-llegan-hoy-a-stadia-steamworld-dig-y-steamworld-heist
layout: post
tags:
- stadia
title: 'Dos nuevos juegos llegan hoy a Stadia: Steamworld Dig y Steamworld Heist '
---
Además llega la temporada Season of theWorthy a Destiny 2 y captura de pantallas en PC


Las noticias semanales comienzan a ser costumbre desde Stadia. Hoy nos anuncian que llegan dos juegos ya anunciados previamente como son **Steamworld Dig y Steamworld Heist**.


*Dos nuevos juegos llegan a Stadia esta semana. SteamWorld Heist te permite vivir la fantasía de tu vida de robar y luchar contra naves enemigas como un pirata espacial robótico de la nave estelar (admítelo, siempre has querido ser uno). SteamWorld Dig es una reinvención completamente única del género de plataformas en el que simultáneamente navegas y moldeas el mundo que te rodea, esculpiendo el terreno para escapar de los enemigos y descubrir tesoros.*


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/nT3lUlLJZxg" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/-o5otg8xmVM" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


Ya se pueden adquirir ambos juegos al precio de 9,99€ para Steamworld Dig y 19,99€ para Steamworld Heist.


Por otra parte, también tenemos la llegada de **Destiny 2: Season of the Worthy** para todos los suscriptores de Stadia Pro, con lo que se completa Destiny 2 The Collection.


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/tZ7eDmNk_YE" width="560"></iframe></div>


Para terminar, Stadia recibe la posibilidad de **grabar tus partidas o capturar imágenes** con un clic desde un Pc como es nuestro caso, cosa que hasta ahora solo era posible desde dispositivos Android.


Tienes toda la información en el [post del anuncio](https://community.stadia.com/t5/Stadia-Community-Blog/This-Week-on-Stadia-New-games-new-season-new-feature/ba-p/16755) de Stadia.


 

