---
author: Pato
category: Noticia
date: 2018-12-13 10:54:42
excerpt: "<p>Si est\xE1s por Valencia o cerca este fin de semana, tienes una cita\
  \ en @LinuxCenterES</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/55df0111232e1c4fd8c02e91888d2313.webp
joomla_id: 930
joomla_url: jugando-en-linux-organiza-su-primera-lan-party-junto-a-linux-center-valencia
layout: post
title: Jugando en Linux organiza su primera Lan Party junto a Linux Center Valencia
---
Si estás por Valencia o cerca este fin de semana, tienes una cita en @LinuxCenterES

¿Te gustaría asistir a una LAN Party este fin de semana? ¿vas a estar en Valencia o cerca? si la respuesta a ambas preguntas es "si", tienes una cita con el juego en Linux en "**Linux Center Valencia**". Allí estaremos durante toda la jornada del Sábado jugando con nuestros equipos a una gran diversidad de juegos, tanto libres como comerciales.


No pierdas la oportunidad de asistir con tu propio equipo y jugar con nosotros. También tendremos algunos equipos a disposición de los asistentes para poder jugar entre otros a CSGO, Super Tux Kart, Xonotic y muchos más.


Además, tendremos torneos en los que podrás competir contra otros asistentes ¡y ganar premios ofrecidos por Slimbook! ¿Te lo vas a perder?


**La apertura de puertas será a las 10:30h y estaremos jugando hasta las 19:00h**. ¿Donde? En las instalaciones de Linux Center Valencia:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3076.6925217660673!2d-0.4663657486910868!3d39.54398987937536!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd605b3934a15aef%3A0xb5da19422cd48168!2sLinux+Center+Spain!5e0!3m2!1ses!2ses!4v1544701338714" style="border: 0px none; display: block; margin-left: auto; margin-right: auto;" width="600"></iframe></div>


 Si quieres apuntarte, o saber más detalles sobre torneos y la organización, puedes visitar:


<https://linuxcenter.es/aprende/proximos-eventos/40-lanparty-de-jugandoenlinux-com?date=2018-12-15-10-30>


¡No te pierdas la oportunidad de jugar con nosotros y conocernos!

