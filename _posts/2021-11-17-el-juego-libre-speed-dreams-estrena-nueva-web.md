---
author: leillo1975
category: Carreras
date: 2021-11-17 10:34:07
excerpt: "<p>Nuestro colaborador @SonLink ha creado la nueva p\xE1gina de este simulador\
  \ de carreras #OpenSource . #SimRacing</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SpeedDreams/SpeedDreams_web.webp
joomla_id: 1388
joomla_url: el-juego-libre-speed-dreams-estrena-nueva-web
layout: post
tags:
- open-source
- speed-dreams
- web
title: El juego libre Speed Dreams estrena nueva web
---
Nuestro colaborador @SonLink ha creado la nueva página de este simulador de carreras #OpenSource . #SimRacing


 Los que seguimos de cerca el desarrollo de este proyecto tan interesante y complejo, estamos al tanto de las novedades que cada cierto tiempo vemos en su [sitio de SourceForge](https://sourceforge.net/projects/speed-dreams/), pero es cierto que **una persona que hasta ahora buscase información sobre este juego, pensaría que está abandonado**, ya que su [obsoleta página oficial](http://speed-dreams.org/) **mostraba información competamente desactualizada**, así como sus cuentas de redes sociales. Este problema se debía a que la persona que gestionaba este tipo de cosas abandonó el proyecto y ha sido imposible contactar con él.


Para poner solución a este problema, nuestro colaborador [**@SonLink**](https://twitter.com/sonlink), que anteriormente colaboró creando el [Flatpak](https://flathub.org/apps/details/org.speed_dreams.SpeedDreams) y la **AppImage** del juego, ha creado una **nueva página web para el proyecto,** donde podremos encontrar toda la información y enlaces actualizadas para poder estar a la última en lo que a este proyecto de software libre se refiere. Este nuevo sitio tiene además la capacidad para **albergar el contenido en varios idiomas**, pudiéndose por ahora leerse en Inglés y Francés. Para ello, **el equipo de desarrollo de Speed Dreams ha cambiado de dominio** pasando de ser speed-dreams.org/ a:


<https://www.speed-dreams.net>
------------------------------


Ahora solo queda que le echeis un vistazo y descubrais el magnífico trabajo que han hecho tanto @SonLink con la nueva web, como los desarrolladores de Speed Dreams con sus contribuciones al proyecto. Podeis decirnos que os parece en los comentarios de este artículo, en nuestro grupo de JugandoEnLinux en [Telegram](https://t.me/jugandoenlinux), o nuestras salas de [Matrix](https://matrix.to/#/#jugandoenlinux.com:matrix.org).

