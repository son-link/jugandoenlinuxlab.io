---
author: Serjor
category: "Acci\xF3n"
date: 2018-03-30 18:14:36
excerpt: "<p>Borderlands y Fortnite se unen en un t\xEDtulo que promete partidas r\xE1\
  pidas e intensas</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/4ad6df0790d749b51a9680aaf0316ad3.webp
joomla_id: 697
joomla_url: crazy-justice-llegara-a-gnu-linux-antes-del-verano
layout: post
tags:
- crowdfunding
- battle-royale
- crazy-justice
- fig
title: "Crazy Justice llegar\xE1 a GNU/Linux antes del verano"
---
Borderlands y Fortnite se unen en un título que promete partidas rápidas e intensas

Leemos en [gamingonlinux](https://www.gamingonlinux.com/articles/cel-shaded-third-person-shooter-crazy-justice-will-have-a-battle-royale-mode-and-linux-support.11493) que Crazy Justice, un juego que se basa en modo tipo Battle Royale que tan de moda han puesto PUBG y Fortnite, con una estética que recuerda mucho a Borderlands, completó su campaña de crowdfunding en [FIG](https://www.fig.co/campaigns/crazyjustice?media_id=v288), y tendrá versión para GNU/Linux.


Y no solamente tendrá vesión para GNU/Linux, básicamente tendrá soporte para todo, PC, Xbox, PS4, Nintendo Switch y Mac, con juego cruzado entre todas las plataformas salvo PS4 (por temas de Sony). No obstante, tendrá modo campaña que ejercerá las veces de tutorial.


Para que os hagáis una idea os dejamos con el trailer para FIG:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/bm3SEDoA8vQ" width="560"></iframe></div>


Y tú, ¿qué piensas de este Crazy Justice? Cuéntanoslo en los comentarios, o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD)

