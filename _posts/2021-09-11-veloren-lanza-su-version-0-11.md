---
author: P_Vader
category: "Exploraci\xF3n"
date: 2021-09-11 08:59:38
excerpt: "<p>Hoy llega la versi\xF3n 0.11 de @velorenproject tras tres meses de trabajo"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Veloren/veloren_portada1_resize.webp
joomla_id: 1356
joomla_url: veloren-lanza-su-version-0-11
layout: post
tags:
- multijugador
- pixel
- rpg
- mundo-abierto
- veloren
title: "Veloren lanza su versi\xF3n 0.11"
---
Hoy llega la versión 0.11 de @velorenproject tras tres meses de trabajo.


Hoy sabado a las **20:00 (UTC+2) oficialmente se lanza el juego software libre [Veloren](https://veloren.net/) a su versión 0.11 y para ello se va a celebrar una fiesta en sus servidores a modo de mega partida.** Como ya hicimos en la anterior versión, desde **JugandoenLinux os invitamos a participar con nosotros**. Vamos a estar en [directo](https://www.twitch.tv/jugandoenlinux) y a daros unas nociones de qué es Veloren y cómo jugarlo, por si aun no lo has probado. Puedes [descargarlo](https://veloren.net/download) en su página web para Linux en muchos formatos.


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/l1oOjvaWJlw" title="YouTube video player" width="780"></iframe></div>


Después de tres meses de vertiginoso desarrollo como nos tienen acostumbrados desde este proyecto, os detallamos que hay de nuevo en esta versión.


**Añadidos nuevos:**


* Se ha añadido un árbol de habilidades para la minería, que gana experiencia por extraer minerales y gemas.
* La información de depuración de HUD ahora muestra el bioma y el sitio actual.
* Las mascotas ahora se guardan al cerrar la sesión 🐕 🦎 🐼
* Los NPC ahora pueden advertir a los jugadores antes de entrar en combate.
* Añadida una configuración del servidor para cambiar de PvE / PvP
* Persistencia del terreno experimental.
* El sistema 'spot', que genera escenarios y estructuras similares a sitios más pequeños.
* Variedades de castaños y cedros.
* Disparar a objetos, como manzanas y colmenas, puede derribarlos de los árboles.
* Asaltantes cultist, uno de los enemigos mas poderosos que te puedes encontrar.
* Control deslizante para efecto Bloom.


![Veloren Screenshot](https://media.discordapp.net/attachments/634860358623821835/884138091537907793/screenshot_1630865233900.png)


**Cambios**:


* El jefe Harvester ahora tiene nuevas habilidades e IA.
* Las partículas y efectos de sonido de muertes.
* Las transacciones ahora consideran si los artículos se pueden acumular en inventarios completos.
* El radio de la zona segura alrededor de la ciudad inicial se ha duplicado.


**Arreglos**:


* Las talleres de crafteo ya no se pueden explotar.
* Mejoró significativamente el rendimiento de la reproducción de efectos de sonido.
* Las fogatas ahora desaparecen cuando están bajo el agua.
* El mapa ahora se acercará a la posición del cursor y se arrastrará correctamente.
* No más tirones mientras corres por las pendientes con el planeador sacado.
* La cámara ahora se ajusta mucho menos.


![Velreon Screenshot](https://media.discordapp.net/attachments/634860358623821835/885141629902684180/bg_12.jpg)


Los desarrolladores hablan que se empieza a gestar las bases para hacer un mundo mas dinámico y detallado. Se han añadido montones de pequeños lugares con sus NPCs que interactúan con otros, en iglús, madrigueras, etc. Hasta ahora han desarrollado el mundo a gran escala, pero ha llegado el momento de comenzar enriquecerlo con mas detalles para explorarlo.


Como siempre recordaros que **este proyecto se sustenta por ahora de su [Open Collective](https://opencollective.com/veloren) y podéis colaborar con ellos principalmente a través de su [Discord](https://discord.gg/ecUxc9N) o [Matrix](https://matrix.to/#/#veloren-space:fachschaften.org).**


¿Preparados para la gran batalla? Cuentanos en [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org) que te parecec el juego y ¡apuntate a la fiesta!
