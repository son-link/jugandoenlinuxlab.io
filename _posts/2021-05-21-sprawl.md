---
author: P_Vader
category: "Acci\xF3n"
date: 2021-05-21 19:09:20
excerpt: "<p>Los desarrolladores @REVEL303 y @MissEmbyr nos confirman que tendremos\
  \ versi\xF3n de @SprawlFPS para nuestro sistema desde el primer d\xEDa.<span class=\"\
  r-18u37iz\">&nbsp;<br /></span></p>\r\n"
image: https://cdn.akamai.steamstatic.com/steam/apps/1549690/header.jpg
joomla_id: 1305
joomla_url: sprawl
layout: post
tags:
- cyberpunk
- fps
- parkour
title: "SPRAWL, un FPS estilo retro cyberpunk, llegar\xE1 nativo a Linux"
---
Los desarrolladores @REVEL303 y @MissEmbyr nos confirman que tendremos versión de @SprawlFPS para nuestro sistema desde el primer día.   



Según nos cuentan sus creadores, tras nueve meses de duro trabajo, planean que en poco mas de un año esté listo para su lanzamiento, siempre que no surja ningún problema. Pero también tienen claro que la versión en **Linux es una prioridad para ellos, comprobándola regularmente a la par que las otras plataformas en su desarrollo.**


El juego se inspira en el parkour de [Mirror's Edge](https://www.mirrorsedge.com/) y el estilo visual de Ghost in the Shell. Se están valiendo del motor Unreal 4 para su desarrollo y salvo una incidencia con la reproducción de videos en el menú, nos alegra saber que no están teniendo problemas con la versión de Linux.


Nos comentaban que Hannah ([@MissEnbyr](https://twitter.com/MissEmbyr/)) es una gran fan de Linux y ella lo usa tanto como puede. Por esa razón **quieren dar todo el soporte a nuestra comunidad y conseguir que Linux sea una plataforma viable para videojuegos.** ¡1000 puntos que se llevan desde jugandoenlinux!


Como anécdota hace unos días Hannah compartió un pantallazo desde su flamante Pop_OS! ejecutando su juego en Vulkan:


 ![sprawl screenshot linux](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/SPRAWL/sprawl_screenshot_linux.webp)


 


Y por último unos prometedores 15 minutos del juego:


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/YGjn8mHDwwI" title="YouTube video player" width="780"></iframe></div>


 


Ya puedes seguirlo o añadirlo a tus deseados en [Steam](https://store.steampowered.com/app/1549690/SPRAWL/) . ¿Qué te parece SPRAWL? Cuéntanos como lo ves en nuestros grupos de [Telegram](https://twitter.com/JugandoenLinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).


 

