---
author: leillo1975
category: Estrategia
date: 2021-04-29 14:09:22
excerpt: "<p>@feralgames y @CAgames nos traen de nuevo este cl\xE1sico @totalwar ambientado\
  \ en el antiguo imperio romano</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/TotalWar-Rome-Remasted/TWRR_Key-Art900.webp
joomla_id: 1302
joomla_url: lanzado-total-war-rome-remastered-con-soporte-nativo
layout: post
tags:
- feral-interactive
- total-war
- creative-assembly
- port
- nativos
- rome-remastered
title: 'Lanzado Total War: ROME REMASTERED con soporte nativo'
---
@feralgames y @CAgames nos traen de nuevo este clásico @totalwar ambientado en el antiguo imperio romano


 Ya os lo [adelantamos]({{ "/posts/total-war-rome-remastered-se-lanzara-en-linux" | absolute_url }}) hace poco más de un mes, y finalmente sus desarrolladores han sido fieles a su palabra. Esta **remasterización del clásico** juego de estrategia que vió la luz en el año 2004 ya puede ser adquirida, y lo mejor de todo con soporte nativo "made in" [Feral Interactive](https://www.feralinteractive.com/es/), esa compañía mítica que en el pasado nos traía los mejores juegos a nuestro sistema con una calidad excelente. El anuncio del lanzamiento acaba de ser realidad hace unos momentos en las redes sociales, como podeis ver en twitter:  
  




> 
> The stage is set, armies assembled and battle lines drawn – Total War: ROME REMASTERED is out now!  
>   
> Grab it from the Feral Store or Steam for £24.99/ $29.99/ €29,99. On Steam, save 50% if you own the original game.  
>   
> Feral: <https://t.co/8DRLw551DT>  
>   
> Steam: <https://t.co/i4e4gPlJ0R> [pic.twitter.com/om7WeB6hLQ](https://t.co/om7WeB6hLQ)
> 
> 
> — Feral Interactive (@feralgames) [April 29, 2021](https://twitter.com/feralgames/status/1387769354465447946?ref_src=twsrc%5Etfw)



En el artículo que escribimos hace un mes ya os adelantamos lo que encontrareis en esta remasterización, pero volvemos a recordaroslo:


***Imágenes mejoradas:** Total War: ROME REMASTERED introduce el clásico título de estrategia en la era de los juegos modernos con visuales 4K completos\*, soporte nativo para resolución de ultra alta definición y modelos de entorno, campo de batalla y personajes revisados.*  
 ***Nuevo contenido de juego:** Libra una guerra en nuevos frentes con 16 facciones anteriormente no jugables que se suman a las 22 originales, y envía a los nuevos agentes mercantes en misiones para establecer lucrativas redes comerciales en todo el mapa, comprar a los mercantes rivales y afirmar el poder económico de tu imperio.*   
 ***Características modernizadas:** Los jugadores pueden ejercer más control que nunca con nuevas características como un mapa táctico durante las batallas, además de mapas de calor y superposiciones de iconos en el modo campaña. También se han mejorado las mecánicas existentes, incluyendo un sistema de diplomacia revisado, niveles de zoom de la cámara más amplios en todo el juego y rotación de la cámara en el mapa de la campaña.*  
 ***Sistemas de ayuda mejorados:** Se ha añadido una franja de ayuda mejorada, que incluye un tutorial rediseñado, una nueva Wiki en el juego, consejos y descripciones ampliadas, y una accesibilidad mejorada para los jugadores daltónicos.*   
 ***Multijugador multiplataforma:** Los jugadores pueden disfrutar del multijugador multiplataforma entre Windows, macOS y Linux, una novedad en la franquicia Total War.*  
 *Contenido completo: Total War: ROME REMASTERED incluye las expansiones Invasión Bárbara y Alejandro con nuevos y gloriosos detalles, y los jugadores también tendrán acceso a la ROME original: Total War Collection (sólo para Windows).*


*\*La resolución de 4K y la ultra alta fidelidad se desbloquean descargando el paquete de gráficos mejorados gratuito de Steam.*


Como veis motivos más que de sobra para considerar su adquisición, especialmente si sois aficionados al género de la estrategia, y más concretamente a los juegos de la saga Total War. Nosotros por nuestra parte intentaremos, si tenemos la colaboración de Feral, realizar algún streaming en directo en nuestros canales de [Youtube](https://www.youtube.com/c/jugandoenlinuxcom) y [Twitch](https://www.twitch.tv/jugandoenlinux) mostrándoos el juego, así como un futuro review en profundidad del título.


Podeis comprar Total War: ROME REMASTERED en la [Humble Store](https://www.humblebundle.com/store/total-war-rome-remastered?partner=jugandoenlinux) (**patrocinado**), la [tienda de Feral](https://store.feralinteractive.com/es/mac-linux-games/rometwremastered/) (recomendado) o en [Steam](https://store.steampowered.com/app/885970/Total_War_ROME_REMASTERED/). Os dejamos con trailer oficial del lanzamiento en Linux:


  
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="440" src="https://www.youtube.com/embed/Inlz6vOvwvk" style="display: block; margin-left: auto; margin-right: auto;" title="YouTube video player" width="780"></iframe></div>


¿Qué os parece este "nuevo" lanzamiento de Feral Interactive? ¿Habeis jugado al título original? Podeis dejarnos vuestras impresiones y opiniones sobre el lanzamiento de este juego en los comentarios en nuestros grupos de JugandoEnLinux en [Telegram](https://t.me/jugandoenlinux) o  [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

