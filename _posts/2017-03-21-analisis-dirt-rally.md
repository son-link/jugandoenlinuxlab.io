---
author: leillo1975
category: "An\xE1lisis"
date: 2017-03-21 08:15:00
excerpt: "<p>Desgranamos las caracter\xEDsticas de lo \xFAltimo de Feral</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/ba0166c7a50d96eb270097f3f911e08a.webp
joomla_id: 240
joomla_url: analisis-dirt-rally
layout: post
tags:
- feral-interactive
- codemasters
- dirt-rally
title: "An\xE1lisis: DIRT Rally"
---
Desgranamos las características de lo último de Feral

5, 4, 3 ,2, 1....YA! Arrancamos este análisis derrapando las ruedas de probablemente el mejor y más fiel juego de Rally's hasta la fecha. De la mano de Codemasters y con el encomiable trabajo de Feral Interactive portando el título, nos llega un juego más de carreras, que viene a completar la experiencia de conducción iniciada con **GRID Autosport**, donde podíamos conducir en 5 pruebas diferentes (Turismo, Monoplazas, Resistencia, Tuning y Urbano); y que luego ampliariamos con **F1 2015**, donde nos poníamos a los mandos de los más potentes monoplazas a lo largo de los circuitos que comforman el campeonato, teniendo este licencia oficial de la FIA. Hay que resaltar también la disponibilidad para Linux de **DIRT Showdown,** aunque en este caso en una variante totalmente arcade, y de la mano de la compañía Virtual Programming.


 


Haciendo un poco de historia, hay que comentar que toda esta aventura de los Rallyes y Codemasters empezó hace ya un montón de años (1998) con Colin McRae Rally, continuando hasta nuestros días con sus revisiones (2, 3, 4, 2005, DIRT y DIRT2), y pasando después solo a llamarse DIRT 3 a secas en la versión de 2011, con el "spinoff" de su versión Showdown un año más tarde. Es innegable la calidad de estos juegos a lo largo del tiempo, teniendo casi siempre muy buena acogida tanto por parte del público en general, como por los aficionados a los Rallys. En esta ocasión es importante destacar que Codemasters decidió abrir su ciclo de desarrollo al público, haciendo de este juego un título con acceso anticipado ("Early Access"), y siendo esta decisión un completo éxito, ya que le ha permitido recopilar mucha información de los usuarios para pulir y mejorar hasta el extremo este juego.


 


![DiRT Rally Wales Banner E](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisDIRTRally/DiRT_Rally_Wales_Banner_E.webp)


 


Pero dejando atrás los otros títulos de Codemasters para nuestro sistema vamos a centrarnos en el que nos ocupa. Lo primero que hay que decir con respecto a DIRT Rally es que, aunque accesible, **es un simulador**, por lo que podeis ir olvidandoos de ir completando etapas sin más dificultad que frenar antes de ciertas curvas. Aquí hay que tener en cuenta un montón de factores importantes, empezando por saber el coche que tenemos entre manos, desde su potencia a como realizar sobrevirajes (derrapes) si tenemos un coche de tracción trasera o delantera. También es importante saber la tracción de la que disponemos en cada momento en función al tipo de la superficie que pisamos (aderencia), así como la repartición de las fuerzas que actuan en nuestro coche en los diversos momentos de la carrera. Tendremos que anticipar, con la ayuda de nuestro copiloto, los trazados para conseguir dominarlos, lo cual os aseguro no es nada fácil. Además repetir etapas hace que perdamos créditos. Dar gas a fondo constantemente en este juego no sirve de nada; es más, es mucho más efectivo ser prudentes e ir aprendiendo poco a poco los "intringulis" de nuestro coche. Una cosa que eché en falta, pero que está más que justificado que no esté, es el poder usar Flashbacks cuando cometemos un error (GRID Autosport, F1 2015), pero como dije al principio, el juego es un simulador donde cada error tiene una consecuencia.


 


Hablando de **coches**, es aquí donde se ha visto mayor empeño por parte de Codemasters, ya que a parte de ser variados, estan muy detallados, y no solo a nivel gráfico, sino en cuanto a sus características a nivel de **físicas** (aceleración, velocidad, tracción, maniobrabilidad, etc). Encontraremos desde coches antiguos hasta los más nuevos (a pesar de no disponer licencia oficial de WRC),  siendo los primeros los más adecuados para los envites iniciales del juego, sobre todo si son con tracción delantera, los cuales son bastante más fáciles de controlar que los de trasera, al menos para mi. Entre los coches míticos que nos encontraremos está el Renault Alpine, El FIAT Abarth 131 Miriafiori, el Lancia Delta, el Subaru Impreza o el Ford Fiesta RS.


 


![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisDIRTRally/DRTR_Announce_20E.webp)*El mini es un coche perfecto para empezar a jugar*
 


Existen mutitud de circuitos, donde encontraremos **tierra, grava, asfalto y nieve**; y donde por supuestro tendremos que modificar nuestra forma de conducir para adaptarnos a cada uno de ellos. Incluso podemos afrontar con etapas donde podemos, por ejemplo, encontrar nieve y asfalto, algo que notaremos inmediatamente en el agarre y físicas del coche. Si acompañamos a esto que puede llover, nevar y aun encima que sea de noche , ya os podeis hacer una idea. En cuanto a los rallys que visitaremos, podremos ir a **Grecia, Finlandia, Suecia, Gales, Alemania** y mi favorito, **Montecarlo**. Cada uno de ellos dispone de una buena ristra de tramos de lo más variado, por lo que no repetiremos muy a menudo, y más si las condiciones climatológicas varían.


 


El juego se puede usar con un gamepad, yo he probado mi Steam Controller sin problemas, y supongo que el de XBOX irá perfecto; pero **se disfruta y domina mucho mejor con un buen volante**. Si disponeis de uno con sistema de respuesta (force feedback), enseguida notareis que su dureza varía según el terreno o la aderencia en el momento determinado. Para ser más concreto, notareis los baches de la carretera en el volante, teniendo que agarraros bien a este si no quereis que este se os escape; o cuando ejecuteis un salto en un desnivel, notareis que el volante se queda suelto. También su uso se hace casi indispensable para realizar los derrapes realizando giros bruscos, tanto para empezarlos como para corregirlos.


 


![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisDIRTRally/DRTR_205_Group_B_Interior_RE.webp)*El uso de un buen volante es altamente recomendable si quieres una buena inmersión en el juego*
 


Durante el juego sufriremos bastantes percances que van a tener su repercusión en el coche, por lo que debeis pensar que cada accidente que tengais va a tener consecuencias negativas en la conducción, haciéndo esta tarea en ocasiones casi inviable, por ejemplo si pinchamos una rueda, lo que provocará que tomar una curva en la dirección contraria al neumático dañado sea muy complicado. Además estos accidentes nos costarán créditos y tiempo si en mitad de un rally queremos reparar nuestro vehículo.


 


En cuanto a los gráficos, hay que decir que al ser un juego de conducción de Codemasters, hace uso del motor gráfico EGO, lo que le otorga una solvencia y calidad más que destacable. Si bien el juego no necesita hacer un alarde muy extenso de de gráficos, es justo decir que los paisajes, coches, efectos climatológicos, polvo, agua, etc, están muy bien implementados, dando una sensación constante de realidad. Uno de los aspectos negativos en cuanto al apartado gráfico es la poca cantidad de público y lo estáticos que son; y en este caso supongo que Codemasters lo ha decidido en función de mejorar el rendimiento. El juego se comporta de forma excelente, incluso en el ajuste gráfico más elevado (Ultra). Basta decir que con mi configuración, muy modesta por cierto (i5-3550, GTX950, 16GBRam), en muy pocas ocasiones bajó de los 60FPS a 1080p. He realizado diversas pruebas de rendimiento con el Benchmark que incluye el propio juego en los diferentes niveles de ajuste y los números que ha ofrecido hablan por si solos. Podeis verlos en la siguiente tabla:


 




|  |  |  |  |  |  |
| --- | --- | --- | --- | --- | --- |
| **FPS / Detalle** | **Muy Bajo** | **Bajo** | **Medio** | **Alto** | **Ultra** |
| **Medios** | **228.48** (252.20) | **107.65** (117.26) | **92.39** (98.83) | **75.28** (78.20) | **66.89** (53.14) |
| **Mínimos** | **179.19** (229.61) | **82.31** (93.37) | **70.05** (81.28) | **61.79** (67.70) | **57.26** (42.80) |
| **Máximos** | **286.69** (273.19) | **144.58** (164.35) | **127.76** (135.22) | **103.50** (109.62) | **96.14** (64.59) |


 


Como veis son unas cifras bastante altas, que van a permitir mover con solvencia el juego en niveles de detalle aceptables para la mayoría de las configuraciones. En este sentido es justo decir que parece que Feral ha dado con la tecla a la hora de adaptar este juego consiguiendo un rendimiento muy cercano a la versión de Windows (en la tabla los valores entre paréntesis), **superándolo incluso en modo Ultra.** Supongo que la experiencia de portar otros juegos con el motor EGO ha servido para algo (GRID Autosport y F1 2015) y en esta ocasión lo han clavado.


 


![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisDIRTRally/DRTR_Announce_16E.webp)*El agua en los cristales está muy conseguida, pero no nos facilitará para nada la tarea de conducir*
 


Hablando de las vistas disponibles en el juego, dispone de una buena cantidad, siendo las más recomendables las interiores, donde tendremos una sensación de conducción mucho más real. Es de resaltar el esfuerzo que han hecho los desarrolladores en recrear el interior de los coches, algo que se agradece bastante sobre todo si lo comparamos con GRID Autosport, donde la cámara interior provocaba sonrojo al difuminar los detalles del interior del coche. En este caso podemos mirar completamente el cuadro de mandos e incluso ver sentado a nuestro lado al copiloto. Una cosa que me ha llamado la atención es que la vista interior es sensible a las irregularidades de la carretera, por lo que si pisamos un bache la cabina se moverá como si realmente fuésemos dentro del coche. Por supuesto también tendremos cámaras exteriores, que dan un aspecto más arcade al título, pero que siendo este juego un simulador, no tienen mucho sentido.


 


![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisDIRTRally/DIRT_Vista_TraseraE.webp)Dispondremos también de vistas traseras
 


En cuanto a los modos de juego hay que decir que no solo dispondremos de las clásicas etapas de Rally, sino que también tendremos subida a la montaña (**Pikes Peak**) y carreras con otros coches mano a mano en el **Rallycross** en tres circuitos reales diferentes. Podremos seguir una carrera profesional (**Trayectoria**) con varios niveles de dificultad, y donde iremos progresando poco a poco y no solo nos haremos con más y más coches, sino  también con mejores **mecánicos** que nos otorgarán un mejor rendimiento en nuestros coches y menos tiempo necesario para repararlos.También podremos hacer **Copas personalizadas** o **carreras a medida**. Por supuesto incluye un modo **multijugador** para medirnos con otros jugadores, y el ya conocido **Racenet**, donde quedarán registrados nuestros tiempos y podremos compararlos con los de nuestros amigos


 


![](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisDIRTRally/DRTR_Peugeot_RX_Hell_Alt_2_AE.webp)*Podemos medirnos directamente con otros coches las pruebas de Rallycross*
 


Sobre el sonido hay que resaltar que es muy real y está perfectamente implementado. No encontraremos música, como es lógico durante las pruebas. Podremos escuchar con fidelidad el sonido de nuestro motor, los derrapes, suspensión, la gravilla, etc, además por supuesto de las indicaciones de nuestro copiloto, que efectivamente nos guiará durante nuestros tramos en perfecto Castellano. También están completamente doblados los tutoriales, que como antes comenté es muy recomendable visionar, especialmente si somos neófitos en el mundo de los Rallys, como es mi caso.


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/3USVRp0j8sc" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


Como conclusión, DIRT Rally es en este momento el mas fiel y mejor juego de conducción de cuantos disponemos en nuestro sistema. El gran trabajo de Codemasters ha dibujado un retrato fiel, pero a la vez divertido del mundo de los Rallys, consiguiendo un juego que provoca unas sensaciones y una inmersión soberbias. Es claramente el más digno suscesor de la mítica saga de Rallys a la que pertenece. Un título que te anima a superarte en cada tramo, cada curva, cada desnivel..., y donde querrás aprender a dominar cada coche con sus particularidades. Por supuesto, es de elogiar el buen hacer de Feral, que ha sabido adaptar a la perfección el juego, consiguiendo que no haya apenas diferencias con la versión de Windows, y que la calidad y sensaciones sean exactamente las mismas, pudiendo disfrutar de él en casi cualquier equipo.


 


Si quieres comprar DIRT Rally para GNULinux/SteamOS puedes hacerlo directamente en la **[tienda de Feral](https://store.feralinteractive.com/es/mac-linux-games/dirtrally/)** o en Steam:


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/310560/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


 ¿Que te ha parecido el análisis? ¿Estas de acuerdo? ¿Añadirías algo? Puedes responder a estas preguntas o escribir lo que quieras en los comentarios, o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD)

