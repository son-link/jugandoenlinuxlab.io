---
author: leillo1975
category: Editorial
date: 2017-05-03 08:50:23
excerpt: "<p>Tambi\xE9n ofrece estadisticas de este para ver los tiempos de resoluci\xF3\
  n</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/85217272b4e7187cce0880e98f060661.webp
joomla_id: 301
joomla_url: steam-mejor-su-servicio-de-soporte
layout: post
tags:
- steam
- estadisticas
- soporte
title: Steam mejora su servicio de Soporte.
---
También ofrece estadisticas de este para ver los tiempos de resolución

Acabamos de recibir la noticia de que Steam ha mejorado sus servicios de soporte. Según la [nota remitida](http://steamcommunity.com/games/593110/announcements/detail/1301948399251160549), los chicos de Valve nos indican que han escuchado a la comunidad y han hecho un esfuerzo **aumentado el número de personas dedicadas a dar soporte** en la plataforma. Ciertamente esta era una demanda recurrente en los últimos tiempos, ya que este servicio registraba tiempos demasiado largos de respuesta, de incluso más de un día (y dos). También se han facilitado [estadísticas de las peticiones de soporte](http://store.steampowered.com/stats/support/) y su tiempo de resolución, viendo en estas que estos últimos se han reducido considerablemente en dos meses, como podeis ver en el siguiente gráfico.


 


![StatsSoporte](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/StatsSoporteSteam.webp)


 


Steam también ha mejorado sus herramientas de soporte, e incluido más opciones de autoservicio. Es muy importante destacar que a partir de ahora **no tendremos que tener una cuenta independiente para el soporte**, sinó que funcionará con la de la tienda. Esto la verdad es que me parecía un sinsentido y un engorro. Como dice la noticia, esperemos que nunca tengas necesidad de usar el soporte de Steam, pero si es así, siempre se agradece el poder disponer de un mejor servicio.


 


¿Que te parece esta noticia? ¿Has tenido que lidiar alguna vez con el soporte de Steam? Dejanos tus impresiones y experiencias en los comentarios de este artículo o en los canales de Jugando en Linux en [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).


 


 

