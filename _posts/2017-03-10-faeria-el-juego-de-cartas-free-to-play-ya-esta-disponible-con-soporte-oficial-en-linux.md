---
author: Pato
category: Estrategia
date: 2017-03-10 12:32:00
excerpt: "<p>El juego llevaba un a\xF1o en acceso anticipado, pero el soporte en Linux\
  \ a\xFAn no era completo</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/556202461533c16857a46ed5bfb7c21c.webp
joomla_id: 243
joomla_url: faeria-el-juego-de-cartas-free-to-play-ya-esta-disponible-con-soporte-oficial-en-linux
layout: post
tags:
- steam
- free-to-play
- estrategia
- cartas
title: "'Faeria' el juego de cartas \"free to play\" ya est\xE1 disponible con soporte\
  \ oficial en Linux"
---
El juego llevaba un año en acceso anticipado, pero el soporte en Linux aún no era completo

Faeria [[web oficial](https://www.faeria.com/)] es un juego de cartas "free to play" donde podrás diseñar tu mazo y combatir en batallas épicas con unas mecánicas que hacen al juego muy interesante. El concepto de juego "facil de jugar, dificil de dominar" suele ser sinónimo de diversión.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/1XAaVqtVkfg" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


El juego que ha pasado por "acceso anticipado" mantenía una versión de Linux que no estaba aún soportada de manera oficial, pero esto ha cambiado de cara al lanzamiento y ya se puede jugar el Linux con todas las de la ley.


Faeria tiene una comunidad activa y quiere hacerse un hueco en los e-sports. Personalmente le he dedicado algunas sesiones pero no me ha terminado de enganchar, lo que no quita para que sea un buen juego de cartas y estratégia.


Si quieres darle una oportunidad, lo tienes en su página de Steam, además disponible en español:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/397060/" width="646"></iframe></div>


 Si juegas o vas a Jugar a 'Faeria' cuéntame qué te parece en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

