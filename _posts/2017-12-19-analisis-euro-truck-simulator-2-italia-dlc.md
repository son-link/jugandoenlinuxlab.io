---
author: leillo1975
category: "An\xE1lisis"
date: 2017-12-19 10:26:30
excerpt: "<p><span id=\"result_box\" class=\"short_text\" lang=\"it\"><span class=\"\
  \">\xA1Bella e geniale</span></span>!</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/7faf56e5874604be96ba8fa8c4e07ea4.webp
joomla_id: 566
joomla_url: analisis-euro-truck-simulator-2-italia-dlc
layout: post
tags:
- ets2
- dlc
- scs-software
- euro-truck-simulator-2
- italia
title: 'Analisis: Euro Truck Simulator 2: Italia DLC'
---
¡Bella e geniale!

De todos es sabido que la laboriosidad es la tónica general en las oficinas de SCS Software. Como se suele decir de forma coloquial, ultimamente están "sembraos" pues no paran de sacar DLC's. En menos de un mes y medio han hecho públicas las expansiones de [Nuevo México](index.php/homepage/analisis/item/656-analisis-american-truck-simulator-dlc-new-mexico) (ATS), [Special Transport](index.php/homepage/generos/simulacion/item/696-ets2-special-transport-dlc-disponible) (ETS2) y la que nos ocupa, **Italia**. Otra de las señas de identidad de la compañía checa es la calidad de sus productos y el respeto por sus clientes. En más de una ocasión hemos comentado que SCS no se conforma con mantener sus juegos ante posibles fallos, sinó que están constantemente mejorándolos, dotando a su simulación de más características, profundidad y calidad.


Como habíamos apuntado en la [noticia](index.php/homepage/generos/simulacion/item/687-disponible-italia-la-nueva-dlc-para-euro-truck-simulator-2) del anuncio de "Italia", **previamente se había actualizado la versión del juego base a la [1.30](http://blog.scssoft.com/2017/12/euro-truck-simulator-2-update-130.html)**, como preparación del terreno para el desembarco final de la expansión, algo que ya habíamos visto poco antes de la salida de [Vive la France!](index.php/homepage/analisis/item/243-analisis-ets2-vive-la-france-dlc). En esta actualización a parte de muchas otras características se ha remodelado toda la parte de Italia que ya salía en el juego base para adaptarla al aspecto general de la expansión, teníendo especial protagonismo la reconstrucción de Milán. Entre otras cosas también se añadieron dos nuevos camiones Scania (Series S y R) y un mejor funcionamiento de la navegación GPS.


![ETS2ItaliaMap](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisETS2BellaItalia/ETS2ItaliaMap.webp)


*En esta ilustración podemos ver la red de carreteras que incluye esta DLC*


 Lo primero que vamos a notar en cuanto entremos en [Italia](http://blog.scssoft.com/2017/12/italia-dlc-just-arrived.html) es que, como es lógico, la señalización cambia (peajes, letreros, matrículas, banderas....), pero no todo es carretera. Para esta expansión **se ha ampliado la variedad de la  vegetación**, y esto se nota de norte a sur, donde a lo largo de la bota **disfrutaremos de diferentes climas**, siendo más alpino al norte y más mediterraneo según avanzamos hacia el sur.  La **Arquitectura** tanto de los edificios como de las viviendas unifamiliares es puramente italiana, y se nota que han trabajado mucho sobre ella. Incluso los almacenes que visitaremos durante el juego tienen sus propias características que los diferencian de los del resto del juego. Otra cosa que llama la atención es que recoge incluso algunas **costumbres de los habitantes** (coches mal aparcados), e incluso detalles como la basura sin recoger o las ruinas en el sur.


Conducir por las "Autoestrades" puede ser muy agradable con esos maravillosos entornos que tiene el pais italiano, pero al igual que en "Vive la France!", la verdadera sensación de transitar por el pais transalpino reside en **tomar las vias alternativas y sumergirse en las zonas rurales**, donde el trabajo de SCS es abrumadoramente brillante. Podremos visitar de esta manera [pueblos](https://youtu.be/I6_bMbWelzs?t=25m35s), que aunque no esten señalados en el mapa como localidades de interés, resultan  encantadores y desprenden la esencia de Italia por los cuatro costados. Son estos detalles los que hacen que "Italia" sea claramente la mejor expansión que sus desarrolladores han creado hasta la fecha. 


![ETS2 Italia Scorzo](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisETS2BellaItalia/ETS2_Italia_Scorzo.webp)


*El trabajo realizado en los pueblos Italianos es magnífico*


  Como viene siendo habitual en sus últimos trabajos, **las ciudades están ahora mucho mejor representadas que antes**, al menos las más importantes. Si sois jugadores habituales del juego sabreis que cuando toda esta aventura de ETS2 empezó, las urbes no eran más que un polígono industrial con un paisaje lejano donde podíamos advertir que había una ciudad. Ahora podremos internarnos en ellas y descubrir en muchos casos sus características propias, pudiendo circular bastante tiempo por sus calles, especialmente en las más importantes como puede ser Milán, Roma o Nápoles.


También es digno de mención la inclusión de **industrias y almacenes propios**, encontrando por ejemplo en [zonas portuarias](https://youtu.be/ctr-wJvdqCM?t=35m19s) los típicos almacenes "de toda la vida", con su anticuada y particular construcción, o los [almacenes agrícolas](https://youtu.be/I6_bMbWelzs?t=30m7s) en las zonas rurales donde se advierten industrias vitivinícolas o aceiteras. Por supuesto encontraremos **puntos de referencia** (landmarks) de lugares tan conocidos como pueden ser los Montes Vesubio en Nápoles y Etna en Sicilia. Otro detalle que está muy bien es la **inclusión en el tráfico de vehículos puramente italianos**, topándonos con modelos claramente inspirados en las marcas italianas de coches y vehículos industriales, incluso deportivos al más puro estilo "Cavallino Rampante" o Toro Bravo. Tendremos que ser prudentes porque por supuesto los "**Carabinieri**" vigilarán que no cometamos infracciones de tráfico.


![ETS2 It4](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisETS2BellaItalia/ETS2_It4.webp)


*La arquitectura Italiana está muy conseguida y sumerge aún más al jugador en esta expansión.*


 Podemos entrar aún más al detalle y enumerar las mil y una características que nos vamos a encontrar en esta expansión, pero aquí **lo que realmente importa es la sensación general que produce**. La recreación de los diferentes paisajes y elementos del entorno es simplemente espectacular y facilmente reconocible, sobre todo si habeis tenido la recomendable oportunidad de visitar este hermoso pais en la vida real. El atravesar túneles y viaductos por los [Apeninos](https://es.wikipedia.org/wiki/Apeninos) a lo largo  y ancho del mapa, o descubrir la maravillosa costa mediterranea, será una actividad que os traerá de vuelta esos recuerdos imborrables de vuestro pasado. Si leisteis el análisis que hicimos hace poco de [ATS: Nuevo México](index.php/homepage/analisis/item/656-analisis-american-truck-simulator-dlc-new-mexico), pudisteis ver que la sensación general de la conducción por sus carreteras era su abrumadora naturaleza que podía incidir en una buscada sensación de agradable soledad y melancolía. En el caso de Italia, es lo contrario, pues el paisaje invita al optimismo, la alegría y **está cargado de matices que hacen mucho más dinámica la sensación de conducir por sus vias**.


En cuanto los **aspectos negativos**, con respecto a esta expansión lo único que se puede echar en falta es la **no inclusión de la isla de Cerdeña**, que por supuesto también forma parte de la República Italiana. Sería muy positivo poder disfrutar de esta región mediante un parche en un futuro, pues así parece que la DLC está incompleta.


![ETS2BISic2](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/AnalisisETS2BellaItalia/ETS2BISic2.webp)


*Las vistas en esta DLC nos harán parar el camión en más de una ocasión para deleitarnos contemplándolas*


 Como punto final y a modo de conclusión, Euro Truck Simulator 2: "Italia" DLC es un trabajo **SOBERBIO** por parte de SCS. Sin ningún tipo de duda se trata de **la mejor expansión que han sacado hasta la fecha**, donde el nivel de detalle alcanza cotas sorprendentes y el cariño que han puesto se ve en cada palmo del mapa.  La ambientación e inmersión que produce es uno de sus puntos a favor. Por supuesto encantará a todos los admiradores de este hermoso y alegre pais. Una **compra absolutamente obligada** para todos los fans de este juego y que estamos completamente seguros de que **no va a defraudar a ningún usuario**. Esperemos que la próxima ampliación del mapa en ETS2  caiga encima de esta piel de toro y podamos circular por "Iberia".


De forma ilustrativa, en JugandoEnLinux.com hemos grabado un par de videos conduciendo de norte a sur por las carreteras de esta DLC, mientras os comentamos diversos aspectos que hemos ido encontrando al jugar. Podeis ver dichos videos justo aquí debajo:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/ctr-wJvdqCM" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/I6_bMbWelzs" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


**Este artículo junto con los videos ha sido realizado gracias a la copia facilitada por SCS Software**. Si sois ya "camioneros" y os ha convencido este análisis podeis comprar ETS2 "Italia" DLC en Steam:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/558244/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 ¿Sois jugadores de Euro Truck Simulator? ¿Que os parece el trabajo hecho con Italia? Puedes responder en los comentarios o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

