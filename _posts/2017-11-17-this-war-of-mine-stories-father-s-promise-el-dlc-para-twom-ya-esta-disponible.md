---
author: Pato
category: Aventuras
date: 2017-11-17 11:14:56
excerpt: <p>Expande el universo de 'This War of Mine' con una nueva historia</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/5003d452a8da016f3ed02a6385cf54e8.webp
joomla_id: 542
joomla_url: this-war-of-mine-stories-father-s-promise-el-dlc-para-twom-ya-esta-disponible
layout: post
tags:
- indie
- aventura
- supervivencia
- violento
title: "'This War of Mine: Stories - Father's Promise', el DLC para TWoM ya est\xE1\
  \ disponible"
---
Expande el universo de 'This War of Mine' con una nueva historia

Hace tan solo unos días que llegó este DLC para 'This War of Mine' [[Steam](http://store.steampowered.com/app/282070/This_War_of_Mine/)], una emotiva aventura sobre la supervivencia y las vicisitudes de la guerra vistas desde el lado de los civiles. En concreto, 'This War of Mine: Stories - Father's Promise' se ambienta en el mismo universo que TWoM y donde tendremos que meternos en la piel de una familia que trata de conservar el último atisbo de humanidad en tiempos de crueldad.



> 
> *Conviértete en Adam, un padre que intenta salvar a su hija del horror de la guerra escapando de la ciudad sitiada. Sigue sus pasos y descubre una crónica de amor, odio y pérdida, emociones que todos compartimos en las horas más oscuras.*
> 
> 
> 


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/ORQCI1JrERk" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 Este DLC expande el juego original con :


– Una nueva trama basada en la obra de radioteatro del famoso autor polaco Łukasz Orbitowski.  
– 4 lugares completamente nuevos.  
– 5 lugares reconstruidos y remasterizados.  
– Mecánicas nuevas: sistemas de diálogo y búsqueda de pruebas.  
– Objetos nuevos relacionados con la historia.


Este es el primer DLC de la serie 'This War of Mine: Stories' de la que tendremos nuevas entregas a partir de este próximo año.


Si etás interesado en expandir tu experiencia en This War of Mine, tienes este DLC en su página de Steam donde está traducido al español:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/750030/220242/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 

