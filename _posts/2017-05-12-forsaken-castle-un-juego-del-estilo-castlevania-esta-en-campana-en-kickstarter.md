---
author: Pato
category: Aventuras
date: 2017-05-12 15:42:32
excerpt: "<p>El juego ya ha alcanzado su meta de financiaci\xF3n y va a por nuevos\
  \ objetivos</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/9feeafa8e740fdbe632b43236148b194.webp
joomla_id: 318
joomla_url: forsaken-castle-un-juego-del-estilo-castlevania-esta-en-campana-en-kickstarter
layout: post
tags:
- accion
- plataformas
- aventura
- kickstarter
title: "'Forsaken Castle' un juego del estilo Castlevania est\xE1 en campa\xF1a en\
  \ Kickstarter"
---
El juego ya ha alcanzado su meta de financiación y va a por nuevos objetivos

Creo que a estas alturas todos sabeis que me gustan los juegos de estilo "Castlevania". En este caso se trata de casi un clon, ya que este 'Forsaken Castle' [[web oficial](http://duckblock.com/forsaken-castle/)] es casi un calco del juego de Konami. 


Gracias a [www.thelinuxgameconsortium.com](https://linuxgameconsortium.com/linux-gaming-news/forsaken-castle-hits-first-stretch-goal-52359/) me ha llegado la noticia de que está en campaña en kickstarter, y enseguida me ha llamado la atención. Se trata de un juego de estilo "pixel art" con píxeles bastante gordos pero que tiene un estilo bastante definido e influenciado por aquellos juegos de aventuras, acción y plataformas. Jugaremos como Lily, una paladín que se adentrará en las mazmorras de un Castillo para aclarar la aparición de no-muertos en las villas cercanas.


Tendremos que explorar, descubrir objetos, y luchar en batallas épicas contra jefes finales para llegar a uno de los mútiples finales que tendrá.


Juzga por ti mismo:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/45dVW-ylSAg" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


 En cuanto al juego en si, ya superó una campaña en [Greenlight](http://steamcommunity.com/sharedfiles/filedetails/?id=911053972) y le quedan aún unos días para terminar su campaña en Kickstarter, donde ya han alcanzado su objetivo. Respecto a su salida en Linux, está anunciada por parte de los desarrolladores y además hay una demo disponible para probar el juego en versión pre-alfa. Puedes descargarla [desde aquí](http://www.indiedb.com/games/forsaken-castle/downloads/forsaken-castle-pre-alpha-v11-build-linux).


Si te gustan este tipo de juegos y quieres aportar, puedes hacerlo en el siguiente enlace:


<div class="resp-iframe"><iframe height="420" src="https://www.kickstarter.com/projects/duckblockgames/forsaken-castle/widget/card.html?v=2" width="220"></iframe></div>


 

