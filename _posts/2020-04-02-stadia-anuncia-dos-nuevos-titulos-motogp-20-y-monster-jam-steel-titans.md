---
author: Pato
category: Noticia
date: 2020-04-02 16:24:56
excerpt: "<p>Pr\xF3ximamente en sus navegadores con base Chromium</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Stadia/Stadia-MotoGP20-MonsterJam.webp
joomla_id: 1200
joomla_url: stadia-anuncia-dos-nuevos-titulos-motogp-20-y-monster-jam-steel-titans
layout: post
tags:
- stadia
- motogp
- monser-jam
title: "Stadia anuncia dos nuevos t\xEDtulos: MotoGP 20 y Monster Jam Steel Titans"
---
Próximamente en sus navegadores con base Chromium



En Stadia tienen novedades del motor a punto de llegar a la parrilla de salida , esta vez con dos títulos que harán las delicias de los amantes de la velocidad sobre dos ruedas y los monster Trucks y la destrucción con **MotoGP 20** (¡por fin un juego de carreras de motogp que podremos disfrutar desde Linux!) y **Monster Jam Steel Titans.**


Con **MotoGP 20** hablamos del videojuego oficial del campeonato de motos MotoGP  desarrollado por Milestone que está de vuelta en 2020 con nuevo contenido y características.


 *Experimenta todas las emociones del modo Manager de Carrera más completo que nunca y decide si unirte a un equipo de la temporada 2020 o unirte a un equipo totalmente nuevo. Nuevos desafíos históricos y un modo de juego renovado te permitirán revivir la historia de los motores.*


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/bBVTboo1ULM" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


características:


* Física más realista, gráficos mejorados, nuevos modelos 3D para los pilotos y escaneos faciales de los gerentes de equipo oficiales, además de nuevas animaciones.
* Personaliza bicicletas y trajes para encontrar la mejor combinación de patrocinador y librea, y elije colores y materiales.
* Compite con pilotos legendarios en el nuevo modo histórico.
* ¡Elige tu camino en el nuevo modo manager de Carrera!
* Compite contra pilotos más rápidos, inteligentes y precisos gracias a la inteligencia artificial neuronal mejorada basada en el aprendizaje automático.


Por otra parte, tenemos **Monster Jam Steel Titans**, un juego que hará las delicias de los que gustan de los monster trucks y aplastar cacharros esta vez de la mano de Rainbow Studios y THQ Nordic.


<div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/5-fh25KhaUU" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


*Camiones reales. Acción real. ¡Monster Jam! ¡Monster Jam Steel Titans ofrece la experiencia completa de Monster Jam para que todos la disfruten!*


*¡Todos los camiones, acrobacias, estadios, carreras y saltos masivos en un solo juego! ¡Juega en varios modos de juego, incluidos estadios y carreras al aire libre, varios desafíos de acrobacias y modos de destrucción!*


![Monster jam](https://community.stadia.com/t5/image/serverpage/image-id/1817iA1E71DA90DC2C575/image-size/large)


Estos juegos estarán disponibles próximamente en Stadia, en fecha y precios aún por determinar.


Recordaros además que **Stacks On Stacks (On Stacks)** está ya disponible gratis para los suscriptores de Stadia Pro con lo que se suma a  **Serious Sam Collection** y **Spitlings**, y si no eres suscriptor está disponible en la tienda por $12.99.


Toda la información [en este enlace](https://community.stadia.com/t5/Stadia-Community-Blog/Jump-into-the-driver-s-seat-with-new-games-coming-to-Stadia/ba-p/18153).


