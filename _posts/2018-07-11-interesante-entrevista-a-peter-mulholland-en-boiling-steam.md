---
author: leillo1975
category: Editorial
date: 2018-07-11 09:04:31
excerpt: "<p>Ekianjo nos ofrece otro genial art\xEDculo sobre este extrabajador de\
  \ Virtual Programming</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/92147a43a78b5983afe1a0dfa23dc633.webp
joomla_id: 802
joomla_url: interesante-entrevista-a-peter-mulholland-en-boiling-steam
layout: post
tags:
- virtual-progamming
- boiling-steam
- ekianjo
- peter-mulholland
title: Interesante entrevista a Peter Mulholland en Boiling Steam.
---
Ekianjo nos ofrece otro genial artículo sobre este extrabajador de Virtual Programming

Una de las webs de referencia para los que amamos los videojuegos en Linux es [Boiling Steam](https://boilingsteam.com/). En ella, a parte de de tratar diferentes temas, cada cierto tiempo se suele publicar una entrevista con algún desarrollador o persona relacionada con nuestro mundillo. Si recordais hace unos meses nos hacíamos eco de la [entrevista a Tim Besset](index.php/homepage/noticias/item/719-segun-tim-basset-el-port-de-street-fighter-v-no-esta-muerto), donde hablaba de Street Fighter V, Rocket League y su prolífica etapa en ID Software. También son remarcables sus entrevistas a [Ryan Gordon](https://boilingsteam.com/icculus-ryan-gordon-tells-us-everything-part-1/) o [LinuxVanGog.](https://boilingsteam.com/linuxvangog-talks-about-gogs-stance-on-linux/) 


En esta ocasión, Ekianjo nos trae una jugosa ronda de preguntas y respuestas de Peter Mulholland, extrabajador de Virtual Programming, donde nos habla de temas tan interesantes como sus comienzos portando juegos de Amiga para Runesoft, aspectos técnicos sobre los ports de Virtual Programming con eON, el controvertido port de "The Witcher 2" y CDPR, o la situación actual del mercado de juegos para Linux. Pero lo mejor no es que os lo explique yo, sinó que lo leais por vosotros mismos en el siguiente enlace:


[Entrevista con Peter Mulholland, ex-Virtual Programming](https://boilingsteam.com/an-interview-with-peter-mulholland-ex-vp/)
-----------------------------------------------------------------------------------------------------------------------------


Desde JugandoEnLinux nos gustaría felicitar una vez más a Boiling Steam por esta fantástica entrevista y sus geniales artículos, y animarles para que nos sigan trayendo contenidos tan suculentos como este. También recordaros que [Virtual Programming](https://www.vpltd.com/) está actualmente trabajando en el [port de MXGP3](index.php/homepage/generos/carreras/item/759-mxgp3-parece-que-llegara-a-linux-finalmente), del que por cierto no sabemos nada hace meses.


¿Habeis leido ya la entrevista? ¿Qué os parecen las palabras de Peter? Déjanos tus impresiones sobre ella en los comentarios o en nuestros grupos de [Telegram](https://t.me/jugandoenlinux) y [Discord](https://discord.gg/fgQubVY).

