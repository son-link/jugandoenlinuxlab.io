---
author: Pato
category: Terror
date: 2017-06-02 08:15:29
excerpt: <p>El juego es obra de Bloober Team creadores de 'Layers of Fear' y lo edita
  Aspyr Media</p>
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/806b6e1806566ad52df50ee6cecd90ef.webp
joomla_id: 353
joomla_url: observer-llegara-este-verano-y-hace-publicos-sus-requisitos
layout: post
tags:
- indie
- aventura
- proximamente
- terror
- gore
title: "'Observer' llegar\xE1 este pr\xF3ximo verano y hace p\xFAblicos sus requisitos"
---
El juego es obra de Bloober Team creadores de 'Layers of Fear' y lo edita Aspyr Media

Hace unos meses, en concreto en Enero [ya hablamos](index.php/homepage/generos/terror/item/271-observer-un-nuevo-juego-de-terror-de-los-creadores-de-layers-of-fear-llegara-a-linux-proximamente) sobre 'Observer', un inquietante juego de terror en un entorno cyberpunk donde las pesadillas estarán garantizadas. 


Pues hoy Aspyr ha echo público que ya tienen página en Steam, donde aparecen los requisitos mínimos para poder mover el juego en Linux/SteamOS:


*SO: Ubuntu 16.04, 17.04, SteamOS 2.0*  
*Procesador: Intel Core i3 (3.4 GHz) / AMD A8-6700 (3.1 GHz)*  
*Memoria: 8 GB de RAM*  
*Gráficos: NVIDIA GeForce 660 / AMD R9 270*  
*Almacenamiento: 10 GB de espacio disponible*  
*Notas adicionales: VRAM: 2 GB*


#### **Sinopsis de Observer:**


*¿Qué harías si te haquearan los miedos?*


*Estamos en el año 2084. Tu nombre es Daniel Lazarski y eres un detective neural de élite conocido como Observer; formas parte de una unidad de policía que funciona con financiación corporativa, cuyo objetivo es haquear e invadir la mente de los sospechosos. Cuando recibes un misterioso mensaje de tu hijo, un ingeniero de alto nivel de la poderosa Chiron Corporation con el que habías perdido el contacto, decides viajar a los sórdidos suburbios de Clase C de Cracovia para investigar. Al haquear la mente de los criminales y sus víctimas en busca de pistas, te verás obligado a revivir sus temores más siniestros. ¿Hasta dónde vas a llegar por desvelar la verdad?*


*Desarrollado Bloober Team, creadores de Layers of Fear, >observer_ es un juego de terror ciberpunk dirigido a un público adulto.*


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/oqU4hESw8m4" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


 


Si te interesan los juegos de Terror psicológico, puedes ver toda la información sobre 'Observer' en su [Wiki oficial](http://observer.gamepedia.com/Observer_Wiki), o en Steam donde aún no hay fecha definitiva o precio oficial:


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/514900/" width="646"></iframe></div>


 

