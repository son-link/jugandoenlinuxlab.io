---
author: Jugando en Linux
category: Apt-Get Update
date: 2022-02-25 17:12:12
excerpt: ''
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Nativos/Guntech2.webp
joomla_id: 1436
joomla_url: nuevos-juegos-con-soporte-nativo-25-02-2022
layout: post
tags:
- guntech-2
- oozescape
- raifu-wars
- megafactory-titan
title: Nuevos juegos con soporte nativo 25/02/2022
---

Vamos a desengrasar un poco de tanta noticia sobre Steam Deck, con el anuncio de una serie de artículos con la intención de nombrar los juegos más relevantes **con soporte nativo** lanzados en un periodo determinado de tiempo. Así, si vas buscando algo a lo que jugar siempre podrás encontrar un juego al que hincarle el diente, y que además el desarrollador se ha tomado la molestia de hacer una versión para nosotros. En principio pretendemos que la frecuencia sea semanal, si no nos fallan los tiempos. Allá vamos!


### Guntech 2


DESARROLLADOR: Jani Penttinen
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/vngaNEWXR-8" title="YouTube video player" width="560"></iframe></div>


 
[Enlace a la tienda](https://store.steampowered.com/app/1885140/Guntech_2/)
 


---


###  OOZescape


DESARROLLADOR: Team Ooze
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/EPE43I4N0GE" title="YouTube video player" width="560"></iframe></div>


[Página de la tienda](https://store.steampowered.com/app/1890010/OOZEscape/)
 


---


### Raifu Wars


DESARROLLADOR: Yotis Studios
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/3qIl6LCInkE" title="YouTube video player" width="560"></iframe></div>


[Página de la tienda](https://store.steampowered.com/app/1116110/Raifu_Wars/)




---


### MegaFactory Titan


DESARROLLADOR: Q Switched Productions, LLC
<div class="resp-iframe"><iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="315" src="https://www.youtube-nocookie.com/embed/_Qti2gbX9tc" title="YouTube video player" width="560"></iframe></div>


[Pagina de la tienda](https://store.steampowered.com/app/1803550/MegaFactory_Titan/) 
 
Sabes de algún lanzamiento importante que no hayamos puesto?


Dínoslo en nuestros canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org).

