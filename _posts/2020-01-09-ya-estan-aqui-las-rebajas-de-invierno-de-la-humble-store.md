---
author: leillo1975
category: Ofertas
date: 2020-01-09 21:03:17
excerpt: "<p>La tienda de @humble Bundle tambi\xE9n regala \"Headsnatchers\".</p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/HumbleBundle/WinterSale2020.webp
joomla_id: 1148
joomla_url: ya-estan-aqui-las-rebajas-de-invierno-de-la-humble-store
layout: post
tags:
- humble-bundle
- rebajas
- ofertas
- humble-store
- winter-sale
title: "Ya est\xE1n aqu\xED las rebajas de Invierno de la Humble Store."
---
La tienda de @humble Bundle también regala "Headsnatchers".


 Cuando aun tenemos la cartera temblando después de estas navidades, llega una vez más la esperada "[Winter Sale](https://www.humblebundle.com/store?partner=jugandoenlinux)" de nuestra tienda predilecta, donde podremos encontrar muchos de nuestros juegos favoritos a precios de saldo. Como solemos hacer siempre os dejamos una pequeña selección de algunos títulos a precios realmente interesantes:


[Shadow of the Tomb Raider: Definitive Edition](https://www.humblebundle.com/store/shadow-of-the-tomb-raider-definitive-edition?partner=jugandoenlinux): 23.99€ (-60%) - [Análisis]({{ "/posts/primeras-impresiones-de-shadow-of-the-tomb-raider" | absolute_url }})
=======================================================================================================================================================================================================================================================================


[BioShock Infinite](https://www.humblebundle.com/store/bioshock-infinite?partner=jugandoenlinux): 7.49€ (-75%)
==============================================================================================================


[Life is Strange 2 - Complete Season](https://www.humblebundle.com/store/life-is-strange-2-complete-season?partner=jugandoenlinux): 19.99€ (-50%) - [Análisis]({{ "/posts/analisis-life-is-strange-2" | absolute_url }})
===========================================================================================================================================================================================================================


[DiRT Rally](https://www.humblebundle.com/store/dirt-rally?partner=jugandoenlinux): 6.99€ (-80%) - [Análisis]({{ "/posts/analisis-dirt-rally" | absolute_url }})
==================================================================================================================================================================


[Shadow Warrior](https://www.humblebundle.com/store/shadow-warrior?partner=jugandoenlinux): 8.74€ (-75%)
========================================================================================================


[EVERSPACE™](https://www.humblebundle.com/store/everspace?partner=jugandoenlinux)**: 4.19€ (-85%) - [Análisis]({{ "/posts/analisis-everspace" | absolute_url }})**
====================================================================================================================================================================


[The Witcher® 2: Assassins of Kings Enhanced Edition](https://www.humblebundle.com/store/the-witcher-2-assassins-of-kings-enhanced-edition?partner=jugandoenlinux): 2.99€ (-85%)
================================================================================================================================================================================


[F1 2017](https://www.humblebundle.com/store/f1-2017?partner=jugandoenlinux): 12.49€ (-75%) - [Análisis]({{ "/posts/analisis-f1-2017" | absolute_url }})
==========================================================================================================================================================


[Surviving Mars](https://www.humblebundle.com/store/surviving-mars?partner=jugandoenlinux): 10.19€ (-66%)
=========================================================================================================


... y esto es una recomendación personal y un tanto polémica, pues se trata de un juego que no tiene soporte nativo , pero que funciona de maravilla con Steam Play/Proton, además de ser claramente uno de los mejores juegos de todos los tiempos:


[The Witcher® 3: Wild Hunt GAME OF THE YEAR EDITION](https://www.humblebundle.com/store/the-witcher-3-wild-hunt-game-of-the-year-edition?partner=jugandoenlinux): 14.99€ (-70%)
===============================================================================================================================================================================


Por supuesto estos solo son unos pocos ejemplos, pero hay muchos más, por lo que os recomendamos que [echeis un ojo por vuestra cuenta](https://www.humblebundle.com/store?partner=jugandoenlinux) o busqueis ese juego que hace tanto tiempo que quereis comprar. También para todos aquellos que querais haceros con "Headsnatchers" de forma totalmente gratuita, tan solo teneis que registraros (si aun no lo habeis hecho) y seguir [este enlace](https://www.humblebundle.com/store/headsnatchers-free-game?partner=jugandoenlinux). Este título multijugador tanto local como online, que no tiene soporte nativo, tiene unas críticas "Mayormente positivas", por lo que merece la pena intentar ejecutarlo con Steam Play/Proton.


Ya sabeis, si quereis indicarnos algún juego interesante a buen precio podeis hacerlo en los comentarios de este artículo, o en mensajes en nuestra comunidad de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

