---
author: Serjor
category: Noticia
date: 2018-09-21 18:00:49
excerpt: "<p>Pero como siempre, tendremos que esperar para saber cu\xE1l es</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/bb649b5e9b78ccc3d37b687d9b2104de.webp
joomla_id: 856
joomla_url: feral-interactive-no-se-queda-quieta-y-nos-anuncia-un-nuevo-juego
layout: post
tags:
- feral-interactive
- anuncios
- teaser
title: Feral Interactive no se queda quieta y nos anuncia un nuevo juego
---
Pero como siempre, tendremos que esperar para saber cuál es

Parece que el anuncio de [Proton](index.php/homepage/generos/software/item/960-steam-play-de-valve-da-un-salto-de-gigante-y-amplia-enormemente-el-catalogo-para-linux-steamos-actualizado) y las [sombras que puede implicar](https://www.reddit.com/r/linux_gaming/comments/9hgukr/the_native_linux_build_of_butcher_has_been/) no impide que la gente de Feral Interactive salten a la palestra con un nuevo port, el cuál lo han anunciado con su cripticidad habitual:



> 
> To those of you exasperated by the beautifully obscure and slightly broken Feral Radar, thanks for letting us know. We'll make sure no other clues for upcoming macOS and Linux games appear ever agai— ooh dear... <https://t.co/huXFd95Dab>  
>   
> Have a lovely weekend! ?
> 
> 
> — Feral Interactive (@feralgames) [21 de septiembre de 2018](https://twitter.com/feralgames/status/1043164011179778048?ref_src=twsrc%5Etfw)


  





Parece que aún tendremos que esperar para averiguar con qué juego nos sorprenderán esta vez, pero realmente se agradece que las editoras sigan apostando por ports nativos cuando el camino fácil sería dejar que Valve se encargara.


 


Y tú, ¿crees saber a qué juego hacen referencia esta vez? Si es así déjanoslo en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD).


 

