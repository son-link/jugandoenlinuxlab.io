---
author: leillo1975
category: Ofertas
date: 2018-09-24 17:51:15
excerpt: "<p>Humble Store ofrece grandes descuentos en t\xEDtulos de <span class=\"\
  attribution txt-mute txt-sub-antialiased txt-ellipsis vertical-align--baseline\"\
  >@AspyrMedia</span></p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/6793dfaeaedce2b3d0e76a48b5f73adc.webp
joomla_id: 858
joomla_url: grandes-ofertas-en-titulos-de-aspyr-media
layout: post
tags:
- aspyr-media
- ofertas
- humble-store
- publisher-sale
title: "Grandes ofertas en t\xEDtulos de Aspyr Media."
---
Humble Store ofrece grandes descuentos en títulos de @AspyrMedia

Una vez más la conocida tienda de Humble Bundle nos trae [suculentas ofertas](https://www.humblebundle.com/store/promo/aspyr-publisher-sale?partner=jugandoenlinux) de **títulos portados o editados** por los Texanos [Aspyr.](https://www.aspyr.com/) Como sabreis esta compañía es responsable de que juegos tan conocidos como Civilization VI o Borderlands 2 se puedan jugar en GNU-Linux. Los juegos con soporte que encontrareis en esta promoción son los siguientes:



> 
> -[Observer](https://www.humblebundle.com/store/observer?partner=jugandoenlinux) a **11,99€** (-60%)
> 
> 
> -[Civilization V](https://www.humblebundle.com/store/sid-meiers-civilization-v?partner=jugandoenlinux) a **7.49€** (-75%)
> 
> 
> -[Civilization: Beyond Earth](https://www.humblebundle.com/store/sid-meiers-civilization-beyond-earth?partner=jugandoenlinux) a **9.99€** (-75%)
> 
> 
> -[Civilization VI](https://www.humblebundle.com/store/sid-meiers-civilization-6?partner=jugandoenlinux) a **23.99€** (-60%)
> 
> 
> -[Fahrenheit Indigo Prophecy Remastered](https://www.humblebundle.com/store/fahrenheit-indigo-prophecy-remastered?partner=jugandoenlinux) a **1.79€** (-80%)
> 
> 
> -[Borderlands II GOTY](https://www.humblebundle.com/store/borderlands-2-game-of-the-year?partner=jugandoenlinux) a **9.89€** (-78%)
> 
> 
> -[Layers of Fear Masterpiece Edition](https://www.humblebundle.com/store/layers-of-fear-masterpiece-edition?partner=jugandoenlinux) a **6.44€** (-70%)
> 
> 
> -[Inner Space](https://www.humblebundle.com/store/innerspace?partner=jugandoenlinux) a **9.99€** (-50%)
> 
> 
> -[Bioshock Infinite](https://www.humblebundle.com/store/bioshock-infinite?partner=jugandoenlinux) a **7.49€** (-75%) --> Este juego fué portado a Linux por [Virtual Programming](https://www.vpltd.com/)
> 
> 
> 


También podeis encontrar bastantes **DLC's interesantes** de estos juegos a muy buen precio en estas [ofertas](https://www.humblebundle.com/store/promo/aspyr-publisher-sale?partner=jugandoenlinux). Os recordamos también que se trata de **enlaces patrocinados** y que comprando en la Humble Store colaborais con el sostenimiento de nuestra página web.


 


¿Le teneis el ojo echado a alguno de esta lista? Esta es vuestra oportunidad de conseguirlo a precios irrepetibles. Podeis decirnos cuales son vuestros favoritos en los comentarios o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).


