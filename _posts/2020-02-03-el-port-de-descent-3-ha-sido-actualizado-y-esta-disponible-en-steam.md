---
author: leillo1975
category: "Acci\xF3n"
date: 2020-02-03 16:39:51
excerpt: "<p>&nbsp;Ryan Gordon , @icculus, ha estado trabajando en el juego de nuevo.</p>\r\
  \n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Descent3/Descent3.webp
joomla_id: 1159
joomla_url: el-port-de-descent-3-ha-sido-actualizado-y-esta-disponible-en-steam
layout: post
tags:
- port
- icculus
- ryan-c-gordon
- descent-3
- loki-software
title: "El port de Descent 3 ha sido actualizado y est\xE1 disponible en Steam"
---
 Ryan Gordon , @icculus, ha estado trabajando en el juego de nuevo.


 Si recordais hace unos días os hablábamos de el re-port de [Little Racers STREET](index.php/listado-de-categorias/carreras/1157-tras-varios-anos-little-racers-street-tiene-nueva-beta) por parte de **Ethan Lee**, un juego que previamente ya tenía una versión nativa, pero que se ha adaptado y mejorado conforme a las posibilidades actuales. Gracias a la información que acabamos de leer en [PlayingTux.com](https://playingtux.com/articles/descent-3-modernized-linux-version-now-available-steam?lang=en), nos enteramos que desde hace un par de días también podremos volver a jugar nativamente un juego clásico donde los haya, como es **Descent 3**. En este tweet anunciaba el relanzamiento del juego:



> 
> "But while it lived, I got to work on so many amazing things. Being a small company, you could walk in the door and be in charge of something almost immediately, and this is how I came to be in charge of Descent 3." <https://t.co/F9NWgiz6y3>
> 
> 
> — Ryan C. Gordon (@icculus) [February 1, 2020](https://twitter.com/icculus/status/1223433329397174273?ref_src=twsrc%5Etfw)



Según nos comenta [**Ryan Gordon**](https://twitter.com/icculus?lang=es) en su [Patreon](https://www.patreon.com/posts/project-descent-33611585), el juego que había sido portado en el pasado por el mismo cuando militaba en la extinta [Loki Software](https://en.wikipedia.org/wiki/Loki_Entertainment), compañía que como sabreis **realizaba ports a Linux  hace un par de décadas**, ha sido readaptado para poder ser jugado de forma adecuada en la actualidad. Para ello **ha realizado diversos trabajos en el juego como los que podeis ver a continuación**:


-El juego se ha convertido de 32 a 64 bits


-Está construido usando SDL 2.0 funcionando en Linux y MacOS modernos (antes usaba glibc2)


-Permite usar resoluciones de pantalla más grandes, incluso en las escenas cinemáticas, mostrándose en framebuffer_object y escalandose automaticamente según corresponda.


-Al no tener el codigo fuente original del codec usado en las películas, los videos están en formato Ogg Theora, por lo que la instalación del juego ahora ocupa muchísimo menos, ya que el juego original ocupaba por culpa de estas escenas ¡la friolera de dos CD-ROM!


Si teneis el juego podeis disfrutar de la nueva versión desde ya, y si no, podeis comprar este clásico en [Steam](https://store.steampowered.com/app/273590/Descent_3/), incluyendo el Mercenary Expansion Pack. Os dejamos con un gameplay del juego:  



 <div class="resp-iframe"><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="allowfullscreen" height="450" src="https://www.youtube.com/embed/uKJzFjPkw_U" style="display: block; margin-left: auto; margin-right: auto;" width="800"></iframe></div>


Esperemos poder ver más juegos de Loki readaptados por Ryan Gordon en el futuro, ya que muchos de ellos son verdaderas joyas que merecen mucho la pena. ¿Has jugado alguna vez a algún juego de la saga Descent? ¿Qué te parece el trabajo de Ryan C. Gordon? Puedes contarnos lo que quieras sobre esta noticia en los comentarios, o en mensajes en nuestra comunidad de [Matrix](https://matrix.to/#/+jugandoenlinux.com:matrix.org), o en nuestro grupo de [Telegram](https://t.me/jugandoenlinux).

