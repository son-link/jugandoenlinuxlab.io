---
author: Pato
category: Software
date: 2019-05-30 20:24:05
excerpt: "<p>El editor del popular motor gr\xE1fico hasta ahora estaba en fase experimental</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/3859934a326e2c87d63665dfcdf6c509.webp
joomla_id: 1054
joomla_url: unity-anuncia-que-su-editor-sera-oficialmente-soportado-en-linux
layout: post
tags:
- unity3d
- unity
title: "Unity anuncia que su editor ser\xE1 oficialmente soportado en Linux"
---
El editor del popular motor gráfico hasta ahora estaba en fase experimental

Gracias a [Phoronix](https://www.phoronix.com/scan.php?page=news_item&px=Unity-Editor-Linux-2019) nos ha llegado la noticia de que Unity Tech, la compañía tras el popular motor gráfico **Unity 3D** ha anunciado que **soportará oficialmente su editor Unity para sistemas Linux.**


Algunos os preguntaréis cómo es esto, si ya existía el editor de Unity en Linux, y la respuesta es que aunque es cierto que el editor estaba disponible en en nuestro sistema favorito, esta versión era a modo experimental y aunque obtenía actualizaciones frecuentes no tenía soporte oficial. Sin embargo esto va a cambiar en el próximo lanzamiento de la versión 2019.3.


Tal y como anuncian en su último comunicado, debido al creciente interés de industrias como la audiovisual, automoción o los desarrolladores independientes que buscan poder desarrollar sobre sistemas Linux ha hecho que la empresa se plantee dar soporte y mejorar el editor en estabilidad, robustez y opciones.


Tal y como anuncian, los requisitos para ejecutar el editor Unity 3D de forma oficial serán:


* Ubuntu 16.04, 18.04
* CentOS 7
* Arquitectura x86-64
* Entorno de escritorio Gnome sobre X11
* Drivers oficiales propietarios Nvidia y drivers gráficos AMD Mesa
* Sistemas de escritorio sobre dispositivos de hardware sin emulación o capas de compatibilidad


Si estás interesado en desarrollar con Unity 3D sobre Linux y quieres saber mas, puedes visitar la [página web oficial](https://unity.com/) y el [blog del anuncio](https://blogs.unity3d.com/2019/05/30/announcing-the-unity-editor-for-linux/).

