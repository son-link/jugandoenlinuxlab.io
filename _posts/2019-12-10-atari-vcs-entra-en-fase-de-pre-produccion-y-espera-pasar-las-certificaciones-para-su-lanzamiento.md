---
author: Jugando en Linux
category: Hardware
date: 2019-12-10 17:19:21
excerpt: "<p>La consola de la m\xEDtica marca con coraz\xF3n Linux espera cumplir\
  \ plazos y llegar en el primer cuarto de este pr\xF3ximo a\xF1o</p>\r\n"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Ataribox/ataribox.webp
joomla_id: 1137
joomla_url: atari-vcs-entra-en-fase-de-pre-produccion-y-espera-pasar-las-certificaciones-para-su-lanzamiento
layout: post
title: "Atari VCS entra en fase de pre-producci\xF3n y espera pasar las certificaciones\
  \ para su lanzamiento"
---
La consola de la mítica marca con corazón Linux espera cumplir plazos y llegar en el primer cuarto de este próximo año


En un nuevo comunicado a través de su [blog en Medium](https://medium.com/@atarivcs/the-latest-from-the-atari-vcs-factory-its-all-in-the-details-now-6c8ed3365231), Atari confirma que la fase de pre-producción ya está en marcha, con lo que esperan pasar las certificaciones de ingeniería pertinentes para poder comenzar con la fabricación en masa de la consola.


Según nos cuentan, la línea de producción estaría ya en fase de pruebas, habiendo completado alrededor de un centenar de unidades que según parece estarían siendo distribuidas a los partners de Atari. Además, en la fase de pre-producción han estado refinando el proceso de fabricación y algunos sistemas del novedoso procesador para pulir ciertos aspectos de la salida de vídeo a 4K, la red WIFI o el Bluetooth.


![vcsunchain](/https://miro.medium.com/max/4032/1*Ncr7T0M2dXca_0DGuPR1Cw.webp)


Además de esto, se han testeado los mandos introduciendo algunos cambios gracias a la colaboración con la empresa PowerA solucionando algunos problemas.


![ataripad](/https://miro.medium.com/max/1200/1*fv_yFTnFT-ydBCFXuaNZxA.webp)


Si quereis saber más, podéis visitar el [blog de Atari en Medium](https://medium.com/@atarivcs/the-latest-from-the-atari-vcs-factory-its-all-in-the-details-now-6c8ed3365231).

