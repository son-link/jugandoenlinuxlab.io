---
author: Pato
category: Estrategia
date: 2018-03-07 19:35:21
excerpt: "<p>El juego de estrategia llega con una gran actualizaci\xF3n para balancear\
  \ el juego</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/d1b649d7a1dd257acec4d62941b366d2.webp
joomla_id: 671
joomla_url: northgard-ya-ha-salido-de-acceso-anticipado-y-estara-disponible-en-linux-steamos-manana-dia-8
layout: post
tags:
- indie
- multijugador
- rts
- estrategia
title: "'Northgard' ya ha salido de acceso anticipado y estar\xE1 disponible en Linux/SteamOS\
  \ ma\xF1ana d\xEDa 8"
---
El juego de estrategia llega con una gran actualización para balancear el juego

Tras un año de acceso anticipado en exclusiva para sistemas Windows, mañana mismo nos llegará un nuevo exponente de estrategia en tiempo real con este 'Nortgard'. Se trata de un juego en el que tomaremos el papel de un pueblo vikingo que descubre un nuevo continente y en el que tendremos que gestionar y luchar para conquistar el terreno frente a clanes rivales.


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/ULcpckAJqLc" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 Como podéis leer en un extenso [post de lanzamiento](http://steamcommunity.com/app/466560/discussions/0/1471967615856332848/) (en inglés), el juego acaba de recibir un extenso parche que balancea el juego y añade suculentas novedades, así como el anuncio de la llegada de las traducciones a varios idiomas, entre ellos el español.


Algunas características del juego:


* Cosntruye tu asentamiento en el nuevo continente Northgard
* Asigna a tus vikingos diversos trabajos (Granjero, luchador, comerciante...)
* Gestiona tus recursos con cuidado para sobrevivir a los duros inviernos y a los enemigos
* Expande y descubre nuevos territorios con oportunidades estratégicas únicas
* Consigue diferentes condiciones de victoria (Conquista, fama, Lore, comercio)
* Juega contra tus amigos o contra la IA con diferentes niveles de dificultad y personalidades
* Disfruta de servidores dedicados y escala puestos en los rankings para alcanzar el rango de Dios Norse!


El juego cuenta ya con una campaña llamada "Rig's Saga":


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="315" src="https://www.youtube.com/embed/Mxp7UKCnUHU" style="display: block; margin-left: auto; margin-right: auto;" width="560"></iframe></div>


 


Aparte del anuncio de la versión para Linux **para mañana día 8**, aún no se han hecho públicos los requisitos mínimos ni recomendados.


Si quieres echarle un vistazo puedes visitar su [página web oficial](http://northgard.net/), o visitar su página de Steam, donde estará próximamente en español y ahora mismo tiene un 25% de descuento por su lanzamiento:


<div class="steam-iframe"><iframe frameborder="0" height="190" src="https://store.steampowered.com/widget/466560/" width="646"></iframe></div>
 

