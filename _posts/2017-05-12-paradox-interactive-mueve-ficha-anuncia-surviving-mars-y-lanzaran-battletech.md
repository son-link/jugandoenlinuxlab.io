---
author: Pato
category: Estrategia
date: 2017-05-12 10:54:22
excerpt: "<p>Los dos juegos llegar\xE1n a Linux, el primero en 2018 y el segundo est\xE1\
  \ anunciado para este mismo a\xF1o</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/b7e607e23f1646b9ce9f7d0da4fbe580.webp
joomla_id: 317
joomla_url: paradox-interactive-mueve-ficha-anuncia-surviving-mars-y-lanzaran-battletech
layout: post
tags:
- accion
- proximamente
- simulador
- estrategia
- paradox
title: "Paradox Interactive mueve ficha: Anuncia 'Surviving Mars' y lanzar\xE1n 'Battletech'"
---
Los dos juegos llegarán a Linux, el primero en 2018 y el segundo está anunciado para este mismo año

Buenos tiempos se avecinan para los amantes de la estrategia en Linux. Paradox Interactive ha anunciado esta mañana sus dos próximos proyectos en los que están trabajando, y además nos ha dado la alegría de que saldrán para nuestro sistema favorito.


#### Surviving Mars


![Surviving mars](http://cdn.edgecast.steamstatic.com/steam/apps/464920/header.webp)


En concreto, Paradox [ha anunciado](https://twitter.com/PdxInteractive/status/862938714481324032) el desarrollo de 'Surviving Mars' [[web oficial](https://www.survivingmars.com/)], un juego en el que tendremos que construir y desarrollar colonias dentro de nuestro vecino planeta rojo al estilo de juegos de "construcción de ciudades". Habrá que buscar el lugar idóneo para ello, desarrollar edificios y estructuras, planificar los servicios básicos y gestionar todo para poder sobrevivir en un ambiente tan hostil.


A continuación el vídeo del anuncio


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/qORbw2jZM50" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


Surviving Mars vendrá con traducción al español y los requisitos mínimos y recomendados (de momento) son:


Mínimos:


SO: Ubuntu 14 x86 o superior  
Procesador: Dual-core CPU  
Memoria: 4 GB de RAM  
Gráficos: GeForce 600/AMD Radeon 7000 o superior con 1GB VRAM  
Almacenamiento: 10 GB de espacio disponible


Recomendados:


SO: Ubuntu 14 x86 o superior  
Procesador: quad-core CPUs Rapidos  
Memoria: 8 MB de RAM  
Gráficos: GeForce 970-level GPU con 4GB de VRAM  
Almacenamiento: 10 GB de espacio disponible


En cuanto a su versión Linux, está anunciada para el mismo día de lanzamiento tal y como se puede ver tanto en su [web oficial de Paradox](https://www.paradoxplaza.com/surviving-mars?utm_source=twitter&utm_medium=social&utm_campaign=suma_sm_twitter_all_20170512_ann&utm_content=tweet) como en Steam, donde podrás encontrar mas información acerca de este juego: 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/464920/" width="646"></iframe></div>


#### BATTLETECH


![BattleTech](http://cdn.akamai.steamstatic.com/steam/apps/637090/header.jpg)


Por otra parte, Paradox Interactive [ha anunciado](https://www.paradoxplaza.com/news/Battletech-Announced/) su alianza con "Harebrained Schemes" para lanzar 'Battletech" [[web oficial](http://www.battletechgame.com/)]. Se trata de un título de estratégia por turnos en los que tendremos que manejar robots "mechas" en duras batallas ya sea en solitario o en multijugador.


A "Harebrained Schemes" se le conoce sobre todo por que son los responsables de la saga 'Shadowrun' [[Steam](http://store.steampowered.com/search/?developer=Harebrained%20Schemes)] también disponible para Linux. Para 'Battletech lanzaron una campaña en [Kickstarter](https://www.kickstarter.com/projects/webeharebrained/battletech/posts/1403208) donde lograron financiarse con mas de 2,7 millones de dólares.


En cuanto al juego en si, tendremos la posibilidad de comandar a un escuadrón de "Mechs" en combates por turnos dentro de un entorno de guerra civil, gestionar nuestra compañía de mercenarios, personalizar nuestros mechas y mucho mas en modos de juego multijugador PVP o en modo un jugador donde podremos probar nuestras estrategias contra la IA del juego. A continuación el trailer:


<div class="resp-iframe"><iframe allowfullscreen="allowfullscreen" height="360" src="https://www.youtube.com/embed/RyoBwysAWVo" style="display: block; margin-left: auto; margin-right: auto;" width="640"></iframe></div>


 En cuanto a su lanzamiento en Linux, así lo han confirmado los chicos de Paradox y "si la cosa no se tuerce" tendremos el juego disponible para nuestro sistema favorito el día de lanzamiento:

> 
> [@JugandoenLinux](https://twitter.com/JugandoenLinux) If all goes to plan, yes! :)
> 
> 
> — Paradox Interactive (@PdxInteractive) [12 de mayo de 2017](https://twitter.com/PdxInteractive/status/862968879454334976)


Además, tanto en los [foros oficiales](https://community.battletechgame.com/forums/threads/3533) del juego ya hay hilos con cuestiones técnicas de Linux.


'Battletech' llegará en algún momento de este mismo año y parece que no estará traducido al español, aunque esto puede cambiar al ser un proyecto aún en desarrollo. Puedes ver mas información en su web oficial o en su página de Steam:
 

<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/637090/" width="646"></iframe></div>


¿Qué te parecen los anuncios de Paradox Interactive? ¿Te van los títulos de estratégia?


Cuéntamelo en los comentarios, o en los canales de Jugando en Linux de [Telegram](https://telegram.me/jugandoenlinux) o [Discord](https://discord.gg/ftcmBjD).

