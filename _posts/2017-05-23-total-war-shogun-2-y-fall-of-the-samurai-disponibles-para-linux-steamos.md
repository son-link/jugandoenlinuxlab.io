---
author: leillo1975
category: Estrategia
date: 2017-05-23 08:14:19
excerpt: "<p>La serie de juegos Total War nos lleva en esta ocasi\xF3n al Jap\xF3\
  n feudal</p>"
image: https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/joomlart/article/0fcccca5b510e5bb86a94045046d252d.webp
joomla_id: 334
joomla_url: total-war-shogun-2-y-fall-of-the-samurai-disponibles-para-linux-steamos
layout: post
tags:
- feral-interactive
- total-war
- shogun-2
- fall-of-the-samurai
- creative-assembly
title: 'Total War: Shogun 2 y Fall of the Samurai disponibles para Linux/SteamOS'
---
La serie de juegos Total War nos lleva en esta ocasión al Japón feudal

[El lunes pasado](index.php/homepage/generos/estrategia/item/442-total-war-shogun-2-y-fall-of-the-samurai-el-23-de-mayo) nos llevábamos "la sorpresa" de que [**TW:Shogun 2**](http://www.feralinteractive.com/es/games/shogun2tw/) vendría a nuestros PC's. Con este lanzamiento, los amantes de los juegos Total War, ya disponemos de un título más que disfrutar (Empire, Medieval II, Warhammer, Attila).De la mano de [Feral Interactive](http://www.feralinteractive.com/es/), como viene siendo habitual con los juegos de **Creative Assembly** nos llega este juego publicado por **SEGA** en el que nos llevarán al antiguo Japón, donde rivalizaremos con otros clanes para dominar el país mediante guerras, diplomacia, comercio... 


 


También estará disponible su expansión independiente **Fall Of The Samurai**, donde viviremos la lucha de los últimos vestigios de los Samurais con los ejércitos modernos en el siglo XIX, introduciendo muchismas novedades, entre las que se encuentran la inclusión potencias extranjeras que tomarán parte en la contienda, nuevos clanes tanto imperiales como del Shogunato, múltiples actualizaciones y añadidos de unidades, cambios en el mapa de Japón, inclusión del Ferrocarril, ciudades totalmente remodeladas, etc.


 


En cuanto el apartado multiplayer podremos disfrutar de **juego cooperativo** y **uno contra otro**, así como de juego a través de **LAN**. En este caso nos veremos condicionados a jugar unicamente con usuarios de Linux, pero Feral está trabajando en un parche que permita jugar también con usuarios de Mac en un futuro.


 


![shogun02](https://gitlab.com/jugandoenlinux/imagenes/-/raw/main/Articulos/Shogun01.webp)*Las batallas en tiempo real son como siempre espectaculares*
 


Haciendo uso de **OpenGL** y con unos **requisitos técnicos comedidos** podremos disfrutar de este título si disponemos de la siguiente configuración:



> 
> **Requisitos mínimos:**
> 
> 
> Procesador 2GHz con SteamOS 2.0 / Ubuntu 16.04 o posterior  
> 4GB de RAM  
> Tarjeta gráfica Nvidia 600 series / AMD 6000 series / Intel Iris Pro graphics de 1GB o superior
> 
> 
> **Requisitos recomendados:**
> 
> 
> Procesador 3GHz con SteamOS 2.0 / Ubuntu 16.04 o posterior  
> 4GB de RAM   
> Tarjeta gráfica Nvidia 700 series / AMD R7 series de 2GB o superior
> 
> 
> **Nota:** Las GPUs de NVIDIA requieren la versión del controlador 375.26 o posterior. Las GPUs de AMD e Intel requieren MESA 17.1.
> 
> 
> 


**Descripción Oficial:**




> 
> El supremo arte de la guerra.
> 
> 
> Como líder de uno de los nueve clanes en rivalidad, usa la ambición política, la brillantez militar y una implacable astucia para unificar al Japón del siglo XVI bajo un único sogún: tú. Domina las habilidades únicas de tu clan en artes marciales para triunfar en batallas espectaculares en tiempo real y controlar el mapa de campaña por turnos.
> 
> 
> Con estrategias de juego inspiradas en el código de honor Samurái, Total War: SHOGUN 2 es una representación dramática y detallada de acontecimientos épicos y seña de identidad única de la gran herencia de la serie Total War.
> 
> 
> * Toma el mando de samuráis, ninjas y monjes guerreros en espectaculares batallas en tiempo real a través de montañas nevadas, alrededor de ciudadelas en alto fortificadas y en campos de cerezos en flor.
> * Haz valer tu dominio sobre un mapa de campaña basado en turnos desarrollando infraestructuras y nuevas tecnologías, y usando la diplomacia inteligentemente y a agentes en cubierto para que los clanes rivales se sometan a tu voluntad.
> * Actúa con sabiduría en tus negociaciones, sé misericorde en el campo de batalla y sigue los postulados del Bushido y el Qi para ganar honor, fortalecer los vínculos de lealtad de tus generales y mejorar tu posición con respecto a otros clanes.
> * Afina tu estrategia con la ayuda de la enciclopedia del juego, que contiene tanto información sobre el juego como detalles históricos, y ejecútala después utilizando el intuitivo y estético interfaz inspirado en los grabados japoneses en madera.
> * Reta a oponentes humanos en línea en batallas multijugador de Linux a Linux, alíate en campañas cooperativas y únete a conflictos épicos con clanes en el modo “Conquista del avatar”, en el que puedes formar equipo con otros jugadores para hacerte con territorios a lo largo del exclusive mapa de campaña.
> 
> 
> 



Si quereis comprar Total War: Shogun 2 y Fall Of The Samurai podeis hacerlo directamente en [**la tienda de Feral**](https://store.feralinteractive.com/es/mac-linux-games/shogun2twcollection/) o en Steam:


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/201270/7587/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


<div class="steam-iframe"><iframe height="190" src="https://store.steampowered.com/widget/201271/13258/" style="display: block; margin-left: auto; margin-right: auto;" width="646"></iframe></div>


 


Nosotros por nuestra parte, **estamos elaborando un completo análisis** de este título para ofreceros lo antes posible nuestra impresión sobre este título y su funcionamiento en GNU/Linux, y en donde trataremos de desgranar las virtudes y defectos de este juego. Permaneced atentos!


 


<div class="resp-iframe"><iframe allowfullscreen="" frameborder="0" height="315" src="https://www.youtube.com/embed/PzCRn6S4s1Q" width="560"></iframe></div>
 


¿Qué te parece este título? ¿Piensas jugarlo? Deja tu mensaje en los comentarios o en nuestros canales de [Telegram](https://telegram.me/jugandoenlinux) y [Discord](https://discord.gg/ftcmBjD)

